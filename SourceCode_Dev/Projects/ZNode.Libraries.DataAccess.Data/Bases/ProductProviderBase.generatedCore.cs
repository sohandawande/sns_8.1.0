﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ProductProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class ProductProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.Product, ZNode.Libraries.DataAccess.Entities.ProductKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ProductKey key)
		{
			return Delete(transactionManager, key.ProductID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_productID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _productID)
		{
			return Delete(null, _productID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _productID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProduct_ZNodeProductReviewState key.
		///		FK_ZNodeProduct_ZNodeProductReviewState Description: 
		/// </summary>
		/// <param name="_reviewStateID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Product objects.</returns>
		public TList<Product> GetByReviewStateID(System.Int32? _reviewStateID)
		{
			int count = -1;
			return GetByReviewStateID(_reviewStateID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProduct_ZNodeProductReviewState key.
		///		FK_ZNodeProduct_ZNodeProductReviewState Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_reviewStateID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Product objects.</returns>
		/// <remarks></remarks>
		public TList<Product> GetByReviewStateID(TransactionManager transactionManager, System.Int32? _reviewStateID)
		{
			int count = -1;
			return GetByReviewStateID(transactionManager, _reviewStateID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProduct_ZNodeProductReviewState key.
		///		FK_ZNodeProduct_ZNodeProductReviewState Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_reviewStateID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Product objects.</returns>
		public TList<Product> GetByReviewStateID(TransactionManager transactionManager, System.Int32? _reviewStateID, int start, int pageLength)
		{
			int count = -1;
			return GetByReviewStateID(transactionManager, _reviewStateID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProduct_ZNodeProductReviewState key.
		///		fKZNodeProductZNodeProductReviewState Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_reviewStateID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Product objects.</returns>
		public TList<Product> GetByReviewStateID(System.Int32? _reviewStateID, int start, int pageLength)
		{
			int count =  -1;
			return GetByReviewStateID(null, _reviewStateID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProduct_ZNodeProductReviewState key.
		///		fKZNodeProductZNodeProductReviewState Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_reviewStateID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Product objects.</returns>
		public TList<Product> GetByReviewStateID(System.Int32? _reviewStateID, int start, int pageLength,out int count)
		{
			return GetByReviewStateID(null, _reviewStateID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProduct_ZNodeProductReviewState key.
		///		FK_ZNodeProduct_ZNodeProductReviewState Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_reviewStateID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Product objects.</returns>
		public abstract TList<Product> GetByReviewStateID(TransactionManager transactionManager, System.Int32? _reviewStateID, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.Product Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ProductKey key, int start, int pageLength)
		{
			return GetByProductID(transactionManager, key.ProductID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeProduct_AccountID index.
		/// </summary>
		/// <param name="_accountID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetByAccountID(System.Int32? _accountID)
		{
			int count = -1;
			return GetByAccountID(null,_accountID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_AccountID index.
		/// </summary>
		/// <param name="_accountID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetByAccountID(System.Int32? _accountID, int start, int pageLength)
		{
			int count = -1;
			return GetByAccountID(null, _accountID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_AccountID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accountID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetByAccountID(TransactionManager transactionManager, System.Int32? _accountID)
		{
			int count = -1;
			return GetByAccountID(transactionManager, _accountID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_AccountID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accountID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetByAccountID(TransactionManager transactionManager, System.Int32? _accountID, int start, int pageLength)
		{
			int count = -1;
			return GetByAccountID(transactionManager, _accountID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_AccountID index.
		/// </summary>
		/// <param name="_accountID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetByAccountID(System.Int32? _accountID, int start, int pageLength, out int count)
		{
			return GetByAccountID(null, _accountID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_AccountID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accountID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public abstract TList<Product> GetByAccountID(TransactionManager transactionManager, System.Int32? _accountID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeProduct_ActiveInd index.
		/// </summary>
		/// <param name="_activeInd"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetByActiveInd(System.Boolean _activeInd)
		{
			int count = -1;
			return GetByActiveInd(null,_activeInd, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_ActiveInd index.
		/// </summary>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetByActiveInd(System.Boolean _activeInd, int start, int pageLength)
		{
			int count = -1;
			return GetByActiveInd(null, _activeInd, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_ActiveInd index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_activeInd"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetByActiveInd(TransactionManager transactionManager, System.Boolean _activeInd)
		{
			int count = -1;
			return GetByActiveInd(transactionManager, _activeInd, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_ActiveInd index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetByActiveInd(TransactionManager transactionManager, System.Boolean _activeInd, int start, int pageLength)
		{
			int count = -1;
			return GetByActiveInd(transactionManager, _activeInd, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_ActiveInd index.
		/// </summary>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetByActiveInd(System.Boolean _activeInd, int start, int pageLength, out int count)
		{
			return GetByActiveInd(null, _activeInd, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_ActiveInd index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public abstract TList<Product> GetByActiveInd(TransactionManager transactionManager, System.Boolean _activeInd, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeProduct_FeaturedInd index.
		/// </summary>
		/// <param name="_featuredInd"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetByFeaturedInd(System.Boolean _featuredInd)
		{
			int count = -1;
			return GetByFeaturedInd(null,_featuredInd, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_FeaturedInd index.
		/// </summary>
		/// <param name="_featuredInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetByFeaturedInd(System.Boolean _featuredInd, int start, int pageLength)
		{
			int count = -1;
			return GetByFeaturedInd(null, _featuredInd, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_FeaturedInd index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_featuredInd"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetByFeaturedInd(TransactionManager transactionManager, System.Boolean _featuredInd)
		{
			int count = -1;
			return GetByFeaturedInd(transactionManager, _featuredInd, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_FeaturedInd index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_featuredInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetByFeaturedInd(TransactionManager transactionManager, System.Boolean _featuredInd, int start, int pageLength)
		{
			int count = -1;
			return GetByFeaturedInd(transactionManager, _featuredInd, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_FeaturedInd index.
		/// </summary>
		/// <param name="_featuredInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetByFeaturedInd(System.Boolean _featuredInd, int start, int pageLength, out int count)
		{
			return GetByFeaturedInd(null, _featuredInd, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_FeaturedInd index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_featuredInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public abstract TList<Product> GetByFeaturedInd(TransactionManager transactionManager, System.Boolean _featuredInd, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZnodeProduct_HomepageSpecial index.
		/// </summary>
		/// <param name="_homepageSpecial"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetByHomepageSpecial(System.Boolean _homepageSpecial)
		{
			int count = -1;
			return GetByHomepageSpecial(null,_homepageSpecial, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZnodeProduct_HomepageSpecial index.
		/// </summary>
		/// <param name="_homepageSpecial"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetByHomepageSpecial(System.Boolean _homepageSpecial, int start, int pageLength)
		{
			int count = -1;
			return GetByHomepageSpecial(null, _homepageSpecial, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZnodeProduct_HomepageSpecial index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_homepageSpecial"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetByHomepageSpecial(TransactionManager transactionManager, System.Boolean _homepageSpecial)
		{
			int count = -1;
			return GetByHomepageSpecial(transactionManager, _homepageSpecial, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZnodeProduct_HomepageSpecial index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_homepageSpecial"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetByHomepageSpecial(TransactionManager transactionManager, System.Boolean _homepageSpecial, int start, int pageLength)
		{
			int count = -1;
			return GetByHomepageSpecial(transactionManager, _homepageSpecial, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZnodeProduct_HomepageSpecial index.
		/// </summary>
		/// <param name="_homepageSpecial"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetByHomepageSpecial(System.Boolean _homepageSpecial, int start, int pageLength, out int count)
		{
			return GetByHomepageSpecial(null, _homepageSpecial, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZnodeProduct_HomepageSpecial index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_homepageSpecial"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public abstract TList<Product> GetByHomepageSpecial(TransactionManager transactionManager, System.Boolean _homepageSpecial, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeProduct_ManufacturerID index.
		/// </summary>
		/// <param name="_manufacturerID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetByManufacturerID(System.Int32? _manufacturerID)
		{
			int count = -1;
			return GetByManufacturerID(null,_manufacturerID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_ManufacturerID index.
		/// </summary>
		/// <param name="_manufacturerID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetByManufacturerID(System.Int32? _manufacturerID, int start, int pageLength)
		{
			int count = -1;
			return GetByManufacturerID(null, _manufacturerID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_ManufacturerID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_manufacturerID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetByManufacturerID(TransactionManager transactionManager, System.Int32? _manufacturerID)
		{
			int count = -1;
			return GetByManufacturerID(transactionManager, _manufacturerID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_ManufacturerID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_manufacturerID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetByManufacturerID(TransactionManager transactionManager, System.Int32? _manufacturerID, int start, int pageLength)
		{
			int count = -1;
			return GetByManufacturerID(transactionManager, _manufacturerID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_ManufacturerID index.
		/// </summary>
		/// <param name="_manufacturerID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetByManufacturerID(System.Int32? _manufacturerID, int start, int pageLength, out int count)
		{
			return GetByManufacturerID(null, _manufacturerID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_ManufacturerID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_manufacturerID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public abstract TList<Product> GetByManufacturerID(TransactionManager transactionManager, System.Int32? _manufacturerID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeProduct_NewProductInd index.
		/// </summary>
		/// <param name="_newProductInd"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetByNewProductInd(System.Boolean? _newProductInd)
		{
			int count = -1;
			return GetByNewProductInd(null,_newProductInd, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_NewProductInd index.
		/// </summary>
		/// <param name="_newProductInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetByNewProductInd(System.Boolean? _newProductInd, int start, int pageLength)
		{
			int count = -1;
			return GetByNewProductInd(null, _newProductInd, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_NewProductInd index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_newProductInd"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetByNewProductInd(TransactionManager transactionManager, System.Boolean? _newProductInd)
		{
			int count = -1;
			return GetByNewProductInd(transactionManager, _newProductInd, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_NewProductInd index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_newProductInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetByNewProductInd(TransactionManager transactionManager, System.Boolean? _newProductInd, int start, int pageLength)
		{
			int count = -1;
			return GetByNewProductInd(transactionManager, _newProductInd, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_NewProductInd index.
		/// </summary>
		/// <param name="_newProductInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetByNewProductInd(System.Boolean? _newProductInd, int start, int pageLength, out int count)
		{
			return GetByNewProductInd(null, _newProductInd, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_NewProductInd index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_newProductInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public abstract TList<Product> GetByNewProductInd(TransactionManager transactionManager, System.Boolean? _newProductInd, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeProduct_PortalID index.
		/// </summary>
		/// <param name="_portalID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetByPortalID(System.Int32? _portalID)
		{
			int count = -1;
			return GetByPortalID(null,_portalID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_PortalID index.
		/// </summary>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetByPortalID(System.Int32? _portalID, int start, int pageLength)
		{
			int count = -1;
			return GetByPortalID(null, _portalID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_PortalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetByPortalID(TransactionManager transactionManager, System.Int32? _portalID)
		{
			int count = -1;
			return GetByPortalID(transactionManager, _portalID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_PortalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetByPortalID(TransactionManager transactionManager, System.Int32? _portalID, int start, int pageLength)
		{
			int count = -1;
			return GetByPortalID(transactionManager, _portalID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_PortalID index.
		/// </summary>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetByPortalID(System.Int32? _portalID, int start, int pageLength, out int count)
		{
			return GetByPortalID(null, _portalID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_PortalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public abstract TList<Product> GetByPortalID(TransactionManager transactionManager, System.Int32? _portalID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeProduct_ProductNum index.
		/// </summary>
		/// <param name="_productNum"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetByProductNum(System.String _productNum)
		{
			int count = -1;
			return GetByProductNum(null,_productNum, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_ProductNum index.
		/// </summary>
		/// <param name="_productNum"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetByProductNum(System.String _productNum, int start, int pageLength)
		{
			int count = -1;
			return GetByProductNum(null, _productNum, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_ProductNum index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productNum"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetByProductNum(TransactionManager transactionManager, System.String _productNum)
		{
			int count = -1;
			return GetByProductNum(transactionManager, _productNum, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_ProductNum index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productNum"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetByProductNum(TransactionManager transactionManager, System.String _productNum, int start, int pageLength)
		{
			int count = -1;
			return GetByProductNum(transactionManager, _productNum, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_ProductNum index.
		/// </summary>
		/// <param name="_productNum"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetByProductNum(System.String _productNum, int start, int pageLength, out int count)
		{
			return GetByProductNum(null, _productNum, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_ProductNum index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productNum"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public abstract TList<Product> GetByProductNum(TransactionManager transactionManager, System.String _productNum, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeProduct_ProductTypeID index.
		/// </summary>
		/// <param name="_productTypeID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetByProductTypeID(System.Int32 _productTypeID)
		{
			int count = -1;
			return GetByProductTypeID(null,_productTypeID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_ProductTypeID index.
		/// </summary>
		/// <param name="_productTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetByProductTypeID(System.Int32 _productTypeID, int start, int pageLength)
		{
			int count = -1;
			return GetByProductTypeID(null, _productTypeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_ProductTypeID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productTypeID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetByProductTypeID(TransactionManager transactionManager, System.Int32 _productTypeID)
		{
			int count = -1;
			return GetByProductTypeID(transactionManager, _productTypeID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_ProductTypeID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetByProductTypeID(TransactionManager transactionManager, System.Int32 _productTypeID, int start, int pageLength)
		{
			int count = -1;
			return GetByProductTypeID(transactionManager, _productTypeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_ProductTypeID index.
		/// </summary>
		/// <param name="_productTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetByProductTypeID(System.Int32 _productTypeID, int start, int pageLength, out int count)
		{
			return GetByProductTypeID(null, _productTypeID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_ProductTypeID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public abstract TList<Product> GetByProductTypeID(TransactionManager transactionManager, System.Int32 _productTypeID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeProduct_RetailPrice index.
		/// </summary>
		/// <param name="_retailPrice"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetByRetailPrice(System.Decimal? _retailPrice)
		{
			int count = -1;
			return GetByRetailPrice(null,_retailPrice, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_RetailPrice index.
		/// </summary>
		/// <param name="_retailPrice"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetByRetailPrice(System.Decimal? _retailPrice, int start, int pageLength)
		{
			int count = -1;
			return GetByRetailPrice(null, _retailPrice, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_RetailPrice index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_retailPrice"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetByRetailPrice(TransactionManager transactionManager, System.Decimal? _retailPrice)
		{
			int count = -1;
			return GetByRetailPrice(transactionManager, _retailPrice, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_RetailPrice index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_retailPrice"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetByRetailPrice(TransactionManager transactionManager, System.Decimal? _retailPrice, int start, int pageLength)
		{
			int count = -1;
			return GetByRetailPrice(transactionManager, _retailPrice, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_RetailPrice index.
		/// </summary>
		/// <param name="_retailPrice"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetByRetailPrice(System.Decimal? _retailPrice, int start, int pageLength, out int count)
		{
			return GetByRetailPrice(null, _retailPrice, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_RetailPrice index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_retailPrice"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public abstract TList<Product> GetByRetailPrice(TransactionManager transactionManager, System.Decimal? _retailPrice, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeProduct_SalePrice index.
		/// </summary>
		/// <param name="_salePrice"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetBySalePrice(System.Decimal? _salePrice)
		{
			int count = -1;
			return GetBySalePrice(null,_salePrice, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_SalePrice index.
		/// </summary>
		/// <param name="_salePrice"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetBySalePrice(System.Decimal? _salePrice, int start, int pageLength)
		{
			int count = -1;
			return GetBySalePrice(null, _salePrice, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_SalePrice index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_salePrice"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetBySalePrice(TransactionManager transactionManager, System.Decimal? _salePrice)
		{
			int count = -1;
			return GetBySalePrice(transactionManager, _salePrice, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_SalePrice index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_salePrice"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetBySalePrice(TransactionManager transactionManager, System.Decimal? _salePrice, int start, int pageLength)
		{
			int count = -1;
			return GetBySalePrice(transactionManager, _salePrice, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_SalePrice index.
		/// </summary>
		/// <param name="_salePrice"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetBySalePrice(System.Decimal? _salePrice, int start, int pageLength, out int count)
		{
			return GetBySalePrice(null, _salePrice, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_SalePrice index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_salePrice"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public abstract TList<Product> GetBySalePrice(TransactionManager transactionManager, System.Decimal? _salePrice, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeProduct_SEOURL index.
		/// </summary>
		/// <param name="_sEOURL"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetBySEOURL(System.String _sEOURL)
		{
			int count = -1;
			return GetBySEOURL(null,_sEOURL, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_SEOURL index.
		/// </summary>
		/// <param name="_sEOURL"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetBySEOURL(System.String _sEOURL, int start, int pageLength)
		{
			int count = -1;
			return GetBySEOURL(null, _sEOURL, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_SEOURL index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_sEOURL"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetBySEOURL(TransactionManager transactionManager, System.String _sEOURL)
		{
			int count = -1;
			return GetBySEOURL(transactionManager, _sEOURL, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_SEOURL index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_sEOURL"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetBySEOURL(TransactionManager transactionManager, System.String _sEOURL, int start, int pageLength)
		{
			int count = -1;
			return GetBySEOURL(transactionManager, _sEOURL, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_SEOURL index.
		/// </summary>
		/// <param name="_sEOURL"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetBySEOURL(System.String _sEOURL, int start, int pageLength, out int count)
		{
			return GetBySEOURL(null, _sEOURL, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_SEOURL index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_sEOURL"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public abstract TList<Product> GetBySEOURL(TransactionManager transactionManager, System.String _sEOURL, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeProduct_ShippingRuleTypeID index.
		/// </summary>
		/// <param name="_shippingRuleTypeID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetByShippingRuleTypeID(System.Int32? _shippingRuleTypeID)
		{
			int count = -1;
			return GetByShippingRuleTypeID(null,_shippingRuleTypeID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_ShippingRuleTypeID index.
		/// </summary>
		/// <param name="_shippingRuleTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetByShippingRuleTypeID(System.Int32? _shippingRuleTypeID, int start, int pageLength)
		{
			int count = -1;
			return GetByShippingRuleTypeID(null, _shippingRuleTypeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_ShippingRuleTypeID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_shippingRuleTypeID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetByShippingRuleTypeID(TransactionManager transactionManager, System.Int32? _shippingRuleTypeID)
		{
			int count = -1;
			return GetByShippingRuleTypeID(transactionManager, _shippingRuleTypeID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_ShippingRuleTypeID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_shippingRuleTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetByShippingRuleTypeID(TransactionManager transactionManager, System.Int32? _shippingRuleTypeID, int start, int pageLength)
		{
			int count = -1;
			return GetByShippingRuleTypeID(transactionManager, _shippingRuleTypeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_ShippingRuleTypeID index.
		/// </summary>
		/// <param name="_shippingRuleTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetByShippingRuleTypeID(System.Int32? _shippingRuleTypeID, int start, int pageLength, out int count)
		{
			return GetByShippingRuleTypeID(null, _shippingRuleTypeID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_ShippingRuleTypeID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_shippingRuleTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public abstract TList<Product> GetByShippingRuleTypeID(TransactionManager transactionManager, System.Int32? _shippingRuleTypeID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeProduct_SupplierID index.
		/// </summary>
		/// <param name="_supplierID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetBySupplierID(System.Int32? _supplierID)
		{
			int count = -1;
			return GetBySupplierID(null,_supplierID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_SupplierID index.
		/// </summary>
		/// <param name="_supplierID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetBySupplierID(System.Int32? _supplierID, int start, int pageLength)
		{
			int count = -1;
			return GetBySupplierID(null, _supplierID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_SupplierID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_supplierID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetBySupplierID(TransactionManager transactionManager, System.Int32? _supplierID)
		{
			int count = -1;
			return GetBySupplierID(transactionManager, _supplierID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_SupplierID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_supplierID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetBySupplierID(TransactionManager transactionManager, System.Int32? _supplierID, int start, int pageLength)
		{
			int count = -1;
			return GetBySupplierID(transactionManager, _supplierID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_SupplierID index.
		/// </summary>
		/// <param name="_supplierID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetBySupplierID(System.Int32? _supplierID, int start, int pageLength, out int count)
		{
			return GetBySupplierID(null, _supplierID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_SupplierID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_supplierID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public abstract TList<Product> GetBySupplierID(TransactionManager transactionManager, System.Int32? _supplierID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeProduct_TaxClassID index.
		/// </summary>
		/// <param name="_taxClassID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetByTaxClassID(System.Int32? _taxClassID)
		{
			int count = -1;
			return GetByTaxClassID(null,_taxClassID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_TaxClassID index.
		/// </summary>
		/// <param name="_taxClassID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetByTaxClassID(System.Int32? _taxClassID, int start, int pageLength)
		{
			int count = -1;
			return GetByTaxClassID(null, _taxClassID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_TaxClassID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_taxClassID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetByTaxClassID(TransactionManager transactionManager, System.Int32? _taxClassID)
		{
			int count = -1;
			return GetByTaxClassID(transactionManager, _taxClassID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_TaxClassID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_taxClassID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetByTaxClassID(TransactionManager transactionManager, System.Int32? _taxClassID, int start, int pageLength)
		{
			int count = -1;
			return GetByTaxClassID(transactionManager, _taxClassID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_TaxClassID index.
		/// </summary>
		/// <param name="_taxClassID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetByTaxClassID(System.Int32? _taxClassID, int start, int pageLength, out int count)
		{
			return GetByTaxClassID(null, _taxClassID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_TaxClassID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_taxClassID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public abstract TList<Product> GetByTaxClassID(TransactionManager transactionManager, System.Int32? _taxClassID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeProduct_WholesalePrice index.
		/// </summary>
		/// <param name="_wholesalePrice"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetByWholesalePrice(System.Decimal? _wholesalePrice)
		{
			int count = -1;
			return GetByWholesalePrice(null,_wholesalePrice, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_WholesalePrice index.
		/// </summary>
		/// <param name="_wholesalePrice"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetByWholesalePrice(System.Decimal? _wholesalePrice, int start, int pageLength)
		{
			int count = -1;
			return GetByWholesalePrice(null, _wholesalePrice, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_WholesalePrice index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_wholesalePrice"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetByWholesalePrice(TransactionManager transactionManager, System.Decimal? _wholesalePrice)
		{
			int count = -1;
			return GetByWholesalePrice(transactionManager, _wholesalePrice, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_WholesalePrice index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_wholesalePrice"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetByWholesalePrice(TransactionManager transactionManager, System.Decimal? _wholesalePrice, int start, int pageLength)
		{
			int count = -1;
			return GetByWholesalePrice(transactionManager, _wholesalePrice, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_WholesalePrice index.
		/// </summary>
		/// <param name="_wholesalePrice"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public TList<Product> GetByWholesalePrice(System.Decimal? _wholesalePrice, int start, int pageLength, out int count)
		{
			return GetByWholesalePrice(null, _wholesalePrice, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProduct_WholesalePrice index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_wholesalePrice"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Product&gt;"/> class.</returns>
		public abstract TList<Product> GetByWholesalePrice(TransactionManager transactionManager, System.Decimal? _wholesalePrice, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key SC_Product_PK index.
		/// </summary>
		/// <param name="_productID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Product"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Product GetByProductID(System.Int32 _productID)
		{
			int count = -1;
			return GetByProductID(null,_productID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the SC_Product_PK index.
		/// </summary>
		/// <param name="_productID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Product"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Product GetByProductID(System.Int32 _productID, int start, int pageLength)
		{
			int count = -1;
			return GetByProductID(null, _productID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the SC_Product_PK index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Product"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Product GetByProductID(TransactionManager transactionManager, System.Int32 _productID)
		{
			int count = -1;
			return GetByProductID(transactionManager, _productID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the SC_Product_PK index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Product"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Product GetByProductID(TransactionManager transactionManager, System.Int32 _productID, int start, int pageLength)
		{
			int count = -1;
			return GetByProductID(transactionManager, _productID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the SC_Product_PK index.
		/// </summary>
		/// <param name="_productID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Product"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Product GetByProductID(System.Int32 _productID, int start, int pageLength, out int count)
		{
			return GetByProductID(null, _productID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the SC_Product_PK index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Product"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.Product GetByProductID(TransactionManager transactionManager, System.Int32 _productID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;Product&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;Product&gt;"/></returns>
		public static TList<Product> Fill(IDataReader reader, TList<Product> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.Product c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("Product")
					.Append("|").Append((System.Int32)reader[((int)ProductColumn.ProductID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<Product>(
					key.ToString(), // EntityTrackingKey
					"Product",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.Product();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.ProductID = (System.Int32)reader[((int)ProductColumn.ProductID - 1)];
					c.Name = (System.String)reader[((int)ProductColumn.Name - 1)];
					c.ShortDescription = (reader.IsDBNull(((int)ProductColumn.ShortDescription - 1)))?null:(System.String)reader[((int)ProductColumn.ShortDescription - 1)];
					c.Description = (System.String)reader[((int)ProductColumn.Description - 1)];
					c.FeaturesDesc = (reader.IsDBNull(((int)ProductColumn.FeaturesDesc - 1)))?null:(System.String)reader[((int)ProductColumn.FeaturesDesc - 1)];
					c.ProductNum = (System.String)reader[((int)ProductColumn.ProductNum - 1)];
					c.ProductTypeID = (System.Int32)reader[((int)ProductColumn.ProductTypeID - 1)];
					c.RetailPrice = (reader.IsDBNull(((int)ProductColumn.RetailPrice - 1)))?null:(System.Decimal?)reader[((int)ProductColumn.RetailPrice - 1)];
					c.SalePrice = (reader.IsDBNull(((int)ProductColumn.SalePrice - 1)))?null:(System.Decimal?)reader[((int)ProductColumn.SalePrice - 1)];
					c.WholesalePrice = (reader.IsDBNull(((int)ProductColumn.WholesalePrice - 1)))?null:(System.Decimal?)reader[((int)ProductColumn.WholesalePrice - 1)];
					c.ImageFile = (reader.IsDBNull(((int)ProductColumn.ImageFile - 1)))?null:(System.String)reader[((int)ProductColumn.ImageFile - 1)];
					c.ImageAltTag = (reader.IsDBNull(((int)ProductColumn.ImageAltTag - 1)))?null:(System.String)reader[((int)ProductColumn.ImageAltTag - 1)];
					c.Weight = (reader.IsDBNull(((int)ProductColumn.Weight - 1)))?null:(System.Decimal?)reader[((int)ProductColumn.Weight - 1)];
					c.Length = (reader.IsDBNull(((int)ProductColumn.Length - 1)))?null:(System.Decimal?)reader[((int)ProductColumn.Length - 1)];
					c.Width = (reader.IsDBNull(((int)ProductColumn.Width - 1)))?null:(System.Decimal?)reader[((int)ProductColumn.Width - 1)];
					c.Height = (reader.IsDBNull(((int)ProductColumn.Height - 1)))?null:(System.Decimal?)reader[((int)ProductColumn.Height - 1)];
					c.BeginActiveDate = (reader.IsDBNull(((int)ProductColumn.BeginActiveDate - 1)))?null:(System.DateTime?)reader[((int)ProductColumn.BeginActiveDate - 1)];
					c.EndActiveDate = (reader.IsDBNull(((int)ProductColumn.EndActiveDate - 1)))?null:(System.DateTime?)reader[((int)ProductColumn.EndActiveDate - 1)];
					c.DisplayOrder = (reader.IsDBNull(((int)ProductColumn.DisplayOrder - 1)))?null:(System.Int32?)reader[((int)ProductColumn.DisplayOrder - 1)];
					c.ActiveInd = (System.Boolean)reader[((int)ProductColumn.ActiveInd - 1)];
					c.CallForPricing = (System.Boolean)reader[((int)ProductColumn.CallForPricing - 1)];
					c.HomepageSpecial = (System.Boolean)reader[((int)ProductColumn.HomepageSpecial - 1)];
					c.CategorySpecial = (System.Boolean)reader[((int)ProductColumn.CategorySpecial - 1)];
					c.InventoryDisplay = (System.Byte)reader[((int)ProductColumn.InventoryDisplay - 1)];
					c.Keywords = (reader.IsDBNull(((int)ProductColumn.Keywords - 1)))?null:(System.String)reader[((int)ProductColumn.Keywords - 1)];
					c.ManufacturerID = (reader.IsDBNull(((int)ProductColumn.ManufacturerID - 1)))?null:(System.Int32?)reader[((int)ProductColumn.ManufacturerID - 1)];
					c.AdditionalInfoLink = (reader.IsDBNull(((int)ProductColumn.AdditionalInfoLink - 1)))?null:(System.String)reader[((int)ProductColumn.AdditionalInfoLink - 1)];
					c.AdditionalInfoLinkLabel = (reader.IsDBNull(((int)ProductColumn.AdditionalInfoLinkLabel - 1)))?null:(System.String)reader[((int)ProductColumn.AdditionalInfoLinkLabel - 1)];
					c.ShippingRuleTypeID = (reader.IsDBNull(((int)ProductColumn.ShippingRuleTypeID - 1)))?null:(System.Int32?)reader[((int)ProductColumn.ShippingRuleTypeID - 1)];
					c.ShippingRate = (reader.IsDBNull(((int)ProductColumn.ShippingRate - 1)))?null:(System.Decimal?)reader[((int)ProductColumn.ShippingRate - 1)];
					c.SEOTitle = (reader.IsDBNull(((int)ProductColumn.SEOTitle - 1)))?null:(System.String)reader[((int)ProductColumn.SEOTitle - 1)];
					c.SEOKeywords = (reader.IsDBNull(((int)ProductColumn.SEOKeywords - 1)))?null:(System.String)reader[((int)ProductColumn.SEOKeywords - 1)];
					c.SEODescription = (reader.IsDBNull(((int)ProductColumn.SEODescription - 1)))?null:(System.String)reader[((int)ProductColumn.SEODescription - 1)];
					c.Custom1 = (reader.IsDBNull(((int)ProductColumn.Custom1 - 1)))?null:(System.String)reader[((int)ProductColumn.Custom1 - 1)];
					c.Custom2 = (reader.IsDBNull(((int)ProductColumn.Custom2 - 1)))?null:(System.String)reader[((int)ProductColumn.Custom2 - 1)];
					c.Custom3 = (reader.IsDBNull(((int)ProductColumn.Custom3 - 1)))?null:(System.String)reader[((int)ProductColumn.Custom3 - 1)];
					c.ShipEachItemSeparately = (reader.IsDBNull(((int)ProductColumn.ShipEachItemSeparately - 1)))?null:(System.Boolean?)reader[((int)ProductColumn.ShipEachItemSeparately - 1)];
					c.AllowBackOrder = (reader.IsDBNull(((int)ProductColumn.AllowBackOrder - 1)))?null:(System.Boolean?)reader[((int)ProductColumn.AllowBackOrder - 1)];
					c.BackOrderMsg = (reader.IsDBNull(((int)ProductColumn.BackOrderMsg - 1)))?null:(System.String)reader[((int)ProductColumn.BackOrderMsg - 1)];
					c.DropShipInd = (reader.IsDBNull(((int)ProductColumn.DropShipInd - 1)))?null:(System.Boolean?)reader[((int)ProductColumn.DropShipInd - 1)];
					c.DropShipEmailID = (reader.IsDBNull(((int)ProductColumn.DropShipEmailID - 1)))?null:(System.String)reader[((int)ProductColumn.DropShipEmailID - 1)];
					c.Specifications = (reader.IsDBNull(((int)ProductColumn.Specifications - 1)))?null:(System.String)reader[((int)ProductColumn.Specifications - 1)];
					c.AdditionalInformation = (reader.IsDBNull(((int)ProductColumn.AdditionalInformation - 1)))?null:(System.String)reader[((int)ProductColumn.AdditionalInformation - 1)];
					c.InStockMsg = (reader.IsDBNull(((int)ProductColumn.InStockMsg - 1)))?null:(System.String)reader[((int)ProductColumn.InStockMsg - 1)];
					c.OutOfStockMsg = (reader.IsDBNull(((int)ProductColumn.OutOfStockMsg - 1)))?null:(System.String)reader[((int)ProductColumn.OutOfStockMsg - 1)];
					c.TrackInventoryInd = (reader.IsDBNull(((int)ProductColumn.TrackInventoryInd - 1)))?null:(System.Boolean?)reader[((int)ProductColumn.TrackInventoryInd - 1)];
					c.DownloadLink = (reader.IsDBNull(((int)ProductColumn.DownloadLink - 1)))?null:(System.String)reader[((int)ProductColumn.DownloadLink - 1)];
					c.FreeShippingInd = (reader.IsDBNull(((int)ProductColumn.FreeShippingInd - 1)))?null:(System.Boolean?)reader[((int)ProductColumn.FreeShippingInd - 1)];
					c.NewProductInd = (reader.IsDBNull(((int)ProductColumn.NewProductInd - 1)))?null:(System.Boolean?)reader[((int)ProductColumn.NewProductInd - 1)];
					c.SEOURL = (reader.IsDBNull(((int)ProductColumn.SEOURL - 1)))?null:(System.String)reader[((int)ProductColumn.SEOURL - 1)];
					c.MaxQty = (reader.IsDBNull(((int)ProductColumn.MaxQty - 1)))?null:(System.Int32?)reader[((int)ProductColumn.MaxQty - 1)];
					c.ShipSeparately = (System.Boolean)reader[((int)ProductColumn.ShipSeparately - 1)];
					c.FeaturedInd = (System.Boolean)reader[((int)ProductColumn.FeaturedInd - 1)];
					c.WebServiceDownloadDte = (reader.IsDBNull(((int)ProductColumn.WebServiceDownloadDte - 1)))?null:(System.DateTime?)reader[((int)ProductColumn.WebServiceDownloadDte - 1)];
					c.UpdateDte = (reader.IsDBNull(((int)ProductColumn.UpdateDte - 1)))?null:(System.DateTime?)reader[((int)ProductColumn.UpdateDte - 1)];
					c.SupplierID = (reader.IsDBNull(((int)ProductColumn.SupplierID - 1)))?null:(System.Int32?)reader[((int)ProductColumn.SupplierID - 1)];
					c.RecurringBillingInd = (System.Boolean)reader[((int)ProductColumn.RecurringBillingInd - 1)];
					c.RecurringBillingInstallmentInd = (System.Boolean)reader[((int)ProductColumn.RecurringBillingInstallmentInd - 1)];
					c.RecurringBillingPeriod = (reader.IsDBNull(((int)ProductColumn.RecurringBillingPeriod - 1)))?null:(System.String)reader[((int)ProductColumn.RecurringBillingPeriod - 1)];
					c.RecurringBillingFrequency = (reader.IsDBNull(((int)ProductColumn.RecurringBillingFrequency - 1)))?null:(System.String)reader[((int)ProductColumn.RecurringBillingFrequency - 1)];
					c.RecurringBillingTotalCycles = (reader.IsDBNull(((int)ProductColumn.RecurringBillingTotalCycles - 1)))?null:(System.Int32?)reader[((int)ProductColumn.RecurringBillingTotalCycles - 1)];
					c.RecurringBillingInitialAmount = (reader.IsDBNull(((int)ProductColumn.RecurringBillingInitialAmount - 1)))?null:(System.Decimal?)reader[((int)ProductColumn.RecurringBillingInitialAmount - 1)];
					c.TaxClassID = (reader.IsDBNull(((int)ProductColumn.TaxClassID - 1)))?null:(System.Int32?)reader[((int)ProductColumn.TaxClassID - 1)];
					c.MinQty = (reader.IsDBNull(((int)ProductColumn.MinQty - 1)))?null:(System.Int32?)reader[((int)ProductColumn.MinQty - 1)];
					c.ReviewStateID = (reader.IsDBNull(((int)ProductColumn.ReviewStateID - 1)))?null:(System.Int32?)reader[((int)ProductColumn.ReviewStateID - 1)];
					c.AffiliateUrl = (reader.IsDBNull(((int)ProductColumn.AffiliateUrl - 1)))?null:(System.String)reader[((int)ProductColumn.AffiliateUrl - 1)];
					c.IsShippable = (reader.IsDBNull(((int)ProductColumn.IsShippable - 1)))?null:(System.Boolean?)reader[((int)ProductColumn.IsShippable - 1)];
					c.AccountID = (reader.IsDBNull(((int)ProductColumn.AccountID - 1)))?null:(System.Int32?)reader[((int)ProductColumn.AccountID - 1)];
					c.PortalID = (reader.IsDBNull(((int)ProductColumn.PortalID - 1)))?null:(System.Int32?)reader[((int)ProductColumn.PortalID - 1)];
					c.Franchisable = (System.Boolean)reader[((int)ProductColumn.Franchisable - 1)];
					c.ExpirationPeriod = (reader.IsDBNull(((int)ProductColumn.ExpirationPeriod - 1)))?null:(System.Int32?)reader[((int)ProductColumn.ExpirationPeriod - 1)];
					c.ExpirationFrequency = (reader.IsDBNull(((int)ProductColumn.ExpirationFrequency - 1)))?null:(System.Int32?)reader[((int)ProductColumn.ExpirationFrequency - 1)];
					c.CreateDate = (reader.IsDBNull(((int)ProductColumn.CreateDate - 1)))?null:(System.DateTime?)reader[((int)ProductColumn.CreateDate - 1)];
					c.ExternalID = (reader.IsDBNull(((int)ProductColumn.ExternalID - 1)))?null:(System.String)reader[((int)ProductColumn.ExternalID - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.Product"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.Product"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.Product entity)
		{
			if (!reader.Read()) return;
			
			entity.ProductID = (System.Int32)reader[((int)ProductColumn.ProductID - 1)];
			entity.Name = (System.String)reader[((int)ProductColumn.Name - 1)];
			entity.ShortDescription = (reader.IsDBNull(((int)ProductColumn.ShortDescription - 1)))?null:(System.String)reader[((int)ProductColumn.ShortDescription - 1)];
			entity.Description = (System.String)reader[((int)ProductColumn.Description - 1)];
			entity.FeaturesDesc = (reader.IsDBNull(((int)ProductColumn.FeaturesDesc - 1)))?null:(System.String)reader[((int)ProductColumn.FeaturesDesc - 1)];
			entity.ProductNum = (System.String)reader[((int)ProductColumn.ProductNum - 1)];
			entity.ProductTypeID = (System.Int32)reader[((int)ProductColumn.ProductTypeID - 1)];
			entity.RetailPrice = (reader.IsDBNull(((int)ProductColumn.RetailPrice - 1)))?null:(System.Decimal?)reader[((int)ProductColumn.RetailPrice - 1)];
			entity.SalePrice = (reader.IsDBNull(((int)ProductColumn.SalePrice - 1)))?null:(System.Decimal?)reader[((int)ProductColumn.SalePrice - 1)];
			entity.WholesalePrice = (reader.IsDBNull(((int)ProductColumn.WholesalePrice - 1)))?null:(System.Decimal?)reader[((int)ProductColumn.WholesalePrice - 1)];
			entity.ImageFile = (reader.IsDBNull(((int)ProductColumn.ImageFile - 1)))?null:(System.String)reader[((int)ProductColumn.ImageFile - 1)];
			entity.ImageAltTag = (reader.IsDBNull(((int)ProductColumn.ImageAltTag - 1)))?null:(System.String)reader[((int)ProductColumn.ImageAltTag - 1)];
			entity.Weight = (reader.IsDBNull(((int)ProductColumn.Weight - 1)))?null:(System.Decimal?)reader[((int)ProductColumn.Weight - 1)];
			entity.Length = (reader.IsDBNull(((int)ProductColumn.Length - 1)))?null:(System.Decimal?)reader[((int)ProductColumn.Length - 1)];
			entity.Width = (reader.IsDBNull(((int)ProductColumn.Width - 1)))?null:(System.Decimal?)reader[((int)ProductColumn.Width - 1)];
			entity.Height = (reader.IsDBNull(((int)ProductColumn.Height - 1)))?null:(System.Decimal?)reader[((int)ProductColumn.Height - 1)];
			entity.BeginActiveDate = (reader.IsDBNull(((int)ProductColumn.BeginActiveDate - 1)))?null:(System.DateTime?)reader[((int)ProductColumn.BeginActiveDate - 1)];
			entity.EndActiveDate = (reader.IsDBNull(((int)ProductColumn.EndActiveDate - 1)))?null:(System.DateTime?)reader[((int)ProductColumn.EndActiveDate - 1)];
			entity.DisplayOrder = (reader.IsDBNull(((int)ProductColumn.DisplayOrder - 1)))?null:(System.Int32?)reader[((int)ProductColumn.DisplayOrder - 1)];
			entity.ActiveInd = (System.Boolean)reader[((int)ProductColumn.ActiveInd - 1)];
			entity.CallForPricing = (System.Boolean)reader[((int)ProductColumn.CallForPricing - 1)];
			entity.HomepageSpecial = (System.Boolean)reader[((int)ProductColumn.HomepageSpecial - 1)];
			entity.CategorySpecial = (System.Boolean)reader[((int)ProductColumn.CategorySpecial - 1)];
			entity.InventoryDisplay = (System.Byte)reader[((int)ProductColumn.InventoryDisplay - 1)];
			entity.Keywords = (reader.IsDBNull(((int)ProductColumn.Keywords - 1)))?null:(System.String)reader[((int)ProductColumn.Keywords - 1)];
			entity.ManufacturerID = (reader.IsDBNull(((int)ProductColumn.ManufacturerID - 1)))?null:(System.Int32?)reader[((int)ProductColumn.ManufacturerID - 1)];
			entity.AdditionalInfoLink = (reader.IsDBNull(((int)ProductColumn.AdditionalInfoLink - 1)))?null:(System.String)reader[((int)ProductColumn.AdditionalInfoLink - 1)];
			entity.AdditionalInfoLinkLabel = (reader.IsDBNull(((int)ProductColumn.AdditionalInfoLinkLabel - 1)))?null:(System.String)reader[((int)ProductColumn.AdditionalInfoLinkLabel - 1)];
			entity.ShippingRuleTypeID = (reader.IsDBNull(((int)ProductColumn.ShippingRuleTypeID - 1)))?null:(System.Int32?)reader[((int)ProductColumn.ShippingRuleTypeID - 1)];
			entity.ShippingRate = (reader.IsDBNull(((int)ProductColumn.ShippingRate - 1)))?null:(System.Decimal?)reader[((int)ProductColumn.ShippingRate - 1)];
			entity.SEOTitle = (reader.IsDBNull(((int)ProductColumn.SEOTitle - 1)))?null:(System.String)reader[((int)ProductColumn.SEOTitle - 1)];
			entity.SEOKeywords = (reader.IsDBNull(((int)ProductColumn.SEOKeywords - 1)))?null:(System.String)reader[((int)ProductColumn.SEOKeywords - 1)];
			entity.SEODescription = (reader.IsDBNull(((int)ProductColumn.SEODescription - 1)))?null:(System.String)reader[((int)ProductColumn.SEODescription - 1)];
			entity.Custom1 = (reader.IsDBNull(((int)ProductColumn.Custom1 - 1)))?null:(System.String)reader[((int)ProductColumn.Custom1 - 1)];
			entity.Custom2 = (reader.IsDBNull(((int)ProductColumn.Custom2 - 1)))?null:(System.String)reader[((int)ProductColumn.Custom2 - 1)];
			entity.Custom3 = (reader.IsDBNull(((int)ProductColumn.Custom3 - 1)))?null:(System.String)reader[((int)ProductColumn.Custom3 - 1)];
			entity.ShipEachItemSeparately = (reader.IsDBNull(((int)ProductColumn.ShipEachItemSeparately - 1)))?null:(System.Boolean?)reader[((int)ProductColumn.ShipEachItemSeparately - 1)];
			entity.AllowBackOrder = (reader.IsDBNull(((int)ProductColumn.AllowBackOrder - 1)))?null:(System.Boolean?)reader[((int)ProductColumn.AllowBackOrder - 1)];
			entity.BackOrderMsg = (reader.IsDBNull(((int)ProductColumn.BackOrderMsg - 1)))?null:(System.String)reader[((int)ProductColumn.BackOrderMsg - 1)];
			entity.DropShipInd = (reader.IsDBNull(((int)ProductColumn.DropShipInd - 1)))?null:(System.Boolean?)reader[((int)ProductColumn.DropShipInd - 1)];
			entity.DropShipEmailID = (reader.IsDBNull(((int)ProductColumn.DropShipEmailID - 1)))?null:(System.String)reader[((int)ProductColumn.DropShipEmailID - 1)];
			entity.Specifications = (reader.IsDBNull(((int)ProductColumn.Specifications - 1)))?null:(System.String)reader[((int)ProductColumn.Specifications - 1)];
			entity.AdditionalInformation = (reader.IsDBNull(((int)ProductColumn.AdditionalInformation - 1)))?null:(System.String)reader[((int)ProductColumn.AdditionalInformation - 1)];
			entity.InStockMsg = (reader.IsDBNull(((int)ProductColumn.InStockMsg - 1)))?null:(System.String)reader[((int)ProductColumn.InStockMsg - 1)];
			entity.OutOfStockMsg = (reader.IsDBNull(((int)ProductColumn.OutOfStockMsg - 1)))?null:(System.String)reader[((int)ProductColumn.OutOfStockMsg - 1)];
			entity.TrackInventoryInd = (reader.IsDBNull(((int)ProductColumn.TrackInventoryInd - 1)))?null:(System.Boolean?)reader[((int)ProductColumn.TrackInventoryInd - 1)];
			entity.DownloadLink = (reader.IsDBNull(((int)ProductColumn.DownloadLink - 1)))?null:(System.String)reader[((int)ProductColumn.DownloadLink - 1)];
			entity.FreeShippingInd = (reader.IsDBNull(((int)ProductColumn.FreeShippingInd - 1)))?null:(System.Boolean?)reader[((int)ProductColumn.FreeShippingInd - 1)];
			entity.NewProductInd = (reader.IsDBNull(((int)ProductColumn.NewProductInd - 1)))?null:(System.Boolean?)reader[((int)ProductColumn.NewProductInd - 1)];
			entity.SEOURL = (reader.IsDBNull(((int)ProductColumn.SEOURL - 1)))?null:(System.String)reader[((int)ProductColumn.SEOURL - 1)];
			entity.MaxQty = (reader.IsDBNull(((int)ProductColumn.MaxQty - 1)))?null:(System.Int32?)reader[((int)ProductColumn.MaxQty - 1)];
			entity.ShipSeparately = (System.Boolean)reader[((int)ProductColumn.ShipSeparately - 1)];
			entity.FeaturedInd = (System.Boolean)reader[((int)ProductColumn.FeaturedInd - 1)];
			entity.WebServiceDownloadDte = (reader.IsDBNull(((int)ProductColumn.WebServiceDownloadDte - 1)))?null:(System.DateTime?)reader[((int)ProductColumn.WebServiceDownloadDte - 1)];
			entity.UpdateDte = (reader.IsDBNull(((int)ProductColumn.UpdateDte - 1)))?null:(System.DateTime?)reader[((int)ProductColumn.UpdateDte - 1)];
			entity.SupplierID = (reader.IsDBNull(((int)ProductColumn.SupplierID - 1)))?null:(System.Int32?)reader[((int)ProductColumn.SupplierID - 1)];
			entity.RecurringBillingInd = (System.Boolean)reader[((int)ProductColumn.RecurringBillingInd - 1)];
			entity.RecurringBillingInstallmentInd = (System.Boolean)reader[((int)ProductColumn.RecurringBillingInstallmentInd - 1)];
			entity.RecurringBillingPeriod = (reader.IsDBNull(((int)ProductColumn.RecurringBillingPeriod - 1)))?null:(System.String)reader[((int)ProductColumn.RecurringBillingPeriod - 1)];
			entity.RecurringBillingFrequency = (reader.IsDBNull(((int)ProductColumn.RecurringBillingFrequency - 1)))?null:(System.String)reader[((int)ProductColumn.RecurringBillingFrequency - 1)];
			entity.RecurringBillingTotalCycles = (reader.IsDBNull(((int)ProductColumn.RecurringBillingTotalCycles - 1)))?null:(System.Int32?)reader[((int)ProductColumn.RecurringBillingTotalCycles - 1)];
			entity.RecurringBillingInitialAmount = (reader.IsDBNull(((int)ProductColumn.RecurringBillingInitialAmount - 1)))?null:(System.Decimal?)reader[((int)ProductColumn.RecurringBillingInitialAmount - 1)];
			entity.TaxClassID = (reader.IsDBNull(((int)ProductColumn.TaxClassID - 1)))?null:(System.Int32?)reader[((int)ProductColumn.TaxClassID - 1)];
			entity.MinQty = (reader.IsDBNull(((int)ProductColumn.MinQty - 1)))?null:(System.Int32?)reader[((int)ProductColumn.MinQty - 1)];
			entity.ReviewStateID = (reader.IsDBNull(((int)ProductColumn.ReviewStateID - 1)))?null:(System.Int32?)reader[((int)ProductColumn.ReviewStateID - 1)];
			entity.AffiliateUrl = (reader.IsDBNull(((int)ProductColumn.AffiliateUrl - 1)))?null:(System.String)reader[((int)ProductColumn.AffiliateUrl - 1)];
			entity.IsShippable = (reader.IsDBNull(((int)ProductColumn.IsShippable - 1)))?null:(System.Boolean?)reader[((int)ProductColumn.IsShippable - 1)];
			entity.AccountID = (reader.IsDBNull(((int)ProductColumn.AccountID - 1)))?null:(System.Int32?)reader[((int)ProductColumn.AccountID - 1)];
			entity.PortalID = (reader.IsDBNull(((int)ProductColumn.PortalID - 1)))?null:(System.Int32?)reader[((int)ProductColumn.PortalID - 1)];
			entity.Franchisable = (System.Boolean)reader[((int)ProductColumn.Franchisable - 1)];
			entity.ExpirationPeriod = (reader.IsDBNull(((int)ProductColumn.ExpirationPeriod - 1)))?null:(System.Int32?)reader[((int)ProductColumn.ExpirationPeriod - 1)];
			entity.ExpirationFrequency = (reader.IsDBNull(((int)ProductColumn.ExpirationFrequency - 1)))?null:(System.Int32?)reader[((int)ProductColumn.ExpirationFrequency - 1)];
			entity.CreateDate = (reader.IsDBNull(((int)ProductColumn.CreateDate - 1)))?null:(System.DateTime?)reader[((int)ProductColumn.CreateDate - 1)];
			entity.ExternalID = (reader.IsDBNull(((int)ProductColumn.ExternalID - 1)))?null:(System.String)reader[((int)ProductColumn.ExternalID - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.Product"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.Product"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.Product entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.ProductID = (System.Int32)dataRow["ProductID"];
			entity.Name = (System.String)dataRow["Name"];
			entity.ShortDescription = Convert.IsDBNull(dataRow["ShortDescription"]) ? null : (System.String)dataRow["ShortDescription"];
			entity.Description = (System.String)dataRow["Description"];
			entity.FeaturesDesc = Convert.IsDBNull(dataRow["FeaturesDesc"]) ? null : (System.String)dataRow["FeaturesDesc"];
			entity.ProductNum = (System.String)dataRow["ProductNum"];
			entity.ProductTypeID = (System.Int32)dataRow["ProductTypeID"];
			entity.RetailPrice = Convert.IsDBNull(dataRow["RetailPrice"]) ? null : (System.Decimal?)dataRow["RetailPrice"];
			entity.SalePrice = Convert.IsDBNull(dataRow["SalePrice"]) ? null : (System.Decimal?)dataRow["SalePrice"];
			entity.WholesalePrice = Convert.IsDBNull(dataRow["WholesalePrice"]) ? null : (System.Decimal?)dataRow["WholesalePrice"];
			entity.ImageFile = Convert.IsDBNull(dataRow["ImageFile"]) ? null : (System.String)dataRow["ImageFile"];
			entity.ImageAltTag = Convert.IsDBNull(dataRow["ImageAltTag"]) ? null : (System.String)dataRow["ImageAltTag"];
			entity.Weight = Convert.IsDBNull(dataRow["Weight"]) ? null : (System.Decimal?)dataRow["Weight"];
			entity.Length = Convert.IsDBNull(dataRow["Length"]) ? null : (System.Decimal?)dataRow["Length"];
			entity.Width = Convert.IsDBNull(dataRow["Width"]) ? null : (System.Decimal?)dataRow["Width"];
			entity.Height = Convert.IsDBNull(dataRow["Height"]) ? null : (System.Decimal?)dataRow["Height"];
			entity.BeginActiveDate = Convert.IsDBNull(dataRow["BeginActiveDate"]) ? null : (System.DateTime?)dataRow["BeginActiveDate"];
			entity.EndActiveDate = Convert.IsDBNull(dataRow["EndActiveDate"]) ? null : (System.DateTime?)dataRow["EndActiveDate"];
			entity.DisplayOrder = Convert.IsDBNull(dataRow["DisplayOrder"]) ? null : (System.Int32?)dataRow["DisplayOrder"];
			entity.ActiveInd = (System.Boolean)dataRow["ActiveInd"];
			entity.CallForPricing = (System.Boolean)dataRow["CallForPricing"];
			entity.HomepageSpecial = (System.Boolean)dataRow["HomepageSpecial"];
			entity.CategorySpecial = (System.Boolean)dataRow["CategorySpecial"];
			entity.InventoryDisplay = (System.Byte)dataRow["InventoryDisplay"];
			entity.Keywords = Convert.IsDBNull(dataRow["Keywords"]) ? null : (System.String)dataRow["Keywords"];
			entity.ManufacturerID = Convert.IsDBNull(dataRow["ManufacturerID"]) ? null : (System.Int32?)dataRow["ManufacturerID"];
			entity.AdditionalInfoLink = Convert.IsDBNull(dataRow["AdditionalInfoLink"]) ? null : (System.String)dataRow["AdditionalInfoLink"];
			entity.AdditionalInfoLinkLabel = Convert.IsDBNull(dataRow["AdditionalInfoLinkLabel"]) ? null : (System.String)dataRow["AdditionalInfoLinkLabel"];
			entity.ShippingRuleTypeID = Convert.IsDBNull(dataRow["ShippingRuleTypeID"]) ? null : (System.Int32?)dataRow["ShippingRuleTypeID"];
			entity.ShippingRate = Convert.IsDBNull(dataRow["ShippingRate"]) ? null : (System.Decimal?)dataRow["ShippingRate"];
			entity.SEOTitle = Convert.IsDBNull(dataRow["SEOTitle"]) ? null : (System.String)dataRow["SEOTitle"];
			entity.SEOKeywords = Convert.IsDBNull(dataRow["SEOKeywords"]) ? null : (System.String)dataRow["SEOKeywords"];
			entity.SEODescription = Convert.IsDBNull(dataRow["SEODescription"]) ? null : (System.String)dataRow["SEODescription"];
			entity.Custom1 = Convert.IsDBNull(dataRow["Custom1"]) ? null : (System.String)dataRow["Custom1"];
			entity.Custom2 = Convert.IsDBNull(dataRow["Custom2"]) ? null : (System.String)dataRow["Custom2"];
			entity.Custom3 = Convert.IsDBNull(dataRow["Custom3"]) ? null : (System.String)dataRow["Custom3"];
			entity.ShipEachItemSeparately = Convert.IsDBNull(dataRow["ShipEachItemSeparately"]) ? null : (System.Boolean?)dataRow["ShipEachItemSeparately"];
			entity.AllowBackOrder = Convert.IsDBNull(dataRow["AllowBackOrder"]) ? null : (System.Boolean?)dataRow["AllowBackOrder"];
			entity.BackOrderMsg = Convert.IsDBNull(dataRow["BackOrderMsg"]) ? null : (System.String)dataRow["BackOrderMsg"];
			entity.DropShipInd = Convert.IsDBNull(dataRow["DropShipInd"]) ? null : (System.Boolean?)dataRow["DropShipInd"];
			entity.DropShipEmailID = Convert.IsDBNull(dataRow["DropShipEmailID"]) ? null : (System.String)dataRow["DropShipEmailID"];
			entity.Specifications = Convert.IsDBNull(dataRow["Specifications"]) ? null : (System.String)dataRow["Specifications"];
			entity.AdditionalInformation = Convert.IsDBNull(dataRow["AdditionalInformation"]) ? null : (System.String)dataRow["AdditionalInformation"];
			entity.InStockMsg = Convert.IsDBNull(dataRow["InStockMsg"]) ? null : (System.String)dataRow["InStockMsg"];
			entity.OutOfStockMsg = Convert.IsDBNull(dataRow["OutOfStockMsg"]) ? null : (System.String)dataRow["OutOfStockMsg"];
			entity.TrackInventoryInd = Convert.IsDBNull(dataRow["TrackInventoryInd"]) ? null : (System.Boolean?)dataRow["TrackInventoryInd"];
			entity.DownloadLink = Convert.IsDBNull(dataRow["DownloadLink"]) ? null : (System.String)dataRow["DownloadLink"];
			entity.FreeShippingInd = Convert.IsDBNull(dataRow["FreeShippingInd"]) ? null : (System.Boolean?)dataRow["FreeShippingInd"];
			entity.NewProductInd = Convert.IsDBNull(dataRow["NewProductInd"]) ? null : (System.Boolean?)dataRow["NewProductInd"];
			entity.SEOURL = Convert.IsDBNull(dataRow["SEOURL"]) ? null : (System.String)dataRow["SEOURL"];
			entity.MaxQty = Convert.IsDBNull(dataRow["MaxQty"]) ? null : (System.Int32?)dataRow["MaxQty"];
			entity.ShipSeparately = (System.Boolean)dataRow["ShipSeparately"];
			entity.FeaturedInd = (System.Boolean)dataRow["FeaturedInd"];
			entity.WebServiceDownloadDte = Convert.IsDBNull(dataRow["WebServiceDownloadDte"]) ? null : (System.DateTime?)dataRow["WebServiceDownloadDte"];
			entity.UpdateDte = Convert.IsDBNull(dataRow["UpdateDte"]) ? null : (System.DateTime?)dataRow["UpdateDte"];
			entity.SupplierID = Convert.IsDBNull(dataRow["SupplierID"]) ? null : (System.Int32?)dataRow["SupplierID"];
			entity.RecurringBillingInd = (System.Boolean)dataRow["RecurringBillingInd"];
			entity.RecurringBillingInstallmentInd = (System.Boolean)dataRow["RecurringBillingInstallmentInd"];
			entity.RecurringBillingPeriod = Convert.IsDBNull(dataRow["RecurringBillingPeriod"]) ? null : (System.String)dataRow["RecurringBillingPeriod"];
			entity.RecurringBillingFrequency = Convert.IsDBNull(dataRow["RecurringBillingFrequency"]) ? null : (System.String)dataRow["RecurringBillingFrequency"];
			entity.RecurringBillingTotalCycles = Convert.IsDBNull(dataRow["RecurringBillingTotalCycles"]) ? null : (System.Int32?)dataRow["RecurringBillingTotalCycles"];
			entity.RecurringBillingInitialAmount = Convert.IsDBNull(dataRow["RecurringBillingInitialAmount"]) ? null : (System.Decimal?)dataRow["RecurringBillingInitialAmount"];
			entity.TaxClassID = Convert.IsDBNull(dataRow["TaxClassID"]) ? null : (System.Int32?)dataRow["TaxClassID"];
			entity.MinQty = Convert.IsDBNull(dataRow["MinQty"]) ? null : (System.Int32?)dataRow["MinQty"];
			entity.ReviewStateID = Convert.IsDBNull(dataRow["ReviewStateID"]) ? null : (System.Int32?)dataRow["ReviewStateID"];
			entity.AffiliateUrl = Convert.IsDBNull(dataRow["AffiliateUrl"]) ? null : (System.String)dataRow["AffiliateUrl"];
			entity.IsShippable = Convert.IsDBNull(dataRow["IsShippable"]) ? null : (System.Boolean?)dataRow["IsShippable"];
			entity.AccountID = Convert.IsDBNull(dataRow["AccountID"]) ? null : (System.Int32?)dataRow["AccountID"];
			entity.PortalID = Convert.IsDBNull(dataRow["PortalID"]) ? null : (System.Int32?)dataRow["PortalID"];
			entity.Franchisable = (System.Boolean)dataRow["Franchisable"];
			entity.ExpirationPeriod = Convert.IsDBNull(dataRow["ExpirationPeriod"]) ? null : (System.Int32?)dataRow["ExpirationPeriod"];
			entity.ExpirationFrequency = Convert.IsDBNull(dataRow["ExpirationFrequency"]) ? null : (System.Int32?)dataRow["ExpirationFrequency"];
			entity.CreateDate = Convert.IsDBNull(dataRow["CreateDate"]) ? null : (System.DateTime?)dataRow["CreateDate"];
			entity.ExternalID = Convert.IsDBNull(dataRow["ExternalID"]) ? null : (System.String)dataRow["ExternalID"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.Product"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.Product Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.Product entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region ManufacturerIDSource	
			if (CanDeepLoad(entity, "Manufacturer|ManufacturerIDSource", deepLoadType, innerList) 
				&& entity.ManufacturerIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.ManufacturerID ?? (int)0);
				Manufacturer tmpEntity = EntityManager.LocateEntity<Manufacturer>(EntityLocator.ConstructKeyFromPkItems(typeof(Manufacturer), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ManufacturerIDSource = tmpEntity;
				else
					entity.ManufacturerIDSource = DataRepository.ManufacturerProvider.GetByManufacturerID(transactionManager, (entity.ManufacturerID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ManufacturerIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ManufacturerIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.ManufacturerProvider.DeepLoad(transactionManager, entity.ManufacturerIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ManufacturerIDSource

			#region ProductTypeIDSource	
			if (CanDeepLoad(entity, "ProductType|ProductTypeIDSource", deepLoadType, innerList) 
				&& entity.ProductTypeIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.ProductTypeID;
				ProductType tmpEntity = EntityManager.LocateEntity<ProductType>(EntityLocator.ConstructKeyFromPkItems(typeof(ProductType), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ProductTypeIDSource = tmpEntity;
				else
					entity.ProductTypeIDSource = DataRepository.ProductTypeProvider.GetByProductTypeId(transactionManager, entity.ProductTypeID);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ProductTypeIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ProductTypeIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.ProductTypeProvider.DeepLoad(transactionManager, entity.ProductTypeIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ProductTypeIDSource

			#region ShippingRuleTypeIDSource	
			if (CanDeepLoad(entity, "ShippingRuleType|ShippingRuleTypeIDSource", deepLoadType, innerList) 
				&& entity.ShippingRuleTypeIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.ShippingRuleTypeID ?? (int)0);
				ShippingRuleType tmpEntity = EntityManager.LocateEntity<ShippingRuleType>(EntityLocator.ConstructKeyFromPkItems(typeof(ShippingRuleType), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ShippingRuleTypeIDSource = tmpEntity;
				else
					entity.ShippingRuleTypeIDSource = DataRepository.ShippingRuleTypeProvider.GetByShippingRuleTypeID(transactionManager, (entity.ShippingRuleTypeID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ShippingRuleTypeIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ShippingRuleTypeIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.ShippingRuleTypeProvider.DeepLoad(transactionManager, entity.ShippingRuleTypeIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ShippingRuleTypeIDSource

			#region AccountIDSource	
			if (CanDeepLoad(entity, "Account|AccountIDSource", deepLoadType, innerList) 
				&& entity.AccountIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.AccountID ?? (int)0);
				Account tmpEntity = EntityManager.LocateEntity<Account>(EntityLocator.ConstructKeyFromPkItems(typeof(Account), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.AccountIDSource = tmpEntity;
				else
					entity.AccountIDSource = DataRepository.AccountProvider.GetByAccountID(transactionManager, (entity.AccountID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AccountIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.AccountIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.AccountProvider.DeepLoad(transactionManager, entity.AccountIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion AccountIDSource

			#region PortalIDSource	
			if (CanDeepLoad(entity, "Portal|PortalIDSource", deepLoadType, innerList) 
				&& entity.PortalIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.PortalID ?? (int)0);
				Portal tmpEntity = EntityManager.LocateEntity<Portal>(EntityLocator.ConstructKeyFromPkItems(typeof(Portal), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.PortalIDSource = tmpEntity;
				else
					entity.PortalIDSource = DataRepository.PortalProvider.GetByPortalID(transactionManager, (entity.PortalID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PortalIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.PortalIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.PortalProvider.DeepLoad(transactionManager, entity.PortalIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion PortalIDSource

			#region ReviewStateIDSource	
			if (CanDeepLoad(entity, "ProductReviewState|ReviewStateIDSource", deepLoadType, innerList) 
				&& entity.ReviewStateIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.ReviewStateID ?? (int)0);
				ProductReviewState tmpEntity = EntityManager.LocateEntity<ProductReviewState>(EntityLocator.ConstructKeyFromPkItems(typeof(ProductReviewState), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ReviewStateIDSource = tmpEntity;
				else
					entity.ReviewStateIDSource = DataRepository.ProductReviewStateProvider.GetByReviewStateID(transactionManager, (entity.ReviewStateID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ReviewStateIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ReviewStateIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.ProductReviewStateProvider.DeepLoad(transactionManager, entity.ReviewStateIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ReviewStateIDSource

			#region SupplierIDSource	
			if (CanDeepLoad(entity, "Supplier|SupplierIDSource", deepLoadType, innerList) 
				&& entity.SupplierIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.SupplierID ?? (int)0);
				Supplier tmpEntity = EntityManager.LocateEntity<Supplier>(EntityLocator.ConstructKeyFromPkItems(typeof(Supplier), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.SupplierIDSource = tmpEntity;
				else
					entity.SupplierIDSource = DataRepository.SupplierProvider.GetBySupplierID(transactionManager, (entity.SupplierID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SupplierIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.SupplierIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.SupplierProvider.DeepLoad(transactionManager, entity.SupplierIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion SupplierIDSource

			#region TaxClassIDSource	
			if (CanDeepLoad(entity, "TaxClass|TaxClassIDSource", deepLoadType, innerList) 
				&& entity.TaxClassIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.TaxClassID ?? (int)0);
				TaxClass tmpEntity = EntityManager.LocateEntity<TaxClass>(EntityLocator.ConstructKeyFromPkItems(typeof(TaxClass), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.TaxClassIDSource = tmpEntity;
				else
					entity.TaxClassIDSource = DataRepository.TaxClassProvider.GetByTaxClassID(transactionManager, (entity.TaxClassID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'TaxClassIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.TaxClassIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.TaxClassProvider.DeepLoad(transactionManager, entity.TaxClassIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion TaxClassIDSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByProductID methods when available
			
			#region LuceneGlobalProductBoost
			// RelationshipType.OneToOne
			if (CanDeepLoad(entity, "LuceneGlobalProductBoost|LuceneGlobalProductBoost", deepLoadType, innerList))
			{
				entity.LuceneGlobalProductBoost = DataRepository.LuceneGlobalProductBoostProvider.GetByProductID(transactionManager, entity.ProductID);
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'LuceneGlobalProductBoost' loaded. key " + entity.EntityTrackingKey);
				#endif 

				if (deep && entity.LuceneGlobalProductBoost != null)
				{
					deepHandles.Add("LuceneGlobalProductBoost",
						new KeyValuePair<Delegate, object>((DeepLoadSingleHandle< LuceneGlobalProductBoost >) DataRepository.LuceneGlobalProductBoostProvider.DeepLoad,
						new object[] { transactionManager, entity.LuceneGlobalProductBoost, deep, deepLoadType, childTypes, innerList }
					));
				}
			}
			#endregion 
			
			
			
			#region ProductAddOnCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<ProductAddOn>|ProductAddOnCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ProductAddOnCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.ProductAddOnCollection = DataRepository.ProductAddOnProvider.GetByProductID(transactionManager, entity.ProductID);

				if (deep && entity.ProductAddOnCollection.Count > 0)
				{
					deepHandles.Add("ProductAddOnCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<ProductAddOn>) DataRepository.ProductAddOnProvider.DeepLoad,
						new object[] { transactionManager, entity.ProductAddOnCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region WishListCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<WishList>|WishListCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'WishListCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.WishListCollection = DataRepository.WishListProvider.GetByProductID(transactionManager, entity.ProductID);

				if (deep && entity.WishListCollection.Count > 0)
				{
					deepHandles.Add("WishListCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<WishList>) DataRepository.WishListProvider.DeepLoad,
						new object[] { transactionManager, entity.WishListCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region ReviewCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Review>|ReviewCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ReviewCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.ReviewCollection = DataRepository.ReviewProvider.GetByProductID(transactionManager, entity.ProductID);

				if (deep && entity.ReviewCollection.Count > 0)
				{
					deepHandles.Add("ReviewCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Review>) DataRepository.ReviewProvider.DeepLoad,
						new object[] { transactionManager, entity.ReviewCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region SKUCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<SKU>|SKUCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SKUCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.SKUCollection = DataRepository.SKUProvider.GetByProductID(transactionManager, entity.ProductID);

				if (deep && entity.SKUCollection.Count > 0)
				{
					deepHandles.Add("SKUCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<SKU>) DataRepository.SKUProvider.DeepLoad,
						new object[] { transactionManager, entity.SKUCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region ProductTierCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<ProductTier>|ProductTierCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ProductTierCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.ProductTierCollection = DataRepository.ProductTierProvider.GetByProductID(transactionManager, entity.ProductID);

				if (deep && entity.ProductTierCollection.Count > 0)
				{
					deepHandles.Add("ProductTierCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<ProductTier>) DataRepository.ProductTierProvider.DeepLoad,
						new object[] { transactionManager, entity.ProductTierCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region DigitalAssetCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<DigitalAsset>|DigitalAssetCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'DigitalAssetCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.DigitalAssetCollection = DataRepository.DigitalAssetProvider.GetByProductID(transactionManager, entity.ProductID);

				if (deep && entity.DigitalAssetCollection.Count > 0)
				{
					deepHandles.Add("DigitalAssetCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<DigitalAsset>) DataRepository.DigitalAssetProvider.DeepLoad,
						new object[] { transactionManager, entity.DigitalAssetCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region ProductReviewHistoryCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<ProductReviewHistory>|ProductReviewHistoryCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ProductReviewHistoryCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.ProductReviewHistoryCollection = DataRepository.ProductReviewHistoryProvider.GetByProductID(transactionManager, entity.ProductID);

				if (deep && entity.ProductReviewHistoryCollection.Count > 0)
				{
					deepHandles.Add("ProductReviewHistoryCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<ProductReviewHistory>) DataRepository.ProductReviewHistoryProvider.DeepLoad,
						new object[] { transactionManager, entity.ProductReviewHistoryCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region ProductCategoryCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<ProductCategory>|ProductCategoryCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ProductCategoryCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.ProductCategoryCollection = DataRepository.ProductCategoryProvider.GetByProductID(transactionManager, entity.ProductID);

				if (deep && entity.ProductCategoryCollection.Count > 0)
				{
					deepHandles.Add("ProductCategoryCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<ProductCategory>) DataRepository.ProductCategoryProvider.DeepLoad,
						new object[] { transactionManager, entity.ProductCategoryCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region FacetProductSKUCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<FacetProductSKU>|FacetProductSKUCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'FacetProductSKUCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.FacetProductSKUCollection = DataRepository.FacetProductSKUProvider.GetByProductID(transactionManager, entity.ProductID);

				if (deep && entity.FacetProductSKUCollection.Count > 0)
				{
					deepHandles.Add("FacetProductSKUCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<FacetProductSKU>) DataRepository.FacetProductSKUProvider.DeepLoad,
						new object[] { transactionManager, entity.FacetProductSKUCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region ParentChildProductCollectionGetByParentProductID
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<ParentChildProduct>|ParentChildProductCollectionGetByParentProductID", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ParentChildProductCollectionGetByParentProductID' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.ParentChildProductCollectionGetByParentProductID = DataRepository.ParentChildProductProvider.GetByParentProductID(transactionManager, entity.ProductID);

				if (deep && entity.ParentChildProductCollectionGetByParentProductID.Count > 0)
				{
					deepHandles.Add("ParentChildProductCollectionGetByParentProductID",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<ParentChildProduct>) DataRepository.ParentChildProductProvider.DeepLoad,
						new object[] { transactionManager, entity.ParentChildProductCollectionGetByParentProductID, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region PromotionCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Promotion>|PromotionCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PromotionCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.PromotionCollection = DataRepository.PromotionProvider.GetByProductID(transactionManager, entity.ProductID);

				if (deep && entity.PromotionCollection.Count > 0)
				{
					deepHandles.Add("PromotionCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Promotion>) DataRepository.PromotionProvider.DeepLoad,
						new object[] { transactionManager, entity.PromotionCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region ProductCrossSellCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<ProductCrossSell>|ProductCrossSellCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ProductCrossSellCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.ProductCrossSellCollection = DataRepository.ProductCrossSellProvider.GetByProductId(transactionManager, entity.ProductID);

				if (deep && entity.ProductCrossSellCollection.Count > 0)
				{
					deepHandles.Add("ProductCrossSellCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<ProductCrossSell>) DataRepository.ProductCrossSellProvider.DeepLoad,
						new object[] { transactionManager, entity.ProductCrossSellCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region ProductImageCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<ProductImage>|ProductImageCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ProductImageCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.ProductImageCollection = DataRepository.ProductImageProvider.GetByProductID(transactionManager, entity.ProductID);

				if (deep && entity.ProductImageCollection.Count > 0)
				{
					deepHandles.Add("ProductImageCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<ProductImage>) DataRepository.ProductImageProvider.DeepLoad,
						new object[] { transactionManager, entity.ProductImageCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region ParentChildProductCollectionGetByChildProductID
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<ParentChildProduct>|ParentChildProductCollectionGetByChildProductID", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ParentChildProductCollectionGetByChildProductID' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.ParentChildProductCollectionGetByChildProductID = DataRepository.ParentChildProductProvider.GetByChildProductID(transactionManager, entity.ProductID);

				if (deep && entity.ParentChildProductCollectionGetByChildProductID.Count > 0)
				{
					deepHandles.Add("ParentChildProductCollectionGetByChildProductID",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<ParentChildProduct>) DataRepository.ParentChildProductProvider.DeepLoad,
						new object[] { transactionManager, entity.ParentChildProductCollectionGetByChildProductID, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region ProductHighlightCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<ProductHighlight>|ProductHighlightCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ProductHighlightCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.ProductHighlightCollection = DataRepository.ProductHighlightProvider.GetByProductID(transactionManager, entity.ProductID);

				if (deep && entity.ProductHighlightCollection.Count > 0)
				{
					deepHandles.Add("ProductHighlightCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<ProductHighlight>) DataRepository.ProductHighlightProvider.DeepLoad,
						new object[] { transactionManager, entity.ProductHighlightCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region LuceneGlobalProductCatalogBoostCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<LuceneGlobalProductCatalogBoost>|LuceneGlobalProductCatalogBoostCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'LuceneGlobalProductCatalogBoostCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.LuceneGlobalProductCatalogBoostCollection = DataRepository.LuceneGlobalProductCatalogBoostProvider.GetByProductID(transactionManager, entity.ProductID);

				if (deep && entity.LuceneGlobalProductCatalogBoostCollection.Count > 0)
				{
					deepHandles.Add("LuceneGlobalProductCatalogBoostCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<LuceneGlobalProductCatalogBoost>) DataRepository.LuceneGlobalProductCatalogBoostProvider.DeepLoad,
						new object[] { transactionManager, entity.LuceneGlobalProductCatalogBoostCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region TrackingOutboundCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<TrackingOutbound>|TrackingOutboundCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'TrackingOutboundCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.TrackingOutboundCollection = DataRepository.TrackingOutboundProvider.GetByProductID(transactionManager, entity.ProductID);

				if (deep && entity.TrackingOutboundCollection.Count > 0)
				{
					deepHandles.Add("TrackingOutboundCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<TrackingOutbound>) DataRepository.TrackingOutboundProvider.DeepLoad,
						new object[] { transactionManager, entity.TrackingOutboundCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region TagsCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Tags>|TagsCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'TagsCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.TagsCollection = DataRepository.TagsProvider.GetByProductID(transactionManager, entity.ProductID);

				if (deep && entity.TagsCollection.Count > 0)
				{
					deepHandles.Add("TagsCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Tags>) DataRepository.TagsProvider.DeepLoad,
						new object[] { transactionManager, entity.TagsCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region LuceneGlobalProductBrandBoost
			// RelationshipType.OneToOne
			if (CanDeepLoad(entity, "LuceneGlobalProductBrandBoost|LuceneGlobalProductBrandBoost", deepLoadType, innerList))
			{
				entity.LuceneGlobalProductBrandBoost = DataRepository.LuceneGlobalProductBrandBoostProvider.GetByProductID(transactionManager, entity.ProductID);
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'LuceneGlobalProductBrandBoost' loaded. key " + entity.EntityTrackingKey);
				#endif 

				if (deep && entity.LuceneGlobalProductBrandBoost != null)
				{
					deepHandles.Add("LuceneGlobalProductBrandBoost",
						new KeyValuePair<Delegate, object>((DeepLoadSingleHandle< LuceneGlobalProductBrandBoost >) DataRepository.LuceneGlobalProductBrandBoostProvider.DeepLoad,
						new object[] { transactionManager, entity.LuceneGlobalProductBrandBoost, deep, deepLoadType, childTypes, innerList }
					));
				}
			}
			#endregion 
			
			
			
			#region ProductProfileCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<ProductProfile>|ProductProfileCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ProductProfileCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.ProductProfileCollection = DataRepository.ProductProfileProvider.GetByProductID(transactionManager, entity.ProductID);

				if (deep && entity.ProductProfileCollection.Count > 0)
				{
					deepHandles.Add("ProductProfileCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<ProductProfile>) DataRepository.ProductProfileProvider.DeepLoad,
						new object[] { transactionManager, entity.ProductProfileCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.Product object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.Product instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.Product Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.Product entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region ManufacturerIDSource
			if (CanDeepSave(entity, "Manufacturer|ManufacturerIDSource", deepSaveType, innerList) 
				&& entity.ManufacturerIDSource != null)
			{
				DataRepository.ManufacturerProvider.Save(transactionManager, entity.ManufacturerIDSource);
				entity.ManufacturerID = entity.ManufacturerIDSource.ManufacturerID;
			}
			#endregion 
			
			#region ProductTypeIDSource
			if (CanDeepSave(entity, "ProductType|ProductTypeIDSource", deepSaveType, innerList) 
				&& entity.ProductTypeIDSource != null)
			{
				DataRepository.ProductTypeProvider.Save(transactionManager, entity.ProductTypeIDSource);
				entity.ProductTypeID = entity.ProductTypeIDSource.ProductTypeId;
			}
			#endregion 
			
			#region ShippingRuleTypeIDSource
			if (CanDeepSave(entity, "ShippingRuleType|ShippingRuleTypeIDSource", deepSaveType, innerList) 
				&& entity.ShippingRuleTypeIDSource != null)
			{
				DataRepository.ShippingRuleTypeProvider.Save(transactionManager, entity.ShippingRuleTypeIDSource);
				entity.ShippingRuleTypeID = entity.ShippingRuleTypeIDSource.ShippingRuleTypeID;
			}
			#endregion 
			
			#region AccountIDSource
			if (CanDeepSave(entity, "Account|AccountIDSource", deepSaveType, innerList) 
				&& entity.AccountIDSource != null)
			{
				DataRepository.AccountProvider.Save(transactionManager, entity.AccountIDSource);
				entity.AccountID = entity.AccountIDSource.AccountID;
			}
			#endregion 
			
			#region PortalIDSource
			if (CanDeepSave(entity, "Portal|PortalIDSource", deepSaveType, innerList) 
				&& entity.PortalIDSource != null)
			{
				DataRepository.PortalProvider.Save(transactionManager, entity.PortalIDSource);
				entity.PortalID = entity.PortalIDSource.PortalID;
			}
			#endregion 
			
			#region ReviewStateIDSource
			if (CanDeepSave(entity, "ProductReviewState|ReviewStateIDSource", deepSaveType, innerList) 
				&& entity.ReviewStateIDSource != null)
			{
				DataRepository.ProductReviewStateProvider.Save(transactionManager, entity.ReviewStateIDSource);
				entity.ReviewStateID = entity.ReviewStateIDSource.ReviewStateID;
			}
			#endregion 
			
			#region SupplierIDSource
			if (CanDeepSave(entity, "Supplier|SupplierIDSource", deepSaveType, innerList) 
				&& entity.SupplierIDSource != null)
			{
				DataRepository.SupplierProvider.Save(transactionManager, entity.SupplierIDSource);
				entity.SupplierID = entity.SupplierIDSource.SupplierID;
			}
			#endregion 
			
			#region TaxClassIDSource
			if (CanDeepSave(entity, "TaxClass|TaxClassIDSource", deepSaveType, innerList) 
				&& entity.TaxClassIDSource != null)
			{
				DataRepository.TaxClassProvider.Save(transactionManager, entity.TaxClassIDSource);
				entity.TaxClassID = entity.TaxClassIDSource.TaxClassID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();

			#region LuceneGlobalProductBoost
			if (CanDeepSave(entity.LuceneGlobalProductBoost, "LuceneGlobalProductBoost|LuceneGlobalProductBoost", deepSaveType, innerList))
			{

				if (entity.LuceneGlobalProductBoost != null)
				{
					// update each child parent id with the real parent id (mostly used on insert)

					entity.LuceneGlobalProductBoost.ProductID = entity.ProductID;
					//DataRepository.LuceneGlobalProductBoostProvider.Save(transactionManager, entity.LuceneGlobalProductBoost);
					deepHandles.Add("LuceneGlobalProductBoost",
						new KeyValuePair<Delegate, object>((DeepSaveSingleHandle< LuceneGlobalProductBoost >) DataRepository.LuceneGlobalProductBoostProvider.DeepSave,
						new object[] { transactionManager, entity.LuceneGlobalProductBoost, deepSaveType, childTypes, innerList }
					));
				}
			} 
			#endregion 

			#region LuceneGlobalProductBrandBoost
			if (CanDeepSave(entity.LuceneGlobalProductBrandBoost, "LuceneGlobalProductBrandBoost|LuceneGlobalProductBrandBoost", deepSaveType, innerList))
			{

				if (entity.LuceneGlobalProductBrandBoost != null)
				{
					// update each child parent id with the real parent id (mostly used on insert)

					entity.LuceneGlobalProductBrandBoost.ProductID = entity.ProductID;
					//DataRepository.LuceneGlobalProductBrandBoostProvider.Save(transactionManager, entity.LuceneGlobalProductBrandBoost);
					deepHandles.Add("LuceneGlobalProductBrandBoost",
						new KeyValuePair<Delegate, object>((DeepSaveSingleHandle< LuceneGlobalProductBrandBoost >) DataRepository.LuceneGlobalProductBrandBoostProvider.DeepSave,
						new object[] { transactionManager, entity.LuceneGlobalProductBrandBoost, deepSaveType, childTypes, innerList }
					));
				}
			} 
			#endregion 
	
			#region List<ProductAddOn>
				if (CanDeepSave(entity.ProductAddOnCollection, "List<ProductAddOn>|ProductAddOnCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(ProductAddOn child in entity.ProductAddOnCollection)
					{
						if(child.ProductIDSource != null)
						{
							child.ProductID = child.ProductIDSource.ProductID;
						}
						else
						{
							child.ProductID = entity.ProductID;
						}

					}

					if (entity.ProductAddOnCollection.Count > 0 || entity.ProductAddOnCollection.DeletedItems.Count > 0)
					{
						//DataRepository.ProductAddOnProvider.Save(transactionManager, entity.ProductAddOnCollection);
						
						deepHandles.Add("ProductAddOnCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< ProductAddOn >) DataRepository.ProductAddOnProvider.DeepSave,
							new object[] { transactionManager, entity.ProductAddOnCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<WishList>
				if (CanDeepSave(entity.WishListCollection, "List<WishList>|WishListCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(WishList child in entity.WishListCollection)
					{
						if(child.ProductIDSource != null)
						{
							child.ProductID = child.ProductIDSource.ProductID;
						}
						else
						{
							child.ProductID = entity.ProductID;
						}

					}

					if (entity.WishListCollection.Count > 0 || entity.WishListCollection.DeletedItems.Count > 0)
					{
						//DataRepository.WishListProvider.Save(transactionManager, entity.WishListCollection);
						
						deepHandles.Add("WishListCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< WishList >) DataRepository.WishListProvider.DeepSave,
							new object[] { transactionManager, entity.WishListCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Review>
				if (CanDeepSave(entity.ReviewCollection, "List<Review>|ReviewCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Review child in entity.ReviewCollection)
					{
						if(child.ProductIDSource != null)
						{
							child.ProductID = child.ProductIDSource.ProductID;
						}
						else
						{
							child.ProductID = entity.ProductID;
						}

					}

					if (entity.ReviewCollection.Count > 0 || entity.ReviewCollection.DeletedItems.Count > 0)
					{
						//DataRepository.ReviewProvider.Save(transactionManager, entity.ReviewCollection);
						
						deepHandles.Add("ReviewCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Review >) DataRepository.ReviewProvider.DeepSave,
							new object[] { transactionManager, entity.ReviewCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<SKU>
				if (CanDeepSave(entity.SKUCollection, "List<SKU>|SKUCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(SKU child in entity.SKUCollection)
					{
						if(child.ProductIDSource != null)
						{
							child.ProductID = child.ProductIDSource.ProductID;
						}
						else
						{
							child.ProductID = entity.ProductID;
						}

					}

					if (entity.SKUCollection.Count > 0 || entity.SKUCollection.DeletedItems.Count > 0)
					{
						//DataRepository.SKUProvider.Save(transactionManager, entity.SKUCollection);
						
						deepHandles.Add("SKUCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< SKU >) DataRepository.SKUProvider.DeepSave,
							new object[] { transactionManager, entity.SKUCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<ProductTier>
				if (CanDeepSave(entity.ProductTierCollection, "List<ProductTier>|ProductTierCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(ProductTier child in entity.ProductTierCollection)
					{
						if(child.ProductIDSource != null)
						{
							child.ProductID = child.ProductIDSource.ProductID;
						}
						else
						{
							child.ProductID = entity.ProductID;
						}

					}

					if (entity.ProductTierCollection.Count > 0 || entity.ProductTierCollection.DeletedItems.Count > 0)
					{
						//DataRepository.ProductTierProvider.Save(transactionManager, entity.ProductTierCollection);
						
						deepHandles.Add("ProductTierCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< ProductTier >) DataRepository.ProductTierProvider.DeepSave,
							new object[] { transactionManager, entity.ProductTierCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<DigitalAsset>
				if (CanDeepSave(entity.DigitalAssetCollection, "List<DigitalAsset>|DigitalAssetCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(DigitalAsset child in entity.DigitalAssetCollection)
					{
						if(child.ProductIDSource != null)
						{
							child.ProductID = child.ProductIDSource.ProductID;
						}
						else
						{
							child.ProductID = entity.ProductID;
						}

					}

					if (entity.DigitalAssetCollection.Count > 0 || entity.DigitalAssetCollection.DeletedItems.Count > 0)
					{
						//DataRepository.DigitalAssetProvider.Save(transactionManager, entity.DigitalAssetCollection);
						
						deepHandles.Add("DigitalAssetCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< DigitalAsset >) DataRepository.DigitalAssetProvider.DeepSave,
							new object[] { transactionManager, entity.DigitalAssetCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<ProductReviewHistory>
				if (CanDeepSave(entity.ProductReviewHistoryCollection, "List<ProductReviewHistory>|ProductReviewHistoryCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(ProductReviewHistory child in entity.ProductReviewHistoryCollection)
					{
						if(child.ProductIDSource != null)
						{
							child.ProductID = child.ProductIDSource.ProductID;
						}
						else
						{
							child.ProductID = entity.ProductID;
						}

					}

					if (entity.ProductReviewHistoryCollection.Count > 0 || entity.ProductReviewHistoryCollection.DeletedItems.Count > 0)
					{
						//DataRepository.ProductReviewHistoryProvider.Save(transactionManager, entity.ProductReviewHistoryCollection);
						
						deepHandles.Add("ProductReviewHistoryCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< ProductReviewHistory >) DataRepository.ProductReviewHistoryProvider.DeepSave,
							new object[] { transactionManager, entity.ProductReviewHistoryCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<ProductCategory>
				if (CanDeepSave(entity.ProductCategoryCollection, "List<ProductCategory>|ProductCategoryCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(ProductCategory child in entity.ProductCategoryCollection)
					{
						if(child.ProductIDSource != null)
						{
							child.ProductID = child.ProductIDSource.ProductID;
						}
						else
						{
							child.ProductID = entity.ProductID;
						}

					}

					if (entity.ProductCategoryCollection.Count > 0 || entity.ProductCategoryCollection.DeletedItems.Count > 0)
					{
						//DataRepository.ProductCategoryProvider.Save(transactionManager, entity.ProductCategoryCollection);
						
						deepHandles.Add("ProductCategoryCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< ProductCategory >) DataRepository.ProductCategoryProvider.DeepSave,
							new object[] { transactionManager, entity.ProductCategoryCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<FacetProductSKU>
				if (CanDeepSave(entity.FacetProductSKUCollection, "List<FacetProductSKU>|FacetProductSKUCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(FacetProductSKU child in entity.FacetProductSKUCollection)
					{
						if(child.ProductIDSource != null)
						{
							child.ProductID = child.ProductIDSource.ProductID;
						}
						else
						{
							child.ProductID = entity.ProductID;
						}

					}

					if (entity.FacetProductSKUCollection.Count > 0 || entity.FacetProductSKUCollection.DeletedItems.Count > 0)
					{
						//DataRepository.FacetProductSKUProvider.Save(transactionManager, entity.FacetProductSKUCollection);
						
						deepHandles.Add("FacetProductSKUCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< FacetProductSKU >) DataRepository.FacetProductSKUProvider.DeepSave,
							new object[] { transactionManager, entity.FacetProductSKUCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<ParentChildProduct>
				if (CanDeepSave(entity.ParentChildProductCollectionGetByParentProductID, "List<ParentChildProduct>|ParentChildProductCollectionGetByParentProductID", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(ParentChildProduct child in entity.ParentChildProductCollectionGetByParentProductID)
					{
						if(child.ParentProductIDSource != null)
						{
							child.ParentProductID = child.ParentProductIDSource.ProductID;
						}
						else
						{
							child.ParentProductID = entity.ProductID;
						}

					}

					if (entity.ParentChildProductCollectionGetByParentProductID.Count > 0 || entity.ParentChildProductCollectionGetByParentProductID.DeletedItems.Count > 0)
					{
						//DataRepository.ParentChildProductProvider.Save(transactionManager, entity.ParentChildProductCollectionGetByParentProductID);
						
						deepHandles.Add("ParentChildProductCollectionGetByParentProductID",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< ParentChildProduct >) DataRepository.ParentChildProductProvider.DeepSave,
							new object[] { transactionManager, entity.ParentChildProductCollectionGetByParentProductID, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Promotion>
				if (CanDeepSave(entity.PromotionCollection, "List<Promotion>|PromotionCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Promotion child in entity.PromotionCollection)
					{
						if(child.ProductIDSource != null)
						{
							child.ProductID = child.ProductIDSource.ProductID;
						}
						else
						{
							child.ProductID = entity.ProductID;
						}

					}

					if (entity.PromotionCollection.Count > 0 || entity.PromotionCollection.DeletedItems.Count > 0)
					{
						//DataRepository.PromotionProvider.Save(transactionManager, entity.PromotionCollection);
						
						deepHandles.Add("PromotionCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Promotion >) DataRepository.PromotionProvider.DeepSave,
							new object[] { transactionManager, entity.PromotionCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<ProductCrossSell>
				if (CanDeepSave(entity.ProductCrossSellCollection, "List<ProductCrossSell>|ProductCrossSellCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(ProductCrossSell child in entity.ProductCrossSellCollection)
					{
						if(child.ProductIdSource != null)
						{
							child.ProductId = child.ProductIdSource.ProductID;
						}
						else
						{
							child.ProductId = entity.ProductID;
						}

					}

					if (entity.ProductCrossSellCollection.Count > 0 || entity.ProductCrossSellCollection.DeletedItems.Count > 0)
					{
						//DataRepository.ProductCrossSellProvider.Save(transactionManager, entity.ProductCrossSellCollection);
						
						deepHandles.Add("ProductCrossSellCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< ProductCrossSell >) DataRepository.ProductCrossSellProvider.DeepSave,
							new object[] { transactionManager, entity.ProductCrossSellCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<ProductImage>
				if (CanDeepSave(entity.ProductImageCollection, "List<ProductImage>|ProductImageCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(ProductImage child in entity.ProductImageCollection)
					{
						if(child.ProductIDSource != null)
						{
							child.ProductID = child.ProductIDSource.ProductID;
						}
						else
						{
							child.ProductID = entity.ProductID;
						}

					}

					if (entity.ProductImageCollection.Count > 0 || entity.ProductImageCollection.DeletedItems.Count > 0)
					{
						//DataRepository.ProductImageProvider.Save(transactionManager, entity.ProductImageCollection);
						
						deepHandles.Add("ProductImageCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< ProductImage >) DataRepository.ProductImageProvider.DeepSave,
							new object[] { transactionManager, entity.ProductImageCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<ParentChildProduct>
				if (CanDeepSave(entity.ParentChildProductCollectionGetByChildProductID, "List<ParentChildProduct>|ParentChildProductCollectionGetByChildProductID", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(ParentChildProduct child in entity.ParentChildProductCollectionGetByChildProductID)
					{
						if(child.ChildProductIDSource != null)
						{
							child.ChildProductID = child.ChildProductIDSource.ProductID;
						}
						else
						{
							child.ChildProductID = entity.ProductID;
						}

					}

					if (entity.ParentChildProductCollectionGetByChildProductID.Count > 0 || entity.ParentChildProductCollectionGetByChildProductID.DeletedItems.Count > 0)
					{
						//DataRepository.ParentChildProductProvider.Save(transactionManager, entity.ParentChildProductCollectionGetByChildProductID);
						
						deepHandles.Add("ParentChildProductCollectionGetByChildProductID",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< ParentChildProduct >) DataRepository.ParentChildProductProvider.DeepSave,
							new object[] { transactionManager, entity.ParentChildProductCollectionGetByChildProductID, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<ProductHighlight>
				if (CanDeepSave(entity.ProductHighlightCollection, "List<ProductHighlight>|ProductHighlightCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(ProductHighlight child in entity.ProductHighlightCollection)
					{
						if(child.ProductIDSource != null)
						{
							child.ProductID = child.ProductIDSource.ProductID;
						}
						else
						{
							child.ProductID = entity.ProductID;
						}

					}

					if (entity.ProductHighlightCollection.Count > 0 || entity.ProductHighlightCollection.DeletedItems.Count > 0)
					{
						//DataRepository.ProductHighlightProvider.Save(transactionManager, entity.ProductHighlightCollection);
						
						deepHandles.Add("ProductHighlightCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< ProductHighlight >) DataRepository.ProductHighlightProvider.DeepSave,
							new object[] { transactionManager, entity.ProductHighlightCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<LuceneGlobalProductCatalogBoost>
				if (CanDeepSave(entity.LuceneGlobalProductCatalogBoostCollection, "List<LuceneGlobalProductCatalogBoost>|LuceneGlobalProductCatalogBoostCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(LuceneGlobalProductCatalogBoost child in entity.LuceneGlobalProductCatalogBoostCollection)
					{
						if(child.ProductIDSource != null)
						{
							child.ProductID = child.ProductIDSource.ProductID;
						}
						else
						{
							child.ProductID = entity.ProductID;
						}

					}

					if (entity.LuceneGlobalProductCatalogBoostCollection.Count > 0 || entity.LuceneGlobalProductCatalogBoostCollection.DeletedItems.Count > 0)
					{
						//DataRepository.LuceneGlobalProductCatalogBoostProvider.Save(transactionManager, entity.LuceneGlobalProductCatalogBoostCollection);
						
						deepHandles.Add("LuceneGlobalProductCatalogBoostCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< LuceneGlobalProductCatalogBoost >) DataRepository.LuceneGlobalProductCatalogBoostProvider.DeepSave,
							new object[] { transactionManager, entity.LuceneGlobalProductCatalogBoostCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<TrackingOutbound>
				if (CanDeepSave(entity.TrackingOutboundCollection, "List<TrackingOutbound>|TrackingOutboundCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(TrackingOutbound child in entity.TrackingOutboundCollection)
					{
						if(child.ProductIDSource != null)
						{
							child.ProductID = child.ProductIDSource.ProductID;
						}
						else
						{
							child.ProductID = entity.ProductID;
						}

					}

					if (entity.TrackingOutboundCollection.Count > 0 || entity.TrackingOutboundCollection.DeletedItems.Count > 0)
					{
						//DataRepository.TrackingOutboundProvider.Save(transactionManager, entity.TrackingOutboundCollection);
						
						deepHandles.Add("TrackingOutboundCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< TrackingOutbound >) DataRepository.TrackingOutboundProvider.DeepSave,
							new object[] { transactionManager, entity.TrackingOutboundCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Tags>
				if (CanDeepSave(entity.TagsCollection, "List<Tags>|TagsCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Tags child in entity.TagsCollection)
					{
						if(child.ProductIDSource != null)
						{
							child.ProductID = child.ProductIDSource.ProductID;
						}
						else
						{
							child.ProductID = entity.ProductID;
						}

					}

					if (entity.TagsCollection.Count > 0 || entity.TagsCollection.DeletedItems.Count > 0)
					{
						//DataRepository.TagsProvider.Save(transactionManager, entity.TagsCollection);
						
						deepHandles.Add("TagsCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Tags >) DataRepository.TagsProvider.DeepSave,
							new object[] { transactionManager, entity.TagsCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<ProductProfile>
				if (CanDeepSave(entity.ProductProfileCollection, "List<ProductProfile>|ProductProfileCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(ProductProfile child in entity.ProductProfileCollection)
					{
						if(child.ProductIDSource != null)
						{
							child.ProductID = child.ProductIDSource.ProductID;
						}
						else
						{
							child.ProductID = entity.ProductID;
						}

					}

					if (entity.ProductProfileCollection.Count > 0 || entity.ProductProfileCollection.DeletedItems.Count > 0)
					{
						//DataRepository.ProductProfileProvider.Save(transactionManager, entity.ProductProfileCollection);
						
						deepHandles.Add("ProductProfileCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< ProductProfile >) DataRepository.ProductProfileProvider.DeepSave,
							new object[] { transactionManager, entity.ProductProfileCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region ProductChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.Product</c>
	///</summary>
	public enum ProductChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Manufacturer</c> at ManufacturerIDSource
		///</summary>
		[ChildEntityType(typeof(Manufacturer))]
		Manufacturer,
		
		///<summary>
		/// Composite Property for <c>ProductType</c> at ProductTypeIDSource
		///</summary>
		[ChildEntityType(typeof(ProductType))]
		ProductType,
		
		///<summary>
		/// Composite Property for <c>ShippingRuleType</c> at ShippingRuleTypeIDSource
		///</summary>
		[ChildEntityType(typeof(ShippingRuleType))]
		ShippingRuleType,
		
		///<summary>
		/// Composite Property for <c>Account</c> at AccountIDSource
		///</summary>
		[ChildEntityType(typeof(Account))]
		Account,
		
		///<summary>
		/// Composite Property for <c>Portal</c> at PortalIDSource
		///</summary>
		[ChildEntityType(typeof(Portal))]
		Portal,
		
		///<summary>
		/// Composite Property for <c>ProductReviewState</c> at ReviewStateIDSource
		///</summary>
		[ChildEntityType(typeof(ProductReviewState))]
		ProductReviewState,
		
		///<summary>
		/// Composite Property for <c>Supplier</c> at SupplierIDSource
		///</summary>
		[ChildEntityType(typeof(Supplier))]
		Supplier,
		
		///<summary>
		/// Composite Property for <c>TaxClass</c> at TaxClassIDSource
		///</summary>
		[ChildEntityType(typeof(TaxClass))]
		TaxClass,
		///<summary>
		/// Entity <c>LuceneGlobalProductBoost</c> as OneToOne for LuceneGlobalProductBoost
		///</summary>
		[ChildEntityType(typeof(LuceneGlobalProductBoost))]
		LuceneGlobalProductBoost,
		///<summary>
		/// Collection of <c>Product</c> as OneToMany for ProductAddOnCollection
		///</summary>
		[ChildEntityType(typeof(TList<ProductAddOn>))]
		ProductAddOnCollection,
		///<summary>
		/// Collection of <c>Product</c> as OneToMany for WishListCollection
		///</summary>
		[ChildEntityType(typeof(TList<WishList>))]
		WishListCollection,
		///<summary>
		/// Collection of <c>Product</c> as OneToMany for ReviewCollection
		///</summary>
		[ChildEntityType(typeof(TList<Review>))]
		ReviewCollection,
		///<summary>
		/// Collection of <c>Product</c> as OneToMany for SKUCollection
		///</summary>
		[ChildEntityType(typeof(TList<SKU>))]
		SKUCollection,
		///<summary>
		/// Collection of <c>Product</c> as OneToMany for ProductTierCollection
		///</summary>
		[ChildEntityType(typeof(TList<ProductTier>))]
		ProductTierCollection,
		///<summary>
		/// Collection of <c>Product</c> as OneToMany for DigitalAssetCollection
		///</summary>
		[ChildEntityType(typeof(TList<DigitalAsset>))]
		DigitalAssetCollection,
		///<summary>
		/// Collection of <c>Product</c> as OneToMany for ProductReviewHistoryCollection
		///</summary>
		[ChildEntityType(typeof(TList<ProductReviewHistory>))]
		ProductReviewHistoryCollection,
		///<summary>
		/// Collection of <c>Product</c> as OneToMany for ProductCategoryCollection
		///</summary>
		[ChildEntityType(typeof(TList<ProductCategory>))]
		ProductCategoryCollection,
		///<summary>
		/// Collection of <c>Product</c> as OneToMany for FacetProductSKUCollection
		///</summary>
		[ChildEntityType(typeof(TList<FacetProductSKU>))]
		FacetProductSKUCollection,
		///<summary>
		/// Collection of <c>Product</c> as OneToMany for ParentChildProductCollection
		///</summary>
		[ChildEntityType(typeof(TList<ParentChildProduct>))]
		ParentChildProductCollectionGetByParentProductID,
		///<summary>
		/// Collection of <c>Product</c> as OneToMany for PromotionCollection
		///</summary>
		[ChildEntityType(typeof(TList<Promotion>))]
		PromotionCollection,
		///<summary>
		/// Collection of <c>Product</c> as OneToMany for ProductCrossSellCollection
		///</summary>
		[ChildEntityType(typeof(TList<ProductCrossSell>))]
		ProductCrossSellCollection,
		///<summary>
		/// Collection of <c>Product</c> as OneToMany for ProductImageCollection
		///</summary>
		[ChildEntityType(typeof(TList<ProductImage>))]
		ProductImageCollection,
		///<summary>
		/// Collection of <c>Product</c> as OneToMany for ParentChildProductCollection
		///</summary>
		[ChildEntityType(typeof(TList<ParentChildProduct>))]
		ParentChildProductCollectionGetByChildProductID,
		///<summary>
		/// Collection of <c>Product</c> as OneToMany for ProductHighlightCollection
		///</summary>
		[ChildEntityType(typeof(TList<ProductHighlight>))]
		ProductHighlightCollection,
		///<summary>
		/// Collection of <c>Product</c> as OneToMany for LuceneGlobalProductCatalogBoostCollection
		///</summary>
		[ChildEntityType(typeof(TList<LuceneGlobalProductCatalogBoost>))]
		LuceneGlobalProductCatalogBoostCollection,
		///<summary>
		/// Collection of <c>Product</c> as OneToMany for TrackingOutboundCollection
		///</summary>
		[ChildEntityType(typeof(TList<TrackingOutbound>))]
		TrackingOutboundCollection,
		///<summary>
		/// Collection of <c>Product</c> as OneToMany for TagsCollection
		///</summary>
		[ChildEntityType(typeof(TList<Tags>))]
		TagsCollection,
		///<summary>
		/// Entity <c>LuceneGlobalProductBrandBoost</c> as OneToOne for LuceneGlobalProductBrandBoost
		///</summary>
		[ChildEntityType(typeof(LuceneGlobalProductBrandBoost))]
		LuceneGlobalProductBrandBoost,
		///<summary>
		/// Collection of <c>Product</c> as OneToMany for ProductProfileCollection
		///</summary>
		[ChildEntityType(typeof(TList<ProductProfile>))]
		ProductProfileCollection,
	}
	
	#endregion ProductChildEntityTypes
	
	#region ProductFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;ProductColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Product"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProductFilterBuilder : SqlFilterBuilder<ProductColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductFilterBuilder class.
		/// </summary>
		public ProductFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProductFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProductFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProductFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProductFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProductFilterBuilder
	
	#region ProductParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;ProductColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Product"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProductParameterBuilder : ParameterizedSqlFilterBuilder<ProductColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductParameterBuilder class.
		/// </summary>
		public ProductParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProductParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProductParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProductParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProductParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProductParameterBuilder
	
	#region ProductSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;ProductColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Product"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ProductSortBuilder : SqlSortBuilder<ProductColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductSqlSortBuilder class.
		/// </summary>
		public ProductSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ProductSortBuilder
	
} // end namespace
