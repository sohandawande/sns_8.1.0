﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="SourceModificationAuditProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class SourceModificationAuditProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.SourceModificationAudit, ZNode.Libraries.DataAccess.Entities.SourceModificationAuditKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.SourceModificationAuditKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int64 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int64 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZnodeSourceModificationAudit_ZnodeSourceModificationStatus key.
		///		FK_ZnodeSourceModificationAudit_ZnodeSourceModificationStatus Description: 
		/// </summary>
		/// <param name="_statusId"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.SourceModificationAudit objects.</returns>
		public TList<SourceModificationAudit> GetByStatusId(System.Int32 _statusId)
		{
			int count = -1;
			return GetByStatusId(_statusId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZnodeSourceModificationAudit_ZnodeSourceModificationStatus key.
		///		FK_ZnodeSourceModificationAudit_ZnodeSourceModificationStatus Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_statusId"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.SourceModificationAudit objects.</returns>
		/// <remarks></remarks>
		public TList<SourceModificationAudit> GetByStatusId(TransactionManager transactionManager, System.Int32 _statusId)
		{
			int count = -1;
			return GetByStatusId(transactionManager, _statusId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZnodeSourceModificationAudit_ZnodeSourceModificationStatus key.
		///		FK_ZnodeSourceModificationAudit_ZnodeSourceModificationStatus Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_statusId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.SourceModificationAudit objects.</returns>
		public TList<SourceModificationAudit> GetByStatusId(TransactionManager transactionManager, System.Int32 _statusId, int start, int pageLength)
		{
			int count = -1;
			return GetByStatusId(transactionManager, _statusId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZnodeSourceModificationAudit_ZnodeSourceModificationStatus key.
		///		fKZnodeSourceModificationAuditZnodeSourceModificationStatus Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_statusId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.SourceModificationAudit objects.</returns>
		public TList<SourceModificationAudit> GetByStatusId(System.Int32 _statusId, int start, int pageLength)
		{
			int count =  -1;
			return GetByStatusId(null, _statusId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZnodeSourceModificationAudit_ZnodeSourceModificationStatus key.
		///		fKZnodeSourceModificationAuditZnodeSourceModificationStatus Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_statusId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.SourceModificationAudit objects.</returns>
		public TList<SourceModificationAudit> GetByStatusId(System.Int32 _statusId, int start, int pageLength,out int count)
		{
			return GetByStatusId(null, _statusId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZnodeSourceModificationAudit_ZnodeSourceModificationStatus key.
		///		FK_ZnodeSourceModificationAudit_ZnodeSourceModificationStatus Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_statusId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.SourceModificationAudit objects.</returns>
		public abstract TList<SourceModificationAudit> GetByStatusId(TransactionManager transactionManager, System.Int32 _statusId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZnodeSourceModificationAudit_ZnodeSourceModificationType key.
		///		FK_ZnodeSourceModificationAudit_ZnodeSourceModificationType Description: 
		/// </summary>
		/// <param name="_modificationTypeId"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.SourceModificationAudit objects.</returns>
		public TList<SourceModificationAudit> GetByModificationTypeId(System.Int32 _modificationTypeId)
		{
			int count = -1;
			return GetByModificationTypeId(_modificationTypeId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZnodeSourceModificationAudit_ZnodeSourceModificationType key.
		///		FK_ZnodeSourceModificationAudit_ZnodeSourceModificationType Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modificationTypeId"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.SourceModificationAudit objects.</returns>
		/// <remarks></remarks>
		public TList<SourceModificationAudit> GetByModificationTypeId(TransactionManager transactionManager, System.Int32 _modificationTypeId)
		{
			int count = -1;
			return GetByModificationTypeId(transactionManager, _modificationTypeId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZnodeSourceModificationAudit_ZnodeSourceModificationType key.
		///		FK_ZnodeSourceModificationAudit_ZnodeSourceModificationType Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modificationTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.SourceModificationAudit objects.</returns>
		public TList<SourceModificationAudit> GetByModificationTypeId(TransactionManager transactionManager, System.Int32 _modificationTypeId, int start, int pageLength)
		{
			int count = -1;
			return GetByModificationTypeId(transactionManager, _modificationTypeId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZnodeSourceModificationAudit_ZnodeSourceModificationType key.
		///		fKZnodeSourceModificationAuditZnodeSourceModificationType Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_modificationTypeId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.SourceModificationAudit objects.</returns>
		public TList<SourceModificationAudit> GetByModificationTypeId(System.Int32 _modificationTypeId, int start, int pageLength)
		{
			int count =  -1;
			return GetByModificationTypeId(null, _modificationTypeId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZnodeSourceModificationAudit_ZnodeSourceModificationType key.
		///		fKZnodeSourceModificationAuditZnodeSourceModificationType Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_modificationTypeId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.SourceModificationAudit objects.</returns>
		public TList<SourceModificationAudit> GetByModificationTypeId(System.Int32 _modificationTypeId, int start, int pageLength,out int count)
		{
			return GetByModificationTypeId(null, _modificationTypeId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZnodeSourceModificationAudit_ZnodeSourceModificationType key.
		///		FK_ZnodeSourceModificationAudit_ZnodeSourceModificationType Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modificationTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.SourceModificationAudit objects.</returns>
		public abstract TList<SourceModificationAudit> GetByModificationTypeId(TransactionManager transactionManager, System.Int32 _modificationTypeId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeSourceModificationAudit_ZNodeSourceType key.
		///		FK_ZNodeSourceModificationAudit_ZNodeSourceType Description: 
		/// </summary>
		/// <param name="_sourceTypeId"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.SourceModificationAudit objects.</returns>
		public TList<SourceModificationAudit> GetBySourceTypeId(System.Int32 _sourceTypeId)
		{
			int count = -1;
			return GetBySourceTypeId(_sourceTypeId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeSourceModificationAudit_ZNodeSourceType key.
		///		FK_ZNodeSourceModificationAudit_ZNodeSourceType Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_sourceTypeId"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.SourceModificationAudit objects.</returns>
		/// <remarks></remarks>
		public TList<SourceModificationAudit> GetBySourceTypeId(TransactionManager transactionManager, System.Int32 _sourceTypeId)
		{
			int count = -1;
			return GetBySourceTypeId(transactionManager, _sourceTypeId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeSourceModificationAudit_ZNodeSourceType key.
		///		FK_ZNodeSourceModificationAudit_ZNodeSourceType Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_sourceTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.SourceModificationAudit objects.</returns>
		public TList<SourceModificationAudit> GetBySourceTypeId(TransactionManager transactionManager, System.Int32 _sourceTypeId, int start, int pageLength)
		{
			int count = -1;
			return GetBySourceTypeId(transactionManager, _sourceTypeId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeSourceModificationAudit_ZNodeSourceType key.
		///		fKZNodeSourceModificationAuditZNodeSourceType Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_sourceTypeId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.SourceModificationAudit objects.</returns>
		public TList<SourceModificationAudit> GetBySourceTypeId(System.Int32 _sourceTypeId, int start, int pageLength)
		{
			int count =  -1;
			return GetBySourceTypeId(null, _sourceTypeId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeSourceModificationAudit_ZNodeSourceType key.
		///		fKZNodeSourceModificationAuditZNodeSourceType Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_sourceTypeId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.SourceModificationAudit objects.</returns>
		public TList<SourceModificationAudit> GetBySourceTypeId(System.Int32 _sourceTypeId, int start, int pageLength,out int count)
		{
			return GetBySourceTypeId(null, _sourceTypeId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeSourceModificationAudit_ZNodeSourceType key.
		///		FK_ZNodeSourceModificationAudit_ZNodeSourceType Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_sourceTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.SourceModificationAudit objects.</returns>
		public abstract TList<SourceModificationAudit> GetBySourceTypeId(TransactionManager transactionManager, System.Int32 _sourceTypeId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.SourceModificationAudit Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.SourceModificationAuditKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_CreatedDate index.
		/// </summary>
		/// <param name="_createdDate"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;SourceModificationAudit&gt;"/> class.</returns>
		public TList<SourceModificationAudit> GetByCreatedDate(System.DateTime _createdDate)
		{
			int count = -1;
			return GetByCreatedDate(null,_createdDate, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CreatedDate index.
		/// </summary>
		/// <param name="_createdDate"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;SourceModificationAudit&gt;"/> class.</returns>
		public TList<SourceModificationAudit> GetByCreatedDate(System.DateTime _createdDate, int start, int pageLength)
		{
			int count = -1;
			return GetByCreatedDate(null, _createdDate, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CreatedDate index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_createdDate"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;SourceModificationAudit&gt;"/> class.</returns>
		public TList<SourceModificationAudit> GetByCreatedDate(TransactionManager transactionManager, System.DateTime _createdDate)
		{
			int count = -1;
			return GetByCreatedDate(transactionManager, _createdDate, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CreatedDate index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_createdDate"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;SourceModificationAudit&gt;"/> class.</returns>
		public TList<SourceModificationAudit> GetByCreatedDate(TransactionManager transactionManager, System.DateTime _createdDate, int start, int pageLength)
		{
			int count = -1;
			return GetByCreatedDate(transactionManager, _createdDate, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CreatedDate index.
		/// </summary>
		/// <param name="_createdDate"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;SourceModificationAudit&gt;"/> class.</returns>
		public TList<SourceModificationAudit> GetByCreatedDate(System.DateTime _createdDate, int start, int pageLength, out int count)
		{
			return GetByCreatedDate(null, _createdDate, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CreatedDate index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_createdDate"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;SourceModificationAudit&gt;"/> class.</returns>
		public abstract TList<SourceModificationAudit> GetByCreatedDate(TransactionManager transactionManager, System.DateTime _createdDate, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ExternalId index.
		/// </summary>
		/// <param name="_externalId"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;SourceModificationAudit&gt;"/> class.</returns>
		public TList<SourceModificationAudit> GetByExternalId(System.String _externalId)
		{
			int count = -1;
			return GetByExternalId(null,_externalId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ExternalId index.
		/// </summary>
		/// <param name="_externalId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;SourceModificationAudit&gt;"/> class.</returns>
		public TList<SourceModificationAudit> GetByExternalId(System.String _externalId, int start, int pageLength)
		{
			int count = -1;
			return GetByExternalId(null, _externalId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ExternalId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_externalId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;SourceModificationAudit&gt;"/> class.</returns>
		public TList<SourceModificationAudit> GetByExternalId(TransactionManager transactionManager, System.String _externalId)
		{
			int count = -1;
			return GetByExternalId(transactionManager, _externalId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ExternalId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_externalId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;SourceModificationAudit&gt;"/> class.</returns>
		public TList<SourceModificationAudit> GetByExternalId(TransactionManager transactionManager, System.String _externalId, int start, int pageLength)
		{
			int count = -1;
			return GetByExternalId(transactionManager, _externalId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ExternalId index.
		/// </summary>
		/// <param name="_externalId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;SourceModificationAudit&gt;"/> class.</returns>
		public TList<SourceModificationAudit> GetByExternalId(System.String _externalId, int start, int pageLength, out int count)
		{
			return GetByExternalId(null, _externalId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ExternalId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_externalId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;SourceModificationAudit&gt;"/> class.</returns>
		public abstract TList<SourceModificationAudit> GetByExternalId(TransactionManager transactionManager, System.String _externalId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_SourceId index.
		/// </summary>
		/// <param name="_sourceId"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;SourceModificationAudit&gt;"/> class.</returns>
		public TList<SourceModificationAudit> GetBySourceId(System.Int64 _sourceId)
		{
			int count = -1;
			return GetBySourceId(null,_sourceId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_SourceId index.
		/// </summary>
		/// <param name="_sourceId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;SourceModificationAudit&gt;"/> class.</returns>
		public TList<SourceModificationAudit> GetBySourceId(System.Int64 _sourceId, int start, int pageLength)
		{
			int count = -1;
			return GetBySourceId(null, _sourceId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_SourceId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_sourceId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;SourceModificationAudit&gt;"/> class.</returns>
		public TList<SourceModificationAudit> GetBySourceId(TransactionManager transactionManager, System.Int64 _sourceId)
		{
			int count = -1;
			return GetBySourceId(transactionManager, _sourceId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_SourceId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_sourceId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;SourceModificationAudit&gt;"/> class.</returns>
		public TList<SourceModificationAudit> GetBySourceId(TransactionManager transactionManager, System.Int64 _sourceId, int start, int pageLength)
		{
			int count = -1;
			return GetBySourceId(transactionManager, _sourceId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_SourceId index.
		/// </summary>
		/// <param name="_sourceId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;SourceModificationAudit&gt;"/> class.</returns>
		public TList<SourceModificationAudit> GetBySourceId(System.Int64 _sourceId, int start, int pageLength, out int count)
		{
			return GetBySourceId(null, _sourceId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_SourceId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_sourceId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;SourceModificationAudit&gt;"/> class.</returns>
		public abstract TList<SourceModificationAudit> GetBySourceId(TransactionManager transactionManager, System.Int64 _sourceId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZnodeSourceModificationAudit index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SourceModificationAudit"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.SourceModificationAudit GetById(System.Int64 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZnodeSourceModificationAudit index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SourceModificationAudit"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.SourceModificationAudit GetById(System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZnodeSourceModificationAudit index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SourceModificationAudit"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.SourceModificationAudit GetById(TransactionManager transactionManager, System.Int64 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZnodeSourceModificationAudit index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SourceModificationAudit"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.SourceModificationAudit GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZnodeSourceModificationAudit index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SourceModificationAudit"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.SourceModificationAudit GetById(System.Int64 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZnodeSourceModificationAudit index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SourceModificationAudit"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.SourceModificationAudit GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;SourceModificationAudit&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;SourceModificationAudit&gt;"/></returns>
		public static TList<SourceModificationAudit> Fill(IDataReader reader, TList<SourceModificationAudit> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.SourceModificationAudit c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("SourceModificationAudit")
					.Append("|").Append((System.Int64)reader[((int)SourceModificationAuditColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<SourceModificationAudit>(
					key.ToString(), // EntityTrackingKey
					"SourceModificationAudit",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.SourceModificationAudit();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int64)reader[((int)SourceModificationAuditColumn.Id - 1)];
					c.SourceTypeId = (System.Int32)reader[((int)SourceModificationAuditColumn.SourceTypeId - 1)];
					c.SourceId = (System.Int64)reader[((int)SourceModificationAuditColumn.SourceId - 1)];
					c.ExternalId = (reader.IsDBNull(((int)SourceModificationAuditColumn.ExternalId - 1)))?null:(System.String)reader[((int)SourceModificationAuditColumn.ExternalId - 1)];
					c.ModificationTypeId = (System.Int32)reader[((int)SourceModificationAuditColumn.ModificationTypeId - 1)];
					c.StatusId = (System.Int32)reader[((int)SourceModificationAuditColumn.StatusId - 1)];
					c.ProcessedDate = (reader.IsDBNull(((int)SourceModificationAuditColumn.ProcessedDate - 1)))?null:(System.DateTime?)reader[((int)SourceModificationAuditColumn.ProcessedDate - 1)];
					c.CreatedDate = (System.DateTime)reader[((int)SourceModificationAuditColumn.CreatedDate - 1)];
					c.CreatedBy = (System.String)reader[((int)SourceModificationAuditColumn.CreatedBy - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.SourceModificationAudit"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.SourceModificationAudit"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.SourceModificationAudit entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int64)reader[((int)SourceModificationAuditColumn.Id - 1)];
			entity.SourceTypeId = (System.Int32)reader[((int)SourceModificationAuditColumn.SourceTypeId - 1)];
			entity.SourceId = (System.Int64)reader[((int)SourceModificationAuditColumn.SourceId - 1)];
			entity.ExternalId = (reader.IsDBNull(((int)SourceModificationAuditColumn.ExternalId - 1)))?null:(System.String)reader[((int)SourceModificationAuditColumn.ExternalId - 1)];
			entity.ModificationTypeId = (System.Int32)reader[((int)SourceModificationAuditColumn.ModificationTypeId - 1)];
			entity.StatusId = (System.Int32)reader[((int)SourceModificationAuditColumn.StatusId - 1)];
			entity.ProcessedDate = (reader.IsDBNull(((int)SourceModificationAuditColumn.ProcessedDate - 1)))?null:(System.DateTime?)reader[((int)SourceModificationAuditColumn.ProcessedDate - 1)];
			entity.CreatedDate = (System.DateTime)reader[((int)SourceModificationAuditColumn.CreatedDate - 1)];
			entity.CreatedBy = (System.String)reader[((int)SourceModificationAuditColumn.CreatedBy - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.SourceModificationAudit"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.SourceModificationAudit"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.SourceModificationAudit entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int64)dataRow["Id"];
			entity.SourceTypeId = (System.Int32)dataRow["SourceTypeId"];
			entity.SourceId = (System.Int64)dataRow["SourceId"];
			entity.ExternalId = Convert.IsDBNull(dataRow["ExternalId"]) ? null : (System.String)dataRow["ExternalId"];
			entity.ModificationTypeId = (System.Int32)dataRow["ModificationTypeId"];
			entity.StatusId = (System.Int32)dataRow["StatusId"];
			entity.ProcessedDate = Convert.IsDBNull(dataRow["ProcessedDate"]) ? null : (System.DateTime?)dataRow["ProcessedDate"];
			entity.CreatedDate = (System.DateTime)dataRow["CreatedDate"];
			entity.CreatedBy = (System.String)dataRow["CreatedBy"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.SourceModificationAudit"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.SourceModificationAudit Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.SourceModificationAudit entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region StatusIdSource	
			if (CanDeepLoad(entity, "SourceModificationStatus|StatusIdSource", deepLoadType, innerList) 
				&& entity.StatusIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.StatusId;
				SourceModificationStatus tmpEntity = EntityManager.LocateEntity<SourceModificationStatus>(EntityLocator.ConstructKeyFromPkItems(typeof(SourceModificationStatus), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.StatusIdSource = tmpEntity;
				else
					entity.StatusIdSource = DataRepository.SourceModificationStatusProvider.GetById(transactionManager, entity.StatusId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'StatusIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.StatusIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.SourceModificationStatusProvider.DeepLoad(transactionManager, entity.StatusIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion StatusIdSource

			#region ModificationTypeIdSource	
			if (CanDeepLoad(entity, "SourceModificationType|ModificationTypeIdSource", deepLoadType, innerList) 
				&& entity.ModificationTypeIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.ModificationTypeId;
				SourceModificationType tmpEntity = EntityManager.LocateEntity<SourceModificationType>(EntityLocator.ConstructKeyFromPkItems(typeof(SourceModificationType), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ModificationTypeIdSource = tmpEntity;
				else
					entity.ModificationTypeIdSource = DataRepository.SourceModificationTypeProvider.GetById(transactionManager, entity.ModificationTypeId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ModificationTypeIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ModificationTypeIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.SourceModificationTypeProvider.DeepLoad(transactionManager, entity.ModificationTypeIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ModificationTypeIdSource

			#region SourceTypeIdSource	
			if (CanDeepLoad(entity, "SourceType|SourceTypeIdSource", deepLoadType, innerList) 
				&& entity.SourceTypeIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.SourceTypeId;
				SourceType tmpEntity = EntityManager.LocateEntity<SourceType>(EntityLocator.ConstructKeyFromPkItems(typeof(SourceType), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.SourceTypeIdSource = tmpEntity;
				else
					entity.SourceTypeIdSource = DataRepository.SourceTypeProvider.GetById(transactionManager, entity.SourceTypeId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SourceTypeIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.SourceTypeIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.SourceTypeProvider.DeepLoad(transactionManager, entity.SourceTypeIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion SourceTypeIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.SourceModificationAudit object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.SourceModificationAudit instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.SourceModificationAudit Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.SourceModificationAudit entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region StatusIdSource
			if (CanDeepSave(entity, "SourceModificationStatus|StatusIdSource", deepSaveType, innerList) 
				&& entity.StatusIdSource != null)
			{
				DataRepository.SourceModificationStatusProvider.Save(transactionManager, entity.StatusIdSource);
				entity.StatusId = entity.StatusIdSource.Id;
			}
			#endregion 
			
			#region ModificationTypeIdSource
			if (CanDeepSave(entity, "SourceModificationType|ModificationTypeIdSource", deepSaveType, innerList) 
				&& entity.ModificationTypeIdSource != null)
			{
				DataRepository.SourceModificationTypeProvider.Save(transactionManager, entity.ModificationTypeIdSource);
				entity.ModificationTypeId = entity.ModificationTypeIdSource.Id;
			}
			#endregion 
			
			#region SourceTypeIdSource
			if (CanDeepSave(entity, "SourceType|SourceTypeIdSource", deepSaveType, innerList) 
				&& entity.SourceTypeIdSource != null)
			{
				DataRepository.SourceTypeProvider.Save(transactionManager, entity.SourceTypeIdSource);
				entity.SourceTypeId = entity.SourceTypeIdSource.Id;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region SourceModificationAuditChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.SourceModificationAudit</c>
	///</summary>
	public enum SourceModificationAuditChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>SourceModificationStatus</c> at StatusIdSource
		///</summary>
		[ChildEntityType(typeof(SourceModificationStatus))]
		SourceModificationStatus,
		
		///<summary>
		/// Composite Property for <c>SourceModificationType</c> at ModificationTypeIdSource
		///</summary>
		[ChildEntityType(typeof(SourceModificationType))]
		SourceModificationType,
		
		///<summary>
		/// Composite Property for <c>SourceType</c> at SourceTypeIdSource
		///</summary>
		[ChildEntityType(typeof(SourceType))]
		SourceType,
	}
	
	#endregion SourceModificationAuditChildEntityTypes
	
	#region SourceModificationAuditFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;SourceModificationAuditColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SourceModificationAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SourceModificationAuditFilterBuilder : SqlFilterBuilder<SourceModificationAuditColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SourceModificationAuditFilterBuilder class.
		/// </summary>
		public SourceModificationAuditFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the SourceModificationAuditFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SourceModificationAuditFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SourceModificationAuditFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SourceModificationAuditFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SourceModificationAuditFilterBuilder
	
	#region SourceModificationAuditParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;SourceModificationAuditColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SourceModificationAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SourceModificationAuditParameterBuilder : ParameterizedSqlFilterBuilder<SourceModificationAuditColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SourceModificationAuditParameterBuilder class.
		/// </summary>
		public SourceModificationAuditParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the SourceModificationAuditParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SourceModificationAuditParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SourceModificationAuditParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SourceModificationAuditParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SourceModificationAuditParameterBuilder
	
	#region SourceModificationAuditSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;SourceModificationAuditColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SourceModificationAudit"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class SourceModificationAuditSortBuilder : SqlSortBuilder<SourceModificationAuditColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SourceModificationAuditSqlSortBuilder class.
		/// </summary>
		public SourceModificationAuditSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion SourceModificationAuditSortBuilder
	
} // end namespace
