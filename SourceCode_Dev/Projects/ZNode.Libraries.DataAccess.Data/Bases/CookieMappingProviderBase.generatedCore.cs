﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="CookieMappingProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class CookieMappingProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.CookieMapping, ZNode.Libraries.DataAccess.Entities.CookieMappingKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.CookieMappingKey key)
		{
			return Delete(transactionManager, key.CookieMappingID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_cookieMappingID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _cookieMappingID)
		{
			return Delete(null, _cookieMappingID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_cookieMappingID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _cookieMappingID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeCookieMapping_ZNodePortal key.
		///		FK_ZNodeCookieMapping_ZNodePortal Description: 
		/// </summary>
		/// <param name="_portalID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.CookieMapping objects.</returns>
		public TList<CookieMapping> GetByPortalID(System.Int32 _portalID)
		{
			int count = -1;
			return GetByPortalID(_portalID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeCookieMapping_ZNodePortal key.
		///		FK_ZNodeCookieMapping_ZNodePortal Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.CookieMapping objects.</returns>
		/// <remarks></remarks>
		public TList<CookieMapping> GetByPortalID(TransactionManager transactionManager, System.Int32 _portalID)
		{
			int count = -1;
			return GetByPortalID(transactionManager, _portalID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeCookieMapping_ZNodePortal key.
		///		FK_ZNodeCookieMapping_ZNodePortal Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.CookieMapping objects.</returns>
		public TList<CookieMapping> GetByPortalID(TransactionManager transactionManager, System.Int32 _portalID, int start, int pageLength)
		{
			int count = -1;
			return GetByPortalID(transactionManager, _portalID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeCookieMapping_ZNodePortal key.
		///		fKZNodeCookieMappingZNodePortal Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_portalID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.CookieMapping objects.</returns>
		public TList<CookieMapping> GetByPortalID(System.Int32 _portalID, int start, int pageLength)
		{
			int count =  -1;
			return GetByPortalID(null, _portalID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeCookieMapping_ZNodePortal key.
		///		fKZNodeCookieMappingZNodePortal Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_portalID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.CookieMapping objects.</returns>
		public TList<CookieMapping> GetByPortalID(System.Int32 _portalID, int start, int pageLength,out int count)
		{
			return GetByPortalID(null, _portalID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeCookieMapping_ZNodePortal key.
		///		FK_ZNodeCookieMapping_ZNodePortal Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.CookieMapping objects.</returns>
		public abstract TList<CookieMapping> GetByPortalID(TransactionManager transactionManager, System.Int32 _portalID, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.CookieMapping Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.CookieMappingKey key, int start, int pageLength)
		{
			return GetByCookieMappingID(transactionManager, key.CookieMappingID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IDX_ZNodeCookieMapping_AccountID index.
		/// </summary>
		/// <param name="_accountID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;CookieMapping&gt;"/> class.</returns>
		public TList<CookieMapping> GetByAccountID(System.Int32? _accountID)
		{
			int count = -1;
			return GetByAccountID(null,_accountID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IDX_ZNodeCookieMapping_AccountID index.
		/// </summary>
		/// <param name="_accountID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CookieMapping&gt;"/> class.</returns>
		public TList<CookieMapping> GetByAccountID(System.Int32? _accountID, int start, int pageLength)
		{
			int count = -1;
			return GetByAccountID(null, _accountID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IDX_ZNodeCookieMapping_AccountID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accountID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CookieMapping&gt;"/> class.</returns>
		public TList<CookieMapping> GetByAccountID(TransactionManager transactionManager, System.Int32? _accountID)
		{
			int count = -1;
			return GetByAccountID(transactionManager, _accountID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IDX_ZNodeCookieMapping_AccountID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accountID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CookieMapping&gt;"/> class.</returns>
		public TList<CookieMapping> GetByAccountID(TransactionManager transactionManager, System.Int32? _accountID, int start, int pageLength)
		{
			int count = -1;
			return GetByAccountID(transactionManager, _accountID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IDX_ZNodeCookieMapping_AccountID index.
		/// </summary>
		/// <param name="_accountID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CookieMapping&gt;"/> class.</returns>
		public TList<CookieMapping> GetByAccountID(System.Int32? _accountID, int start, int pageLength, out int count)
		{
			return GetByAccountID(null, _accountID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IDX_ZNodeCookieMapping_AccountID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accountID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;CookieMapping&gt;"/> class.</returns>
		public abstract TList<CookieMapping> GetByAccountID(TransactionManager transactionManager, System.Int32? _accountID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodeCookieMapping index.
		/// </summary>
		/// <param name="_cookieMappingID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.CookieMapping"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.CookieMapping GetByCookieMappingID(System.Int32 _cookieMappingID)
		{
			int count = -1;
			return GetByCookieMappingID(null,_cookieMappingID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeCookieMapping index.
		/// </summary>
		/// <param name="_cookieMappingID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.CookieMapping"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.CookieMapping GetByCookieMappingID(System.Int32 _cookieMappingID, int start, int pageLength)
		{
			int count = -1;
			return GetByCookieMappingID(null, _cookieMappingID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeCookieMapping index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_cookieMappingID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.CookieMapping"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.CookieMapping GetByCookieMappingID(TransactionManager transactionManager, System.Int32 _cookieMappingID)
		{
			int count = -1;
			return GetByCookieMappingID(transactionManager, _cookieMappingID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeCookieMapping index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_cookieMappingID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.CookieMapping"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.CookieMapping GetByCookieMappingID(TransactionManager transactionManager, System.Int32 _cookieMappingID, int start, int pageLength)
		{
			int count = -1;
			return GetByCookieMappingID(transactionManager, _cookieMappingID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeCookieMapping index.
		/// </summary>
		/// <param name="_cookieMappingID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.CookieMapping"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.CookieMapping GetByCookieMappingID(System.Int32 _cookieMappingID, int start, int pageLength, out int count)
		{
			return GetByCookieMappingID(null, _cookieMappingID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeCookieMapping index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_cookieMappingID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.CookieMapping"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.CookieMapping GetByCookieMappingID(TransactionManager transactionManager, System.Int32 _cookieMappingID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;CookieMapping&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;CookieMapping&gt;"/></returns>
		public static TList<CookieMapping> Fill(IDataReader reader, TList<CookieMapping> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.CookieMapping c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("CookieMapping")
					.Append("|").Append((System.Int32)reader[((int)CookieMappingColumn.CookieMappingID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<CookieMapping>(
					key.ToString(), // EntityTrackingKey
					"CookieMapping",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.CookieMapping();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.CookieMappingID = (System.Int32)reader[((int)CookieMappingColumn.CookieMappingID - 1)];
					c.AccountID = (reader.IsDBNull(((int)CookieMappingColumn.AccountID - 1)))?null:(System.Int32?)reader[((int)CookieMappingColumn.AccountID - 1)];
					c.PortalID = (System.Int32)reader[((int)CookieMappingColumn.PortalID - 1)];
					c.CreatedDate = (System.DateTime)reader[((int)CookieMappingColumn.CreatedDate - 1)];
					c.LastUpdatedDate = (System.DateTime)reader[((int)CookieMappingColumn.LastUpdatedDate - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.CookieMapping"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.CookieMapping"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.CookieMapping entity)
		{
			if (!reader.Read()) return;
			
			entity.CookieMappingID = (System.Int32)reader[((int)CookieMappingColumn.CookieMappingID - 1)];
			entity.AccountID = (reader.IsDBNull(((int)CookieMappingColumn.AccountID - 1)))?null:(System.Int32?)reader[((int)CookieMappingColumn.AccountID - 1)];
			entity.PortalID = (System.Int32)reader[((int)CookieMappingColumn.PortalID - 1)];
			entity.CreatedDate = (System.DateTime)reader[((int)CookieMappingColumn.CreatedDate - 1)];
			entity.LastUpdatedDate = (System.DateTime)reader[((int)CookieMappingColumn.LastUpdatedDate - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.CookieMapping"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.CookieMapping"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.CookieMapping entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.CookieMappingID = (System.Int32)dataRow["CookieMappingID"];
			entity.AccountID = Convert.IsDBNull(dataRow["AccountID"]) ? null : (System.Int32?)dataRow["AccountID"];
			entity.PortalID = (System.Int32)dataRow["PortalID"];
			entity.CreatedDate = (System.DateTime)dataRow["CreatedDate"];
			entity.LastUpdatedDate = (System.DateTime)dataRow["LastUpdatedDate"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.CookieMapping"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.CookieMapping Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.CookieMapping entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region AccountIDSource	
			if (CanDeepLoad(entity, "Account|AccountIDSource", deepLoadType, innerList) 
				&& entity.AccountIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.AccountID ?? (int)0);
				Account tmpEntity = EntityManager.LocateEntity<Account>(EntityLocator.ConstructKeyFromPkItems(typeof(Account), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.AccountIDSource = tmpEntity;
				else
					entity.AccountIDSource = DataRepository.AccountProvider.GetByAccountID(transactionManager, (entity.AccountID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AccountIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.AccountIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.AccountProvider.DeepLoad(transactionManager, entity.AccountIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion AccountIDSource

			#region PortalIDSource	
			if (CanDeepLoad(entity, "Portal|PortalIDSource", deepLoadType, innerList) 
				&& entity.PortalIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.PortalID;
				Portal tmpEntity = EntityManager.LocateEntity<Portal>(EntityLocator.ConstructKeyFromPkItems(typeof(Portal), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.PortalIDSource = tmpEntity;
				else
					entity.PortalIDSource = DataRepository.PortalProvider.GetByPortalID(transactionManager, entity.PortalID);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PortalIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.PortalIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.PortalProvider.DeepLoad(transactionManager, entity.PortalIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion PortalIDSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByCookieMappingID methods when available
			
			#region SavedCartCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<SavedCart>|SavedCartCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SavedCartCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.SavedCartCollection = DataRepository.SavedCartProvider.GetByCookieMappingID(transactionManager, entity.CookieMappingID);

				if (deep && entity.SavedCartCollection.Count > 0)
				{
					deepHandles.Add("SavedCartCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<SavedCart>) DataRepository.SavedCartProvider.DeepLoad,
						new object[] { transactionManager, entity.SavedCartCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.CookieMapping object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.CookieMapping instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.CookieMapping Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.CookieMapping entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region AccountIDSource
			if (CanDeepSave(entity, "Account|AccountIDSource", deepSaveType, innerList) 
				&& entity.AccountIDSource != null)
			{
				DataRepository.AccountProvider.Save(transactionManager, entity.AccountIDSource);
				entity.AccountID = entity.AccountIDSource.AccountID;
			}
			#endregion 
			
			#region PortalIDSource
			if (CanDeepSave(entity, "Portal|PortalIDSource", deepSaveType, innerList) 
				&& entity.PortalIDSource != null)
			{
				DataRepository.PortalProvider.Save(transactionManager, entity.PortalIDSource);
				entity.PortalID = entity.PortalIDSource.PortalID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<SavedCart>
				if (CanDeepSave(entity.SavedCartCollection, "List<SavedCart>|SavedCartCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(SavedCart child in entity.SavedCartCollection)
					{
						if(child.CookieMappingIDSource != null)
						{
							child.CookieMappingID = child.CookieMappingIDSource.CookieMappingID;
						}
						else
						{
							child.CookieMappingID = entity.CookieMappingID;
						}

					}

					if (entity.SavedCartCollection.Count > 0 || entity.SavedCartCollection.DeletedItems.Count > 0)
					{
						//DataRepository.SavedCartProvider.Save(transactionManager, entity.SavedCartCollection);
						
						deepHandles.Add("SavedCartCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< SavedCart >) DataRepository.SavedCartProvider.DeepSave,
							new object[] { transactionManager, entity.SavedCartCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region CookieMappingChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.CookieMapping</c>
	///</summary>
	public enum CookieMappingChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Account</c> at AccountIDSource
		///</summary>
		[ChildEntityType(typeof(Account))]
		Account,
		
		///<summary>
		/// Composite Property for <c>Portal</c> at PortalIDSource
		///</summary>
		[ChildEntityType(typeof(Portal))]
		Portal,
		///<summary>
		/// Collection of <c>CookieMapping</c> as OneToMany for SavedCartCollection
		///</summary>
		[ChildEntityType(typeof(TList<SavedCart>))]
		SavedCartCollection,
	}
	
	#endregion CookieMappingChildEntityTypes
	
	#region CookieMappingFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;CookieMappingColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CookieMapping"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CookieMappingFilterBuilder : SqlFilterBuilder<CookieMappingColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CookieMappingFilterBuilder class.
		/// </summary>
		public CookieMappingFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CookieMappingFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CookieMappingFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CookieMappingFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CookieMappingFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CookieMappingFilterBuilder
	
	#region CookieMappingParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;CookieMappingColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CookieMapping"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CookieMappingParameterBuilder : ParameterizedSqlFilterBuilder<CookieMappingColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CookieMappingParameterBuilder class.
		/// </summary>
		public CookieMappingParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CookieMappingParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CookieMappingParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CookieMappingParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CookieMappingParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CookieMappingParameterBuilder
	
	#region CookieMappingSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;CookieMappingColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CookieMapping"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class CookieMappingSortBuilder : SqlSortBuilder<CookieMappingColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CookieMappingSqlSortBuilder class.
		/// </summary>
		public CookieMappingSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion CookieMappingSortBuilder
	
} // end namespace
