﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ProductExtensionOptionProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class ProductExtensionOptionProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.ProductExtensionOption, ZNode.Libraries.DataAccess.Entities.ProductExtensionOptionKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ProductExtensionOptionKey key)
		{
			return Delete(transactionManager, key.ProductExtensionOptionID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_productExtensionOptionID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _productExtensionOptionID)
		{
			return Delete(null, _productExtensionOptionID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productExtensionOptionID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _productExtensionOptionID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.ProductExtensionOption Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ProductExtensionOptionKey key, int start, int pageLength)
		{
			return GetByProductExtensionOptionID(transactionManager, key.ProductExtensionOptionID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodeProductExtensionOption index.
		/// </summary>
		/// <param name="_productExtensionOptionID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductExtensionOption"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductExtensionOption GetByProductExtensionOptionID(System.Int32 _productExtensionOptionID)
		{
			int count = -1;
			return GetByProductExtensionOptionID(null,_productExtensionOptionID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeProductExtensionOption index.
		/// </summary>
		/// <param name="_productExtensionOptionID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductExtensionOption"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductExtensionOption GetByProductExtensionOptionID(System.Int32 _productExtensionOptionID, int start, int pageLength)
		{
			int count = -1;
			return GetByProductExtensionOptionID(null, _productExtensionOptionID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeProductExtensionOption index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productExtensionOptionID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductExtensionOption"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductExtensionOption GetByProductExtensionOptionID(TransactionManager transactionManager, System.Int32 _productExtensionOptionID)
		{
			int count = -1;
			return GetByProductExtensionOptionID(transactionManager, _productExtensionOptionID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeProductExtensionOption index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productExtensionOptionID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductExtensionOption"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductExtensionOption GetByProductExtensionOptionID(TransactionManager transactionManager, System.Int32 _productExtensionOptionID, int start, int pageLength)
		{
			int count = -1;
			return GetByProductExtensionOptionID(transactionManager, _productExtensionOptionID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeProductExtensionOption index.
		/// </summary>
		/// <param name="_productExtensionOptionID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductExtensionOption"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductExtensionOption GetByProductExtensionOptionID(System.Int32 _productExtensionOptionID, int start, int pageLength, out int count)
		{
			return GetByProductExtensionOptionID(null, _productExtensionOptionID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeProductExtensionOption index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productExtensionOptionID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductExtensionOption"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.ProductExtensionOption GetByProductExtensionOptionID(TransactionManager transactionManager, System.Int32 _productExtensionOptionID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;ProductExtensionOption&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;ProductExtensionOption&gt;"/></returns>
		public static TList<ProductExtensionOption> Fill(IDataReader reader, TList<ProductExtensionOption> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.ProductExtensionOption c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("ProductExtensionOption")
					.Append("|").Append((System.Int32)reader[((int)ProductExtensionOptionColumn.ProductExtensionOptionID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<ProductExtensionOption>(
					key.ToString(), // EntityTrackingKey
					"ProductExtensionOption",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.ProductExtensionOption();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.ProductExtensionOptionID = (System.Int32)reader[((int)ProductExtensionOptionColumn.ProductExtensionOptionID - 1)];
					c.LabelName = (reader.IsDBNull(((int)ProductExtensionOptionColumn.LabelName - 1)))?null:(System.String)reader[((int)ProductExtensionOptionColumn.LabelName - 1)];
					c.OptionType = (reader.IsDBNull(((int)ProductExtensionOptionColumn.OptionType - 1)))?null:(System.Int32?)reader[((int)ProductExtensionOptionColumn.OptionType - 1)];
					c.ListData = (reader.IsDBNull(((int)ProductExtensionOptionColumn.ListData - 1)))?null:(System.String)reader[((int)ProductExtensionOptionColumn.ListData - 1)];
					c.IsRequired = (reader.IsDBNull(((int)ProductExtensionOptionColumn.IsRequired - 1)))?null:(System.Boolean?)reader[((int)ProductExtensionOptionColumn.IsRequired - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.ProductExtensionOption"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ProductExtensionOption"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.ProductExtensionOption entity)
		{
			if (!reader.Read()) return;
			
			entity.ProductExtensionOptionID = (System.Int32)reader[((int)ProductExtensionOptionColumn.ProductExtensionOptionID - 1)];
			entity.LabelName = (reader.IsDBNull(((int)ProductExtensionOptionColumn.LabelName - 1)))?null:(System.String)reader[((int)ProductExtensionOptionColumn.LabelName - 1)];
			entity.OptionType = (reader.IsDBNull(((int)ProductExtensionOptionColumn.OptionType - 1)))?null:(System.Int32?)reader[((int)ProductExtensionOptionColumn.OptionType - 1)];
			entity.ListData = (reader.IsDBNull(((int)ProductExtensionOptionColumn.ListData - 1)))?null:(System.String)reader[((int)ProductExtensionOptionColumn.ListData - 1)];
			entity.IsRequired = (reader.IsDBNull(((int)ProductExtensionOptionColumn.IsRequired - 1)))?null:(System.Boolean?)reader[((int)ProductExtensionOptionColumn.IsRequired - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.ProductExtensionOption"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ProductExtensionOption"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.ProductExtensionOption entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.ProductExtensionOptionID = (System.Int32)dataRow["ProductExtensionOptionID"];
			entity.LabelName = Convert.IsDBNull(dataRow["LabelName"]) ? null : (System.String)dataRow["LabelName"];
			entity.OptionType = Convert.IsDBNull(dataRow["OptionType"]) ? null : (System.Int32?)dataRow["OptionType"];
			entity.ListData = Convert.IsDBNull(dataRow["ListData"]) ? null : (System.String)dataRow["ListData"];
			entity.IsRequired = Convert.IsDBNull(dataRow["IsRequired"]) ? null : (System.Boolean?)dataRow["IsRequired"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ProductExtensionOption"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.ProductExtensionOption Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ProductExtensionOption entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByProductExtensionOptionID methods when available
			
			#region ProductExtensionOptionProductCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<ProductExtensionOptionProduct>|ProductExtensionOptionProductCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ProductExtensionOptionProductCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.ProductExtensionOptionProductCollection = DataRepository.ProductExtensionOptionProductProvider.GetByProductExtensionOptionID(transactionManager, entity.ProductExtensionOptionID);

				if (deep && entity.ProductExtensionOptionProductCollection.Count > 0)
				{
					deepHandles.Add("ProductExtensionOptionProductCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<ProductExtensionOptionProduct>) DataRepository.ProductExtensionOptionProductProvider.DeepLoad,
						new object[] { transactionManager, entity.ProductExtensionOptionProductCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region OrderDetailProductExtensionOptionCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<OrderDetailProductExtensionOption>|OrderDetailProductExtensionOptionCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'OrderDetailProductExtensionOptionCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.OrderDetailProductExtensionOptionCollection = DataRepository.OrderDetailProductExtensionOptionProvider.GetByProductExtensionOptionID(transactionManager, entity.ProductExtensionOptionID);

				if (deep && entity.OrderDetailProductExtensionOptionCollection.Count > 0)
				{
					deepHandles.Add("OrderDetailProductExtensionOptionCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<OrderDetailProductExtensionOption>) DataRepository.OrderDetailProductExtensionOptionProvider.DeepLoad,
						new object[] { transactionManager, entity.OrderDetailProductExtensionOptionCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.ProductExtensionOption object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.ProductExtensionOption instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.ProductExtensionOption Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ProductExtensionOption entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<ProductExtensionOptionProduct>
				if (CanDeepSave(entity.ProductExtensionOptionProductCollection, "List<ProductExtensionOptionProduct>|ProductExtensionOptionProductCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(ProductExtensionOptionProduct child in entity.ProductExtensionOptionProductCollection)
					{
						if(child.ProductExtensionOptionIDSource != null)
						{
							child.ProductExtensionOptionID = child.ProductExtensionOptionIDSource.ProductExtensionOptionID;
						}
						else
						{
							child.ProductExtensionOptionID = entity.ProductExtensionOptionID;
						}

					}

					if (entity.ProductExtensionOptionProductCollection.Count > 0 || entity.ProductExtensionOptionProductCollection.DeletedItems.Count > 0)
					{
						//DataRepository.ProductExtensionOptionProductProvider.Save(transactionManager, entity.ProductExtensionOptionProductCollection);
						
						deepHandles.Add("ProductExtensionOptionProductCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< ProductExtensionOptionProduct >) DataRepository.ProductExtensionOptionProductProvider.DeepSave,
							new object[] { transactionManager, entity.ProductExtensionOptionProductCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<OrderDetailProductExtensionOption>
				if (CanDeepSave(entity.OrderDetailProductExtensionOptionCollection, "List<OrderDetailProductExtensionOption>|OrderDetailProductExtensionOptionCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(OrderDetailProductExtensionOption child in entity.OrderDetailProductExtensionOptionCollection)
					{
						if(child.ProductExtensionOptionIDSource != null)
						{
							child.ProductExtensionOptionID = child.ProductExtensionOptionIDSource.ProductExtensionOptionID;
						}
						else
						{
							child.ProductExtensionOptionID = entity.ProductExtensionOptionID;
						}

					}

					if (entity.OrderDetailProductExtensionOptionCollection.Count > 0 || entity.OrderDetailProductExtensionOptionCollection.DeletedItems.Count > 0)
					{
						//DataRepository.OrderDetailProductExtensionOptionProvider.Save(transactionManager, entity.OrderDetailProductExtensionOptionCollection);
						
						deepHandles.Add("OrderDetailProductExtensionOptionCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< OrderDetailProductExtensionOption >) DataRepository.OrderDetailProductExtensionOptionProvider.DeepSave,
							new object[] { transactionManager, entity.OrderDetailProductExtensionOptionCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region ProductExtensionOptionChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.ProductExtensionOption</c>
	///</summary>
	public enum ProductExtensionOptionChildEntityTypes
	{
		///<summary>
		/// Collection of <c>ProductExtensionOption</c> as OneToMany for ProductExtensionOptionProductCollection
		///</summary>
		[ChildEntityType(typeof(TList<ProductExtensionOptionProduct>))]
		ProductExtensionOptionProductCollection,
		///<summary>
		/// Collection of <c>ProductExtensionOption</c> as OneToMany for OrderDetailProductExtensionOptionCollection
		///</summary>
		[ChildEntityType(typeof(TList<OrderDetailProductExtensionOption>))]
		OrderDetailProductExtensionOptionCollection,
	}
	
	#endregion ProductExtensionOptionChildEntityTypes
	
	#region ProductExtensionOptionFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;ProductExtensionOptionColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ProductExtensionOption"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProductExtensionOptionFilterBuilder : SqlFilterBuilder<ProductExtensionOptionColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductExtensionOptionFilterBuilder class.
		/// </summary>
		public ProductExtensionOptionFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProductExtensionOptionFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProductExtensionOptionFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProductExtensionOptionFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProductExtensionOptionFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProductExtensionOptionFilterBuilder
	
	#region ProductExtensionOptionParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;ProductExtensionOptionColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ProductExtensionOption"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProductExtensionOptionParameterBuilder : ParameterizedSqlFilterBuilder<ProductExtensionOptionColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductExtensionOptionParameterBuilder class.
		/// </summary>
		public ProductExtensionOptionParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProductExtensionOptionParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProductExtensionOptionParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProductExtensionOptionParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProductExtensionOptionParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProductExtensionOptionParameterBuilder
	
	#region ProductExtensionOptionSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;ProductExtensionOptionColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ProductExtensionOption"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ProductExtensionOptionSortBuilder : SqlSortBuilder<ProductExtensionOptionColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductExtensionOptionSqlSortBuilder class.
		/// </summary>
		public ProductExtensionOptionSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ProductExtensionOptionSortBuilder
	
} // end namespace
