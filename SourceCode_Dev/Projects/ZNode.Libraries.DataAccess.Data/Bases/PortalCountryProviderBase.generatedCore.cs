﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="PortalCountryProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class PortalCountryProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.PortalCountry, ZNode.Libraries.DataAccess.Entities.PortalCountryKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.PortalCountryKey key)
		{
			return Delete(transactionManager, key.PortalCountryID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_portalCountryID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _portalCountryID)
		{
			return Delete(null, _portalCountryID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalCountryID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _portalCountryID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.PortalCountry Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.PortalCountryKey key, int start, int pageLength)
		{
			return GetByPortalCountryID(transactionManager, key.PortalCountryID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodePortalCountry_BillingActive index.
		/// </summary>
		/// <param name="_billingActive"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;PortalCountry&gt;"/> class.</returns>
		public TList<PortalCountry> GetByBillingActive(System.Boolean _billingActive)
		{
			int count = -1;
			return GetByBillingActive(null,_billingActive, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePortalCountry_BillingActive index.
		/// </summary>
		/// <param name="_billingActive"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;PortalCountry&gt;"/> class.</returns>
		public TList<PortalCountry> GetByBillingActive(System.Boolean _billingActive, int start, int pageLength)
		{
			int count = -1;
			return GetByBillingActive(null, _billingActive, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePortalCountry_BillingActive index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_billingActive"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;PortalCountry&gt;"/> class.</returns>
		public TList<PortalCountry> GetByBillingActive(TransactionManager transactionManager, System.Boolean _billingActive)
		{
			int count = -1;
			return GetByBillingActive(transactionManager, _billingActive, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePortalCountry_BillingActive index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_billingActive"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;PortalCountry&gt;"/> class.</returns>
		public TList<PortalCountry> GetByBillingActive(TransactionManager transactionManager, System.Boolean _billingActive, int start, int pageLength)
		{
			int count = -1;
			return GetByBillingActive(transactionManager, _billingActive, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePortalCountry_BillingActive index.
		/// </summary>
		/// <param name="_billingActive"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;PortalCountry&gt;"/> class.</returns>
		public TList<PortalCountry> GetByBillingActive(System.Boolean _billingActive, int start, int pageLength, out int count)
		{
			return GetByBillingActive(null, _billingActive, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePortalCountry_BillingActive index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_billingActive"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;PortalCountry&gt;"/> class.</returns>
		public abstract TList<PortalCountry> GetByBillingActive(TransactionManager transactionManager, System.Boolean _billingActive, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodePortalCountry_CountryCode index.
		/// </summary>
		/// <param name="_countryCode"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;PortalCountry&gt;"/> class.</returns>
		public TList<PortalCountry> GetByCountryCode(System.String _countryCode)
		{
			int count = -1;
			return GetByCountryCode(null,_countryCode, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePortalCountry_CountryCode index.
		/// </summary>
		/// <param name="_countryCode"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;PortalCountry&gt;"/> class.</returns>
		public TList<PortalCountry> GetByCountryCode(System.String _countryCode, int start, int pageLength)
		{
			int count = -1;
			return GetByCountryCode(null, _countryCode, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePortalCountry_CountryCode index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_countryCode"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;PortalCountry&gt;"/> class.</returns>
		public TList<PortalCountry> GetByCountryCode(TransactionManager transactionManager, System.String _countryCode)
		{
			int count = -1;
			return GetByCountryCode(transactionManager, _countryCode, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePortalCountry_CountryCode index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_countryCode"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;PortalCountry&gt;"/> class.</returns>
		public TList<PortalCountry> GetByCountryCode(TransactionManager transactionManager, System.String _countryCode, int start, int pageLength)
		{
			int count = -1;
			return GetByCountryCode(transactionManager, _countryCode, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePortalCountry_CountryCode index.
		/// </summary>
		/// <param name="_countryCode"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;PortalCountry&gt;"/> class.</returns>
		public TList<PortalCountry> GetByCountryCode(System.String _countryCode, int start, int pageLength, out int count)
		{
			return GetByCountryCode(null, _countryCode, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePortalCountry_CountryCode index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_countryCode"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;PortalCountry&gt;"/> class.</returns>
		public abstract TList<PortalCountry> GetByCountryCode(TransactionManager transactionManager, System.String _countryCode, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodePortalCountry_PortalID index.
		/// </summary>
		/// <param name="_portalID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;PortalCountry&gt;"/> class.</returns>
		public TList<PortalCountry> GetByPortalID(System.Int32 _portalID)
		{
			int count = -1;
			return GetByPortalID(null,_portalID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePortalCountry_PortalID index.
		/// </summary>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;PortalCountry&gt;"/> class.</returns>
		public TList<PortalCountry> GetByPortalID(System.Int32 _portalID, int start, int pageLength)
		{
			int count = -1;
			return GetByPortalID(null, _portalID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePortalCountry_PortalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;PortalCountry&gt;"/> class.</returns>
		public TList<PortalCountry> GetByPortalID(TransactionManager transactionManager, System.Int32 _portalID)
		{
			int count = -1;
			return GetByPortalID(transactionManager, _portalID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePortalCountry_PortalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;PortalCountry&gt;"/> class.</returns>
		public TList<PortalCountry> GetByPortalID(TransactionManager transactionManager, System.Int32 _portalID, int start, int pageLength)
		{
			int count = -1;
			return GetByPortalID(transactionManager, _portalID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePortalCountry_PortalID index.
		/// </summary>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;PortalCountry&gt;"/> class.</returns>
		public TList<PortalCountry> GetByPortalID(System.Int32 _portalID, int start, int pageLength, out int count)
		{
			return GetByPortalID(null, _portalID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePortalCountry_PortalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;PortalCountry&gt;"/> class.</returns>
		public abstract TList<PortalCountry> GetByPortalID(TransactionManager transactionManager, System.Int32 _portalID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodePortalCountry_ShippingActive index.
		/// </summary>
		/// <param name="_shippingActive"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;PortalCountry&gt;"/> class.</returns>
		public TList<PortalCountry> GetByShippingActive(System.Boolean _shippingActive)
		{
			int count = -1;
			return GetByShippingActive(null,_shippingActive, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePortalCountry_ShippingActive index.
		/// </summary>
		/// <param name="_shippingActive"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;PortalCountry&gt;"/> class.</returns>
		public TList<PortalCountry> GetByShippingActive(System.Boolean _shippingActive, int start, int pageLength)
		{
			int count = -1;
			return GetByShippingActive(null, _shippingActive, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePortalCountry_ShippingActive index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_shippingActive"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;PortalCountry&gt;"/> class.</returns>
		public TList<PortalCountry> GetByShippingActive(TransactionManager transactionManager, System.Boolean _shippingActive)
		{
			int count = -1;
			return GetByShippingActive(transactionManager, _shippingActive, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePortalCountry_ShippingActive index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_shippingActive"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;PortalCountry&gt;"/> class.</returns>
		public TList<PortalCountry> GetByShippingActive(TransactionManager transactionManager, System.Boolean _shippingActive, int start, int pageLength)
		{
			int count = -1;
			return GetByShippingActive(transactionManager, _shippingActive, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePortalCountry_ShippingActive index.
		/// </summary>
		/// <param name="_shippingActive"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;PortalCountry&gt;"/> class.</returns>
		public TList<PortalCountry> GetByShippingActive(System.Boolean _shippingActive, int start, int pageLength, out int count)
		{
			return GetByShippingActive(null, _shippingActive, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePortalCountry_ShippingActive index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_shippingActive"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;PortalCountry&gt;"/> class.</returns>
		public abstract TList<PortalCountry> GetByShippingActive(TransactionManager transactionManager, System.Boolean _shippingActive, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodePortalCountry index.
		/// </summary>
		/// <param name="_portalCountryID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.PortalCountry"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.PortalCountry GetByPortalCountryID(System.Int32 _portalCountryID)
		{
			int count = -1;
			return GetByPortalCountryID(null,_portalCountryID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodePortalCountry index.
		/// </summary>
		/// <param name="_portalCountryID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.PortalCountry"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.PortalCountry GetByPortalCountryID(System.Int32 _portalCountryID, int start, int pageLength)
		{
			int count = -1;
			return GetByPortalCountryID(null, _portalCountryID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodePortalCountry index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalCountryID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.PortalCountry"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.PortalCountry GetByPortalCountryID(TransactionManager transactionManager, System.Int32 _portalCountryID)
		{
			int count = -1;
			return GetByPortalCountryID(transactionManager, _portalCountryID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodePortalCountry index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalCountryID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.PortalCountry"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.PortalCountry GetByPortalCountryID(TransactionManager transactionManager, System.Int32 _portalCountryID, int start, int pageLength)
		{
			int count = -1;
			return GetByPortalCountryID(transactionManager, _portalCountryID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodePortalCountry index.
		/// </summary>
		/// <param name="_portalCountryID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.PortalCountry"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.PortalCountry GetByPortalCountryID(System.Int32 _portalCountryID, int start, int pageLength, out int count)
		{
			return GetByPortalCountryID(null, _portalCountryID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodePortalCountry index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalCountryID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.PortalCountry"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.PortalCountry GetByPortalCountryID(TransactionManager transactionManager, System.Int32 _portalCountryID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;PortalCountry&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;PortalCountry&gt;"/></returns>
		public static TList<PortalCountry> Fill(IDataReader reader, TList<PortalCountry> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.PortalCountry c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("PortalCountry")
					.Append("|").Append((System.Int32)reader[((int)PortalCountryColumn.PortalCountryID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<PortalCountry>(
					key.ToString(), // EntityTrackingKey
					"PortalCountry",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.PortalCountry();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.PortalCountryID = (System.Int32)reader[((int)PortalCountryColumn.PortalCountryID - 1)];
					c.PortalID = (System.Int32)reader[((int)PortalCountryColumn.PortalID - 1)];
					c.CountryCode = (System.String)reader[((int)PortalCountryColumn.CountryCode - 1)];
					c.BillingActive = (System.Boolean)reader[((int)PortalCountryColumn.BillingActive - 1)];
					c.ShippingActive = (System.Boolean)reader[((int)PortalCountryColumn.ShippingActive - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.PortalCountry"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.PortalCountry"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.PortalCountry entity)
		{
			if (!reader.Read()) return;
			
			entity.PortalCountryID = (System.Int32)reader[((int)PortalCountryColumn.PortalCountryID - 1)];
			entity.PortalID = (System.Int32)reader[((int)PortalCountryColumn.PortalID - 1)];
			entity.CountryCode = (System.String)reader[((int)PortalCountryColumn.CountryCode - 1)];
			entity.BillingActive = (System.Boolean)reader[((int)PortalCountryColumn.BillingActive - 1)];
			entity.ShippingActive = (System.Boolean)reader[((int)PortalCountryColumn.ShippingActive - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.PortalCountry"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.PortalCountry"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.PortalCountry entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.PortalCountryID = (System.Int32)dataRow["PortalCountryID"];
			entity.PortalID = (System.Int32)dataRow["PortalID"];
			entity.CountryCode = (System.String)dataRow["CountryCode"];
			entity.BillingActive = (System.Boolean)dataRow["BillingActive"];
			entity.ShippingActive = (System.Boolean)dataRow["ShippingActive"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.PortalCountry"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.PortalCountry Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.PortalCountry entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region CountryCodeSource	
			if (CanDeepLoad(entity, "Country|CountryCodeSource", deepLoadType, innerList) 
				&& entity.CountryCodeSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.CountryCode;
				Country tmpEntity = EntityManager.LocateEntity<Country>(EntityLocator.ConstructKeyFromPkItems(typeof(Country), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CountryCodeSource = tmpEntity;
				else
					entity.CountryCodeSource = DataRepository.CountryProvider.GetByCode(transactionManager, entity.CountryCode);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CountryCodeSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CountryCodeSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CountryProvider.DeepLoad(transactionManager, entity.CountryCodeSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CountryCodeSource

			#region PortalIDSource	
			if (CanDeepLoad(entity, "Portal|PortalIDSource", deepLoadType, innerList) 
				&& entity.PortalIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.PortalID;
				Portal tmpEntity = EntityManager.LocateEntity<Portal>(EntityLocator.ConstructKeyFromPkItems(typeof(Portal), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.PortalIDSource = tmpEntity;
				else
					entity.PortalIDSource = DataRepository.PortalProvider.GetByPortalID(transactionManager, entity.PortalID);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PortalIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.PortalIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.PortalProvider.DeepLoad(transactionManager, entity.PortalIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion PortalIDSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.PortalCountry object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.PortalCountry instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.PortalCountry Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.PortalCountry entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region CountryCodeSource
			if (CanDeepSave(entity, "Country|CountryCodeSource", deepSaveType, innerList) 
				&& entity.CountryCodeSource != null)
			{
				DataRepository.CountryProvider.Save(transactionManager, entity.CountryCodeSource);
				entity.CountryCode = entity.CountryCodeSource.Code;
			}
			#endregion 
			
			#region PortalIDSource
			if (CanDeepSave(entity, "Portal|PortalIDSource", deepSaveType, innerList) 
				&& entity.PortalIDSource != null)
			{
				DataRepository.PortalProvider.Save(transactionManager, entity.PortalIDSource);
				entity.PortalID = entity.PortalIDSource.PortalID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region PortalCountryChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.PortalCountry</c>
	///</summary>
	public enum PortalCountryChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Country</c> at CountryCodeSource
		///</summary>
		[ChildEntityType(typeof(Country))]
		Country,
		
		///<summary>
		/// Composite Property for <c>Portal</c> at PortalIDSource
		///</summary>
		[ChildEntityType(typeof(Portal))]
		Portal,
	}
	
	#endregion PortalCountryChildEntityTypes
	
	#region PortalCountryFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;PortalCountryColumn&gt;"/> class
	/// that is used exclusively with a <see cref="PortalCountry"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PortalCountryFilterBuilder : SqlFilterBuilder<PortalCountryColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PortalCountryFilterBuilder class.
		/// </summary>
		public PortalCountryFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the PortalCountryFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PortalCountryFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PortalCountryFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PortalCountryFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PortalCountryFilterBuilder
	
	#region PortalCountryParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;PortalCountryColumn&gt;"/> class
	/// that is used exclusively with a <see cref="PortalCountry"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PortalCountryParameterBuilder : ParameterizedSqlFilterBuilder<PortalCountryColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PortalCountryParameterBuilder class.
		/// </summary>
		public PortalCountryParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the PortalCountryParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PortalCountryParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PortalCountryParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PortalCountryParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PortalCountryParameterBuilder
	
	#region PortalCountrySortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;PortalCountryColumn&gt;"/> class
	/// that is used exclusively with a <see cref="PortalCountry"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class PortalCountrySortBuilder : SqlSortBuilder<PortalCountryColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PortalCountrySqlSortBuilder class.
		/// </summary>
		public PortalCountrySortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion PortalCountrySortBuilder
	
} // end namespace
