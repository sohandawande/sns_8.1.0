﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="PaymentSettingProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class PaymentSettingProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.PaymentSetting, ZNode.Libraries.DataAccess.Entities.PaymentSettingKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.PaymentSettingKey key)
		{
			return Delete(transactionManager, key.PaymentSettingID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_paymentSettingID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _paymentSettingID)
		{
			return Delete(null, _paymentSettingID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_paymentSettingID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _paymentSettingID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.PaymentSetting Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.PaymentSettingKey key, int start, int pageLength)
		{
			return GetByPaymentSettingID(transactionManager, key.PaymentSettingID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodePaymentSetting_ActiveInd index.
		/// </summary>
		/// <param name="_activeInd"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;PaymentSetting&gt;"/> class.</returns>
		public TList<PaymentSetting> GetByActiveInd(System.Boolean _activeInd)
		{
			int count = -1;
			return GetByActiveInd(null,_activeInd, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePaymentSetting_ActiveInd index.
		/// </summary>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;PaymentSetting&gt;"/> class.</returns>
		public TList<PaymentSetting> GetByActiveInd(System.Boolean _activeInd, int start, int pageLength)
		{
			int count = -1;
			return GetByActiveInd(null, _activeInd, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePaymentSetting_ActiveInd index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_activeInd"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;PaymentSetting&gt;"/> class.</returns>
		public TList<PaymentSetting> GetByActiveInd(TransactionManager transactionManager, System.Boolean _activeInd)
		{
			int count = -1;
			return GetByActiveInd(transactionManager, _activeInd, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePaymentSetting_ActiveInd index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;PaymentSetting&gt;"/> class.</returns>
		public TList<PaymentSetting> GetByActiveInd(TransactionManager transactionManager, System.Boolean _activeInd, int start, int pageLength)
		{
			int count = -1;
			return GetByActiveInd(transactionManager, _activeInd, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePaymentSetting_ActiveInd index.
		/// </summary>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;PaymentSetting&gt;"/> class.</returns>
		public TList<PaymentSetting> GetByActiveInd(System.Boolean _activeInd, int start, int pageLength, out int count)
		{
			return GetByActiveInd(null, _activeInd, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePaymentSetting_ActiveInd index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;PaymentSetting&gt;"/> class.</returns>
		public abstract TList<PaymentSetting> GetByActiveInd(TransactionManager transactionManager, System.Boolean _activeInd, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodePaymentSetting_GatewayTypeID index.
		/// </summary>
		/// <param name="_gatewayTypeID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;PaymentSetting&gt;"/> class.</returns>
		public TList<PaymentSetting> GetByGatewayTypeID(System.Int32? _gatewayTypeID)
		{
			int count = -1;
			return GetByGatewayTypeID(null,_gatewayTypeID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePaymentSetting_GatewayTypeID index.
		/// </summary>
		/// <param name="_gatewayTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;PaymentSetting&gt;"/> class.</returns>
		public TList<PaymentSetting> GetByGatewayTypeID(System.Int32? _gatewayTypeID, int start, int pageLength)
		{
			int count = -1;
			return GetByGatewayTypeID(null, _gatewayTypeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePaymentSetting_GatewayTypeID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_gatewayTypeID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;PaymentSetting&gt;"/> class.</returns>
		public TList<PaymentSetting> GetByGatewayTypeID(TransactionManager transactionManager, System.Int32? _gatewayTypeID)
		{
			int count = -1;
			return GetByGatewayTypeID(transactionManager, _gatewayTypeID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePaymentSetting_GatewayTypeID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_gatewayTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;PaymentSetting&gt;"/> class.</returns>
		public TList<PaymentSetting> GetByGatewayTypeID(TransactionManager transactionManager, System.Int32? _gatewayTypeID, int start, int pageLength)
		{
			int count = -1;
			return GetByGatewayTypeID(transactionManager, _gatewayTypeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePaymentSetting_GatewayTypeID index.
		/// </summary>
		/// <param name="_gatewayTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;PaymentSetting&gt;"/> class.</returns>
		public TList<PaymentSetting> GetByGatewayTypeID(System.Int32? _gatewayTypeID, int start, int pageLength, out int count)
		{
			return GetByGatewayTypeID(null, _gatewayTypeID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePaymentSetting_GatewayTypeID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_gatewayTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;PaymentSetting&gt;"/> class.</returns>
		public abstract TList<PaymentSetting> GetByGatewayTypeID(TransactionManager transactionManager, System.Int32? _gatewayTypeID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodePaymentSetting_PaymentTypeID index.
		/// </summary>
		/// <param name="_paymentTypeID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;PaymentSetting&gt;"/> class.</returns>
		public TList<PaymentSetting> GetByPaymentTypeID(System.Int32 _paymentTypeID)
		{
			int count = -1;
			return GetByPaymentTypeID(null,_paymentTypeID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePaymentSetting_PaymentTypeID index.
		/// </summary>
		/// <param name="_paymentTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;PaymentSetting&gt;"/> class.</returns>
		public TList<PaymentSetting> GetByPaymentTypeID(System.Int32 _paymentTypeID, int start, int pageLength)
		{
			int count = -1;
			return GetByPaymentTypeID(null, _paymentTypeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePaymentSetting_PaymentTypeID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_paymentTypeID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;PaymentSetting&gt;"/> class.</returns>
		public TList<PaymentSetting> GetByPaymentTypeID(TransactionManager transactionManager, System.Int32 _paymentTypeID)
		{
			int count = -1;
			return GetByPaymentTypeID(transactionManager, _paymentTypeID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePaymentSetting_PaymentTypeID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_paymentTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;PaymentSetting&gt;"/> class.</returns>
		public TList<PaymentSetting> GetByPaymentTypeID(TransactionManager transactionManager, System.Int32 _paymentTypeID, int start, int pageLength)
		{
			int count = -1;
			return GetByPaymentTypeID(transactionManager, _paymentTypeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePaymentSetting_PaymentTypeID index.
		/// </summary>
		/// <param name="_paymentTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;PaymentSetting&gt;"/> class.</returns>
		public TList<PaymentSetting> GetByPaymentTypeID(System.Int32 _paymentTypeID, int start, int pageLength, out int count)
		{
			return GetByPaymentTypeID(null, _paymentTypeID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePaymentSetting_PaymentTypeID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_paymentTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;PaymentSetting&gt;"/> class.</returns>
		public abstract TList<PaymentSetting> GetByPaymentTypeID(TransactionManager transactionManager, System.Int32 _paymentTypeID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodePaymentSetting_ProfileID index.
		/// </summary>
		/// <param name="_profileID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;PaymentSetting&gt;"/> class.</returns>
		public TList<PaymentSetting> GetByProfileID(System.Int32? _profileID)
		{
			int count = -1;
			return GetByProfileID(null,_profileID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePaymentSetting_ProfileID index.
		/// </summary>
		/// <param name="_profileID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;PaymentSetting&gt;"/> class.</returns>
		public TList<PaymentSetting> GetByProfileID(System.Int32? _profileID, int start, int pageLength)
		{
			int count = -1;
			return GetByProfileID(null, _profileID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePaymentSetting_ProfileID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_profileID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;PaymentSetting&gt;"/> class.</returns>
		public TList<PaymentSetting> GetByProfileID(TransactionManager transactionManager, System.Int32? _profileID)
		{
			int count = -1;
			return GetByProfileID(transactionManager, _profileID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePaymentSetting_ProfileID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_profileID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;PaymentSetting&gt;"/> class.</returns>
		public TList<PaymentSetting> GetByProfileID(TransactionManager transactionManager, System.Int32? _profileID, int start, int pageLength)
		{
			int count = -1;
			return GetByProfileID(transactionManager, _profileID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePaymentSetting_ProfileID index.
		/// </summary>
		/// <param name="_profileID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;PaymentSetting&gt;"/> class.</returns>
		public TList<PaymentSetting> GetByProfileID(System.Int32? _profileID, int start, int pageLength, out int count)
		{
			return GetByProfileID(null, _profileID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePaymentSetting_ProfileID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_profileID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;PaymentSetting&gt;"/> class.</returns>
		public abstract TList<PaymentSetting> GetByProfileID(TransactionManager transactionManager, System.Int32? _profileID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX1 index.
		/// </summary>
		/// <param name="_profileID"></param>
		/// <param name="_paymentTypeID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;PaymentSetting&gt;"/> class.</returns>
		public TList<PaymentSetting> GetByProfileIDPaymentTypeID(System.Int32? _profileID, System.Int32 _paymentTypeID)
		{
			int count = -1;
			return GetByProfileIDPaymentTypeID(null,_profileID, _paymentTypeID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX1 index.
		/// </summary>
		/// <param name="_profileID"></param>
		/// <param name="_paymentTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;PaymentSetting&gt;"/> class.</returns>
		public TList<PaymentSetting> GetByProfileIDPaymentTypeID(System.Int32? _profileID, System.Int32 _paymentTypeID, int start, int pageLength)
		{
			int count = -1;
			return GetByProfileIDPaymentTypeID(null, _profileID, _paymentTypeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX1 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_profileID"></param>
		/// <param name="_paymentTypeID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;PaymentSetting&gt;"/> class.</returns>
		public TList<PaymentSetting> GetByProfileIDPaymentTypeID(TransactionManager transactionManager, System.Int32? _profileID, System.Int32 _paymentTypeID)
		{
			int count = -1;
			return GetByProfileIDPaymentTypeID(transactionManager, _profileID, _paymentTypeID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX1 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_profileID"></param>
		/// <param name="_paymentTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;PaymentSetting&gt;"/> class.</returns>
		public TList<PaymentSetting> GetByProfileIDPaymentTypeID(TransactionManager transactionManager, System.Int32? _profileID, System.Int32 _paymentTypeID, int start, int pageLength)
		{
			int count = -1;
			return GetByProfileIDPaymentTypeID(transactionManager, _profileID, _paymentTypeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX1 index.
		/// </summary>
		/// <param name="_profileID"></param>
		/// <param name="_paymentTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;PaymentSetting&gt;"/> class.</returns>
		public TList<PaymentSetting> GetByProfileIDPaymentTypeID(System.Int32? _profileID, System.Int32 _paymentTypeID, int start, int pageLength, out int count)
		{
			return GetByProfileIDPaymentTypeID(null, _profileID, _paymentTypeID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX1 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_profileID"></param>
		/// <param name="_paymentTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;PaymentSetting&gt;"/> class.</returns>
		public abstract TList<PaymentSetting> GetByProfileIDPaymentTypeID(TransactionManager transactionManager, System.Int32? _profileID, System.Int32 _paymentTypeID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_SC_PaymentSetting index.
		/// </summary>
		/// <param name="_paymentSettingID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.PaymentSetting"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.PaymentSetting GetByPaymentSettingID(System.Int32 _paymentSettingID)
		{
			int count = -1;
			return GetByPaymentSettingID(null,_paymentSettingID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SC_PaymentSetting index.
		/// </summary>
		/// <param name="_paymentSettingID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.PaymentSetting"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.PaymentSetting GetByPaymentSettingID(System.Int32 _paymentSettingID, int start, int pageLength)
		{
			int count = -1;
			return GetByPaymentSettingID(null, _paymentSettingID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SC_PaymentSetting index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_paymentSettingID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.PaymentSetting"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.PaymentSetting GetByPaymentSettingID(TransactionManager transactionManager, System.Int32 _paymentSettingID)
		{
			int count = -1;
			return GetByPaymentSettingID(transactionManager, _paymentSettingID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SC_PaymentSetting index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_paymentSettingID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.PaymentSetting"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.PaymentSetting GetByPaymentSettingID(TransactionManager transactionManager, System.Int32 _paymentSettingID, int start, int pageLength)
		{
			int count = -1;
			return GetByPaymentSettingID(transactionManager, _paymentSettingID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SC_PaymentSetting index.
		/// </summary>
		/// <param name="_paymentSettingID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.PaymentSetting"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.PaymentSetting GetByPaymentSettingID(System.Int32 _paymentSettingID, int start, int pageLength, out int count)
		{
			return GetByPaymentSettingID(null, _paymentSettingID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SC_PaymentSetting index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_paymentSettingID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.PaymentSetting"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.PaymentSetting GetByPaymentSettingID(TransactionManager transactionManager, System.Int32 _paymentSettingID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;PaymentSetting&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;PaymentSetting&gt;"/></returns>
		public static TList<PaymentSetting> Fill(IDataReader reader, TList<PaymentSetting> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.PaymentSetting c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("PaymentSetting")
					.Append("|").Append((System.Int32)reader[((int)PaymentSettingColumn.PaymentSettingID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<PaymentSetting>(
					key.ToString(), // EntityTrackingKey
					"PaymentSetting",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.PaymentSetting();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.PaymentSettingID = (System.Int32)reader[((int)PaymentSettingColumn.PaymentSettingID - 1)];
					c.PaymentTypeID = (System.Int32)reader[((int)PaymentSettingColumn.PaymentTypeID - 1)];
					c.ProfileID = (reader.IsDBNull(((int)PaymentSettingColumn.ProfileID - 1)))?null:(System.Int32?)reader[((int)PaymentSettingColumn.ProfileID - 1)];
					c.GatewayTypeID = (reader.IsDBNull(((int)PaymentSettingColumn.GatewayTypeID - 1)))?null:(System.Int32?)reader[((int)PaymentSettingColumn.GatewayTypeID - 1)];
					c.GatewayUsername = (reader.IsDBNull(((int)PaymentSettingColumn.GatewayUsername - 1)))?null:(System.String)reader[((int)PaymentSettingColumn.GatewayUsername - 1)];
					c.GatewayPassword = (reader.IsDBNull(((int)PaymentSettingColumn.GatewayPassword - 1)))?null:(System.String)reader[((int)PaymentSettingColumn.GatewayPassword - 1)];
					c.EnableVisa = (reader.IsDBNull(((int)PaymentSettingColumn.EnableVisa - 1)))?null:(System.Boolean?)reader[((int)PaymentSettingColumn.EnableVisa - 1)];
					c.EnableMasterCard = (reader.IsDBNull(((int)PaymentSettingColumn.EnableMasterCard - 1)))?null:(System.Boolean?)reader[((int)PaymentSettingColumn.EnableMasterCard - 1)];
					c.EnableAmex = (reader.IsDBNull(((int)PaymentSettingColumn.EnableAmex - 1)))?null:(System.Boolean?)reader[((int)PaymentSettingColumn.EnableAmex - 1)];
					c.EnableDiscover = (reader.IsDBNull(((int)PaymentSettingColumn.EnableDiscover - 1)))?null:(System.Boolean?)reader[((int)PaymentSettingColumn.EnableDiscover - 1)];
					c.EnableRecurringPayments = (reader.IsDBNull(((int)PaymentSettingColumn.EnableRecurringPayments - 1)))?null:(System.Boolean?)reader[((int)PaymentSettingColumn.EnableRecurringPayments - 1)];
					c.EnableVault = (reader.IsDBNull(((int)PaymentSettingColumn.EnableVault - 1)))?null:(System.Boolean?)reader[((int)PaymentSettingColumn.EnableVault - 1)];
					c.TransactionKey = (reader.IsDBNull(((int)PaymentSettingColumn.TransactionKey - 1)))?null:(System.String)reader[((int)PaymentSettingColumn.TransactionKey - 1)];
					c.ActiveInd = (System.Boolean)reader[((int)PaymentSettingColumn.ActiveInd - 1)];
					c.DisplayOrder = (System.Int32)reader[((int)PaymentSettingColumn.DisplayOrder - 1)];
					c.TestMode = (System.Boolean)reader[((int)PaymentSettingColumn.TestMode - 1)];
					c.Partner = (reader.IsDBNull(((int)PaymentSettingColumn.Partner - 1)))?null:(System.String)reader[((int)PaymentSettingColumn.Partner - 1)];
					c.Vendor = (reader.IsDBNull(((int)PaymentSettingColumn.Vendor - 1)))?null:(System.String)reader[((int)PaymentSettingColumn.Vendor - 1)];
					c.PreAuthorize = (System.Boolean)reader[((int)PaymentSettingColumn.PreAuthorize - 1)];
					c.IsRMACompatible = (reader.IsDBNull(((int)PaymentSettingColumn.IsRMACompatible - 1)))?null:(System.Boolean?)reader[((int)PaymentSettingColumn.IsRMACompatible - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.PaymentSetting"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.PaymentSetting"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.PaymentSetting entity)
		{
			if (!reader.Read()) return;
			
			entity.PaymentSettingID = (System.Int32)reader[((int)PaymentSettingColumn.PaymentSettingID - 1)];
			entity.PaymentTypeID = (System.Int32)reader[((int)PaymentSettingColumn.PaymentTypeID - 1)];
			entity.ProfileID = (reader.IsDBNull(((int)PaymentSettingColumn.ProfileID - 1)))?null:(System.Int32?)reader[((int)PaymentSettingColumn.ProfileID - 1)];
			entity.GatewayTypeID = (reader.IsDBNull(((int)PaymentSettingColumn.GatewayTypeID - 1)))?null:(System.Int32?)reader[((int)PaymentSettingColumn.GatewayTypeID - 1)];
			entity.GatewayUsername = (reader.IsDBNull(((int)PaymentSettingColumn.GatewayUsername - 1)))?null:(System.String)reader[((int)PaymentSettingColumn.GatewayUsername - 1)];
			entity.GatewayPassword = (reader.IsDBNull(((int)PaymentSettingColumn.GatewayPassword - 1)))?null:(System.String)reader[((int)PaymentSettingColumn.GatewayPassword - 1)];
			entity.EnableVisa = (reader.IsDBNull(((int)PaymentSettingColumn.EnableVisa - 1)))?null:(System.Boolean?)reader[((int)PaymentSettingColumn.EnableVisa - 1)];
			entity.EnableMasterCard = (reader.IsDBNull(((int)PaymentSettingColumn.EnableMasterCard - 1)))?null:(System.Boolean?)reader[((int)PaymentSettingColumn.EnableMasterCard - 1)];
			entity.EnableAmex = (reader.IsDBNull(((int)PaymentSettingColumn.EnableAmex - 1)))?null:(System.Boolean?)reader[((int)PaymentSettingColumn.EnableAmex - 1)];
			entity.EnableDiscover = (reader.IsDBNull(((int)PaymentSettingColumn.EnableDiscover - 1)))?null:(System.Boolean?)reader[((int)PaymentSettingColumn.EnableDiscover - 1)];
			entity.EnableRecurringPayments = (reader.IsDBNull(((int)PaymentSettingColumn.EnableRecurringPayments - 1)))?null:(System.Boolean?)reader[((int)PaymentSettingColumn.EnableRecurringPayments - 1)];
			entity.EnableVault = (reader.IsDBNull(((int)PaymentSettingColumn.EnableVault - 1)))?null:(System.Boolean?)reader[((int)PaymentSettingColumn.EnableVault - 1)];
			entity.TransactionKey = (reader.IsDBNull(((int)PaymentSettingColumn.TransactionKey - 1)))?null:(System.String)reader[((int)PaymentSettingColumn.TransactionKey - 1)];
			entity.ActiveInd = (System.Boolean)reader[((int)PaymentSettingColumn.ActiveInd - 1)];
			entity.DisplayOrder = (System.Int32)reader[((int)PaymentSettingColumn.DisplayOrder - 1)];
			entity.TestMode = (System.Boolean)reader[((int)PaymentSettingColumn.TestMode - 1)];
			entity.Partner = (reader.IsDBNull(((int)PaymentSettingColumn.Partner - 1)))?null:(System.String)reader[((int)PaymentSettingColumn.Partner - 1)];
			entity.Vendor = (reader.IsDBNull(((int)PaymentSettingColumn.Vendor - 1)))?null:(System.String)reader[((int)PaymentSettingColumn.Vendor - 1)];
			entity.PreAuthorize = (System.Boolean)reader[((int)PaymentSettingColumn.PreAuthorize - 1)];
			entity.IsRMACompatible = (reader.IsDBNull(((int)PaymentSettingColumn.IsRMACompatible - 1)))?null:(System.Boolean?)reader[((int)PaymentSettingColumn.IsRMACompatible - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.PaymentSetting"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.PaymentSetting"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.PaymentSetting entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.PaymentSettingID = (System.Int32)dataRow["PaymentSettingID"];
			entity.PaymentTypeID = (System.Int32)dataRow["PaymentTypeID"];
			entity.ProfileID = Convert.IsDBNull(dataRow["ProfileID"]) ? null : (System.Int32?)dataRow["ProfileID"];
			entity.GatewayTypeID = Convert.IsDBNull(dataRow["GatewayTypeID"]) ? null : (System.Int32?)dataRow["GatewayTypeID"];
			entity.GatewayUsername = Convert.IsDBNull(dataRow["GatewayUsername"]) ? null : (System.String)dataRow["GatewayUsername"];
			entity.GatewayPassword = Convert.IsDBNull(dataRow["GatewayPassword"]) ? null : (System.String)dataRow["GatewayPassword"];
			entity.EnableVisa = Convert.IsDBNull(dataRow["EnableVisa"]) ? null : (System.Boolean?)dataRow["EnableVisa"];
			entity.EnableMasterCard = Convert.IsDBNull(dataRow["EnableMasterCard"]) ? null : (System.Boolean?)dataRow["EnableMasterCard"];
			entity.EnableAmex = Convert.IsDBNull(dataRow["EnableAmex"]) ? null : (System.Boolean?)dataRow["EnableAmex"];
			entity.EnableDiscover = Convert.IsDBNull(dataRow["EnableDiscover"]) ? null : (System.Boolean?)dataRow["EnableDiscover"];
			entity.EnableRecurringPayments = Convert.IsDBNull(dataRow["EnableRecurringPayments"]) ? null : (System.Boolean?)dataRow["EnableRecurringPayments"];
			entity.EnableVault = Convert.IsDBNull(dataRow["EnableVault"]) ? null : (System.Boolean?)dataRow["EnableVault"];
			entity.TransactionKey = Convert.IsDBNull(dataRow["TransactionKey"]) ? null : (System.String)dataRow["TransactionKey"];
			entity.ActiveInd = (System.Boolean)dataRow["ActiveInd"];
			entity.DisplayOrder = (System.Int32)dataRow["DisplayOrder"];
			entity.TestMode = (System.Boolean)dataRow["TestMode"];
			entity.Partner = Convert.IsDBNull(dataRow["Partner"]) ? null : (System.String)dataRow["Partner"];
			entity.Vendor = Convert.IsDBNull(dataRow["Vendor"]) ? null : (System.String)dataRow["Vendor"];
			entity.PreAuthorize = (System.Boolean)dataRow["PreAuthorize"];
			entity.IsRMACompatible = Convert.IsDBNull(dataRow["IsRMACompatible"]) ? null : (System.Boolean?)dataRow["IsRMACompatible"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.PaymentSetting"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.PaymentSetting Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.PaymentSetting entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region GatewayTypeIDSource	
			if (CanDeepLoad(entity, "Gateway|GatewayTypeIDSource", deepLoadType, innerList) 
				&& entity.GatewayTypeIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.GatewayTypeID ?? (int)0);
				Gateway tmpEntity = EntityManager.LocateEntity<Gateway>(EntityLocator.ConstructKeyFromPkItems(typeof(Gateway), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.GatewayTypeIDSource = tmpEntity;
				else
					entity.GatewayTypeIDSource = DataRepository.GatewayProvider.GetByGatewayTypeID(transactionManager, (entity.GatewayTypeID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'GatewayTypeIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.GatewayTypeIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.GatewayProvider.DeepLoad(transactionManager, entity.GatewayTypeIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion GatewayTypeIDSource

			#region ProfileIDSource	
			if (CanDeepLoad(entity, "Profile|ProfileIDSource", deepLoadType, innerList) 
				&& entity.ProfileIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.ProfileID ?? (int)0);
				Profile tmpEntity = EntityManager.LocateEntity<Profile>(EntityLocator.ConstructKeyFromPkItems(typeof(Profile), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ProfileIDSource = tmpEntity;
				else
					entity.ProfileIDSource = DataRepository.ProfileProvider.GetByProfileID(transactionManager, (entity.ProfileID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ProfileIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ProfileIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.ProfileProvider.DeepLoad(transactionManager, entity.ProfileIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ProfileIDSource

			#region PaymentSettingIDSource	
			if (CanDeepLoad(entity, "PaymentSetting|PaymentSettingIDSource", deepLoadType, innerList) 
				&& entity.PaymentSettingIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.PaymentSettingID;
				PaymentSetting tmpEntity = EntityManager.LocateEntity<PaymentSetting>(EntityLocator.ConstructKeyFromPkItems(typeof(PaymentSetting), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.PaymentSettingIDSource = tmpEntity;
				else
					entity.PaymentSettingIDSource = DataRepository.PaymentSettingProvider.GetByPaymentSettingID(transactionManager, entity.PaymentSettingID);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PaymentSettingIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.PaymentSettingIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.PaymentSettingProvider.DeepLoad(transactionManager, entity.PaymentSettingIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion PaymentSettingIDSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByPaymentSettingID methods when available
			
			#region OrderCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Order>|OrderCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'OrderCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.OrderCollection = DataRepository.OrderProvider.GetByPaymentSettingID(transactionManager, entity.PaymentSettingID);

				if (deep && entity.OrderCollection.Count > 0)
				{
					deepHandles.Add("OrderCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Order>) DataRepository.OrderProvider.DeepLoad,
						new object[] { transactionManager, entity.OrderCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region PaymentTokenCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<PaymentToken>|PaymentTokenCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PaymentTokenCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.PaymentTokenCollection = DataRepository.PaymentTokenProvider.GetByPaymentSettingID(transactionManager, entity.PaymentSettingID);

				if (deep && entity.PaymentTokenCollection.Count > 0)
				{
					deepHandles.Add("PaymentTokenCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<PaymentToken>) DataRepository.PaymentTokenProvider.DeepLoad,
						new object[] { transactionManager, entity.PaymentTokenCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region PaymentSetting
			// RelationshipType.OneToOne
			if (CanDeepLoad(entity, "PaymentSetting|PaymentSetting", deepLoadType, innerList))
			{
				entity.PaymentSetting = DataRepository.PaymentSettingProvider.GetByPaymentSettingID(transactionManager, entity.PaymentSettingID);
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PaymentSetting' loaded. key " + entity.EntityTrackingKey);
				#endif 

				if (deep && entity.PaymentSetting != null)
				{
					deepHandles.Add("PaymentSetting",
						new KeyValuePair<Delegate, object>((DeepLoadSingleHandle< PaymentSetting >) DataRepository.PaymentSettingProvider.DeepLoad,
						new object[] { transactionManager, entity.PaymentSetting, deep, deepLoadType, childTypes, innerList }
					));
				}
			}
			#endregion 
			
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.PaymentSetting object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.PaymentSetting instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.PaymentSetting Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.PaymentSetting entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region GatewayTypeIDSource
			if (CanDeepSave(entity, "Gateway|GatewayTypeIDSource", deepSaveType, innerList) 
				&& entity.GatewayTypeIDSource != null)
			{
				DataRepository.GatewayProvider.Save(transactionManager, entity.GatewayTypeIDSource);
				entity.GatewayTypeID = entity.GatewayTypeIDSource.GatewayTypeID;
			}
			#endregion 
			
			#region ProfileIDSource
			if (CanDeepSave(entity, "Profile|ProfileIDSource", deepSaveType, innerList) 
				&& entity.ProfileIDSource != null)
			{
				DataRepository.ProfileProvider.Save(transactionManager, entity.ProfileIDSource);
				entity.ProfileID = entity.ProfileIDSource.ProfileID;
			}
			#endregion 
			
			#region PaymentSettingIDSource
			if (CanDeepSave(entity, "PaymentSetting|PaymentSettingIDSource", deepSaveType, innerList) 
				&& entity.PaymentSettingIDSource != null)
			{
				DataRepository.PaymentSettingProvider.Save(transactionManager, entity.PaymentSettingIDSource);
				entity.PaymentSettingID = entity.PaymentSettingIDSource.PaymentSettingID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();

			#region PaymentSetting
			if (CanDeepSave(entity.PaymentSetting, "PaymentSetting|PaymentSetting", deepSaveType, innerList))
			{

				if (entity.PaymentSetting != null)
				{
					// update each child parent id with the real parent id (mostly used on insert)

					entity.PaymentSetting.PaymentSettingID = entity.PaymentSettingID;
					//DataRepository.PaymentSettingProvider.Save(transactionManager, entity.PaymentSetting);
					deepHandles.Add("PaymentSetting",
						new KeyValuePair<Delegate, object>((DeepSaveSingleHandle< PaymentSetting >) DataRepository.PaymentSettingProvider.DeepSave,
						new object[] { transactionManager, entity.PaymentSetting, deepSaveType, childTypes, innerList }
					));
				}
			} 
			#endregion 
	
			#region List<Order>
				if (CanDeepSave(entity.OrderCollection, "List<Order>|OrderCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Order child in entity.OrderCollection)
					{
						if(child.PaymentSettingIDSource != null)
						{
							child.PaymentSettingID = child.PaymentSettingIDSource.PaymentSettingID;
						}
						else
						{
							child.PaymentSettingID = entity.PaymentSettingID;
						}

					}

					if (entity.OrderCollection.Count > 0 || entity.OrderCollection.DeletedItems.Count > 0)
					{
						//DataRepository.OrderProvider.Save(transactionManager, entity.OrderCollection);
						
						deepHandles.Add("OrderCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Order >) DataRepository.OrderProvider.DeepSave,
							new object[] { transactionManager, entity.OrderCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<PaymentToken>
				if (CanDeepSave(entity.PaymentTokenCollection, "List<PaymentToken>|PaymentTokenCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(PaymentToken child in entity.PaymentTokenCollection)
					{
						if(child.PaymentSettingIDSource != null)
						{
							child.PaymentSettingID = child.PaymentSettingIDSource.PaymentSettingID;
						}
						else
						{
							child.PaymentSettingID = entity.PaymentSettingID;
						}

					}

					if (entity.PaymentTokenCollection.Count > 0 || entity.PaymentTokenCollection.DeletedItems.Count > 0)
					{
						//DataRepository.PaymentTokenProvider.Save(transactionManager, entity.PaymentTokenCollection);
						
						deepHandles.Add("PaymentTokenCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< PaymentToken >) DataRepository.PaymentTokenProvider.DeepSave,
							new object[] { transactionManager, entity.PaymentTokenCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region PaymentSettingChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.PaymentSetting</c>
	///</summary>
	public enum PaymentSettingChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Gateway</c> at GatewayTypeIDSource
		///</summary>
		[ChildEntityType(typeof(Gateway))]
		Gateway,
		
		///<summary>
		/// Composite Property for <c>Profile</c> at ProfileIDSource
		///</summary>
		[ChildEntityType(typeof(Profile))]
		Profile,
		
		///<summary>
		/// Composite Property for <c>PaymentSetting</c> at PaymentSettingIDSource
		///</summary>
		[ChildEntityType(typeof(PaymentSetting))]
		PaymentSetting,
		///<summary>
		/// Collection of <c>PaymentSetting</c> as OneToMany for OrderCollection
		///</summary>
		[ChildEntityType(typeof(TList<Order>))]
		OrderCollection,
		///<summary>
		/// Collection of <c>PaymentSetting</c> as OneToMany for PaymentTokenCollection
		///</summary>
		[ChildEntityType(typeof(TList<PaymentToken>))]
		PaymentTokenCollection,
	}
	
	#endregion PaymentSettingChildEntityTypes
	
	#region PaymentSettingFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;PaymentSettingColumn&gt;"/> class
	/// that is used exclusively with a <see cref="PaymentSetting"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PaymentSettingFilterBuilder : SqlFilterBuilder<PaymentSettingColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PaymentSettingFilterBuilder class.
		/// </summary>
		public PaymentSettingFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the PaymentSettingFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PaymentSettingFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PaymentSettingFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PaymentSettingFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PaymentSettingFilterBuilder
	
	#region PaymentSettingParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;PaymentSettingColumn&gt;"/> class
	/// that is used exclusively with a <see cref="PaymentSetting"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PaymentSettingParameterBuilder : ParameterizedSqlFilterBuilder<PaymentSettingColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PaymentSettingParameterBuilder class.
		/// </summary>
		public PaymentSettingParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the PaymentSettingParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PaymentSettingParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PaymentSettingParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PaymentSettingParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PaymentSettingParameterBuilder
	
	#region PaymentSettingSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;PaymentSettingColumn&gt;"/> class
	/// that is used exclusively with a <see cref="PaymentSetting"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class PaymentSettingSortBuilder : SqlSortBuilder<PaymentSettingColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PaymentSettingSqlSortBuilder class.
		/// </summary>
		public PaymentSettingSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion PaymentSettingSortBuilder
	
} // end namespace
