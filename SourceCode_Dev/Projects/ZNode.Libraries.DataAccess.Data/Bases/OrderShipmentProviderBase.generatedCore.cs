﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="OrderShipmentProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class OrderShipmentProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.OrderShipment, ZNode.Libraries.DataAccess.Entities.OrderShipmentKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.OrderShipmentKey key)
		{
			return Delete(transactionManager, key.OrderShipmentID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_orderShipmentID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _orderShipmentID)
		{
			return Delete(null, _orderShipmentID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_orderShipmentID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _orderShipmentID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeOrderShipment_ZNodeShipping key.
		///		FK_ZNodeOrderShipment_ZNodeShipping Description: 
		/// </summary>
		/// <param name="_shippingID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.OrderShipment objects.</returns>
		public TList<OrderShipment> GetByShippingID(System.Int32? _shippingID)
		{
			int count = -1;
			return GetByShippingID(_shippingID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeOrderShipment_ZNodeShipping key.
		///		FK_ZNodeOrderShipment_ZNodeShipping Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_shippingID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.OrderShipment objects.</returns>
		/// <remarks></remarks>
		public TList<OrderShipment> GetByShippingID(TransactionManager transactionManager, System.Int32? _shippingID)
		{
			int count = -1;
			return GetByShippingID(transactionManager, _shippingID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeOrderShipment_ZNodeShipping key.
		///		FK_ZNodeOrderShipment_ZNodeShipping Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_shippingID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.OrderShipment objects.</returns>
		public TList<OrderShipment> GetByShippingID(TransactionManager transactionManager, System.Int32? _shippingID, int start, int pageLength)
		{
			int count = -1;
			return GetByShippingID(transactionManager, _shippingID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeOrderShipment_ZNodeShipping key.
		///		fKZNodeOrderShipmentZNodeShipping Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_shippingID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.OrderShipment objects.</returns>
		public TList<OrderShipment> GetByShippingID(System.Int32? _shippingID, int start, int pageLength)
		{
			int count =  -1;
			return GetByShippingID(null, _shippingID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeOrderShipment_ZNodeShipping key.
		///		fKZNodeOrderShipmentZNodeShipping Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_shippingID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.OrderShipment objects.</returns>
		public TList<OrderShipment> GetByShippingID(System.Int32? _shippingID, int start, int pageLength,out int count)
		{
			return GetByShippingID(null, _shippingID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeOrderShipment_ZNodeShipping key.
		///		FK_ZNodeOrderShipment_ZNodeShipping Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_shippingID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.OrderShipment objects.</returns>
		public abstract TList<OrderShipment> GetByShippingID(TransactionManager transactionManager, System.Int32? _shippingID, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.OrderShipment Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.OrderShipmentKey key, int start, int pageLength)
		{
			return GetByOrderShipmentID(transactionManager, key.OrderShipmentID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodeOrderShipment index.
		/// </summary>
		/// <param name="_orderShipmentID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.OrderShipment"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.OrderShipment GetByOrderShipmentID(System.Int32 _orderShipmentID)
		{
			int count = -1;
			return GetByOrderShipmentID(null,_orderShipmentID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeOrderShipment index.
		/// </summary>
		/// <param name="_orderShipmentID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.OrderShipment"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.OrderShipment GetByOrderShipmentID(System.Int32 _orderShipmentID, int start, int pageLength)
		{
			int count = -1;
			return GetByOrderShipmentID(null, _orderShipmentID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeOrderShipment index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_orderShipmentID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.OrderShipment"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.OrderShipment GetByOrderShipmentID(TransactionManager transactionManager, System.Int32 _orderShipmentID)
		{
			int count = -1;
			return GetByOrderShipmentID(transactionManager, _orderShipmentID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeOrderShipment index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_orderShipmentID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.OrderShipment"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.OrderShipment GetByOrderShipmentID(TransactionManager transactionManager, System.Int32 _orderShipmentID, int start, int pageLength)
		{
			int count = -1;
			return GetByOrderShipmentID(transactionManager, _orderShipmentID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeOrderShipment index.
		/// </summary>
		/// <param name="_orderShipmentID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.OrderShipment"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.OrderShipment GetByOrderShipmentID(System.Int32 _orderShipmentID, int start, int pageLength, out int count)
		{
			return GetByOrderShipmentID(null, _orderShipmentID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeOrderShipment index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_orderShipmentID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.OrderShipment"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.OrderShipment GetByOrderShipmentID(TransactionManager transactionManager, System.Int32 _orderShipmentID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;OrderShipment&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;OrderShipment&gt;"/></returns>
		public static TList<OrderShipment> Fill(IDataReader reader, TList<OrderShipment> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.OrderShipment c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("OrderShipment")
					.Append("|").Append((System.Int32)reader[((int)OrderShipmentColumn.OrderShipmentID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<OrderShipment>(
					key.ToString(), // EntityTrackingKey
					"OrderShipment",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.OrderShipment();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.OrderShipmentID = (System.Int32)reader[((int)OrderShipmentColumn.OrderShipmentID - 1)];
					c.ShipName = (reader.IsDBNull(((int)OrderShipmentColumn.ShipName - 1)))?null:(System.String)reader[((int)OrderShipmentColumn.ShipName - 1)];
					c.ShipToFirstName = (reader.IsDBNull(((int)OrderShipmentColumn.ShipToFirstName - 1)))?null:(System.String)reader[((int)OrderShipmentColumn.ShipToFirstName - 1)];
					c.ShipToLastName = (reader.IsDBNull(((int)OrderShipmentColumn.ShipToLastName - 1)))?null:(System.String)reader[((int)OrderShipmentColumn.ShipToLastName - 1)];
					c.ShipToCompanyName = (reader.IsDBNull(((int)OrderShipmentColumn.ShipToCompanyName - 1)))?null:(System.String)reader[((int)OrderShipmentColumn.ShipToCompanyName - 1)];
					c.ShipToStreet = (reader.IsDBNull(((int)OrderShipmentColumn.ShipToStreet - 1)))?null:(System.String)reader[((int)OrderShipmentColumn.ShipToStreet - 1)];
					c.ShipToStreet1 = (reader.IsDBNull(((int)OrderShipmentColumn.ShipToStreet1 - 1)))?null:(System.String)reader[((int)OrderShipmentColumn.ShipToStreet1 - 1)];
					c.ShipToCity = (reader.IsDBNull(((int)OrderShipmentColumn.ShipToCity - 1)))?null:(System.String)reader[((int)OrderShipmentColumn.ShipToCity - 1)];
					c.ShipToStateCode = (reader.IsDBNull(((int)OrderShipmentColumn.ShipToStateCode - 1)))?null:(System.String)reader[((int)OrderShipmentColumn.ShipToStateCode - 1)];
					c.ShipToPostalCode = (reader.IsDBNull(((int)OrderShipmentColumn.ShipToPostalCode - 1)))?null:(System.String)reader[((int)OrderShipmentColumn.ShipToPostalCode - 1)];
					c.ShipToCountry = (reader.IsDBNull(((int)OrderShipmentColumn.ShipToCountry - 1)))?null:(System.String)reader[((int)OrderShipmentColumn.ShipToCountry - 1)];
					c.ShipToPhoneNumber = (reader.IsDBNull(((int)OrderShipmentColumn.ShipToPhoneNumber - 1)))?null:(System.String)reader[((int)OrderShipmentColumn.ShipToPhoneNumber - 1)];
					c.ShipToEmailID = (reader.IsDBNull(((int)OrderShipmentColumn.ShipToEmailID - 1)))?null:(System.String)reader[((int)OrderShipmentColumn.ShipToEmailID - 1)];
					c.ShippingID = (reader.IsDBNull(((int)OrderShipmentColumn.ShippingID - 1)))?null:(System.Int32?)reader[((int)OrderShipmentColumn.ShippingID - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.OrderShipment"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.OrderShipment"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.OrderShipment entity)
		{
			if (!reader.Read()) return;
			
			entity.OrderShipmentID = (System.Int32)reader[((int)OrderShipmentColumn.OrderShipmentID - 1)];
			entity.ShipName = (reader.IsDBNull(((int)OrderShipmentColumn.ShipName - 1)))?null:(System.String)reader[((int)OrderShipmentColumn.ShipName - 1)];
			entity.ShipToFirstName = (reader.IsDBNull(((int)OrderShipmentColumn.ShipToFirstName - 1)))?null:(System.String)reader[((int)OrderShipmentColumn.ShipToFirstName - 1)];
			entity.ShipToLastName = (reader.IsDBNull(((int)OrderShipmentColumn.ShipToLastName - 1)))?null:(System.String)reader[((int)OrderShipmentColumn.ShipToLastName - 1)];
			entity.ShipToCompanyName = (reader.IsDBNull(((int)OrderShipmentColumn.ShipToCompanyName - 1)))?null:(System.String)reader[((int)OrderShipmentColumn.ShipToCompanyName - 1)];
			entity.ShipToStreet = (reader.IsDBNull(((int)OrderShipmentColumn.ShipToStreet - 1)))?null:(System.String)reader[((int)OrderShipmentColumn.ShipToStreet - 1)];
			entity.ShipToStreet1 = (reader.IsDBNull(((int)OrderShipmentColumn.ShipToStreet1 - 1)))?null:(System.String)reader[((int)OrderShipmentColumn.ShipToStreet1 - 1)];
			entity.ShipToCity = (reader.IsDBNull(((int)OrderShipmentColumn.ShipToCity - 1)))?null:(System.String)reader[((int)OrderShipmentColumn.ShipToCity - 1)];
			entity.ShipToStateCode = (reader.IsDBNull(((int)OrderShipmentColumn.ShipToStateCode - 1)))?null:(System.String)reader[((int)OrderShipmentColumn.ShipToStateCode - 1)];
			entity.ShipToPostalCode = (reader.IsDBNull(((int)OrderShipmentColumn.ShipToPostalCode - 1)))?null:(System.String)reader[((int)OrderShipmentColumn.ShipToPostalCode - 1)];
			entity.ShipToCountry = (reader.IsDBNull(((int)OrderShipmentColumn.ShipToCountry - 1)))?null:(System.String)reader[((int)OrderShipmentColumn.ShipToCountry - 1)];
			entity.ShipToPhoneNumber = (reader.IsDBNull(((int)OrderShipmentColumn.ShipToPhoneNumber - 1)))?null:(System.String)reader[((int)OrderShipmentColumn.ShipToPhoneNumber - 1)];
			entity.ShipToEmailID = (reader.IsDBNull(((int)OrderShipmentColumn.ShipToEmailID - 1)))?null:(System.String)reader[((int)OrderShipmentColumn.ShipToEmailID - 1)];
			entity.ShippingID = (reader.IsDBNull(((int)OrderShipmentColumn.ShippingID - 1)))?null:(System.Int32?)reader[((int)OrderShipmentColumn.ShippingID - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.OrderShipment"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.OrderShipment"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.OrderShipment entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.OrderShipmentID = (System.Int32)dataRow["OrderShipmentID"];
			entity.ShipName = Convert.IsDBNull(dataRow["ShipName"]) ? null : (System.String)dataRow["ShipName"];
			entity.ShipToFirstName = Convert.IsDBNull(dataRow["ShipToFirstName"]) ? null : (System.String)dataRow["ShipToFirstName"];
			entity.ShipToLastName = Convert.IsDBNull(dataRow["ShipToLastName"]) ? null : (System.String)dataRow["ShipToLastName"];
			entity.ShipToCompanyName = Convert.IsDBNull(dataRow["ShipToCompanyName"]) ? null : (System.String)dataRow["ShipToCompanyName"];
			entity.ShipToStreet = Convert.IsDBNull(dataRow["ShipToStreet"]) ? null : (System.String)dataRow["ShipToStreet"];
			entity.ShipToStreet1 = Convert.IsDBNull(dataRow["ShipToStreet1"]) ? null : (System.String)dataRow["ShipToStreet1"];
			entity.ShipToCity = Convert.IsDBNull(dataRow["ShipToCity"]) ? null : (System.String)dataRow["ShipToCity"];
			entity.ShipToStateCode = Convert.IsDBNull(dataRow["ShipToStateCode"]) ? null : (System.String)dataRow["ShipToStateCode"];
			entity.ShipToPostalCode = Convert.IsDBNull(dataRow["ShipToPostalCode"]) ? null : (System.String)dataRow["ShipToPostalCode"];
			entity.ShipToCountry = Convert.IsDBNull(dataRow["ShipToCountry"]) ? null : (System.String)dataRow["ShipToCountry"];
			entity.ShipToPhoneNumber = Convert.IsDBNull(dataRow["ShipToPhoneNumber"]) ? null : (System.String)dataRow["ShipToPhoneNumber"];
			entity.ShipToEmailID = Convert.IsDBNull(dataRow["ShipToEmailID"]) ? null : (System.String)dataRow["ShipToEmailID"];
			entity.ShippingID = Convert.IsDBNull(dataRow["ShippingID"]) ? null : (System.Int32?)dataRow["ShippingID"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.OrderShipment"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.OrderShipment Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.OrderShipment entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region ShippingIDSource	
			if (CanDeepLoad(entity, "Shipping|ShippingIDSource", deepLoadType, innerList) 
				&& entity.ShippingIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.ShippingID ?? (int)0);
				Shipping tmpEntity = EntityManager.LocateEntity<Shipping>(EntityLocator.ConstructKeyFromPkItems(typeof(Shipping), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ShippingIDSource = tmpEntity;
				else
					entity.ShippingIDSource = DataRepository.ShippingProvider.GetByShippingID(transactionManager, (entity.ShippingID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ShippingIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ShippingIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.ShippingProvider.DeepLoad(transactionManager, entity.ShippingIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ShippingIDSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByOrderShipmentID methods when available
			
			#region OrderLineItemCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<OrderLineItem>|OrderLineItemCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'OrderLineItemCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.OrderLineItemCollection = DataRepository.OrderLineItemProvider.GetByOrderShipmentID(transactionManager, entity.OrderShipmentID);

				if (deep && entity.OrderLineItemCollection.Count > 0)
				{
					deepHandles.Add("OrderLineItemCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<OrderLineItem>) DataRepository.OrderLineItemProvider.DeepLoad,
						new object[] { transactionManager, entity.OrderLineItemCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.OrderShipment object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.OrderShipment instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.OrderShipment Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.OrderShipment entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region ShippingIDSource
			if (CanDeepSave(entity, "Shipping|ShippingIDSource", deepSaveType, innerList) 
				&& entity.ShippingIDSource != null)
			{
				DataRepository.ShippingProvider.Save(transactionManager, entity.ShippingIDSource);
				entity.ShippingID = entity.ShippingIDSource.ShippingID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<OrderLineItem>
				if (CanDeepSave(entity.OrderLineItemCollection, "List<OrderLineItem>|OrderLineItemCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(OrderLineItem child in entity.OrderLineItemCollection)
					{
						if(child.OrderShipmentIDSource != null)
						{
							child.OrderShipmentID = child.OrderShipmentIDSource.OrderShipmentID;
						}
						else
						{
							child.OrderShipmentID = entity.OrderShipmentID;
						}

					}

					if (entity.OrderLineItemCollection.Count > 0 || entity.OrderLineItemCollection.DeletedItems.Count > 0)
					{
						//DataRepository.OrderLineItemProvider.Save(transactionManager, entity.OrderLineItemCollection);
						
						deepHandles.Add("OrderLineItemCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< OrderLineItem >) DataRepository.OrderLineItemProvider.DeepSave,
							new object[] { transactionManager, entity.OrderLineItemCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region OrderShipmentChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.OrderShipment</c>
	///</summary>
	public enum OrderShipmentChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Shipping</c> at ShippingIDSource
		///</summary>
		[ChildEntityType(typeof(Shipping))]
		Shipping,
		///<summary>
		/// Collection of <c>OrderShipment</c> as OneToMany for OrderLineItemCollection
		///</summary>
		[ChildEntityType(typeof(TList<OrderLineItem>))]
		OrderLineItemCollection,
	}
	
	#endregion OrderShipmentChildEntityTypes
	
	#region OrderShipmentFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;OrderShipmentColumn&gt;"/> class
	/// that is used exclusively with a <see cref="OrderShipment"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class OrderShipmentFilterBuilder : SqlFilterBuilder<OrderShipmentColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the OrderShipmentFilterBuilder class.
		/// </summary>
		public OrderShipmentFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the OrderShipmentFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public OrderShipmentFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the OrderShipmentFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public OrderShipmentFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion OrderShipmentFilterBuilder
	
	#region OrderShipmentParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;OrderShipmentColumn&gt;"/> class
	/// that is used exclusively with a <see cref="OrderShipment"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class OrderShipmentParameterBuilder : ParameterizedSqlFilterBuilder<OrderShipmentColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the OrderShipmentParameterBuilder class.
		/// </summary>
		public OrderShipmentParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the OrderShipmentParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public OrderShipmentParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the OrderShipmentParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public OrderShipmentParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion OrderShipmentParameterBuilder
	
	#region OrderShipmentSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;OrderShipmentColumn&gt;"/> class
	/// that is used exclusively with a <see cref="OrderShipment"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class OrderShipmentSortBuilder : SqlSortBuilder<OrderShipmentColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the OrderShipmentSqlSortBuilder class.
		/// </summary>
		public OrderShipmentSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion OrderShipmentSortBuilder
	
} // end namespace
