﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ZipCodeProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class ZipCodeProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.ZipCode, ZNode.Libraries.DataAccess.Entities.ZipCodeKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ZipCodeKey key)
		{
			return Delete(transactionManager, key.ZipCodeID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_zipCodeID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _zipCodeID)
		{
			return Delete(null, _zipCodeID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_zipCodeID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _zipCodeID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.ZipCode Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ZipCodeKey key, int start, int pageLength)
		{
			return GetByZipCodeID(transactionManager, key.ZipCodeID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IDX_ZNodeZipCode_AreaCode index.
		/// </summary>
		/// <param name="_areaCode"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;ZipCode&gt;"/> class.</returns>
		public TList<ZipCode> GetByAreaCode(System.String _areaCode)
		{
			int count = -1;
			return GetByAreaCode(null,_areaCode, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IDX_ZNodeZipCode_AreaCode index.
		/// </summary>
		/// <param name="_areaCode"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ZipCode&gt;"/> class.</returns>
		public TList<ZipCode> GetByAreaCode(System.String _areaCode, int start, int pageLength)
		{
			int count = -1;
			return GetByAreaCode(null, _areaCode, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IDX_ZNodeZipCode_AreaCode index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_areaCode"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ZipCode&gt;"/> class.</returns>
		public TList<ZipCode> GetByAreaCode(TransactionManager transactionManager, System.String _areaCode)
		{
			int count = -1;
			return GetByAreaCode(transactionManager, _areaCode, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IDX_ZNodeZipCode_AreaCode index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_areaCode"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ZipCode&gt;"/> class.</returns>
		public TList<ZipCode> GetByAreaCode(TransactionManager transactionManager, System.String _areaCode, int start, int pageLength)
		{
			int count = -1;
			return GetByAreaCode(transactionManager, _areaCode, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IDX_ZNodeZipCode_AreaCode index.
		/// </summary>
		/// <param name="_areaCode"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ZipCode&gt;"/> class.</returns>
		public TList<ZipCode> GetByAreaCode(System.String _areaCode, int start, int pageLength, out int count)
		{
			return GetByAreaCode(null, _areaCode, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IDX_ZNodeZipCode_AreaCode index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_areaCode"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;ZipCode&gt;"/> class.</returns>
		public abstract TList<ZipCode> GetByAreaCode(TransactionManager transactionManager, System.String _areaCode, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IDX_ZNodeZipCode_CityName index.
		/// </summary>
		/// <param name="_cityName"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;ZipCode&gt;"/> class.</returns>
		public TList<ZipCode> GetByCityName(System.String _cityName)
		{
			int count = -1;
			return GetByCityName(null,_cityName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IDX_ZNodeZipCode_CityName index.
		/// </summary>
		/// <param name="_cityName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ZipCode&gt;"/> class.</returns>
		public TList<ZipCode> GetByCityName(System.String _cityName, int start, int pageLength)
		{
			int count = -1;
			return GetByCityName(null, _cityName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IDX_ZNodeZipCode_CityName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_cityName"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ZipCode&gt;"/> class.</returns>
		public TList<ZipCode> GetByCityName(TransactionManager transactionManager, System.String _cityName)
		{
			int count = -1;
			return GetByCityName(transactionManager, _cityName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IDX_ZNodeZipCode_CityName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_cityName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ZipCode&gt;"/> class.</returns>
		public TList<ZipCode> GetByCityName(TransactionManager transactionManager, System.String _cityName, int start, int pageLength)
		{
			int count = -1;
			return GetByCityName(transactionManager, _cityName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IDX_ZNodeZipCode_CityName index.
		/// </summary>
		/// <param name="_cityName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ZipCode&gt;"/> class.</returns>
		public TList<ZipCode> GetByCityName(System.String _cityName, int start, int pageLength, out int count)
		{
			return GetByCityName(null, _cityName, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IDX_ZNodeZipCode_CityName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_cityName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;ZipCode&gt;"/> class.</returns>
		public abstract TList<ZipCode> GetByCityName(TransactionManager transactionManager, System.String _cityName, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IDX_ZNodeZipCode_CountyName index.
		/// </summary>
		/// <param name="_countyName"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;ZipCode&gt;"/> class.</returns>
		public TList<ZipCode> GetByCountyName(System.String _countyName)
		{
			int count = -1;
			return GetByCountyName(null,_countyName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IDX_ZNodeZipCode_CountyName index.
		/// </summary>
		/// <param name="_countyName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ZipCode&gt;"/> class.</returns>
		public TList<ZipCode> GetByCountyName(System.String _countyName, int start, int pageLength)
		{
			int count = -1;
			return GetByCountyName(null, _countyName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IDX_ZNodeZipCode_CountyName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_countyName"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ZipCode&gt;"/> class.</returns>
		public TList<ZipCode> GetByCountyName(TransactionManager transactionManager, System.String _countyName)
		{
			int count = -1;
			return GetByCountyName(transactionManager, _countyName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IDX_ZNodeZipCode_CountyName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_countyName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ZipCode&gt;"/> class.</returns>
		public TList<ZipCode> GetByCountyName(TransactionManager transactionManager, System.String _countyName, int start, int pageLength)
		{
			int count = -1;
			return GetByCountyName(transactionManager, _countyName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IDX_ZNodeZipCode_CountyName index.
		/// </summary>
		/// <param name="_countyName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ZipCode&gt;"/> class.</returns>
		public TList<ZipCode> GetByCountyName(System.String _countyName, int start, int pageLength, out int count)
		{
			return GetByCountyName(null, _countyName, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IDX_ZNodeZipCode_CountyName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_countyName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;ZipCode&gt;"/> class.</returns>
		public abstract TList<ZipCode> GetByCountyName(TransactionManager transactionManager, System.String _countyName, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IDX_ZNodeZipCode_Latitude index.
		/// </summary>
		/// <param name="_latitude"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;ZipCode&gt;"/> class.</returns>
		public TList<ZipCode> GetByLatitude(System.Decimal? _latitude)
		{
			int count = -1;
			return GetByLatitude(null,_latitude, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IDX_ZNodeZipCode_Latitude index.
		/// </summary>
		/// <param name="_latitude"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ZipCode&gt;"/> class.</returns>
		public TList<ZipCode> GetByLatitude(System.Decimal? _latitude, int start, int pageLength)
		{
			int count = -1;
			return GetByLatitude(null, _latitude, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IDX_ZNodeZipCode_Latitude index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_latitude"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ZipCode&gt;"/> class.</returns>
		public TList<ZipCode> GetByLatitude(TransactionManager transactionManager, System.Decimal? _latitude)
		{
			int count = -1;
			return GetByLatitude(transactionManager, _latitude, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IDX_ZNodeZipCode_Latitude index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_latitude"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ZipCode&gt;"/> class.</returns>
		public TList<ZipCode> GetByLatitude(TransactionManager transactionManager, System.Decimal? _latitude, int start, int pageLength)
		{
			int count = -1;
			return GetByLatitude(transactionManager, _latitude, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IDX_ZNodeZipCode_Latitude index.
		/// </summary>
		/// <param name="_latitude"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ZipCode&gt;"/> class.</returns>
		public TList<ZipCode> GetByLatitude(System.Decimal? _latitude, int start, int pageLength, out int count)
		{
			return GetByLatitude(null, _latitude, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IDX_ZNodeZipCode_Latitude index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_latitude"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;ZipCode&gt;"/> class.</returns>
		public abstract TList<ZipCode> GetByLatitude(TransactionManager transactionManager, System.Decimal? _latitude, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IDX_ZNodeZipCode_Longitude index.
		/// </summary>
		/// <param name="_longitude"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;ZipCode&gt;"/> class.</returns>
		public TList<ZipCode> GetByLongitude(System.Decimal? _longitude)
		{
			int count = -1;
			return GetByLongitude(null,_longitude, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IDX_ZNodeZipCode_Longitude index.
		/// </summary>
		/// <param name="_longitude"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ZipCode&gt;"/> class.</returns>
		public TList<ZipCode> GetByLongitude(System.Decimal? _longitude, int start, int pageLength)
		{
			int count = -1;
			return GetByLongitude(null, _longitude, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IDX_ZNodeZipCode_Longitude index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_longitude"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ZipCode&gt;"/> class.</returns>
		public TList<ZipCode> GetByLongitude(TransactionManager transactionManager, System.Decimal? _longitude)
		{
			int count = -1;
			return GetByLongitude(transactionManager, _longitude, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IDX_ZNodeZipCode_Longitude index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_longitude"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ZipCode&gt;"/> class.</returns>
		public TList<ZipCode> GetByLongitude(TransactionManager transactionManager, System.Decimal? _longitude, int start, int pageLength)
		{
			int count = -1;
			return GetByLongitude(transactionManager, _longitude, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IDX_ZNodeZipCode_Longitude index.
		/// </summary>
		/// <param name="_longitude"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ZipCode&gt;"/> class.</returns>
		public TList<ZipCode> GetByLongitude(System.Decimal? _longitude, int start, int pageLength, out int count)
		{
			return GetByLongitude(null, _longitude, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IDX_ZNodeZipCode_Longitude index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_longitude"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;ZipCode&gt;"/> class.</returns>
		public abstract TList<ZipCode> GetByLongitude(TransactionManager transactionManager, System.Decimal? _longitude, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IDX_ZNodeZipCode_StateAbbr index.
		/// </summary>
		/// <param name="_stateAbbr"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;ZipCode&gt;"/> class.</returns>
		public TList<ZipCode> GetByStateAbbr(System.String _stateAbbr)
		{
			int count = -1;
			return GetByStateAbbr(null,_stateAbbr, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IDX_ZNodeZipCode_StateAbbr index.
		/// </summary>
		/// <param name="_stateAbbr"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ZipCode&gt;"/> class.</returns>
		public TList<ZipCode> GetByStateAbbr(System.String _stateAbbr, int start, int pageLength)
		{
			int count = -1;
			return GetByStateAbbr(null, _stateAbbr, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IDX_ZNodeZipCode_StateAbbr index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_stateAbbr"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ZipCode&gt;"/> class.</returns>
		public TList<ZipCode> GetByStateAbbr(TransactionManager transactionManager, System.String _stateAbbr)
		{
			int count = -1;
			return GetByStateAbbr(transactionManager, _stateAbbr, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IDX_ZNodeZipCode_StateAbbr index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_stateAbbr"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ZipCode&gt;"/> class.</returns>
		public TList<ZipCode> GetByStateAbbr(TransactionManager transactionManager, System.String _stateAbbr, int start, int pageLength)
		{
			int count = -1;
			return GetByStateAbbr(transactionManager, _stateAbbr, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IDX_ZNodeZipCode_StateAbbr index.
		/// </summary>
		/// <param name="_stateAbbr"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ZipCode&gt;"/> class.</returns>
		public TList<ZipCode> GetByStateAbbr(System.String _stateAbbr, int start, int pageLength, out int count)
		{
			return GetByStateAbbr(null, _stateAbbr, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IDX_ZNodeZipCode_StateAbbr index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_stateAbbr"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;ZipCode&gt;"/> class.</returns>
		public abstract TList<ZipCode> GetByStateAbbr(TransactionManager transactionManager, System.String _stateAbbr, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IDX_ZNodeZipCode_StateName index.
		/// </summary>
		/// <param name="_stateName"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;ZipCode&gt;"/> class.</returns>
		public TList<ZipCode> GetByStateName(System.String _stateName)
		{
			int count = -1;
			return GetByStateName(null,_stateName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IDX_ZNodeZipCode_StateName index.
		/// </summary>
		/// <param name="_stateName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ZipCode&gt;"/> class.</returns>
		public TList<ZipCode> GetByStateName(System.String _stateName, int start, int pageLength)
		{
			int count = -1;
			return GetByStateName(null, _stateName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IDX_ZNodeZipCode_StateName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_stateName"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ZipCode&gt;"/> class.</returns>
		public TList<ZipCode> GetByStateName(TransactionManager transactionManager, System.String _stateName)
		{
			int count = -1;
			return GetByStateName(transactionManager, _stateName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IDX_ZNodeZipCode_StateName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_stateName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ZipCode&gt;"/> class.</returns>
		public TList<ZipCode> GetByStateName(TransactionManager transactionManager, System.String _stateName, int start, int pageLength)
		{
			int count = -1;
			return GetByStateName(transactionManager, _stateName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IDX_ZNodeZipCode_StateName index.
		/// </summary>
		/// <param name="_stateName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ZipCode&gt;"/> class.</returns>
		public TList<ZipCode> GetByStateName(System.String _stateName, int start, int pageLength, out int count)
		{
			return GetByStateName(null, _stateName, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IDX_ZNodeZipCode_StateName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_stateName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;ZipCode&gt;"/> class.</returns>
		public abstract TList<ZipCode> GetByStateName(TransactionManager transactionManager, System.String _stateName, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IDX_ZNodeZipCode_ZipCode index.
		/// </summary>
		/// <param name="_zIP"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;ZipCode&gt;"/> class.</returns>
		public TList<ZipCode> GetByZIP(System.String _zIP)
		{
			int count = -1;
			return GetByZIP(null,_zIP, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IDX_ZNodeZipCode_ZipCode index.
		/// </summary>
		/// <param name="_zIP"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ZipCode&gt;"/> class.</returns>
		public TList<ZipCode> GetByZIP(System.String _zIP, int start, int pageLength)
		{
			int count = -1;
			return GetByZIP(null, _zIP, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IDX_ZNodeZipCode_ZipCode index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_zIP"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ZipCode&gt;"/> class.</returns>
		public TList<ZipCode> GetByZIP(TransactionManager transactionManager, System.String _zIP)
		{
			int count = -1;
			return GetByZIP(transactionManager, _zIP, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IDX_ZNodeZipCode_ZipCode index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_zIP"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ZipCode&gt;"/> class.</returns>
		public TList<ZipCode> GetByZIP(TransactionManager transactionManager, System.String _zIP, int start, int pageLength)
		{
			int count = -1;
			return GetByZIP(transactionManager, _zIP, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IDX_ZNodeZipCode_ZipCode index.
		/// </summary>
		/// <param name="_zIP"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ZipCode&gt;"/> class.</returns>
		public TList<ZipCode> GetByZIP(System.String _zIP, int start, int pageLength, out int count)
		{
			return GetByZIP(null, _zIP, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IDX_ZNodeZipCode_ZipCode index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_zIP"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;ZipCode&gt;"/> class.</returns>
		public abstract TList<ZipCode> GetByZIP(TransactionManager transactionManager, System.String _zIP, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodeZipCode index.
		/// </summary>
		/// <param name="_zipCodeID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ZipCode"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ZipCode GetByZipCodeID(System.Int32 _zipCodeID)
		{
			int count = -1;
			return GetByZipCodeID(null,_zipCodeID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeZipCode index.
		/// </summary>
		/// <param name="_zipCodeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ZipCode"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ZipCode GetByZipCodeID(System.Int32 _zipCodeID, int start, int pageLength)
		{
			int count = -1;
			return GetByZipCodeID(null, _zipCodeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeZipCode index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_zipCodeID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ZipCode"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ZipCode GetByZipCodeID(TransactionManager transactionManager, System.Int32 _zipCodeID)
		{
			int count = -1;
			return GetByZipCodeID(transactionManager, _zipCodeID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeZipCode index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_zipCodeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ZipCode"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ZipCode GetByZipCodeID(TransactionManager transactionManager, System.Int32 _zipCodeID, int start, int pageLength)
		{
			int count = -1;
			return GetByZipCodeID(transactionManager, _zipCodeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeZipCode index.
		/// </summary>
		/// <param name="_zipCodeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ZipCode"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ZipCode GetByZipCodeID(System.Int32 _zipCodeID, int start, int pageLength, out int count)
		{
			return GetByZipCodeID(null, _zipCodeID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeZipCode index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_zipCodeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ZipCode"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.ZipCode GetByZipCodeID(TransactionManager transactionManager, System.Int32 _zipCodeID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;ZipCode&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;ZipCode&gt;"/></returns>
		public static TList<ZipCode> Fill(IDataReader reader, TList<ZipCode> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.ZipCode c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("ZipCode")
					.Append("|").Append((System.Int32)reader[((int)ZipCodeColumn.ZipCodeID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<ZipCode>(
					key.ToString(), // EntityTrackingKey
					"ZipCode",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.ZipCode();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.ZipCodeID = (System.Int32)reader[((int)ZipCodeColumn.ZipCodeID - 1)];
					c.ZIP = (reader.IsDBNull(((int)ZipCodeColumn.ZIP - 1)))?null:(System.String)reader[((int)ZipCodeColumn.ZIP - 1)];
					c.ZIPType = (reader.IsDBNull(((int)ZipCodeColumn.ZIPType - 1)))?null:(System.String)reader[((int)ZipCodeColumn.ZIPType - 1)];
					c.CityName = (reader.IsDBNull(((int)ZipCodeColumn.CityName - 1)))?null:(System.String)reader[((int)ZipCodeColumn.CityName - 1)];
					c.CityType = (reader.IsDBNull(((int)ZipCodeColumn.CityType - 1)))?null:(System.String)reader[((int)ZipCodeColumn.CityType - 1)];
					c.StateName = (reader.IsDBNull(((int)ZipCodeColumn.StateName - 1)))?null:(System.String)reader[((int)ZipCodeColumn.StateName - 1)];
					c.StateAbbr = (reader.IsDBNull(((int)ZipCodeColumn.StateAbbr - 1)))?null:(System.String)reader[((int)ZipCodeColumn.StateAbbr - 1)];
					c.AreaCode = (reader.IsDBNull(((int)ZipCodeColumn.AreaCode - 1)))?null:(System.String)reader[((int)ZipCodeColumn.AreaCode - 1)];
					c.Latitude = (reader.IsDBNull(((int)ZipCodeColumn.Latitude - 1)))?null:(System.Decimal?)reader[((int)ZipCodeColumn.Latitude - 1)];
					c.Longitude = (reader.IsDBNull(((int)ZipCodeColumn.Longitude - 1)))?null:(System.Decimal?)reader[((int)ZipCodeColumn.Longitude - 1)];
					c.CountyName = (reader.IsDBNull(((int)ZipCodeColumn.CountyName - 1)))?null:(System.String)reader[((int)ZipCodeColumn.CountyName - 1)];
					c.CountyFIPS = (reader.IsDBNull(((int)ZipCodeColumn.CountyFIPS - 1)))?null:(System.String)reader[((int)ZipCodeColumn.CountyFIPS - 1)];
					c.StateFIPS = (reader.IsDBNull(((int)ZipCodeColumn.StateFIPS - 1)))?null:(System.String)reader[((int)ZipCodeColumn.StateFIPS - 1)];
					c.MSACode = (reader.IsDBNull(((int)ZipCodeColumn.MSACode - 1)))?null:(System.String)reader[((int)ZipCodeColumn.MSACode - 1)];
					c.TimeZone = (reader.IsDBNull(((int)ZipCodeColumn.TimeZone - 1)))?null:(System.String)reader[((int)ZipCodeColumn.TimeZone - 1)];
					c.UTC = (reader.IsDBNull(((int)ZipCodeColumn.UTC - 1)))?null:(System.Decimal?)reader[((int)ZipCodeColumn.UTC - 1)];
					c.DST = (reader.IsDBNull(((int)ZipCodeColumn.DST - 1)))?null:(System.String)reader[((int)ZipCodeColumn.DST - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.ZipCode"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ZipCode"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.ZipCode entity)
		{
			if (!reader.Read()) return;
			
			entity.ZipCodeID = (System.Int32)reader[((int)ZipCodeColumn.ZipCodeID - 1)];
			entity.ZIP = (reader.IsDBNull(((int)ZipCodeColumn.ZIP - 1)))?null:(System.String)reader[((int)ZipCodeColumn.ZIP - 1)];
			entity.ZIPType = (reader.IsDBNull(((int)ZipCodeColumn.ZIPType - 1)))?null:(System.String)reader[((int)ZipCodeColumn.ZIPType - 1)];
			entity.CityName = (reader.IsDBNull(((int)ZipCodeColumn.CityName - 1)))?null:(System.String)reader[((int)ZipCodeColumn.CityName - 1)];
			entity.CityType = (reader.IsDBNull(((int)ZipCodeColumn.CityType - 1)))?null:(System.String)reader[((int)ZipCodeColumn.CityType - 1)];
			entity.StateName = (reader.IsDBNull(((int)ZipCodeColumn.StateName - 1)))?null:(System.String)reader[((int)ZipCodeColumn.StateName - 1)];
			entity.StateAbbr = (reader.IsDBNull(((int)ZipCodeColumn.StateAbbr - 1)))?null:(System.String)reader[((int)ZipCodeColumn.StateAbbr - 1)];
			entity.AreaCode = (reader.IsDBNull(((int)ZipCodeColumn.AreaCode - 1)))?null:(System.String)reader[((int)ZipCodeColumn.AreaCode - 1)];
			entity.Latitude = (reader.IsDBNull(((int)ZipCodeColumn.Latitude - 1)))?null:(System.Decimal?)reader[((int)ZipCodeColumn.Latitude - 1)];
			entity.Longitude = (reader.IsDBNull(((int)ZipCodeColumn.Longitude - 1)))?null:(System.Decimal?)reader[((int)ZipCodeColumn.Longitude - 1)];
			entity.CountyName = (reader.IsDBNull(((int)ZipCodeColumn.CountyName - 1)))?null:(System.String)reader[((int)ZipCodeColumn.CountyName - 1)];
			entity.CountyFIPS = (reader.IsDBNull(((int)ZipCodeColumn.CountyFIPS - 1)))?null:(System.String)reader[((int)ZipCodeColumn.CountyFIPS - 1)];
			entity.StateFIPS = (reader.IsDBNull(((int)ZipCodeColumn.StateFIPS - 1)))?null:(System.String)reader[((int)ZipCodeColumn.StateFIPS - 1)];
			entity.MSACode = (reader.IsDBNull(((int)ZipCodeColumn.MSACode - 1)))?null:(System.String)reader[((int)ZipCodeColumn.MSACode - 1)];
			entity.TimeZone = (reader.IsDBNull(((int)ZipCodeColumn.TimeZone - 1)))?null:(System.String)reader[((int)ZipCodeColumn.TimeZone - 1)];
			entity.UTC = (reader.IsDBNull(((int)ZipCodeColumn.UTC - 1)))?null:(System.Decimal?)reader[((int)ZipCodeColumn.UTC - 1)];
			entity.DST = (reader.IsDBNull(((int)ZipCodeColumn.DST - 1)))?null:(System.String)reader[((int)ZipCodeColumn.DST - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.ZipCode"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ZipCode"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.ZipCode entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.ZipCodeID = (System.Int32)dataRow["ZipCodeID"];
			entity.ZIP = Convert.IsDBNull(dataRow["ZIP"]) ? null : (System.String)dataRow["ZIP"];
			entity.ZIPType = Convert.IsDBNull(dataRow["ZIPType"]) ? null : (System.String)dataRow["ZIPType"];
			entity.CityName = Convert.IsDBNull(dataRow["CityName"]) ? null : (System.String)dataRow["CityName"];
			entity.CityType = Convert.IsDBNull(dataRow["CityType"]) ? null : (System.String)dataRow["CityType"];
			entity.StateName = Convert.IsDBNull(dataRow["StateName"]) ? null : (System.String)dataRow["StateName"];
			entity.StateAbbr = Convert.IsDBNull(dataRow["StateAbbr"]) ? null : (System.String)dataRow["StateAbbr"];
			entity.AreaCode = Convert.IsDBNull(dataRow["AreaCode"]) ? null : (System.String)dataRow["AreaCode"];
			entity.Latitude = Convert.IsDBNull(dataRow["Latitude"]) ? null : (System.Decimal?)dataRow["Latitude"];
			entity.Longitude = Convert.IsDBNull(dataRow["Longitude"]) ? null : (System.Decimal?)dataRow["Longitude"];
			entity.CountyName = Convert.IsDBNull(dataRow["CountyName"]) ? null : (System.String)dataRow["CountyName"];
			entity.CountyFIPS = Convert.IsDBNull(dataRow["CountyFIPS"]) ? null : (System.String)dataRow["CountyFIPS"];
			entity.StateFIPS = Convert.IsDBNull(dataRow["StateFIPS"]) ? null : (System.String)dataRow["StateFIPS"];
			entity.MSACode = Convert.IsDBNull(dataRow["MSACode"]) ? null : (System.String)dataRow["MSACode"];
			entity.TimeZone = Convert.IsDBNull(dataRow["TimeZone"]) ? null : (System.String)dataRow["TimeZone"];
			entity.UTC = Convert.IsDBNull(dataRow["UTC"]) ? null : (System.Decimal?)dataRow["UTC"];
			entity.DST = Convert.IsDBNull(dataRow["DST"]) ? null : (System.String)dataRow["DST"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ZipCode"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.ZipCode Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ZipCode entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region ZipCodeIDSource	
			if (CanDeepLoad(entity, "ZipCode|ZipCodeIDSource", deepLoadType, innerList) 
				&& entity.ZipCodeIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.ZipCodeID;
				ZipCode tmpEntity = EntityManager.LocateEntity<ZipCode>(EntityLocator.ConstructKeyFromPkItems(typeof(ZipCode), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ZipCodeIDSource = tmpEntity;
				else
					entity.ZipCodeIDSource = DataRepository.ZipCodeProvider.GetByZipCodeID(transactionManager, entity.ZipCodeID);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ZipCodeIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ZipCodeIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.ZipCodeProvider.DeepLoad(transactionManager, entity.ZipCodeIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ZipCodeIDSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByZipCodeID methods when available
			
			#region ZipCode
			// RelationshipType.OneToOne
			if (CanDeepLoad(entity, "ZipCode|ZipCode", deepLoadType, innerList))
			{
				entity.ZipCode = DataRepository.ZipCodeProvider.GetByZipCodeID(transactionManager, entity.ZipCodeID);
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ZipCode' loaded. key " + entity.EntityTrackingKey);
				#endif 

				if (deep && entity.ZipCode != null)
				{
					deepHandles.Add("ZipCode",
						new KeyValuePair<Delegate, object>((DeepLoadSingleHandle< ZipCode >) DataRepository.ZipCodeProvider.DeepLoad,
						new object[] { transactionManager, entity.ZipCode, deep, deepLoadType, childTypes, innerList }
					));
				}
			}
			#endregion 
			
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.ZipCode object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.ZipCode instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.ZipCode Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ZipCode entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region ZipCodeIDSource
			if (CanDeepSave(entity, "ZipCode|ZipCodeIDSource", deepSaveType, innerList) 
				&& entity.ZipCodeIDSource != null)
			{
				DataRepository.ZipCodeProvider.Save(transactionManager, entity.ZipCodeIDSource);
				entity.ZipCodeID = entity.ZipCodeIDSource.ZipCodeID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();

			#region ZipCode
			if (CanDeepSave(entity.ZipCode, "ZipCode|ZipCode", deepSaveType, innerList))
			{

				if (entity.ZipCode != null)
				{
					// update each child parent id with the real parent id (mostly used on insert)

					entity.ZipCode.ZipCodeID = entity.ZipCodeID;
					//DataRepository.ZipCodeProvider.Save(transactionManager, entity.ZipCode);
					deepHandles.Add("ZipCode",
						new KeyValuePair<Delegate, object>((DeepSaveSingleHandle< ZipCode >) DataRepository.ZipCodeProvider.DeepSave,
						new object[] { transactionManager, entity.ZipCode, deepSaveType, childTypes, innerList }
					));
				}
			} 
			#endregion 
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region ZipCodeChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.ZipCode</c>
	///</summary>
	public enum ZipCodeChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>ZipCode</c> at ZipCodeIDSource
		///</summary>
		[ChildEntityType(typeof(ZipCode))]
		ZipCode,
	}
	
	#endregion ZipCodeChildEntityTypes
	
	#region ZipCodeFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;ZipCodeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ZipCode"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ZipCodeFilterBuilder : SqlFilterBuilder<ZipCodeColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ZipCodeFilterBuilder class.
		/// </summary>
		public ZipCodeFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ZipCodeFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ZipCodeFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ZipCodeFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ZipCodeFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ZipCodeFilterBuilder
	
	#region ZipCodeParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;ZipCodeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ZipCode"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ZipCodeParameterBuilder : ParameterizedSqlFilterBuilder<ZipCodeColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ZipCodeParameterBuilder class.
		/// </summary>
		public ZipCodeParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ZipCodeParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ZipCodeParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ZipCodeParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ZipCodeParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ZipCodeParameterBuilder
	
	#region ZipCodeSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;ZipCodeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ZipCode"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ZipCodeSortBuilder : SqlSortBuilder<ZipCodeColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ZipCodeSqlSortBuilder class.
		/// </summary>
		public ZipCodeSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ZipCodeSortBuilder
	
} // end namespace
