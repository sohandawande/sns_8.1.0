﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ActivityLogTypeProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class ActivityLogTypeProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.ActivityLogType, ZNode.Libraries.DataAccess.Entities.ActivityLogTypeKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ActivityLogTypeKey key)
		{
			return Delete(transactionManager, key.ActivityLogTypeID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_activityLogTypeID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _activityLogTypeID)
		{
			return Delete(null, _activityLogTypeID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_activityLogTypeID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _activityLogTypeID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.ActivityLogType Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ActivityLogTypeKey key, int start, int pageLength)
		{
			return GetByActivityLogTypeID(transactionManager, key.ActivityLogTypeID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ActivityTypeID index.
		/// </summary>
		/// <param name="_activityLogTypeID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ActivityLogType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ActivityLogType GetByActivityLogTypeID(System.Int32 _activityLogTypeID)
		{
			int count = -1;
			return GetByActivityLogTypeID(null,_activityLogTypeID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ActivityTypeID index.
		/// </summary>
		/// <param name="_activityLogTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ActivityLogType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ActivityLogType GetByActivityLogTypeID(System.Int32 _activityLogTypeID, int start, int pageLength)
		{
			int count = -1;
			return GetByActivityLogTypeID(null, _activityLogTypeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ActivityTypeID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_activityLogTypeID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ActivityLogType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ActivityLogType GetByActivityLogTypeID(TransactionManager transactionManager, System.Int32 _activityLogTypeID)
		{
			int count = -1;
			return GetByActivityLogTypeID(transactionManager, _activityLogTypeID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ActivityTypeID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_activityLogTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ActivityLogType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ActivityLogType GetByActivityLogTypeID(TransactionManager transactionManager, System.Int32 _activityLogTypeID, int start, int pageLength)
		{
			int count = -1;
			return GetByActivityLogTypeID(transactionManager, _activityLogTypeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ActivityTypeID index.
		/// </summary>
		/// <param name="_activityLogTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ActivityLogType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ActivityLogType GetByActivityLogTypeID(System.Int32 _activityLogTypeID, int start, int pageLength, out int count)
		{
			return GetByActivityLogTypeID(null, _activityLogTypeID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ActivityTypeID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_activityLogTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ActivityLogType"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.ActivityLogType GetByActivityLogTypeID(TransactionManager transactionManager, System.Int32 _activityLogTypeID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_Name index.
		/// </summary>
		/// <param name="_name"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ActivityLogType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ActivityLogType GetByName(System.String _name)
		{
			int count = -1;
			return GetByName(null,_name, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Name index.
		/// </summary>
		/// <param name="_name"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ActivityLogType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ActivityLogType GetByName(System.String _name, int start, int pageLength)
		{
			int count = -1;
			return GetByName(null, _name, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Name index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_name"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ActivityLogType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ActivityLogType GetByName(TransactionManager transactionManager, System.String _name)
		{
			int count = -1;
			return GetByName(transactionManager, _name, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Name index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_name"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ActivityLogType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ActivityLogType GetByName(TransactionManager transactionManager, System.String _name, int start, int pageLength)
		{
			int count = -1;
			return GetByName(transactionManager, _name, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Name index.
		/// </summary>
		/// <param name="_name"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ActivityLogType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ActivityLogType GetByName(System.String _name, int start, int pageLength, out int count)
		{
			return GetByName(null, _name, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Name index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_name"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ActivityLogType"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.ActivityLogType GetByName(TransactionManager transactionManager, System.String _name, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_TypeCategory index.
		/// </summary>
		/// <param name="_typeCategory"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;ActivityLogType&gt;"/> class.</returns>
		public TList<ActivityLogType> GetByTypeCategory(System.String _typeCategory)
		{
			int count = -1;
			return GetByTypeCategory(null,_typeCategory, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_TypeCategory index.
		/// </summary>
		/// <param name="_typeCategory"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ActivityLogType&gt;"/> class.</returns>
		public TList<ActivityLogType> GetByTypeCategory(System.String _typeCategory, int start, int pageLength)
		{
			int count = -1;
			return GetByTypeCategory(null, _typeCategory, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_TypeCategory index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_typeCategory"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ActivityLogType&gt;"/> class.</returns>
		public TList<ActivityLogType> GetByTypeCategory(TransactionManager transactionManager, System.String _typeCategory)
		{
			int count = -1;
			return GetByTypeCategory(transactionManager, _typeCategory, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_TypeCategory index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_typeCategory"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ActivityLogType&gt;"/> class.</returns>
		public TList<ActivityLogType> GetByTypeCategory(TransactionManager transactionManager, System.String _typeCategory, int start, int pageLength)
		{
			int count = -1;
			return GetByTypeCategory(transactionManager, _typeCategory, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_TypeCategory index.
		/// </summary>
		/// <param name="_typeCategory"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ActivityLogType&gt;"/> class.</returns>
		public TList<ActivityLogType> GetByTypeCategory(System.String _typeCategory, int start, int pageLength, out int count)
		{
			return GetByTypeCategory(null, _typeCategory, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_TypeCategory index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_typeCategory"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;ActivityLogType&gt;"/> class.</returns>
		public abstract TList<ActivityLogType> GetByTypeCategory(TransactionManager transactionManager, System.String _typeCategory, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;ActivityLogType&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;ActivityLogType&gt;"/></returns>
		public static TList<ActivityLogType> Fill(IDataReader reader, TList<ActivityLogType> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.ActivityLogType c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("ActivityLogType")
					.Append("|").Append((System.Int32)reader[((int)ActivityLogTypeColumn.ActivityLogTypeID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<ActivityLogType>(
					key.ToString(), // EntityTrackingKey
					"ActivityLogType",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.ActivityLogType();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.ActivityLogTypeID = (System.Int32)reader[((int)ActivityLogTypeColumn.ActivityLogTypeID - 1)];
					c.OriginalActivityLogTypeID = c.ActivityLogTypeID;
					c.Name = (reader.IsDBNull(((int)ActivityLogTypeColumn.Name - 1)))?null:(System.String)reader[((int)ActivityLogTypeColumn.Name - 1)];
					c.TypeCategory = (reader.IsDBNull(((int)ActivityLogTypeColumn.TypeCategory - 1)))?null:(System.String)reader[((int)ActivityLogTypeColumn.TypeCategory - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.ActivityLogType"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ActivityLogType"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.ActivityLogType entity)
		{
			if (!reader.Read()) return;
			
			entity.ActivityLogTypeID = (System.Int32)reader[((int)ActivityLogTypeColumn.ActivityLogTypeID - 1)];
			entity.OriginalActivityLogTypeID = (System.Int32)reader["ActivityLogTypeID"];
			entity.Name = (reader.IsDBNull(((int)ActivityLogTypeColumn.Name - 1)))?null:(System.String)reader[((int)ActivityLogTypeColumn.Name - 1)];
			entity.TypeCategory = (reader.IsDBNull(((int)ActivityLogTypeColumn.TypeCategory - 1)))?null:(System.String)reader[((int)ActivityLogTypeColumn.TypeCategory - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.ActivityLogType"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ActivityLogType"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.ActivityLogType entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.ActivityLogTypeID = (System.Int32)dataRow["ActivityLogTypeID"];
			entity.OriginalActivityLogTypeID = (System.Int32)dataRow["ActivityLogTypeID"];
			entity.Name = Convert.IsDBNull(dataRow["Name"]) ? null : (System.String)dataRow["Name"];
			entity.TypeCategory = Convert.IsDBNull(dataRow["TypeCategory"]) ? null : (System.String)dataRow["TypeCategory"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ActivityLogType"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.ActivityLogType Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ActivityLogType entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByActivityLogTypeID methods when available
			
			#region ActivityLogCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<ActivityLog>|ActivityLogCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ActivityLogCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.ActivityLogCollection = DataRepository.ActivityLogProvider.GetByActivityLogTypeID(transactionManager, entity.ActivityLogTypeID);

				if (deep && entity.ActivityLogCollection.Count > 0)
				{
					deepHandles.Add("ActivityLogCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<ActivityLog>) DataRepository.ActivityLogProvider.DeepLoad,
						new object[] { transactionManager, entity.ActivityLogCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.ActivityLogType object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.ActivityLogType instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.ActivityLogType Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ActivityLogType entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<ActivityLog>
				if (CanDeepSave(entity.ActivityLogCollection, "List<ActivityLog>|ActivityLogCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(ActivityLog child in entity.ActivityLogCollection)
					{
						if(child.ActivityLogTypeIDSource != null)
						{
							child.ActivityLogTypeID = child.ActivityLogTypeIDSource.ActivityLogTypeID;
						}
						else
						{
							child.ActivityLogTypeID = entity.ActivityLogTypeID;
						}

					}

					if (entity.ActivityLogCollection.Count > 0 || entity.ActivityLogCollection.DeletedItems.Count > 0)
					{
						//DataRepository.ActivityLogProvider.Save(transactionManager, entity.ActivityLogCollection);
						
						deepHandles.Add("ActivityLogCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< ActivityLog >) DataRepository.ActivityLogProvider.DeepSave,
							new object[] { transactionManager, entity.ActivityLogCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region ActivityLogTypeChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.ActivityLogType</c>
	///</summary>
	public enum ActivityLogTypeChildEntityTypes
	{
		///<summary>
		/// Collection of <c>ActivityLogType</c> as OneToMany for ActivityLogCollection
		///</summary>
		[ChildEntityType(typeof(TList<ActivityLog>))]
		ActivityLogCollection,
	}
	
	#endregion ActivityLogTypeChildEntityTypes
	
	#region ActivityLogTypeFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;ActivityLogTypeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ActivityLogType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ActivityLogTypeFilterBuilder : SqlFilterBuilder<ActivityLogTypeColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ActivityLogTypeFilterBuilder class.
		/// </summary>
		public ActivityLogTypeFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ActivityLogTypeFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ActivityLogTypeFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ActivityLogTypeFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ActivityLogTypeFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ActivityLogTypeFilterBuilder
	
	#region ActivityLogTypeParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;ActivityLogTypeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ActivityLogType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ActivityLogTypeParameterBuilder : ParameterizedSqlFilterBuilder<ActivityLogTypeColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ActivityLogTypeParameterBuilder class.
		/// </summary>
		public ActivityLogTypeParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ActivityLogTypeParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ActivityLogTypeParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ActivityLogTypeParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ActivityLogTypeParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ActivityLogTypeParameterBuilder
	
	#region ActivityLogTypeSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;ActivityLogTypeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ActivityLogType"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ActivityLogTypeSortBuilder : SqlSortBuilder<ActivityLogTypeColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ActivityLogTypeSqlSortBuilder class.
		/// </summary>
		public ActivityLogTypeSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ActivityLogTypeSortBuilder
	
} // end namespace
