﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="VwAspnetWebPartStateSharedProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class VwAspnetWebPartStateSharedProviderBaseCore : EntityViewProviderBase<VwAspnetWebPartStateShared>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;VwAspnetWebPartStateShared&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;VwAspnetWebPartStateShared&gt;"/></returns>
		protected static VList&lt;VwAspnetWebPartStateShared&gt; Fill(DataSet dataSet, VList<VwAspnetWebPartStateShared> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<VwAspnetWebPartStateShared>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;VwAspnetWebPartStateShared&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<VwAspnetWebPartStateShared>"/></returns>
		protected static VList&lt;VwAspnetWebPartStateShared&gt; Fill(DataTable dataTable, VList<VwAspnetWebPartStateShared> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					VwAspnetWebPartStateShared c = new VwAspnetWebPartStateShared();
					c.PathId = (Convert.IsDBNull(row["PathId"]))?Guid.Empty:(System.Guid)row["PathId"];
					c.DataSize = (Convert.IsDBNull(row["DataSize"]))?(int)0:(System.Int32?)row["DataSize"];
					c.LastUpdatedDate = (Convert.IsDBNull(row["LastUpdatedDate"]))?DateTime.MinValue:(System.DateTime)row["LastUpdatedDate"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;VwAspnetWebPartStateShared&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;VwAspnetWebPartStateShared&gt;"/></returns>
		protected VList<VwAspnetWebPartStateShared> Fill(IDataReader reader, VList<VwAspnetWebPartStateShared> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					VwAspnetWebPartStateShared entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<VwAspnetWebPartStateShared>("VwAspnetWebPartStateShared",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new VwAspnetWebPartStateShared();
					}
					
					entity.SuppressEntityEvents = true;

					entity.PathId = (System.Guid)reader[((int)VwAspnetWebPartStateSharedColumn.PathId)];
					//entity.PathId = (Convert.IsDBNull(reader["PathId"]))?Guid.Empty:(System.Guid)reader["PathId"];
					entity.DataSize = (reader.IsDBNull(((int)VwAspnetWebPartStateSharedColumn.DataSize)))?null:(System.Int32?)reader[((int)VwAspnetWebPartStateSharedColumn.DataSize)];
					//entity.DataSize = (Convert.IsDBNull(reader["DataSize"]))?(int)0:(System.Int32?)reader["DataSize"];
					entity.LastUpdatedDate = (System.DateTime)reader[((int)VwAspnetWebPartStateSharedColumn.LastUpdatedDate)];
					//entity.LastUpdatedDate = (Convert.IsDBNull(reader["LastUpdatedDate"]))?DateTime.MinValue:(System.DateTime)reader["LastUpdatedDate"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="VwAspnetWebPartStateShared"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="VwAspnetWebPartStateShared"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, VwAspnetWebPartStateShared entity)
		{
			reader.Read();
			entity.PathId = (System.Guid)reader[((int)VwAspnetWebPartStateSharedColumn.PathId)];
			//entity.PathId = (Convert.IsDBNull(reader["PathId"]))?Guid.Empty:(System.Guid)reader["PathId"];
			entity.DataSize = (reader.IsDBNull(((int)VwAspnetWebPartStateSharedColumn.DataSize)))?null:(System.Int32?)reader[((int)VwAspnetWebPartStateSharedColumn.DataSize)];
			//entity.DataSize = (Convert.IsDBNull(reader["DataSize"]))?(int)0:(System.Int32?)reader["DataSize"];
			entity.LastUpdatedDate = (System.DateTime)reader[((int)VwAspnetWebPartStateSharedColumn.LastUpdatedDate)];
			//entity.LastUpdatedDate = (Convert.IsDBNull(reader["LastUpdatedDate"]))?DateTime.MinValue:(System.DateTime)reader["LastUpdatedDate"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="VwAspnetWebPartStateShared"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="VwAspnetWebPartStateShared"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, VwAspnetWebPartStateShared entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.PathId = (Convert.IsDBNull(dataRow["PathId"]))?Guid.Empty:(System.Guid)dataRow["PathId"];
			entity.DataSize = (Convert.IsDBNull(dataRow["DataSize"]))?(int)0:(System.Int32?)dataRow["DataSize"];
			entity.LastUpdatedDate = (Convert.IsDBNull(dataRow["LastUpdatedDate"]))?DateTime.MinValue:(System.DateTime)dataRow["LastUpdatedDate"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region VwAspnetWebPartStateSharedFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="VwAspnetWebPartStateShared"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class VwAspnetWebPartStateSharedFilterBuilder : SqlFilterBuilder<VwAspnetWebPartStateSharedColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the VwAspnetWebPartStateSharedFilterBuilder class.
		/// </summary>
		public VwAspnetWebPartStateSharedFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the VwAspnetWebPartStateSharedFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public VwAspnetWebPartStateSharedFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the VwAspnetWebPartStateSharedFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public VwAspnetWebPartStateSharedFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion VwAspnetWebPartStateSharedFilterBuilder

	#region VwAspnetWebPartStateSharedParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="VwAspnetWebPartStateShared"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class VwAspnetWebPartStateSharedParameterBuilder : ParameterizedSqlFilterBuilder<VwAspnetWebPartStateSharedColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the VwAspnetWebPartStateSharedParameterBuilder class.
		/// </summary>
		public VwAspnetWebPartStateSharedParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the VwAspnetWebPartStateSharedParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public VwAspnetWebPartStateSharedParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the VwAspnetWebPartStateSharedParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public VwAspnetWebPartStateSharedParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion VwAspnetWebPartStateSharedParameterBuilder
	
	#region VwAspnetWebPartStateSharedSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="VwAspnetWebPartStateShared"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class VwAspnetWebPartStateSharedSortBuilder : SqlSortBuilder<VwAspnetWebPartStateSharedColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the VwAspnetWebPartStateSharedSqlSortBuilder class.
		/// </summary>
		public VwAspnetWebPartStateSharedSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion VwAspnetWebPartStateSharedSortBuilder

} // end namespace
