﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="VwAspnetApplicationsProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class VwAspnetApplicationsProviderBaseCore : EntityViewProviderBase<VwAspnetApplications>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;VwAspnetApplications&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;VwAspnetApplications&gt;"/></returns>
		protected static VList&lt;VwAspnetApplications&gt; Fill(DataSet dataSet, VList<VwAspnetApplications> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<VwAspnetApplications>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;VwAspnetApplications&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<VwAspnetApplications>"/></returns>
		protected static VList&lt;VwAspnetApplications&gt; Fill(DataTable dataTable, VList<VwAspnetApplications> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					VwAspnetApplications c = new VwAspnetApplications();
					c.ApplicationName = (Convert.IsDBNull(row["ApplicationName"]))?string.Empty:(System.String)row["ApplicationName"];
					c.LoweredApplicationName = (Convert.IsDBNull(row["LoweredApplicationName"]))?string.Empty:(System.String)row["LoweredApplicationName"];
					c.ApplicationId = (Convert.IsDBNull(row["ApplicationId"]))?Guid.Empty:(System.Guid)row["ApplicationId"];
					c.Description = (Convert.IsDBNull(row["Description"]))?string.Empty:(System.String)row["Description"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;VwAspnetApplications&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;VwAspnetApplications&gt;"/></returns>
		protected VList<VwAspnetApplications> Fill(IDataReader reader, VList<VwAspnetApplications> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					VwAspnetApplications entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<VwAspnetApplications>("VwAspnetApplications",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new VwAspnetApplications();
					}
					
					entity.SuppressEntityEvents = true;

					entity.ApplicationName = (System.String)reader[((int)VwAspnetApplicationsColumn.ApplicationName)];
					//entity.ApplicationName = (Convert.IsDBNull(reader["ApplicationName"]))?string.Empty:(System.String)reader["ApplicationName"];
					entity.LoweredApplicationName = (System.String)reader[((int)VwAspnetApplicationsColumn.LoweredApplicationName)];
					//entity.LoweredApplicationName = (Convert.IsDBNull(reader["LoweredApplicationName"]))?string.Empty:(System.String)reader["LoweredApplicationName"];
					entity.ApplicationId = (System.Guid)reader[((int)VwAspnetApplicationsColumn.ApplicationId)];
					//entity.ApplicationId = (Convert.IsDBNull(reader["ApplicationId"]))?Guid.Empty:(System.Guid)reader["ApplicationId"];
					entity.Description = (reader.IsDBNull(((int)VwAspnetApplicationsColumn.Description)))?null:(System.String)reader[((int)VwAspnetApplicationsColumn.Description)];
					//entity.Description = (Convert.IsDBNull(reader["Description"]))?string.Empty:(System.String)reader["Description"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="VwAspnetApplications"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="VwAspnetApplications"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, VwAspnetApplications entity)
		{
			reader.Read();
			entity.ApplicationName = (System.String)reader[((int)VwAspnetApplicationsColumn.ApplicationName)];
			//entity.ApplicationName = (Convert.IsDBNull(reader["ApplicationName"]))?string.Empty:(System.String)reader["ApplicationName"];
			entity.LoweredApplicationName = (System.String)reader[((int)VwAspnetApplicationsColumn.LoweredApplicationName)];
			//entity.LoweredApplicationName = (Convert.IsDBNull(reader["LoweredApplicationName"]))?string.Empty:(System.String)reader["LoweredApplicationName"];
			entity.ApplicationId = (System.Guid)reader[((int)VwAspnetApplicationsColumn.ApplicationId)];
			//entity.ApplicationId = (Convert.IsDBNull(reader["ApplicationId"]))?Guid.Empty:(System.Guid)reader["ApplicationId"];
			entity.Description = (reader.IsDBNull(((int)VwAspnetApplicationsColumn.Description)))?null:(System.String)reader[((int)VwAspnetApplicationsColumn.Description)];
			//entity.Description = (Convert.IsDBNull(reader["Description"]))?string.Empty:(System.String)reader["Description"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="VwAspnetApplications"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="VwAspnetApplications"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, VwAspnetApplications entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.ApplicationName = (Convert.IsDBNull(dataRow["ApplicationName"]))?string.Empty:(System.String)dataRow["ApplicationName"];
			entity.LoweredApplicationName = (Convert.IsDBNull(dataRow["LoweredApplicationName"]))?string.Empty:(System.String)dataRow["LoweredApplicationName"];
			entity.ApplicationId = (Convert.IsDBNull(dataRow["ApplicationId"]))?Guid.Empty:(System.Guid)dataRow["ApplicationId"];
			entity.Description = (Convert.IsDBNull(dataRow["Description"]))?string.Empty:(System.String)dataRow["Description"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region VwAspnetApplicationsFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="VwAspnetApplications"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class VwAspnetApplicationsFilterBuilder : SqlFilterBuilder<VwAspnetApplicationsColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the VwAspnetApplicationsFilterBuilder class.
		/// </summary>
		public VwAspnetApplicationsFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the VwAspnetApplicationsFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public VwAspnetApplicationsFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the VwAspnetApplicationsFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public VwAspnetApplicationsFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion VwAspnetApplicationsFilterBuilder

	#region VwAspnetApplicationsParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="VwAspnetApplications"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class VwAspnetApplicationsParameterBuilder : ParameterizedSqlFilterBuilder<VwAspnetApplicationsColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the VwAspnetApplicationsParameterBuilder class.
		/// </summary>
		public VwAspnetApplicationsParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the VwAspnetApplicationsParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public VwAspnetApplicationsParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the VwAspnetApplicationsParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public VwAspnetApplicationsParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion VwAspnetApplicationsParameterBuilder
	
	#region VwAspnetApplicationsSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="VwAspnetApplications"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class VwAspnetApplicationsSortBuilder : SqlSortBuilder<VwAspnetApplicationsColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the VwAspnetApplicationsSqlSortBuilder class.
		/// </summary>
		public VwAspnetApplicationsSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion VwAspnetApplicationsSortBuilder

} // end namespace
