﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="VwAspnetRolesProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class VwAspnetRolesProviderBaseCore : EntityViewProviderBase<VwAspnetRoles>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;VwAspnetRoles&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;VwAspnetRoles&gt;"/></returns>
		protected static VList&lt;VwAspnetRoles&gt; Fill(DataSet dataSet, VList<VwAspnetRoles> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<VwAspnetRoles>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;VwAspnetRoles&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<VwAspnetRoles>"/></returns>
		protected static VList&lt;VwAspnetRoles&gt; Fill(DataTable dataTable, VList<VwAspnetRoles> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					VwAspnetRoles c = new VwAspnetRoles();
					c.ApplicationId = (Convert.IsDBNull(row["ApplicationId"]))?Guid.Empty:(System.Guid)row["ApplicationId"];
					c.RoleId = (Convert.IsDBNull(row["RoleId"]))?Guid.Empty:(System.Guid)row["RoleId"];
					c.RoleName = (Convert.IsDBNull(row["RoleName"]))?string.Empty:(System.String)row["RoleName"];
					c.LoweredRoleName = (Convert.IsDBNull(row["LoweredRoleName"]))?string.Empty:(System.String)row["LoweredRoleName"];
					c.Description = (Convert.IsDBNull(row["Description"]))?string.Empty:(System.String)row["Description"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;VwAspnetRoles&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;VwAspnetRoles&gt;"/></returns>
		protected VList<VwAspnetRoles> Fill(IDataReader reader, VList<VwAspnetRoles> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					VwAspnetRoles entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<VwAspnetRoles>("VwAspnetRoles",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new VwAspnetRoles();
					}
					
					entity.SuppressEntityEvents = true;

					entity.ApplicationId = (System.Guid)reader[((int)VwAspnetRolesColumn.ApplicationId)];
					//entity.ApplicationId = (Convert.IsDBNull(reader["ApplicationId"]))?Guid.Empty:(System.Guid)reader["ApplicationId"];
					entity.RoleId = (System.Guid)reader[((int)VwAspnetRolesColumn.RoleId)];
					//entity.RoleId = (Convert.IsDBNull(reader["RoleId"]))?Guid.Empty:(System.Guid)reader["RoleId"];
					entity.RoleName = (System.String)reader[((int)VwAspnetRolesColumn.RoleName)];
					//entity.RoleName = (Convert.IsDBNull(reader["RoleName"]))?string.Empty:(System.String)reader["RoleName"];
					entity.LoweredRoleName = (System.String)reader[((int)VwAspnetRolesColumn.LoweredRoleName)];
					//entity.LoweredRoleName = (Convert.IsDBNull(reader["LoweredRoleName"]))?string.Empty:(System.String)reader["LoweredRoleName"];
					entity.Description = (reader.IsDBNull(((int)VwAspnetRolesColumn.Description)))?null:(System.String)reader[((int)VwAspnetRolesColumn.Description)];
					//entity.Description = (Convert.IsDBNull(reader["Description"]))?string.Empty:(System.String)reader["Description"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="VwAspnetRoles"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="VwAspnetRoles"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, VwAspnetRoles entity)
		{
			reader.Read();
			entity.ApplicationId = (System.Guid)reader[((int)VwAspnetRolesColumn.ApplicationId)];
			//entity.ApplicationId = (Convert.IsDBNull(reader["ApplicationId"]))?Guid.Empty:(System.Guid)reader["ApplicationId"];
			entity.RoleId = (System.Guid)reader[((int)VwAspnetRolesColumn.RoleId)];
			//entity.RoleId = (Convert.IsDBNull(reader["RoleId"]))?Guid.Empty:(System.Guid)reader["RoleId"];
			entity.RoleName = (System.String)reader[((int)VwAspnetRolesColumn.RoleName)];
			//entity.RoleName = (Convert.IsDBNull(reader["RoleName"]))?string.Empty:(System.String)reader["RoleName"];
			entity.LoweredRoleName = (System.String)reader[((int)VwAspnetRolesColumn.LoweredRoleName)];
			//entity.LoweredRoleName = (Convert.IsDBNull(reader["LoweredRoleName"]))?string.Empty:(System.String)reader["LoweredRoleName"];
			entity.Description = (reader.IsDBNull(((int)VwAspnetRolesColumn.Description)))?null:(System.String)reader[((int)VwAspnetRolesColumn.Description)];
			//entity.Description = (Convert.IsDBNull(reader["Description"]))?string.Empty:(System.String)reader["Description"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="VwAspnetRoles"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="VwAspnetRoles"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, VwAspnetRoles entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.ApplicationId = (Convert.IsDBNull(dataRow["ApplicationId"]))?Guid.Empty:(System.Guid)dataRow["ApplicationId"];
			entity.RoleId = (Convert.IsDBNull(dataRow["RoleId"]))?Guid.Empty:(System.Guid)dataRow["RoleId"];
			entity.RoleName = (Convert.IsDBNull(dataRow["RoleName"]))?string.Empty:(System.String)dataRow["RoleName"];
			entity.LoweredRoleName = (Convert.IsDBNull(dataRow["LoweredRoleName"]))?string.Empty:(System.String)dataRow["LoweredRoleName"];
			entity.Description = (Convert.IsDBNull(dataRow["Description"]))?string.Empty:(System.String)dataRow["Description"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region VwAspnetRolesFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="VwAspnetRoles"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class VwAspnetRolesFilterBuilder : SqlFilterBuilder<VwAspnetRolesColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the VwAspnetRolesFilterBuilder class.
		/// </summary>
		public VwAspnetRolesFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the VwAspnetRolesFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public VwAspnetRolesFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the VwAspnetRolesFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public VwAspnetRolesFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion VwAspnetRolesFilterBuilder

	#region VwAspnetRolesParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="VwAspnetRoles"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class VwAspnetRolesParameterBuilder : ParameterizedSqlFilterBuilder<VwAspnetRolesColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the VwAspnetRolesParameterBuilder class.
		/// </summary>
		public VwAspnetRolesParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the VwAspnetRolesParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public VwAspnetRolesParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the VwAspnetRolesParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public VwAspnetRolesParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion VwAspnetRolesParameterBuilder
	
	#region VwAspnetRolesSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="VwAspnetRoles"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class VwAspnetRolesSortBuilder : SqlSortBuilder<VwAspnetRolesColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the VwAspnetRolesSqlSortBuilder class.
		/// </summary>
		public VwAspnetRolesSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion VwAspnetRolesSortBuilder

} // end namespace
