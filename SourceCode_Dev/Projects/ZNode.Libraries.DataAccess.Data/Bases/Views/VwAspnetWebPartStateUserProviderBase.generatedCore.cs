﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="VwAspnetWebPartStateUserProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class VwAspnetWebPartStateUserProviderBaseCore : EntityViewProviderBase<VwAspnetWebPartStateUser>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;VwAspnetWebPartStateUser&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;VwAspnetWebPartStateUser&gt;"/></returns>
		protected static VList&lt;VwAspnetWebPartStateUser&gt; Fill(DataSet dataSet, VList<VwAspnetWebPartStateUser> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<VwAspnetWebPartStateUser>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;VwAspnetWebPartStateUser&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<VwAspnetWebPartStateUser>"/></returns>
		protected static VList&lt;VwAspnetWebPartStateUser&gt; Fill(DataTable dataTable, VList<VwAspnetWebPartStateUser> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					VwAspnetWebPartStateUser c = new VwAspnetWebPartStateUser();
					c.PathId = (Convert.IsDBNull(row["PathId"]))?Guid.Empty:(System.Guid?)row["PathId"];
					c.UserId = (Convert.IsDBNull(row["UserId"]))?Guid.Empty:(System.Guid?)row["UserId"];
					c.DataSize = (Convert.IsDBNull(row["DataSize"]))?(int)0:(System.Int32?)row["DataSize"];
					c.LastUpdatedDate = (Convert.IsDBNull(row["LastUpdatedDate"]))?DateTime.MinValue:(System.DateTime)row["LastUpdatedDate"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;VwAspnetWebPartStateUser&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;VwAspnetWebPartStateUser&gt;"/></returns>
		protected VList<VwAspnetWebPartStateUser> Fill(IDataReader reader, VList<VwAspnetWebPartStateUser> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					VwAspnetWebPartStateUser entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<VwAspnetWebPartStateUser>("VwAspnetWebPartStateUser",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new VwAspnetWebPartStateUser();
					}
					
					entity.SuppressEntityEvents = true;

					entity.PathId = (reader.IsDBNull(((int)VwAspnetWebPartStateUserColumn.PathId)))?null:(System.Guid?)reader[((int)VwAspnetWebPartStateUserColumn.PathId)];
					//entity.PathId = (Convert.IsDBNull(reader["PathId"]))?Guid.Empty:(System.Guid?)reader["PathId"];
					entity.UserId = (reader.IsDBNull(((int)VwAspnetWebPartStateUserColumn.UserId)))?null:(System.Guid?)reader[((int)VwAspnetWebPartStateUserColumn.UserId)];
					//entity.UserId = (Convert.IsDBNull(reader["UserId"]))?Guid.Empty:(System.Guid?)reader["UserId"];
					entity.DataSize = (reader.IsDBNull(((int)VwAspnetWebPartStateUserColumn.DataSize)))?null:(System.Int32?)reader[((int)VwAspnetWebPartStateUserColumn.DataSize)];
					//entity.DataSize = (Convert.IsDBNull(reader["DataSize"]))?(int)0:(System.Int32?)reader["DataSize"];
					entity.LastUpdatedDate = (System.DateTime)reader[((int)VwAspnetWebPartStateUserColumn.LastUpdatedDate)];
					//entity.LastUpdatedDate = (Convert.IsDBNull(reader["LastUpdatedDate"]))?DateTime.MinValue:(System.DateTime)reader["LastUpdatedDate"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="VwAspnetWebPartStateUser"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="VwAspnetWebPartStateUser"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, VwAspnetWebPartStateUser entity)
		{
			reader.Read();
			entity.PathId = (reader.IsDBNull(((int)VwAspnetWebPartStateUserColumn.PathId)))?null:(System.Guid?)reader[((int)VwAspnetWebPartStateUserColumn.PathId)];
			//entity.PathId = (Convert.IsDBNull(reader["PathId"]))?Guid.Empty:(System.Guid?)reader["PathId"];
			entity.UserId = (reader.IsDBNull(((int)VwAspnetWebPartStateUserColumn.UserId)))?null:(System.Guid?)reader[((int)VwAspnetWebPartStateUserColumn.UserId)];
			//entity.UserId = (Convert.IsDBNull(reader["UserId"]))?Guid.Empty:(System.Guid?)reader["UserId"];
			entity.DataSize = (reader.IsDBNull(((int)VwAspnetWebPartStateUserColumn.DataSize)))?null:(System.Int32?)reader[((int)VwAspnetWebPartStateUserColumn.DataSize)];
			//entity.DataSize = (Convert.IsDBNull(reader["DataSize"]))?(int)0:(System.Int32?)reader["DataSize"];
			entity.LastUpdatedDate = (System.DateTime)reader[((int)VwAspnetWebPartStateUserColumn.LastUpdatedDate)];
			//entity.LastUpdatedDate = (Convert.IsDBNull(reader["LastUpdatedDate"]))?DateTime.MinValue:(System.DateTime)reader["LastUpdatedDate"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="VwAspnetWebPartStateUser"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="VwAspnetWebPartStateUser"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, VwAspnetWebPartStateUser entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.PathId = (Convert.IsDBNull(dataRow["PathId"]))?Guid.Empty:(System.Guid?)dataRow["PathId"];
			entity.UserId = (Convert.IsDBNull(dataRow["UserId"]))?Guid.Empty:(System.Guid?)dataRow["UserId"];
			entity.DataSize = (Convert.IsDBNull(dataRow["DataSize"]))?(int)0:(System.Int32?)dataRow["DataSize"];
			entity.LastUpdatedDate = (Convert.IsDBNull(dataRow["LastUpdatedDate"]))?DateTime.MinValue:(System.DateTime)dataRow["LastUpdatedDate"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region VwAspnetWebPartStateUserFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="VwAspnetWebPartStateUser"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class VwAspnetWebPartStateUserFilterBuilder : SqlFilterBuilder<VwAspnetWebPartStateUserColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the VwAspnetWebPartStateUserFilterBuilder class.
		/// </summary>
		public VwAspnetWebPartStateUserFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the VwAspnetWebPartStateUserFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public VwAspnetWebPartStateUserFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the VwAspnetWebPartStateUserFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public VwAspnetWebPartStateUserFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion VwAspnetWebPartStateUserFilterBuilder

	#region VwAspnetWebPartStateUserParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="VwAspnetWebPartStateUser"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class VwAspnetWebPartStateUserParameterBuilder : ParameterizedSqlFilterBuilder<VwAspnetWebPartStateUserColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the VwAspnetWebPartStateUserParameterBuilder class.
		/// </summary>
		public VwAspnetWebPartStateUserParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the VwAspnetWebPartStateUserParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public VwAspnetWebPartStateUserParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the VwAspnetWebPartStateUserParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public VwAspnetWebPartStateUserParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion VwAspnetWebPartStateUserParameterBuilder
	
	#region VwAspnetWebPartStateUserSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="VwAspnetWebPartStateUser"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class VwAspnetWebPartStateUserSortBuilder : SqlSortBuilder<VwAspnetWebPartStateUserColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the VwAspnetWebPartStateUserSqlSortBuilder class.
		/// </summary>
		public VwAspnetWebPartStateUserSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion VwAspnetWebPartStateUserSortBuilder

} // end namespace
