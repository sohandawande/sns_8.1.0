﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="VwAspnetProfilesProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class VwAspnetProfilesProviderBaseCore : EntityViewProviderBase<VwAspnetProfiles>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;VwAspnetProfiles&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;VwAspnetProfiles&gt;"/></returns>
		protected static VList&lt;VwAspnetProfiles&gt; Fill(DataSet dataSet, VList<VwAspnetProfiles> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<VwAspnetProfiles>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;VwAspnetProfiles&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<VwAspnetProfiles>"/></returns>
		protected static VList&lt;VwAspnetProfiles&gt; Fill(DataTable dataTable, VList<VwAspnetProfiles> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					VwAspnetProfiles c = new VwAspnetProfiles();
					c.UserId = (Convert.IsDBNull(row["UserId"]))?Guid.Empty:(System.Guid)row["UserId"];
					c.LastUpdatedDate = (Convert.IsDBNull(row["LastUpdatedDate"]))?DateTime.MinValue:(System.DateTime)row["LastUpdatedDate"];
					c.DataSize = (Convert.IsDBNull(row["DataSize"]))?(int)0:(System.Int32?)row["DataSize"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;VwAspnetProfiles&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;VwAspnetProfiles&gt;"/></returns>
		protected VList<VwAspnetProfiles> Fill(IDataReader reader, VList<VwAspnetProfiles> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					VwAspnetProfiles entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<VwAspnetProfiles>("VwAspnetProfiles",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new VwAspnetProfiles();
					}
					
					entity.SuppressEntityEvents = true;

					entity.UserId = (System.Guid)reader[((int)VwAspnetProfilesColumn.UserId)];
					//entity.UserId = (Convert.IsDBNull(reader["UserId"]))?Guid.Empty:(System.Guid)reader["UserId"];
					entity.LastUpdatedDate = (System.DateTime)reader[((int)VwAspnetProfilesColumn.LastUpdatedDate)];
					//entity.LastUpdatedDate = (Convert.IsDBNull(reader["LastUpdatedDate"]))?DateTime.MinValue:(System.DateTime)reader["LastUpdatedDate"];
					entity.DataSize = (reader.IsDBNull(((int)VwAspnetProfilesColumn.DataSize)))?null:(System.Int32?)reader[((int)VwAspnetProfilesColumn.DataSize)];
					//entity.DataSize = (Convert.IsDBNull(reader["DataSize"]))?(int)0:(System.Int32?)reader["DataSize"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="VwAspnetProfiles"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="VwAspnetProfiles"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, VwAspnetProfiles entity)
		{
			reader.Read();
			entity.UserId = (System.Guid)reader[((int)VwAspnetProfilesColumn.UserId)];
			//entity.UserId = (Convert.IsDBNull(reader["UserId"]))?Guid.Empty:(System.Guid)reader["UserId"];
			entity.LastUpdatedDate = (System.DateTime)reader[((int)VwAspnetProfilesColumn.LastUpdatedDate)];
			//entity.LastUpdatedDate = (Convert.IsDBNull(reader["LastUpdatedDate"]))?DateTime.MinValue:(System.DateTime)reader["LastUpdatedDate"];
			entity.DataSize = (reader.IsDBNull(((int)VwAspnetProfilesColumn.DataSize)))?null:(System.Int32?)reader[((int)VwAspnetProfilesColumn.DataSize)];
			//entity.DataSize = (Convert.IsDBNull(reader["DataSize"]))?(int)0:(System.Int32?)reader["DataSize"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="VwAspnetProfiles"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="VwAspnetProfiles"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, VwAspnetProfiles entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.UserId = (Convert.IsDBNull(dataRow["UserId"]))?Guid.Empty:(System.Guid)dataRow["UserId"];
			entity.LastUpdatedDate = (Convert.IsDBNull(dataRow["LastUpdatedDate"]))?DateTime.MinValue:(System.DateTime)dataRow["LastUpdatedDate"];
			entity.DataSize = (Convert.IsDBNull(dataRow["DataSize"]))?(int)0:(System.Int32?)dataRow["DataSize"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region VwAspnetProfilesFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="VwAspnetProfiles"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class VwAspnetProfilesFilterBuilder : SqlFilterBuilder<VwAspnetProfilesColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the VwAspnetProfilesFilterBuilder class.
		/// </summary>
		public VwAspnetProfilesFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the VwAspnetProfilesFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public VwAspnetProfilesFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the VwAspnetProfilesFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public VwAspnetProfilesFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion VwAspnetProfilesFilterBuilder

	#region VwAspnetProfilesParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="VwAspnetProfiles"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class VwAspnetProfilesParameterBuilder : ParameterizedSqlFilterBuilder<VwAspnetProfilesColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the VwAspnetProfilesParameterBuilder class.
		/// </summary>
		public VwAspnetProfilesParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the VwAspnetProfilesParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public VwAspnetProfilesParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the VwAspnetProfilesParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public VwAspnetProfilesParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion VwAspnetProfilesParameterBuilder
	
	#region VwAspnetProfilesSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="VwAspnetProfiles"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class VwAspnetProfilesSortBuilder : SqlSortBuilder<VwAspnetProfilesColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the VwAspnetProfilesSqlSortBuilder class.
		/// </summary>
		public VwAspnetProfilesSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion VwAspnetProfilesSortBuilder

} // end namespace
