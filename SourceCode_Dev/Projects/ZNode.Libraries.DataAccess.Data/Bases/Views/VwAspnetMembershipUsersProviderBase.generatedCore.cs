﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="VwAspnetMembershipUsersProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class VwAspnetMembershipUsersProviderBaseCore : EntityViewProviderBase<VwAspnetMembershipUsers>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;VwAspnetMembershipUsers&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;VwAspnetMembershipUsers&gt;"/></returns>
		protected static VList&lt;VwAspnetMembershipUsers&gt; Fill(DataSet dataSet, VList<VwAspnetMembershipUsers> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<VwAspnetMembershipUsers>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;VwAspnetMembershipUsers&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<VwAspnetMembershipUsers>"/></returns>
		protected static VList&lt;VwAspnetMembershipUsers&gt; Fill(DataTable dataTable, VList<VwAspnetMembershipUsers> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					VwAspnetMembershipUsers c = new VwAspnetMembershipUsers();
					c.UserId = (Convert.IsDBNull(row["UserId"]))?Guid.Empty:(System.Guid)row["UserId"];
					c.PasswordFormat = (Convert.IsDBNull(row["PasswordFormat"]))?(int)0:(System.Int32)row["PasswordFormat"];
					c.MobilePIN = (Convert.IsDBNull(row["MobilePIN"]))?string.Empty:(System.String)row["MobilePIN"];
					c.Email = (Convert.IsDBNull(row["Email"]))?string.Empty:(System.String)row["Email"];
					c.LoweredEmail = (Convert.IsDBNull(row["LoweredEmail"]))?string.Empty:(System.String)row["LoweredEmail"];
					c.PasswordQuestion = (Convert.IsDBNull(row["PasswordQuestion"]))?string.Empty:(System.String)row["PasswordQuestion"];
					c.PasswordAnswer = (Convert.IsDBNull(row["PasswordAnswer"]))?string.Empty:(System.String)row["PasswordAnswer"];
					c.IsApproved = (Convert.IsDBNull(row["IsApproved"]))?false:(System.Boolean)row["IsApproved"];
					c.IsLockedOut = (Convert.IsDBNull(row["IsLockedOut"]))?false:(System.Boolean)row["IsLockedOut"];
					c.CreateDate = (Convert.IsDBNull(row["CreateDate"]))?DateTime.MinValue:(System.DateTime)row["CreateDate"];
					c.LastLoginDate = (Convert.IsDBNull(row["LastLoginDate"]))?DateTime.MinValue:(System.DateTime)row["LastLoginDate"];
					c.LastPasswordChangedDate = (Convert.IsDBNull(row["LastPasswordChangedDate"]))?DateTime.MinValue:(System.DateTime)row["LastPasswordChangedDate"];
					c.LastLockoutDate = (Convert.IsDBNull(row["LastLockoutDate"]))?DateTime.MinValue:(System.DateTime)row["LastLockoutDate"];
					c.FailedPasswordAttemptCount = (Convert.IsDBNull(row["FailedPasswordAttemptCount"]))?(int)0:(System.Int32)row["FailedPasswordAttemptCount"];
					c.FailedPasswordAttemptWindowStart = (Convert.IsDBNull(row["FailedPasswordAttemptWindowStart"]))?DateTime.MinValue:(System.DateTime)row["FailedPasswordAttemptWindowStart"];
					c.FailedPasswordAnswerAttemptCount = (Convert.IsDBNull(row["FailedPasswordAnswerAttemptCount"]))?(int)0:(System.Int32)row["FailedPasswordAnswerAttemptCount"];
					c.FailedPasswordAnswerAttemptWindowStart = (Convert.IsDBNull(row["FailedPasswordAnswerAttemptWindowStart"]))?DateTime.MinValue:(System.DateTime)row["FailedPasswordAnswerAttemptWindowStart"];
					c.Comment = (Convert.IsDBNull(row["Comment"]))?string.Empty:(System.String)row["Comment"];
					c.ApplicationId = (Convert.IsDBNull(row["ApplicationId"]))?Guid.Empty:(System.Guid)row["ApplicationId"];
					c.UserName = (Convert.IsDBNull(row["UserName"]))?string.Empty:(System.String)row["UserName"];
					c.MobileAlias = (Convert.IsDBNull(row["MobileAlias"]))?string.Empty:(System.String)row["MobileAlias"];
					c.IsAnonymous = (Convert.IsDBNull(row["IsAnonymous"]))?false:(System.Boolean)row["IsAnonymous"];
					c.LastActivityDate = (Convert.IsDBNull(row["LastActivityDate"]))?DateTime.MinValue:(System.DateTime)row["LastActivityDate"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;VwAspnetMembershipUsers&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;VwAspnetMembershipUsers&gt;"/></returns>
		protected VList<VwAspnetMembershipUsers> Fill(IDataReader reader, VList<VwAspnetMembershipUsers> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					VwAspnetMembershipUsers entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<VwAspnetMembershipUsers>("VwAspnetMembershipUsers",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new VwAspnetMembershipUsers();
					}
					
					entity.SuppressEntityEvents = true;

					entity.UserId = (System.Guid)reader[((int)VwAspnetMembershipUsersColumn.UserId)];
					//entity.UserId = (Convert.IsDBNull(reader["UserId"]))?Guid.Empty:(System.Guid)reader["UserId"];
					entity.PasswordFormat = (System.Int32)reader[((int)VwAspnetMembershipUsersColumn.PasswordFormat)];
					//entity.PasswordFormat = (Convert.IsDBNull(reader["PasswordFormat"]))?(int)0:(System.Int32)reader["PasswordFormat"];
					entity.MobilePIN = (reader.IsDBNull(((int)VwAspnetMembershipUsersColumn.MobilePIN)))?null:(System.String)reader[((int)VwAspnetMembershipUsersColumn.MobilePIN)];
					//entity.MobilePIN = (Convert.IsDBNull(reader["MobilePIN"]))?string.Empty:(System.String)reader["MobilePIN"];
					entity.Email = (reader.IsDBNull(((int)VwAspnetMembershipUsersColumn.Email)))?null:(System.String)reader[((int)VwAspnetMembershipUsersColumn.Email)];
					//entity.Email = (Convert.IsDBNull(reader["Email"]))?string.Empty:(System.String)reader["Email"];
					entity.LoweredEmail = (reader.IsDBNull(((int)VwAspnetMembershipUsersColumn.LoweredEmail)))?null:(System.String)reader[((int)VwAspnetMembershipUsersColumn.LoweredEmail)];
					//entity.LoweredEmail = (Convert.IsDBNull(reader["LoweredEmail"]))?string.Empty:(System.String)reader["LoweredEmail"];
					entity.PasswordQuestion = (reader.IsDBNull(((int)VwAspnetMembershipUsersColumn.PasswordQuestion)))?null:(System.String)reader[((int)VwAspnetMembershipUsersColumn.PasswordQuestion)];
					//entity.PasswordQuestion = (Convert.IsDBNull(reader["PasswordQuestion"]))?string.Empty:(System.String)reader["PasswordQuestion"];
					entity.PasswordAnswer = (reader.IsDBNull(((int)VwAspnetMembershipUsersColumn.PasswordAnswer)))?null:(System.String)reader[((int)VwAspnetMembershipUsersColumn.PasswordAnswer)];
					//entity.PasswordAnswer = (Convert.IsDBNull(reader["PasswordAnswer"]))?string.Empty:(System.String)reader["PasswordAnswer"];
					entity.IsApproved = (System.Boolean)reader[((int)VwAspnetMembershipUsersColumn.IsApproved)];
					//entity.IsApproved = (Convert.IsDBNull(reader["IsApproved"]))?false:(System.Boolean)reader["IsApproved"];
					entity.IsLockedOut = (System.Boolean)reader[((int)VwAspnetMembershipUsersColumn.IsLockedOut)];
					//entity.IsLockedOut = (Convert.IsDBNull(reader["IsLockedOut"]))?false:(System.Boolean)reader["IsLockedOut"];
					entity.CreateDate = (System.DateTime)reader[((int)VwAspnetMembershipUsersColumn.CreateDate)];
					//entity.CreateDate = (Convert.IsDBNull(reader["CreateDate"]))?DateTime.MinValue:(System.DateTime)reader["CreateDate"];
					entity.LastLoginDate = (System.DateTime)reader[((int)VwAspnetMembershipUsersColumn.LastLoginDate)];
					//entity.LastLoginDate = (Convert.IsDBNull(reader["LastLoginDate"]))?DateTime.MinValue:(System.DateTime)reader["LastLoginDate"];
					entity.LastPasswordChangedDate = (System.DateTime)reader[((int)VwAspnetMembershipUsersColumn.LastPasswordChangedDate)];
					//entity.LastPasswordChangedDate = (Convert.IsDBNull(reader["LastPasswordChangedDate"]))?DateTime.MinValue:(System.DateTime)reader["LastPasswordChangedDate"];
					entity.LastLockoutDate = (System.DateTime)reader[((int)VwAspnetMembershipUsersColumn.LastLockoutDate)];
					//entity.LastLockoutDate = (Convert.IsDBNull(reader["LastLockoutDate"]))?DateTime.MinValue:(System.DateTime)reader["LastLockoutDate"];
					entity.FailedPasswordAttemptCount = (System.Int32)reader[((int)VwAspnetMembershipUsersColumn.FailedPasswordAttemptCount)];
					//entity.FailedPasswordAttemptCount = (Convert.IsDBNull(reader["FailedPasswordAttemptCount"]))?(int)0:(System.Int32)reader["FailedPasswordAttemptCount"];
					entity.FailedPasswordAttemptWindowStart = (System.DateTime)reader[((int)VwAspnetMembershipUsersColumn.FailedPasswordAttemptWindowStart)];
					//entity.FailedPasswordAttemptWindowStart = (Convert.IsDBNull(reader["FailedPasswordAttemptWindowStart"]))?DateTime.MinValue:(System.DateTime)reader["FailedPasswordAttemptWindowStart"];
					entity.FailedPasswordAnswerAttemptCount = (System.Int32)reader[((int)VwAspnetMembershipUsersColumn.FailedPasswordAnswerAttemptCount)];
					//entity.FailedPasswordAnswerAttemptCount = (Convert.IsDBNull(reader["FailedPasswordAnswerAttemptCount"]))?(int)0:(System.Int32)reader["FailedPasswordAnswerAttemptCount"];
					entity.FailedPasswordAnswerAttemptWindowStart = (System.DateTime)reader[((int)VwAspnetMembershipUsersColumn.FailedPasswordAnswerAttemptWindowStart)];
					//entity.FailedPasswordAnswerAttemptWindowStart = (Convert.IsDBNull(reader["FailedPasswordAnswerAttemptWindowStart"]))?DateTime.MinValue:(System.DateTime)reader["FailedPasswordAnswerAttemptWindowStart"];
					entity.Comment = (reader.IsDBNull(((int)VwAspnetMembershipUsersColumn.Comment)))?null:(System.String)reader[((int)VwAspnetMembershipUsersColumn.Comment)];
					//entity.Comment = (Convert.IsDBNull(reader["Comment"]))?string.Empty:(System.String)reader["Comment"];
					entity.ApplicationId = (System.Guid)reader[((int)VwAspnetMembershipUsersColumn.ApplicationId)];
					//entity.ApplicationId = (Convert.IsDBNull(reader["ApplicationId"]))?Guid.Empty:(System.Guid)reader["ApplicationId"];
					entity.UserName = (System.String)reader[((int)VwAspnetMembershipUsersColumn.UserName)];
					//entity.UserName = (Convert.IsDBNull(reader["UserName"]))?string.Empty:(System.String)reader["UserName"];
					entity.MobileAlias = (reader.IsDBNull(((int)VwAspnetMembershipUsersColumn.MobileAlias)))?null:(System.String)reader[((int)VwAspnetMembershipUsersColumn.MobileAlias)];
					//entity.MobileAlias = (Convert.IsDBNull(reader["MobileAlias"]))?string.Empty:(System.String)reader["MobileAlias"];
					entity.IsAnonymous = (System.Boolean)reader[((int)VwAspnetMembershipUsersColumn.IsAnonymous)];
					//entity.IsAnonymous = (Convert.IsDBNull(reader["IsAnonymous"]))?false:(System.Boolean)reader["IsAnonymous"];
					entity.LastActivityDate = (System.DateTime)reader[((int)VwAspnetMembershipUsersColumn.LastActivityDate)];
					//entity.LastActivityDate = (Convert.IsDBNull(reader["LastActivityDate"]))?DateTime.MinValue:(System.DateTime)reader["LastActivityDate"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="VwAspnetMembershipUsers"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="VwAspnetMembershipUsers"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, VwAspnetMembershipUsers entity)
		{
			reader.Read();
			entity.UserId = (System.Guid)reader[((int)VwAspnetMembershipUsersColumn.UserId)];
			//entity.UserId = (Convert.IsDBNull(reader["UserId"]))?Guid.Empty:(System.Guid)reader["UserId"];
			entity.PasswordFormat = (System.Int32)reader[((int)VwAspnetMembershipUsersColumn.PasswordFormat)];
			//entity.PasswordFormat = (Convert.IsDBNull(reader["PasswordFormat"]))?(int)0:(System.Int32)reader["PasswordFormat"];
			entity.MobilePIN = (reader.IsDBNull(((int)VwAspnetMembershipUsersColumn.MobilePIN)))?null:(System.String)reader[((int)VwAspnetMembershipUsersColumn.MobilePIN)];
			//entity.MobilePIN = (Convert.IsDBNull(reader["MobilePIN"]))?string.Empty:(System.String)reader["MobilePIN"];
			entity.Email = (reader.IsDBNull(((int)VwAspnetMembershipUsersColumn.Email)))?null:(System.String)reader[((int)VwAspnetMembershipUsersColumn.Email)];
			//entity.Email = (Convert.IsDBNull(reader["Email"]))?string.Empty:(System.String)reader["Email"];
			entity.LoweredEmail = (reader.IsDBNull(((int)VwAspnetMembershipUsersColumn.LoweredEmail)))?null:(System.String)reader[((int)VwAspnetMembershipUsersColumn.LoweredEmail)];
			//entity.LoweredEmail = (Convert.IsDBNull(reader["LoweredEmail"]))?string.Empty:(System.String)reader["LoweredEmail"];
			entity.PasswordQuestion = (reader.IsDBNull(((int)VwAspnetMembershipUsersColumn.PasswordQuestion)))?null:(System.String)reader[((int)VwAspnetMembershipUsersColumn.PasswordQuestion)];
			//entity.PasswordQuestion = (Convert.IsDBNull(reader["PasswordQuestion"]))?string.Empty:(System.String)reader["PasswordQuestion"];
			entity.PasswordAnswer = (reader.IsDBNull(((int)VwAspnetMembershipUsersColumn.PasswordAnswer)))?null:(System.String)reader[((int)VwAspnetMembershipUsersColumn.PasswordAnswer)];
			//entity.PasswordAnswer = (Convert.IsDBNull(reader["PasswordAnswer"]))?string.Empty:(System.String)reader["PasswordAnswer"];
			entity.IsApproved = (System.Boolean)reader[((int)VwAspnetMembershipUsersColumn.IsApproved)];
			//entity.IsApproved = (Convert.IsDBNull(reader["IsApproved"]))?false:(System.Boolean)reader["IsApproved"];
			entity.IsLockedOut = (System.Boolean)reader[((int)VwAspnetMembershipUsersColumn.IsLockedOut)];
			//entity.IsLockedOut = (Convert.IsDBNull(reader["IsLockedOut"]))?false:(System.Boolean)reader["IsLockedOut"];
			entity.CreateDate = (System.DateTime)reader[((int)VwAspnetMembershipUsersColumn.CreateDate)];
			//entity.CreateDate = (Convert.IsDBNull(reader["CreateDate"]))?DateTime.MinValue:(System.DateTime)reader["CreateDate"];
			entity.LastLoginDate = (System.DateTime)reader[((int)VwAspnetMembershipUsersColumn.LastLoginDate)];
			//entity.LastLoginDate = (Convert.IsDBNull(reader["LastLoginDate"]))?DateTime.MinValue:(System.DateTime)reader["LastLoginDate"];
			entity.LastPasswordChangedDate = (System.DateTime)reader[((int)VwAspnetMembershipUsersColumn.LastPasswordChangedDate)];
			//entity.LastPasswordChangedDate = (Convert.IsDBNull(reader["LastPasswordChangedDate"]))?DateTime.MinValue:(System.DateTime)reader["LastPasswordChangedDate"];
			entity.LastLockoutDate = (System.DateTime)reader[((int)VwAspnetMembershipUsersColumn.LastLockoutDate)];
			//entity.LastLockoutDate = (Convert.IsDBNull(reader["LastLockoutDate"]))?DateTime.MinValue:(System.DateTime)reader["LastLockoutDate"];
			entity.FailedPasswordAttemptCount = (System.Int32)reader[((int)VwAspnetMembershipUsersColumn.FailedPasswordAttemptCount)];
			//entity.FailedPasswordAttemptCount = (Convert.IsDBNull(reader["FailedPasswordAttemptCount"]))?(int)0:(System.Int32)reader["FailedPasswordAttemptCount"];
			entity.FailedPasswordAttemptWindowStart = (System.DateTime)reader[((int)VwAspnetMembershipUsersColumn.FailedPasswordAttemptWindowStart)];
			//entity.FailedPasswordAttemptWindowStart = (Convert.IsDBNull(reader["FailedPasswordAttemptWindowStart"]))?DateTime.MinValue:(System.DateTime)reader["FailedPasswordAttemptWindowStart"];
			entity.FailedPasswordAnswerAttemptCount = (System.Int32)reader[((int)VwAspnetMembershipUsersColumn.FailedPasswordAnswerAttemptCount)];
			//entity.FailedPasswordAnswerAttemptCount = (Convert.IsDBNull(reader["FailedPasswordAnswerAttemptCount"]))?(int)0:(System.Int32)reader["FailedPasswordAnswerAttemptCount"];
			entity.FailedPasswordAnswerAttemptWindowStart = (System.DateTime)reader[((int)VwAspnetMembershipUsersColumn.FailedPasswordAnswerAttemptWindowStart)];
			//entity.FailedPasswordAnswerAttemptWindowStart = (Convert.IsDBNull(reader["FailedPasswordAnswerAttemptWindowStart"]))?DateTime.MinValue:(System.DateTime)reader["FailedPasswordAnswerAttemptWindowStart"];
			entity.Comment = (reader.IsDBNull(((int)VwAspnetMembershipUsersColumn.Comment)))?null:(System.String)reader[((int)VwAspnetMembershipUsersColumn.Comment)];
			//entity.Comment = (Convert.IsDBNull(reader["Comment"]))?string.Empty:(System.String)reader["Comment"];
			entity.ApplicationId = (System.Guid)reader[((int)VwAspnetMembershipUsersColumn.ApplicationId)];
			//entity.ApplicationId = (Convert.IsDBNull(reader["ApplicationId"]))?Guid.Empty:(System.Guid)reader["ApplicationId"];
			entity.UserName = (System.String)reader[((int)VwAspnetMembershipUsersColumn.UserName)];
			//entity.UserName = (Convert.IsDBNull(reader["UserName"]))?string.Empty:(System.String)reader["UserName"];
			entity.MobileAlias = (reader.IsDBNull(((int)VwAspnetMembershipUsersColumn.MobileAlias)))?null:(System.String)reader[((int)VwAspnetMembershipUsersColumn.MobileAlias)];
			//entity.MobileAlias = (Convert.IsDBNull(reader["MobileAlias"]))?string.Empty:(System.String)reader["MobileAlias"];
			entity.IsAnonymous = (System.Boolean)reader[((int)VwAspnetMembershipUsersColumn.IsAnonymous)];
			//entity.IsAnonymous = (Convert.IsDBNull(reader["IsAnonymous"]))?false:(System.Boolean)reader["IsAnonymous"];
			entity.LastActivityDate = (System.DateTime)reader[((int)VwAspnetMembershipUsersColumn.LastActivityDate)];
			//entity.LastActivityDate = (Convert.IsDBNull(reader["LastActivityDate"]))?DateTime.MinValue:(System.DateTime)reader["LastActivityDate"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="VwAspnetMembershipUsers"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="VwAspnetMembershipUsers"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, VwAspnetMembershipUsers entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.UserId = (Convert.IsDBNull(dataRow["UserId"]))?Guid.Empty:(System.Guid)dataRow["UserId"];
			entity.PasswordFormat = (Convert.IsDBNull(dataRow["PasswordFormat"]))?(int)0:(System.Int32)dataRow["PasswordFormat"];
			entity.MobilePIN = (Convert.IsDBNull(dataRow["MobilePIN"]))?string.Empty:(System.String)dataRow["MobilePIN"];
			entity.Email = (Convert.IsDBNull(dataRow["Email"]))?string.Empty:(System.String)dataRow["Email"];
			entity.LoweredEmail = (Convert.IsDBNull(dataRow["LoweredEmail"]))?string.Empty:(System.String)dataRow["LoweredEmail"];
			entity.PasswordQuestion = (Convert.IsDBNull(dataRow["PasswordQuestion"]))?string.Empty:(System.String)dataRow["PasswordQuestion"];
			entity.PasswordAnswer = (Convert.IsDBNull(dataRow["PasswordAnswer"]))?string.Empty:(System.String)dataRow["PasswordAnswer"];
			entity.IsApproved = (Convert.IsDBNull(dataRow["IsApproved"]))?false:(System.Boolean)dataRow["IsApproved"];
			entity.IsLockedOut = (Convert.IsDBNull(dataRow["IsLockedOut"]))?false:(System.Boolean)dataRow["IsLockedOut"];
			entity.CreateDate = (Convert.IsDBNull(dataRow["CreateDate"]))?DateTime.MinValue:(System.DateTime)dataRow["CreateDate"];
			entity.LastLoginDate = (Convert.IsDBNull(dataRow["LastLoginDate"]))?DateTime.MinValue:(System.DateTime)dataRow["LastLoginDate"];
			entity.LastPasswordChangedDate = (Convert.IsDBNull(dataRow["LastPasswordChangedDate"]))?DateTime.MinValue:(System.DateTime)dataRow["LastPasswordChangedDate"];
			entity.LastLockoutDate = (Convert.IsDBNull(dataRow["LastLockoutDate"]))?DateTime.MinValue:(System.DateTime)dataRow["LastLockoutDate"];
			entity.FailedPasswordAttemptCount = (Convert.IsDBNull(dataRow["FailedPasswordAttemptCount"]))?(int)0:(System.Int32)dataRow["FailedPasswordAttemptCount"];
			entity.FailedPasswordAttemptWindowStart = (Convert.IsDBNull(dataRow["FailedPasswordAttemptWindowStart"]))?DateTime.MinValue:(System.DateTime)dataRow["FailedPasswordAttemptWindowStart"];
			entity.FailedPasswordAnswerAttemptCount = (Convert.IsDBNull(dataRow["FailedPasswordAnswerAttemptCount"]))?(int)0:(System.Int32)dataRow["FailedPasswordAnswerAttemptCount"];
			entity.FailedPasswordAnswerAttemptWindowStart = (Convert.IsDBNull(dataRow["FailedPasswordAnswerAttemptWindowStart"]))?DateTime.MinValue:(System.DateTime)dataRow["FailedPasswordAnswerAttemptWindowStart"];
			entity.Comment = (Convert.IsDBNull(dataRow["Comment"]))?string.Empty:(System.String)dataRow["Comment"];
			entity.ApplicationId = (Convert.IsDBNull(dataRow["ApplicationId"]))?Guid.Empty:(System.Guid)dataRow["ApplicationId"];
			entity.UserName = (Convert.IsDBNull(dataRow["UserName"]))?string.Empty:(System.String)dataRow["UserName"];
			entity.MobileAlias = (Convert.IsDBNull(dataRow["MobileAlias"]))?string.Empty:(System.String)dataRow["MobileAlias"];
			entity.IsAnonymous = (Convert.IsDBNull(dataRow["IsAnonymous"]))?false:(System.Boolean)dataRow["IsAnonymous"];
			entity.LastActivityDate = (Convert.IsDBNull(dataRow["LastActivityDate"]))?DateTime.MinValue:(System.DateTime)dataRow["LastActivityDate"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region VwAspnetMembershipUsersFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="VwAspnetMembershipUsers"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class VwAspnetMembershipUsersFilterBuilder : SqlFilterBuilder<VwAspnetMembershipUsersColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the VwAspnetMembershipUsersFilterBuilder class.
		/// </summary>
		public VwAspnetMembershipUsersFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the VwAspnetMembershipUsersFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public VwAspnetMembershipUsersFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the VwAspnetMembershipUsersFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public VwAspnetMembershipUsersFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion VwAspnetMembershipUsersFilterBuilder

	#region VwAspnetMembershipUsersParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="VwAspnetMembershipUsers"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class VwAspnetMembershipUsersParameterBuilder : ParameterizedSqlFilterBuilder<VwAspnetMembershipUsersColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the VwAspnetMembershipUsersParameterBuilder class.
		/// </summary>
		public VwAspnetMembershipUsersParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the VwAspnetMembershipUsersParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public VwAspnetMembershipUsersParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the VwAspnetMembershipUsersParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public VwAspnetMembershipUsersParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion VwAspnetMembershipUsersParameterBuilder
	
	#region VwAspnetMembershipUsersSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="VwAspnetMembershipUsers"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class VwAspnetMembershipUsersSortBuilder : SqlSortBuilder<VwAspnetMembershipUsersColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the VwAspnetMembershipUsersSqlSortBuilder class.
		/// </summary>
		public VwAspnetMembershipUsersSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion VwAspnetMembershipUsersSortBuilder

} // end namespace
