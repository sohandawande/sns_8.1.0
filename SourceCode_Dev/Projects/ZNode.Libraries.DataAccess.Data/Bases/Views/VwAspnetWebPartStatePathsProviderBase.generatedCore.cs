﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="VwAspnetWebPartStatePathsProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class VwAspnetWebPartStatePathsProviderBaseCore : EntityViewProviderBase<VwAspnetWebPartStatePaths>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;VwAspnetWebPartStatePaths&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;VwAspnetWebPartStatePaths&gt;"/></returns>
		protected static VList&lt;VwAspnetWebPartStatePaths&gt; Fill(DataSet dataSet, VList<VwAspnetWebPartStatePaths> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<VwAspnetWebPartStatePaths>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;VwAspnetWebPartStatePaths&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<VwAspnetWebPartStatePaths>"/></returns>
		protected static VList&lt;VwAspnetWebPartStatePaths&gt; Fill(DataTable dataTable, VList<VwAspnetWebPartStatePaths> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					VwAspnetWebPartStatePaths c = new VwAspnetWebPartStatePaths();
					c.ApplicationId = (Convert.IsDBNull(row["ApplicationId"]))?Guid.Empty:(System.Guid)row["ApplicationId"];
					c.PathId = (Convert.IsDBNull(row["PathId"]))?Guid.Empty:(System.Guid)row["PathId"];
					c.Path = (Convert.IsDBNull(row["Path"]))?string.Empty:(System.String)row["Path"];
					c.LoweredPath = (Convert.IsDBNull(row["LoweredPath"]))?string.Empty:(System.String)row["LoweredPath"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;VwAspnetWebPartStatePaths&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;VwAspnetWebPartStatePaths&gt;"/></returns>
		protected VList<VwAspnetWebPartStatePaths> Fill(IDataReader reader, VList<VwAspnetWebPartStatePaths> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					VwAspnetWebPartStatePaths entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<VwAspnetWebPartStatePaths>("VwAspnetWebPartStatePaths",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new VwAspnetWebPartStatePaths();
					}
					
					entity.SuppressEntityEvents = true;

					entity.ApplicationId = (System.Guid)reader[((int)VwAspnetWebPartStatePathsColumn.ApplicationId)];
					//entity.ApplicationId = (Convert.IsDBNull(reader["ApplicationId"]))?Guid.Empty:(System.Guid)reader["ApplicationId"];
					entity.PathId = (System.Guid)reader[((int)VwAspnetWebPartStatePathsColumn.PathId)];
					//entity.PathId = (Convert.IsDBNull(reader["PathId"]))?Guid.Empty:(System.Guid)reader["PathId"];
					entity.Path = (System.String)reader[((int)VwAspnetWebPartStatePathsColumn.Path)];
					//entity.Path = (Convert.IsDBNull(reader["Path"]))?string.Empty:(System.String)reader["Path"];
					entity.LoweredPath = (System.String)reader[((int)VwAspnetWebPartStatePathsColumn.LoweredPath)];
					//entity.LoweredPath = (Convert.IsDBNull(reader["LoweredPath"]))?string.Empty:(System.String)reader["LoweredPath"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="VwAspnetWebPartStatePaths"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="VwAspnetWebPartStatePaths"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, VwAspnetWebPartStatePaths entity)
		{
			reader.Read();
			entity.ApplicationId = (System.Guid)reader[((int)VwAspnetWebPartStatePathsColumn.ApplicationId)];
			//entity.ApplicationId = (Convert.IsDBNull(reader["ApplicationId"]))?Guid.Empty:(System.Guid)reader["ApplicationId"];
			entity.PathId = (System.Guid)reader[((int)VwAspnetWebPartStatePathsColumn.PathId)];
			//entity.PathId = (Convert.IsDBNull(reader["PathId"]))?Guid.Empty:(System.Guid)reader["PathId"];
			entity.Path = (System.String)reader[((int)VwAspnetWebPartStatePathsColumn.Path)];
			//entity.Path = (Convert.IsDBNull(reader["Path"]))?string.Empty:(System.String)reader["Path"];
			entity.LoweredPath = (System.String)reader[((int)VwAspnetWebPartStatePathsColumn.LoweredPath)];
			//entity.LoweredPath = (Convert.IsDBNull(reader["LoweredPath"]))?string.Empty:(System.String)reader["LoweredPath"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="VwAspnetWebPartStatePaths"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="VwAspnetWebPartStatePaths"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, VwAspnetWebPartStatePaths entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.ApplicationId = (Convert.IsDBNull(dataRow["ApplicationId"]))?Guid.Empty:(System.Guid)dataRow["ApplicationId"];
			entity.PathId = (Convert.IsDBNull(dataRow["PathId"]))?Guid.Empty:(System.Guid)dataRow["PathId"];
			entity.Path = (Convert.IsDBNull(dataRow["Path"]))?string.Empty:(System.String)dataRow["Path"];
			entity.LoweredPath = (Convert.IsDBNull(dataRow["LoweredPath"]))?string.Empty:(System.String)dataRow["LoweredPath"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region VwAspnetWebPartStatePathsFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="VwAspnetWebPartStatePaths"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class VwAspnetWebPartStatePathsFilterBuilder : SqlFilterBuilder<VwAspnetWebPartStatePathsColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the VwAspnetWebPartStatePathsFilterBuilder class.
		/// </summary>
		public VwAspnetWebPartStatePathsFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the VwAspnetWebPartStatePathsFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public VwAspnetWebPartStatePathsFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the VwAspnetWebPartStatePathsFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public VwAspnetWebPartStatePathsFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion VwAspnetWebPartStatePathsFilterBuilder

	#region VwAspnetWebPartStatePathsParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="VwAspnetWebPartStatePaths"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class VwAspnetWebPartStatePathsParameterBuilder : ParameterizedSqlFilterBuilder<VwAspnetWebPartStatePathsColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the VwAspnetWebPartStatePathsParameterBuilder class.
		/// </summary>
		public VwAspnetWebPartStatePathsParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the VwAspnetWebPartStatePathsParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public VwAspnetWebPartStatePathsParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the VwAspnetWebPartStatePathsParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public VwAspnetWebPartStatePathsParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion VwAspnetWebPartStatePathsParameterBuilder
	
	#region VwAspnetWebPartStatePathsSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="VwAspnetWebPartStatePaths"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class VwAspnetWebPartStatePathsSortBuilder : SqlSortBuilder<VwAspnetWebPartStatePathsColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the VwAspnetWebPartStatePathsSqlSortBuilder class.
		/// </summary>
		public VwAspnetWebPartStatePathsSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion VwAspnetWebPartStatePathsSortBuilder

} // end namespace
