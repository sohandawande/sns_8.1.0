﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="VwZnodeCategoriesPortalsProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class VwZnodeCategoriesPortalsProviderBaseCore : EntityViewProviderBase<VwZnodeCategoriesPortals>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;VwZnodeCategoriesPortals&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;VwZnodeCategoriesPortals&gt;"/></returns>
		protected static VList&lt;VwZnodeCategoriesPortals&gt; Fill(DataSet dataSet, VList<VwZnodeCategoriesPortals> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<VwZnodeCategoriesPortals>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;VwZnodeCategoriesPortals&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<VwZnodeCategoriesPortals>"/></returns>
		protected static VList&lt;VwZnodeCategoriesPortals&gt; Fill(DataTable dataTable, VList<VwZnodeCategoriesPortals> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					VwZnodeCategoriesPortals c = new VwZnodeCategoriesPortals();
					c.PortalID = (Convert.IsDBNull(row["PortalID"]))?(int)0:(System.Int32)row["PortalID"];
					c.CatalogID = (Convert.IsDBNull(row["CatalogID"]))?(int)0:(System.Int32?)row["CatalogID"];
					c.CategoryID = (Convert.IsDBNull(row["CategoryID"]))?(int)0:(System.Int32)row["CategoryID"];
					c.Name = (Convert.IsDBNull(row["Name"]))?string.Empty:(System.String)row["Name"];
					c.Title = (Convert.IsDBNull(row["Title"]))?string.Empty:(System.String)row["Title"];
					c.ShortDescription = (Convert.IsDBNull(row["ShortDescription"]))?string.Empty:(System.String)row["ShortDescription"];
					c.Description = (Convert.IsDBNull(row["Description"]))?string.Empty:(System.String)row["Description"];
					c.ImageFile = (Convert.IsDBNull(row["ImageFile"]))?string.Empty:(System.String)row["ImageFile"];
					c.ImageAltTag = (Convert.IsDBNull(row["ImageAltTag"]))?string.Empty:(System.String)row["ImageAltTag"];
					c.VisibleInd = (Convert.IsDBNull(row["VisibleInd"]))?false:(System.Boolean)row["VisibleInd"];
					c.SubCategoryGridVisibleInd = (Convert.IsDBNull(row["SubCategoryGridVisibleInd"]))?false:(System.Boolean)row["SubCategoryGridVisibleInd"];
					c.SEOTitle = (Convert.IsDBNull(row["SEOTitle"]))?string.Empty:(System.String)row["SEOTitle"];
					c.SEOKeywords = (Convert.IsDBNull(row["SEOKeywords"]))?string.Empty:(System.String)row["SEOKeywords"];
					c.SEODescription = (Convert.IsDBNull(row["SEODescription"]))?string.Empty:(System.String)row["SEODescription"];
					c.AlternateDescription = (Convert.IsDBNull(row["AlternateDescription"]))?string.Empty:(System.String)row["AlternateDescription"];
					c.DisplayOrder = (Convert.IsDBNull(row["DisplayOrder"]))?(int)0:(System.Int32?)row["DisplayOrder"];
					c.Custom1 = (Convert.IsDBNull(row["Custom1"]))?string.Empty:(System.String)row["Custom1"];
					c.Custom2 = (Convert.IsDBNull(row["Custom2"]))?string.Empty:(System.String)row["Custom2"];
					c.Custom3 = (Convert.IsDBNull(row["Custom3"]))?string.Empty:(System.String)row["Custom3"];
					c.SEOURL = (Convert.IsDBNull(row["SEOURL"]))?string.Empty:(System.String)row["SEOURL"];
					c.CreateDate = (Convert.IsDBNull(row["CreateDate"]))?DateTime.MinValue:(System.DateTime?)row["CreateDate"];
					c.UpdateDate = (Convert.IsDBNull(row["UpdateDate"]))?DateTime.MinValue:(System.DateTime?)row["UpdateDate"];
					c.ExternalID = (Convert.IsDBNull(row["ExternalID"]))?string.Empty:(System.String)row["ExternalID"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;VwZnodeCategoriesPortals&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;VwZnodeCategoriesPortals&gt;"/></returns>
		protected VList<VwZnodeCategoriesPortals> Fill(IDataReader reader, VList<VwZnodeCategoriesPortals> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					VwZnodeCategoriesPortals entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<VwZnodeCategoriesPortals>("VwZnodeCategoriesPortals",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new VwZnodeCategoriesPortals();
					}
					
					entity.SuppressEntityEvents = true;

					entity.PortalID = (System.Int32)reader[((int)VwZnodeCategoriesPortalsColumn.PortalID)];
					//entity.PortalID = (Convert.IsDBNull(reader["PortalID"]))?(int)0:(System.Int32)reader["PortalID"];
					entity.CatalogID = (reader.IsDBNull(((int)VwZnodeCategoriesPortalsColumn.CatalogID)))?null:(System.Int32?)reader[((int)VwZnodeCategoriesPortalsColumn.CatalogID)];
					//entity.CatalogID = (Convert.IsDBNull(reader["CatalogID"]))?(int)0:(System.Int32?)reader["CatalogID"];
					entity.CategoryID = (System.Int32)reader[((int)VwZnodeCategoriesPortalsColumn.CategoryID)];
					//entity.CategoryID = (Convert.IsDBNull(reader["CategoryID"]))?(int)0:(System.Int32)reader["CategoryID"];
					entity.Name = (System.String)reader[((int)VwZnodeCategoriesPortalsColumn.Name)];
					//entity.Name = (Convert.IsDBNull(reader["Name"]))?string.Empty:(System.String)reader["Name"];
					entity.Title = (reader.IsDBNull(((int)VwZnodeCategoriesPortalsColumn.Title)))?null:(System.String)reader[((int)VwZnodeCategoriesPortalsColumn.Title)];
					//entity.Title = (Convert.IsDBNull(reader["Title"]))?string.Empty:(System.String)reader["Title"];
					entity.ShortDescription = (reader.IsDBNull(((int)VwZnodeCategoriesPortalsColumn.ShortDescription)))?null:(System.String)reader[((int)VwZnodeCategoriesPortalsColumn.ShortDescription)];
					//entity.ShortDescription = (Convert.IsDBNull(reader["ShortDescription"]))?string.Empty:(System.String)reader["ShortDescription"];
					entity.Description = (reader.IsDBNull(((int)VwZnodeCategoriesPortalsColumn.Description)))?null:(System.String)reader[((int)VwZnodeCategoriesPortalsColumn.Description)];
					//entity.Description = (Convert.IsDBNull(reader["Description"]))?string.Empty:(System.String)reader["Description"];
					entity.ImageFile = (reader.IsDBNull(((int)VwZnodeCategoriesPortalsColumn.ImageFile)))?null:(System.String)reader[((int)VwZnodeCategoriesPortalsColumn.ImageFile)];
					//entity.ImageFile = (Convert.IsDBNull(reader["ImageFile"]))?string.Empty:(System.String)reader["ImageFile"];
					entity.ImageAltTag = (reader.IsDBNull(((int)VwZnodeCategoriesPortalsColumn.ImageAltTag)))?null:(System.String)reader[((int)VwZnodeCategoriesPortalsColumn.ImageAltTag)];
					//entity.ImageAltTag = (Convert.IsDBNull(reader["ImageAltTag"]))?string.Empty:(System.String)reader["ImageAltTag"];
					entity.VisibleInd = (System.Boolean)reader[((int)VwZnodeCategoriesPortalsColumn.VisibleInd)];
					//entity.VisibleInd = (Convert.IsDBNull(reader["VisibleInd"]))?false:(System.Boolean)reader["VisibleInd"];
					entity.SubCategoryGridVisibleInd = (System.Boolean)reader[((int)VwZnodeCategoriesPortalsColumn.SubCategoryGridVisibleInd)];
					//entity.SubCategoryGridVisibleInd = (Convert.IsDBNull(reader["SubCategoryGridVisibleInd"]))?false:(System.Boolean)reader["SubCategoryGridVisibleInd"];
					entity.SEOTitle = (reader.IsDBNull(((int)VwZnodeCategoriesPortalsColumn.SEOTitle)))?null:(System.String)reader[((int)VwZnodeCategoriesPortalsColumn.SEOTitle)];
					//entity.SEOTitle = (Convert.IsDBNull(reader["SEOTitle"]))?string.Empty:(System.String)reader["SEOTitle"];
					entity.SEOKeywords = (reader.IsDBNull(((int)VwZnodeCategoriesPortalsColumn.SEOKeywords)))?null:(System.String)reader[((int)VwZnodeCategoriesPortalsColumn.SEOKeywords)];
					//entity.SEOKeywords = (Convert.IsDBNull(reader["SEOKeywords"]))?string.Empty:(System.String)reader["SEOKeywords"];
					entity.SEODescription = (reader.IsDBNull(((int)VwZnodeCategoriesPortalsColumn.SEODescription)))?null:(System.String)reader[((int)VwZnodeCategoriesPortalsColumn.SEODescription)];
					//entity.SEODescription = (Convert.IsDBNull(reader["SEODescription"]))?string.Empty:(System.String)reader["SEODescription"];
					entity.AlternateDescription = (reader.IsDBNull(((int)VwZnodeCategoriesPortalsColumn.AlternateDescription)))?null:(System.String)reader[((int)VwZnodeCategoriesPortalsColumn.AlternateDescription)];
					//entity.AlternateDescription = (Convert.IsDBNull(reader["AlternateDescription"]))?string.Empty:(System.String)reader["AlternateDescription"];
					entity.DisplayOrder = (reader.IsDBNull(((int)VwZnodeCategoriesPortalsColumn.DisplayOrder)))?null:(System.Int32?)reader[((int)VwZnodeCategoriesPortalsColumn.DisplayOrder)];
					//entity.DisplayOrder = (Convert.IsDBNull(reader["DisplayOrder"]))?(int)0:(System.Int32?)reader["DisplayOrder"];
					entity.Custom1 = (reader.IsDBNull(((int)VwZnodeCategoriesPortalsColumn.Custom1)))?null:(System.String)reader[((int)VwZnodeCategoriesPortalsColumn.Custom1)];
					//entity.Custom1 = (Convert.IsDBNull(reader["Custom1"]))?string.Empty:(System.String)reader["Custom1"];
					entity.Custom2 = (reader.IsDBNull(((int)VwZnodeCategoriesPortalsColumn.Custom2)))?null:(System.String)reader[((int)VwZnodeCategoriesPortalsColumn.Custom2)];
					//entity.Custom2 = (Convert.IsDBNull(reader["Custom2"]))?string.Empty:(System.String)reader["Custom2"];
					entity.Custom3 = (reader.IsDBNull(((int)VwZnodeCategoriesPortalsColumn.Custom3)))?null:(System.String)reader[((int)VwZnodeCategoriesPortalsColumn.Custom3)];
					//entity.Custom3 = (Convert.IsDBNull(reader["Custom3"]))?string.Empty:(System.String)reader["Custom3"];
					entity.SEOURL = (reader.IsDBNull(((int)VwZnodeCategoriesPortalsColumn.SEOURL)))?null:(System.String)reader[((int)VwZnodeCategoriesPortalsColumn.SEOURL)];
					//entity.SEOURL = (Convert.IsDBNull(reader["SEOURL"]))?string.Empty:(System.String)reader["SEOURL"];
					entity.CreateDate = (reader.IsDBNull(((int)VwZnodeCategoriesPortalsColumn.CreateDate)))?null:(System.DateTime?)reader[((int)VwZnodeCategoriesPortalsColumn.CreateDate)];
					//entity.CreateDate = (Convert.IsDBNull(reader["CreateDate"]))?DateTime.MinValue:(System.DateTime?)reader["CreateDate"];
					entity.UpdateDate = (reader.IsDBNull(((int)VwZnodeCategoriesPortalsColumn.UpdateDate)))?null:(System.DateTime?)reader[((int)VwZnodeCategoriesPortalsColumn.UpdateDate)];
					//entity.UpdateDate = (Convert.IsDBNull(reader["UpdateDate"]))?DateTime.MinValue:(System.DateTime?)reader["UpdateDate"];
					entity.ExternalID = (reader.IsDBNull(((int)VwZnodeCategoriesPortalsColumn.ExternalID)))?null:(System.String)reader[((int)VwZnodeCategoriesPortalsColumn.ExternalID)];
					//entity.ExternalID = (Convert.IsDBNull(reader["ExternalID"]))?string.Empty:(System.String)reader["ExternalID"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="VwZnodeCategoriesPortals"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="VwZnodeCategoriesPortals"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, VwZnodeCategoriesPortals entity)
		{
			reader.Read();
			entity.PortalID = (System.Int32)reader[((int)VwZnodeCategoriesPortalsColumn.PortalID)];
			//entity.PortalID = (Convert.IsDBNull(reader["PortalID"]))?(int)0:(System.Int32)reader["PortalID"];
			entity.CatalogID = (reader.IsDBNull(((int)VwZnodeCategoriesPortalsColumn.CatalogID)))?null:(System.Int32?)reader[((int)VwZnodeCategoriesPortalsColumn.CatalogID)];
			//entity.CatalogID = (Convert.IsDBNull(reader["CatalogID"]))?(int)0:(System.Int32?)reader["CatalogID"];
			entity.CategoryID = (System.Int32)reader[((int)VwZnodeCategoriesPortalsColumn.CategoryID)];
			//entity.CategoryID = (Convert.IsDBNull(reader["CategoryID"]))?(int)0:(System.Int32)reader["CategoryID"];
			entity.Name = (System.String)reader[((int)VwZnodeCategoriesPortalsColumn.Name)];
			//entity.Name = (Convert.IsDBNull(reader["Name"]))?string.Empty:(System.String)reader["Name"];
			entity.Title = (reader.IsDBNull(((int)VwZnodeCategoriesPortalsColumn.Title)))?null:(System.String)reader[((int)VwZnodeCategoriesPortalsColumn.Title)];
			//entity.Title = (Convert.IsDBNull(reader["Title"]))?string.Empty:(System.String)reader["Title"];
			entity.ShortDescription = (reader.IsDBNull(((int)VwZnodeCategoriesPortalsColumn.ShortDescription)))?null:(System.String)reader[((int)VwZnodeCategoriesPortalsColumn.ShortDescription)];
			//entity.ShortDescription = (Convert.IsDBNull(reader["ShortDescription"]))?string.Empty:(System.String)reader["ShortDescription"];
			entity.Description = (reader.IsDBNull(((int)VwZnodeCategoriesPortalsColumn.Description)))?null:(System.String)reader[((int)VwZnodeCategoriesPortalsColumn.Description)];
			//entity.Description = (Convert.IsDBNull(reader["Description"]))?string.Empty:(System.String)reader["Description"];
			entity.ImageFile = (reader.IsDBNull(((int)VwZnodeCategoriesPortalsColumn.ImageFile)))?null:(System.String)reader[((int)VwZnodeCategoriesPortalsColumn.ImageFile)];
			//entity.ImageFile = (Convert.IsDBNull(reader["ImageFile"]))?string.Empty:(System.String)reader["ImageFile"];
			entity.ImageAltTag = (reader.IsDBNull(((int)VwZnodeCategoriesPortalsColumn.ImageAltTag)))?null:(System.String)reader[((int)VwZnodeCategoriesPortalsColumn.ImageAltTag)];
			//entity.ImageAltTag = (Convert.IsDBNull(reader["ImageAltTag"]))?string.Empty:(System.String)reader["ImageAltTag"];
			entity.VisibleInd = (System.Boolean)reader[((int)VwZnodeCategoriesPortalsColumn.VisibleInd)];
			//entity.VisibleInd = (Convert.IsDBNull(reader["VisibleInd"]))?false:(System.Boolean)reader["VisibleInd"];
			entity.SubCategoryGridVisibleInd = (System.Boolean)reader[((int)VwZnodeCategoriesPortalsColumn.SubCategoryGridVisibleInd)];
			//entity.SubCategoryGridVisibleInd = (Convert.IsDBNull(reader["SubCategoryGridVisibleInd"]))?false:(System.Boolean)reader["SubCategoryGridVisibleInd"];
			entity.SEOTitle = (reader.IsDBNull(((int)VwZnodeCategoriesPortalsColumn.SEOTitle)))?null:(System.String)reader[((int)VwZnodeCategoriesPortalsColumn.SEOTitle)];
			//entity.SEOTitle = (Convert.IsDBNull(reader["SEOTitle"]))?string.Empty:(System.String)reader["SEOTitle"];
			entity.SEOKeywords = (reader.IsDBNull(((int)VwZnodeCategoriesPortalsColumn.SEOKeywords)))?null:(System.String)reader[((int)VwZnodeCategoriesPortalsColumn.SEOKeywords)];
			//entity.SEOKeywords = (Convert.IsDBNull(reader["SEOKeywords"]))?string.Empty:(System.String)reader["SEOKeywords"];
			entity.SEODescription = (reader.IsDBNull(((int)VwZnodeCategoriesPortalsColumn.SEODescription)))?null:(System.String)reader[((int)VwZnodeCategoriesPortalsColumn.SEODescription)];
			//entity.SEODescription = (Convert.IsDBNull(reader["SEODescription"]))?string.Empty:(System.String)reader["SEODescription"];
			entity.AlternateDescription = (reader.IsDBNull(((int)VwZnodeCategoriesPortalsColumn.AlternateDescription)))?null:(System.String)reader[((int)VwZnodeCategoriesPortalsColumn.AlternateDescription)];
			//entity.AlternateDescription = (Convert.IsDBNull(reader["AlternateDescription"]))?string.Empty:(System.String)reader["AlternateDescription"];
			entity.DisplayOrder = (reader.IsDBNull(((int)VwZnodeCategoriesPortalsColumn.DisplayOrder)))?null:(System.Int32?)reader[((int)VwZnodeCategoriesPortalsColumn.DisplayOrder)];
			//entity.DisplayOrder = (Convert.IsDBNull(reader["DisplayOrder"]))?(int)0:(System.Int32?)reader["DisplayOrder"];
			entity.Custom1 = (reader.IsDBNull(((int)VwZnodeCategoriesPortalsColumn.Custom1)))?null:(System.String)reader[((int)VwZnodeCategoriesPortalsColumn.Custom1)];
			//entity.Custom1 = (Convert.IsDBNull(reader["Custom1"]))?string.Empty:(System.String)reader["Custom1"];
			entity.Custom2 = (reader.IsDBNull(((int)VwZnodeCategoriesPortalsColumn.Custom2)))?null:(System.String)reader[((int)VwZnodeCategoriesPortalsColumn.Custom2)];
			//entity.Custom2 = (Convert.IsDBNull(reader["Custom2"]))?string.Empty:(System.String)reader["Custom2"];
			entity.Custom3 = (reader.IsDBNull(((int)VwZnodeCategoriesPortalsColumn.Custom3)))?null:(System.String)reader[((int)VwZnodeCategoriesPortalsColumn.Custom3)];
			//entity.Custom3 = (Convert.IsDBNull(reader["Custom3"]))?string.Empty:(System.String)reader["Custom3"];
			entity.SEOURL = (reader.IsDBNull(((int)VwZnodeCategoriesPortalsColumn.SEOURL)))?null:(System.String)reader[((int)VwZnodeCategoriesPortalsColumn.SEOURL)];
			//entity.SEOURL = (Convert.IsDBNull(reader["SEOURL"]))?string.Empty:(System.String)reader["SEOURL"];
			entity.CreateDate = (reader.IsDBNull(((int)VwZnodeCategoriesPortalsColumn.CreateDate)))?null:(System.DateTime?)reader[((int)VwZnodeCategoriesPortalsColumn.CreateDate)];
			//entity.CreateDate = (Convert.IsDBNull(reader["CreateDate"]))?DateTime.MinValue:(System.DateTime?)reader["CreateDate"];
			entity.UpdateDate = (reader.IsDBNull(((int)VwZnodeCategoriesPortalsColumn.UpdateDate)))?null:(System.DateTime?)reader[((int)VwZnodeCategoriesPortalsColumn.UpdateDate)];
			//entity.UpdateDate = (Convert.IsDBNull(reader["UpdateDate"]))?DateTime.MinValue:(System.DateTime?)reader["UpdateDate"];
			entity.ExternalID = (reader.IsDBNull(((int)VwZnodeCategoriesPortalsColumn.ExternalID)))?null:(System.String)reader[((int)VwZnodeCategoriesPortalsColumn.ExternalID)];
			//entity.ExternalID = (Convert.IsDBNull(reader["ExternalID"]))?string.Empty:(System.String)reader["ExternalID"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="VwZnodeCategoriesPortals"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="VwZnodeCategoriesPortals"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, VwZnodeCategoriesPortals entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.PortalID = (Convert.IsDBNull(dataRow["PortalID"]))?(int)0:(System.Int32)dataRow["PortalID"];
			entity.CatalogID = (Convert.IsDBNull(dataRow["CatalogID"]))?(int)0:(System.Int32?)dataRow["CatalogID"];
			entity.CategoryID = (Convert.IsDBNull(dataRow["CategoryID"]))?(int)0:(System.Int32)dataRow["CategoryID"];
			entity.Name = (Convert.IsDBNull(dataRow["Name"]))?string.Empty:(System.String)dataRow["Name"];
			entity.Title = (Convert.IsDBNull(dataRow["Title"]))?string.Empty:(System.String)dataRow["Title"];
			entity.ShortDescription = (Convert.IsDBNull(dataRow["ShortDescription"]))?string.Empty:(System.String)dataRow["ShortDescription"];
			entity.Description = (Convert.IsDBNull(dataRow["Description"]))?string.Empty:(System.String)dataRow["Description"];
			entity.ImageFile = (Convert.IsDBNull(dataRow["ImageFile"]))?string.Empty:(System.String)dataRow["ImageFile"];
			entity.ImageAltTag = (Convert.IsDBNull(dataRow["ImageAltTag"]))?string.Empty:(System.String)dataRow["ImageAltTag"];
			entity.VisibleInd = (Convert.IsDBNull(dataRow["VisibleInd"]))?false:(System.Boolean)dataRow["VisibleInd"];
			entity.SubCategoryGridVisibleInd = (Convert.IsDBNull(dataRow["SubCategoryGridVisibleInd"]))?false:(System.Boolean)dataRow["SubCategoryGridVisibleInd"];
			entity.SEOTitle = (Convert.IsDBNull(dataRow["SEOTitle"]))?string.Empty:(System.String)dataRow["SEOTitle"];
			entity.SEOKeywords = (Convert.IsDBNull(dataRow["SEOKeywords"]))?string.Empty:(System.String)dataRow["SEOKeywords"];
			entity.SEODescription = (Convert.IsDBNull(dataRow["SEODescription"]))?string.Empty:(System.String)dataRow["SEODescription"];
			entity.AlternateDescription = (Convert.IsDBNull(dataRow["AlternateDescription"]))?string.Empty:(System.String)dataRow["AlternateDescription"];
			entity.DisplayOrder = (Convert.IsDBNull(dataRow["DisplayOrder"]))?(int)0:(System.Int32?)dataRow["DisplayOrder"];
			entity.Custom1 = (Convert.IsDBNull(dataRow["Custom1"]))?string.Empty:(System.String)dataRow["Custom1"];
			entity.Custom2 = (Convert.IsDBNull(dataRow["Custom2"]))?string.Empty:(System.String)dataRow["Custom2"];
			entity.Custom3 = (Convert.IsDBNull(dataRow["Custom3"]))?string.Empty:(System.String)dataRow["Custom3"];
			entity.SEOURL = (Convert.IsDBNull(dataRow["SEOURL"]))?string.Empty:(System.String)dataRow["SEOURL"];
			entity.CreateDate = (Convert.IsDBNull(dataRow["CreateDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["CreateDate"];
			entity.UpdateDate = (Convert.IsDBNull(dataRow["UpdateDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["UpdateDate"];
			entity.ExternalID = (Convert.IsDBNull(dataRow["ExternalID"]))?string.Empty:(System.String)dataRow["ExternalID"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region VwZnodeCategoriesPortalsFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="VwZnodeCategoriesPortals"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class VwZnodeCategoriesPortalsFilterBuilder : SqlFilterBuilder<VwZnodeCategoriesPortalsColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the VwZnodeCategoriesPortalsFilterBuilder class.
		/// </summary>
		public VwZnodeCategoriesPortalsFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the VwZnodeCategoriesPortalsFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public VwZnodeCategoriesPortalsFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the VwZnodeCategoriesPortalsFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public VwZnodeCategoriesPortalsFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion VwZnodeCategoriesPortalsFilterBuilder

	#region VwZnodeCategoriesPortalsParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="VwZnodeCategoriesPortals"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class VwZnodeCategoriesPortalsParameterBuilder : ParameterizedSqlFilterBuilder<VwZnodeCategoriesPortalsColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the VwZnodeCategoriesPortalsParameterBuilder class.
		/// </summary>
		public VwZnodeCategoriesPortalsParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the VwZnodeCategoriesPortalsParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public VwZnodeCategoriesPortalsParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the VwZnodeCategoriesPortalsParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public VwZnodeCategoriesPortalsParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion VwZnodeCategoriesPortalsParameterBuilder
	
	#region VwZnodeCategoriesPortalsSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="VwZnodeCategoriesPortals"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class VwZnodeCategoriesPortalsSortBuilder : SqlSortBuilder<VwZnodeCategoriesPortalsColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the VwZnodeCategoriesPortalsSqlSortBuilder class.
		/// </summary>
		public VwZnodeCategoriesPortalsSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion VwZnodeCategoriesPortalsSortBuilder

} // end namespace
