﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="VwAspnetUsersProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class VwAspnetUsersProviderBaseCore : EntityViewProviderBase<VwAspnetUsers>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;VwAspnetUsers&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;VwAspnetUsers&gt;"/></returns>
		protected static VList&lt;VwAspnetUsers&gt; Fill(DataSet dataSet, VList<VwAspnetUsers> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<VwAspnetUsers>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;VwAspnetUsers&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<VwAspnetUsers>"/></returns>
		protected static VList&lt;VwAspnetUsers&gt; Fill(DataTable dataTable, VList<VwAspnetUsers> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					VwAspnetUsers c = new VwAspnetUsers();
					c.ApplicationId = (Convert.IsDBNull(row["ApplicationId"]))?Guid.Empty:(System.Guid)row["ApplicationId"];
					c.UserId = (Convert.IsDBNull(row["UserId"]))?Guid.Empty:(System.Guid)row["UserId"];
					c.UserName = (Convert.IsDBNull(row["UserName"]))?string.Empty:(System.String)row["UserName"];
					c.LoweredUserName = (Convert.IsDBNull(row["LoweredUserName"]))?string.Empty:(System.String)row["LoweredUserName"];
					c.MobileAlias = (Convert.IsDBNull(row["MobileAlias"]))?string.Empty:(System.String)row["MobileAlias"];
					c.IsAnonymous = (Convert.IsDBNull(row["IsAnonymous"]))?false:(System.Boolean)row["IsAnonymous"];
					c.LastActivityDate = (Convert.IsDBNull(row["LastActivityDate"]))?DateTime.MinValue:(System.DateTime)row["LastActivityDate"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;VwAspnetUsers&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;VwAspnetUsers&gt;"/></returns>
		protected VList<VwAspnetUsers> Fill(IDataReader reader, VList<VwAspnetUsers> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					VwAspnetUsers entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<VwAspnetUsers>("VwAspnetUsers",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new VwAspnetUsers();
					}
					
					entity.SuppressEntityEvents = true;

					entity.ApplicationId = (System.Guid)reader[((int)VwAspnetUsersColumn.ApplicationId)];
					//entity.ApplicationId = (Convert.IsDBNull(reader["ApplicationId"]))?Guid.Empty:(System.Guid)reader["ApplicationId"];
					entity.UserId = (System.Guid)reader[((int)VwAspnetUsersColumn.UserId)];
					//entity.UserId = (Convert.IsDBNull(reader["UserId"]))?Guid.Empty:(System.Guid)reader["UserId"];
					entity.UserName = (System.String)reader[((int)VwAspnetUsersColumn.UserName)];
					//entity.UserName = (Convert.IsDBNull(reader["UserName"]))?string.Empty:(System.String)reader["UserName"];
					entity.LoweredUserName = (System.String)reader[((int)VwAspnetUsersColumn.LoweredUserName)];
					//entity.LoweredUserName = (Convert.IsDBNull(reader["LoweredUserName"]))?string.Empty:(System.String)reader["LoweredUserName"];
					entity.MobileAlias = (reader.IsDBNull(((int)VwAspnetUsersColumn.MobileAlias)))?null:(System.String)reader[((int)VwAspnetUsersColumn.MobileAlias)];
					//entity.MobileAlias = (Convert.IsDBNull(reader["MobileAlias"]))?string.Empty:(System.String)reader["MobileAlias"];
					entity.IsAnonymous = (System.Boolean)reader[((int)VwAspnetUsersColumn.IsAnonymous)];
					//entity.IsAnonymous = (Convert.IsDBNull(reader["IsAnonymous"]))?false:(System.Boolean)reader["IsAnonymous"];
					entity.LastActivityDate = (System.DateTime)reader[((int)VwAspnetUsersColumn.LastActivityDate)];
					//entity.LastActivityDate = (Convert.IsDBNull(reader["LastActivityDate"]))?DateTime.MinValue:(System.DateTime)reader["LastActivityDate"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="VwAspnetUsers"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="VwAspnetUsers"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, VwAspnetUsers entity)
		{
			reader.Read();
			entity.ApplicationId = (System.Guid)reader[((int)VwAspnetUsersColumn.ApplicationId)];
			//entity.ApplicationId = (Convert.IsDBNull(reader["ApplicationId"]))?Guid.Empty:(System.Guid)reader["ApplicationId"];
			entity.UserId = (System.Guid)reader[((int)VwAspnetUsersColumn.UserId)];
			//entity.UserId = (Convert.IsDBNull(reader["UserId"]))?Guid.Empty:(System.Guid)reader["UserId"];
			entity.UserName = (System.String)reader[((int)VwAspnetUsersColumn.UserName)];
			//entity.UserName = (Convert.IsDBNull(reader["UserName"]))?string.Empty:(System.String)reader["UserName"];
			entity.LoweredUserName = (System.String)reader[((int)VwAspnetUsersColumn.LoweredUserName)];
			//entity.LoweredUserName = (Convert.IsDBNull(reader["LoweredUserName"]))?string.Empty:(System.String)reader["LoweredUserName"];
			entity.MobileAlias = (reader.IsDBNull(((int)VwAspnetUsersColumn.MobileAlias)))?null:(System.String)reader[((int)VwAspnetUsersColumn.MobileAlias)];
			//entity.MobileAlias = (Convert.IsDBNull(reader["MobileAlias"]))?string.Empty:(System.String)reader["MobileAlias"];
			entity.IsAnonymous = (System.Boolean)reader[((int)VwAspnetUsersColumn.IsAnonymous)];
			//entity.IsAnonymous = (Convert.IsDBNull(reader["IsAnonymous"]))?false:(System.Boolean)reader["IsAnonymous"];
			entity.LastActivityDate = (System.DateTime)reader[((int)VwAspnetUsersColumn.LastActivityDate)];
			//entity.LastActivityDate = (Convert.IsDBNull(reader["LastActivityDate"]))?DateTime.MinValue:(System.DateTime)reader["LastActivityDate"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="VwAspnetUsers"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="VwAspnetUsers"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, VwAspnetUsers entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.ApplicationId = (Convert.IsDBNull(dataRow["ApplicationId"]))?Guid.Empty:(System.Guid)dataRow["ApplicationId"];
			entity.UserId = (Convert.IsDBNull(dataRow["UserId"]))?Guid.Empty:(System.Guid)dataRow["UserId"];
			entity.UserName = (Convert.IsDBNull(dataRow["UserName"]))?string.Empty:(System.String)dataRow["UserName"];
			entity.LoweredUserName = (Convert.IsDBNull(dataRow["LoweredUserName"]))?string.Empty:(System.String)dataRow["LoweredUserName"];
			entity.MobileAlias = (Convert.IsDBNull(dataRow["MobileAlias"]))?string.Empty:(System.String)dataRow["MobileAlias"];
			entity.IsAnonymous = (Convert.IsDBNull(dataRow["IsAnonymous"]))?false:(System.Boolean)dataRow["IsAnonymous"];
			entity.LastActivityDate = (Convert.IsDBNull(dataRow["LastActivityDate"]))?DateTime.MinValue:(System.DateTime)dataRow["LastActivityDate"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region VwAspnetUsersFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="VwAspnetUsers"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class VwAspnetUsersFilterBuilder : SqlFilterBuilder<VwAspnetUsersColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the VwAspnetUsersFilterBuilder class.
		/// </summary>
		public VwAspnetUsersFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the VwAspnetUsersFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public VwAspnetUsersFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the VwAspnetUsersFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public VwAspnetUsersFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion VwAspnetUsersFilterBuilder

	#region VwAspnetUsersParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="VwAspnetUsers"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class VwAspnetUsersParameterBuilder : ParameterizedSqlFilterBuilder<VwAspnetUsersColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the VwAspnetUsersParameterBuilder class.
		/// </summary>
		public VwAspnetUsersParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the VwAspnetUsersParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public VwAspnetUsersParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the VwAspnetUsersParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public VwAspnetUsersParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion VwAspnetUsersParameterBuilder
	
	#region VwAspnetUsersSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="VwAspnetUsers"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class VwAspnetUsersSortBuilder : SqlSortBuilder<VwAspnetUsersColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the VwAspnetUsersSqlSortBuilder class.
		/// </summary>
		public VwAspnetUsersSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion VwAspnetUsersSortBuilder

} // end namespace
