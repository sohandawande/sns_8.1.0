﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="VwAspnetUsersInRolesProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class VwAspnetUsersInRolesProviderBaseCore : EntityViewProviderBase<VwAspnetUsersInRoles>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;VwAspnetUsersInRoles&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;VwAspnetUsersInRoles&gt;"/></returns>
		protected static VList&lt;VwAspnetUsersInRoles&gt; Fill(DataSet dataSet, VList<VwAspnetUsersInRoles> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<VwAspnetUsersInRoles>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;VwAspnetUsersInRoles&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<VwAspnetUsersInRoles>"/></returns>
		protected static VList&lt;VwAspnetUsersInRoles&gt; Fill(DataTable dataTable, VList<VwAspnetUsersInRoles> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					VwAspnetUsersInRoles c = new VwAspnetUsersInRoles();
					c.UserId = (Convert.IsDBNull(row["UserId"]))?Guid.Empty:(System.Guid)row["UserId"];
					c.RoleId = (Convert.IsDBNull(row["RoleId"]))?Guid.Empty:(System.Guid)row["RoleId"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;VwAspnetUsersInRoles&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;VwAspnetUsersInRoles&gt;"/></returns>
		protected VList<VwAspnetUsersInRoles> Fill(IDataReader reader, VList<VwAspnetUsersInRoles> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					VwAspnetUsersInRoles entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<VwAspnetUsersInRoles>("VwAspnetUsersInRoles",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new VwAspnetUsersInRoles();
					}
					
					entity.SuppressEntityEvents = true;

					entity.UserId = (System.Guid)reader[((int)VwAspnetUsersInRolesColumn.UserId)];
					//entity.UserId = (Convert.IsDBNull(reader["UserId"]))?Guid.Empty:(System.Guid)reader["UserId"];
					entity.RoleId = (System.Guid)reader[((int)VwAspnetUsersInRolesColumn.RoleId)];
					//entity.RoleId = (Convert.IsDBNull(reader["RoleId"]))?Guid.Empty:(System.Guid)reader["RoleId"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="VwAspnetUsersInRoles"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="VwAspnetUsersInRoles"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, VwAspnetUsersInRoles entity)
		{
			reader.Read();
			entity.UserId = (System.Guid)reader[((int)VwAspnetUsersInRolesColumn.UserId)];
			//entity.UserId = (Convert.IsDBNull(reader["UserId"]))?Guid.Empty:(System.Guid)reader["UserId"];
			entity.RoleId = (System.Guid)reader[((int)VwAspnetUsersInRolesColumn.RoleId)];
			//entity.RoleId = (Convert.IsDBNull(reader["RoleId"]))?Guid.Empty:(System.Guid)reader["RoleId"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="VwAspnetUsersInRoles"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="VwAspnetUsersInRoles"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, VwAspnetUsersInRoles entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.UserId = (Convert.IsDBNull(dataRow["UserId"]))?Guid.Empty:(System.Guid)dataRow["UserId"];
			entity.RoleId = (Convert.IsDBNull(dataRow["RoleId"]))?Guid.Empty:(System.Guid)dataRow["RoleId"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region VwAspnetUsersInRolesFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="VwAspnetUsersInRoles"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class VwAspnetUsersInRolesFilterBuilder : SqlFilterBuilder<VwAspnetUsersInRolesColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the VwAspnetUsersInRolesFilterBuilder class.
		/// </summary>
		public VwAspnetUsersInRolesFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the VwAspnetUsersInRolesFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public VwAspnetUsersInRolesFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the VwAspnetUsersInRolesFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public VwAspnetUsersInRolesFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion VwAspnetUsersInRolesFilterBuilder

	#region VwAspnetUsersInRolesParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="VwAspnetUsersInRoles"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class VwAspnetUsersInRolesParameterBuilder : ParameterizedSqlFilterBuilder<VwAspnetUsersInRolesColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the VwAspnetUsersInRolesParameterBuilder class.
		/// </summary>
		public VwAspnetUsersInRolesParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the VwAspnetUsersInRolesParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public VwAspnetUsersInRolesParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the VwAspnetUsersInRolesParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public VwAspnetUsersInRolesParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion VwAspnetUsersInRolesParameterBuilder
	
	#region VwAspnetUsersInRolesSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="VwAspnetUsersInRoles"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class VwAspnetUsersInRolesSortBuilder : SqlSortBuilder<VwAspnetUsersInRolesColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the VwAspnetUsersInRolesSqlSortBuilder class.
		/// </summary>
		public VwAspnetUsersInRolesSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion VwAspnetUsersInRolesSortBuilder

} // end namespace
