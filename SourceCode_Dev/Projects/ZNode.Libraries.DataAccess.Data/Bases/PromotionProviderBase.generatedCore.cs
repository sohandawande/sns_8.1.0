﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="PromotionProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class PromotionProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.Promotion, ZNode.Libraries.DataAccess.Entities.PromotionKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.PromotionKey key)
		{
			return Delete(transactionManager, key.PromotionID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_promotionID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _promotionID)
		{
			return Delete(null, _promotionID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_promotionID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _promotionID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePromotion_ZNodeAddOnValue key.
		///		FK_ZNodePromotion_ZNodeAddOnValue Description: 
		/// </summary>
		/// <param name="_addOnValueID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Promotion objects.</returns>
		public TList<Promotion> GetByAddOnValueID(System.Int32? _addOnValueID)
		{
			int count = -1;
			return GetByAddOnValueID(_addOnValueID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePromotion_ZNodeAddOnValue key.
		///		FK_ZNodePromotion_ZNodeAddOnValue Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_addOnValueID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Promotion objects.</returns>
		/// <remarks></remarks>
		public TList<Promotion> GetByAddOnValueID(TransactionManager transactionManager, System.Int32? _addOnValueID)
		{
			int count = -1;
			return GetByAddOnValueID(transactionManager, _addOnValueID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePromotion_ZNodeAddOnValue key.
		///		FK_ZNodePromotion_ZNodeAddOnValue Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_addOnValueID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Promotion objects.</returns>
		public TList<Promotion> GetByAddOnValueID(TransactionManager transactionManager, System.Int32? _addOnValueID, int start, int pageLength)
		{
			int count = -1;
			return GetByAddOnValueID(transactionManager, _addOnValueID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePromotion_ZNodeAddOnValue key.
		///		fKZNodePromotionZNodeAddOnValue Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_addOnValueID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Promotion objects.</returns>
		public TList<Promotion> GetByAddOnValueID(System.Int32? _addOnValueID, int start, int pageLength)
		{
			int count =  -1;
			return GetByAddOnValueID(null, _addOnValueID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePromotion_ZNodeAddOnValue key.
		///		fKZNodePromotionZNodeAddOnValue Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_addOnValueID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Promotion objects.</returns>
		public TList<Promotion> GetByAddOnValueID(System.Int32? _addOnValueID, int start, int pageLength,out int count)
		{
			return GetByAddOnValueID(null, _addOnValueID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePromotion_ZNodeAddOnValue key.
		///		FK_ZNodePromotion_ZNodeAddOnValue Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_addOnValueID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Promotion objects.</returns>
		public abstract TList<Promotion> GetByAddOnValueID(TransactionManager transactionManager, System.Int32? _addOnValueID, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePromotion_ZNodeSKU key.
		///		FK_ZNodePromotion_ZNodeSKU Description: 
		/// </summary>
		/// <param name="_sKUID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Promotion objects.</returns>
		public TList<Promotion> GetBySKUID(System.Int32? _sKUID)
		{
			int count = -1;
			return GetBySKUID(_sKUID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePromotion_ZNodeSKU key.
		///		FK_ZNodePromotion_ZNodeSKU Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_sKUID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Promotion objects.</returns>
		/// <remarks></remarks>
		public TList<Promotion> GetBySKUID(TransactionManager transactionManager, System.Int32? _sKUID)
		{
			int count = -1;
			return GetBySKUID(transactionManager, _sKUID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePromotion_ZNodeSKU key.
		///		FK_ZNodePromotion_ZNodeSKU Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_sKUID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Promotion objects.</returns>
		public TList<Promotion> GetBySKUID(TransactionManager transactionManager, System.Int32? _sKUID, int start, int pageLength)
		{
			int count = -1;
			return GetBySKUID(transactionManager, _sKUID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePromotion_ZNodeSKU key.
		///		fKZNodePromotionZNodeSKU Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_sKUID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Promotion objects.</returns>
		public TList<Promotion> GetBySKUID(System.Int32? _sKUID, int start, int pageLength)
		{
			int count =  -1;
			return GetBySKUID(null, _sKUID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePromotion_ZNodeSKU key.
		///		fKZNodePromotionZNodeSKU Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_sKUID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Promotion objects.</returns>
		public TList<Promotion> GetBySKUID(System.Int32? _sKUID, int start, int pageLength,out int count)
		{
			return GetBySKUID(null, _sKUID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePromotion_ZNodeSKU key.
		///		FK_ZNodePromotion_ZNodeSKU Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_sKUID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Promotion objects.</returns>
		public abstract TList<Promotion> GetBySKUID(TransactionManager transactionManager, System.Int32? _sKUID, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.Promotion Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.PromotionKey key, int start, int pageLength)
		{
			return GetByPromotionID(transactionManager, key.PromotionID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodePromotion_AccountID index.
		/// </summary>
		/// <param name="_accountID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public TList<Promotion> GetByAccountID(System.Int32? _accountID)
		{
			int count = -1;
			return GetByAccountID(null,_accountID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePromotion_AccountID index.
		/// </summary>
		/// <param name="_accountID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public TList<Promotion> GetByAccountID(System.Int32? _accountID, int start, int pageLength)
		{
			int count = -1;
			return GetByAccountID(null, _accountID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePromotion_AccountID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accountID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public TList<Promotion> GetByAccountID(TransactionManager transactionManager, System.Int32? _accountID)
		{
			int count = -1;
			return GetByAccountID(transactionManager, _accountID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePromotion_AccountID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accountID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public TList<Promotion> GetByAccountID(TransactionManager transactionManager, System.Int32? _accountID, int start, int pageLength)
		{
			int count = -1;
			return GetByAccountID(transactionManager, _accountID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePromotion_AccountID index.
		/// </summary>
		/// <param name="_accountID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public TList<Promotion> GetByAccountID(System.Int32? _accountID, int start, int pageLength, out int count)
		{
			return GetByAccountID(null, _accountID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePromotion_AccountID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accountID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public abstract TList<Promotion> GetByAccountID(TransactionManager transactionManager, System.Int32? _accountID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodePromotion_CatalogID index.
		/// </summary>
		/// <param name="_catalogID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public TList<Promotion> GetByCatalogID(System.Int32? _catalogID)
		{
			int count = -1;
			return GetByCatalogID(null,_catalogID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePromotion_CatalogID index.
		/// </summary>
		/// <param name="_catalogID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public TList<Promotion> GetByCatalogID(System.Int32? _catalogID, int start, int pageLength)
		{
			int count = -1;
			return GetByCatalogID(null, _catalogID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePromotion_CatalogID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_catalogID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public TList<Promotion> GetByCatalogID(TransactionManager transactionManager, System.Int32? _catalogID)
		{
			int count = -1;
			return GetByCatalogID(transactionManager, _catalogID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePromotion_CatalogID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_catalogID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public TList<Promotion> GetByCatalogID(TransactionManager transactionManager, System.Int32? _catalogID, int start, int pageLength)
		{
			int count = -1;
			return GetByCatalogID(transactionManager, _catalogID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePromotion_CatalogID index.
		/// </summary>
		/// <param name="_catalogID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public TList<Promotion> GetByCatalogID(System.Int32? _catalogID, int start, int pageLength, out int count)
		{
			return GetByCatalogID(null, _catalogID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePromotion_CatalogID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_catalogID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public abstract TList<Promotion> GetByCatalogID(TransactionManager transactionManager, System.Int32? _catalogID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodePromotion_CategoryID index.
		/// </summary>
		/// <param name="_categoryID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public TList<Promotion> GetByCategoryID(System.Int32? _categoryID)
		{
			int count = -1;
			return GetByCategoryID(null,_categoryID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePromotion_CategoryID index.
		/// </summary>
		/// <param name="_categoryID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public TList<Promotion> GetByCategoryID(System.Int32? _categoryID, int start, int pageLength)
		{
			int count = -1;
			return GetByCategoryID(null, _categoryID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePromotion_CategoryID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_categoryID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public TList<Promotion> GetByCategoryID(TransactionManager transactionManager, System.Int32? _categoryID)
		{
			int count = -1;
			return GetByCategoryID(transactionManager, _categoryID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePromotion_CategoryID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_categoryID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public TList<Promotion> GetByCategoryID(TransactionManager transactionManager, System.Int32? _categoryID, int start, int pageLength)
		{
			int count = -1;
			return GetByCategoryID(transactionManager, _categoryID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePromotion_CategoryID index.
		/// </summary>
		/// <param name="_categoryID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public TList<Promotion> GetByCategoryID(System.Int32? _categoryID, int start, int pageLength, out int count)
		{
			return GetByCategoryID(null, _categoryID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePromotion_CategoryID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_categoryID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public abstract TList<Promotion> GetByCategoryID(TransactionManager transactionManager, System.Int32? _categoryID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodePromotion_Discount index.
		/// </summary>
		/// <param name="_discount"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public TList<Promotion> GetByDiscount(System.Decimal _discount)
		{
			int count = -1;
			return GetByDiscount(null,_discount, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePromotion_Discount index.
		/// </summary>
		/// <param name="_discount"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public TList<Promotion> GetByDiscount(System.Decimal _discount, int start, int pageLength)
		{
			int count = -1;
			return GetByDiscount(null, _discount, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePromotion_Discount index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_discount"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public TList<Promotion> GetByDiscount(TransactionManager transactionManager, System.Decimal _discount)
		{
			int count = -1;
			return GetByDiscount(transactionManager, _discount, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePromotion_Discount index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_discount"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public TList<Promotion> GetByDiscount(TransactionManager transactionManager, System.Decimal _discount, int start, int pageLength)
		{
			int count = -1;
			return GetByDiscount(transactionManager, _discount, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePromotion_Discount index.
		/// </summary>
		/// <param name="_discount"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public TList<Promotion> GetByDiscount(System.Decimal _discount, int start, int pageLength, out int count)
		{
			return GetByDiscount(null, _discount, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePromotion_Discount index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_discount"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public abstract TList<Promotion> GetByDiscount(TransactionManager transactionManager, System.Decimal _discount, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodePromotion_DiscountTypeID index.
		/// </summary>
		/// <param name="_discountTypeID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public TList<Promotion> GetByDiscountTypeID(System.Int32 _discountTypeID)
		{
			int count = -1;
			return GetByDiscountTypeID(null,_discountTypeID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePromotion_DiscountTypeID index.
		/// </summary>
		/// <param name="_discountTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public TList<Promotion> GetByDiscountTypeID(System.Int32 _discountTypeID, int start, int pageLength)
		{
			int count = -1;
			return GetByDiscountTypeID(null, _discountTypeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePromotion_DiscountTypeID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_discountTypeID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public TList<Promotion> GetByDiscountTypeID(TransactionManager transactionManager, System.Int32 _discountTypeID)
		{
			int count = -1;
			return GetByDiscountTypeID(transactionManager, _discountTypeID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePromotion_DiscountTypeID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_discountTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public TList<Promotion> GetByDiscountTypeID(TransactionManager transactionManager, System.Int32 _discountTypeID, int start, int pageLength)
		{
			int count = -1;
			return GetByDiscountTypeID(transactionManager, _discountTypeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePromotion_DiscountTypeID index.
		/// </summary>
		/// <param name="_discountTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public TList<Promotion> GetByDiscountTypeID(System.Int32 _discountTypeID, int start, int pageLength, out int count)
		{
			return GetByDiscountTypeID(null, _discountTypeID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePromotion_DiscountTypeID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_discountTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public abstract TList<Promotion> GetByDiscountTypeID(TransactionManager transactionManager, System.Int32 _discountTypeID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodePromotion_EndDate index.
		/// </summary>
		/// <param name="_endDate"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public TList<Promotion> GetByEndDate(System.DateTime _endDate)
		{
			int count = -1;
			return GetByEndDate(null,_endDate, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePromotion_EndDate index.
		/// </summary>
		/// <param name="_endDate"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public TList<Promotion> GetByEndDate(System.DateTime _endDate, int start, int pageLength)
		{
			int count = -1;
			return GetByEndDate(null, _endDate, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePromotion_EndDate index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_endDate"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public TList<Promotion> GetByEndDate(TransactionManager transactionManager, System.DateTime _endDate)
		{
			int count = -1;
			return GetByEndDate(transactionManager, _endDate, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePromotion_EndDate index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_endDate"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public TList<Promotion> GetByEndDate(TransactionManager transactionManager, System.DateTime _endDate, int start, int pageLength)
		{
			int count = -1;
			return GetByEndDate(transactionManager, _endDate, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePromotion_EndDate index.
		/// </summary>
		/// <param name="_endDate"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public TList<Promotion> GetByEndDate(System.DateTime _endDate, int start, int pageLength, out int count)
		{
			return GetByEndDate(null, _endDate, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePromotion_EndDate index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_endDate"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public abstract TList<Promotion> GetByEndDate(TransactionManager transactionManager, System.DateTime _endDate, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodePromotion_ExternalID index.
		/// </summary>
		/// <param name="_externalID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public TList<Promotion> GetByExternalID(System.String _externalID)
		{
			int count = -1;
			return GetByExternalID(null,_externalID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePromotion_ExternalID index.
		/// </summary>
		/// <param name="_externalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public TList<Promotion> GetByExternalID(System.String _externalID, int start, int pageLength)
		{
			int count = -1;
			return GetByExternalID(null, _externalID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePromotion_ExternalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_externalID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public TList<Promotion> GetByExternalID(TransactionManager transactionManager, System.String _externalID)
		{
			int count = -1;
			return GetByExternalID(transactionManager, _externalID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePromotion_ExternalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_externalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public TList<Promotion> GetByExternalID(TransactionManager transactionManager, System.String _externalID, int start, int pageLength)
		{
			int count = -1;
			return GetByExternalID(transactionManager, _externalID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePromotion_ExternalID index.
		/// </summary>
		/// <param name="_externalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public TList<Promotion> GetByExternalID(System.String _externalID, int start, int pageLength, out int count)
		{
			return GetByExternalID(null, _externalID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePromotion_ExternalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_externalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public abstract TList<Promotion> GetByExternalID(TransactionManager transactionManager, System.String _externalID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodePromotion_ManufacturerID index.
		/// </summary>
		/// <param name="_manufacturerID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public TList<Promotion> GetByManufacturerID(System.Int32? _manufacturerID)
		{
			int count = -1;
			return GetByManufacturerID(null,_manufacturerID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePromotion_ManufacturerID index.
		/// </summary>
		/// <param name="_manufacturerID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public TList<Promotion> GetByManufacturerID(System.Int32? _manufacturerID, int start, int pageLength)
		{
			int count = -1;
			return GetByManufacturerID(null, _manufacturerID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePromotion_ManufacturerID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_manufacturerID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public TList<Promotion> GetByManufacturerID(TransactionManager transactionManager, System.Int32? _manufacturerID)
		{
			int count = -1;
			return GetByManufacturerID(transactionManager, _manufacturerID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePromotion_ManufacturerID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_manufacturerID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public TList<Promotion> GetByManufacturerID(TransactionManager transactionManager, System.Int32? _manufacturerID, int start, int pageLength)
		{
			int count = -1;
			return GetByManufacturerID(transactionManager, _manufacturerID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePromotion_ManufacturerID index.
		/// </summary>
		/// <param name="_manufacturerID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public TList<Promotion> GetByManufacturerID(System.Int32? _manufacturerID, int start, int pageLength, out int count)
		{
			return GetByManufacturerID(null, _manufacturerID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePromotion_ManufacturerID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_manufacturerID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public abstract TList<Promotion> GetByManufacturerID(TransactionManager transactionManager, System.Int32? _manufacturerID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodePromotion_PortalID index.
		/// </summary>
		/// <param name="_portalID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public TList<Promotion> GetByPortalID(System.Int32? _portalID)
		{
			int count = -1;
			return GetByPortalID(null,_portalID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePromotion_PortalID index.
		/// </summary>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public TList<Promotion> GetByPortalID(System.Int32? _portalID, int start, int pageLength)
		{
			int count = -1;
			return GetByPortalID(null, _portalID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePromotion_PortalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public TList<Promotion> GetByPortalID(TransactionManager transactionManager, System.Int32? _portalID)
		{
			int count = -1;
			return GetByPortalID(transactionManager, _portalID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePromotion_PortalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public TList<Promotion> GetByPortalID(TransactionManager transactionManager, System.Int32? _portalID, int start, int pageLength)
		{
			int count = -1;
			return GetByPortalID(transactionManager, _portalID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePromotion_PortalID index.
		/// </summary>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public TList<Promotion> GetByPortalID(System.Int32? _portalID, int start, int pageLength, out int count)
		{
			return GetByPortalID(null, _portalID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePromotion_PortalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public abstract TList<Promotion> GetByPortalID(TransactionManager transactionManager, System.Int32? _portalID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodePromotion_ProductID index.
		/// </summary>
		/// <param name="_productID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public TList<Promotion> GetByProductID(System.Int32? _productID)
		{
			int count = -1;
			return GetByProductID(null,_productID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePromotion_ProductID index.
		/// </summary>
		/// <param name="_productID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public TList<Promotion> GetByProductID(System.Int32? _productID, int start, int pageLength)
		{
			int count = -1;
			return GetByProductID(null, _productID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePromotion_ProductID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public TList<Promotion> GetByProductID(TransactionManager transactionManager, System.Int32? _productID)
		{
			int count = -1;
			return GetByProductID(transactionManager, _productID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePromotion_ProductID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public TList<Promotion> GetByProductID(TransactionManager transactionManager, System.Int32? _productID, int start, int pageLength)
		{
			int count = -1;
			return GetByProductID(transactionManager, _productID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePromotion_ProductID index.
		/// </summary>
		/// <param name="_productID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public TList<Promotion> GetByProductID(System.Int32? _productID, int start, int pageLength, out int count)
		{
			return GetByProductID(null, _productID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePromotion_ProductID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public abstract TList<Promotion> GetByProductID(TransactionManager transactionManager, System.Int32? _productID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodePromotion_ProfileID index.
		/// </summary>
		/// <param name="_profileID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public TList<Promotion> GetByProfileID(System.Int32? _profileID)
		{
			int count = -1;
			return GetByProfileID(null,_profileID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePromotion_ProfileID index.
		/// </summary>
		/// <param name="_profileID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public TList<Promotion> GetByProfileID(System.Int32? _profileID, int start, int pageLength)
		{
			int count = -1;
			return GetByProfileID(null, _profileID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePromotion_ProfileID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_profileID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public TList<Promotion> GetByProfileID(TransactionManager transactionManager, System.Int32? _profileID)
		{
			int count = -1;
			return GetByProfileID(transactionManager, _profileID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePromotion_ProfileID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_profileID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public TList<Promotion> GetByProfileID(TransactionManager transactionManager, System.Int32? _profileID, int start, int pageLength)
		{
			int count = -1;
			return GetByProfileID(transactionManager, _profileID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePromotion_ProfileID index.
		/// </summary>
		/// <param name="_profileID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public TList<Promotion> GetByProfileID(System.Int32? _profileID, int start, int pageLength, out int count)
		{
			return GetByProfileID(null, _profileID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePromotion_ProfileID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_profileID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public abstract TList<Promotion> GetByProfileID(TransactionManager transactionManager, System.Int32? _profileID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodePromotion_PromotionProductID index.
		/// </summary>
		/// <param name="_promotionProductID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public TList<Promotion> GetByPromotionProductID(System.Int32? _promotionProductID)
		{
			int count = -1;
			return GetByPromotionProductID(null,_promotionProductID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePromotion_PromotionProductID index.
		/// </summary>
		/// <param name="_promotionProductID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public TList<Promotion> GetByPromotionProductID(System.Int32? _promotionProductID, int start, int pageLength)
		{
			int count = -1;
			return GetByPromotionProductID(null, _promotionProductID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePromotion_PromotionProductID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_promotionProductID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public TList<Promotion> GetByPromotionProductID(TransactionManager transactionManager, System.Int32? _promotionProductID)
		{
			int count = -1;
			return GetByPromotionProductID(transactionManager, _promotionProductID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePromotion_PromotionProductID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_promotionProductID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public TList<Promotion> GetByPromotionProductID(TransactionManager transactionManager, System.Int32? _promotionProductID, int start, int pageLength)
		{
			int count = -1;
			return GetByPromotionProductID(transactionManager, _promotionProductID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePromotion_PromotionProductID index.
		/// </summary>
		/// <param name="_promotionProductID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public TList<Promotion> GetByPromotionProductID(System.Int32? _promotionProductID, int start, int pageLength, out int count)
		{
			return GetByPromotionProductID(null, _promotionProductID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePromotion_PromotionProductID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_promotionProductID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public abstract TList<Promotion> GetByPromotionProductID(TransactionManager transactionManager, System.Int32? _promotionProductID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodePromotion_StartDate index.
		/// </summary>
		/// <param name="_startDate"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public TList<Promotion> GetByStartDate(System.DateTime _startDate)
		{
			int count = -1;
			return GetByStartDate(null,_startDate, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePromotion_StartDate index.
		/// </summary>
		/// <param name="_startDate"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public TList<Promotion> GetByStartDate(System.DateTime _startDate, int start, int pageLength)
		{
			int count = -1;
			return GetByStartDate(null, _startDate, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePromotion_StartDate index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_startDate"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public TList<Promotion> GetByStartDate(TransactionManager transactionManager, System.DateTime _startDate)
		{
			int count = -1;
			return GetByStartDate(transactionManager, _startDate, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePromotion_StartDate index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_startDate"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public TList<Promotion> GetByStartDate(TransactionManager transactionManager, System.DateTime _startDate, int start, int pageLength)
		{
			int count = -1;
			return GetByStartDate(transactionManager, _startDate, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePromotion_StartDate index.
		/// </summary>
		/// <param name="_startDate"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public TList<Promotion> GetByStartDate(System.DateTime _startDate, int start, int pageLength, out int count)
		{
			return GetByStartDate(null, _startDate, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePromotion_StartDate index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_startDate"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Promotion&gt;"/> class.</returns>
		public abstract TList<Promotion> GetByStartDate(TransactionManager transactionManager, System.DateTime _startDate, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodePromotion index.
		/// </summary>
		/// <param name="_promotionID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Promotion"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Promotion GetByPromotionID(System.Int32 _promotionID)
		{
			int count = -1;
			return GetByPromotionID(null,_promotionID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodePromotion index.
		/// </summary>
		/// <param name="_promotionID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Promotion"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Promotion GetByPromotionID(System.Int32 _promotionID, int start, int pageLength)
		{
			int count = -1;
			return GetByPromotionID(null, _promotionID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodePromotion index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_promotionID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Promotion"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Promotion GetByPromotionID(TransactionManager transactionManager, System.Int32 _promotionID)
		{
			int count = -1;
			return GetByPromotionID(transactionManager, _promotionID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodePromotion index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_promotionID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Promotion"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Promotion GetByPromotionID(TransactionManager transactionManager, System.Int32 _promotionID, int start, int pageLength)
		{
			int count = -1;
			return GetByPromotionID(transactionManager, _promotionID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodePromotion index.
		/// </summary>
		/// <param name="_promotionID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Promotion"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Promotion GetByPromotionID(System.Int32 _promotionID, int start, int pageLength, out int count)
		{
			return GetByPromotionID(null, _promotionID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodePromotion index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_promotionID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Promotion"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.Promotion GetByPromotionID(TransactionManager transactionManager, System.Int32 _promotionID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;Promotion&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;Promotion&gt;"/></returns>
		public static TList<Promotion> Fill(IDataReader reader, TList<Promotion> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.Promotion c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("Promotion")
					.Append("|").Append((System.Int32)reader[((int)PromotionColumn.PromotionID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<Promotion>(
					key.ToString(), // EntityTrackingKey
					"Promotion",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.Promotion();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.PromotionID = (System.Int32)reader[((int)PromotionColumn.PromotionID - 1)];
					c.Name = (System.String)reader[((int)PromotionColumn.Name - 1)];
					c.Description = (reader.IsDBNull(((int)PromotionColumn.Description - 1)))?null:(System.String)reader[((int)PromotionColumn.Description - 1)];
					c.ProfileID = (reader.IsDBNull(((int)PromotionColumn.ProfileID - 1)))?null:(System.Int32?)reader[((int)PromotionColumn.ProfileID - 1)];
					c.AccountID = (reader.IsDBNull(((int)PromotionColumn.AccountID - 1)))?null:(System.Int32?)reader[((int)PromotionColumn.AccountID - 1)];
					c.ProductID = (reader.IsDBNull(((int)PromotionColumn.ProductID - 1)))?null:(System.Int32?)reader[((int)PromotionColumn.ProductID - 1)];
					c.AddOnValueID = (reader.IsDBNull(((int)PromotionColumn.AddOnValueID - 1)))?null:(System.Int32?)reader[((int)PromotionColumn.AddOnValueID - 1)];
					c.SKUID = (reader.IsDBNull(((int)PromotionColumn.SKUID - 1)))?null:(System.Int32?)reader[((int)PromotionColumn.SKUID - 1)];
					c.DiscountTypeID = (System.Int32)reader[((int)PromotionColumn.DiscountTypeID - 1)];
					c.Discount = (System.Decimal)reader[((int)PromotionColumn.Discount - 1)];
					c.StartDate = (System.DateTime)reader[((int)PromotionColumn.StartDate - 1)];
					c.EndDate = (System.DateTime)reader[((int)PromotionColumn.EndDate - 1)];
					c.CouponInd = (System.Boolean)reader[((int)PromotionColumn.CouponInd - 1)];
					c.CouponCode = (reader.IsDBNull(((int)PromotionColumn.CouponCode - 1)))?null:(System.String)reader[((int)PromotionColumn.CouponCode - 1)];
					c.CouponQuantityAvailable = (reader.IsDBNull(((int)PromotionColumn.CouponQuantityAvailable - 1)))?null:(System.Int32?)reader[((int)PromotionColumn.CouponQuantityAvailable - 1)];
					c.PromotionMessage = (reader.IsDBNull(((int)PromotionColumn.PromotionMessage - 1)))?null:(System.String)reader[((int)PromotionColumn.PromotionMessage - 1)];
					c.OrderMinimum = (reader.IsDBNull(((int)PromotionColumn.OrderMinimum - 1)))?null:(System.Decimal?)reader[((int)PromotionColumn.OrderMinimum - 1)];
					c.QuantityMinimum = (reader.IsDBNull(((int)PromotionColumn.QuantityMinimum - 1)))?null:(System.Int32?)reader[((int)PromotionColumn.QuantityMinimum - 1)];
					c.PromotionProductQty = (reader.IsDBNull(((int)PromotionColumn.PromotionProductQty - 1)))?null:(System.Int32?)reader[((int)PromotionColumn.PromotionProductQty - 1)];
					c.PromotionProductID = (reader.IsDBNull(((int)PromotionColumn.PromotionProductID - 1)))?null:(System.Int32?)reader[((int)PromotionColumn.PromotionProductID - 1)];
					c.DisplayOrder = (System.Int32)reader[((int)PromotionColumn.DisplayOrder - 1)];
					c.Custom1 = (reader.IsDBNull(((int)PromotionColumn.Custom1 - 1)))?null:(System.String)reader[((int)PromotionColumn.Custom1 - 1)];
					c.Custom2 = (reader.IsDBNull(((int)PromotionColumn.Custom2 - 1)))?null:(System.String)reader[((int)PromotionColumn.Custom2 - 1)];
					c.Custom3 = (reader.IsDBNull(((int)PromotionColumn.Custom3 - 1)))?null:(System.String)reader[((int)PromotionColumn.Custom3 - 1)];
					c.EnableCouponUrl = (reader.IsDBNull(((int)PromotionColumn.EnableCouponUrl - 1)))?null:(System.Boolean?)reader[((int)PromotionColumn.EnableCouponUrl - 1)];
					c.PromoCode = (reader.IsDBNull(((int)PromotionColumn.PromoCode - 1)))?null:(System.String)reader[((int)PromotionColumn.PromoCode - 1)];
					c.ExternalID = (reader.IsDBNull(((int)PromotionColumn.ExternalID - 1)))?null:(System.String)reader[((int)PromotionColumn.ExternalID - 1)];
					c.PortalID = (reader.IsDBNull(((int)PromotionColumn.PortalID - 1)))?null:(System.Int32?)reader[((int)PromotionColumn.PortalID - 1)];
					c.CategoryID = (reader.IsDBNull(((int)PromotionColumn.CategoryID - 1)))?null:(System.Int32?)reader[((int)PromotionColumn.CategoryID - 1)];
					c.ManufacturerID = (reader.IsDBNull(((int)PromotionColumn.ManufacturerID - 1)))?null:(System.Int32?)reader[((int)PromotionColumn.ManufacturerID - 1)];
					c.CatalogID = (reader.IsDBNull(((int)PromotionColumn.CatalogID - 1)))?null:(System.Int32?)reader[((int)PromotionColumn.CatalogID - 1)];
					c.IsCouponAllowedWithOtherCoupons = (System.Boolean)reader[((int)PromotionColumn.IsCouponAllowedWithOtherCoupons - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.Promotion"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.Promotion"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.Promotion entity)
		{
			if (!reader.Read()) return;
			
			entity.PromotionID = (System.Int32)reader[((int)PromotionColumn.PromotionID - 1)];
			entity.Name = (System.String)reader[((int)PromotionColumn.Name - 1)];
			entity.Description = (reader.IsDBNull(((int)PromotionColumn.Description - 1)))?null:(System.String)reader[((int)PromotionColumn.Description - 1)];
			entity.ProfileID = (reader.IsDBNull(((int)PromotionColumn.ProfileID - 1)))?null:(System.Int32?)reader[((int)PromotionColumn.ProfileID - 1)];
			entity.AccountID = (reader.IsDBNull(((int)PromotionColumn.AccountID - 1)))?null:(System.Int32?)reader[((int)PromotionColumn.AccountID - 1)];
			entity.ProductID = (reader.IsDBNull(((int)PromotionColumn.ProductID - 1)))?null:(System.Int32?)reader[((int)PromotionColumn.ProductID - 1)];
			entity.AddOnValueID = (reader.IsDBNull(((int)PromotionColumn.AddOnValueID - 1)))?null:(System.Int32?)reader[((int)PromotionColumn.AddOnValueID - 1)];
			entity.SKUID = (reader.IsDBNull(((int)PromotionColumn.SKUID - 1)))?null:(System.Int32?)reader[((int)PromotionColumn.SKUID - 1)];
			entity.DiscountTypeID = (System.Int32)reader[((int)PromotionColumn.DiscountTypeID - 1)];
			entity.Discount = (System.Decimal)reader[((int)PromotionColumn.Discount - 1)];
			entity.StartDate = (System.DateTime)reader[((int)PromotionColumn.StartDate - 1)];
			entity.EndDate = (System.DateTime)reader[((int)PromotionColumn.EndDate - 1)];
			entity.CouponInd = (System.Boolean)reader[((int)PromotionColumn.CouponInd - 1)];
			entity.CouponCode = (reader.IsDBNull(((int)PromotionColumn.CouponCode - 1)))?null:(System.String)reader[((int)PromotionColumn.CouponCode - 1)];
			entity.CouponQuantityAvailable = (reader.IsDBNull(((int)PromotionColumn.CouponQuantityAvailable - 1)))?null:(System.Int32?)reader[((int)PromotionColumn.CouponQuantityAvailable - 1)];
			entity.PromotionMessage = (reader.IsDBNull(((int)PromotionColumn.PromotionMessage - 1)))?null:(System.String)reader[((int)PromotionColumn.PromotionMessage - 1)];
			entity.OrderMinimum = (reader.IsDBNull(((int)PromotionColumn.OrderMinimum - 1)))?null:(System.Decimal?)reader[((int)PromotionColumn.OrderMinimum - 1)];
			entity.QuantityMinimum = (reader.IsDBNull(((int)PromotionColumn.QuantityMinimum - 1)))?null:(System.Int32?)reader[((int)PromotionColumn.QuantityMinimum - 1)];
			entity.PromotionProductQty = (reader.IsDBNull(((int)PromotionColumn.PromotionProductQty - 1)))?null:(System.Int32?)reader[((int)PromotionColumn.PromotionProductQty - 1)];
			entity.PromotionProductID = (reader.IsDBNull(((int)PromotionColumn.PromotionProductID - 1)))?null:(System.Int32?)reader[((int)PromotionColumn.PromotionProductID - 1)];
			entity.DisplayOrder = (System.Int32)reader[((int)PromotionColumn.DisplayOrder - 1)];
			entity.Custom1 = (reader.IsDBNull(((int)PromotionColumn.Custom1 - 1)))?null:(System.String)reader[((int)PromotionColumn.Custom1 - 1)];
			entity.Custom2 = (reader.IsDBNull(((int)PromotionColumn.Custom2 - 1)))?null:(System.String)reader[((int)PromotionColumn.Custom2 - 1)];
			entity.Custom3 = (reader.IsDBNull(((int)PromotionColumn.Custom3 - 1)))?null:(System.String)reader[((int)PromotionColumn.Custom3 - 1)];
			entity.EnableCouponUrl = (reader.IsDBNull(((int)PromotionColumn.EnableCouponUrl - 1)))?null:(System.Boolean?)reader[((int)PromotionColumn.EnableCouponUrl - 1)];
			entity.PromoCode = (reader.IsDBNull(((int)PromotionColumn.PromoCode - 1)))?null:(System.String)reader[((int)PromotionColumn.PromoCode - 1)];
			entity.ExternalID = (reader.IsDBNull(((int)PromotionColumn.ExternalID - 1)))?null:(System.String)reader[((int)PromotionColumn.ExternalID - 1)];
			entity.PortalID = (reader.IsDBNull(((int)PromotionColumn.PortalID - 1)))?null:(System.Int32?)reader[((int)PromotionColumn.PortalID - 1)];
			entity.CategoryID = (reader.IsDBNull(((int)PromotionColumn.CategoryID - 1)))?null:(System.Int32?)reader[((int)PromotionColumn.CategoryID - 1)];
			entity.ManufacturerID = (reader.IsDBNull(((int)PromotionColumn.ManufacturerID - 1)))?null:(System.Int32?)reader[((int)PromotionColumn.ManufacturerID - 1)];
			entity.CatalogID = (reader.IsDBNull(((int)PromotionColumn.CatalogID - 1)))?null:(System.Int32?)reader[((int)PromotionColumn.CatalogID - 1)];
			entity.IsCouponAllowedWithOtherCoupons = (System.Boolean)reader[((int)PromotionColumn.IsCouponAllowedWithOtherCoupons - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.Promotion"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.Promotion"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.Promotion entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.PromotionID = (System.Int32)dataRow["PromotionID"];
			entity.Name = (System.String)dataRow["Name"];
			entity.Description = Convert.IsDBNull(dataRow["Description"]) ? null : (System.String)dataRow["Description"];
			entity.ProfileID = Convert.IsDBNull(dataRow["ProfileID"]) ? null : (System.Int32?)dataRow["ProfileID"];
			entity.AccountID = Convert.IsDBNull(dataRow["AccountID"]) ? null : (System.Int32?)dataRow["AccountID"];
			entity.ProductID = Convert.IsDBNull(dataRow["ProductID"]) ? null : (System.Int32?)dataRow["ProductID"];
			entity.AddOnValueID = Convert.IsDBNull(dataRow["AddOnValueID"]) ? null : (System.Int32?)dataRow["AddOnValueID"];
			entity.SKUID = Convert.IsDBNull(dataRow["SKUID"]) ? null : (System.Int32?)dataRow["SKUID"];
			entity.DiscountTypeID = (System.Int32)dataRow["DiscountTypeID"];
			entity.Discount = (System.Decimal)dataRow["Discount"];
			entity.StartDate = (System.DateTime)dataRow["StartDate"];
			entity.EndDate = (System.DateTime)dataRow["EndDate"];
			entity.CouponInd = (System.Boolean)dataRow["CouponInd"];
			entity.CouponCode = Convert.IsDBNull(dataRow["CouponCode"]) ? null : (System.String)dataRow["CouponCode"];
			entity.CouponQuantityAvailable = Convert.IsDBNull(dataRow["CouponQuantityAvailable"]) ? null : (System.Int32?)dataRow["CouponQuantityAvailable"];
			entity.PromotionMessage = Convert.IsDBNull(dataRow["PromotionMessage"]) ? null : (System.String)dataRow["PromotionMessage"];
			entity.OrderMinimum = Convert.IsDBNull(dataRow["OrderMinimum"]) ? null : (System.Decimal?)dataRow["OrderMinimum"];
			entity.QuantityMinimum = Convert.IsDBNull(dataRow["QuantityMinimum"]) ? null : (System.Int32?)dataRow["QuantityMinimum"];
			entity.PromotionProductQty = Convert.IsDBNull(dataRow["PromotionProductQty"]) ? null : (System.Int32?)dataRow["PromotionProductQty"];
			entity.PromotionProductID = Convert.IsDBNull(dataRow["PromotionProductID"]) ? null : (System.Int32?)dataRow["PromotionProductID"];
			entity.DisplayOrder = (System.Int32)dataRow["DisplayOrder"];
			entity.Custom1 = Convert.IsDBNull(dataRow["Custom1"]) ? null : (System.String)dataRow["Custom1"];
			entity.Custom2 = Convert.IsDBNull(dataRow["Custom2"]) ? null : (System.String)dataRow["Custom2"];
			entity.Custom3 = Convert.IsDBNull(dataRow["Custom3"]) ? null : (System.String)dataRow["Custom3"];
			entity.EnableCouponUrl = Convert.IsDBNull(dataRow["EnableCouponUrl"]) ? null : (System.Boolean?)dataRow["EnableCouponUrl"];
			entity.PromoCode = Convert.IsDBNull(dataRow["PromoCode"]) ? null : (System.String)dataRow["PromoCode"];
			entity.ExternalID = Convert.IsDBNull(dataRow["ExternalID"]) ? null : (System.String)dataRow["ExternalID"];
			entity.PortalID = Convert.IsDBNull(dataRow["PortalID"]) ? null : (System.Int32?)dataRow["PortalID"];
			entity.CategoryID = Convert.IsDBNull(dataRow["CategoryID"]) ? null : (System.Int32?)dataRow["CategoryID"];
			entity.ManufacturerID = Convert.IsDBNull(dataRow["ManufacturerID"]) ? null : (System.Int32?)dataRow["ManufacturerID"];
			entity.CatalogID = Convert.IsDBNull(dataRow["CatalogID"]) ? null : (System.Int32?)dataRow["CatalogID"];
			entity.IsCouponAllowedWithOtherCoupons = (System.Boolean)dataRow["IsCouponAllowedWithOtherCoupons"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.Promotion"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.Promotion Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.Promotion entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region AccountIDSource	
			if (CanDeepLoad(entity, "Account|AccountIDSource", deepLoadType, innerList) 
				&& entity.AccountIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.AccountID ?? (int)0);
				Account tmpEntity = EntityManager.LocateEntity<Account>(EntityLocator.ConstructKeyFromPkItems(typeof(Account), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.AccountIDSource = tmpEntity;
				else
					entity.AccountIDSource = DataRepository.AccountProvider.GetByAccountID(transactionManager, (entity.AccountID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AccountIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.AccountIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.AccountProvider.DeepLoad(transactionManager, entity.AccountIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion AccountIDSource

			#region AddOnValueIDSource	
			if (CanDeepLoad(entity, "AddOnValue|AddOnValueIDSource", deepLoadType, innerList) 
				&& entity.AddOnValueIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.AddOnValueID ?? (int)0);
				AddOnValue tmpEntity = EntityManager.LocateEntity<AddOnValue>(EntityLocator.ConstructKeyFromPkItems(typeof(AddOnValue), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.AddOnValueIDSource = tmpEntity;
				else
					entity.AddOnValueIDSource = DataRepository.AddOnValueProvider.GetByAddOnValueID(transactionManager, (entity.AddOnValueID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AddOnValueIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.AddOnValueIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.AddOnValueProvider.DeepLoad(transactionManager, entity.AddOnValueIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion AddOnValueIDSource

			#region CatalogIDSource	
			if (CanDeepLoad(entity, "Catalog|CatalogIDSource", deepLoadType, innerList) 
				&& entity.CatalogIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.CatalogID ?? (int)0);
				Catalog tmpEntity = EntityManager.LocateEntity<Catalog>(EntityLocator.ConstructKeyFromPkItems(typeof(Catalog), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CatalogIDSource = tmpEntity;
				else
					entity.CatalogIDSource = DataRepository.CatalogProvider.GetByCatalogID(transactionManager, (entity.CatalogID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CatalogIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CatalogIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CatalogProvider.DeepLoad(transactionManager, entity.CatalogIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CatalogIDSource

			#region CategoryIDSource	
			if (CanDeepLoad(entity, "Category|CategoryIDSource", deepLoadType, innerList) 
				&& entity.CategoryIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.CategoryID ?? (int)0);
				Category tmpEntity = EntityManager.LocateEntity<Category>(EntityLocator.ConstructKeyFromPkItems(typeof(Category), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CategoryIDSource = tmpEntity;
				else
					entity.CategoryIDSource = DataRepository.CategoryProvider.GetByCategoryID(transactionManager, (entity.CategoryID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CategoryIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CategoryIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CategoryProvider.DeepLoad(transactionManager, entity.CategoryIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CategoryIDSource

			#region DiscountTypeIDSource	
			if (CanDeepLoad(entity, "DiscountType|DiscountTypeIDSource", deepLoadType, innerList) 
				&& entity.DiscountTypeIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.DiscountTypeID;
				DiscountType tmpEntity = EntityManager.LocateEntity<DiscountType>(EntityLocator.ConstructKeyFromPkItems(typeof(DiscountType), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.DiscountTypeIDSource = tmpEntity;
				else
					entity.DiscountTypeIDSource = DataRepository.DiscountTypeProvider.GetByDiscountTypeID(transactionManager, entity.DiscountTypeID);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'DiscountTypeIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.DiscountTypeIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.DiscountTypeProvider.DeepLoad(transactionManager, entity.DiscountTypeIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion DiscountTypeIDSource

			#region ManufacturerIDSource	
			if (CanDeepLoad(entity, "Manufacturer|ManufacturerIDSource", deepLoadType, innerList) 
				&& entity.ManufacturerIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.ManufacturerID ?? (int)0);
				Manufacturer tmpEntity = EntityManager.LocateEntity<Manufacturer>(EntityLocator.ConstructKeyFromPkItems(typeof(Manufacturer), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ManufacturerIDSource = tmpEntity;
				else
					entity.ManufacturerIDSource = DataRepository.ManufacturerProvider.GetByManufacturerID(transactionManager, (entity.ManufacturerID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ManufacturerIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ManufacturerIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.ManufacturerProvider.DeepLoad(transactionManager, entity.ManufacturerIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ManufacturerIDSource

			#region PortalIDSource	
			if (CanDeepLoad(entity, "Portal|PortalIDSource", deepLoadType, innerList) 
				&& entity.PortalIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.PortalID ?? (int)0);
				Portal tmpEntity = EntityManager.LocateEntity<Portal>(EntityLocator.ConstructKeyFromPkItems(typeof(Portal), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.PortalIDSource = tmpEntity;
				else
					entity.PortalIDSource = DataRepository.PortalProvider.GetByPortalID(transactionManager, (entity.PortalID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PortalIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.PortalIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.PortalProvider.DeepLoad(transactionManager, entity.PortalIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion PortalIDSource

			#region ProductIDSource	
			if (CanDeepLoad(entity, "Product|ProductIDSource", deepLoadType, innerList) 
				&& entity.ProductIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.ProductID ?? (int)0);
				Product tmpEntity = EntityManager.LocateEntity<Product>(EntityLocator.ConstructKeyFromPkItems(typeof(Product), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ProductIDSource = tmpEntity;
				else
					entity.ProductIDSource = DataRepository.ProductProvider.GetByProductID(transactionManager, (entity.ProductID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ProductIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ProductIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.ProductProvider.DeepLoad(transactionManager, entity.ProductIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ProductIDSource

			#region ProfileIDSource	
			if (CanDeepLoad(entity, "Profile|ProfileIDSource", deepLoadType, innerList) 
				&& entity.ProfileIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.ProfileID ?? (int)0);
				Profile tmpEntity = EntityManager.LocateEntity<Profile>(EntityLocator.ConstructKeyFromPkItems(typeof(Profile), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ProfileIDSource = tmpEntity;
				else
					entity.ProfileIDSource = DataRepository.ProfileProvider.GetByProfileID(transactionManager, (entity.ProfileID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ProfileIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ProfileIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.ProfileProvider.DeepLoad(transactionManager, entity.ProfileIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ProfileIDSource

			#region SKUIDSource	
			if (CanDeepLoad(entity, "SKU|SKUIDSource", deepLoadType, innerList) 
				&& entity.SKUIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.SKUID ?? (int)0);
				SKU tmpEntity = EntityManager.LocateEntity<SKU>(EntityLocator.ConstructKeyFromPkItems(typeof(SKU), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.SKUIDSource = tmpEntity;
				else
					entity.SKUIDSource = DataRepository.SKUProvider.GetBySKUID(transactionManager, (entity.SKUID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SKUIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.SKUIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.SKUProvider.DeepLoad(transactionManager, entity.SKUIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion SKUIDSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.Promotion object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.Promotion instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.Promotion Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.Promotion entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region AccountIDSource
			if (CanDeepSave(entity, "Account|AccountIDSource", deepSaveType, innerList) 
				&& entity.AccountIDSource != null)
			{
				DataRepository.AccountProvider.Save(transactionManager, entity.AccountIDSource);
				entity.AccountID = entity.AccountIDSource.AccountID;
			}
			#endregion 
			
			#region AddOnValueIDSource
			if (CanDeepSave(entity, "AddOnValue|AddOnValueIDSource", deepSaveType, innerList) 
				&& entity.AddOnValueIDSource != null)
			{
				DataRepository.AddOnValueProvider.Save(transactionManager, entity.AddOnValueIDSource);
				entity.AddOnValueID = entity.AddOnValueIDSource.AddOnValueID;
			}
			#endregion 
			
			#region CatalogIDSource
			if (CanDeepSave(entity, "Catalog|CatalogIDSource", deepSaveType, innerList) 
				&& entity.CatalogIDSource != null)
			{
				DataRepository.CatalogProvider.Save(transactionManager, entity.CatalogIDSource);
				entity.CatalogID = entity.CatalogIDSource.CatalogID;
			}
			#endregion 
			
			#region CategoryIDSource
			if (CanDeepSave(entity, "Category|CategoryIDSource", deepSaveType, innerList) 
				&& entity.CategoryIDSource != null)
			{
				DataRepository.CategoryProvider.Save(transactionManager, entity.CategoryIDSource);
				entity.CategoryID = entity.CategoryIDSource.CategoryID;
			}
			#endregion 
			
			#region DiscountTypeIDSource
			if (CanDeepSave(entity, "DiscountType|DiscountTypeIDSource", deepSaveType, innerList) 
				&& entity.DiscountTypeIDSource != null)
			{
				DataRepository.DiscountTypeProvider.Save(transactionManager, entity.DiscountTypeIDSource);
				entity.DiscountTypeID = entity.DiscountTypeIDSource.DiscountTypeID;
			}
			#endregion 
			
			#region ManufacturerIDSource
			if (CanDeepSave(entity, "Manufacturer|ManufacturerIDSource", deepSaveType, innerList) 
				&& entity.ManufacturerIDSource != null)
			{
				DataRepository.ManufacturerProvider.Save(transactionManager, entity.ManufacturerIDSource);
				entity.ManufacturerID = entity.ManufacturerIDSource.ManufacturerID;
			}
			#endregion 
			
			#region PortalIDSource
			if (CanDeepSave(entity, "Portal|PortalIDSource", deepSaveType, innerList) 
				&& entity.PortalIDSource != null)
			{
				DataRepository.PortalProvider.Save(transactionManager, entity.PortalIDSource);
				entity.PortalID = entity.PortalIDSource.PortalID;
			}
			#endregion 
			
			#region ProductIDSource
			if (CanDeepSave(entity, "Product|ProductIDSource", deepSaveType, innerList) 
				&& entity.ProductIDSource != null)
			{
				DataRepository.ProductProvider.Save(transactionManager, entity.ProductIDSource);
				entity.ProductID = entity.ProductIDSource.ProductID;
			}
			#endregion 
			
			#region ProfileIDSource
			if (CanDeepSave(entity, "Profile|ProfileIDSource", deepSaveType, innerList) 
				&& entity.ProfileIDSource != null)
			{
				DataRepository.ProfileProvider.Save(transactionManager, entity.ProfileIDSource);
				entity.ProfileID = entity.ProfileIDSource.ProfileID;
			}
			#endregion 
			
			#region SKUIDSource
			if (CanDeepSave(entity, "SKU|SKUIDSource", deepSaveType, innerList) 
				&& entity.SKUIDSource != null)
			{
				DataRepository.SKUProvider.Save(transactionManager, entity.SKUIDSource);
				entity.SKUID = entity.SKUIDSource.SKUID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region PromotionChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.Promotion</c>
	///</summary>
	public enum PromotionChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Account</c> at AccountIDSource
		///</summary>
		[ChildEntityType(typeof(Account))]
		Account,
		
		///<summary>
		/// Composite Property for <c>AddOnValue</c> at AddOnValueIDSource
		///</summary>
		[ChildEntityType(typeof(AddOnValue))]
		AddOnValue,
		
		///<summary>
		/// Composite Property for <c>Catalog</c> at CatalogIDSource
		///</summary>
		[ChildEntityType(typeof(Catalog))]
		Catalog,
		
		///<summary>
		/// Composite Property for <c>Category</c> at CategoryIDSource
		///</summary>
		[ChildEntityType(typeof(Category))]
		Category,
		
		///<summary>
		/// Composite Property for <c>DiscountType</c> at DiscountTypeIDSource
		///</summary>
		[ChildEntityType(typeof(DiscountType))]
		DiscountType,
		
		///<summary>
		/// Composite Property for <c>Manufacturer</c> at ManufacturerIDSource
		///</summary>
		[ChildEntityType(typeof(Manufacturer))]
		Manufacturer,
		
		///<summary>
		/// Composite Property for <c>Portal</c> at PortalIDSource
		///</summary>
		[ChildEntityType(typeof(Portal))]
		Portal,
		
		///<summary>
		/// Composite Property for <c>Product</c> at ProductIDSource
		///</summary>
		[ChildEntityType(typeof(Product))]
		Product,
		
		///<summary>
		/// Composite Property for <c>Profile</c> at ProfileIDSource
		///</summary>
		[ChildEntityType(typeof(Profile))]
		Profile,
		
		///<summary>
		/// Composite Property for <c>SKU</c> at SKUIDSource
		///</summary>
		[ChildEntityType(typeof(SKU))]
		SKU,
	}
	
	#endregion PromotionChildEntityTypes
	
	#region PromotionFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;PromotionColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Promotion"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PromotionFilterBuilder : SqlFilterBuilder<PromotionColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PromotionFilterBuilder class.
		/// </summary>
		public PromotionFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the PromotionFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PromotionFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PromotionFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PromotionFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PromotionFilterBuilder
	
	#region PromotionParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;PromotionColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Promotion"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PromotionParameterBuilder : ParameterizedSqlFilterBuilder<PromotionColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PromotionParameterBuilder class.
		/// </summary>
		public PromotionParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the PromotionParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PromotionParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PromotionParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PromotionParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PromotionParameterBuilder
	
	#region PromotionSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;PromotionColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Promotion"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class PromotionSortBuilder : SqlSortBuilder<PromotionColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PromotionSqlSortBuilder class.
		/// </summary>
		public PromotionSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion PromotionSortBuilder
	
} // end namespace
