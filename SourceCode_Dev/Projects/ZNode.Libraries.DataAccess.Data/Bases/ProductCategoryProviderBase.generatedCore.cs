﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ProductCategoryProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class ProductCategoryProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.ProductCategory, ZNode.Libraries.DataAccess.Entities.ProductCategoryKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ProductCategoryKey key)
		{
			return Delete(transactionManager, key.ProductCategoryID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_productCategoryID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _productCategoryID)
		{
			return Delete(null, _productCategoryID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productCategoryID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _productCategoryID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeProd__CSSID__2B8A53B1 key.
		///		FK__ZNodeProd__CSSID__2B8A53B1 Description: 
		/// </summary>
		/// <param name="_cSSID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductCategory objects.</returns>
		public TList<ProductCategory> GetByCSSID(System.Int32? _cSSID)
		{
			int count = -1;
			return GetByCSSID(_cSSID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeProd__CSSID__2B8A53B1 key.
		///		FK__ZNodeProd__CSSID__2B8A53B1 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_cSSID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductCategory objects.</returns>
		/// <remarks></remarks>
		public TList<ProductCategory> GetByCSSID(TransactionManager transactionManager, System.Int32? _cSSID)
		{
			int count = -1;
			return GetByCSSID(transactionManager, _cSSID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeProd__CSSID__2B8A53B1 key.
		///		FK__ZNodeProd__CSSID__2B8A53B1 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_cSSID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductCategory objects.</returns>
		public TList<ProductCategory> GetByCSSID(TransactionManager transactionManager, System.Int32? _cSSID, int start, int pageLength)
		{
			int count = -1;
			return GetByCSSID(transactionManager, _cSSID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeProd__CSSID__2B8A53B1 key.
		///		fKZNodeProdCSSID2B8A53B1 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_cSSID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductCategory objects.</returns>
		public TList<ProductCategory> GetByCSSID(System.Int32? _cSSID, int start, int pageLength)
		{
			int count =  -1;
			return GetByCSSID(null, _cSSID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeProd__CSSID__2B8A53B1 key.
		///		fKZNodeProdCSSID2B8A53B1 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_cSSID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductCategory objects.</returns>
		public TList<ProductCategory> GetByCSSID(System.Int32? _cSSID, int start, int pageLength,out int count)
		{
			return GetByCSSID(null, _cSSID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeProd__CSSID__2B8A53B1 key.
		///		FK__ZNodeProd__CSSID__2B8A53B1 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_cSSID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductCategory objects.</returns>
		public abstract TList<ProductCategory> GetByCSSID(TransactionManager transactionManager, System.Int32? _cSSID, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeProd__Maste__2C7E77EA key.
		///		FK__ZNodeProd__Maste__2C7E77EA Description: 
		/// </summary>
		/// <param name="_masterPageID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductCategory objects.</returns>
		public TList<ProductCategory> GetByMasterPageID(System.Int32? _masterPageID)
		{
			int count = -1;
			return GetByMasterPageID(_masterPageID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeProd__Maste__2C7E77EA key.
		///		FK__ZNodeProd__Maste__2C7E77EA Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_masterPageID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductCategory objects.</returns>
		/// <remarks></remarks>
		public TList<ProductCategory> GetByMasterPageID(TransactionManager transactionManager, System.Int32? _masterPageID)
		{
			int count = -1;
			return GetByMasterPageID(transactionManager, _masterPageID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeProd__Maste__2C7E77EA key.
		///		FK__ZNodeProd__Maste__2C7E77EA Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_masterPageID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductCategory objects.</returns>
		public TList<ProductCategory> GetByMasterPageID(TransactionManager transactionManager, System.Int32? _masterPageID, int start, int pageLength)
		{
			int count = -1;
			return GetByMasterPageID(transactionManager, _masterPageID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeProd__Maste__2C7E77EA key.
		///		fKZNodeProdMaste2C7E77EA Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_masterPageID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductCategory objects.</returns>
		public TList<ProductCategory> GetByMasterPageID(System.Int32? _masterPageID, int start, int pageLength)
		{
			int count =  -1;
			return GetByMasterPageID(null, _masterPageID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeProd__Maste__2C7E77EA key.
		///		fKZNodeProdMaste2C7E77EA Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_masterPageID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductCategory objects.</returns>
		public TList<ProductCategory> GetByMasterPageID(System.Int32? _masterPageID, int start, int pageLength,out int count)
		{
			return GetByMasterPageID(null, _masterPageID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeProd__Maste__2C7E77EA key.
		///		FK__ZNodeProd__Maste__2C7E77EA Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_masterPageID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductCategory objects.</returns>
		public abstract TList<ProductCategory> GetByMasterPageID(TransactionManager transactionManager, System.Int32? _masterPageID, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeProd__Theme__2D729C23 key.
		///		FK__ZNodeProd__Theme__2D729C23 Description: 
		/// </summary>
		/// <param name="_themeID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductCategory objects.</returns>
		public TList<ProductCategory> GetByThemeID(System.Int32? _themeID)
		{
			int count = -1;
			return GetByThemeID(_themeID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeProd__Theme__2D729C23 key.
		///		FK__ZNodeProd__Theme__2D729C23 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_themeID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductCategory objects.</returns>
		/// <remarks></remarks>
		public TList<ProductCategory> GetByThemeID(TransactionManager transactionManager, System.Int32? _themeID)
		{
			int count = -1;
			return GetByThemeID(transactionManager, _themeID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeProd__Theme__2D729C23 key.
		///		FK__ZNodeProd__Theme__2D729C23 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_themeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductCategory objects.</returns>
		public TList<ProductCategory> GetByThemeID(TransactionManager transactionManager, System.Int32? _themeID, int start, int pageLength)
		{
			int count = -1;
			return GetByThemeID(transactionManager, _themeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeProd__Theme__2D729C23 key.
		///		fKZNodeProdTheme2D729C23 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_themeID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductCategory objects.</returns>
		public TList<ProductCategory> GetByThemeID(System.Int32? _themeID, int start, int pageLength)
		{
			int count =  -1;
			return GetByThemeID(null, _themeID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeProd__Theme__2D729C23 key.
		///		fKZNodeProdTheme2D729C23 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_themeID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductCategory objects.</returns>
		public TList<ProductCategory> GetByThemeID(System.Int32? _themeID, int start, int pageLength,out int count)
		{
			return GetByThemeID(null, _themeID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeProd__Theme__2D729C23 key.
		///		FK__ZNodeProd__Theme__2D729C23 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_themeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductCategory objects.</returns>
		public abstract TList<ProductCategory> GetByThemeID(TransactionManager transactionManager, System.Int32? _themeID, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.ProductCategory Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ProductCategoryKey key, int start, int pageLength)
		{
			return GetByProductCategoryID(transactionManager, key.ProductCategoryID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeProductCategory_ActiveInd index.
		/// </summary>
		/// <param name="_activeInd"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;ProductCategory&gt;"/> class.</returns>
		public TList<ProductCategory> GetByActiveInd(System.Boolean _activeInd)
		{
			int count = -1;
			return GetByActiveInd(null,_activeInd, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProductCategory_ActiveInd index.
		/// </summary>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ProductCategory&gt;"/> class.</returns>
		public TList<ProductCategory> GetByActiveInd(System.Boolean _activeInd, int start, int pageLength)
		{
			int count = -1;
			return GetByActiveInd(null, _activeInd, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProductCategory_ActiveInd index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_activeInd"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ProductCategory&gt;"/> class.</returns>
		public TList<ProductCategory> GetByActiveInd(TransactionManager transactionManager, System.Boolean _activeInd)
		{
			int count = -1;
			return GetByActiveInd(transactionManager, _activeInd, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProductCategory_ActiveInd index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ProductCategory&gt;"/> class.</returns>
		public TList<ProductCategory> GetByActiveInd(TransactionManager transactionManager, System.Boolean _activeInd, int start, int pageLength)
		{
			int count = -1;
			return GetByActiveInd(transactionManager, _activeInd, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProductCategory_ActiveInd index.
		/// </summary>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ProductCategory&gt;"/> class.</returns>
		public TList<ProductCategory> GetByActiveInd(System.Boolean _activeInd, int start, int pageLength, out int count)
		{
			return GetByActiveInd(null, _activeInd, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProductCategory_ActiveInd index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;ProductCategory&gt;"/> class.</returns>
		public abstract TList<ProductCategory> GetByActiveInd(TransactionManager transactionManager, System.Boolean _activeInd, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeProductCategory_CategoryProductActive index.
		/// </summary>
		/// <param name="_categoryID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;ProductCategory&gt;"/> class.</returns>
		public TList<ProductCategory> GetByCategoryID(System.Int32 _categoryID)
		{
			int count = -1;
			return GetByCategoryID(null,_categoryID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProductCategory_CategoryProductActive index.
		/// </summary>
		/// <param name="_categoryID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ProductCategory&gt;"/> class.</returns>
		public TList<ProductCategory> GetByCategoryID(System.Int32 _categoryID, int start, int pageLength)
		{
			int count = -1;
			return GetByCategoryID(null, _categoryID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProductCategory_CategoryProductActive index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_categoryID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ProductCategory&gt;"/> class.</returns>
		public TList<ProductCategory> GetByCategoryID(TransactionManager transactionManager, System.Int32 _categoryID)
		{
			int count = -1;
			return GetByCategoryID(transactionManager, _categoryID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProductCategory_CategoryProductActive index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_categoryID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ProductCategory&gt;"/> class.</returns>
		public TList<ProductCategory> GetByCategoryID(TransactionManager transactionManager, System.Int32 _categoryID, int start, int pageLength)
		{
			int count = -1;
			return GetByCategoryID(transactionManager, _categoryID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProductCategory_CategoryProductActive index.
		/// </summary>
		/// <param name="_categoryID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ProductCategory&gt;"/> class.</returns>
		public TList<ProductCategory> GetByCategoryID(System.Int32 _categoryID, int start, int pageLength, out int count)
		{
			return GetByCategoryID(null, _categoryID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProductCategory_CategoryProductActive index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_categoryID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;ProductCategory&gt;"/> class.</returns>
		public abstract TList<ProductCategory> GetByCategoryID(TransactionManager transactionManager, System.Int32 _categoryID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeProductCategory_ProductCategory index.
		/// </summary>
		/// <param name="_productID"></param>
		/// <param name="_categoryID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductCategory"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductCategory GetByProductIDCategoryID(System.Int32 _productID, System.Int32 _categoryID)
		{
			int count = -1;
			return GetByProductIDCategoryID(null,_productID, _categoryID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProductCategory_ProductCategory index.
		/// </summary>
		/// <param name="_productID"></param>
		/// <param name="_categoryID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductCategory"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductCategory GetByProductIDCategoryID(System.Int32 _productID, System.Int32 _categoryID, int start, int pageLength)
		{
			int count = -1;
			return GetByProductIDCategoryID(null, _productID, _categoryID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProductCategory_ProductCategory index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productID"></param>
		/// <param name="_categoryID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductCategory"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductCategory GetByProductIDCategoryID(TransactionManager transactionManager, System.Int32 _productID, System.Int32 _categoryID)
		{
			int count = -1;
			return GetByProductIDCategoryID(transactionManager, _productID, _categoryID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProductCategory_ProductCategory index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productID"></param>
		/// <param name="_categoryID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductCategory"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductCategory GetByProductIDCategoryID(TransactionManager transactionManager, System.Int32 _productID, System.Int32 _categoryID, int start, int pageLength)
		{
			int count = -1;
			return GetByProductIDCategoryID(transactionManager, _productID, _categoryID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProductCategory_ProductCategory index.
		/// </summary>
		/// <param name="_productID"></param>
		/// <param name="_categoryID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductCategory"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductCategory GetByProductIDCategoryID(System.Int32 _productID, System.Int32 _categoryID, int start, int pageLength, out int count)
		{
			return GetByProductIDCategoryID(null, _productID, _categoryID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProductCategory_ProductCategory index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productID"></param>
		/// <param name="_categoryID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductCategory"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.ProductCategory GetByProductIDCategoryID(TransactionManager transactionManager, System.Int32 _productID, System.Int32 _categoryID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeProductCategory_ProductID index.
		/// </summary>
		/// <param name="_productID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;ProductCategory&gt;"/> class.</returns>
		public TList<ProductCategory> GetByProductID(System.Int32 _productID)
		{
			int count = -1;
			return GetByProductID(null,_productID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProductCategory_ProductID index.
		/// </summary>
		/// <param name="_productID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ProductCategory&gt;"/> class.</returns>
		public TList<ProductCategory> GetByProductID(System.Int32 _productID, int start, int pageLength)
		{
			int count = -1;
			return GetByProductID(null, _productID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProductCategory_ProductID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ProductCategory&gt;"/> class.</returns>
		public TList<ProductCategory> GetByProductID(TransactionManager transactionManager, System.Int32 _productID)
		{
			int count = -1;
			return GetByProductID(transactionManager, _productID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProductCategory_ProductID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ProductCategory&gt;"/> class.</returns>
		public TList<ProductCategory> GetByProductID(TransactionManager transactionManager, System.Int32 _productID, int start, int pageLength)
		{
			int count = -1;
			return GetByProductID(transactionManager, _productID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProductCategory_ProductID index.
		/// </summary>
		/// <param name="_productID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ProductCategory&gt;"/> class.</returns>
		public TList<ProductCategory> GetByProductID(System.Int32 _productID, int start, int pageLength, out int count)
		{
			return GetByProductID(null, _productID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProductCategory_ProductID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;ProductCategory&gt;"/> class.</returns>
		public abstract TList<ProductCategory> GetByProductID(TransactionManager transactionManager, System.Int32 _productID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ProductCategory index.
		/// </summary>
		/// <param name="_productCategoryID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductCategory"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductCategory GetByProductCategoryID(System.Int32 _productCategoryID)
		{
			int count = -1;
			return GetByProductCategoryID(null,_productCategoryID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ProductCategory index.
		/// </summary>
		/// <param name="_productCategoryID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductCategory"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductCategory GetByProductCategoryID(System.Int32 _productCategoryID, int start, int pageLength)
		{
			int count = -1;
			return GetByProductCategoryID(null, _productCategoryID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ProductCategory index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productCategoryID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductCategory"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductCategory GetByProductCategoryID(TransactionManager transactionManager, System.Int32 _productCategoryID)
		{
			int count = -1;
			return GetByProductCategoryID(transactionManager, _productCategoryID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ProductCategory index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productCategoryID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductCategory"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductCategory GetByProductCategoryID(TransactionManager transactionManager, System.Int32 _productCategoryID, int start, int pageLength)
		{
			int count = -1;
			return GetByProductCategoryID(transactionManager, _productCategoryID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ProductCategory index.
		/// </summary>
		/// <param name="_productCategoryID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductCategory"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductCategory GetByProductCategoryID(System.Int32 _productCategoryID, int start, int pageLength, out int count)
		{
			return GetByProductCategoryID(null, _productCategoryID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ProductCategory index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productCategoryID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductCategory"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.ProductCategory GetByProductCategoryID(TransactionManager transactionManager, System.Int32 _productCategoryID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;ProductCategory&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;ProductCategory&gt;"/></returns>
		public static TList<ProductCategory> Fill(IDataReader reader, TList<ProductCategory> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.ProductCategory c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("ProductCategory")
					.Append("|").Append((System.Int32)reader[((int)ProductCategoryColumn.ProductCategoryID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<ProductCategory>(
					key.ToString(), // EntityTrackingKey
					"ProductCategory",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.ProductCategory();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.ProductCategoryID = (System.Int32)reader[((int)ProductCategoryColumn.ProductCategoryID - 1)];
					c.ProductID = (System.Int32)reader[((int)ProductCategoryColumn.ProductID - 1)];
					c.CategoryID = (System.Int32)reader[((int)ProductCategoryColumn.CategoryID - 1)];
					c.BeginDate = (reader.IsDBNull(((int)ProductCategoryColumn.BeginDate - 1)))?null:(System.DateTime?)reader[((int)ProductCategoryColumn.BeginDate - 1)];
					c.EndDate = (reader.IsDBNull(((int)ProductCategoryColumn.EndDate - 1)))?null:(System.DateTime?)reader[((int)ProductCategoryColumn.EndDate - 1)];
					c.DisplayOrder = (reader.IsDBNull(((int)ProductCategoryColumn.DisplayOrder - 1)))?null:(System.Int32?)reader[((int)ProductCategoryColumn.DisplayOrder - 1)];
					c.ActiveInd = (System.Boolean)reader[((int)ProductCategoryColumn.ActiveInd - 1)];
					c.ThemeID = (reader.IsDBNull(((int)ProductCategoryColumn.ThemeID - 1)))?null:(System.Int32?)reader[((int)ProductCategoryColumn.ThemeID - 1)];
					c.CSSID = (reader.IsDBNull(((int)ProductCategoryColumn.CSSID - 1)))?null:(System.Int32?)reader[((int)ProductCategoryColumn.CSSID - 1)];
					c.MasterPageID = (reader.IsDBNull(((int)ProductCategoryColumn.MasterPageID - 1)))?null:(System.Int32?)reader[((int)ProductCategoryColumn.MasterPageID - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.ProductCategory"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ProductCategory"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.ProductCategory entity)
		{
			if (!reader.Read()) return;
			
			entity.ProductCategoryID = (System.Int32)reader[((int)ProductCategoryColumn.ProductCategoryID - 1)];
			entity.ProductID = (System.Int32)reader[((int)ProductCategoryColumn.ProductID - 1)];
			entity.CategoryID = (System.Int32)reader[((int)ProductCategoryColumn.CategoryID - 1)];
			entity.BeginDate = (reader.IsDBNull(((int)ProductCategoryColumn.BeginDate - 1)))?null:(System.DateTime?)reader[((int)ProductCategoryColumn.BeginDate - 1)];
			entity.EndDate = (reader.IsDBNull(((int)ProductCategoryColumn.EndDate - 1)))?null:(System.DateTime?)reader[((int)ProductCategoryColumn.EndDate - 1)];
			entity.DisplayOrder = (reader.IsDBNull(((int)ProductCategoryColumn.DisplayOrder - 1)))?null:(System.Int32?)reader[((int)ProductCategoryColumn.DisplayOrder - 1)];
			entity.ActiveInd = (System.Boolean)reader[((int)ProductCategoryColumn.ActiveInd - 1)];
			entity.ThemeID = (reader.IsDBNull(((int)ProductCategoryColumn.ThemeID - 1)))?null:(System.Int32?)reader[((int)ProductCategoryColumn.ThemeID - 1)];
			entity.CSSID = (reader.IsDBNull(((int)ProductCategoryColumn.CSSID - 1)))?null:(System.Int32?)reader[((int)ProductCategoryColumn.CSSID - 1)];
			entity.MasterPageID = (reader.IsDBNull(((int)ProductCategoryColumn.MasterPageID - 1)))?null:(System.Int32?)reader[((int)ProductCategoryColumn.MasterPageID - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.ProductCategory"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ProductCategory"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.ProductCategory entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.ProductCategoryID = (System.Int32)dataRow["ProductCategoryID"];
			entity.ProductID = (System.Int32)dataRow["ProductID"];
			entity.CategoryID = (System.Int32)dataRow["CategoryID"];
			entity.BeginDate = Convert.IsDBNull(dataRow["BeginDate"]) ? null : (System.DateTime?)dataRow["BeginDate"];
			entity.EndDate = Convert.IsDBNull(dataRow["EndDate"]) ? null : (System.DateTime?)dataRow["EndDate"];
			entity.DisplayOrder = Convert.IsDBNull(dataRow["DisplayOrder"]) ? null : (System.Int32?)dataRow["DisplayOrder"];
			entity.ActiveInd = (System.Boolean)dataRow["ActiveInd"];
			entity.ThemeID = Convert.IsDBNull(dataRow["ThemeID"]) ? null : (System.Int32?)dataRow["ThemeID"];
			entity.CSSID = Convert.IsDBNull(dataRow["CSSID"]) ? null : (System.Int32?)dataRow["CSSID"];
			entity.MasterPageID = Convert.IsDBNull(dataRow["MasterPageID"]) ? null : (System.Int32?)dataRow["MasterPageID"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ProductCategory"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.ProductCategory Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ProductCategory entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region CSSIDSource	
			if (CanDeepLoad(entity, "CSS|CSSIDSource", deepLoadType, innerList) 
				&& entity.CSSIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.CSSID ?? (int)0);
				CSS tmpEntity = EntityManager.LocateEntity<CSS>(EntityLocator.ConstructKeyFromPkItems(typeof(CSS), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CSSIDSource = tmpEntity;
				else
					entity.CSSIDSource = DataRepository.CSSProvider.GetByCSSID(transactionManager, (entity.CSSID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CSSIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CSSIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CSSProvider.DeepLoad(transactionManager, entity.CSSIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CSSIDSource

			#region MasterPageIDSource	
			if (CanDeepLoad(entity, "MasterPage|MasterPageIDSource", deepLoadType, innerList) 
				&& entity.MasterPageIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.MasterPageID ?? (int)0);
				MasterPage tmpEntity = EntityManager.LocateEntity<MasterPage>(EntityLocator.ConstructKeyFromPkItems(typeof(MasterPage), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.MasterPageIDSource = tmpEntity;
				else
					entity.MasterPageIDSource = DataRepository.MasterPageProvider.GetByMasterPageID(transactionManager, (entity.MasterPageID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'MasterPageIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.MasterPageIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.MasterPageProvider.DeepLoad(transactionManager, entity.MasterPageIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion MasterPageIDSource

			#region ThemeIDSource	
			if (CanDeepLoad(entity, "Theme|ThemeIDSource", deepLoadType, innerList) 
				&& entity.ThemeIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.ThemeID ?? (int)0);
				Theme tmpEntity = EntityManager.LocateEntity<Theme>(EntityLocator.ConstructKeyFromPkItems(typeof(Theme), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ThemeIDSource = tmpEntity;
				else
					entity.ThemeIDSource = DataRepository.ThemeProvider.GetByThemeID(transactionManager, (entity.ThemeID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ThemeIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ThemeIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.ThemeProvider.DeepLoad(transactionManager, entity.ThemeIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ThemeIDSource

			#region CategoryIDSource	
			if (CanDeepLoad(entity, "Category|CategoryIDSource", deepLoadType, innerList) 
				&& entity.CategoryIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.CategoryID;
				Category tmpEntity = EntityManager.LocateEntity<Category>(EntityLocator.ConstructKeyFromPkItems(typeof(Category), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CategoryIDSource = tmpEntity;
				else
					entity.CategoryIDSource = DataRepository.CategoryProvider.GetByCategoryID(transactionManager, entity.CategoryID);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CategoryIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CategoryIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CategoryProvider.DeepLoad(transactionManager, entity.CategoryIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CategoryIDSource

			#region ProductIDSource	
			if (CanDeepLoad(entity, "Product|ProductIDSource", deepLoadType, innerList) 
				&& entity.ProductIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.ProductID;
				Product tmpEntity = EntityManager.LocateEntity<Product>(EntityLocator.ConstructKeyFromPkItems(typeof(Product), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ProductIDSource = tmpEntity;
				else
					entity.ProductIDSource = DataRepository.ProductProvider.GetByProductID(transactionManager, entity.ProductID);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ProductIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ProductIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.ProductProvider.DeepLoad(transactionManager, entity.ProductIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ProductIDSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByProductCategoryID methods when available
			
			#region LuceneGlobalProductCategoryBoost
			// RelationshipType.OneToOne
			if (CanDeepLoad(entity, "LuceneGlobalProductCategoryBoost|LuceneGlobalProductCategoryBoost", deepLoadType, innerList))
			{
				entity.LuceneGlobalProductCategoryBoost = DataRepository.LuceneGlobalProductCategoryBoostProvider.GetByProductCategoryID(transactionManager, entity.ProductCategoryID);
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'LuceneGlobalProductCategoryBoost' loaded. key " + entity.EntityTrackingKey);
				#endif 

				if (deep && entity.LuceneGlobalProductCategoryBoost != null)
				{
					deepHandles.Add("LuceneGlobalProductCategoryBoost",
						new KeyValuePair<Delegate, object>((DeepLoadSingleHandle< LuceneGlobalProductCategoryBoost >) DataRepository.LuceneGlobalProductCategoryBoostProvider.DeepLoad,
						new object[] { transactionManager, entity.LuceneGlobalProductCategoryBoost, deep, deepLoadType, childTypes, innerList }
					));
				}
			}
			#endregion 
			
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.ProductCategory object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.ProductCategory instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.ProductCategory Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ProductCategory entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region CSSIDSource
			if (CanDeepSave(entity, "CSS|CSSIDSource", deepSaveType, innerList) 
				&& entity.CSSIDSource != null)
			{
				DataRepository.CSSProvider.Save(transactionManager, entity.CSSIDSource);
				entity.CSSID = entity.CSSIDSource.CSSID;
			}
			#endregion 
			
			#region MasterPageIDSource
			if (CanDeepSave(entity, "MasterPage|MasterPageIDSource", deepSaveType, innerList) 
				&& entity.MasterPageIDSource != null)
			{
				DataRepository.MasterPageProvider.Save(transactionManager, entity.MasterPageIDSource);
				entity.MasterPageID = entity.MasterPageIDSource.MasterPageID;
			}
			#endregion 
			
			#region ThemeIDSource
			if (CanDeepSave(entity, "Theme|ThemeIDSource", deepSaveType, innerList) 
				&& entity.ThemeIDSource != null)
			{
				DataRepository.ThemeProvider.Save(transactionManager, entity.ThemeIDSource);
				entity.ThemeID = entity.ThemeIDSource.ThemeID;
			}
			#endregion 
			
			#region CategoryIDSource
			if (CanDeepSave(entity, "Category|CategoryIDSource", deepSaveType, innerList) 
				&& entity.CategoryIDSource != null)
			{
				DataRepository.CategoryProvider.Save(transactionManager, entity.CategoryIDSource);
				entity.CategoryID = entity.CategoryIDSource.CategoryID;
			}
			#endregion 
			
			#region ProductIDSource
			if (CanDeepSave(entity, "Product|ProductIDSource", deepSaveType, innerList) 
				&& entity.ProductIDSource != null)
			{
				DataRepository.ProductProvider.Save(transactionManager, entity.ProductIDSource);
				entity.ProductID = entity.ProductIDSource.ProductID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();

			#region LuceneGlobalProductCategoryBoost
			if (CanDeepSave(entity.LuceneGlobalProductCategoryBoost, "LuceneGlobalProductCategoryBoost|LuceneGlobalProductCategoryBoost", deepSaveType, innerList))
			{

				if (entity.LuceneGlobalProductCategoryBoost != null)
				{
					// update each child parent id with the real parent id (mostly used on insert)

					entity.LuceneGlobalProductCategoryBoost.ProductCategoryID = entity.ProductCategoryID;
					//DataRepository.LuceneGlobalProductCategoryBoostProvider.Save(transactionManager, entity.LuceneGlobalProductCategoryBoost);
					deepHandles.Add("LuceneGlobalProductCategoryBoost",
						new KeyValuePair<Delegate, object>((DeepSaveSingleHandle< LuceneGlobalProductCategoryBoost >) DataRepository.LuceneGlobalProductCategoryBoostProvider.DeepSave,
						new object[] { transactionManager, entity.LuceneGlobalProductCategoryBoost, deepSaveType, childTypes, innerList }
					));
				}
			} 
			#endregion 
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region ProductCategoryChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.ProductCategory</c>
	///</summary>
	public enum ProductCategoryChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>CSS</c> at CSSIDSource
		///</summary>
		[ChildEntityType(typeof(CSS))]
		CSS,
		
		///<summary>
		/// Composite Property for <c>MasterPage</c> at MasterPageIDSource
		///</summary>
		[ChildEntityType(typeof(MasterPage))]
		MasterPage,
		
		///<summary>
		/// Composite Property for <c>Theme</c> at ThemeIDSource
		///</summary>
		[ChildEntityType(typeof(Theme))]
		Theme,
		
		///<summary>
		/// Composite Property for <c>Category</c> at CategoryIDSource
		///</summary>
		[ChildEntityType(typeof(Category))]
		Category,
		
		///<summary>
		/// Composite Property for <c>Product</c> at ProductIDSource
		///</summary>
		[ChildEntityType(typeof(Product))]
		Product,
		///<summary>
		/// Entity <c>LuceneGlobalProductCategoryBoost</c> as OneToOne for LuceneGlobalProductCategoryBoost
		///</summary>
		[ChildEntityType(typeof(LuceneGlobalProductCategoryBoost))]
		LuceneGlobalProductCategoryBoost,
	}
	
	#endregion ProductCategoryChildEntityTypes
	
	#region ProductCategoryFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;ProductCategoryColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ProductCategory"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProductCategoryFilterBuilder : SqlFilterBuilder<ProductCategoryColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductCategoryFilterBuilder class.
		/// </summary>
		public ProductCategoryFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProductCategoryFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProductCategoryFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProductCategoryFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProductCategoryFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProductCategoryFilterBuilder
	
	#region ProductCategoryParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;ProductCategoryColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ProductCategory"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProductCategoryParameterBuilder : ParameterizedSqlFilterBuilder<ProductCategoryColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductCategoryParameterBuilder class.
		/// </summary>
		public ProductCategoryParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProductCategoryParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProductCategoryParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProductCategoryParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProductCategoryParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProductCategoryParameterBuilder
	
	#region ProductCategorySortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;ProductCategoryColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ProductCategory"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ProductCategorySortBuilder : SqlSortBuilder<ProductCategoryColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductCategorySqlSortBuilder class.
		/// </summary>
		public ProductCategorySortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ProductCategorySortBuilder
	
} // end namespace
