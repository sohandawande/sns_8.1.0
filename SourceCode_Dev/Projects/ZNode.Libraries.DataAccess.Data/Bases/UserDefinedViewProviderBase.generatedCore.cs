﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="UserDefinedViewProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class UserDefinedViewProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.UserDefinedView, ZNode.Libraries.DataAccess.Entities.UserDefinedViewKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.UserDefinedViewKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.UserDefinedView Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.UserDefinedViewKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodeUserDefinedView index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.UserDefinedView"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.UserDefinedView GetById(System.Int32 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeUserDefinedView index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.UserDefinedView"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.UserDefinedView GetById(System.Int32 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeUserDefinedView index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.UserDefinedView"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.UserDefinedView GetById(TransactionManager transactionManager, System.Int32 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeUserDefinedView index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.UserDefinedView"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.UserDefinedView GetById(TransactionManager transactionManager, System.Int32 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeUserDefinedView index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.UserDefinedView"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.UserDefinedView GetById(System.Int32 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeUserDefinedView index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.UserDefinedView"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.UserDefinedView GetById(TransactionManager transactionManager, System.Int32 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;UserDefinedView&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;UserDefinedView&gt;"/></returns>
		public static TList<UserDefinedView> Fill(IDataReader reader, TList<UserDefinedView> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.UserDefinedView c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("UserDefinedView")
					.Append("|").Append((System.Int32)reader[((int)UserDefinedViewColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<UserDefinedView>(
					key.ToString(), // EntityTrackingKey
					"UserDefinedView",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.UserDefinedView();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int32)reader[((int)UserDefinedViewColumn.Id - 1)];
					c.UserKey = (reader.IsDBNull(((int)UserDefinedViewColumn.UserKey - 1)))?null:(System.Int32?)reader[((int)UserDefinedViewColumn.UserKey - 1)];
					c.ViewName = (reader.IsDBNull(((int)UserDefinedViewColumn.ViewName - 1)))?null:(System.String)reader[((int)UserDefinedViewColumn.ViewName - 1)];
					c.SourceViewName = (reader.IsDBNull(((int)UserDefinedViewColumn.SourceViewName - 1)))?null:(System.String)reader[((int)UserDefinedViewColumn.SourceViewName - 1)];
					c.SelectedColumns = (reader.IsDBNull(((int)UserDefinedViewColumn.SelectedColumns - 1)))?null:(System.String)reader[((int)UserDefinedViewColumn.SelectedColumns - 1)];
					c.FilterConditionDefinition = (reader.IsDBNull(((int)UserDefinedViewColumn.FilterConditionDefinition - 1)))?null:(System.String)reader[((int)UserDefinedViewColumn.FilterConditionDefinition - 1)];
					c.FilteConditionQuery = (reader.IsDBNull(((int)UserDefinedViewColumn.FilteConditionQuery - 1)))?null:(System.String)reader[((int)UserDefinedViewColumn.FilteConditionQuery - 1)];
					c.IsShared = (reader.IsDBNull(((int)UserDefinedViewColumn.IsShared - 1)))?null:(System.Boolean?)reader[((int)UserDefinedViewColumn.IsShared - 1)];
					c.CreatedDate = (reader.IsDBNull(((int)UserDefinedViewColumn.CreatedDate - 1)))?null:(System.DateTime?)reader[((int)UserDefinedViewColumn.CreatedDate - 1)];
					c.ModifiedDate = (reader.IsDBNull(((int)UserDefinedViewColumn.ModifiedDate - 1)))?null:(System.DateTime?)reader[((int)UserDefinedViewColumn.ModifiedDate - 1)];
					c.CreatedBy = (reader.IsDBNull(((int)UserDefinedViewColumn.CreatedBy - 1)))?null:(System.Int32?)reader[((int)UserDefinedViewColumn.CreatedBy - 1)];
					c.ModifiedBy = (reader.IsDBNull(((int)UserDefinedViewColumn.ModifiedBy - 1)))?null:(System.Int32?)reader[((int)UserDefinedViewColumn.ModifiedBy - 1)];
					c.CreatedByName = (reader.IsDBNull(((int)UserDefinedViewColumn.CreatedByName - 1)))?null:(System.String)reader[((int)UserDefinedViewColumn.CreatedByName - 1)];
					c.ModifiedByName = (reader.IsDBNull(((int)UserDefinedViewColumn.ModifiedByName - 1)))?null:(System.String)reader[((int)UserDefinedViewColumn.ModifiedByName - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.UserDefinedView"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.UserDefinedView"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.UserDefinedView entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int32)reader[((int)UserDefinedViewColumn.Id - 1)];
			entity.UserKey = (reader.IsDBNull(((int)UserDefinedViewColumn.UserKey - 1)))?null:(System.Int32?)reader[((int)UserDefinedViewColumn.UserKey - 1)];
			entity.ViewName = (reader.IsDBNull(((int)UserDefinedViewColumn.ViewName - 1)))?null:(System.String)reader[((int)UserDefinedViewColumn.ViewName - 1)];
			entity.SourceViewName = (reader.IsDBNull(((int)UserDefinedViewColumn.SourceViewName - 1)))?null:(System.String)reader[((int)UserDefinedViewColumn.SourceViewName - 1)];
			entity.SelectedColumns = (reader.IsDBNull(((int)UserDefinedViewColumn.SelectedColumns - 1)))?null:(System.String)reader[((int)UserDefinedViewColumn.SelectedColumns - 1)];
			entity.FilterConditionDefinition = (reader.IsDBNull(((int)UserDefinedViewColumn.FilterConditionDefinition - 1)))?null:(System.String)reader[((int)UserDefinedViewColumn.FilterConditionDefinition - 1)];
			entity.FilteConditionQuery = (reader.IsDBNull(((int)UserDefinedViewColumn.FilteConditionQuery - 1)))?null:(System.String)reader[((int)UserDefinedViewColumn.FilteConditionQuery - 1)];
			entity.IsShared = (reader.IsDBNull(((int)UserDefinedViewColumn.IsShared - 1)))?null:(System.Boolean?)reader[((int)UserDefinedViewColumn.IsShared - 1)];
			entity.CreatedDate = (reader.IsDBNull(((int)UserDefinedViewColumn.CreatedDate - 1)))?null:(System.DateTime?)reader[((int)UserDefinedViewColumn.CreatedDate - 1)];
			entity.ModifiedDate = (reader.IsDBNull(((int)UserDefinedViewColumn.ModifiedDate - 1)))?null:(System.DateTime?)reader[((int)UserDefinedViewColumn.ModifiedDate - 1)];
			entity.CreatedBy = (reader.IsDBNull(((int)UserDefinedViewColumn.CreatedBy - 1)))?null:(System.Int32?)reader[((int)UserDefinedViewColumn.CreatedBy - 1)];
			entity.ModifiedBy = (reader.IsDBNull(((int)UserDefinedViewColumn.ModifiedBy - 1)))?null:(System.Int32?)reader[((int)UserDefinedViewColumn.ModifiedBy - 1)];
			entity.CreatedByName = (reader.IsDBNull(((int)UserDefinedViewColumn.CreatedByName - 1)))?null:(System.String)reader[((int)UserDefinedViewColumn.CreatedByName - 1)];
			entity.ModifiedByName = (reader.IsDBNull(((int)UserDefinedViewColumn.ModifiedByName - 1)))?null:(System.String)reader[((int)UserDefinedViewColumn.ModifiedByName - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.UserDefinedView"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.UserDefinedView"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.UserDefinedView entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int32)dataRow["Id"];
			entity.UserKey = Convert.IsDBNull(dataRow["UserKey"]) ? null : (System.Int32?)dataRow["UserKey"];
			entity.ViewName = Convert.IsDBNull(dataRow["ViewName"]) ? null : (System.String)dataRow["ViewName"];
			entity.SourceViewName = Convert.IsDBNull(dataRow["SourceViewName"]) ? null : (System.String)dataRow["SourceViewName"];
			entity.SelectedColumns = Convert.IsDBNull(dataRow["SelectedColumns"]) ? null : (System.String)dataRow["SelectedColumns"];
			entity.FilterConditionDefinition = Convert.IsDBNull(dataRow["FilterConditionDefinition"]) ? null : (System.String)dataRow["FilterConditionDefinition"];
			entity.FilteConditionQuery = Convert.IsDBNull(dataRow["FilteConditionQuery"]) ? null : (System.String)dataRow["FilteConditionQuery"];
			entity.IsShared = Convert.IsDBNull(dataRow["IsShared"]) ? null : (System.Boolean?)dataRow["IsShared"];
			entity.CreatedDate = Convert.IsDBNull(dataRow["CreatedDate"]) ? null : (System.DateTime?)dataRow["CreatedDate"];
			entity.ModifiedDate = Convert.IsDBNull(dataRow["ModifiedDate"]) ? null : (System.DateTime?)dataRow["ModifiedDate"];
			entity.CreatedBy = Convert.IsDBNull(dataRow["CreatedBy"]) ? null : (System.Int32?)dataRow["CreatedBy"];
			entity.ModifiedBy = Convert.IsDBNull(dataRow["ModifiedBy"]) ? null : (System.Int32?)dataRow["ModifiedBy"];
			entity.CreatedByName = Convert.IsDBNull(dataRow["CreatedByName"]) ? null : (System.String)dataRow["CreatedByName"];
			entity.ModifiedByName = Convert.IsDBNull(dataRow["ModifiedByName"]) ? null : (System.String)dataRow["ModifiedByName"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.UserDefinedView"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.UserDefinedView Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.UserDefinedView entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.UserDefinedView object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.UserDefinedView instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.UserDefinedView Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.UserDefinedView entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region UserDefinedViewChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.UserDefinedView</c>
	///</summary>
	public enum UserDefinedViewChildEntityTypes
	{
	}
	
	#endregion UserDefinedViewChildEntityTypes
	
	#region UserDefinedViewFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;UserDefinedViewColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UserDefinedView"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UserDefinedViewFilterBuilder : SqlFilterBuilder<UserDefinedViewColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UserDefinedViewFilterBuilder class.
		/// </summary>
		public UserDefinedViewFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the UserDefinedViewFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UserDefinedViewFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UserDefinedViewFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UserDefinedViewFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UserDefinedViewFilterBuilder
	
	#region UserDefinedViewParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;UserDefinedViewColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UserDefinedView"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UserDefinedViewParameterBuilder : ParameterizedSqlFilterBuilder<UserDefinedViewColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UserDefinedViewParameterBuilder class.
		/// </summary>
		public UserDefinedViewParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the UserDefinedViewParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UserDefinedViewParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UserDefinedViewParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UserDefinedViewParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UserDefinedViewParameterBuilder
	
	#region UserDefinedViewSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;UserDefinedViewColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UserDefinedView"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class UserDefinedViewSortBuilder : SqlSortBuilder<UserDefinedViewColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UserDefinedViewSqlSortBuilder class.
		/// </summary>
		public UserDefinedViewSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion UserDefinedViewSortBuilder
	
} // end namespace
