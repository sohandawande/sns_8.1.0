﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="FacetControlTypeProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class FacetControlTypeProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.FacetControlType, ZNode.Libraries.DataAccess.Entities.FacetControlTypeKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.FacetControlTypeKey key)
		{
			return Delete(transactionManager, key.ControlTypeID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_controlTypeID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _controlTypeID)
		{
			return Delete(null, _controlTypeID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_controlTypeID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _controlTypeID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.FacetControlType Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.FacetControlTypeKey key, int start, int pageLength)
		{
			return GetByControlTypeID(transactionManager, key.ControlTypeID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK__ZNodeFacet__3399DDCB58042D84 index.
		/// </summary>
		/// <param name="_controlTypeID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.FacetControlType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.FacetControlType GetByControlTypeID(System.Int32 _controlTypeID)
		{
			int count = -1;
			return GetByControlTypeID(null,_controlTypeID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK__ZNodeFacet__3399DDCB58042D84 index.
		/// </summary>
		/// <param name="_controlTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.FacetControlType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.FacetControlType GetByControlTypeID(System.Int32 _controlTypeID, int start, int pageLength)
		{
			int count = -1;
			return GetByControlTypeID(null, _controlTypeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK__ZNodeFacet__3399DDCB58042D84 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_controlTypeID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.FacetControlType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.FacetControlType GetByControlTypeID(TransactionManager transactionManager, System.Int32 _controlTypeID)
		{
			int count = -1;
			return GetByControlTypeID(transactionManager, _controlTypeID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK__ZNodeFacet__3399DDCB58042D84 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_controlTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.FacetControlType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.FacetControlType GetByControlTypeID(TransactionManager transactionManager, System.Int32 _controlTypeID, int start, int pageLength)
		{
			int count = -1;
			return GetByControlTypeID(transactionManager, _controlTypeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK__ZNodeFacet__3399DDCB58042D84 index.
		/// </summary>
		/// <param name="_controlTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.FacetControlType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.FacetControlType GetByControlTypeID(System.Int32 _controlTypeID, int start, int pageLength, out int count)
		{
			return GetByControlTypeID(null, _controlTypeID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK__ZNodeFacet__3399DDCB58042D84 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_controlTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.FacetControlType"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.FacetControlType GetByControlTypeID(TransactionManager transactionManager, System.Int32 _controlTypeID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;FacetControlType&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;FacetControlType&gt;"/></returns>
		public static TList<FacetControlType> Fill(IDataReader reader, TList<FacetControlType> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.FacetControlType c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("FacetControlType")
					.Append("|").Append((System.Int32)reader[((int)FacetControlTypeColumn.ControlTypeID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<FacetControlType>(
					key.ToString(), // EntityTrackingKey
					"FacetControlType",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.FacetControlType();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.ControlTypeID = (System.Int32)reader[((int)FacetControlTypeColumn.ControlTypeID - 1)];
					c.ControlName = (reader.IsDBNull(((int)FacetControlTypeColumn.ControlName - 1)))?null:(System.String)reader[((int)FacetControlTypeColumn.ControlName - 1)];
					c.ControlDescription = (reader.IsDBNull(((int)FacetControlTypeColumn.ControlDescription - 1)))?null:(System.String)reader[((int)FacetControlTypeColumn.ControlDescription - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.FacetControlType"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.FacetControlType"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.FacetControlType entity)
		{
			if (!reader.Read()) return;
			
			entity.ControlTypeID = (System.Int32)reader[((int)FacetControlTypeColumn.ControlTypeID - 1)];
			entity.ControlName = (reader.IsDBNull(((int)FacetControlTypeColumn.ControlName - 1)))?null:(System.String)reader[((int)FacetControlTypeColumn.ControlName - 1)];
			entity.ControlDescription = (reader.IsDBNull(((int)FacetControlTypeColumn.ControlDescription - 1)))?null:(System.String)reader[((int)FacetControlTypeColumn.ControlDescription - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.FacetControlType"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.FacetControlType"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.FacetControlType entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.ControlTypeID = (System.Int32)dataRow["ControlTypeID"];
			entity.ControlName = Convert.IsDBNull(dataRow["ControlName"]) ? null : (System.String)dataRow["ControlName"];
			entity.ControlDescription = Convert.IsDBNull(dataRow["ControlDescription"]) ? null : (System.String)dataRow["ControlDescription"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.FacetControlType"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.FacetControlType Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.FacetControlType entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByControlTypeID methods when available
			
			#region FacetGroupCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<FacetGroup>|FacetGroupCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'FacetGroupCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.FacetGroupCollection = DataRepository.FacetGroupProvider.GetByControlTypeID(transactionManager, entity.ControlTypeID);

				if (deep && entity.FacetGroupCollection.Count > 0)
				{
					deepHandles.Add("FacetGroupCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<FacetGroup>) DataRepository.FacetGroupProvider.DeepLoad,
						new object[] { transactionManager, entity.FacetGroupCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.FacetControlType object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.FacetControlType instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.FacetControlType Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.FacetControlType entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<FacetGroup>
				if (CanDeepSave(entity.FacetGroupCollection, "List<FacetGroup>|FacetGroupCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(FacetGroup child in entity.FacetGroupCollection)
					{
						if(child.ControlTypeIDSource != null)
						{
							child.ControlTypeID = child.ControlTypeIDSource.ControlTypeID;
						}
						else
						{
							child.ControlTypeID = entity.ControlTypeID;
						}

					}

					if (entity.FacetGroupCollection.Count > 0 || entity.FacetGroupCollection.DeletedItems.Count > 0)
					{
						//DataRepository.FacetGroupProvider.Save(transactionManager, entity.FacetGroupCollection);
						
						deepHandles.Add("FacetGroupCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< FacetGroup >) DataRepository.FacetGroupProvider.DeepSave,
							new object[] { transactionManager, entity.FacetGroupCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region FacetControlTypeChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.FacetControlType</c>
	///</summary>
	public enum FacetControlTypeChildEntityTypes
	{
		///<summary>
		/// Collection of <c>FacetControlType</c> as OneToMany for FacetGroupCollection
		///</summary>
		[ChildEntityType(typeof(TList<FacetGroup>))]
		FacetGroupCollection,
	}
	
	#endregion FacetControlTypeChildEntityTypes
	
	#region FacetControlTypeFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;FacetControlTypeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FacetControlType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FacetControlTypeFilterBuilder : SqlFilterBuilder<FacetControlTypeColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FacetControlTypeFilterBuilder class.
		/// </summary>
		public FacetControlTypeFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the FacetControlTypeFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FacetControlTypeFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FacetControlTypeFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FacetControlTypeFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FacetControlTypeFilterBuilder
	
	#region FacetControlTypeParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;FacetControlTypeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FacetControlType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FacetControlTypeParameterBuilder : ParameterizedSqlFilterBuilder<FacetControlTypeColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FacetControlTypeParameterBuilder class.
		/// </summary>
		public FacetControlTypeParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the FacetControlTypeParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FacetControlTypeParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FacetControlTypeParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FacetControlTypeParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FacetControlTypeParameterBuilder
	
	#region FacetControlTypeSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;FacetControlTypeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FacetControlType"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class FacetControlTypeSortBuilder : SqlSortBuilder<FacetControlTypeColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FacetControlTypeSqlSortBuilder class.
		/// </summary>
		public FacetControlTypeSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion FacetControlTypeSortBuilder
	
} // end namespace
