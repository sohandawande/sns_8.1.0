﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="AttributeTypeProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class AttributeTypeProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.AttributeType, ZNode.Libraries.DataAccess.Entities.AttributeTypeKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.AttributeTypeKey key)
		{
			return Delete(transactionManager, key.AttributeTypeId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_attributeTypeId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _attributeTypeId)
		{
			return Delete(null, _attributeTypeId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_attributeTypeId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _attributeTypeId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.AttributeType Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.AttributeTypeKey key, int start, int pageLength)
		{
			return GetByAttributeTypeId(transactionManager, key.AttributeTypeId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeAttributeType_IsPrivate index.
		/// </summary>
		/// <param name="_isPrivate"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;AttributeType&gt;"/> class.</returns>
		public TList<AttributeType> GetByIsPrivate(System.Boolean _isPrivate)
		{
			int count = -1;
			return GetByIsPrivate(null,_isPrivate, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAttributeType_IsPrivate index.
		/// </summary>
		/// <param name="_isPrivate"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;AttributeType&gt;"/> class.</returns>
		public TList<AttributeType> GetByIsPrivate(System.Boolean _isPrivate, int start, int pageLength)
		{
			int count = -1;
			return GetByIsPrivate(null, _isPrivate, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAttributeType_IsPrivate index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_isPrivate"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;AttributeType&gt;"/> class.</returns>
		public TList<AttributeType> GetByIsPrivate(TransactionManager transactionManager, System.Boolean _isPrivate)
		{
			int count = -1;
			return GetByIsPrivate(transactionManager, _isPrivate, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAttributeType_IsPrivate index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_isPrivate"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;AttributeType&gt;"/> class.</returns>
		public TList<AttributeType> GetByIsPrivate(TransactionManager transactionManager, System.Boolean _isPrivate, int start, int pageLength)
		{
			int count = -1;
			return GetByIsPrivate(transactionManager, _isPrivate, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAttributeType_IsPrivate index.
		/// </summary>
		/// <param name="_isPrivate"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;AttributeType&gt;"/> class.</returns>
		public TList<AttributeType> GetByIsPrivate(System.Boolean _isPrivate, int start, int pageLength, out int count)
		{
			return GetByIsPrivate(null, _isPrivate, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAttributeType_IsPrivate index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_isPrivate"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;AttributeType&gt;"/> class.</returns>
		public abstract TList<AttributeType> GetByIsPrivate(TransactionManager transactionManager, System.Boolean _isPrivate, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeAttributeType_LocaleId index.
		/// </summary>
		/// <param name="_localeId"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;AttributeType&gt;"/> class.</returns>
		public TList<AttributeType> GetByLocaleId(System.Int32 _localeId)
		{
			int count = -1;
			return GetByLocaleId(null,_localeId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAttributeType_LocaleId index.
		/// </summary>
		/// <param name="_localeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;AttributeType&gt;"/> class.</returns>
		public TList<AttributeType> GetByLocaleId(System.Int32 _localeId, int start, int pageLength)
		{
			int count = -1;
			return GetByLocaleId(null, _localeId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAttributeType_LocaleId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_localeId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;AttributeType&gt;"/> class.</returns>
		public TList<AttributeType> GetByLocaleId(TransactionManager transactionManager, System.Int32 _localeId)
		{
			int count = -1;
			return GetByLocaleId(transactionManager, _localeId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAttributeType_LocaleId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_localeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;AttributeType&gt;"/> class.</returns>
		public TList<AttributeType> GetByLocaleId(TransactionManager transactionManager, System.Int32 _localeId, int start, int pageLength)
		{
			int count = -1;
			return GetByLocaleId(transactionManager, _localeId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAttributeType_LocaleId index.
		/// </summary>
		/// <param name="_localeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;AttributeType&gt;"/> class.</returns>
		public TList<AttributeType> GetByLocaleId(System.Int32 _localeId, int start, int pageLength, out int count)
		{
			return GetByLocaleId(null, _localeId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAttributeType_LocaleId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_localeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;AttributeType&gt;"/> class.</returns>
		public abstract TList<AttributeType> GetByLocaleId(TransactionManager transactionManager, System.Int32 _localeId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeAttributeType_PortalID index.
		/// </summary>
		/// <param name="_portalID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;AttributeType&gt;"/> class.</returns>
		public TList<AttributeType> GetByPortalID(System.Int32? _portalID)
		{
			int count = -1;
			return GetByPortalID(null,_portalID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAttributeType_PortalID index.
		/// </summary>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;AttributeType&gt;"/> class.</returns>
		public TList<AttributeType> GetByPortalID(System.Int32? _portalID, int start, int pageLength)
		{
			int count = -1;
			return GetByPortalID(null, _portalID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAttributeType_PortalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;AttributeType&gt;"/> class.</returns>
		public TList<AttributeType> GetByPortalID(TransactionManager transactionManager, System.Int32? _portalID)
		{
			int count = -1;
			return GetByPortalID(transactionManager, _portalID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAttributeType_PortalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;AttributeType&gt;"/> class.</returns>
		public TList<AttributeType> GetByPortalID(TransactionManager transactionManager, System.Int32? _portalID, int start, int pageLength)
		{
			int count = -1;
			return GetByPortalID(transactionManager, _portalID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAttributeType_PortalID index.
		/// </summary>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;AttributeType&gt;"/> class.</returns>
		public TList<AttributeType> GetByPortalID(System.Int32? _portalID, int start, int pageLength, out int count)
		{
			return GetByPortalID(null, _portalID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAttributeType_PortalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;AttributeType&gt;"/> class.</returns>
		public abstract TList<AttributeType> GetByPortalID(TransactionManager transactionManager, System.Int32? _portalID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_SC_AttributeType index.
		/// </summary>
		/// <param name="_attributeTypeId"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.AttributeType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.AttributeType GetByAttributeTypeId(System.Int32 _attributeTypeId)
		{
			int count = -1;
			return GetByAttributeTypeId(null,_attributeTypeId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SC_AttributeType index.
		/// </summary>
		/// <param name="_attributeTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.AttributeType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.AttributeType GetByAttributeTypeId(System.Int32 _attributeTypeId, int start, int pageLength)
		{
			int count = -1;
			return GetByAttributeTypeId(null, _attributeTypeId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SC_AttributeType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_attributeTypeId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.AttributeType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.AttributeType GetByAttributeTypeId(TransactionManager transactionManager, System.Int32 _attributeTypeId)
		{
			int count = -1;
			return GetByAttributeTypeId(transactionManager, _attributeTypeId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SC_AttributeType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_attributeTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.AttributeType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.AttributeType GetByAttributeTypeId(TransactionManager transactionManager, System.Int32 _attributeTypeId, int start, int pageLength)
		{
			int count = -1;
			return GetByAttributeTypeId(transactionManager, _attributeTypeId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SC_AttributeType index.
		/// </summary>
		/// <param name="_attributeTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.AttributeType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.AttributeType GetByAttributeTypeId(System.Int32 _attributeTypeId, int start, int pageLength, out int count)
		{
			return GetByAttributeTypeId(null, _attributeTypeId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SC_AttributeType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_attributeTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.AttributeType"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.AttributeType GetByAttributeTypeId(TransactionManager transactionManager, System.Int32 _attributeTypeId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;AttributeType&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;AttributeType&gt;"/></returns>
		public static TList<AttributeType> Fill(IDataReader reader, TList<AttributeType> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.AttributeType c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("AttributeType")
					.Append("|").Append((System.Int32)reader[((int)AttributeTypeColumn.AttributeTypeId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<AttributeType>(
					key.ToString(), // EntityTrackingKey
					"AttributeType",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.AttributeType();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.AttributeTypeId = (System.Int32)reader[((int)AttributeTypeColumn.AttributeTypeId - 1)];
					c.Name = (System.String)reader[((int)AttributeTypeColumn.Name - 1)];
					c.Description = (reader.IsDBNull(((int)AttributeTypeColumn.Description - 1)))?null:(System.String)reader[((int)AttributeTypeColumn.Description - 1)];
					c.DisplayOrder = (System.Int32)reader[((int)AttributeTypeColumn.DisplayOrder - 1)];
					c.IsPrivate = (System.Boolean)reader[((int)AttributeTypeColumn.IsPrivate - 1)];
					c.LocaleId = (System.Int32)reader[((int)AttributeTypeColumn.LocaleId - 1)];
					c.PortalID = (reader.IsDBNull(((int)AttributeTypeColumn.PortalID - 1)))?null:(System.Int32?)reader[((int)AttributeTypeColumn.PortalID - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.AttributeType"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.AttributeType"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.AttributeType entity)
		{
			if (!reader.Read()) return;
			
			entity.AttributeTypeId = (System.Int32)reader[((int)AttributeTypeColumn.AttributeTypeId - 1)];
			entity.Name = (System.String)reader[((int)AttributeTypeColumn.Name - 1)];
			entity.Description = (reader.IsDBNull(((int)AttributeTypeColumn.Description - 1)))?null:(System.String)reader[((int)AttributeTypeColumn.Description - 1)];
			entity.DisplayOrder = (System.Int32)reader[((int)AttributeTypeColumn.DisplayOrder - 1)];
			entity.IsPrivate = (System.Boolean)reader[((int)AttributeTypeColumn.IsPrivate - 1)];
			entity.LocaleId = (System.Int32)reader[((int)AttributeTypeColumn.LocaleId - 1)];
			entity.PortalID = (reader.IsDBNull(((int)AttributeTypeColumn.PortalID - 1)))?null:(System.Int32?)reader[((int)AttributeTypeColumn.PortalID - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.AttributeType"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.AttributeType"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.AttributeType entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.AttributeTypeId = (System.Int32)dataRow["AttributeTypeId"];
			entity.Name = (System.String)dataRow["Name"];
			entity.Description = Convert.IsDBNull(dataRow["Description"]) ? null : (System.String)dataRow["Description"];
			entity.DisplayOrder = (System.Int32)dataRow["DisplayOrder"];
			entity.IsPrivate = (System.Boolean)dataRow["IsPrivate"];
			entity.LocaleId = (System.Int32)dataRow["LocaleId"];
			entity.PortalID = Convert.IsDBNull(dataRow["PortalID"]) ? null : (System.Int32?)dataRow["PortalID"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.AttributeType"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.AttributeType Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.AttributeType entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region LocaleIdSource	
			if (CanDeepLoad(entity, "Locale|LocaleIdSource", deepLoadType, innerList) 
				&& entity.LocaleIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.LocaleId;
				Locale tmpEntity = EntityManager.LocateEntity<Locale>(EntityLocator.ConstructKeyFromPkItems(typeof(Locale), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.LocaleIdSource = tmpEntity;
				else
					entity.LocaleIdSource = DataRepository.LocaleProvider.GetByLocaleID(transactionManager, entity.LocaleId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'LocaleIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.LocaleIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.LocaleProvider.DeepLoad(transactionManager, entity.LocaleIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion LocaleIdSource

			#region PortalIDSource	
			if (CanDeepLoad(entity, "Portal|PortalIDSource", deepLoadType, innerList) 
				&& entity.PortalIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.PortalID ?? (int)0);
				Portal tmpEntity = EntityManager.LocateEntity<Portal>(EntityLocator.ConstructKeyFromPkItems(typeof(Portal), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.PortalIDSource = tmpEntity;
				else
					entity.PortalIDSource = DataRepository.PortalProvider.GetByPortalID(transactionManager, (entity.PortalID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PortalIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.PortalIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.PortalProvider.DeepLoad(transactionManager, entity.PortalIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion PortalIDSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByAttributeTypeId methods when available
			
			#region ProductTypeAttributeCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<ProductTypeAttribute>|ProductTypeAttributeCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ProductTypeAttributeCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.ProductTypeAttributeCollection = DataRepository.ProductTypeAttributeProvider.GetByAttributeTypeId(transactionManager, entity.AttributeTypeId);

				if (deep && entity.ProductTypeAttributeCollection.Count > 0)
				{
					deepHandles.Add("ProductTypeAttributeCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<ProductTypeAttribute>) DataRepository.ProductTypeAttributeProvider.DeepLoad,
						new object[] { transactionManager, entity.ProductTypeAttributeCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region ProductAttributeCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<ProductAttribute>|ProductAttributeCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ProductAttributeCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.ProductAttributeCollection = DataRepository.ProductAttributeProvider.GetByAttributeTypeId(transactionManager, entity.AttributeTypeId);

				if (deep && entity.ProductAttributeCollection.Count > 0)
				{
					deepHandles.Add("ProductAttributeCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<ProductAttribute>) DataRepository.ProductAttributeProvider.DeepLoad,
						new object[] { transactionManager, entity.ProductAttributeCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.AttributeType object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.AttributeType instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.AttributeType Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.AttributeType entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region LocaleIdSource
			if (CanDeepSave(entity, "Locale|LocaleIdSource", deepSaveType, innerList) 
				&& entity.LocaleIdSource != null)
			{
				DataRepository.LocaleProvider.Save(transactionManager, entity.LocaleIdSource);
				entity.LocaleId = entity.LocaleIdSource.LocaleID;
			}
			#endregion 
			
			#region PortalIDSource
			if (CanDeepSave(entity, "Portal|PortalIDSource", deepSaveType, innerList) 
				&& entity.PortalIDSource != null)
			{
				DataRepository.PortalProvider.Save(transactionManager, entity.PortalIDSource);
				entity.PortalID = entity.PortalIDSource.PortalID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<ProductTypeAttribute>
				if (CanDeepSave(entity.ProductTypeAttributeCollection, "List<ProductTypeAttribute>|ProductTypeAttributeCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(ProductTypeAttribute child in entity.ProductTypeAttributeCollection)
					{
						if(child.AttributeTypeIdSource != null)
						{
							child.AttributeTypeId = child.AttributeTypeIdSource.AttributeTypeId;
						}
						else
						{
							child.AttributeTypeId = entity.AttributeTypeId;
						}

					}

					if (entity.ProductTypeAttributeCollection.Count > 0 || entity.ProductTypeAttributeCollection.DeletedItems.Count > 0)
					{
						//DataRepository.ProductTypeAttributeProvider.Save(transactionManager, entity.ProductTypeAttributeCollection);
						
						deepHandles.Add("ProductTypeAttributeCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< ProductTypeAttribute >) DataRepository.ProductTypeAttributeProvider.DeepSave,
							new object[] { transactionManager, entity.ProductTypeAttributeCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<ProductAttribute>
				if (CanDeepSave(entity.ProductAttributeCollection, "List<ProductAttribute>|ProductAttributeCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(ProductAttribute child in entity.ProductAttributeCollection)
					{
						if(child.AttributeTypeIdSource != null)
						{
							child.AttributeTypeId = child.AttributeTypeIdSource.AttributeTypeId;
						}
						else
						{
							child.AttributeTypeId = entity.AttributeTypeId;
						}

					}

					if (entity.ProductAttributeCollection.Count > 0 || entity.ProductAttributeCollection.DeletedItems.Count > 0)
					{
						//DataRepository.ProductAttributeProvider.Save(transactionManager, entity.ProductAttributeCollection);
						
						deepHandles.Add("ProductAttributeCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< ProductAttribute >) DataRepository.ProductAttributeProvider.DeepSave,
							new object[] { transactionManager, entity.ProductAttributeCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region AttributeTypeChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.AttributeType</c>
	///</summary>
	public enum AttributeTypeChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Locale</c> at LocaleIdSource
		///</summary>
		[ChildEntityType(typeof(Locale))]
		Locale,
		
		///<summary>
		/// Composite Property for <c>Portal</c> at PortalIDSource
		///</summary>
		[ChildEntityType(typeof(Portal))]
		Portal,
		///<summary>
		/// Collection of <c>AttributeType</c> as OneToMany for ProductTypeAttributeCollection
		///</summary>
		[ChildEntityType(typeof(TList<ProductTypeAttribute>))]
		ProductTypeAttributeCollection,
		///<summary>
		/// Collection of <c>AttributeType</c> as OneToMany for ProductAttributeCollection
		///</summary>
		[ChildEntityType(typeof(TList<ProductAttribute>))]
		ProductAttributeCollection,
	}
	
	#endregion AttributeTypeChildEntityTypes
	
	#region AttributeTypeFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;AttributeTypeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AttributeType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AttributeTypeFilterBuilder : SqlFilterBuilder<AttributeTypeColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AttributeTypeFilterBuilder class.
		/// </summary>
		public AttributeTypeFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the AttributeTypeFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AttributeTypeFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AttributeTypeFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AttributeTypeFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AttributeTypeFilterBuilder
	
	#region AttributeTypeParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;AttributeTypeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AttributeType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AttributeTypeParameterBuilder : ParameterizedSqlFilterBuilder<AttributeTypeColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AttributeTypeParameterBuilder class.
		/// </summary>
		public AttributeTypeParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the AttributeTypeParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AttributeTypeParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AttributeTypeParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AttributeTypeParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AttributeTypeParameterBuilder
	
	#region AttributeTypeSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;AttributeTypeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AttributeType"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class AttributeTypeSortBuilder : SqlSortBuilder<AttributeTypeColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AttributeTypeSqlSortBuilder class.
		/// </summary>
		public AttributeTypeSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion AttributeTypeSortBuilder
	
} // end namespace
