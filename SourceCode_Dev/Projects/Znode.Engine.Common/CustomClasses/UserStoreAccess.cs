﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Profile;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.UserAccount;

namespace Znode.Engine.Common
{
    #region Public Enumeration

    /// <summary>
    /// Represents the ZNodeUserType enum
    /// </summary>
    public enum ZNodeUserType
    {
        /// <summary>
        /// User type is vendor.
        /// </summary>
        Vendor = 0,

        /// <summary>
        /// User type is Reviewer
        /// </summary>
        Reviewer = 1,

        /// <summary>
        /// User type is Reviewer Manager
        /// </summary>
        ReviewerManager = 2,

        /// <summary>
        /// User type is Admin
        /// </summary>
        Admin = 3,

        /// <summary>
        /// User type is Franchise
        /// </summary>
        Franchise = 4,

        /// <summary>
        /// Other role user.
        /// </summary>
        Other = 10
    }

    #endregion

    /// <summary>
    /// Represents a User Store Access class
    /// </summary>
    public class UserStoreAccess : System.Web.Profile.ProfileBase
    {
        /// <summary>
        /// Initializes a new instance of the UserStoreAccess class.
        /// </summary>
        public UserStoreAccess()
        {
        }

        /// <summary>
        /// Gets the Available Portals
        /// </summary>
        public static string GetAvailablePortals
        {
            get
            {
                string portals = null;

                ProfileCommon common = ProfileCommon.Create(HttpContext.Current.User.Identity.Name, true) as ProfileCommon;

                if (common.StoreAccess == "AllStores")
                {
                    portals = "0";
                }
                else
                {
                    portals = common.StoreAccess;
                }

                return portals;
            }
        }

        /// <summary>
        /// Gets the Turnkey Store PortalID 
        /// </summary>
        public static int? GetTurnkeyStorePortalID
        {
            get
            {
                string portals = null;

                ProfileCommon common = ProfileCommon.Create(HttpContext.Current.User.Identity.Name, true) as ProfileCommon;

                if (common.StoreAccess == "AllStores")
                {
                    portals = string.Empty;
                }
                else
                {
                    portals = common.StoreAccess;
                }

                int portalID = 0;
                if (int.TryParse(portals.Split(',')[0], out portalID))
                {
                    return portalID;
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// Gets the Turnkey Store Profile ID
        /// </summary>
        public static int GetTurnkeyStoreProfileID
        {
            get
            {
                PortalProfileService postalProfileService = new PortalProfileService();
                string storeList = GetAvailablePortals;
                if (string.Compare(storeList, "0") == 0)
                {
                    return 0;
                }

                string[] stores = storeList.Split(',');

                if (string.IsNullOrEmpty(storeList.Trim()))
                {
                    return 0;
                }

                TList<PortalProfile> profiles = postalProfileService.GetByPortalID(Convert.ToInt32(stores[0]));
                if (profiles != null && profiles.Count > 0)
                {
                    return profiles[0].ProfileID;
                }

                return 0;
            }
        }

        public static TList<PortalProfile> GetTurnkeyStoreProfiles
        {
            get
            {
                PortalProfileService postalProfileService = new PortalProfileService();
                string storeList = GetAvailablePortals;
                if (string.Compare(storeList, "0") == 0)
                {
                    return postalProfileService.GetAll();
                }

                string[] stores = storeList.Split(',');
                TList<PortalProfile> profiles = new TList<PortalProfile>();

                if (string.IsNullOrEmpty(storeList.Trim()))
                {
                    return profiles;
                }

                for (int idx = 0; idx < stores.Length; idx++)
                {
                    profiles.AddRange(postalProfileService.GetByPortalID(Convert.ToInt32(stores[idx])));
                }

                return profiles;
            }
        }

        public static TList<Catalog> GetTurnkeyStoreCatalogs
        {
            get
            {
                CatalogService service = new CatalogService();
                string storeList = GetAvailablePortals;
                if (string.Compare(storeList, "0") == 0)
                {
                    return service.GetAll();
                }

                string[] stores = storeList.Split(',');
                TList<Catalog> catalogs = new TList<Catalog>();

                if (string.IsNullOrEmpty(storeList.Trim()))
                {
                    return catalogs;
                }

                for (int idx = 0; idx < stores.Length; idx++)
                {
                    catalogs.AddRange(service.GetByPortalID(Convert.ToInt32(stores[idx])));
                }

                return catalogs;
            }
        }

        /// <summary>
        /// Gets the Domain Name
        /// </summary>
        public static string DomainName
        {
            get
            {
                ZNode.Libraries.DataAccess.Entities.Domain domain = ZNode.Libraries.Framework.Business.ZNodeConfigManager.DomainConfig;

                return domain.DomainName;
            }
        }

        /// <summary>
        /// Gets the single store edition upgrade message.
        /// </summary>
        public static string UpgradeMessage
        {
            get
            {
                return "This functionality is not available in the Single Store edition. Please upgrade your software to the Multi-Store edition to enable this functionality.";
            }
        }
        
        /// <summary>
        /// Gets or Sets the user type. If user type is Revier then checkbox should be visible, Edit and Delete command should be invisible from Grid.
        /// </summary>
        public ZNodeUserType UserType
        {
            get
            {
				ZNodeUserType _UserType = Znode.Engine.Common.ZNodeUserType.Other;
                if (Roles.IsUserInRole("REVIEWER"))
                {
                    _UserType = Znode.Engine.Common.ZNodeUserType.Reviewer;
                }
                else if (Roles.IsUserInRole("VENDOR"))
                {
                    _UserType = Znode.Engine.Common.ZNodeUserType.Vendor;
                }
                else if (Roles.IsUserInRole("REVIEWERMANAGER"))
                {
					_UserType = Znode.Engine.Common.ZNodeUserType.ReviewerManager;
                }
                else if (Roles.IsUserInRole("ADMIN"))
                {
					_UserType = Znode.Engine.Common.ZNodeUserType.Admin;
                }
                else if (Roles.IsUserInRole("FRANCHISE"))
                {
					_UserType = Znode.Engine.Common.ZNodeUserType.Franchise;
                }

                return _UserType;
            }
        }

        /// <summary>
        /// Check is singlefront edition.
        /// </summary>
        /// <returns>Returns True if single front edition else False.</returns>
        public static bool IsMultiStoreAdminEnabled()
        {
            if (ZNode.Libraries.Framework.Business.ZNodeConfigManager.EnvironmentConfig.MultiStoreAdminEnabled)
            {
                // Multifront edition
                return true;
            }
            else
            {
                // SingleFront Edition
                return false;
            }
        }

        /// <summary>
        /// Check User Role In Category
        /// </summary>
        /// <param name="profiles">Profile Common instance</param>
        /// <param name="ID">The value of ID</param>
        /// <returns>Returns a bool value</returns>
        public static bool CheckUserRoleInCategory(ProfileCommon profiles, int ID)
        {
            CatalogAdmin catalogAdmin = new CatalogAdmin();
            if (profiles.StoreAccess != "AllStores")
            {
                string[] stores = catalogAdmin.GetCatalogIDsByPortalIDs(profiles.StoreAccess).Split(',');
                bool isFound = false;
                CategoryNodeService service = new CategoryNodeService();
                TList<CategoryNode> categoryNodes = service.GetByCategoryID(ID);
                if (categoryNodes.Count == 0)
                {
                    return true;
                }

                foreach (CategoryNode categoryNode in categoryNodes)
                {
                    int found = Array.IndexOf(stores, categoryNode.CatalogID.ToString());
                    if (found != -1) 
                    { 
                        isFound = true; 
                    }
                }

                if (!isFound)
                {
                    return false;
                }
            }
            
            return true;
        }

        /// <summary>
        /// Check User Role in Product
        /// </summary>
        /// <param name="profiles">Profile Common instance</param>
        /// <param name="ID">The value of ID</param>
        /// <returns></returns>
        public static bool CheckUserRoleInProduct(ProfileCommon profiles, int ID)
        {
            CatalogAdmin catalogAdmin = new CatalogAdmin();
            if (profiles.StoreAccess != "AllStores")
            {
                string[] stores = catalogAdmin.GetCatalogIDsByPortalIDs(profiles.StoreAccess).Split(',');
                bool isFound = false;

                ProductCategoryService productService = new ProductCategoryService();
                foreach (ProductCategory productCategory in productService.GetByProductID(ID))
                {
                    CategoryNodeService service = new CategoryNodeService();
                    foreach (CategoryNode categoryNode in service.GetByCategoryID(productCategory.CategoryID))
                    {
                        int found = Array.IndexOf(stores, categoryNode.CatalogID.ToString());
                        if (found != -1) 
                        { 
                            isFound = true; 
                        }
                    }

                    if (!isFound)
                    {
                        return false;
                    }
                }
            }
            
            return true;
        }

        /// <summary>
        /// Get the Current User Name
        /// </summary>
        /// <returns>Returns the Current User Name</returns>
        public static string GetCurrentUserName()
        {
            return HttpContext.Current.User.Identity.Name;
        }
       
        /// <summary>
        /// Check Store Access method
        /// </summary>
        /// <param name="dt">Represents the DataTable</param>
        /// <returns>Returns a Datatable</returns>
        public static DataTable CheckStoreAccess(DataTable dt)
        {
            string storeList = GetAvailablePortals;
            if (storeList != "0")
            {
                string[] stores = storeList.Split(',');
                DataTable newDt = new DataTable();

                foreach (DataColumn column in dt.Columns)
                {                    
                    newDt.Columns.Add(column.ColumnName, column.DataType);
                }

                if (string.IsNullOrEmpty(storeList.Trim()))
                {
                    return newDt;
                }

                foreach (DataRow dataRow in dt.Rows)
                {
                    int found = Array.IndexOf(stores, Convert.ToString(dataRow["PortalID"]));
                    DataRow newRow = newDt.NewRow();
                    newRow.ItemArray = dataRow.ItemArray;
                    if (found != -1) 
                    { 
                        newDt.Rows.Add(newRow); 
                    }
                }

                return newDt;
            }

            return dt;
        }

        /// <summary>
        /// Check Store Access Method
        /// </summary>
        /// <param name="dt">Represents a Data Table</param>
        /// <param name="includeNull">Include Null values or not</param>
        /// <returns>Returns a Datatable</returns>
        public static DataTable CheckStoreAccess(DataTable dt, bool includeNull)
        {
            string storeList = GetAvailablePortals;
            if (storeList != "0")
            {
                string[] stores = storeList.Split(',');
                DataTable newDt = new DataTable();

                foreach (DataColumn column in dt.Columns)
                {
                    newDt.Columns.Add(column.ColumnName, column.DataType);
                }

                if (string.IsNullOrEmpty(storeList.Trim()))
                {
                    return newDt;
                }

                foreach (DataRow dataRow in dt.Rows)
                {
                    int found = Array.IndexOf(stores, Convert.ToString(dataRow["PortalID"]));
                    if (found != -1 || (includeNull && dataRow["PortalID"] == System.DBNull.Value))
                    {
                        DataRow newRow = newDt.NewRow();
                        newRow.ItemArray = dataRow.ItemArray;
                        newDt.Rows.Add(newRow);
                    }
                }

                return newDt;
            }

            return dt;
        }

        /// <summary>
        /// Check Product Access Method
        /// </summary>
        /// <param name="dt">Represents a Data Table</param>
        /// <param name="includeFranchisable">Bool value whether to include Franchisable</param>
        /// <returns>Represents a Data Table</returns>
        public static DataTable CheckProductAccess(DataTable dt, bool includeFranchisable)
        {
            string storeList = GetAvailablePortals;
            if (storeList != "0")
            {
                string[] stores = storeList.Split(',');
                DataTable newDt = new DataTable();

                foreach (DataColumn column in dt.Columns)
                {
                    newDt.Columns.Add(column.ColumnName, column.DataType);
                }

                if (string.IsNullOrEmpty(storeList.Trim()))
                {
                    return newDt;
                }

                foreach (DataRow dataRow in dt.Rows)
                {
                    int found = Array.IndexOf(stores, Convert.ToString(dataRow["PortalID"]));
                    if (found != -1 || (includeFranchisable && Convert.ToBoolean(dataRow["Franchisable"]) == true))
                    {
                        DataRow newRow = newDt.NewRow();
                        newRow.ItemArray = dataRow.ItemArray;
                        newDt.Rows.Add(newRow);
                    }
                }

                return newDt;
            }

            return dt;
        }

        /// <summary>
        /// Check Store Access Method
        /// </summary>
        /// <param name="portalId">The value of PortalId</param>
        /// <returns>Returns a bool value</returns>
        public static bool CheckStoreAccess(int portalId)
        {
            return CheckStoreAccess(portalId, false);
        }

        /// <summary>
        /// Check Store Access Method
        /// </summary>
        /// <param name="portalId">The value of PortalID</param>
        /// <param name="isRedirect">Represents a bool value</param>
        /// <returns>Returns a bool value</returns>
        public static bool CheckStoreAccess(int portalId, bool isRedirect)
        {
            return CheckStoreAccess(portalId, isRedirect, false);
        }

        /// <summary>
        /// Check Store Access Method
        /// </summary>
        /// <param name="portalId">The value of PortalID</param>
        /// <param name="isRedirect">Bool value that represents the IsRedirect</param>
        /// <param name="franchisable">Bool value represents the franchisable</param>
        /// <returns>Returns a bool value</returns>
        public static bool CheckStoreAccess(int portalId, bool isRedirect, bool franchisable)
        {
            string storeList = GetAvailablePortals;
            bool status = true;

            if (storeList != "0")
            {
                string[] stores = storeList.Split(',');
                int found = Array.IndexOf(stores, Convert.ToString(portalId));
                if (found == -1) 
                { 
                    status = false; 
                }
            }

            if (franchisable) 
            { 
                status = true; 
            }
            else 
            {
                if ((isRedirect & !status) || string.IsNullOrEmpty(storeList))
                {
                    HttpContext.Current.Response.Redirect("~/FranchiseAdmin/Secure/default.aspx");
                }
            }
            
            return status;
        }

        /// <summary>
        /// Check Store Access Method
        /// </summary>
        /// <typeparam name="T">The value of T</typeparam>
        /// <param name="listObject">The value of List Object</param>
        /// <returns>Returns the TList object</returns>
        public static TList<T> CheckStoreAccess<T>(TList<T> listObject) where T : IEntity, new()
        {
            string storeList = GetAvailablePortals;
            if (storeList != "0")
            {
                string[] stores = storeList.Split(',');

                if (string.IsNullOrEmpty(storeList))
                {
                    return new TList<T>();
                }

                TList<T> objects = listObject.Copy();

                for (int idx = 0; idx < objects.Count; idx++)
                {
                    T obj = objects[idx];
                    int found = Array.IndexOf(stores, Convert.ToString(obj.GetType().GetProperty("PortalID").GetValue(obj, null)));
                    if (found == -1) 
                    { 
                        listObject.Remove(obj); 
                    }
                }
            }

            return listObject;
        }

        /// <summary>
        /// Check Store Access Method
        /// </summary>
        /// <typeparam name="T">The value of T</typeparam>
        /// <param name="listObject">The value od list object</param>
        /// <param name="includeNull">Bool value to Include values or not</param>
        /// <returns>Returns a Tlist object</returns>
        public static TList<T> CheckStoreAccess<T>(TList<T> listObject, bool includeNull) where T : IEntity, new()
        {
            string storeList = GetAvailablePortals;
            if (storeList != "0")
            {
                string[] stores = storeList.Split(',');

                if (string.IsNullOrEmpty(storeList))
                {
                    return new TList<T>();
                }

                TList<T> objects = listObject.Copy();

                for (int idx = 0; idx < objects.Count; idx++)
                {
                    T obj = objects[idx];
                    int found = Array.IndexOf(stores, Convert.ToString(obj.GetType().GetProperty("PortalID").GetValue(obj, null)));
                    if (found != -1 || (includeNull && Convert.ToBoolean(obj.GetType().GetProperty("PortalID").GetValue(obj, null))))
                    { 
                        listObject.Remove(obj); 
                    }
                }
            }

            return listObject;
        }

        /// <summary>
        /// Check Profile Access Method
        /// </summary>
        /// <param name="dt">Returns a DataTable</param>
        /// <returns>Returns a Data Table</returns>
        public static DataTable CheckProfileAccess(DataTable dt)
        {
            TList<PortalProfile> storeList = GetTurnkeyStoreProfiles;
            if (storeList != null)
            {
                DataTable newDt = new DataTable();

                foreach (DataColumn column in dt.Columns)
                {
                    newDt.Columns.Add(column.ColumnName, column.DataType);
                }

                if (storeList.Count == 0)
                {
                    return newDt;
                }

                foreach (DataRow dataRow in dt.Rows)
                {
                    int ProfileID = 0;
                    int.TryParse(Convert.ToString(dataRow["ProfileID"]), out ProfileID);
                    PortalProfile found = storeList.Find(PortalProfileColumn.ProfileID, ProfileID);

                    if (found != null)
                    {
                        DataRow newRow = newDt.NewRow();
                        newRow.ItemArray = dataRow.ItemArray;
                        newDt.Rows.Add(newRow);
                    }
                }

                return newDt;
            }

            return dt;
        }

        /// <summary>
        /// Check Profile Access
        /// </summary>
        /// <param name="dt">Represents a DataTable</param>
        /// <param name="includeNull">Bool Value to include Null values or not</param>
        /// <returns>Returns a Data Table</returns>
        public static DataTable CheckProfileAccess(DataTable dt, bool includeNull)
        {
            TList<PortalProfile> storeList = GetTurnkeyStoreProfiles;
            if (storeList != null)
            {
                DataTable newDt = new DataTable();

                foreach (DataColumn column in dt.Columns)
                {
                    newDt.Columns.Add(column.ColumnName, column.DataType);
                }

                if (storeList.Count == 0)
                {
                    return newDt;
                }

                bool IsAdmin = GetTurnkeyStorePortalID == null;
                foreach (DataRow dataRow in dt.Rows)
                {
                    int ProfileID = 0;
                    int.TryParse(Convert.ToString(dataRow["ProfileID"]), out ProfileID);
                    PortalProfile found = storeList.Find(PortalProfileColumn.ProfileID, ProfileID);

                    if (found != null || (includeNull && dataRow["ProfileID"] == System.DBNull.Value) || IsAdmin)
                    {
                        DataRow newRow = newDt.NewRow();
                        newRow.ItemArray = dataRow.ItemArray;
                        newDt.Rows.Add(newRow);
                    }
                }

                return newDt;
            }

            return dt;
        }

        /// <summary>
        /// Check Profile Access Method
        /// </summary>
        /// <typeparam name="T">The value of T</typeparam>
        /// <param name="listObject">The value of list object</param>
        /// <returns>Returns a TList</returns>
        public static TList<T> CheckProfileAccess<T>(TList<T> listObject) where T : IEntity, new()
        {
            TList<PortalProfile> storeList = GetTurnkeyStoreProfiles;

            if (storeList.Count == 0)
            {
                return new TList<T>();
            }

            if (storeList != null)
            {
                TList<T> objects = listObject.Copy();

                for (int idx = 0; idx < objects.Count; idx++)
                {
                    T obj = objects[idx];
                    PortalProfile found = storeList.Find(PortalProfileColumn.ProfileID, obj.GetType().GetProperty("ProfileID").GetValue(obj, null));
                    if (found == null) 
                    { 
                        listObject.Remove(obj); 
                    }
                }
            }

            return listObject;
        }

        /// <summary>
        /// Check Profile Access Method
        /// </summary>
        /// <param name="profileID">The value of ProfileID</param>
        /// <returns>Returns a Bool value</returns>
        public static bool CheckProfileAccess(int profileID)
        {
            return CheckProfileAccess(profileID, false);
        }

        /// <summary>
        /// Check Profile Access Method
        /// </summary>
        /// <param name="profileID">The value of Profile ID</param>
        /// <param name="IsRedirect">Bool value to Redirect or not</param>
        /// <returns>Returns true or false</returns>
        public static bool CheckProfileAccess(int profileID, bool IsRedirect)
        {
            TList<PortalProfile> storeList = GetTurnkeyStoreProfiles;
            bool status = true;
            if (storeList != null)
            {
                PortalProfile found = storeList.Find(PortalProfileColumn.ProfileID, profileID);
                if (found == null) 
                { 
                    status = false; 
                }
            }

            if ((IsRedirect & !status) || storeList.Count == 0)
            {
                HttpContext.Current.Response.Redirect("~/FranchiseAdmin/Secure/default.aspx");
            }

            return status;
        }

        /// <summary>
        /// Check Catalog Access
        /// </summary>
        /// <param name="dt">Represents a Data table</param>
        /// <returns>Returns a Data Table</returns>
        public static DataTable CheckCatalogAccess(DataTable dt)
        {
            TList<Catalog> storeList = GetTurnkeyStoreCatalogs;
            if (storeList != null)
            {
                DataTable newDt = new DataTable();

                foreach (DataColumn column in dt.Columns)
                {
                    newDt.Columns.Add(column.ColumnName, column.DataType);
                }

                if (storeList.Count == 0)
                {
                    return newDt;
                }

                foreach (DataRow dataRow in dt.Rows)
                {
                    int CatalogID = 0;
                    int.TryParse(Convert.ToString(dataRow["CatalogID"]), out CatalogID);
                    Catalog found = storeList.Find(CatalogColumn.CatalogID, CatalogID);

                    if (found != null)
                    {
                        DataRow newRow = newDt.NewRow();
                        newRow.ItemArray = dataRow.ItemArray;
                        newDt.Rows.Add(newRow);
                    }
                }

                return newDt;
            }

            return dt;
        }

        /// <summary>
        /// Check Catalog Access
        /// </summary>
        /// <param name="dt">Represents a DataTable</param>
        /// <param name="includeNull">The bool value of includeNull</param>
        /// <returns>Returns a DataTable</returns>
        public static DataTable CheckCatalogAccess(DataTable dt, bool includeNull)
        {
            TList<Catalog> storeList = GetTurnkeyStoreCatalogs;
            if (storeList != null)
            {
                DataTable newDt = new DataTable();

                foreach (DataColumn column in dt.Columns)
                {
                    newDt.Columns.Add(column.ColumnName, column.DataType);
                }

                if (storeList.Count == 0)
                {
                    return newDt;
                }

                foreach (DataRow dataRow in dt.Rows)
                {
                    int CatalogID = 0;
                    int.TryParse(Convert.ToString(dataRow["CatalogID"]), out CatalogID);
                    Catalog found = storeList.Find(CatalogColumn.CatalogID, CatalogID);

                    if (found != null || (includeNull && dataRow["CatalogID"] == System.DBNull.Value))
                    {
                        DataRow newRow = newDt.NewRow();
                        newRow.ItemArray = dataRow.ItemArray;
                        newDt.Rows.Add(newRow);
                    }
                }

                return newDt;
            }

            return dt;
        }

        /// <summary>
        /// Check Catalog Access Method
        /// </summary>
        /// <param name="catalogID">The value of Catalog ID</param>
        /// <param name="isRedirect">The bool value IsRedirect</param>
        /// <returns>Returns a bool value that has the status</returns>
        public static bool CheckCatalogAccess(int catalogID, bool isRedirect)
        {
            TList<Catalog> storeList = GetTurnkeyStoreCatalogs;
            bool status = true;
            if (storeList != null)
            {
                Catalog found = storeList.Find(CatalogColumn.CatalogID, catalogID);
                if (found == null) 
                { 
                    status = false; 
                }
            }

            if ((isRedirect & !status) || (storeList.Count == 0))
            {
                HttpContext.Current.Response.Redirect("~/FranchiseAdmin/Secure/default.aspx");
            }

            return status;
        }

        /// <summary>
        /// Check Catalog Access
        /// </summary>
        /// <typeparam name="T">The value of T</typeparam>
        /// <param name="listObject">The value of List object</param>
        /// <returns>Returns the list object</returns>
        public static TList<T> CheckCatalogAccess<T>(TList<T> listObject) where T : IEntity, new()
        {
            TList<Catalog> storeList = GetTurnkeyStoreCatalogs;
            if (storeList != null)
            {
                if (storeList.Count == 0)
                {
                    return new TList<T>();
                }

                TList<T> objects = listObject.Copy();

                for (int idx = 0; idx < objects.Count; idx++)
                {
                    T obj = objects[idx];
                    Catalog found = storeList.Find(CatalogColumn.CatalogID, obj.GetType().GetProperty("CatalogID").GetValue(obj, null));
                    if (found == null) 
                    { 
                        listObject.Remove(obj); 
                    }
                }
            }

            return listObject;
        }
    }    
}
