﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Common.CustomClasses.WebControls
{
    public class Button : System.Web.UI.WebControls.Button
    {
        public ButtonType ButtonType { get; set; }

        protected override void AddAttributesToRender(System.Web.UI.HtmlTextWriter writer)
        {
            switch (ButtonType)
            {
                case ButtonType.SubmitButton:
                    CssClass += " submitbutton";
                    break;
                case ButtonType.CancelButton:
                    CssClass += " cancelbutton";
                    break;
                case ButtonType.EditButton:
                    CssClass += " editbutton";
                    break;
                default:
                    CssClass += " cancelbutton";
                    break;
            }

            base.AddAttributesToRender(writer);
        }

        // To Fire Click event when Default Button configured on Panel.
        protected override void OnLoad(System.EventArgs e)
        {
            Page.ClientScript.RegisterStartupScript(GetType(), "addClickFunctionScript",
                _addClickFunctionScript, true);

            string script = String.Format(_addClickScript, ClientID);
            Page.ClientScript.RegisterStartupScript(GetType(), "click_" + ClientID,
                script, true);
            base.OnLoad(e);
        }

        private const string _addClickScript = "addClickFunction('{0}');";

        private const string _addClickFunctionScript =
            @"  function addClickFunction(id) {{
            var b = document.getElementById(id);
            if (b && typeof(b.click) == 'undefined') b.click = function() {{
                var result = true; if (b.onclick) result = b.onclick();
                if (typeof(result) == 'undefined' || result) {{ eval(b.getAttribute('href')); }}
            }}}};";
    }
}
