﻿using System;
using System.Configuration;
using System.Data;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

namespace Znode.Engine.Common
{
    /// <summary>
    /// Represents the ZNodeEULA class
    /// </summary>
    public class ZNodeEULA
    {
        /// <summary>
        /// Initializes a new instance of the ZNodeEULA class.
        /// </summary>
        public ZNodeEULA()
        {
        }

        /// <summary>
        /// Get the Software License Agreement
        /// </summary>
        /// <returns>Returns the Software License Agreement</returns>
        public static string GetEULA()
        {
            string path = HttpContext.Current.Server.MapPath("~/eula.txt");
            return File.ReadAllText(path);
        }
    }
}