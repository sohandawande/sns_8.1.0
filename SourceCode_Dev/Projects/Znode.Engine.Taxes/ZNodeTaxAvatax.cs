﻿using Avalara.AvaTax.Adapter.TaxService;
using System;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.DataAccess.Service;

namespace Znode.Engine.Taxes
{
    public class ZNodeTaxAvatax : ZnodeTaxType
    {

        public ZNodeTaxAvatax()
        {
            Name = "Avatax";
            Description = "Applies Avalara Tax to shopiing cart";

            Controls.Add(ZnodeTaxRuleControl.Avatax);
            Controls.Add(ZnodeTaxRuleControl.VAT);
            Controls.Add(ZnodeTaxRuleControl.GST);
            Controls.Add(ZnodeTaxRuleControl.PST);
            Controls.Add(ZnodeTaxRuleControl.HST);
            Controls.Add(ZnodeTaxRuleControl.Precedence);
            Controls.Add(ZnodeTaxRuleControl.Inclusive);
        }

        //Custom Implemenation of Calculate Method.
        public override void Calculate()
        {
            

            GetTaxRequest getTaxRequestObject = new GetTaxRequest();
            Avalara.AvaTax.Adapter.AddressService.Address addressObject = new Avalara.AvaTax.Adapter.AddressService.Address();
            TaxSvc taxSvcObject = new TaxSvc();

            //Get the user type and if type is Generic then only call for tax calculation
            bool isTaxCallRequired = IsTaxCallRequired(ShoppingCart.CustomerExternalId);

            if (isTaxCallRequired && IsValid())
            {
                try
                {
                    bool isCustomerTaxExempt = false;

                    if (ShoppingCart.Payment != null && ShoppingCart.Payment.ShippingAddress != null && !string.IsNullOrEmpty(ShoppingCart.Payment.ShippingAddress.PostalCode))
                    {


                        if (IsShippedToListedInTaxableStates(ShoppingCart.Payment.ShippingAddress))
                        {
                            //Set From Shipping Address
                            getTaxRequestObject.OriginAddress = ShippingFromAddress();

                            //Set To Shipping Address
                            getTaxRequestObject.DestinationAddress = ShippingToAddress();
                            getTaxRequestObject.CustomerCode = Convert.ToString(ConfigurationManager.AppSettings["GenericUserExternalID"]);

                            // Add the orderline items into the tax class.
                            int index = 1;

                            foreach (ZNode.Libraries.ECommerce.Entities.ZNodeShoppingCartItem item in ShoppingCart.ShoppingCartItems)
                            {
                                Line lineObject = new Line();
                                lineObject.No = index.ToString();
                                lineObject.ItemCode = item.Product.ProductNum;
                                lineObject.Qty = item.Quantity;
                                lineObject.Amount = item.ExtendedPrice;
                                lineObject.Discounted = false;
                                lineObject.TaxCode = isCustomerTaxExempt ? // Tax Code For Products
                                    ConfigurationManager.AppSettings["AvataxProductTaxExemptedCode"].ToString() :
                                    ConfigurationManager.AppSettings["AvataxProductTaxCode"].ToString();
                                getTaxRequestObject.Lines.Add(lineObject);
                                index++;
                            }

                            //get shipping code
                            string shipTaxCode = ConfigurationManager.AppSettings["AvataxShippingTaxCode"].ToString();
                            decimal completeShippingCost = ShoppingCart.ShippingCost;

                            //Added the Shipping line Item for calculating avatax.
                            //This condition 'shoppingCart.ShippingCost > 0' will not add item for free shipping in AVATAX.
                            if (ShoppingCart != null && completeShippingCost > 0 && ShoppingCart.Shipping != null)
                            {
                                if (IsShippingCostTaxableForState(ShoppingCart.Payment.ShippingAddress))
                                {
                                    Line lineObject = new Line();
                                    lineObject.No = index.ToString();
                                    lineObject.ItemCode = string.IsNullOrEmpty(ShoppingCart.Shipping.ShippingName) ? ConfigurationManager.AppSettings["AvalaraShippingDescription"].ToString() : ShoppingCart.Shipping.ShippingName;
                                    lineObject.Description = string.IsNullOrEmpty(ShoppingCart.Shipping.ShippingName) ? ConfigurationManager.AppSettings["AvalaraShippingDescription"].ToString() : ShoppingCart.Shipping.ShippingName;
                                    lineObject.Qty = 1;
                                    lineObject.Amount = completeShippingCost;
                                    lineObject.Discounted = false;
                                    lineObject.TaxCode = isCustomerTaxExempt ? ConfigurationManager.AppSettings["AvalaraProductLevelTaxExemptionCode"].ToString() : shipTaxCode;
                                    getTaxRequestObject.Lines.Add(lineObject);
                                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("shipping cost "+ completeShippingCost+" is included for tax calculation");
                                }
                            }

                            getTaxRequestObject.DocDate = DateTime.Today;
                            getTaxRequestObject.DocType = DocumentType.SalesOrder; //As invoice is not generated from znode. Other value is DocumentType.SalesInvoice                                                     
                            getTaxRequestObject.Discount = 0m;
                            getTaxRequestObject.ExemptionNo = null;
                            getTaxRequestObject.DetailLevel = DetailLevel.Tax;
                            getTaxRequestObject.PurchaseOrderNo = null;
                            getTaxRequestObject.SalespersonCode = null;

                            // Retrieve encrypted credientials

                            if (!string.IsNullOrEmpty(TaxBag.Custom1))
                            {
                                string[] credientails = TaxBag.Custom1.Split('|');
                                if (credientails.Length > 0 && credientails.Length == 6)
                                {

                                    getTaxRequestObject.CompanyCode = credientails[2].Split('=')[1];
                                    taxSvcObject.Configuration.Url = credientails[3].Split('=')[1];
                                    taxSvcObject.Configuration.RequestTimeout = 100;
                                    taxSvcObject.Configuration.Security.Account = credientails[0].Split('=')[1];
                                    taxSvcObject.Configuration.Security.License = credientails[1].Split('=')[1];
                                    taxSvcObject.Profile.Client = credientails[2].Split('=')[1];
                                    taxSvcObject.Profile.Name = credientails[2].Split('=')[1];

                                }
                            }

                            //PRFT Custom Code for allowing TLS12 with the new Avatax updates
                            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                            GetTaxResult getTaxResultObject = taxSvcObject.GetTax(getTaxRequestObject);
                            //Zeon Custom Code
                            //System.Web.HttpContext.Current.Session["AvataxResponse"] = getTaxResultObject;
                            SetTaxResponse(getTaxResultObject);
                        }
                    }

                    //}
                }
                catch (Exception ex)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.ToString());
                    ShoppingCart.AddErrorMessage = "Unable to calculate tax rates at this time. Please try again later.";
                }
            }
        }

        private bool IsShippedToListedInTaxableStates(ZNode.Libraries.DataAccess.Entities.Address shippingAddress)
        {
            if (!string.IsNullOrEmpty(shippingAddress.StateCode))
            {
                MessageConfigService messageConfigService = new MessageConfigService();
                var messageConfigList = messageConfigService.GetByKeyPortalIDLocaleIDMessageTypeID("TaxableStates", ZNodeConfigManager.SiteConfig.PortalID, 43, 1);
                string comaSeparatedTaxableStates = string.Empty;
                if (messageConfigList != null && messageConfigList.Count > 0 && !string.IsNullOrEmpty(messageConfigList[0].Value))
                {
                    comaSeparatedTaxableStates = messageConfigList[0].Value;
                }
                bool isShipToStateExists = comaSeparatedTaxableStates.Split(',').Contains(shippingAddress.StateCode.Trim());
                return isShipToStateExists;
            }
            return false;
        }

        private bool IsShippingCostTaxableForState(ZNode.Libraries.DataAccess.Entities.Address shippingAddress)
        {
            if (!string.IsNullOrEmpty(shippingAddress.StateCode))
            {
                MessageConfigService messageConfigService = new MessageConfigService();
                var messageConfigList = messageConfigService.GetByKeyPortalIDLocaleIDMessageTypeID("NoShippingTaxableStates", ZNodeConfigManager.SiteConfig.PortalID, 43, 1);
                string comaSeparatedNoShippingTaxableStates = string.Empty;
                if (messageConfigList != null && messageConfigList.Count > 0 && !string.IsNullOrEmpty(messageConfigList[0].Value))
                {
                    comaSeparatedNoShippingTaxableStates = messageConfigList[0].Value;
                }
                bool isShipToStateExists = !comaSeparatedNoShippingTaxableStates.Split(',').Contains(shippingAddress.StateCode.Trim());
                return isShipToStateExists;
            }
            return true;
        }

        /// <summary>
        /// Check if logged in customer is B2B or not
        /// </summary>
        /// <returns></returns>
        private bool IsTaxCallRequired(string customerExternalId)
        {
            bool isTaxCallRequired = false;
            bool isTaxAllowedForAllUsers = false;
            bool.TryParse(Convert.ToString(ConfigurationManager.AppSettings["AllowTaxForAllUser"]), out isTaxAllowedForAllUsers);

            if (isTaxAllowedForAllUsers)
            {
                isTaxCallRequired = true;
            }
            else
            {
                if (!string.IsNullOrEmpty(customerExternalId))
                {
                    string genericCustomerID = Convert.ToString(ConfigurationManager.AppSettings["GenericUserExternalID"]);
                    if (genericCustomerID.Trim().Equals(customerExternalId.Trim()))
                    {
                        isTaxCallRequired = true;
                    }
                }
                else
                {
                    isTaxCallRequired = true;
                }
            }
            return isTaxCallRequired;
        }


        #region Private Methods

        private void SetTaxResponse(GetTaxResult getTaxResultObject)
        {
            //// Get tax from the Webservice.
            decimal taxSalesTax = 0;
            if (getTaxResultObject.ResultCode != Avalara.AvaTax.Adapter.SeverityLevel.Success)
            {
                int errorCount = getTaxResultObject.Messages.Count;

                for (int errorIndex = 0; errorIndex <= errorCount - 1; errorIndex++)
                {
                    StringBuilder errorText = new StringBuilder();
                    errorText.Append("Avatax Error: \n");
                    errorText.Append(getTaxResultObject.Messages[errorIndex].Name + "\n");
                    errorText.Append(getTaxResultObject.Messages[errorIndex].Severity + "\n");
                    errorText.Append(getTaxResultObject.Messages[errorIndex].Summary + "\n");
                    errorText.Append(getTaxResultObject.Messages[errorIndex].Details + "\n");
                    errorText.Append(getTaxResultObject.Messages[errorIndex].Source + "\n");
                    errorText.Append(getTaxResultObject.Messages[errorIndex].RefersTo + "\n");
                    errorText.Append(getTaxResultObject.Messages[errorIndex].HelpLink + "\n");

                    // Log the error message received from Avatax.
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(errorText.ToString());
                    //ZNode.Libraries.ECommerce.Utilities.ZActivityLogHelper.LogMessage(errorText.ToString());

                    //As we have implement avalara tax calculation with developer license. If error occured then user is able to placed order with SaleTax = 0
                    //_ShoppingCart.AddErrorMessage = "Unable to calculate tax rates at this time. Please try again later.";
                    ShoppingCart.SalesTax = 0;

                }
            }
            else
            {
                // If success                                
                int taxCount = getTaxResultObject.TaxLines.Count;
                ShoppingCart.SalesTax = 0;
                for (int taxIndex = 0; taxIndex < taxCount; taxIndex++)
                {
                    if (getTaxResultObject.TaxLines[taxIndex] != null)
                    {
                        taxSalesTax = getTaxResultObject.TaxLines[taxIndex].Tax;
                        int lineItemIndex = 0;
                        int.TryParse(getTaxResultObject.TaxLines[taxIndex].No, out lineItemIndex);
                        if (lineItemIndex <= ShoppingCart.ShoppingCartItems.Count)
                        {
                            ShoppingCart.ShoppingCartItems[lineItemIndex - 1].Product.SalesTax = taxSalesTax;
                            ShoppingCart.ShoppingCartItems[lineItemIndex - 1].IsTaxCalculated = true;
                        }
                        ShoppingCart.SalesTax += taxSalesTax;

                    }
                }

                if (ShoppingCart.ErrorMessage.Contains("Unable to calculate tax"))
                {
                    ShoppingCart.AddErrorMessage = string.Empty;
                }
            }
        }
        private static Avalara.AvaTax.Adapter.AddressService.Address ShippingFromAddress()
        {
            Avalara.AvaTax.Adapter.AddressService.Address shippingFrom = new Avalara.AvaTax.Adapter.AddressService.Address();

            // Shipping from Address
            shippingFrom.Line1 = ZNodeConfigManager.SiteConfig.ShippingOriginAddress1;
            shippingFrom.Line2 = ZNodeConfigManager.SiteConfig.ShippingOriginAddress2;
            shippingFrom.Line3 = null;
            shippingFrom.City = ZNodeConfigManager.SiteConfig.ShippingOriginCity;
            shippingFrom.Region = ZNodeConfigManager.SiteConfig.ShippingOriginStateCode;
            shippingFrom.PostalCode = ZNodeConfigManager.SiteConfig.ShippingOriginZipCode;
            shippingFrom.Country = ZNodeConfigManager.SiteConfig.ShippingOriginCountryCode;
            return shippingFrom;
        }

        private Avalara.AvaTax.Adapter.AddressService.Address ShippingToAddress()
        {
            Avalara.AvaTax.Adapter.AddressService.Address shippingTo = new Avalara.AvaTax.Adapter.AddressService.Address();

            //Zeon Custom Code:Start
            //Commented since _UserAccount holds object of logged in user and if Order is placed by Admin on behalf of Other User then it is not correct.
            //Use shopping cart's Payments shipping address which always holds address of customer for which order is being placed

            //shippingTo.Line1 = _UserAccount.ShippingAddress.Street;
            //shippingTo.Line2 = _UserAccount.ShippingAddress.Street1;
            //shippingTo.Line3 = null;
            //shippingTo.City = _UserAccount.ShippingAddress.City;
            //shippingTo.Region = _UserAccount.ShippingAddress.StateCode;
            //shippingTo.PostalCode = _UserAccount.ShippingAddress.PostalCode;
            //shippingTo.Country = _UserAccount.ShippingAddress.CountryCode;

            if (ShoppingCart != null && ShoppingCart.Payment != null && ShoppingCart.Payment.ShippingAddress != null)
            {
                shippingTo.Line1 = ShoppingCart.Payment.ShippingAddress.Street;
                shippingTo.Line2 = ShoppingCart.Payment.ShippingAddress.Street1;
                shippingTo.Line3 = null;
                shippingTo.City = ShoppingCart.Payment.ShippingAddress.City;
                shippingTo.Region = ShoppingCart.Payment.ShippingAddress.StateCode;
                shippingTo.PostalCode = ShoppingCart.Payment.ShippingAddress.PostalCode;
                shippingTo.Country = ShoppingCart.Payment.ShippingAddress.CountryCode;
            }

            //Zeon Custom Code:End
            return shippingTo;
        }

        #endregion
    }
}
