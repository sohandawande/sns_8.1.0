using System;
using System.Data;
using System.Web;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Taxes
{
	/// <summary>
	/// Used to calculate and return inclusive/exclusive pricing for a product.
	/// </summary>
	[Serializable]
	public class ZnodeInclusiveTax : ZNodeBusinessBase
	{
		/// <summary>
		/// Checks to see if the current profile is tax exempt.
		/// </summary>
		public bool TaxExemptProfile
		{
			get
			{
				if (HttpContext.Current.Session["ProfileCache"] != null)
				{
					var customerProfile = (Profile)HttpContext.Current.Session["ProfileCache"];
					if (customerProfile != null)
					{
						return customerProfile.TaxExempt;
					}
				}

				return false;
			}
		}

		/// <summary>
		/// Caches the inclusive tax rules.
		/// </summary>
		public void CacheInclusiveTax()
		{
			if (ZNodeConfigManager.SiteConfig.InclusiveTax && !TaxExemptProfile)
			{
				if (HttpContext.Current.Cache["InclusiveTaxRules"] == null)
				{
					var taxHelper = new TaxHelper();

					var dataTable = taxHelper.GetInclusiveTaxRules();
					dataTable.TableName = "TaxRules";

					ZNodeCacheDependencyManager.Insert("InclusiveTaxRules", dataTable, "ZNodeTaxRule");

					// Release memory
					dataTable.Dispose();
				}
			}
		}

		/// <summary>
		/// Gets data from the inclusive tax cache.
		/// </summary>
		private DataTable InclusiveTaxTable
		{
			get
			{
				var dataTable = (DataTable)HttpRuntime.Cache.Get("InclusiveTaxRules");
				if (dataTable == null)
				{
					CacheInclusiveTax();
					dataTable = (DataTable)HttpRuntime.Cache.Get("InclusiveTaxRules");
				}

				return dataTable;
			}
		}

		/// <summary>
		/// Gets the tax rules for the tax class.
		/// </summary>
		/// <param name="taxClassId">The ID of the tax class.</param>
		/// <param name="address">The shipping address.</param>
		/// <returns>The tax rules for the tax class.</returns>
		private DataRowCollection GetRulesByTaxClassId(int taxClassId, Address address)
		{
			DataRowCollection rows = null;

			if (InclusiveTaxTable != null)
			{
				var filterExpression = "TaxClassId=" + taxClassId;
				var tempTable = new DataTable();
				tempTable = InclusiveTaxTable.Clone();

				var row = tempTable.NewRow();

				var userAccount = (ZNodeUserAccount)HttpContext.Current.Session["AliasUserAccount"];
				if (userAccount == null)
				{
					userAccount = ZNodeUserAccount.CurrentAccount();
				}

				if ((address == null || address.AddressID == 0) && userAccount != null)
				{
					address = userAccount.ShippingAddress;
				}

				if (address != null)
				{
					filterExpression += " AND (DestinationCountryCode = ''";
					filterExpression += " OR (DestinationCountryCode = '" + address.CountryCode + "'";
					filterExpression += " AND DestinationStateCode = '')";
					filterExpression += " OR (DestinationCountryCode = '" + address.CountryCode + "'";
					filterExpression += " AND DestinationStateCode = '" + address.StateCode + "'))";

					var filterRows = InclusiveTaxTable.Select(filterExpression, "CountyFIPS,DestinationStateCode,DestinationCountryCode,Precedence DESC");

					foreach (var dataRow in filterRows)
					{
						if (dataRow["DestinationStateCode"].ToString() == "")
						{
							row = dataRow;
						}
						else
						{
							if (dataRow["CountyFIPS"].ToString() == "")
							{
								row = dataRow;
							}
							else
							{
								var filters = new ZipCodeQuery();
								filters.Append(ZipCodeColumn.CountyFIPS, dataRow["CountyFIPS"].ToString());
								filters.Append(ZipCodeColumn.CityName, address.City);
								filters.Append(ZipCodeColumn.StateAbbr, address.StateCode);
								filters.Append(ZipCodeColumn.ZIP, address.PostalCode);

								var zipService = new ZipCodeService();
								var zipCodeList = zipService.Find(filters.GetParameters());

								if (zipCodeList.Count != 0)
								{
									row = dataRow;
								}
							}
						}
					}

					if (row.RowState != DataRowState.Detached)
					{
						tempTable.Rows.Add(row.ItemArray);
					}

					rows = tempTable.Rows;
				}
				else
				{
					filterExpression += " AND DestinationCountryCode = ''";

					var filterRows = InclusiveTaxTable.Select(filterExpression, "Precedence ASC");

					foreach (var dataRow in filterRows)
					{
						row = dataRow;
						break;
					}

					if (row.RowState != DataRowState.Detached)
					{
						tempTable.Rows.Add(row.ItemArray);
					}

					rows = tempTable.Rows;
				}
			}

			return rows;
		}

		/// <summary>
		/// Gets the product price including the tax.
		/// </summary>
		/// <param name="taxClassId">The ID of the tax class.</param>
		/// <param name="productPrice">The product price.</param>
		/// <param name="address">The shipping address.</param>
		/// <returns>The price of the product including tax.</returns>
		public decimal GetInclusivePrice(int taxClassId, decimal productPrice, Address address)
		{
			decimal totalTax = 0;

			if (ZNodeConfigManager.SiteConfig.InclusiveTax && !TaxExemptProfile)
			{
				if (taxClassId > 0 && productPrice > 0)
				{
					var rows = GetRulesByTaxClassId(taxClassId, address);
					if (rows != null)
					{
						foreach (DataRow row in rows)
						{
							var taxRate = decimal.Parse(row["TaxRate"].ToString());
							totalTax += productPrice * (taxRate / 100);
						}
					}
				}
			}

			totalTax = Math.Round(totalTax, 2);
			return productPrice + totalTax;
		}

		/// <summary>
		/// Gets the total tax rate for the given tax class.
		/// </summary>
		/// <param name="taxClassId">The ID of the tax class.</param>
		/// <returns>The total tax rate for the tax class.</returns>
		public decimal GetInclusiveTaxRate(int taxClassId)
		{
			decimal totalTaxRate = 0;

			if (ZNodeConfigManager.SiteConfig.InclusiveTax && !TaxExemptProfile)
			{
				if (taxClassId > 0)
				{
					var rows = GetRulesByTaxClassId(taxClassId, null);
					if (rows != null)
					{
						foreach (DataRow row in rows)
						{
							var taxRate = decimal.Parse(row["TaxRate"].ToString());
							totalTaxRate += taxRate;
						}
					}
				}
			}

			return totalTaxRate;
		}
	}
}
