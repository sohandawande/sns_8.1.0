﻿using System.Collections.ObjectModel;
using ZNode.Libraries.ECommerce.Entities;

namespace Znode.Engine.Taxes
{
	/// <summary>
	/// This is the root interface for all tax types.
	/// </summary>
	public interface IZnodeTaxType : IZnodeProviderType
    {
		int Precedence { get; set; }
		Collection<ZnodeTaxRuleControl> Controls { get; }

		void Bind(ZNodeShoppingCart shoppingCart, ZnodeTaxBag taxBag);
		void Calculate();
		bool PreSubmitOrderProcess();
		void PostSubmitOrderProcess();
    }
}
