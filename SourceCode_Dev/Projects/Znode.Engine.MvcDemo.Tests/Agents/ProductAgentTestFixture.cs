﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Znode.Engine.MvcDemo.Agents;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Tests.Agents
{
    
    [TestFixture]
    public class ProductAgentTestFixture : BaseFixture
    {
        [Test]
        public void Test01_GetProduct()
        {
            IProductAgent agent = new ProductAgent();

            var viewModel = agent.GetProduct(302);

            Assert.IsNotNull(viewModel);

            Assert.IsTrue(viewModel.ProductId == 302);
        }

        [Test]
        public void Test02_GetAttributes()
        {
            IProductAgent agent = new ProductAgent();
            string selectedSku = string.Empty;
            string imagePath = string.Empty;
            //ZNode Version 7.2.2 - Add imageMediumPath Out Parameter
            string imageMediumPath = string.Empty;
            decimal productPrice;
            var viewModel = agent.GetAttributes(302, 4, "4", 2, out productPrice, out selectedSku, out imagePath,out imageMediumPath);
            Assert.IsNotNull(viewModel.ProductAttributes);
            Assert.IsTrue(viewModel.ProductAttributes.ProductId == 302);
        }

        [Test]
        public void Test03_GetHomeSpecials()
        {
            IProductAgent agent = new ProductAgent();
            Collection<ProductViewModel> viewModel = agent.GetHomeSpecials();
            Assert.IsNotNull(viewModel);
            Assert.IsTrue(viewModel.Count > 1);
        }

        [Test]
        public void Test04_GetBundles()
        {
            IProductAgent agent =  new ProductAgent();
            List<BundleDisplayViewModel> viewModel = agent.GetBundles("302,303");
            Assert.IsNotNull(viewModel);
            Assert.IsTrue(viewModel.Count > 1);
        }

        [Test]
        public void Test05_GetProductReviews()
        {
            IProductAgent agent = new ProductAgent();
            ReviewViewModel viewModel = agent.GetAllReviews(303);
            Assert.IsNotNull(viewModel.Items);
            Assert.IsTrue(viewModel.Items.Count > 1);
        }

        [Test]
        public void Test06_GetBundleProductsDetails()
        {
            IProductAgent agent = new ProductAgent();
            var cartModel = new CartItemViewModel();
            cartModel.ProductId = 329;
            cartModel.Quantity = 1;
            cartModel.ExternalId = "24c539c1-4f99-4d19-8fc0-e3c1537325a3";
            cartModel.ProductName = "BAABundle";
            cartModel.BundleItemsIds = "302,303";
            BundleDetailsViewModel model = agent.GetBundleProductsDetails(cartModel);
            Assert.IsNotNull(model.Bundles);
        }

        [Test]
        public void Test07_GetAddOnDetails()
        {
            IProductAgent agent  = new ProductAgent();
            ProductViewModel productModel = agent.GetProduct(303);
            string selectedAddOnIds = "3";
            decimal productPrice;
            decimal finalProductPrice;
            int quantity = 1;
            productPrice = 2.65M;
            var model = agent.GetAddOnDetails(productModel, selectedAddOnIds, quantity, productPrice, out finalProductPrice);
            Assert.IsNotNull(model);
        }
       
        [Test]
        public void Test08_CreateWishlist()
        {
            IProductAgent productAgent =  new ProductAgent();
            IAccountAgent accountAgent =  new AccountAgent();
            var loginViewModel = new LoginViewModel();
            loginViewModel.Username = "testuser1";
            loginViewModel.Password = "password1";

            var loginModel = accountAgent.Login(loginViewModel);

            var wishlistmodel = productAgent.CreateWishList(303);
            Assert.IsNotNull(wishlistmodel);
            Assert.IsTrue(wishlistmodel.Items.Any(x => x.ProductId == 303));
        }

        [Test]
        public void Test09_CreateReview()
        {
            IProductAgent productAgent = new ProductAgent();
            IAccountAgent accountAgent = new AccountAgent();
            var loginViewModel = new LoginViewModel();
            loginViewModel.Username = "testuser1";
            loginViewModel.Password = "password1";

            var loginModel = accountAgent.Login(loginViewModel);
            var reviewItemModel = new ReviewItemViewModel()
                {
                    AccountId = 11547,
                    CreateDate = System.DateTime.Now.ToString(),
                    CreateUser = "Edward Cullen",
                    Duration = string.Empty,
                    Comments = "These Pear fruits are very good, highly recommended.",
			        ProductId = 304,
				    Rating = 5,
				    Status = "A",
				    Subject = "Very delicious Pear!",
				    UserLocation = "Columbus, OH"
                };
            var reviewModel = productAgent.CreateReview(reviewItemModel);
            Assert.IsNotNull(reviewModel);
            Assert.IsTrue(reviewModel.ProductId == 304);
        }
    }
}
