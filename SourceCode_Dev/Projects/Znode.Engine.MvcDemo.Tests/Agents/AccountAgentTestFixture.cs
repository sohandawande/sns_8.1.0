﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Znode.Engine.MvcDemo.Agents;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Tests.Agents
{
    [TestFixture]
    public class AccountAgentTestFixture : BaseFixture
    {
        [Test]
        public void Test01_SignUp()
        {
            IAccountAgent agent = new AccountAgent();

            var registerViewModel = new RegisterViewModel();
            registerViewModel.EmailAddress = "test1@znode.com";
            registerViewModel.UserName = "testuser" + DateTime.Now.Ticks.ToString();
            registerViewModel.Password = "password1";
            registerViewModel.ReTypePassword = "password1";
            registerViewModel.EmailOptIn = true;
            
            registerViewModel = agent.SignUp(registerViewModel);

            Assert.IsNotNull(registerViewModel);
            Assert.AreEqual("test1@znode.com", registerViewModel.EmailAddress);
        }

        [Test]
        public void Test02_SignUpUser()
        {
            IAccountAgent agent = new AccountAgent();

            var registerViewModel = new RegisterViewModel
            {
                EmailAddress = "test1@znode.com",
                UserName = "testuser1",
                Password = "password1",
                ReTypePassword = "password1",
                EmailOptIn = true
            };

            agent.SignUp(registerViewModel);

            registerViewModel = new RegisterViewModel
                {
                    EmailAddress = "test1@znode.com",
                    UserName = "testuser1",
                    Password = "password1",
                    ReTypePassword = "password1",
                    EmailOptIn = true
                };

            registerViewModel = agent.SignUp(registerViewModel);

            Assert.IsNotNull(registerViewModel);
            Assert.IsNull(registerViewModel.UserName);
        }

        [Test]
        public void Test03_SignUpUserInvalid()
        {
            IAccountAgent agent = new AccountAgent();

            var registerViewModel = new RegisterViewModel
            {
                EmailAddress = "test1@znode.com",
                UserName = "testuser1",
                Password = "password1",
                ReTypePassword = "password1",
                EmailOptIn = true
            };

            registerViewModel = agent.SignUp(registerViewModel);

            Assert.IsNotNull(registerViewModel);
            Assert.AreEqual("testuser1 User Name is  not available", registerViewModel.ErrorMessage);
        }

        [Test]
        public void Test04_LoginUser()
        {
            IAccountAgent agent = new AccountAgent();

            var loginViewModel = new LoginViewModel
                {
                    Username = "testuser1",
                    Password = "password1"
                };

            loginViewModel = agent.Login(loginViewModel);

            Assert.IsNotNull(loginViewModel);
            Assert.AreEqual("testuser1", loginViewModel.Username);
        }

        [Test]
        public void Test05_LoginWrongUser()
        {
            IAccountAgent agent = new AccountAgent();

            var loginViewModel = new LoginViewModel
            {
                Username = "testuser1",
                Password = "password2"
            };

            loginViewModel = agent.Login(loginViewModel);

            Assert.IsNotNull(loginViewModel);
            Assert.AreEqual("Login failed", loginViewModel.ErrorMessage);
        }


        [Test]
        public void Test06_GetAccountViewModel()
        {
            IAccountAgent agent = new AccountAgent();

            var loginViewModel = new LoginViewModel
            {
                Username = "testuser1",
                Password = "password1"
            };

            loginViewModel = agent.Login(loginViewModel);

            Assert.IsNotNull(loginViewModel);
            Assert.AreEqual("testuser1", loginViewModel.Username);

            var accountViewModel = agent.GetAccountViewModel();

            Assert.IsNotNull(accountViewModel);
            Assert.AreEqual("testuser1", accountViewModel.UserName);

            
        }


        [Test]
        public void Test07_GetDashboard()
        {
            IAccountAgent agent = new AccountAgent();

            var loginViewModel = new LoginViewModel
            {
                Username = "testuser1",
                Password = "password1"
            };

            loginViewModel = agent.Login(loginViewModel);

            Assert.IsNotNull(loginViewModel);
            Assert.AreEqual("testuser1", loginViewModel.Username);

            var dashBoardViewModel = agent.GetDashboard();

            Assert.IsNotNull(dashBoardViewModel);
            Assert.AreEqual("testuser1", dashBoardViewModel.UserName);
        }

        [Test]
        public void Test08_CreateWishListByProductId()
        {
            IAccountAgent agent = new AccountAgent();

            Login();

            var wishListViewModel = agent.CreateWishList(351);

            Assert.IsNotNull(wishListViewModel);
            Assert.IsTrue(wishListViewModel.Items.Count > 0);
        }

        [Test]
        public void Test09_GetWishlistByProductId()
        {
            IAccountAgent agent = new AccountAgent();

            Login();

            var wishId = agent.GetWishlistByProductId(302);

            Assert.IsTrue(wishId > 0);
        }

        [Test]
        public void Test10_GetWishlist()
        {
            IAccountAgent agent = new AccountAgent();

            Login();

            var wishListViewModel = agent.GetWishList();

            Assert.IsNotNull(wishListViewModel);
            Assert.IsTrue(wishListViewModel.Items.Count > 0);
        }

        [Test]
        public void Test11_CheckWishListNotExists()
        {
            IAccountAgent agent = new AccountAgent();

            Login();

            var wishListId = agent.CheckWishListExists(303, 11522);

            Assert.IsTrue(wishListId == 0);
        }

        [Test]
        public void Test12_CheckWishListExists()
        {
            IAccountAgent agent = new AccountAgent();

            Login();

            var accountViewModel = agent.GetAccountViewModel();

            var wishListId = agent.CheckWishListExists(302, accountViewModel.AccountId);

            Assert.IsTrue(wishListId > 0);
        }

        [Test]
        public void Test13_DeleteWishList()
        {
            IAccountAgent agent = new AccountAgent();

            Login();

            var wishListViewModel = agent.CreateWishList(347);
            
            var wishId = agent.GetWishlistByProductId(347);

            var isDeleted = agent.DeleteWishList(wishId);

            Assert.IsTrue(isDeleted);
        }

        // To Do Create a Order before checking the receipt
        [Test]
        public void Test14_GetOrderReceiptDetails()
        {
            IAccountAgent agent = new AccountAgent();

            Login();

            var accountViewModel = agent.GetAccountViewModel();

            if (accountViewModel.Orders != null)
            {
                var ordersViewModel = agent.GetOrderReceiptDetails(1);
                Assert.IsNotNull(ordersViewModel);
                Assert.AreEqual("1", ordersViewModel.OrderId);
            }
        }

        [Test]
        public void Test15_CreateUpdateAddress()
        {
            IAccountAgent agent = new AccountAgent();

            Login();

            var addressViewModel = new AddressViewModel
                {
                    Name = "Home Address",
                    FirstName = "FirstName",
                    LastName = "LastName",
                    CompanyName = "CompanyName",
                    StreetAddress1 = "StreetAddress1",
                    StreetAddress2 = "StreetAddress2",
                    City = "City",
                    StateCode = "OH",
                    CountryCode = "US",
                    PostalCode = "40256",
                    IsDefaultBilling = true,
                    IsDefaultShipping = true
                };

            addressViewModel = agent.UpdateAddress(addressViewModel);

            Assert.IsNotNull(addressViewModel);
            Assert.IsTrue(addressViewModel.AddressId > 0);
        }

        [Test]
        public void Test16_CreateUpdateAddress()
        {
            IAccountAgent agent = new AccountAgent();

            Login();

            var addressViewModel = new AddressViewModel
            {
                Name = "Office Address",
                FirstName = "Office FirstName",
                LastName = "Office LastName",
                CompanyName = "Office CompanyName",
                StreetAddress1 = "Office StreetAddress1",
                StreetAddress2 = "Office StreetAddress2",
                City = "Office City",
                StateCode = "OH",
                CountryCode = "US",
                PostalCode = "40588",
                IsDefaultBilling = true,
                IsDefaultShipping = false
            };

            addressViewModel = agent.UpdateAddress(addressViewModel);

            Assert.IsNotNull(addressViewModel);
            Assert.IsTrue(addressViewModel.AddressId > 0);


        }

        [Test]
        public void Test17_UpdateAddress()
        {
            IAccountAgent agent = new AccountAgent();

            Login();

            var accountViewModel = agent.GetAccountViewModel();

            var addressViewModel = new AddressViewModel
            {
                AddressId = accountViewModel.Addresses != null ? accountViewModel.Addresses[0].AddressId : 2,
                AccountId = accountViewModel.AccountId,
                Name = "Update Home Address",
                FirstName = "UpdateFirstName",
                LastName = "UpdateLastName",
                CompanyName = "UpdateCompanyName",
                StreetAddress1 = "UpdateStreetAddress1",
                StreetAddress2 = "UpdateStreetAddress2",
                City = "UpdateCity",
                StateCode = "OH",
                CountryCode = "US",
                PostalCode = "40256",
                IsDefaultBilling = false,
                IsDefaultShipping = true
            };

            addressViewModel = agent.UpdateAddress(addressViewModel);

            Assert.IsNotNull(addressViewModel);
            Assert.IsTrue(addressViewModel.AddressId > 0);
        }

        [Test]
        public void Test18_UpdateDefaultShippingAddress()
        {
            IAccountAgent agent = new AccountAgent();

            Login();

            var accountViewModel = agent.GetAccountViewModel();

            var addressViewModel = agent.UpdateAddress(accountViewModel.Addresses != null ? accountViewModel.Addresses[1].AddressId : 2, false);

            Assert.IsNotNull(addressViewModel);
            Assert.IsTrue(addressViewModel.AddressId > 0);
        }

        [Test]
        public void Test19_UpdateDefaultBillingAddress()
        {
            IAccountAgent agent = new AccountAgent();

            Login();

            var accountViewModel = agent.GetAccountViewModel();

            var addressViewModel = agent.UpdateAddress(accountViewModel.Addresses != null ? accountViewModel.Addresses[0].AddressId : 2, true);

            Assert.IsNotNull(addressViewModel);
            Assert.IsTrue(addressViewModel.AddressId > 0);
        }

        [Test]
        public void Test20_GetAddress()
        {
            IAccountAgent agent = new AccountAgent();

            Login();

            var accountViewModel = agent.GetAccountViewModel();

            var addressViewModel = agent.GetAddress(accountViewModel.Addresses != null ? accountViewModel.Addresses[0].AddressId : 2);

            Assert.IsNotNull(addressViewModel);
            Assert.IsTrue(addressViewModel.AddressId > 0);
        }

        [Test]
        public void Test21_GetStates()
        {
            IAccountAgent agent = new AccountAgent();

            var states = agent.GetStates();

            Assert.IsNotNull(states);
            Assert.IsTrue(states.Count > 1);
        }

        [Test]
        public void Test22_DeleteAddress()
        {
            IAccountAgent agent = new AccountAgent();

            Login();

            var addressViewModel = new AddressViewModel
            {
                Name = "Delete Address",
                FirstName = "FirstName",
                LastName = "LastName",
                CompanyName = "CompanyName",
                StreetAddress1 = "StreetAddress1",
                StreetAddress2 = "StreetAddress2",
                City = "City",
                StateCode = "OH",
                CountryCode = "US",
                PostalCode = "40256",
                IsDefaultBilling = true,
                IsDefaultShipping = true
            };

            addressViewModel = agent.UpdateAddress(addressViewModel);

            addressViewModel = agent.DeleteAddress(addressViewModel.AddressId);

            Assert.IsNotNull(addressViewModel);
            Assert.IsTrue(addressViewModel.AddressId > 0);
        }

        // To Do Call Write Review and then get review
        [Test]
        public void Test23_GetReviews()
        {
            IAccountAgent agent = new AccountAgent();
            IProductAgent prodAgent = new ProductAgent();

            Login();

            var reviewViewModel = agent.GetReviews();

            Assert.IsNotNull(reviewViewModel);
            Assert.IsTrue(reviewViewModel.Items.Count > 0);
        }

        [Test]
        public void Test24_UpdateProfile()
        {
            IAccountAgent agent = new AccountAgent();

            Login();

            var accountViewModel = agent.GetAccountViewModel();

            accountViewModel.UserName = "testuser1";
            accountViewModel.EmailAddress = "testznode@znode.com";
            accountViewModel.EmailOptIn = false;

            accountViewModel = agent.UpdateProfile(accountViewModel);

            Assert.IsNotNull(accountViewModel);
            Assert.AreEqual("testznode@znode.com", accountViewModel.EmailAddress);
        }

        [Test]
        public void Test25_ChangePassword()
        {
            IAccountAgent agent = new AccountAgent();

            var registerViewModel = new RegisterViewModel();
            registerViewModel.EmailAddress = "test2@znode.com";
            registerViewModel.UserName = "testuser" + DateTime.Now.Ticks.ToString();
            registerViewModel.Password = "password1";
            registerViewModel.ReTypePassword = "password1";
            registerViewModel.EmailOptIn = true;

            registerViewModel = agent.SignUp(registerViewModel);

            var loginViewModel = new LoginViewModel
            {
                Username = registerViewModel.UserName,
                Password = "password1"
            };

            agent.Login(loginViewModel);

            var changePasswordViewModel = new ChangePasswordViewModel
            {
                OldPassword = "password1",
                NewPassword = "password2",
                ReTypeNewPassword = "password2",
                UserName = registerViewModel.UserName
            };

            changePasswordViewModel = agent.ChangePassword(changePasswordViewModel);

            Assert.IsNotNull(changePasswordViewModel);
            Assert.AreEqual("Password changed successfully", changePasswordViewModel.SuccessMessage);
        }

        [Test]
        public void Test26_ForgotPassword()
        {
            IAccountAgent agent = new AccountAgent();

            var registerViewModel = new RegisterViewModel();
            registerViewModel.EmailAddress = "test3@znode.com";
            registerViewModel.UserName = "testuser" + DateTime.Now.Ticks.ToString();
            registerViewModel.Password = "password1";
            registerViewModel.ReTypePassword = "password1";
            registerViewModel.EmailOptIn = true;

            registerViewModel = agent.SignUp(registerViewModel);

            var loginViewModel = new LoginViewModel
            {
                Username = registerViewModel.UserName,
                Password = "password1"
            };

            agent.Login(loginViewModel);
            
            var accountViewModel = new AccountViewModel()
            {
                UserName = registerViewModel.UserName,
                EmailAddress = "test3@znode.com",
                PasswordQuestion = "What is your mother's maiden name?",
                PasswordAnswer = "NA",
            };

            accountViewModel = agent.ForgotPassword(accountViewModel);

            Assert.IsNotNull(accountViewModel);
            Assert.AreEqual("Your password has been sent to your email address.", accountViewModel.SuccessMessage);
        }

        [Test]
        public void Test27_GetGiftCardBalanceTest()
        {
            IAccountAgent agent = new AccountAgent();

            var giftCardViewModel = agent.GetGiftCardBalance("DVU4VXOZ9R");

            Assert.IsNotNull(giftCardViewModel);
            Assert.AreEqual("Gift card found", giftCardViewModel.SuccessMessage);
        }
        
        /// <summary>
        /// Private Method for Login
        /// </summary>
        private void Login()
        {
            IAccountAgent agent = new AccountAgent();

            var loginViewModel = new LoginViewModel
            {
                Username = "testuser1",
                Password = "password1"
            };

            agent.Login(loginViewModel);
        }

        private void RegisterUser()
        {
            IAccountAgent agent = new AccountAgent();

            var registerViewModel = new RegisterViewModel
                {
                    EmailAddress = "test1@znode.com",
                    UserName = "testuser" + DateTime.Now.Ticks.ToString(),
                    Password = "password1",
                    ReTypePassword = "password1",
                    EmailOptIn = true
                };

            registerViewModel = agent.SignUp(registerViewModel);
        }
    }
}
