﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Znode.Engine.MvcDemo.Agents;

namespace Znode.Engine.MvcDemo.Tests.Agents
{

    [TestFixture]
    public class CategoryAgentTestFixture : BaseFixture
    {
        [Test]
        public void GetCategoryTest()
        {
            ICategoryAgent agent = new CategoryAgent();
            var viewModel = agent.GetCategory(81);

            Assert.IsNotNull(viewModel);
            
            Assert.AreEqual(81,viewModel.CategoryId);
        }

        [Test]
        public void GetCategoryByNameTest()
        {
            ICategoryAgent agent = new CategoryAgent();
            var viewModel = agent.GetCategory(81);

            Assert.IsNotNull(viewModel);

            Assert.AreEqual("Fruit", viewModel.Name);
        }
    }
}
