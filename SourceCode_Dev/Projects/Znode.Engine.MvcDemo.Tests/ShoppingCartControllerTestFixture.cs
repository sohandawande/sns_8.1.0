﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using NUnit.Framework;
using Znode.Engine.MvcDemo.Controllers;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Tests
{
     [TestFixture]
    public class ShoppingCartControllerTestFixture 
    {
         [Test]
         public void AddItemToShoppingCartTest()
         {
             var controller = new ProductController();
             var cartItem = new CartItemViewModel();
             cartItem.ProductId = 302;
             cartItem.SkuId = 1;
             cartItem.Quantity = 5;
             cartItem.Sku = "apr234";
             cartItem.UnitPrice = 4.23M;
             var result = controller.AddToCart(cartItem);
             Assert.IsNotNull(result);
         }

         [Test]
         public void RemoveCartItemTest()
         {
             var controller = new ProductController();
             var cartItem = new CartItemViewModel();
             cartItem.ProductId = 302;
             cartItem.SkuId = 1;
             cartItem.Quantity = 5;
             cartItem.Sku = "apr234";
             cartItem.UnitPrice = 4.23M;
             var result = controller.AddToCart(cartItem) as ViewResult;
             if (result != null)
             {
                 var shoppingcart = (CartViewModel)result.ViewData.Model;

                 if (shoppingcart != null && shoppingcart.Items.Any())
                 {
                     var item = shoppingcart.Items.First();
                     var cartController = new CartController();

                     var cartResult = cartController.RemoveItem(item.ExternalId);
                     Assert.IsNotNull(cartResult);
                 }
             }
             Assert.IsNotNull(result);
         }

         [Test]
         public void CreateAccountWhileShoppingTest()
         {
             var accountcontroller = new AccountController();
             var registerModel = new RegisterViewModel
             {
                 EmailAddress = "user45@gmail.com",
                 EmailOptIn = true,
                 UserName = "user48",
                 Password = "password1",
                 ReTypePassword = "password1",
             };
             accountcontroller.Signup(registerModel, null);
             var controller = new ProductController();
             var cartItem = new CartItemViewModel();
             cartItem.ProductId = 302;
             cartItem.SkuId = 1;
             cartItem.Quantity = 5;
             cartItem.Sku = "apr234";
             cartItem.UnitPrice = 4.23M;
             var result = controller.AddToCart(cartItem) as ViewResult;
             if (result != null)
             {
                 var shoppingcart = (CartViewModel)result.ViewData.Model;

                 if (shoppingcart != null && shoppingcart.Items.Any())
                 {
                     var item = shoppingcart.Items.First();
                     var cartController = new CartController();

                     var cartResult = cartController.RemoveItem(item.ExternalId);
                     Assert.IsNotNull(cartResult);
                 }
             }
             Assert.IsNotNull(result);
         }

        [Test]
         public void AddToCartTest()
         {
             var controller = new ProductController();
             var cartItem = new CartItemViewModel();
             cartItem.ProductId = 302;
             cartItem.SkuId = 1;
             cartItem.Quantity = 5;
             cartItem.Sku = "apr234";
             cartItem.UnitPrice = 4.23M;
             var result = controller.AddToCart(cartItem);
             Assert.IsNotNull(result);
         }

        [Test]
        public void AddToCartMultipleProductsTest()
        {
            var controller = new ProductController();
            var cartItem = new CartItemViewModel();
            cartItem.ProductId = 302;
            cartItem.SkuId = 1;
            cartItem.Quantity = 1;
            cartItem.Sku = "apr234";
            cartItem.UnitPrice = 4.23M;
            var result = controller.AddToCart(cartItem);
            Assert.IsNotNull(result);

            cartItem = new CartItemViewModel
                {
                    ProductId = 302,
                    SkuId = 3,
                    Quantity = 1,
                    Sku = "apy234",
                    UnitPrice = 2.99M
                };
            result = controller.AddToCart(cartItem);

            Assert.IsNotNull(result);
        }

        [Test]
        public void AddToCartProductChangeQuantity()
        {
            var controller = new ProductController();
            
            var cartItem = new CartItemViewModel
                {
                    ProductId = 302,
                    SkuId = 1,
                    Quantity = 1,
                    Sku = "apr234",
                    UnitPrice = 4.23M
                };
            var result = controller.AddToCart(cartItem) as ViewResult;
            if (result != null)
            {
                var shoppingcart = (CartViewModel) result.ViewData.Model;
        
                if (shoppingcart != null && shoppingcart.Items.Any())
                {
                    var item = shoppingcart.Items.First();
                    var cartController = new CartController();
                   
                    var cartResult = cartController.UpdateQuantity(item.ExternalId, 3) ;
                    Assert.IsNotNull(cartResult);
                }
            }
            Assert.IsNotNull(result);
        }

        [Test]
        public void AddToCartProductOutofStockQuantityTest()
        {
            var controller = new ProductController();
            var cartItem = new CartItemViewModel
            {
                ProductId = 302,
                SkuId = 1,
                Quantity = 1,
                Sku = "apr234",
                UnitPrice = 4.23M
            };
            var result = controller.AddToCart(cartItem) as ViewResult;
            if (result != null)
            {
                var shoppingcart = (CartViewModel)result.ViewData.Model;

                if (shoppingcart != null && shoppingcart.Items.Any())
                {
                    var item = shoppingcart.Items.First();
                    var cartController = new CartController();
                    
                    var cartResult = cartController.UpdateQuantity(item.ExternalId, 3) as ViewResult;
                    if (cartResult != null)
                    {
                        shoppingcart = (CartViewModel)cartResult.ViewData.Model;
                        Assert.IsTrue(shoppingcart.Items.Any(x=>x.InsufficientQuantity));
                    }
                }
            }
        }
    }
}
