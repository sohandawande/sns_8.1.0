﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using NUnit.Framework;

namespace Znode.Engine.MvcDemo.Tests
{
    [TestFixture]
    public abstract class BaseFixture
    {

        [SetUp]
        public void SetUp()
        {
            HttpContext.Current = HttpContextFactory.GetConext();
        }
    }
}
