﻿using System;
using System.Diagnostics;
using System.Web.Mvc;
using NUnit.Framework;
using Znode.Engine.MvcDemo.Agents;
using Znode.Engine.MvcDemo.Controllers;
using Znode.Engine.MvcDemo.ViewModels;


namespace Znode.Engine.MvcDemo.Tests
{
    [TestFixture]
    public class ProductControllerTestFixture : BaseFixture
    {


        [Test]
        public void DetailsTest()
        {
            var controller = new ProductController();
            var result = controller.Details(302, string.Empty,false);
            Assert.IsNotNull(result);
            var viewResult = result as ViewResult;
            Assert.IsInstanceOf(typeof (ProductViewModel), viewResult.Model);

        }

        [Test]
        public void AttributeTest()
        {

            var controller = new ProductController();
            var result = controller.GetAttributes(302, 4, "4", 2, string.Empty);
            Assert.IsNotNull(result);

        }

        [Test]
        public void ChangeAttributePriceTest()
        {
            var controller = new ProductController();
            var result = controller.Details(302, string.Empty, false);
            Assert.IsNotNull(result);
        }

        [Test]
        public void ChangeQuantityTest()
        {
            var  controller  = new ProductController();
            var result = controller.Details(302, string.Empty, false);
            Assert.IsNotNull(result);
        }

        [Test]
        public void OutOfStockQuantityTest()
        {
            var controller = new ProductController();
            var result = controller.Details(302, string.Empty, false);
            Assert.IsNotNull(result);
        }

        [Test]
        public void AddToCartEnabledTest()
        {
            var controller = new ProductController();
            var cartItem = new CartItemViewModel
            {
                ProductId = 302,
                SkuId = 1,
                Quantity = 1,
                Sku = "apr234",
                UnitPrice = 4.23M
            };
            var result = controller.AddToCart(cartItem);
            Assert.IsNotNull(result);
        }

        [Test]
        public void AddToCartDisabledTest()
        {
            var controller = new ProductController();
            var cartItem = new CartItemViewModel
            {
                ProductId = 302,
                SkuId = 1,
                Quantity = 1,
                Sku = "apr234",
                UnitPrice = 4.23M
            };
            var result = controller.AddToCart(cartItem);
            Assert.IsNotNull(result);

        }

        [Test]
        public void AddToWishlistTest()
        {
            
            var controller = new AccountController();
            
            var productcontroller = new ProductController();
            var model = new LoginViewModel();
            model.Username = "testuser01";
            model.Password = "password1";
            var result = controller.Login(model, string.Empty);
            var wishlistresult = productcontroller.WishList(405);
            Assert.IsNotNull(wishlistresult);
        }

        [Test]
        public void FrequentyBoughtTogetherTest()
        {
            var controller = new ProductController();
            var result = controller.Details(318, string.Empty, false);
            Assert.IsNotNull(result);
        }

        [Test]
        public void YouMayAlsoLikeTest()
        {
            var controller = new ProductController();
            var result = controller.Details(318, string.Empty, false);
            Assert.IsNotNull(result);
        }


        [Test]
        public void AsycController ()
        {
            int number = Process.GetCurrentProcess().Threads.Count;
            var controller = new ProductAsyncController();
            var result = controller.GetProductsByIds("318",0);
            int lastnumber = Process.GetCurrentProcess().Threads.Count;
            Assert.IsNotNull(result);
        }

        [Test]
        public void NotAsyncController()
        {
            int number = Process.GetCurrentProcess().Threads.Count;
            var controller = new ProductController();
            var result = controller.GetProductsByIds("318");
            int lastnumber = Process.GetCurrentProcess().Threads.Count;
            Assert.IsNotNull(result);
        }
    }
}
