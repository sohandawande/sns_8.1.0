﻿using NUnit.Framework;
using System.Web.Mvc;
using Znode.Engine.MvcDemo.Controllers;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Tests
{
    [TestFixture]
    public class HomeControllerTestFixture
    {
        [Test]
        public void HomePageTest()
        {
            var controller = new HomeController();
            var result = controller.Index();
            Assert.IsNotNull(result);
        }

        [Test]
        public void ProductDetailsTest()
        {
            var controller = new ProductController();
            var result = controller.Details(302, string.Empty,false) as ViewResult;
            if (result != null) Assert.AreEqual("Details", result.ViewName);
        }

        [Test]
        public void CategoryLandingTest()
        {
            var controller = new CategoryController();
            var requestModel = new SearchRequestModel();
            requestModel.SearchTerm = string.Empty;
            requestModel.Category = "Fruit";
            requestModel.PageNumber = 1;
            requestModel.PageSize = 5;
            requestModel.Sort = "1";
            var result = controller.Index(requestModel,"","");
            Assert.IsNotNull(result);
        }

        [Test]
        public void KeywordSearchTest()
        {
            var controller = new CategoryController();
            var requestModel = new SearchRequestModel();
            requestModel.Category = string.Empty;
            requestModel.SearchTerm = "Apple";

            var result = controller.Index(requestModel, string.Empty, "");
            Assert.IsNotNull(result);
        }


        [Test]
        public void AccountLoginTest()
        {
            var controller = new AccountController();
            LoginViewModel viewModel = new LoginViewModel();
            viewModel.Password = "admin12345";
            viewModel.Username = "admin12345";
            var result = controller.Login(viewModel, string.Empty);
            Assert.IsNotNull(result);
        }

        [Test]
        public void RegisterAccountTest()
        {
            var controller = new AccountController();
            var result = controller.Login(string.Empty);
            Assert.IsNotNull(result);
        }

    }
}
