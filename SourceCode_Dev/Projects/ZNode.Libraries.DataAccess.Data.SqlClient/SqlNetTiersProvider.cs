﻿#region Using directives

using System;
using System.Collections;
using System.Collections.Specialized;


using System.Web.Configuration;
using System.Data;
using System.Data.Common;
using System.Configuration.Provider;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;

#endregion

namespace ZNode.Libraries.DataAccess.Data.SqlClient
{
	/// <summary>
	/// This class is the Sql implementation of the NetTiersProvider.
	/// </summary>
	public sealed class SqlNetTiersProvider : ZNode.Libraries.DataAccess.Data.Bases.NetTiersProvider
	{
		private static object syncRoot = new Object();
		private string _applicationName;
        private string _connectionString;
        private bool _useStoredProcedure;
        string _providerInvariantName;
		
		/// <summary>
		/// Initializes a new instance of the <see cref="SqlNetTiersProvider"/> class.
		///</summary>
		public SqlNetTiersProvider()
		{	
		}		
		
		/// <summary>
        /// Initializes the provider.
        /// </summary>
        /// <param name="name">The friendly name of the provider.</param>
        /// <param name="config">A collection of the name/value pairs representing the provider-specific attributes specified in the configuration for this provider.</param>
        /// <exception cref="T:System.ArgumentNullException">The name of the provider is null.</exception>
        /// <exception cref="T:System.InvalidOperationException">An attempt is made to call <see cref="M:System.Configuration.Provider.ProviderBase.Initialize(System.String,System.Collections.Specialized.NameValueCollection)"></see> on a provider after the provider has already been initialized.</exception>
        /// <exception cref="T:System.ArgumentException">The name of the provider has a length of zero.</exception>
		public override void Initialize(string name, NameValueCollection config)
        {
            // Verify that config isn't null
            if (config == null)
            {
                throw new ArgumentNullException("config");
            }

            // Assign the provider a default name if it doesn't have one
            if (String.IsNullOrEmpty(name))
            {
                name = "SqlNetTiersProvider";
            }

            // Add a default "description" attribute to config if the
            // attribute doesn't exist or is empty
            if (string.IsNullOrEmpty(config["description"]))
            {
                config.Remove("description");
                config.Add("description", "NetTiers Sql provider");
            }

            // Call the base class's Initialize method
            base.Initialize(name, config);

            // Initialize _applicationName
            _applicationName = config["applicationName"];

            if (string.IsNullOrEmpty(_applicationName))
            {
                _applicationName = "/";
            }
            config.Remove("applicationName");


            #region "Initialize UseStoredProcedure"
            string storedProcedure  = config["useStoredProcedure"];
           	if (string.IsNullOrEmpty(storedProcedure))
            {
                throw new ProviderException("Empty or missing useStoredProcedure");
            }
            this._useStoredProcedure = Convert.ToBoolean(config["useStoredProcedure"]);
            config.Remove("useStoredProcedure");
            #endregion

			#region ConnectionString

			// Initialize _connectionString
			_connectionString = config["connectionString"];
			config.Remove("connectionString");

			string connect = config["connectionStringName"];
			config.Remove("connectionStringName");

			if ( String.IsNullOrEmpty(_connectionString) )
			{
				if ( String.IsNullOrEmpty(connect) )
				{
					throw new ProviderException("Empty or missing connectionStringName");
				}

				if ( DataRepository.ConnectionStrings[connect] == null )
				{
					throw new ProviderException("Missing connection string");
				}

				_connectionString = DataRepository.ConnectionStrings[connect].ConnectionString;
			}

            if ( String.IsNullOrEmpty(_connectionString) )
            {
                throw new ProviderException("Empty connection string");
			}

			#endregion
            
             #region "_providerInvariantName"

            // initialize _providerInvariantName
            this._providerInvariantName = config["providerInvariantName"];

            if (String.IsNullOrEmpty(_providerInvariantName))
            {
                throw new ProviderException("Empty or missing providerInvariantName");
            }
            config.Remove("providerInvariantName");

            #endregion

        }
		
		/// <summary>
		/// Creates a new <see cref="TransactionManager"/> instance from the current datasource.
		/// </summary>
		/// <returns></returns>
		public override TransactionManager CreateTransaction()
		{
			return new TransactionManager(this._connectionString);
		}
		
		/// <summary>
		/// Gets a value indicating whether to use stored procedure or not.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this repository use stored procedures; otherwise, <c>false</c>.
		/// </value>
		public bool UseStoredProcedure
		{
			get {return this._useStoredProcedure;}
			set {this._useStoredProcedure = value;}
		}
		
		 /// <summary>
        /// Gets or sets the connection string.
        /// </summary>
        /// <value>The connection string.</value>
		public string ConnectionString
		{
			get {return this._connectionString;}
			set {this._connectionString = value;}
		}
		
		/// <summary>
	    /// Gets or sets the invariant provider name listed in the DbProviderFactories machine.config section.
	    /// </summary>
	    /// <value>The name of the provider invariant.</value>
	    public string ProviderInvariantName
	    {
	        get { return this._providerInvariantName; }
	        set { this._providerInvariantName = value; }
	    }		
		
		///<summary>
		/// Indicates if the current <see cref="NetTiersProvider"/> implementation supports Transacton.
		///</summary>
		public override bool IsTransactionSupported
		{
			get
			{
				return true;
			}
		}

		
		#region "SKUProfileEffectiveProvider"
			
		private SqlSKUProfileEffectiveProvider innerSqlSKUProfileEffectiveProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="SKUProfileEffective"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override SKUProfileEffectiveProviderBase SKUProfileEffectiveProvider
		{
			get
			{
				if (innerSqlSKUProfileEffectiveProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlSKUProfileEffectiveProvider == null)
						{
							this.innerSqlSKUProfileEffectiveProvider = new SqlSKUProfileEffectiveProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlSKUProfileEffectiveProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlSKUProfileEffectiveProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlSKUProfileEffectiveProvider SqlSKUProfileEffectiveProvider
		{
			get {return SKUProfileEffectiveProvider as SqlSKUProfileEffectiveProvider;}
		}
		
		#endregion
		
		
		#region "TagsProvider"
			
		private SqlTagsProvider innerSqlTagsProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="Tags"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override TagsProviderBase TagsProvider
		{
			get
			{
				if (innerSqlTagsProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlTagsProvider == null)
						{
							this.innerSqlTagsProvider = new SqlTagsProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlTagsProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlTagsProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlTagsProvider SqlTagsProvider
		{
			get {return TagsProvider as SqlTagsProvider;}
		}
		
		#endregion
		
		
		#region "ProductTypeAttributeProvider"
			
		private SqlProductTypeAttributeProvider innerSqlProductTypeAttributeProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ProductTypeAttribute"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ProductTypeAttributeProviderBase ProductTypeAttributeProvider
		{
			get
			{
				if (innerSqlProductTypeAttributeProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlProductTypeAttributeProvider == null)
						{
							this.innerSqlProductTypeAttributeProvider = new SqlProductTypeAttributeProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlProductTypeAttributeProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlProductTypeAttributeProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlProductTypeAttributeProvider SqlProductTypeAttributeProvider
		{
			get {return ProductTypeAttributeProvider as SqlProductTypeAttributeProvider;}
		}
		
		#endregion
		
		
		#region "ProductImageProvider"
			
		private SqlProductImageProvider innerSqlProductImageProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ProductImage"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ProductImageProviderBase ProductImageProvider
		{
			get
			{
				if (innerSqlProductImageProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlProductImageProvider == null)
						{
							this.innerSqlProductImageProvider = new SqlProductImageProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlProductImageProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlProductImageProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlProductImageProvider SqlProductImageProvider
		{
			get {return ProductImageProvider as SqlProductImageProvider;}
		}
		
		#endregion
		
		
		#region "AddOnProvider"
			
		private SqlAddOnProvider innerSqlAddOnProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="AddOn"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override AddOnProviderBase AddOnProvider
		{
			get
			{
				if (innerSqlAddOnProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlAddOnProvider == null)
						{
							this.innerSqlAddOnProvider = new SqlAddOnProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlAddOnProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlAddOnProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlAddOnProvider SqlAddOnProvider
		{
			get {return AddOnProvider as SqlAddOnProvider;}
		}
		
		#endregion
		
		
		#region "CSSProvider"
			
		private SqlCSSProvider innerSqlCSSProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="CSS"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override CSSProviderBase CSSProvider
		{
			get
			{
				if (innerSqlCSSProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlCSSProvider == null)
						{
							this.innerSqlCSSProvider = new SqlCSSProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlCSSProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlCSSProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlCSSProvider SqlCSSProvider
		{
			get {return CSSProvider as SqlCSSProvider;}
		}
		
		#endregion
		
		
		#region "PriceListProvider"
			
		private SqlPriceListProvider innerSqlPriceListProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="PriceList"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override PriceListProviderBase PriceListProvider
		{
			get
			{
				if (innerSqlPriceListProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlPriceListProvider == null)
						{
							this.innerSqlPriceListProvider = new SqlPriceListProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlPriceListProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlPriceListProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlPriceListProvider SqlPriceListProvider
		{
			get {return PriceListProvider as SqlPriceListProvider;}
		}
		
		#endregion
		
		
		#region "ProductHighlightProvider"
			
		private SqlProductHighlightProvider innerSqlProductHighlightProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ProductHighlight"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ProductHighlightProviderBase ProductHighlightProvider
		{
			get
			{
				if (innerSqlProductHighlightProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlProductHighlightProvider == null)
						{
							this.innerSqlProductHighlightProvider = new SqlProductHighlightProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlProductHighlightProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlProductHighlightProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlProductHighlightProvider SqlProductHighlightProvider
		{
			get {return ProductHighlightProvider as SqlProductHighlightProvider;}
		}
		
		#endregion
		
		
		#region "CategoryProvider"
			
		private SqlCategoryProvider innerSqlCategoryProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="Category"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override CategoryProviderBase CategoryProvider
		{
			get
			{
				if (innerSqlCategoryProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlCategoryProvider == null)
						{
							this.innerSqlCategoryProvider = new SqlCategoryProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlCategoryProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlCategoryProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlCategoryProvider SqlCategoryProvider
		{
			get {return CategoryProvider as SqlCategoryProvider;}
		}
		
		#endregion
		
		
		#region "CaseTypeProvider"
			
		private SqlCaseTypeProvider innerSqlCaseTypeProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="CaseType"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override CaseTypeProviderBase CaseTypeProvider
		{
			get
			{
				if (innerSqlCaseTypeProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlCaseTypeProvider == null)
						{
							this.innerSqlCaseTypeProvider = new SqlCaseTypeProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlCaseTypeProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlCaseTypeProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlCaseTypeProvider SqlCaseTypeProvider
		{
			get {return CaseTypeProvider as SqlCaseTypeProvider;}
		}
		
		#endregion
		
		
		#region "ResetPasswordProvider"
			
		private SqlResetPasswordProvider innerSqlResetPasswordProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ResetPassword"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ResetPasswordProviderBase ResetPasswordProvider
		{
			get
			{
				if (innerSqlResetPasswordProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlResetPasswordProvider == null)
						{
							this.innerSqlResetPasswordProvider = new SqlResetPasswordProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlResetPasswordProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlResetPasswordProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlResetPasswordProvider SqlResetPasswordProvider
		{
			get {return ResetPasswordProvider as SqlResetPasswordProvider;}
		}
		
		#endregion
		
		
		#region "CategoryProfileProvider"
			
		private SqlCategoryProfileProvider innerSqlCategoryProfileProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="CategoryProfile"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override CategoryProfileProviderBase CategoryProfileProvider
		{
			get
			{
				if (innerSqlCategoryProfileProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlCategoryProfileProvider == null)
						{
							this.innerSqlCategoryProfileProvider = new SqlCategoryProfileProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlCategoryProfileProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlCategoryProfileProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlCategoryProfileProvider SqlCategoryProfileProvider
		{
			get {return CategoryProfileProvider as SqlCategoryProfileProvider;}
		}
		
		#endregion
		
		
		#region "AccountProfileProvider"
			
		private SqlAccountProfileProvider innerSqlAccountProfileProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="AccountProfile"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override AccountProfileProviderBase AccountProfileProvider
		{
			get
			{
				if (innerSqlAccountProfileProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlAccountProfileProvider == null)
						{
							this.innerSqlAccountProfileProvider = new SqlAccountProfileProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlAccountProfileProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlAccountProfileProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlAccountProfileProvider SqlAccountProfileProvider
		{
			get {return AccountProfileProvider as SqlAccountProfileProvider;}
		}
		
		#endregion
		
		
		#region "ProductReviewHistoryProvider"
			
		private SqlProductReviewHistoryProvider innerSqlProductReviewHistoryProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ProductReviewHistory"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ProductReviewHistoryProviderBase ProductReviewHistoryProvider
		{
			get
			{
				if (innerSqlProductReviewHistoryProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlProductReviewHistoryProvider == null)
						{
							this.innerSqlProductReviewHistoryProvider = new SqlProductReviewHistoryProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlProductReviewHistoryProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlProductReviewHistoryProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlProductReviewHistoryProvider SqlProductReviewHistoryProvider
		{
			get {return ProductReviewHistoryProvider as SqlProductReviewHistoryProvider;}
		}
		
		#endregion
		
		
		#region "GatewayProvider"
			
		private SqlGatewayProvider innerSqlGatewayProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="Gateway"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override GatewayProviderBase GatewayProvider
		{
			get
			{
				if (innerSqlGatewayProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlGatewayProvider == null)
						{
							this.innerSqlGatewayProvider = new SqlGatewayProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlGatewayProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlGatewayProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlGatewayProvider SqlGatewayProvider
		{
			get {return GatewayProvider as SqlGatewayProvider;}
		}
		
		#endregion
		
		
		#region "SavedCartLineItemProvider"
			
		private SqlSavedCartLineItemProvider innerSqlSavedCartLineItemProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="SavedCartLineItem"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override SavedCartLineItemProviderBase SavedCartLineItemProvider
		{
			get
			{
				if (innerSqlSavedCartLineItemProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlSavedCartLineItemProvider == null)
						{
							this.innerSqlSavedCartLineItemProvider = new SqlSavedCartLineItemProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlSavedCartLineItemProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlSavedCartLineItemProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlSavedCartLineItemProvider SqlSavedCartLineItemProvider
		{
			get {return SavedCartLineItemProvider as SqlSavedCartLineItemProvider;}
		}
		
		#endregion
		
		
		#region "SKUInventoryProvider"
			
		private SqlSKUInventoryProvider innerSqlSKUInventoryProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="SKUInventory"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override SKUInventoryProviderBase SKUInventoryProvider
		{
			get
			{
				if (innerSqlSKUInventoryProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlSKUInventoryProvider == null)
						{
							this.innerSqlSKUInventoryProvider = new SqlSKUInventoryProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlSKUInventoryProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlSKUInventoryProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlSKUInventoryProvider SqlSKUInventoryProvider
		{
			get {return SKUInventoryProvider as SqlSKUInventoryProvider;}
		}
		
		#endregion
		
		
		#region "WishListProvider"
			
		private SqlWishListProvider innerSqlWishListProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="WishList"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override WishListProviderBase WishListProvider
		{
			get
			{
				if (innerSqlWishListProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlWishListProvider == null)
						{
							this.innerSqlWishListProvider = new SqlWishListProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlWishListProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlWishListProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlWishListProvider SqlWishListProvider
		{
			get {return WishListProvider as SqlWishListProvider;}
		}
		
		#endregion
		
		
		#region "ProductCategoryProvider"
			
		private SqlProductCategoryProvider innerSqlProductCategoryProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ProductCategory"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ProductCategoryProviderBase ProductCategoryProvider
		{
			get
			{
				if (innerSqlProductCategoryProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlProductCategoryProvider == null)
						{
							this.innerSqlProductCategoryProvider = new SqlProductCategoryProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlProductCategoryProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlProductCategoryProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlProductCategoryProvider SqlProductCategoryProvider
		{
			get {return ProductCategoryProvider as SqlProductCategoryProvider;}
		}
		
		#endregion
		
		
		#region "ContentPageProvider"
			
		private SqlContentPageProvider innerSqlContentPageProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ContentPage"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ContentPageProviderBase ContentPageProvider
		{
			get
			{
				if (innerSqlContentPageProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlContentPageProvider == null)
						{
							this.innerSqlContentPageProvider = new SqlContentPageProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlContentPageProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlContentPageProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlContentPageProvider SqlContentPageProvider
		{
			get {return ContentPageProvider as SqlContentPageProvider;}
		}
		
		#endregion
		
		
		#region "ProductAttributeProvider"
			
		private SqlProductAttributeProvider innerSqlProductAttributeProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ProductAttribute"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ProductAttributeProviderBase ProductAttributeProvider
		{
			get
			{
				if (innerSqlProductAttributeProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlProductAttributeProvider == null)
						{
							this.innerSqlProductAttributeProvider = new SqlProductAttributeProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlProductAttributeProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlProductAttributeProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlProductAttributeProvider SqlProductAttributeProvider
		{
			get {return ProductAttributeProvider as SqlProductAttributeProvider;}
		}
		
		#endregion
		
		
		#region "PortalCatalogProvider"
			
		private SqlPortalCatalogProvider innerSqlPortalCatalogProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="PortalCatalog"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override PortalCatalogProviderBase PortalCatalogProvider
		{
			get
			{
				if (innerSqlPortalCatalogProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlPortalCatalogProvider == null)
						{
							this.innerSqlPortalCatalogProvider = new SqlPortalCatalogProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlPortalCatalogProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlPortalCatalogProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlPortalCatalogProvider SqlPortalCatalogProvider
		{
			get {return PortalCatalogProvider as SqlPortalCatalogProvider;}
		}
		
		#endregion
		
		
		#region "ContentPageRevisionProvider"
			
		private SqlContentPageRevisionProvider innerSqlContentPageRevisionProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ContentPageRevision"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ContentPageRevisionProviderBase ContentPageRevisionProvider
		{
			get
			{
				if (innerSqlContentPageRevisionProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlContentPageRevisionProvider == null)
						{
							this.innerSqlContentPageRevisionProvider = new SqlContentPageRevisionProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlContentPageRevisionProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlContentPageRevisionProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlContentPageRevisionProvider SqlContentPageRevisionProvider
		{
			get {return ContentPageRevisionProvider as SqlContentPageRevisionProvider;}
		}
		
		#endregion
		
		
		#region "SKUAttributeProvider"
			
		private SqlSKUAttributeProvider innerSqlSKUAttributeProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="SKUAttribute"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override SKUAttributeProviderBase SKUAttributeProvider
		{
			get
			{
				if (innerSqlSKUAttributeProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlSKUAttributeProvider == null)
						{
							this.innerSqlSKUAttributeProvider = new SqlSKUAttributeProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlSKUAttributeProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlSKUAttributeProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlSKUAttributeProvider SqlSKUAttributeProvider
		{
			get {return SKUAttributeProvider as SqlSKUAttributeProvider;}
		}
		
		#endregion
		
		
		#region "ShippingProvider"
			
		private SqlShippingProvider innerSqlShippingProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="Shipping"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ShippingProviderBase ShippingProvider
		{
			get
			{
				if (innerSqlShippingProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlShippingProvider == null)
						{
							this.innerSqlShippingProvider = new SqlShippingProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlShippingProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlShippingProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlShippingProvider SqlShippingProvider
		{
			get {return ShippingProvider as SqlShippingProvider;}
		}
		
		#endregion
		
		
		#region "ProductAddOnProvider"
			
		private SqlProductAddOnProvider innerSqlProductAddOnProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ProductAddOn"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ProductAddOnProviderBase ProductAddOnProvider
		{
			get
			{
				if (innerSqlProductAddOnProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlProductAddOnProvider == null)
						{
							this.innerSqlProductAddOnProvider = new SqlProductAddOnProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlProductAddOnProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlProductAddOnProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlProductAddOnProvider SqlProductAddOnProvider
		{
			get {return ProductAddOnProvider as SqlProductAddOnProvider;}
		}
		
		#endregion
		
		
		#region "HighlightProvider"
			
		private SqlHighlightProvider innerSqlHighlightProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="Highlight"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override HighlightProviderBase HighlightProvider
		{
			get
			{
				if (innerSqlHighlightProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlHighlightProvider == null)
						{
							this.innerSqlHighlightProvider = new SqlHighlightProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlHighlightProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlHighlightProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlHighlightProvider SqlHighlightProvider
		{
			get {return HighlightProvider as SqlHighlightProvider;}
		}
		
		#endregion
		
		
		#region "CategoryNodeProvider"
			
		private SqlCategoryNodeProvider innerSqlCategoryNodeProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="CategoryNode"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override CategoryNodeProviderBase CategoryNodeProvider
		{
			get
			{
				if (innerSqlCategoryNodeProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlCategoryNodeProvider == null)
						{
							this.innerSqlCategoryNodeProvider = new SqlCategoryNodeProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlCategoryNodeProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlCategoryNodeProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlCategoryNodeProvider SqlCategoryNodeProvider
		{
			get {return CategoryNodeProvider as SqlCategoryNodeProvider;}
		}
		
		#endregion
		
		
		#region "AddressProvider"
			
		private SqlAddressProvider innerSqlAddressProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="Address"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override AddressProviderBase AddressProvider
		{
			get
			{
				if (innerSqlAddressProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlAddressProvider == null)
						{
							this.innerSqlAddressProvider = new SqlAddressProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlAddressProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlAddressProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlAddressProvider SqlAddressProvider
		{
			get {return AddressProvider as SqlAddressProvider;}
		}
		
		#endregion
		
		
		#region "ActivityLogProvider"
			
		private SqlActivityLogProvider innerSqlActivityLogProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ActivityLog"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ActivityLogProviderBase ActivityLogProvider
		{
			get
			{
				if (innerSqlActivityLogProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlActivityLogProvider == null)
						{
							this.innerSqlActivityLogProvider = new SqlActivityLogProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlActivityLogProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlActivityLogProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlActivityLogProvider SqlActivityLogProvider
		{
			get {return ActivityLogProvider as SqlActivityLogProvider;}
		}
		
		#endregion
		
		
		#region "FacetProvider"
			
		private SqlFacetProvider innerSqlFacetProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="Facet"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override FacetProviderBase FacetProvider
		{
			get
			{
				if (innerSqlFacetProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlFacetProvider == null)
						{
							this.innerSqlFacetProvider = new SqlFacetProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlFacetProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlFacetProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlFacetProvider SqlFacetProvider
		{
			get {return FacetProvider as SqlFacetProvider;}
		}
		
		#endregion
		
		
		#region "HighlightTypeProvider"
			
		private SqlHighlightTypeProvider innerSqlHighlightTypeProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="HighlightType"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override HighlightTypeProviderBase HighlightTypeProvider
		{
			get
			{
				if (innerSqlHighlightTypeProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlHighlightTypeProvider == null)
						{
							this.innerSqlHighlightTypeProvider = new SqlHighlightTypeProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlHighlightTypeProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlHighlightTypeProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlHighlightTypeProvider SqlHighlightTypeProvider
		{
			get {return HighlightTypeProvider as SqlHighlightTypeProvider;}
		}
		
		#endregion
		
		
		#region "ProductProfileProvider"
			
		private SqlProductProfileProvider innerSqlProductProfileProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ProductProfile"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ProductProfileProviderBase ProductProfileProvider
		{
			get
			{
				if (innerSqlProductProfileProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlProductProfileProvider == null)
						{
							this.innerSqlProductProfileProvider = new SqlProductProfileProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlProductProfileProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlProductProfileProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlProductProfileProvider SqlProductProfileProvider
		{
			get {return ProductProfileProvider as SqlProductProfileProvider;}
		}
		
		#endregion
		
		
		#region "CaseRequestProvider"
			
		private SqlCaseRequestProvider innerSqlCaseRequestProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="CaseRequest"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override CaseRequestProviderBase CaseRequestProvider
		{
			get
			{
				if (innerSqlCaseRequestProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlCaseRequestProvider == null)
						{
							this.innerSqlCaseRequestProvider = new SqlCaseRequestProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlCaseRequestProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlCaseRequestProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlCaseRequestProvider SqlCaseRequestProvider
		{
			get {return CaseRequestProvider as SqlCaseRequestProvider;}
		}
		
		#endregion
		
		
		#region "ReviewProvider"
			
		private SqlReviewProvider innerSqlReviewProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="Review"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ReviewProviderBase ReviewProvider
		{
			get
			{
				if (innerSqlReviewProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlReviewProvider == null)
						{
							this.innerSqlReviewProvider = new SqlReviewProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlReviewProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlReviewProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlReviewProvider SqlReviewProvider
		{
			get {return ReviewProvider as SqlReviewProvider;}
		}
		
		#endregion
		
		
		#region "TrackingOutboundProvider"
			
		private SqlTrackingOutboundProvider innerSqlTrackingOutboundProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="TrackingOutbound"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override TrackingOutboundProviderBase TrackingOutboundProvider
		{
			get
			{
				if (innerSqlTrackingOutboundProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlTrackingOutboundProvider == null)
						{
							this.innerSqlTrackingOutboundProvider = new SqlTrackingOutboundProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlTrackingOutboundProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlTrackingOutboundProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlTrackingOutboundProvider SqlTrackingOutboundProvider
		{
			get {return TrackingOutboundProvider as SqlTrackingOutboundProvider;}
		}
		
		#endregion
		
		
		#region "ActivityLogTypeProvider"
			
		private SqlActivityLogTypeProvider innerSqlActivityLogTypeProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ActivityLogType"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ActivityLogTypeProviderBase ActivityLogTypeProvider
		{
			get
			{
				if (innerSqlActivityLogTypeProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlActivityLogTypeProvider == null)
						{
							this.innerSqlActivityLogTypeProvider = new SqlActivityLogTypeProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlActivityLogTypeProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlActivityLogTypeProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlActivityLogTypeProvider SqlActivityLogTypeProvider
		{
			get {return ActivityLogTypeProvider as SqlActivityLogTypeProvider;}
		}
		
		#endregion
		
		
		#region "PortalProfileProvider"
			
		private SqlPortalProfileProvider innerSqlPortalProfileProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="PortalProfile"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override PortalProfileProviderBase PortalProfileProvider
		{
			get
			{
				if (innerSqlPortalProfileProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlPortalProfileProvider == null)
						{
							this.innerSqlPortalProfileProvider = new SqlPortalProfileProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlPortalProfileProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlPortalProfileProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlPortalProfileProvider SqlPortalProfileProvider
		{
			get {return PortalProfileProvider as SqlPortalProfileProvider;}
		}
		
		#endregion
		
		
		#region "FacetGroupProvider"
			
		private SqlFacetGroupProvider innerSqlFacetGroupProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="FacetGroup"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override FacetGroupProviderBase FacetGroupProvider
		{
			get
			{
				if (innerSqlFacetGroupProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlFacetGroupProvider == null)
						{
							this.innerSqlFacetGroupProvider = new SqlFacetGroupProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlFacetGroupProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlFacetGroupProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlFacetGroupProvider SqlFacetGroupProvider
		{
			get {return FacetGroupProvider as SqlFacetGroupProvider;}
		}
		
		#endregion
		
		
		#region "LuceneDocumentMappingProvider"
			
		private SqlLuceneDocumentMappingProvider innerSqlLuceneDocumentMappingProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="LuceneDocumentMapping"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override LuceneDocumentMappingProviderBase LuceneDocumentMappingProvider
		{
			get
			{
				if (innerSqlLuceneDocumentMappingProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlLuceneDocumentMappingProvider == null)
						{
							this.innerSqlLuceneDocumentMappingProvider = new SqlLuceneDocumentMappingProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlLuceneDocumentMappingProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlLuceneDocumentMappingProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlLuceneDocumentMappingProvider SqlLuceneDocumentMappingProvider
		{
			get {return LuceneDocumentMappingProvider as SqlLuceneDocumentMappingProvider;}
		}
		
		#endregion
		
		
		#region "FacetControlTypeProvider"
			
		private SqlFacetControlTypeProvider innerSqlFacetControlTypeProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="FacetControlType"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override FacetControlTypeProviderBase FacetControlTypeProvider
		{
			get
			{
				if (innerSqlFacetControlTypeProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlFacetControlTypeProvider == null)
						{
							this.innerSqlFacetControlTypeProvider = new SqlFacetControlTypeProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlFacetControlTypeProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlFacetControlTypeProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlFacetControlTypeProvider SqlFacetControlTypeProvider
		{
			get {return FacetControlTypeProvider as SqlFacetControlTypeProvider;}
		}
		
		#endregion
		
		
		#region "DomainProvider"
			
		private SqlDomainProvider innerSqlDomainProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="Domain"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override DomainProviderBase DomainProvider
		{
			get
			{
				if (innerSqlDomainProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlDomainProvider == null)
						{
							this.innerSqlDomainProvider = new SqlDomainProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlDomainProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlDomainProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlDomainProvider SqlDomainProvider
		{
			get {return DomainProvider as SqlDomainProvider;}
		}
		
		#endregion
		
		
		#region "FacetProductSKUProvider"
			
		private SqlFacetProductSKUProvider innerSqlFacetProductSKUProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="FacetProductSKU"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override FacetProductSKUProviderBase FacetProductSKUProvider
		{
			get
			{
				if (innerSqlFacetProductSKUProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlFacetProductSKUProvider == null)
						{
							this.innerSqlFacetProductSKUProvider = new SqlFacetProductSKUProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlFacetProductSKUProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlFacetProductSKUProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlFacetProductSKUProvider SqlFacetProductSKUProvider
		{
			get {return FacetProductSKUProvider as SqlFacetProductSKUProvider;}
		}
		
		#endregion
		
		
		#region "DiscountTypeProvider"
			
		private SqlDiscountTypeProvider innerSqlDiscountTypeProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="DiscountType"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override DiscountTypeProviderBase DiscountTypeProvider
		{
			get
			{
				if (innerSqlDiscountTypeProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlDiscountTypeProvider == null)
						{
							this.innerSqlDiscountTypeProvider = new SqlDiscountTypeProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlDiscountTypeProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlDiscountTypeProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlDiscountTypeProvider SqlDiscountTypeProvider
		{
			get {return DiscountTypeProvider as SqlDiscountTypeProvider;}
		}
		
		#endregion
		
		
		#region "PortalCountryProvider"
			
		private SqlPortalCountryProvider innerSqlPortalCountryProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="PortalCountry"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override PortalCountryProviderBase PortalCountryProvider
		{
			get
			{
				if (innerSqlPortalCountryProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlPortalCountryProvider == null)
						{
							this.innerSqlPortalCountryProvider = new SqlPortalCountryProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlPortalCountryProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlPortalCountryProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlPortalCountryProvider SqlPortalCountryProvider
		{
			get {return PortalCountryProvider as SqlPortalCountryProvider;}
		}
		
		#endregion
		
		
		#region "ZipCodeProvider"
			
		private SqlZipCodeProvider innerSqlZipCodeProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ZipCode"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ZipCodeProviderBase ZipCodeProvider
		{
			get
			{
				if (innerSqlZipCodeProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlZipCodeProvider == null)
						{
							this.innerSqlZipCodeProvider = new SqlZipCodeProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlZipCodeProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlZipCodeProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlZipCodeProvider SqlZipCodeProvider
		{
			get {return ZipCodeProvider as SqlZipCodeProvider;}
		}
		
		#endregion
		
		
		#region "SKUProfileProvider"
			
		private SqlSKUProfileProvider innerSqlSKUProfileProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="SKUProfile"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override SKUProfileProviderBase SKUProfileProvider
		{
			get
			{
				if (innerSqlSKUProfileProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlSKUProfileProvider == null)
						{
							this.innerSqlSKUProfileProvider = new SqlSKUProfileProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlSKUProfileProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlSKUProfileProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlSKUProfileProvider SqlSKUProfileProvider
		{
			get {return SKUProfileProvider as SqlSKUProfileProvider;}
		}
		
		#endregion
		
		
		#region "TrackingProvider"
			
		private SqlTrackingProvider innerSqlTrackingProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="Tracking"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override TrackingProviderBase TrackingProvider
		{
			get
			{
				if (innerSqlTrackingProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlTrackingProvider == null)
						{
							this.innerSqlTrackingProvider = new SqlTrackingProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlTrackingProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlTrackingProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlTrackingProvider SqlTrackingProvider
		{
			get {return TrackingProvider as SqlTrackingProvider;}
		}
		
		#endregion
		
		
		#region "LuceneGlobalProductBoostProvider"
			
		private SqlLuceneGlobalProductBoostProvider innerSqlLuceneGlobalProductBoostProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="LuceneGlobalProductBoost"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override LuceneGlobalProductBoostProviderBase LuceneGlobalProductBoostProvider
		{
			get
			{
				if (innerSqlLuceneGlobalProductBoostProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlLuceneGlobalProductBoostProvider == null)
						{
							this.innerSqlLuceneGlobalProductBoostProvider = new SqlLuceneGlobalProductBoostProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlLuceneGlobalProductBoostProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlLuceneGlobalProductBoostProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlLuceneGlobalProductBoostProvider SqlLuceneGlobalProductBoostProvider
		{
			get {return LuceneGlobalProductBoostProvider as SqlLuceneGlobalProductBoostProvider;}
		}
		
		#endregion
		
		
		#region "CasePriorityProvider"
			
		private SqlCasePriorityProvider innerSqlCasePriorityProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="CasePriority"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override CasePriorityProviderBase CasePriorityProvider
		{
			get
			{
				if (innerSqlCasePriorityProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlCasePriorityProvider == null)
						{
							this.innerSqlCasePriorityProvider = new SqlCasePriorityProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlCasePriorityProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlCasePriorityProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlCasePriorityProvider SqlCasePriorityProvider
		{
			get {return CasePriorityProvider as SqlCasePriorityProvider;}
		}
		
		#endregion
		
		
		#region "AddOnValueProvider"
			
		private SqlAddOnValueProvider innerSqlAddOnValueProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="AddOnValue"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override AddOnValueProviderBase AddOnValueProvider
		{
			get
			{
				if (innerSqlAddOnValueProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlAddOnValueProvider == null)
						{
							this.innerSqlAddOnValueProvider = new SqlAddOnValueProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlAddOnValueProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlAddOnValueProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlAddOnValueProvider SqlAddOnValueProvider
		{
			get {return AddOnValueProvider as SqlAddOnValueProvider;}
		}
		
		#endregion
		
		
		#region "PromotionProvider"
			
		private SqlPromotionProvider innerSqlPromotionProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="Promotion"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override PromotionProviderBase PromotionProvider
		{
			get
			{
				if (innerSqlPromotionProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlPromotionProvider == null)
						{
							this.innerSqlPromotionProvider = new SqlPromotionProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlPromotionProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlPromotionProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlPromotionProvider SqlPromotionProvider
		{
			get {return PromotionProvider as SqlPromotionProvider;}
		}
		
		#endregion
		
		
		#region "ProductTierProvider"
			
		private SqlProductTierProvider innerSqlProductTierProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ProductTier"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ProductTierProviderBase ProductTierProvider
		{
			get
			{
				if (innerSqlProductTierProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlProductTierProvider == null)
						{
							this.innerSqlProductTierProvider = new SqlProductTierProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlProductTierProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlProductTierProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlProductTierProvider SqlProductTierProvider
		{
			get {return ProductTierProvider as SqlProductTierProvider;}
		}
		
		#endregion
		
		
		#region "TaxRuleProvider"
			
		private SqlTaxRuleProvider innerSqlTaxRuleProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="TaxRule"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override TaxRuleProviderBase TaxRuleProvider
		{
			get
			{
				if (innerSqlTaxRuleProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlTaxRuleProvider == null)
						{
							this.innerSqlTaxRuleProvider = new SqlTaxRuleProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlTaxRuleProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlTaxRuleProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlTaxRuleProvider SqlTaxRuleProvider
		{
			get {return TaxRuleProvider as SqlTaxRuleProvider;}
		}
		
		#endregion
		
		
		#region "ShippingRuleProvider"
			
		private SqlShippingRuleProvider innerSqlShippingRuleProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ShippingRule"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ShippingRuleProviderBase ShippingRuleProvider
		{
			get
			{
				if (innerSqlShippingRuleProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlShippingRuleProvider == null)
						{
							this.innerSqlShippingRuleProvider = new SqlShippingRuleProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlShippingRuleProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlShippingRuleProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlShippingRuleProvider SqlShippingRuleProvider
		{
			get {return ShippingRuleProvider as SqlShippingRuleProvider;}
		}
		
		#endregion
		
		
		#region "LuceneGlobalProductCategoryBoostProvider"
			
		private SqlLuceneGlobalProductCategoryBoostProvider innerSqlLuceneGlobalProductCategoryBoostProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="LuceneGlobalProductCategoryBoost"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override LuceneGlobalProductCategoryBoostProviderBase LuceneGlobalProductCategoryBoostProvider
		{
			get
			{
				if (innerSqlLuceneGlobalProductCategoryBoostProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlLuceneGlobalProductCategoryBoostProvider == null)
						{
							this.innerSqlLuceneGlobalProductCategoryBoostProvider = new SqlLuceneGlobalProductCategoryBoostProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlLuceneGlobalProductCategoryBoostProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlLuceneGlobalProductCategoryBoostProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlLuceneGlobalProductCategoryBoostProvider SqlLuceneGlobalProductCategoryBoostProvider
		{
			get {return LuceneGlobalProductCategoryBoostProvider as SqlLuceneGlobalProductCategoryBoostProvider;}
		}
		
		#endregion
		
		
		#region "SavedCartProvider"
			
		private SqlSavedCartProvider innerSqlSavedCartProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="SavedCart"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override SavedCartProviderBase SavedCartProvider
		{
			get
			{
				if (innerSqlSavedCartProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlSavedCartProvider == null)
						{
							this.innerSqlSavedCartProvider = new SqlSavedCartProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlSavedCartProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlSavedCartProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlSavedCartProvider SqlSavedCartProvider
		{
			get {return SavedCartProvider as SqlSavedCartProvider;}
		}
		
		#endregion
		
		
		#region "ProductCrossSellProvider"
			
		private SqlProductCrossSellProvider innerSqlProductCrossSellProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ProductCrossSell"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ProductCrossSellProviderBase ProductCrossSellProvider
		{
			get
			{
				if (innerSqlProductCrossSellProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlProductCrossSellProvider == null)
						{
							this.innerSqlProductCrossSellProvider = new SqlProductCrossSellProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlProductCrossSellProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlProductCrossSellProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlProductCrossSellProvider SqlProductCrossSellProvider
		{
			get {return ProductCrossSellProvider as SqlProductCrossSellProvider;}
		}
		
		#endregion
		
		
		#region "LuceneGlobalProductBrandBoostProvider"
			
		private SqlLuceneGlobalProductBrandBoostProvider innerSqlLuceneGlobalProductBrandBoostProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="LuceneGlobalProductBrandBoost"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override LuceneGlobalProductBrandBoostProviderBase LuceneGlobalProductBrandBoostProvider
		{
			get
			{
				if (innerSqlLuceneGlobalProductBrandBoostProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlLuceneGlobalProductBrandBoostProvider == null)
						{
							this.innerSqlLuceneGlobalProductBrandBoostProvider = new SqlLuceneGlobalProductBrandBoostProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlLuceneGlobalProductBrandBoostProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlLuceneGlobalProductBrandBoostProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlLuceneGlobalProductBrandBoostProvider SqlLuceneGlobalProductBrandBoostProvider
		{
			get {return LuceneGlobalProductBrandBoostProvider as SqlLuceneGlobalProductBrandBoostProvider;}
		}
		
		#endregion
		
		
		#region "TaxRuleTypeProvider"
			
		private SqlTaxRuleTypeProvider innerSqlTaxRuleTypeProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="TaxRuleType"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override TaxRuleTypeProviderBase TaxRuleTypeProvider
		{
			get
			{
				if (innerSqlTaxRuleTypeProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlTaxRuleTypeProvider == null)
						{
							this.innerSqlTaxRuleTypeProvider = new SqlTaxRuleTypeProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlTaxRuleTypeProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlTaxRuleTypeProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlTaxRuleTypeProvider SqlTaxRuleTypeProvider
		{
			get {return TaxRuleTypeProvider as SqlTaxRuleTypeProvider;}
		}
		
		#endregion
		
		
		#region "FacetGroupCategoryProvider"
			
		private SqlFacetGroupCategoryProvider innerSqlFacetGroupCategoryProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="FacetGroupCategory"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override FacetGroupCategoryProviderBase FacetGroupCategoryProvider
		{
			get
			{
				if (innerSqlFacetGroupCategoryProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlFacetGroupCategoryProvider == null)
						{
							this.innerSqlFacetGroupCategoryProvider = new SqlFacetGroupCategoryProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlFacetGroupCategoryProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlFacetGroupCategoryProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlFacetGroupCategoryProvider SqlFacetGroupCategoryProvider
		{
			get {return FacetGroupCategoryProvider as SqlFacetGroupCategoryProvider;}
		}
		
		#endregion
		
		
		#region "AttributeTypeProvider"
			
		private SqlAttributeTypeProvider innerSqlAttributeTypeProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="AttributeType"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override AttributeTypeProviderBase AttributeTypeProvider
		{
			get
			{
				if (innerSqlAttributeTypeProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlAttributeTypeProvider == null)
						{
							this.innerSqlAttributeTypeProvider = new SqlAttributeTypeProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlAttributeTypeProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlAttributeTypeProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlAttributeTypeProvider SqlAttributeTypeProvider
		{
			get {return AttributeTypeProvider as SqlAttributeTypeProvider;}
		}
		
		#endregion
		
		
		#region "NoteProvider"
			
		private SqlNoteProvider innerSqlNoteProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="Note"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override NoteProviderBase NoteProvider
		{
			get
			{
				if (innerSqlNoteProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlNoteProvider == null)
						{
							this.innerSqlNoteProvider = new SqlNoteProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlNoteProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlNoteProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlNoteProvider SqlNoteProvider
		{
			get {return NoteProvider as SqlNoteProvider;}
		}
		
		#endregion
		
		
		#region "TrackingEventProvider"
			
		private SqlTrackingEventProvider innerSqlTrackingEventProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="TrackingEvent"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override TrackingEventProviderBase TrackingEventProvider
		{
			get
			{
				if (innerSqlTrackingEventProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlTrackingEventProvider == null)
						{
							this.innerSqlTrackingEventProvider = new SqlTrackingEventProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlTrackingEventProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlTrackingEventProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlTrackingEventProvider SqlTrackingEventProvider
		{
			get {return TrackingEventProvider as SqlTrackingEventProvider;}
		}
		
		#endregion
		
		
		#region "LuceneGlobalProductCatalogBoostProvider"
			
		private SqlLuceneGlobalProductCatalogBoostProvider innerSqlLuceneGlobalProductCatalogBoostProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="LuceneGlobalProductCatalogBoost"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override LuceneGlobalProductCatalogBoostProviderBase LuceneGlobalProductCatalogBoostProvider
		{
			get
			{
				if (innerSqlLuceneGlobalProductCatalogBoostProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlLuceneGlobalProductCatalogBoostProvider == null)
						{
							this.innerSqlLuceneGlobalProductCatalogBoostProvider = new SqlLuceneGlobalProductCatalogBoostProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlLuceneGlobalProductCatalogBoostProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlLuceneGlobalProductCatalogBoostProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlLuceneGlobalProductCatalogBoostProvider SqlLuceneGlobalProductCatalogBoostProvider
		{
			get {return LuceneGlobalProductCatalogBoostProvider as SqlLuceneGlobalProductCatalogBoostProvider;}
		}
		
		#endregion
		
		
		#region "OrderStateProvider"
			
		private SqlOrderStateProvider innerSqlOrderStateProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="OrderState"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override OrderStateProviderBase OrderStateProvider
		{
			get
			{
				if (innerSqlOrderStateProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlOrderStateProvider == null)
						{
							this.innerSqlOrderStateProvider = new SqlOrderStateProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlOrderStateProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlOrderStateProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlOrderStateProvider SqlOrderStateProvider
		{
			get {return OrderStateProvider as SqlOrderStateProvider;}
		}
		
		#endregion
		
		
		#region "OrderProcessingTypeProvider"
			
		private SqlOrderProcessingTypeProvider innerSqlOrderProcessingTypeProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="OrderProcessingType"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override OrderProcessingTypeProviderBase OrderProcessingTypeProvider
		{
			get
			{
				if (innerSqlOrderProcessingTypeProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlOrderProcessingTypeProvider == null)
						{
							this.innerSqlOrderProcessingTypeProvider = new SqlOrderProcessingTypeProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlOrderProcessingTypeProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlOrderProcessingTypeProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlOrderProcessingTypeProvider SqlOrderProcessingTypeProvider
		{
			get {return OrderProcessingTypeProvider as SqlOrderProcessingTypeProvider;}
		}
		
		#endregion
		
		
		#region "StoreProvider"
			
		private SqlStoreProvider innerSqlStoreProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="Store"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override StoreProviderBase StoreProvider
		{
			get
			{
				if (innerSqlStoreProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlStoreProvider == null)
						{
							this.innerSqlStoreProvider = new SqlStoreProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlStoreProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlStoreProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlStoreProvider SqlStoreProvider
		{
			get {return StoreProvider as SqlStoreProvider;}
		}
		
		#endregion
		
		
		#region "LocaleProvider"
			
		private SqlLocaleProvider innerSqlLocaleProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="Locale"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override LocaleProviderBase LocaleProvider
		{
			get
			{
				if (innerSqlLocaleProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlLocaleProvider == null)
						{
							this.innerSqlLocaleProvider = new SqlLocaleProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlLocaleProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlLocaleProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlLocaleProvider SqlLocaleProvider
		{
			get {return LocaleProvider as SqlLocaleProvider;}
		}
		
		#endregion
		
		
		#region "ProductReviewStateProvider"
			
		private SqlProductReviewStateProvider innerSqlProductReviewStateProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ProductReviewState"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ProductReviewStateProviderBase ProductReviewStateProvider
		{
			get
			{
				if (innerSqlProductReviewStateProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlProductReviewStateProvider == null)
						{
							this.innerSqlProductReviewStateProvider = new SqlProductReviewStateProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlProductReviewStateProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlProductReviewStateProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlProductReviewStateProvider SqlProductReviewStateProvider
		{
			get {return ProductReviewStateProvider as SqlProductReviewStateProvider;}
		}
		
		#endregion
		
		
		#region "AccountTypeProvider"
			
		private SqlAccountTypeProvider innerSqlAccountTypeProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="AccountType"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override AccountTypeProviderBase AccountTypeProvider
		{
			get
			{
				if (innerSqlAccountTypeProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlAccountTypeProvider == null)
						{
							this.innerSqlAccountTypeProvider = new SqlAccountTypeProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlAccountTypeProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlAccountTypeProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlAccountTypeProvider SqlAccountTypeProvider
		{
			get {return AccountTypeProvider as SqlAccountTypeProvider;}
		}
		
		#endregion
		
		
		#region "ShippingTypeProvider"
			
		private SqlShippingTypeProvider innerSqlShippingTypeProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ShippingType"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ShippingTypeProviderBase ShippingTypeProvider
		{
			get
			{
				if (innerSqlShippingTypeProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlShippingTypeProvider == null)
						{
							this.innerSqlShippingTypeProvider = new SqlShippingTypeProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlShippingTypeProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlShippingTypeProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlShippingTypeProvider SqlShippingTypeProvider
		{
			get {return ShippingTypeProvider as SqlShippingTypeProvider;}
		}
		
		#endregion
		
		
		#region "StateProvider"
			
		private SqlStateProvider innerSqlStateProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="State"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override StateProviderBase StateProvider
		{
			get
			{
				if (innerSqlStateProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlStateProvider == null)
						{
							this.innerSqlStateProvider = new SqlStateProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlStateProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlStateProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlStateProvider SqlStateProvider
		{
			get {return StateProvider as SqlStateProvider;}
		}
		
		#endregion
		
		
		#region "CurrencyTypeProvider"
			
		private SqlCurrencyTypeProvider innerSqlCurrencyTypeProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="CurrencyType"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override CurrencyTypeProviderBase CurrencyTypeProvider
		{
			get
			{
				if (innerSqlCurrencyTypeProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlCurrencyTypeProvider == null)
						{
							this.innerSqlCurrencyTypeProvider = new SqlCurrencyTypeProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlCurrencyTypeProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlCurrencyTypeProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlCurrencyTypeProvider SqlCurrencyTypeProvider
		{
			get {return CurrencyTypeProvider as SqlCurrencyTypeProvider;}
		}
		
		#endregion
		
		
		#region "ProductRelationTypeProvider"
			
		private SqlProductRelationTypeProvider innerSqlProductRelationTypeProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ProductRelationType"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ProductRelationTypeProviderBase ProductRelationTypeProvider
		{
			get
			{
				if (innerSqlProductRelationTypeProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlProductRelationTypeProvider == null)
						{
							this.innerSqlProductRelationTypeProvider = new SqlProductRelationTypeProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlProductRelationTypeProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlProductRelationTypeProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlProductRelationTypeProvider SqlProductRelationTypeProvider
		{
			get {return ProductRelationTypeProvider as SqlProductRelationTypeProvider;}
		}
		
		#endregion
		
		
		#region "ShippingRuleTypeProvider"
			
		private SqlShippingRuleTypeProvider innerSqlShippingRuleTypeProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ShippingRuleType"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ShippingRuleTypeProviderBase ShippingRuleTypeProvider
		{
			get
			{
				if (innerSqlShippingRuleTypeProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlShippingRuleTypeProvider == null)
						{
							this.innerSqlShippingRuleTypeProvider = new SqlShippingRuleTypeProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlShippingRuleTypeProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlShippingRuleTypeProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlShippingRuleTypeProvider SqlShippingRuleTypeProvider
		{
			get {return ShippingRuleTypeProvider as SqlShippingRuleTypeProvider;}
		}
		
		#endregion
		
		
		#region "PortalProvider"
			
		private SqlPortalProvider innerSqlPortalProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="Portal"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override PortalProviderBase PortalProvider
		{
			get
			{
				if (innerSqlPortalProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlPortalProvider == null)
						{
							this.innerSqlPortalProvider = new SqlPortalProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlPortalProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlPortalProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlPortalProvider SqlPortalProvider
		{
			get {return PortalProvider as SqlPortalProvider;}
		}
		
		#endregion
		
		
		#region "ProductExtensionOptionProductProvider"
			
		private SqlProductExtensionOptionProductProvider innerSqlProductExtensionOptionProductProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ProductExtensionOptionProduct"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ProductExtensionOptionProductProviderBase ProductExtensionOptionProductProvider
		{
			get
			{
				if (innerSqlProductExtensionOptionProductProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlProductExtensionOptionProductProvider == null)
						{
							this.innerSqlProductExtensionOptionProductProvider = new SqlProductExtensionOptionProductProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlProductExtensionOptionProductProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlProductExtensionOptionProductProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlProductExtensionOptionProductProvider SqlProductExtensionOptionProductProvider
		{
			get {return ProductExtensionOptionProductProvider as SqlProductExtensionOptionProductProvider;}
		}
		
		#endregion
		
		
		#region "ReferralCommissionTypeProvider"
			
		private SqlReferralCommissionTypeProvider innerSqlReferralCommissionTypeProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ReferralCommissionType"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ReferralCommissionTypeProviderBase ReferralCommissionTypeProvider
		{
			get
			{
				if (innerSqlReferralCommissionTypeProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlReferralCommissionTypeProvider == null)
						{
							this.innerSqlReferralCommissionTypeProvider = new SqlReferralCommissionTypeProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlReferralCommissionTypeProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlReferralCommissionTypeProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlReferralCommissionTypeProvider SqlReferralCommissionTypeProvider
		{
			get {return ReferralCommissionTypeProvider as SqlReferralCommissionTypeProvider;}
		}
		
		#endregion
		
		
		#region "TaxClassProvider"
			
		private SqlTaxClassProvider innerSqlTaxClassProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="TaxClass"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override TaxClassProviderBase TaxClassProvider
		{
			get
			{
				if (innerSqlTaxClassProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlTaxClassProvider == null)
						{
							this.innerSqlTaxClassProvider = new SqlTaxClassProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlTaxClassProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlTaxClassProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlTaxClassProvider SqlTaxClassProvider
		{
			get {return TaxClassProvider as SqlTaxClassProvider;}
		}
		
		#endregion
		
		
		#region "ProductExtensionRelationProvider"
			
		private SqlProductExtensionRelationProvider innerSqlProductExtensionRelationProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ProductExtensionRelation"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ProductExtensionRelationProviderBase ProductExtensionRelationProvider
		{
			get
			{
				if (innerSqlProductExtensionRelationProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlProductExtensionRelationProvider == null)
						{
							this.innerSqlProductExtensionRelationProvider = new SqlProductExtensionRelationProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlProductExtensionRelationProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlProductExtensionRelationProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlProductExtensionRelationProvider SqlProductExtensionRelationProvider
		{
			get {return ProductExtensionRelationProvider as SqlProductExtensionRelationProvider;}
		}
		
		#endregion
		
		
		#region "ProfileProvider"
			
		private SqlProfileProvider innerSqlProfileProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="Profile"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ProfileProviderBase ProfileProvider
		{
			get
			{
				if (innerSqlProfileProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlProfileProvider == null)
						{
							this.innerSqlProfileProvider = new SqlProfileProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlProfileProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlProfileProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlProfileProvider SqlProfileProvider
		{
			get {return ProfileProvider as SqlProfileProvider;}
		}
		
		#endregion
		
		
		#region "RMARequestProvider"
			
		private SqlRMARequestProvider innerSqlRMARequestProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="RMARequest"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override RMARequestProviderBase RMARequestProvider
		{
			get
			{
				if (innerSqlRMARequestProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlRMARequestProvider == null)
						{
							this.innerSqlRMARequestProvider = new SqlRMARequestProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlRMARequestProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlRMARequestProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlRMARequestProvider SqlRMARequestProvider
		{
			get {return RMARequestProvider as SqlRMARequestProvider;}
		}
		
		#endregion
		
		
		#region "PaymentSettingProvider"
			
		private SqlPaymentSettingProvider innerSqlPaymentSettingProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="PaymentSetting"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override PaymentSettingProviderBase PaymentSettingProvider
		{
			get
			{
				if (innerSqlPaymentSettingProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlPaymentSettingProvider == null)
						{
							this.innerSqlPaymentSettingProvider = new SqlPaymentSettingProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlPaymentSettingProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlPaymentSettingProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlPaymentSettingProvider SqlPaymentSettingProvider
		{
			get {return PaymentSettingProvider as SqlPaymentSettingProvider;}
		}
		
		#endregion
		
		
		#region "OrderShipmentProvider"
			
		private SqlOrderShipmentProvider innerSqlOrderShipmentProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="OrderShipment"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override OrderShipmentProviderBase OrderShipmentProvider
		{
			get
			{
				if (innerSqlOrderShipmentProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlOrderShipmentProvider == null)
						{
							this.innerSqlOrderShipmentProvider = new SqlOrderShipmentProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlOrderShipmentProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlOrderShipmentProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlOrderShipmentProvider SqlOrderShipmentProvider
		{
			get {return OrderShipmentProvider as SqlOrderShipmentProvider;}
		}
		
		#endregion
		
		
		#region "ProductExtensionOptionProvider"
			
		private SqlProductExtensionOptionProvider innerSqlProductExtensionOptionProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ProductExtensionOption"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ProductExtensionOptionProviderBase ProductExtensionOptionProvider
		{
			get
			{
				if (innerSqlProductExtensionOptionProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlProductExtensionOptionProvider == null)
						{
							this.innerSqlProductExtensionOptionProvider = new SqlProductExtensionOptionProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlProductExtensionOptionProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlProductExtensionOptionProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlProductExtensionOptionProvider SqlProductExtensionOptionProvider
		{
			get {return ProductExtensionOptionProvider as SqlProductExtensionOptionProvider;}
		}
		
		#endregion
		
		
		#region "OrderLineItemProvider"
			
		private SqlOrderLineItemProvider innerSqlOrderLineItemProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="OrderLineItem"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override OrderLineItemProviderBase OrderLineItemProvider
		{
			get
			{
				if (innerSqlOrderLineItemProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlOrderLineItemProvider == null)
						{
							this.innerSqlOrderLineItemProvider = new SqlOrderLineItemProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlOrderLineItemProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlOrderLineItemProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlOrderLineItemProvider SqlOrderLineItemProvider
		{
			get {return OrderLineItemProvider as SqlOrderLineItemProvider;}
		}
		
		#endregion
		
		
		#region "ReasonForReturnProvider"
			
		private SqlReasonForReturnProvider innerSqlReasonForReturnProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ReasonForReturn"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ReasonForReturnProviderBase ReasonForReturnProvider
		{
			get
			{
				if (innerSqlReasonForReturnProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlReasonForReturnProvider == null)
						{
							this.innerSqlReasonForReturnProvider = new SqlReasonForReturnProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlReasonForReturnProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlReasonForReturnProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlReasonForReturnProvider SqlReasonForReturnProvider
		{
			get {return ReasonForReturnProvider as SqlReasonForReturnProvider;}
		}
		
		#endregion
		
		
		#region "SourceModificationAuditProvider"
			
		private SqlSourceModificationAuditProvider innerSqlSourceModificationAuditProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="SourceModificationAudit"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override SourceModificationAuditProviderBase SourceModificationAuditProvider
		{
			get
			{
				if (innerSqlSourceModificationAuditProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlSourceModificationAuditProvider == null)
						{
							this.innerSqlSourceModificationAuditProvider = new SqlSourceModificationAuditProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlSourceModificationAuditProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlSourceModificationAuditProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlSourceModificationAuditProvider SqlSourceModificationAuditProvider
		{
			get {return SourceModificationAuditProvider as SqlSourceModificationAuditProvider;}
		}
		
		#endregion
		
		
		#region "UserDefinedViewProvider"
			
		private SqlUserDefinedViewProvider innerSqlUserDefinedViewProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="UserDefinedView"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override UserDefinedViewProviderBase UserDefinedViewProvider
		{
			get
			{
				if (innerSqlUserDefinedViewProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlUserDefinedViewProvider == null)
						{
							this.innerSqlUserDefinedViewProvider = new SqlUserDefinedViewProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlUserDefinedViewProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlUserDefinedViewProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlUserDefinedViewProvider SqlUserDefinedViewProvider
		{
			get {return UserDefinedViewProvider as SqlUserDefinedViewProvider;}
		}
		
		#endregion
		
		
		#region "RMARequestItemProvider"
			
		private SqlRMARequestItemProvider innerSqlRMARequestItemProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="RMARequestItem"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override RMARequestItemProviderBase RMARequestItemProvider
		{
			get
			{
				if (innerSqlRMARequestItemProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlRMARequestItemProvider == null)
						{
							this.innerSqlRMARequestItemProvider = new SqlRMARequestItemProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlRMARequestItemProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlRMARequestItemProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlRMARequestItemProvider SqlRMARequestItemProvider
		{
			get {return RMARequestItemProvider as SqlRMARequestItemProvider;}
		}
		
		#endregion
		
		
		#region "WorkflowProvider"
			
		private SqlWorkflowProvider innerSqlWorkflowProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="Workflow"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override WorkflowProviderBase WorkflowProvider
		{
			get
			{
				if (innerSqlWorkflowProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlWorkflowProvider == null)
						{
							this.innerSqlWorkflowProvider = new SqlWorkflowProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlWorkflowProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlWorkflowProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlWorkflowProvider SqlWorkflowProvider
		{
			get {return WorkflowProvider as SqlWorkflowProvider;}
		}
		
		#endregion
		
		
		#region "CustomerPricingProvider"
			
		private SqlCustomerPricingProvider innerSqlCustomerPricingProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="CustomerPricing"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override CustomerPricingProviderBase CustomerPricingProvider
		{
			get
			{
				if (innerSqlCustomerPricingProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlCustomerPricingProvider == null)
						{
							this.innerSqlCustomerPricingProvider = new SqlCustomerPricingProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlCustomerPricingProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlCustomerPricingProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlCustomerPricingProvider SqlCustomerPricingProvider
		{
			get {return CustomerPricingProvider as SqlCustomerPricingProvider;}
		}
		
		#endregion
		
		
		#region "SourceModificationTypeProvider"
			
		private SqlSourceModificationTypeProvider innerSqlSourceModificationTypeProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="SourceModificationType"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override SourceModificationTypeProviderBase SourceModificationTypeProvider
		{
			get
			{
				if (innerSqlSourceModificationTypeProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlSourceModificationTypeProvider == null)
						{
							this.innerSqlSourceModificationTypeProvider = new SqlSourceModificationTypeProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlSourceModificationTypeProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlSourceModificationTypeProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlSourceModificationTypeProvider SqlSourceModificationTypeProvider
		{
			get {return SourceModificationTypeProvider as SqlSourceModificationTypeProvider;}
		}
		
		#endregion
		
		
		#region "ApplicationSettingProvider"
			
		private SqlApplicationSettingProvider innerSqlApplicationSettingProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ApplicationSetting"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ApplicationSettingProviderBase ApplicationSettingProvider
		{
			get
			{
				if (innerSqlApplicationSettingProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlApplicationSettingProvider == null)
						{
							this.innerSqlApplicationSettingProvider = new SqlApplicationSettingProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlApplicationSettingProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlApplicationSettingProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlApplicationSettingProvider SqlApplicationSettingProvider
		{
			get {return ApplicationSettingProvider as SqlApplicationSettingProvider;}
		}
		
		#endregion
		
		
		#region "PaymentTokenProvider"
			
		private SqlPaymentTokenProvider innerSqlPaymentTokenProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="PaymentToken"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override PaymentTokenProviderBase PaymentTokenProvider
		{
			get
			{
				if (innerSqlPaymentTokenProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlPaymentTokenProvider == null)
						{
							this.innerSqlPaymentTokenProvider = new SqlPaymentTokenProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlPaymentTokenProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlPaymentTokenProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlPaymentTokenProvider SqlPaymentTokenProvider
		{
			get {return PaymentTokenProvider as SqlPaymentTokenProvider;}
		}
		
		#endregion
		
		
		#region "RequestStatusProvider"
			
		private SqlRequestStatusProvider innerSqlRequestStatusProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="RequestStatus"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override RequestStatusProviderBase RequestStatusProvider
		{
			get
			{
				if (innerSqlRequestStatusProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlRequestStatusProvider == null)
						{
							this.innerSqlRequestStatusProvider = new SqlRequestStatusProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlRequestStatusProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlRequestStatusProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlRequestStatusProvider SqlRequestStatusProvider
		{
			get {return RequestStatusProvider as SqlRequestStatusProvider;}
		}
		
		#endregion
		
		
		#region "UrlRedirectProvider"
			
		private SqlUrlRedirectProvider innerSqlUrlRedirectProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="UrlRedirect"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override UrlRedirectProviderBase UrlRedirectProvider
		{
			get
			{
				if (innerSqlUrlRedirectProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlUrlRedirectProvider == null)
						{
							this.innerSqlUrlRedirectProvider = new SqlUrlRedirectProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlUrlRedirectProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlUrlRedirectProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlUrlRedirectProvider SqlUrlRedirectProvider
		{
			get {return UrlRedirectProvider as SqlUrlRedirectProvider;}
		}
		
		#endregion
		
		
		#region "OrderDetailProductExtensionOptionProvider"
			
		private SqlOrderDetailProductExtensionOptionProvider innerSqlOrderDetailProductExtensionOptionProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="OrderDetailProductExtensionOption"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override OrderDetailProductExtensionOptionProviderBase OrderDetailProductExtensionOptionProvider
		{
			get
			{
				if (innerSqlOrderDetailProductExtensionOptionProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlOrderDetailProductExtensionOptionProvider == null)
						{
							this.innerSqlOrderDetailProductExtensionOptionProvider = new SqlOrderDetailProductExtensionOptionProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlOrderDetailProductExtensionOptionProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlOrderDetailProductExtensionOptionProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlOrderDetailProductExtensionOptionProvider SqlOrderDetailProductExtensionOptionProvider
		{
			get {return OrderDetailProductExtensionOptionProvider as SqlOrderDetailProductExtensionOptionProvider;}
		}
		
		#endregion
		
		
		#region "AccountProvider"
			
		private SqlAccountProvider innerSqlAccountProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="Account"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override AccountProviderBase AccountProvider
		{
			get
			{
				if (innerSqlAccountProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlAccountProvider == null)
						{
							this.innerSqlAccountProvider = new SqlAccountProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlAccountProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlAccountProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlAccountProvider SqlAccountProvider
		{
			get {return AccountProvider as SqlAccountProvider;}
		}
		
		#endregion
		
		
		#region "ThemeProvider"
			
		private SqlThemeProvider innerSqlThemeProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="Theme"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ThemeProviderBase ThemeProvider
		{
			get
			{
				if (innerSqlThemeProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlThemeProvider == null)
						{
							this.innerSqlThemeProvider = new SqlThemeProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlThemeProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlThemeProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlThemeProvider SqlThemeProvider
		{
			get {return ThemeProvider as SqlThemeProvider;}
		}
		
		#endregion
		
		
		#region "OrderLineItemRelationshipTypeProvider"
			
		private SqlOrderLineItemRelationshipTypeProvider innerSqlOrderLineItemRelationshipTypeProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="OrderLineItemRelationshipType"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override OrderLineItemRelationshipTypeProviderBase OrderLineItemRelationshipTypeProvider
		{
			get
			{
				if (innerSqlOrderLineItemRelationshipTypeProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlOrderLineItemRelationshipTypeProvider == null)
						{
							this.innerSqlOrderLineItemRelationshipTypeProvider = new SqlOrderLineItemRelationshipTypeProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlOrderLineItemRelationshipTypeProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlOrderLineItemRelationshipTypeProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlOrderLineItemRelationshipTypeProvider SqlOrderLineItemRelationshipTypeProvider
		{
			get {return OrderLineItemRelationshipTypeProvider as SqlOrderLineItemRelationshipTypeProvider;}
		}
		
		#endregion
		
		
		#region "SourceModificationStatusProvider"
			
		private SqlSourceModificationStatusProvider innerSqlSourceModificationStatusProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="SourceModificationStatus"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override SourceModificationStatusProviderBase SourceModificationStatusProvider
		{
			get
			{
				if (innerSqlSourceModificationStatusProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlSourceModificationStatusProvider == null)
						{
							this.innerSqlSourceModificationStatusProvider = new SqlSourceModificationStatusProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlSourceModificationStatusProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlSourceModificationStatusProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlSourceModificationStatusProvider SqlSourceModificationStatusProvider
		{
			get {return SourceModificationStatusProvider as SqlSourceModificationStatusProvider;}
		}
		
		#endregion
		
		
		#region "LuceneIndexStatusNamesProvider"
			
		private SqlLuceneIndexStatusNamesProvider innerSqlLuceneIndexStatusNamesProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="LuceneIndexStatusNames"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override LuceneIndexStatusNamesProviderBase LuceneIndexStatusNamesProvider
		{
			get
			{
				if (innerSqlLuceneIndexStatusNamesProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlLuceneIndexStatusNamesProvider == null)
						{
							this.innerSqlLuceneIndexStatusNamesProvider = new SqlLuceneIndexStatusNamesProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlLuceneIndexStatusNamesProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlLuceneIndexStatusNamesProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlLuceneIndexStatusNamesProvider SqlLuceneIndexStatusNamesProvider
		{
			get {return LuceneIndexStatusNamesProvider as SqlLuceneIndexStatusNamesProvider;}
		}
		
		#endregion
		
		
		#region "DigitalAssetProvider"
			
		private SqlDigitalAssetProvider innerSqlDigitalAssetProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="DigitalAsset"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override DigitalAssetProviderBase DigitalAssetProvider
		{
			get
			{
				if (innerSqlDigitalAssetProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlDigitalAssetProvider == null)
						{
							this.innerSqlDigitalAssetProvider = new SqlDigitalAssetProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlDigitalAssetProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlDigitalAssetProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlDigitalAssetProvider SqlDigitalAssetProvider
		{
			get {return DigitalAssetProvider as SqlDigitalAssetProvider;}
		}
		
		#endregion
		
		
		#region "OrderProvider"
			
		private SqlOrderProvider innerSqlOrderProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="Order"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override OrderProviderBase OrderProvider
		{
			get
			{
				if (innerSqlOrderProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlOrderProvider == null)
						{
							this.innerSqlOrderProvider = new SqlOrderProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlOrderProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlOrderProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlOrderProvider SqlOrderProvider
		{
			get {return OrderProvider as SqlOrderProvider;}
		}
		
		#endregion
		
		
		#region "SourceTypeProvider"
			
		private SqlSourceTypeProvider innerSqlSourceTypeProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="SourceType"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override SourceTypeProviderBase SourceTypeProvider
		{
			get
			{
				if (innerSqlSourceTypeProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlSourceTypeProvider == null)
						{
							this.innerSqlSourceTypeProvider = new SqlSourceTypeProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlSourceTypeProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlSourceTypeProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlSourceTypeProvider SqlSourceTypeProvider
		{
			get {return SourceTypeProvider as SqlSourceTypeProvider;}
		}
		
		#endregion
		
		
		#region "PasswordLogProvider"
			
		private SqlPasswordLogProvider innerSqlPasswordLogProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="PasswordLog"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override PasswordLogProviderBase PasswordLogProvider
		{
			get
			{
				if (innerSqlPasswordLogProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlPasswordLogProvider == null)
						{
							this.innerSqlPasswordLogProvider = new SqlPasswordLogProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlPasswordLogProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlPasswordLogProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlPasswordLogProvider SqlPasswordLogProvider
		{
			get {return PasswordLogProvider as SqlPasswordLogProvider;}
		}
		
		#endregion
		
		
		#region "LuceneIndexMonitorProvider"
			
		private SqlLuceneIndexMonitorProvider innerSqlLuceneIndexMonitorProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="LuceneIndexMonitor"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override LuceneIndexMonitorProviderBase LuceneIndexMonitorProvider
		{
			get
			{
				if (innerSqlLuceneIndexMonitorProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlLuceneIndexMonitorProvider == null)
						{
							this.innerSqlLuceneIndexMonitorProvider = new SqlLuceneIndexMonitorProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlLuceneIndexMonitorProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlLuceneIndexMonitorProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlLuceneIndexMonitorProvider SqlLuceneIndexMonitorProvider
		{
			get {return LuceneIndexMonitorProvider as SqlLuceneIndexMonitorProvider;}
		}
		
		#endregion
		
		
		#region "MultifrontProvider"
			
		private SqlMultifrontProvider innerSqlMultifrontProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="Multifront"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override MultifrontProviderBase MultifrontProvider
		{
			get
			{
				if (innerSqlMultifrontProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlMultifrontProvider == null)
						{
							this.innerSqlMultifrontProvider = new SqlMultifrontProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlMultifrontProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlMultifrontProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlMultifrontProvider SqlMultifrontProvider
		{
			get {return MultifrontProvider as SqlMultifrontProvider;}
		}
		
		#endregion
		
		
		#region "GiftCardProvider"
			
		private SqlGiftCardProvider innerSqlGiftCardProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="GiftCard"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override GiftCardProviderBase GiftCardProvider
		{
			get
			{
				if (innerSqlGiftCardProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlGiftCardProvider == null)
						{
							this.innerSqlGiftCardProvider = new SqlGiftCardProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlGiftCardProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlGiftCardProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlGiftCardProvider SqlGiftCardProvider
		{
			get {return GiftCardProvider as SqlGiftCardProvider;}
		}
		
		#endregion
		
		
		#region "GiftCardHistoryProvider"
			
		private SqlGiftCardHistoryProvider innerSqlGiftCardHistoryProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="GiftCardHistory"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override GiftCardHistoryProviderBase GiftCardHistoryProvider
		{
			get
			{
				if (innerSqlGiftCardHistoryProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlGiftCardHistoryProvider == null)
						{
							this.innerSqlGiftCardHistoryProvider = new SqlGiftCardHistoryProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlGiftCardHistoryProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlGiftCardHistoryProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlGiftCardHistoryProvider SqlGiftCardHistoryProvider
		{
			get {return GiftCardHistoryProvider as SqlGiftCardHistoryProvider;}
		}
		
		#endregion
		
		
		#region "MessageTypeProvider"
			
		private SqlMessageTypeProvider innerSqlMessageTypeProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="MessageType"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override MessageTypeProviderBase MessageTypeProvider
		{
			get
			{
				if (innerSqlMessageTypeProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlMessageTypeProvider == null)
						{
							this.innerSqlMessageTypeProvider = new SqlMessageTypeProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlMessageTypeProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlMessageTypeProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlMessageTypeProvider SqlMessageTypeProvider
		{
			get {return MessageTypeProvider as SqlMessageTypeProvider;}
		}
		
		#endregion
		
		
		#region "MessageConfigProvider"
			
		private SqlMessageConfigProvider innerSqlMessageConfigProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="MessageConfig"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override MessageConfigProviderBase MessageConfigProvider
		{
			get
			{
				if (innerSqlMessageConfigProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlMessageConfigProvider == null)
						{
							this.innerSqlMessageConfigProvider = new SqlMessageConfigProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlMessageConfigProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlMessageConfigProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlMessageConfigProvider SqlMessageConfigProvider
		{
			get {return MessageConfigProvider as SqlMessageConfigProvider;}
		}
		
		#endregion
		
		
		#region "ProductTypeProvider"
			
		private SqlProductTypeProvider innerSqlProductTypeProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ProductType"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ProductTypeProviderBase ProductTypeProvider
		{
			get
			{
				if (innerSqlProductTypeProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlProductTypeProvider == null)
						{
							this.innerSqlProductTypeProvider = new SqlProductTypeProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlProductTypeProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlProductTypeProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlProductTypeProvider SqlProductTypeProvider
		{
			get {return ProductTypeProvider as SqlProductTypeProvider;}
		}
		
		#endregion
		
		
		#region "CatalogProvider"
			
		private SqlCatalogProvider innerSqlCatalogProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="Catalog"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override CatalogProviderBase CatalogProvider
		{
			get
			{
				if (innerSqlCatalogProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlCatalogProvider == null)
						{
							this.innerSqlCatalogProvider = new SqlCatalogProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlCatalogProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlCatalogProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlCatalogProvider SqlCatalogProvider
		{
			get {return CatalogProvider as SqlCatalogProvider;}
		}
		
		#endregion
		
		
		#region "ManufacturerProvider"
			
		private SqlManufacturerProvider innerSqlManufacturerProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="Manufacturer"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ManufacturerProviderBase ManufacturerProvider
		{
			get
			{
				if (innerSqlManufacturerProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlManufacturerProvider == null)
						{
							this.innerSqlManufacturerProvider = new SqlManufacturerProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlManufacturerProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlManufacturerProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlManufacturerProvider SqlManufacturerProvider
		{
			get {return ManufacturerProvider as SqlManufacturerProvider;}
		}
		
		#endregion
		
		
		#region "SupplierProvider"
			
		private SqlSupplierProvider innerSqlSupplierProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="Supplier"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override SupplierProviderBase SupplierProvider
		{
			get
			{
				if (innerSqlSupplierProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlSupplierProvider == null)
						{
							this.innerSqlSupplierProvider = new SqlSupplierProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlSupplierProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlSupplierProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlSupplierProvider SqlSupplierProvider
		{
			get {return SupplierProvider as SqlSupplierProvider;}
		}
		
		#endregion
		
		
		#region "ProductProvider"
			
		private SqlProductProvider innerSqlProductProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="Product"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ProductProviderBase ProductProvider
		{
			get
			{
				if (innerSqlProductProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlProductProvider == null)
						{
							this.innerSqlProductProvider = new SqlProductProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlProductProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlProductProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlProductProvider SqlProductProvider
		{
			get {return ProductProvider as SqlProductProvider;}
		}
		
		#endregion
		
		
		#region "PaymentStatusProvider"
			
		private SqlPaymentStatusProvider innerSqlPaymentStatusProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="PaymentStatus"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override PaymentStatusProviderBase PaymentStatusProvider
		{
			get
			{
				if (innerSqlPaymentStatusProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlPaymentStatusProvider == null)
						{
							this.innerSqlPaymentStatusProvider = new SqlPaymentStatusProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlPaymentStatusProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlPaymentStatusProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlPaymentStatusProvider SqlPaymentStatusProvider
		{
			get {return PaymentStatusProvider as SqlPaymentStatusProvider;}
		}
		
		#endregion
		
		
		#region "SKUProvider"
			
		private SqlSKUProvider innerSqlSKUProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="SKU"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override SKUProviderBase SKUProvider
		{
			get
			{
				if (innerSqlSKUProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlSKUProvider == null)
						{
							this.innerSqlSKUProvider = new SqlSKUProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlSKUProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlSKUProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlSKUProvider SqlSKUProvider
		{
			get {return SKUProvider as SqlSKUProvider;}
		}
		
		#endregion
		
		
		#region "CookieMappingProvider"
			
		private SqlCookieMappingProvider innerSqlCookieMappingProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="CookieMapping"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override CookieMappingProviderBase CookieMappingProvider
		{
			get
			{
				if (innerSqlCookieMappingProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlCookieMappingProvider == null)
						{
							this.innerSqlCookieMappingProvider = new SqlCookieMappingProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlCookieMappingProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlCookieMappingProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlCookieMappingProvider SqlCookieMappingProvider
		{
			get {return CookieMappingProvider as SqlCookieMappingProvider;}
		}
		
		#endregion
		
		
		#region "ReferralCommissionProvider"
			
		private SqlReferralCommissionProvider innerSqlReferralCommissionProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ReferralCommission"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ReferralCommissionProviderBase ReferralCommissionProvider
		{
			get
			{
				if (innerSqlReferralCommissionProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlReferralCommissionProvider == null)
						{
							this.innerSqlReferralCommissionProvider = new SqlReferralCommissionProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlReferralCommissionProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlReferralCommissionProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlReferralCommissionProvider SqlReferralCommissionProvider
		{
			get {return ReferralCommissionProvider as SqlReferralCommissionProvider;}
		}
		
		#endregion
		
		
		#region "MasterPageProvider"
			
		private SqlMasterPageProvider innerSqlMasterPageProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="MasterPage"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override MasterPageProviderBase MasterPageProvider
		{
			get
			{
				if (innerSqlMasterPageProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlMasterPageProvider == null)
						{
							this.innerSqlMasterPageProvider = new SqlMasterPageProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlMasterPageProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlMasterPageProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlMasterPageProvider SqlMasterPageProvider
		{
			get {return MasterPageProvider as SqlMasterPageProvider;}
		}
		
		#endregion
		
		
		#region "CountryProvider"
			
		private SqlCountryProvider innerSqlCountryProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="Country"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override CountryProviderBase CountryProvider
		{
			get
			{
				if (innerSqlCountryProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlCountryProvider == null)
						{
							this.innerSqlCountryProvider = new SqlCountryProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlCountryProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlCountryProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlCountryProvider SqlCountryProvider
		{
			get {return CountryProvider as SqlCountryProvider;}
		}
		
		#endregion
		
		
		#region "SupplierTypeProvider"
			
		private SqlSupplierTypeProvider innerSqlSupplierTypeProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="SupplierType"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override SupplierTypeProviderBase SupplierTypeProvider
		{
			get
			{
				if (innerSqlSupplierTypeProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlSupplierTypeProvider == null)
						{
							this.innerSqlSupplierTypeProvider = new SqlSupplierTypeProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlSupplierTypeProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlSupplierTypeProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlSupplierTypeProvider SqlSupplierTypeProvider
		{
			get {return SupplierTypeProvider as SqlSupplierTypeProvider;}
		}
		
		#endregion
		
		
		#region "CaseStatusProvider"
			
		private SqlCaseStatusProvider innerSqlCaseStatusProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="CaseStatus"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override CaseStatusProviderBase CaseStatusProvider
		{
			get
			{
				if (innerSqlCaseStatusProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlCaseStatusProvider == null)
						{
							this.innerSqlCaseStatusProvider = new SqlCaseStatusProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlCaseStatusProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlCaseStatusProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlCaseStatusProvider SqlCaseStatusProvider
		{
			get {return CaseStatusProvider as SqlCaseStatusProvider;}
		}
		
		#endregion
		
		
		#region "ShippingServiceCodeProvider"
			
		private SqlShippingServiceCodeProvider innerSqlShippingServiceCodeProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ShippingServiceCode"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ShippingServiceCodeProviderBase ShippingServiceCodeProvider
		{
			get
			{
				if (innerSqlShippingServiceCodeProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlShippingServiceCodeProvider == null)
						{
							this.innerSqlShippingServiceCodeProvider = new SqlShippingServiceCodeProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlShippingServiceCodeProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlShippingServiceCodeProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlShippingServiceCodeProvider SqlShippingServiceCodeProvider
		{
			get {return ShippingServiceCodeProvider as SqlShippingServiceCodeProvider;}
		}
		
		#endregion
		
		
		#region "PaymentTokenAuthorizeProvider"
			
		private SqlPaymentTokenAuthorizeProvider innerSqlPaymentTokenAuthorizeProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="PaymentTokenAuthorize"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override PaymentTokenAuthorizeProviderBase PaymentTokenAuthorizeProvider
		{
			get
			{
				if (innerSqlPaymentTokenAuthorizeProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlPaymentTokenAuthorizeProvider == null)
						{
							this.innerSqlPaymentTokenAuthorizeProvider = new SqlPaymentTokenAuthorizeProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlPaymentTokenAuthorizeProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlPaymentTokenAuthorizeProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlPaymentTokenAuthorizeProvider SqlPaymentTokenAuthorizeProvider
		{
			get {return PaymentTokenAuthorizeProvider as SqlPaymentTokenAuthorizeProvider;}
		}
		
		#endregion
		
		
		#region "RMAConfigurationProvider"
			
		private SqlRMAConfigurationProvider innerSqlRMAConfigurationProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="RMAConfiguration"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override RMAConfigurationProviderBase RMAConfigurationProvider
		{
			get
			{
				if (innerSqlRMAConfigurationProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlRMAConfigurationProvider == null)
						{
							this.innerSqlRMAConfigurationProvider = new SqlRMAConfigurationProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlRMAConfigurationProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlRMAConfigurationProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlRMAConfigurationProvider SqlRMAConfigurationProvider
		{
			get {return RMAConfigurationProvider as SqlRMAConfigurationProvider;}
		}
		
		#endregion
		
		
		#region "LuceneServerConfigurationStatusProvider"
			
		private SqlLuceneServerConfigurationStatusProvider innerSqlLuceneServerConfigurationStatusProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="LuceneServerConfigurationStatus"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override LuceneServerConfigurationStatusProviderBase LuceneServerConfigurationStatusProvider
		{
			get
			{
				if (innerSqlLuceneServerConfigurationStatusProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlLuceneServerConfigurationStatusProvider == null)
						{
							this.innerSqlLuceneServerConfigurationStatusProvider = new SqlLuceneServerConfigurationStatusProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlLuceneServerConfigurationStatusProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlLuceneServerConfigurationStatusProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlLuceneServerConfigurationStatusProvider SqlLuceneServerConfigurationStatusProvider
		{
			get {return LuceneServerConfigurationStatusProvider as SqlLuceneServerConfigurationStatusProvider;}
		}
		
		#endregion
		
		
		#region "PortalWorkflowProvider"
			
		private SqlPortalWorkflowProvider innerSqlPortalWorkflowProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="PortalWorkflow"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override PortalWorkflowProviderBase PortalWorkflowProvider
		{
			get
			{
				if (innerSqlPortalWorkflowProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlPortalWorkflowProvider == null)
						{
							this.innerSqlPortalWorkflowProvider = new SqlPortalWorkflowProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlPortalWorkflowProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlPortalWorkflowProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlPortalWorkflowProvider SqlPortalWorkflowProvider
		{
			get {return PortalWorkflowProvider as SqlPortalWorkflowProvider;}
		}
		
		#endregion
		
		
		#region "AccountPaymentProvider"
			
		private SqlAccountPaymentProvider innerSqlAccountPaymentProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="AccountPayment"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override AccountPaymentProviderBase AccountPaymentProvider
		{
			get
			{
				if (innerSqlAccountPaymentProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlAccountPaymentProvider == null)
						{
							this.innerSqlAccountPaymentProvider = new SqlAccountPaymentProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlAccountPaymentProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlAccountPaymentProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlAccountPaymentProvider SqlAccountPaymentProvider
		{
			get {return AccountPaymentProvider as SqlAccountPaymentProvider;}
		}
		
		#endregion
		
		
		#region "ProductImageTypeProvider"
			
		private SqlProductImageTypeProvider innerSqlProductImageTypeProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ProductImageType"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ProductImageTypeProviderBase ProductImageTypeProvider
		{
			get
			{
				if (innerSqlProductImageTypeProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlProductImageTypeProvider == null)
						{
							this.innerSqlProductImageTypeProvider = new SqlProductImageTypeProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlProductImageTypeProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlProductImageTypeProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlProductImageTypeProvider SqlProductImageTypeProvider
		{
			get {return ProductImageTypeProvider as SqlProductImageTypeProvider;}
		}
		
		#endregion
		
		
		#region "ProductExtensionProvider"
			
		private SqlProductExtensionProvider innerSqlProductExtensionProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ProductExtension"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ProductExtensionProviderBase ProductExtensionProvider
		{
			get
			{
				if (innerSqlProductExtensionProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlProductExtensionProvider == null)
						{
							this.innerSqlProductExtensionProvider = new SqlProductExtensionProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlProductExtensionProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlProductExtensionProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlProductExtensionProvider SqlProductExtensionProvider
		{
			get {return ProductExtensionProvider as SqlProductExtensionProvider;}
		}
		
		#endregion
		
		
		#region "SavedCartLineItemProductExtensionOptionProvider"
			
		private SqlSavedCartLineItemProductExtensionOptionProvider innerSqlSavedCartLineItemProductExtensionOptionProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="SavedCartLineItemProductExtensionOption"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override SavedCartLineItemProductExtensionOptionProviderBase SavedCartLineItemProductExtensionOptionProvider
		{
			get
			{
				if (innerSqlSavedCartLineItemProductExtensionOptionProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlSavedCartLineItemProductExtensionOptionProvider == null)
						{
							this.innerSqlSavedCartLineItemProductExtensionOptionProvider = new SqlSavedCartLineItemProductExtensionOptionProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlSavedCartLineItemProductExtensionOptionProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlSavedCartLineItemProductExtensionOptionProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlSavedCartLineItemProductExtensionOptionProvider SqlSavedCartLineItemProductExtensionOptionProvider
		{
			get {return SavedCartLineItemProductExtensionOptionProvider as SqlSavedCartLineItemProductExtensionOptionProvider;}
		}
		
		#endregion
		
		
		#region "PaymentTypeProvider"
			
		private SqlPaymentTypeProvider innerSqlPaymentTypeProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="PaymentType"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override PaymentTypeProviderBase PaymentTypeProvider
		{
			get
			{
				if (innerSqlPaymentTypeProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlPaymentTypeProvider == null)
						{
							this.innerSqlPaymentTypeProvider = new SqlPaymentTypeProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlPaymentTypeProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlPaymentTypeProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlPaymentTypeProvider SqlPaymentTypeProvider
		{
			get {return PaymentTypeProvider as SqlPaymentTypeProvider;}
		}
		
		#endregion
		
		
		#region "ParentChildProductProvider"
			
		private SqlParentChildProductProvider innerSqlParentChildProductProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ParentChildProduct"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ParentChildProductProviderBase ParentChildProductProvider
		{
			get
			{
				if (innerSqlParentChildProductProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlParentChildProductProvider == null)
						{
							this.innerSqlParentChildProductProvider = new SqlParentChildProductProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlParentChildProductProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlParentChildProductProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlParentChildProductProvider SqlParentChildProductProvider
		{
			get {return ParentChildProductProvider as SqlParentChildProductProvider;}
		}
		
		#endregion
		
		
		#region "LuceneIndexServerStatusProvider"
			
		private SqlLuceneIndexServerStatusProvider innerSqlLuceneIndexServerStatusProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="LuceneIndexServerStatus"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override LuceneIndexServerStatusProviderBase LuceneIndexServerStatusProvider
		{
			get
			{
				if (innerSqlLuceneIndexServerStatusProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlLuceneIndexServerStatusProvider == null)
						{
							this.innerSqlLuceneIndexServerStatusProvider = new SqlLuceneIndexServerStatusProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlLuceneIndexServerStatusProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlLuceneIndexServerStatusProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlLuceneIndexServerStatusProvider SqlLuceneIndexServerStatusProvider
		{
			get {return LuceneIndexServerStatusProvider as SqlLuceneIndexServerStatusProvider;}
		}
		
		#endregion
		
		
		
		#region "VwZnodeCategoriesCatalogsProvider"
		
		private SqlVwZnodeCategoriesCatalogsProvider innerSqlVwZnodeCategoriesCatalogsProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="VwZnodeCategoriesCatalogs"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override VwZnodeCategoriesCatalogsProviderBase VwZnodeCategoriesCatalogsProvider
		{
			get
			{
				if (innerSqlVwZnodeCategoriesCatalogsProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlVwZnodeCategoriesCatalogsProvider == null)
						{
							this.innerSqlVwZnodeCategoriesCatalogsProvider = new SqlVwZnodeCategoriesCatalogsProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlVwZnodeCategoriesCatalogsProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlVwZnodeCategoriesCatalogsProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlVwZnodeCategoriesCatalogsProvider SqlVwZnodeCategoriesCatalogsProvider
		{
			get {return VwZnodeCategoriesCatalogsProvider as SqlVwZnodeCategoriesCatalogsProvider;}
		}
		
		#endregion
		
		
		#region "VwZnodeCategoriesPortalsProvider"
		
		private SqlVwZnodeCategoriesPortalsProvider innerSqlVwZnodeCategoriesPortalsProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="VwZnodeCategoriesPortals"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override VwZnodeCategoriesPortalsProviderBase VwZnodeCategoriesPortalsProvider
		{
			get
			{
				if (innerSqlVwZnodeCategoriesPortalsProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlVwZnodeCategoriesPortalsProvider == null)
						{
							this.innerSqlVwZnodeCategoriesPortalsProvider = new SqlVwZnodeCategoriesPortalsProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlVwZnodeCategoriesPortalsProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlVwZnodeCategoriesPortalsProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlVwZnodeCategoriesPortalsProvider SqlVwZnodeCategoriesPortalsProvider
		{
			get {return VwZnodeCategoriesPortalsProvider as SqlVwZnodeCategoriesPortalsProvider;}
		}
		
		#endregion
		
		
		#region "VwZnodeProductsCatalogsProvider"
		
		private SqlVwZnodeProductsCatalogsProvider innerSqlVwZnodeProductsCatalogsProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="VwZnodeProductsCatalogs"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override VwZnodeProductsCatalogsProviderBase VwZnodeProductsCatalogsProvider
		{
			get
			{
				if (innerSqlVwZnodeProductsCatalogsProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlVwZnodeProductsCatalogsProvider == null)
						{
							this.innerSqlVwZnodeProductsCatalogsProvider = new SqlVwZnodeProductsCatalogsProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlVwZnodeProductsCatalogsProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlVwZnodeProductsCatalogsProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlVwZnodeProductsCatalogsProvider SqlVwZnodeProductsCatalogsProvider
		{
			get {return VwZnodeProductsCatalogsProvider as SqlVwZnodeProductsCatalogsProvider;}
		}
		
		#endregion
		
		
		#region "VwZnodeProductsCategoriesProvider"
		
		private SqlVwZnodeProductsCategoriesProvider innerSqlVwZnodeProductsCategoriesProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="VwZnodeProductsCategories"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override VwZnodeProductsCategoriesProviderBase VwZnodeProductsCategoriesProvider
		{
			get
			{
				if (innerSqlVwZnodeProductsCategoriesProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlVwZnodeProductsCategoriesProvider == null)
						{
							this.innerSqlVwZnodeProductsCategoriesProvider = new SqlVwZnodeProductsCategoriesProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlVwZnodeProductsCategoriesProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlVwZnodeProductsCategoriesProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlVwZnodeProductsCategoriesProvider SqlVwZnodeProductsCategoriesProvider
		{
			get {return VwZnodeProductsCategoriesProvider as SqlVwZnodeProductsCategoriesProvider;}
		}
		
		#endregion
		
		
		#region "VwZnodeProductsCategoriesPromotionsProvider"
		
		private SqlVwZnodeProductsCategoriesPromotionsProvider innerSqlVwZnodeProductsCategoriesPromotionsProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="VwZnodeProductsCategoriesPromotions"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override VwZnodeProductsCategoriesPromotionsProviderBase VwZnodeProductsCategoriesPromotionsProvider
		{
			get
			{
				if (innerSqlVwZnodeProductsCategoriesPromotionsProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlVwZnodeProductsCategoriesPromotionsProvider == null)
						{
							this.innerSqlVwZnodeProductsCategoriesPromotionsProvider = new SqlVwZnodeProductsCategoriesPromotionsProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlVwZnodeProductsCategoriesPromotionsProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlVwZnodeProductsCategoriesPromotionsProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlVwZnodeProductsCategoriesPromotionsProvider SqlVwZnodeProductsCategoriesPromotionsProvider
		{
			get {return VwZnodeProductsCategoriesPromotionsProvider as SqlVwZnodeProductsCategoriesPromotionsProvider;}
		}
		
		#endregion
		
		
		#region "VwZnodeProductsPortalsProvider"
		
		private SqlVwZnodeProductsPortalsProvider innerSqlVwZnodeProductsPortalsProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="VwZnodeProductsPortals"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override VwZnodeProductsPortalsProviderBase VwZnodeProductsPortalsProvider
		{
			get
			{
				if (innerSqlVwZnodeProductsPortalsProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlVwZnodeProductsPortalsProvider == null)
						{
							this.innerSqlVwZnodeProductsPortalsProvider = new SqlVwZnodeProductsPortalsProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlVwZnodeProductsPortalsProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlVwZnodeProductsPortalsProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlVwZnodeProductsPortalsProvider SqlVwZnodeProductsPortalsProvider
		{
			get {return VwZnodeProductsPortalsProvider as SqlVwZnodeProductsPortalsProvider;}
		}
		
		#endregion
		
		
		#region "VwZnodeProductsPromotionsProvider"
		
		private SqlVwZnodeProductsPromotionsProvider innerSqlVwZnodeProductsPromotionsProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="VwZnodeProductsPromotions"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override VwZnodeProductsPromotionsProviderBase VwZnodeProductsPromotionsProvider
		{
			get
			{
				if (innerSqlVwZnodeProductsPromotionsProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlVwZnodeProductsPromotionsProvider == null)
						{
							this.innerSqlVwZnodeProductsPromotionsProvider = new SqlVwZnodeProductsPromotionsProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlVwZnodeProductsPromotionsProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlVwZnodeProductsPromotionsProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlVwZnodeProductsPromotionsProvider SqlVwZnodeProductsPromotionsProvider
		{
			get {return VwZnodeProductsPromotionsProvider as SqlVwZnodeProductsPromotionsProvider;}
		}
		
		#endregion
		
		
		#region "General data access methods"

		#region "ExecuteNonQuery"
		/// <summary>
		/// Executes the non query.
		/// </summary>
		/// <param name="storedProcedureName">Name of the stored procedure.</param>
		/// <param name="parameterValues">The parameter values.</param>
		/// <returns></returns>
		public override int ExecuteNonQuery(string storedProcedureName, params object[] parameterValues)
		{
			SqlDatabase database = new SqlDatabase(this._connectionString);
			return database.ExecuteNonQuery(storedProcedureName, parameterValues);	
		}

		/// <summary>
		/// Executes the non query.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="storedProcedureName">Name of the stored procedure.</param>
		/// <param name="parameterValues">The parameter values.</param>
		/// <returns></returns>
		public override int ExecuteNonQuery(TransactionManager transactionManager, string storedProcedureName, params object[] parameterValues)
		{
			SqlDatabase database = new SqlDatabase(this._connectionString);
			return database.ExecuteNonQuery(transactionManager.TransactionObject, storedProcedureName, parameterValues);	
		}

		/// <summary>
		/// Executes the non query.
		/// </summary>
		/// <param name="commandWrapper">The command wrapper.</param>
		public override void ExecuteNonQuery(DbCommand commandWrapper)
		{
			SqlDatabase database = new SqlDatabase(this._connectionString);
			database.ExecuteNonQuery(commandWrapper);	
			
		}

		/// <summary>
		/// Executes the non query.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="commandWrapper">The command wrapper.</param>
		public override void ExecuteNonQuery(TransactionManager transactionManager, DbCommand commandWrapper)
		{
			SqlDatabase database = new SqlDatabase(this._connectionString);
			database.ExecuteNonQuery(commandWrapper, transactionManager.TransactionObject);	
		}


		/// <summary>
		/// Executes the non query.
		/// </summary>
		/// <param name="commandType">Type of the command.</param>
		/// <param name="commandText">The command text.</param>
		/// <returns></returns>
		public override int ExecuteNonQuery(CommandType commandType, string commandText)
		{
			SqlDatabase database = new SqlDatabase(this._connectionString);
			return database.ExecuteNonQuery(commandType, commandText);	
		}
		/// <summary>
		/// Executes the non query.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="commandType">Type of the command.</param>
		/// <param name="commandText">The command text.</param>
		/// <returns></returns>
		public override int ExecuteNonQuery(TransactionManager transactionManager, CommandType commandType, string commandText)
		{
			Database database = transactionManager.Database;			
			return database.ExecuteNonQuery(transactionManager.TransactionObject , commandType, commandText);				
		}
		#endregion

		#region "ExecuteDataReader"
		/// <summary>
		/// Executes the reader.
		/// </summary>
		/// <param name="storedProcedureName">Name of the stored procedure.</param>
		/// <param name="parameterValues">The parameter values.</param>
		/// <returns></returns>
		public override IDataReader ExecuteReader(string storedProcedureName, params object[] parameterValues)
		{
			SqlDatabase database = new SqlDatabase(this._connectionString);			
			return database.ExecuteReader(storedProcedureName, parameterValues);	
		}

		/// <summary>
		/// Executes the reader.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="storedProcedureName">Name of the stored procedure.</param>
		/// <param name="parameterValues">The parameter values.</param>
		/// <returns></returns>
		public override IDataReader ExecuteReader(TransactionManager transactionManager, string storedProcedureName, params object[] parameterValues)
		{
			Database database = transactionManager.Database;
			return database.ExecuteReader(transactionManager.TransactionObject, storedProcedureName, parameterValues);	
		}

		/// <summary>
		/// Executes the reader.
		/// </summary>
		/// <param name="commandWrapper">The command wrapper.</param>
		/// <returns></returns>
		public override IDataReader ExecuteReader(DbCommand commandWrapper)
		{
			SqlDatabase database = new SqlDatabase(this._connectionString);			
			return database.ExecuteReader(commandWrapper);	
		}

		/// <summary>
		/// Executes the reader.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="commandWrapper">The command wrapper.</param>
		/// <returns></returns>
		public override IDataReader ExecuteReader(TransactionManager transactionManager, DbCommand commandWrapper)
		{
			Database database = transactionManager.Database;
			return database.ExecuteReader(commandWrapper, transactionManager.TransactionObject);	
		}


		/// <summary>
		/// Executes the reader.
		/// </summary>
		/// <param name="commandType">Type of the command.</param>
		/// <param name="commandText">The command text.</param>
		/// <returns></returns>
		public override IDataReader ExecuteReader(CommandType commandType, string commandText)
		{
			SqlDatabase database = new SqlDatabase(this._connectionString);
			return database.ExecuteReader(commandType, commandText);	
		}
		/// <summary>
		/// Executes the reader.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="commandType">Type of the command.</param>
		/// <param name="commandText">The command text.</param>
		/// <returns></returns>
		public override IDataReader ExecuteReader(TransactionManager transactionManager, CommandType commandType, string commandText)
		{
			Database database = transactionManager.Database;			
			return database.ExecuteReader(transactionManager.TransactionObject , commandType, commandText);				
		}
		#endregion

		#region "ExecuteDataSet"
		/// <summary>
		/// Executes the data set.
		/// </summary>
		/// <param name="storedProcedureName">Name of the stored procedure.</param>
		/// <param name="parameterValues">The parameter values.</param>
		/// <returns></returns>
		public override DataSet ExecuteDataSet(string storedProcedureName, params object[] parameterValues)
		{
			SqlDatabase database = new SqlDatabase(this._connectionString);			
			return database.ExecuteDataSet(storedProcedureName, parameterValues);	
		}

		/// <summary>
		/// Executes the data set.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="storedProcedureName">Name of the stored procedure.</param>
		/// <param name="parameterValues">The parameter values.</param>
		/// <returns></returns>
		public override DataSet ExecuteDataSet(TransactionManager transactionManager, string storedProcedureName, params object[] parameterValues)
		{
			Database database = transactionManager.Database;
			return database.ExecuteDataSet(transactionManager.TransactionObject, storedProcedureName, parameterValues);	
		}

		/// <summary>
		/// Executes the data set.
		/// </summary>
		/// <param name="commandWrapper">The command wrapper.</param>
		/// <returns></returns>
		public override DataSet ExecuteDataSet(DbCommand commandWrapper)
		{
			SqlDatabase database = new SqlDatabase(this._connectionString);			
			return database.ExecuteDataSet(commandWrapper);	
		}

		/// <summary>
		/// Executes the data set.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="commandWrapper">The command wrapper.</param>
		/// <returns></returns>
		public override DataSet ExecuteDataSet(TransactionManager transactionManager, DbCommand commandWrapper)
		{
			Database database = transactionManager.Database;
			return database.ExecuteDataSet(commandWrapper, transactionManager.TransactionObject);	
		}


		/// <summary>
		/// Executes the data set.
		/// </summary>
		/// <param name="commandType">Type of the command.</param>
		/// <param name="commandText">The command text.</param>
		/// <returns></returns>
		public override DataSet ExecuteDataSet(CommandType commandType, string commandText)
		{
			SqlDatabase database = new SqlDatabase(this._connectionString);
			return database.ExecuteDataSet(commandType, commandText);	
		}
		/// <summary>
		/// Executes the data set.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="commandType">Type of the command.</param>
		/// <param name="commandText">The command text.</param>
		/// <returns></returns>
		public override DataSet ExecuteDataSet(TransactionManager transactionManager, CommandType commandType, string commandText)
		{
			Database database = transactionManager.Database;			
			return database.ExecuteDataSet(transactionManager.TransactionObject , commandType, commandText);				
		}
		#endregion

		#region "ExecuteScalar"
		/// <summary>
		/// Executes the scalar.
		/// </summary>
		/// <param name="storedProcedureName">Name of the stored procedure.</param>
		/// <param name="parameterValues">The parameter values.</param>
		/// <returns></returns>
		public override object ExecuteScalar(string storedProcedureName, params object[] parameterValues)
		{
			SqlDatabase database = new SqlDatabase(this._connectionString);			
			return database.ExecuteScalar(storedProcedureName, parameterValues);	
		}

		/// <summary>
		/// Executes the scalar.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="storedProcedureName">Name of the stored procedure.</param>
		/// <param name="parameterValues">The parameter values.</param>
		/// <returns></returns>
		public override object ExecuteScalar(TransactionManager transactionManager, string storedProcedureName, params object[] parameterValues)
		{
			Database database = transactionManager.Database;
			return database.ExecuteScalar(transactionManager.TransactionObject, storedProcedureName, parameterValues);	
		}

		/// <summary>
		/// Executes the scalar.
		/// </summary>
		/// <param name="commandWrapper">The command wrapper.</param>
		/// <returns></returns>
		public override object ExecuteScalar(DbCommand commandWrapper)
		{
			SqlDatabase database = new SqlDatabase(this._connectionString);			
			return database.ExecuteScalar(commandWrapper);	
		}

		/// <summary>
		/// Executes the scalar.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="commandWrapper">The command wrapper.</param>
		/// <returns></returns>
		public override object ExecuteScalar(TransactionManager transactionManager, DbCommand commandWrapper)
		{
			Database database = transactionManager.Database;
			return database.ExecuteScalar(commandWrapper, transactionManager.TransactionObject);	
		}

		/// <summary>
		/// Executes the scalar.
		/// </summary>
		/// <param name="commandType">Type of the command.</param>
		/// <param name="commandText">The command text.</param>
		/// <returns></returns>
		public override object ExecuteScalar(CommandType commandType, string commandText)
		{
			SqlDatabase database = new SqlDatabase(this._connectionString);
			return database.ExecuteScalar(commandType, commandText);	
		}
		/// <summary>
		/// Executes the scalar.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="commandType">Type of the command.</param>
		/// <param name="commandText">The command text.</param>
		/// <returns></returns>
		public override object ExecuteScalar(TransactionManager transactionManager, CommandType commandType, string commandText)
		{
			Database database = transactionManager.Database;			
			return database.ExecuteScalar(transactionManager.TransactionObject , commandType, commandText);				
		}
		#endregion

		#endregion


	}
}
