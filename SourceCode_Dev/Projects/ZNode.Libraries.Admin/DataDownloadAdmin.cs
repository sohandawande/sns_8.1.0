using System;
using System.Data;
using System.Data.OleDb;
using System.Text;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.Admin
{
    /// <summary>
    /// Export Helper Function - Provides method to export the data from Dataset to CSV format.
    /// </summary>
    public class DataDownloadAdmin : ZNodeBusinessBase
    {
        /// <summary>
        ///  Returns string for a Given Dataset Values
        /// </summary>
        /// <param name="ds">DataSet to export.</param>
        /// <param name="exportColumnHeadings">Indicates whether to export column headings or not.</param>
        /// <returns>Returns the "|" delimitted data.</returns>
        public string Export(DataSet ds, bool exportColumnHeadings)
        {
            // By Default Comma as a Delimeter.
            return this.Export(ds, exportColumnHeadings, "|");
        }

        /// <summary>
        ///  Returns string for a Given Dataset Values
        /// </summary>
        /// <param name="ds">DataSet to export.</param>
        /// <param name="exportColumnHeadings">Indicates whether to export column headings or not.</param>
        /// <param name="delimeter">Column delimeter string.</param>
        /// <param name="constraint">Filter constraint.</param>
        /// <returns>Returns the data in CSV format</returns>
        public string Export(DataSet ds, bool exportColumnHeadings, string delimeter, string constraint)
        {
            StringBuilder header = new StringBuilder();
            StringBuilder body = new StringBuilder();
            StringBuilder record = new StringBuilder();

            // If you want column Heading  to be part of the CSV
            if (exportColumnHeadings)
            {
                foreach (DataColumn col in ds.Tables[0].Columns)
                {
                    header.Append(col.ColumnName);
                    header.Append(delimeter);
                }

                header = new StringBuilder(header.ToString().Substring(0, header.Length - 1));
            }

            // Iterate into the rows
            DataRow[] dataRows;
            if (string.IsNullOrEmpty(constraint))
            {
                dataRows = ds.Tables[0].Select();
            }
            else
            {
                dataRows = ds.Tables[0].Select(constraint);
            }

            foreach (DataRow row in dataRows)
            {
                object[] arr = row.ItemArray;

                for (int i = 0; i < arr.Length; i++)
                {
                    if (arr[i].ToString().IndexOf(",") > 0)
                    {
                        record.Append(row[i].ToString());
                        record.Append(delimeter);
                    }
                    else
                    {
                        record.Append(row[i].ToString());
                        record.Append(delimeter);
                    }
                }

                body.Append(record.ToString().Substring(0, record.Length - 1));
                body.Append(Environment.NewLine);

                record = new StringBuilder();
            }

            if (!string.IsNullOrEmpty(body.ToString()))
            {
                if (exportColumnHeadings)
                {
                    return string.Concat(header.ToString(), Environment.NewLine, body.ToString());
                }
                else
                {
                    return body.ToString();
                }
            }
            else
            {
				return string.Concat(header.ToString(), Environment.NewLine);
                //return string.Empty;
            }
        }

        /// <summary>
        ///  Returns string for a Given Dataset Values
        /// </summary>
        /// <param name="ds">DataSet to export</param>
        /// <param name="exportColumnHeadings">Indicates whether to export column headings or not.</param>
        /// <param name="delimeter">Column delimeter string.</param>
        /// <returns>Returns the string data.</returns>
        public string Export(DataSet ds, bool exportColumnHeadings, string delimeter)
        {
            return this.Export(ds, exportColumnHeadings, delimeter, string.Empty);
        }

        /// <summary>
        /// Get the datatable from excel sheet.
        /// </summary>
        /// <param name="filePath">File path to read data from.</param>
        /// <returns>A System.Data.DataTable that contains schema information.</returns>
        public System.Data.DataTable GetDataTable(string filePath)
        {
            string folderPath = System.IO.Path.GetDirectoryName(filePath);
            string fileName = System.IO.Path.GetFileName(filePath);
            System.IO.FileInfo fileInfo = new System.IO.FileInfo(filePath);
            System.Data.DataSet ds = new System.Data.DataSet();
            string workSheetName = "Sheet1";

            if (fileInfo.Extension.ToLower() == ".csv")
            {
                System.Data.OleDb.OleDbConnection connection = new System.Data.OleDb.OleDbConnection("Provider=Microsoft.Jet.OleDb.4.0; Data Source = " + folderPath + "; Extended Properties = \"Text;HDR=YES;FMT=Delimited;\"");

                // Opens a database connection
                connection.Open();

                string query = "SELECT * FROM [" + fileName + "]";
                System.Data.OleDb.OleDbDataAdapter adapter = new System.Data.OleDb.OleDbDataAdapter(query, connection);

                // Adds or refreshes rows in the dataset
                adapter.Fill(ds);

                // Closes the connectionection
                connection.Close();
            }
            else if (fileInfo.Extension.ToLower() == ".xls")
            {
                // Create the connection string For to read file
                string strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + folderPath + "\\" + fileName + ";Extended Properties='Excel 8.0;HDR=Yes;Imex=1;'";

                // Create Instance of Connection and Command Object
                System.Data.OleDb.OleDbConnection objConn = new System.Data.OleDb.OleDbConnection(strConn);

                // Establishing the connection
                objConn.Open();

                // Get the data table containg the schema guid.
                DataTable dt = objConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });

                if (dt != null)
                {
                    // Loop through the sheet name
                    foreach (DataRow row in dt.Rows)
                    {
                        workSheetName = row["TABLE_NAME"].ToString();

                        if (workSheetName.Contains("$"))
                        {
                            break;
                        }
                    }
                }

                System.Data.OleDb.OleDbDataAdapter command = new System.Data.OleDb.OleDbDataAdapter("SELECT * FROM [" + workSheetName + "]", objConn);

                command.Fill(ds, workSheetName);

                // Closes the connection
                objConn.Close();
            }

            return ds.Tables[0];
        }
    }
}