using System;
using System.Data;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.Admin
{
    /// <summary>
    /// This class manages multifront and portal settings
    /// </summary>
    public class StoreSettingsAdmin : ZNodeBusinessBase
    {
        #region General Store Settings
        /// <summary>
        /// Updates portal settings
        /// </summary>
        /// <param name="portal">Portal object to update.</param>
        /// <returns>Returns true if udpated otherwise false.</returns>
        public bool Update(Portal portal)
        {
            PortalService portalService = new PortalService();
            bool isUpdated = portalService.Update(portal);

            return isUpdated;
        }

        /// <summary>
        /// Get store settings by portal Id
        /// </summary>
        /// <param name="portalId">Portal Id to get the portal object.</param>
        /// <returns>Returns the Portal object.</returns>
        public Portal GetByPortalId(int portalId)
        {
            PortalService portalService = new PortalService();
            return portalService.GetByPortalID(portalId);
        }

        /// <summary>
        /// Get all store settings 
        /// </summary>        
        /// <returns>Returns all store settings.</returns>
        public TList<Portal> Getall()
        {
            PortalService portalService = new PortalService();
            return portalService.GetAll();
        }

        /// <summary>
        /// Delete portal setting by portal Id.
        /// </summary>
        /// <param name="portalId">Portal Id to delete the portal object.</param>
        /// <returns>Returns true if deleted otherwise false.</returns>
        public bool DeletePortal(int portalId)
        {
            PortalService portalService = new PortalService();
            bool isDeleted = portalService.Delete(portalId);

            return isDeleted;
        }
        #endregion

        #region Payment Settings

        /// <summary>
        /// Get all payment settings for a portal
        /// </summary>
        /// <returns>Returns the payment settings dataset.</returns>
        public DataSet GetAllPaymentSettings()
        {
            StoreSettingsHelper storeSettingsHelper = new StoreSettingsHelper();
            return storeSettingsHelper.GetAllPaymentSettings();
        }

        /// <summary>
        /// Get payment settings by profile Id.
        /// </summary>
        /// <param name="profileId">Profile Id to get the portal settings.</param>
        /// <returns>Retuns the payment settings for the profile as datset.</returns>
        public DataSet GetAllPaymentSettings(int profileId)
        {
            StoreSettingsHelper storeSettingsHelper = new StoreSettingsHelper();
            return storeSettingsHelper.GetAllPaymentSettings(profileId);
        }

        /// <summary>
        /// Get a specific payment setting
        /// </summary>
        /// <param name="paymentSettingId">Payment setting Id to get the payment setting object.</param>
        /// <returns>Returns the PaymentSetting object.</returns>
        public PaymentSetting GetPaymentSettingByID(int paymentSettingId)
        {
            PaymentSettingService paymentSettingService = new PaymentSettingService();
            return paymentSettingService.GetByPaymentSettingID(paymentSettingId);
        }

        /// <summary>
        /// Get the list of gateways
        /// </summary>
        /// <returns>Returns the list of GateWay object.</returns>
        public TList<Gateway> GetGateways()
        {
            GatewayService gatewayService = new GatewayService();
            return gatewayService.GetAll();
        }

        /// <summary>
        /// Getthe payment type by payment type Id.
        /// </summary>
        /// <param name="paymentTypeId">Payment type Id to get the payment type object.</param>
        /// <returns>Returns the PaymentType object.</returns>
        public PaymentType GetPaymentTypeById(int paymentTypeId)
        {
            PaymentTypeService paymentTypeService = new PaymentTypeService();
            return paymentTypeService.GetByPaymentTypeID(paymentTypeId);
        }

        /// <summary>
        /// Get all profiles 
        /// </summary>
        /// <returns>Returns list of Profile object.</returns>
        public TList<Profile> GetProfiles()
        {
            ProfileService profileService = new ProfileService();
            return profileService.GetAll();
        }

        /// <summary>
        /// Get all the payment types
        /// </summary>
        /// <returns>Returns list of PaymentType object.</returns>
        public TList<PaymentType> GetPaymentTypes()
        {
            PaymentTypeService paymentTypeService = new PaymentTypeService();
            return paymentTypeService.GetAll();
        }

        /// <summary>
        /// Update a payment setting.
        /// </summary>
        /// <param name="paymentSetting">PaymentSetting object to update.</param>
        /// <returns>Returns true if udpated otherwise false.</returns>
        public bool UpdatePaymentSetting(PaymentSetting paymentSetting)
        {
            PaymentSettingService paymentSettingService = new PaymentSettingService();
            return paymentSettingService.Update(paymentSetting);
        }

        /// <summary>
        /// Add a new payment setting
        /// </summary>
        /// <param name="paymentSetting">PaymentSetting object to insert.</param>        
        /// <returns>Returns true if inserted otherwise false.</returns>
        public bool AddPaymentSetting(PaymentSetting paymentSetting)
        {
            PaymentSettingService paymentSettingService = new PaymentSettingService();
            return paymentSettingService.Insert(paymentSetting);
        }

        /// <summary>
        /// Delete a payment setting by Id.
        /// </summary>
        /// <param name="paymentSettingId">Payment setting Id to delete the payment setting object.</param>
        /// <returns>Returns true if deleted otherwise false.</returns>
        public bool DeletePaymentSetting(int paymentSettingId)
        {
            PaymentSettingService paymentSettingService = new PaymentSettingService();
            return paymentSettingService.Delete(paymentSettingId);
        }

        /// <summary>
        /// Check if a setting with the paymenttype atleast one for this profile 
        /// </summary>
        /// <param name="profileId">Profile Id to check.</param>
        /// <returns>Returns true if exists otherwise false.</returns>
        public bool CheckPaymentSettingProfile(int profileId)
        {
            PaymentSettingService paymentSettingService = new PaymentSettingService();
            TList<PaymentSetting> paymentSettingList = new TList<PaymentSetting>();

            // Get all profile types
            paymentSettingList = paymentSettingService.GetAll();

            // Apply filter based on the profile Id
            paymentSettingList.ApplyFilter(delegate(ZNode.Libraries.DataAccess.Entities.PaymentSetting pmt)
            {
                return pmt.ProfileID == null || pmt.ProfileID == profileId;
            });

            if (paymentSettingList.Count > 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Check if a setting with the paymenttype already exists for this profile
        /// </summary>
        /// <param name="profileId">Profile Id to check.</param>
        /// <param name="paymentTypeId">Payment type Id to check.</param>
        /// <returns>Returns true if exists otherwise false.</returns>
        public bool PaymentSettingExists(int profileId, int paymentTypeId)
        {
            PaymentSettingService paymentSettingService = new PaymentSettingService();
            TList<PaymentSetting> paymentSettingList = new TList<PaymentSetting>();

            if (profileId > -1)
            {
                paymentSettingList = paymentSettingService.GetByProfileIDPaymentTypeID(profileId, paymentTypeId);
            }
            else
            {
                paymentSettingList = paymentSettingService.GetAll();
                if (paymentSettingList != null)
                {
                    paymentSettingList.ApplyFilter(delegate(ZNode.Libraries.DataAccess.Entities.PaymentSetting pmt)
                    {
                        return pmt.ProfileID == null && pmt.PaymentTypeID == paymentTypeId;
                    });
                }
            }

            if (paymentSettingList.Count == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        /// <summary>
        /// Check if a setting with the paymenttype already exists for this profile
        /// </summary>
        /// <param name="profileId">Profile Id to check.</param>
        /// <param name="paymentTypeId">Payment type Id to check.</param>
        /// <returns>Returns true if exists otherwise false.</returns>
        public bool PaymentSettingExists(int profileId, int paymentTypeId, int gatewayId, int paymentsettingId)
        {
            PaymentSettingService paymentSettingService = new PaymentSettingService();
            TList<PaymentSetting> paymentSettingList = new TList<PaymentSetting>();

            if (profileId > -1)
            {
                paymentSettingList = paymentSettingService.GetByProfileIDPaymentTypeID(profileId, paymentTypeId);
            }
            else
            {
                paymentSettingList = paymentSettingService.GetAll();
                paymentSettingList.ApplyFilter(delegate(ZNode.Libraries.DataAccess.Entities.PaymentSetting pmt)
                {
                    return pmt.ProfileID == null && pmt.PaymentTypeID == paymentTypeId;
                });
            }

            if (paymentSettingList.Count == 0)
            {
                return false;
            }
            else
            {

                foreach (PaymentSetting pmtsetting in paymentSettingList)
                {
                    if (pmtsetting.PaymentTypeID == (int)ZNode.Libraries.ECommerce.Entities.PaymentType.CREDIT_CARD && pmtsetting.GatewayTypeID == gatewayId && pmtsetting.PaymentSettingID != paymentsettingId)
                        return true;
                    if (pmtsetting.PaymentSettingID != paymentsettingId && pmtsetting.PaymentTypeID == (int)ZNode.Libraries.ECommerce.Entities.PaymentType.CREDIT_CARD)
                    {
                        if (pmtsetting.GatewayTypeID == gatewayId || pmtsetting.ActiveInd)
                        {
                            pmtsetting.ActiveInd = false;
                            paymentSettingService.Update(pmtsetting);
                        }
                    }
                    else if (pmtsetting.PaymentSettingID != paymentsettingId && pmtsetting.PaymentTypeID != (int)ZNode.Libraries.ECommerce.Entities.PaymentType.CREDIT_CARD)
                    {
                        return true;
                    }

                }
                return false;
            }
        }

        #endregion

        #region Currency Type Settings
        /// <summary>
        /// Returns all currency types.
        /// </summary>
        /// <returns>Returns list of CurrencyType object.</returns>
        public TList<CurrencyType> GetAll()
        {
            ZNode.Libraries.DataAccess.Service.CurrencyTypeService currencyTypeSettings = new ZNode.Libraries.DataAccess.Service.CurrencyTypeService();
            return currencyTypeSettings.GetAll();
        }

        /// <summary>
        /// Returns currency type by Id
        /// </summary>
        /// <param name="currencyTypeId">Currency type Id to get the currency type object.</param>
        /// <returns>Returns the CurrencyType object.</returns>
        public CurrencyType GetByCurrencyTypeID(int currencyTypeId)
        {
            ZNode.Libraries.DataAccess.Service.CurrencyTypeService currencyTypeSettings = new ZNode.Libraries.DataAccess.Service.CurrencyTypeService();
            return currencyTypeSettings.GetByCurrencyTypeID(currencyTypeId);
        }

        /// <summary>
        /// Update the currency type.
        /// </summary>
        /// <param name="currencyType">CurrencyType object to udpate.</param>
        /// <returns>Returns true if updated otherwise false.</returns>
        public bool UpdateCurrencyType(CurrencyType currencyType)
        {
            ZNode.Libraries.DataAccess.Service.CurrencyTypeService currencyTypeSettings = new ZNode.Libraries.DataAccess.Service.CurrencyTypeService();
            return currencyTypeSettings.Update(currencyType);
        }

        /// <summary>
        /// Return Currency Symbol for this currencyTypeId
        /// According to the Culture settings,the currency symbol will be return to calling emthod
        /// </summary>
        /// <param name="currencyTypeId">Currency type Id to get the curreny type object.</param>
        /// <returns>Returns the currency symbol.</returns>
        public string GetCurrencySymbol(int currencyTypeId)
        {
            ZNode.Libraries.DataAccess.Service.CurrencyTypeService currencyTypeSettings = new ZNode.Libraries.DataAccess.Service.CurrencyTypeService();
            ZNode.Libraries.DataAccess.Entities.CurrencyType currencyType = currencyTypeSettings.GetByCurrencyTypeID(currencyTypeId);

            if (currencyType != null)
            {
                return currencyType.Name;
            }

            // Default currency symbol "$" for this culture setting will be return 
            return "en-US";
        }
        #endregion

        #region Store Settings
        /// <summary>
        /// Get all stores.
        /// </summary>
        /// <returns>Returns list of Portal object.</returns>
        public TList<Portal> GetAllStores()
        {
            ZNode.Libraries.DataAccess.Service.PortalService settings = new PortalService();
            TList<Portal> portalList = settings.GetAll();

            return portalList;
        }

        /// <summary>
        /// Add a new store setting and clear the cache manager.
        /// </summary>       
        /// <param name="portal">Portal object to insert.</param>
        /// <returns>Returns true if inserted otherwise false.</returns>
        public bool InsertStore(ZNode.Libraries.DataAccess.Entities.Portal portal)
        {
            PortalService portalService = new PortalService();
            bool isAdded = portalService.Insert(portal);

            // Clear all cache memory.
            CacheManager.Clear();

            return isAdded;
        }

        /// <summary>
        /// Update the store and clear the cache manager.
        /// </summary>       
        /// <param name="portal">Portal object to udpate.</param>
        /// <returns>Returns true if udpated otherwise false.</returns>
        public bool UpdateStore(ZNode.Libraries.DataAccess.Entities.Portal portal)
        {
            PortalService portalService = new PortalService();
            bool isUpdated = portalService.Update(portal);

            // Clear all cache memory.
            CacheManager.Clear();

            return isUpdated;
        }
        #endregion

        #region CopyStore Settings
        /// <summary>
        /// Copy store details from existing store and clear the cache manager.
        /// </summary>
        /// <param name="portalId">Portal Id to get the souce Portal object.</param>
        /// <returns>Returns true if copied otherwise false.</returns>
        public bool CopyStoreByPortalID(int portalId)
        {
            bool isAdded = false;

            // Portal Service
            PortalService portalService = new PortalService();
            Portal oldportal = portalService.GetByPortalID(portalId);
            Portal portal = oldportal.Copy();
            portal.CompanyName = "Copy of " + portal.CompanyName;
            portal.StoreName = "Copy of " + portal.StoreName;
            portal.PortalID = 0;
            isAdded = portalService.Insert(portal);

            if (isAdded)
            {
                // Portal Profile Service
                PortalProfileService portalProfileService = new PortalProfileService();
                TList<PortalProfile> portalProfileList = portalProfileService.GetByPortalID(portalId);
                if (portalProfileList.Count > 0)
                {
                    // Add a list of Profile
                    foreach (PortalProfile portalProfile in portalProfileList)
                    {
                        portalProfile.PortalID = portal.PortalID;
                        isAdded = portalProfileService.Insert(portalProfile);
                    }
                }

                // Catalog Service
                PortalCatalogService portalCatalogService = new PortalCatalogService();
                PortalCatalog portalCatalog = new PortalCatalog();
                TList<PortalCatalog> portalCatalogList = portalCatalogService.GetByPortalID(portalId);

                // Add a Catalog
                if (portalCatalogList.Count > 0)
                {
                    // Add a list of Profile
                    foreach (PortalCatalog portalcatalog in portalCatalogList)
                    {
                        portalCatalog.PortalID = portal.PortalID;
                        portalCatalog.CatalogID = portalcatalog.CatalogID;
                        portalCatalog.LocaleID = portalcatalog.LocaleID;
                        portalCatalog.ThemeID = portalcatalog.ThemeID;
                        portalCatalog.CSSID = portalcatalog.CSSID;
                        isAdded = portalCatalogService.Insert(portalCatalog);

                        // Create Custom Messages
                        this.CreateMessage(portal.PortalID.ToString(), portalcatalog.LocaleID.ToString());
                    }
                }

                // Portal Country Service
                PortalCountryService portalCountryService = new PortalCountryService();
                PortalCountry portalCountry = new PortalCountry();
                TList<PortalCountry> portalCountryList = portalCountryService.GetByPortalID(portalId);

                // Add PortalCountry List
                foreach (PortalCountry country in portalCountryList)
                {
                    country.PortalID = portal.PortalID;
                    isAdded = portalCountryService.Insert(country);
                }

                // Add Default Content Pages
                ContentPageService contentPageService = new ContentPageService();
                ContentPageAdmin contentPageAdmin = new ContentPageAdmin();
                ContentPage contentPage = new ContentPage();
                TList<ContentPage> contentPageList = contentPageService.GetByPortalID(portalId);

                foreach (ContentPage page in contentPageList)
                {
                    contentPage.PortalID = portal.PortalID;
                    contentPage.LocaleId = page.LocaleId;
                    contentPage.SEOURL = null;
                    contentPage.Name = page.Name;
                    contentPage.MasterPageID = page.MasterPageID;
                    contentPage.ActiveInd = page.ActiveInd;
                    contentPage.AllowDelete = page.AllowDelete;

                    contentPageAdmin.AddPage(contentPage, string.Empty, portal.PortalID, page.LocaleId.ToString(), System.Web.HttpContext.Current.User.Identity.Name, null, false);
                }
            }

            // Clear all cache memory.
            CacheManager.Clear();

            return isAdded;
        }

        /// <summary>
        ///  Delete the portal by portal Id.
        /// </summary>
        /// <param name="portalId">Portal Id to delete the portal object.</param>
        /// <returns>Returns true if deleted otherwise false.</returns>
        public bool DeleteByPortalId(int portalId)
        {
            StoreSettingsHelper storeSettingsHelper = new StoreSettingsHelper();
            return storeSettingsHelper.DeleteByPortalId(portalId);
        }

        /// <summary>
        /// Create Message Config dynamically based on store.
        /// </summary>
        /// <param name="portalId">Portal Id to check.</param>
        /// <param name="localeId">Locale Id to create this locale message.</param>
        public void CreateMessage(string portalId, string localeId)
        {
            MessageConfigAdmin messageConfigAdmin = new MessageConfigAdmin();
            TList<MessageConfig> messageConfigList = messageConfigAdmin.GetMessagesConfigByPortalIdLocaleId(0, Convert.ToInt32(localeId));
            messageConfigList.Sort("PortalID");

            TList<MessageConfig> resultList = new TList<MessageConfig>();

            if (messageConfigList.Count > 0)
            {
                resultList = messageConfigList.FindAll(delegate(MessageConfig mconfigEntity) { return mconfigEntity.PortalID == 0 && Convert.ToInt32(mconfigEntity.LocaleID) == Convert.ToInt32(localeId); });
            }
            else
            {
                resultList = messageConfigAdmin.GetMessagesConfigByPortalIdLocaleId(0, 43);
            }

            MessageConfig messageconfig = new MessageConfig();

            int messageId = 0;
            if (resultList.Count > 0)
            {
                foreach (MessageConfig message in resultList)
                {
                    messageconfig.Key = message.Key;
                    messageconfig.Description = message.Description;
                    messageconfig.Value = message.Value;
                    messageconfig.MessageTypeID = (int)ZNodeMessageType.Message;
                    messageconfig.PortalID = Convert.ToInt32(portalId);
                    messageconfig.LocaleID = Convert.ToInt32(localeId);
                    messageConfigAdmin.Insert(messageconfig, out messageId);
                }
            }
        }

        /// <summary>
        /// Delete message config by portal Id.
        /// </summary>
        /// <param name="portalId">Portal Id to delete all message config for the portal.</param>
        public void DeleteCustomMessage(string portalId)
        {
            MessageConfigService mconfigservice = new MessageConfigService();
            MessageConfigAdmin messageConfigAdmin = new MessageConfigAdmin();
            TList<MessageConfig> messageConfigList = messageConfigAdmin.GetMessagesConfigByPortalIdLocaleId(Convert.ToInt32(portalId), 0);

            if (messageConfigList.Count > 0)
            {
                for (int index = 0; index < messageConfigList.Count; index++)
                {
                    if (messageConfigList[index]["PortalID"] == portalId)
                    {
                        mconfigservice.Delete(Convert.ToInt32(messageConfigList[index]["MessageID"]));
                    }
                }
            }
        }

        /// <summary>
        /// Delete message config for the store and particular locale.
        /// </summary>
        /// <param name="portalId">Portal Id to delete the message config.</param>
        /// <param name="localeId">Locale Id to delete the message config.</param>
        public void DeleteCustomMessage(string portalId, string localeId)
        {
            MessageConfigService mconfigservice = new MessageConfigService();
            MessageConfigAdmin messageConfigAdmin = new MessageConfigAdmin();
            TList<MessageConfig> messageConfigList = messageConfigAdmin.GetMessagesConfigByPortalIdLocaleId(Convert.ToInt32(portalId), Convert.ToInt32(localeId));

            if (messageConfigList.Count > 0)
            {
                for (int index = 0; index < messageConfigList.Count; index++)
                {
                    if (messageConfigList[index]["PortalID"] == portalId)
                    {
                        mconfigservice.Delete(Convert.ToInt32(messageConfigList[index]["MessageID"]));
                    }
                }
            }
        }
        #endregion
    }
}
