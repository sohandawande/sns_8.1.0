using System;
using System.Data;
using System.IO;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;
using Znode.Engine.Shipping;

namespace ZNode.Libraries.Admin
{
    /// <summary>
    /// Provides methods  to manage shipping options
    /// </summary>
    public class ShippingAdmin : ZNodeBusinessBase
    {
        private string _ErrorCode = "0";
        private string _ErrorDescription = string.Empty;

        #region Public Properties
        /// <summary>
        /// Gets or sets the shipping error code.
        /// </summary>
        public string ErrorCode
        {
            get { return this._ErrorCode; }
            set { this._ErrorCode = value; }
        }

        /// <summary>
        /// Gets or sets the shipping error description.
        /// </summary>
        public string ErrorDescription
        {
            get { return this._ErrorDescription; }
            set { this._ErrorDescription = value; }
        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Get all the shipping options for this portal
        /// </summary>        
        /// <returns>Returns the shipping options dataset.</returns>
        public DataSet GetAllShippingOptions()
        {
            ZNode.Libraries.DataAccess.Custom.ShippingHelper shippingHelper = new ZNode.Libraries.DataAccess.Custom.ShippingHelper();
            DataSet shippingTypeDataSet = shippingHelper.GetAllShippingOptions();

            return shippingTypeDataSet;
        }

        /// <summary>
        /// Get all shipping options.
        /// </summary>
        /// <returns>Returns list of Shipping option object.</returns>
        public TList<ZNode.Libraries.DataAccess.Entities.Shipping> GetAll()
        {
            ZNode.Libraries.DataAccess.Service.ShippingService shippingService = new ZNode.Libraries.DataAccess.Service.ShippingService();
            TList<ZNode.Libraries.DataAccess.Entities.Shipping> shippingList = shippingService.GetAll();

            return shippingList;
        }

        /// <summary>
        /// Get a shipping option by Id.
        /// </summary>
        /// <param name="shippingId">Shipping option Id to get the shipping object.</param>
        /// <returns>Returns the Shipping object.</returns>
        public ZNode.Libraries.DataAccess.Entities.Shipping GetShippingOptionById(int shippingId)
        {
            ZNode.Libraries.DataAccess.Service.ShippingService shippingService = new ZNode.Libraries.DataAccess.Service.ShippingService();
            ZNode.Libraries.DataAccess.Entities.Shipping shipping = shippingService.GetByShippingID(shippingId);

            return shipping;
        }

        /// <summary>
        /// Add shipping option
        /// </summary>
        /// <param name="shipping">Shipping object to insert.</param>
        /// <returns>Returns true if inserted otherwise false.</returns>
        public bool AddShippingOption(ZNode.Libraries.DataAccess.Entities.Shipping shipping)
        {
            ZNode.Libraries.DataAccess.Service.ShippingService shippingService = new ZNode.Libraries.DataAccess.Service.ShippingService();
            return shippingService.Insert(shipping);
        }

        /// <summary>
        /// Update shipping option
        /// </summary>
        /// <param name="shipping">Shipping object to insert.</param>
        /// <returns>Returns true if udpated otherwise false.</returns>
        public bool UpdateShippingOption(ZNode.Libraries.DataAccess.Entities.Shipping shipping)
        {
            ZNode.Libraries.DataAccess.Service.ShippingService shippingService = new ZNode.Libraries.DataAccess.Service.ShippingService();
            return shippingService.Update(shipping);
        }

        /// <summary>
        /// Delete the shipping option by shipping option.
        /// </summary>
        /// <param name="shipping">Shipping object to delete.</param>
        /// <returns>Returns true if deleted otherwise false.</returns>
        public bool DeleteShippingOption(ZNode.Libraries.DataAccess.Entities.Shipping shipping)
        {
            ZNode.Libraries.DataAccess.Service.ShippingService shippingService = new ZNode.Libraries.DataAccess.Service.ShippingService();
            return shippingService.Delete(shipping);
        }

        /// <summary>
        /// Get the list of shipping service codes
        /// </summary>
        /// <param name="shippingTypeId">Shipping type Id to get the list of shipping service codes.</param>
        /// <returns>Returns list of ShippingServiceCode object.</returns>
        public DataSet GetShippingServiceCodes(int shippingTypeId)
        {
            ShippingServiceCodeService shippingService = new ShippingServiceCodeService();
            TList<ShippingServiceCode> serviceCodeList = shippingService.GetByShippingTypeID(shippingTypeId);

            return serviceCodeList.ToDataSet(true);
        }

        /// <summary>
        /// Get all shipping types.
        /// </summary>
        /// <returns>Returns the list of ShippingType object.</returns>
        public TList<ShippingType> GetShippingTypes()
        {
            ShippingTypeService shippingTypeService = new ShippingTypeService();
            return shippingTypeService.GetAll();
        }

        /// <summary>
        /// Get the list of destination countries
        /// </summary>
        /// <returns>Returns the list of shipping destination Country object.</returns>
        public TList<Country> GetDestinationCountries()
        {
            CountryService countryService = new CountryService();
            TList<ZNode.Libraries.DataAccess.Entities.Country> countries = countryService.GetAll();

            // Add a default country
            Country cty = new Country();
            cty.Name = "All Countries";
            cty.Code = "*";
            cty.DisplayOrder = 0;
            countries.Add(cty);

            // Sort
            countries.Sort("DisplayOrder,Name");

            return countries;
        }

        /// <summary>
        /// Get shipping type by Id.
        /// </summary>
        /// <param name="shippingTypeId">Shipping type Id to get the shipping type name.</param>
        /// <returns>Returns the shipping type name.</returns>
        public string GetShippingTypeName(int shippingTypeId)
        {
            ShippingTypeService shippingTypeService = new ShippingTypeService();
            return shippingTypeService.GetByShippingTypeID(shippingTypeId).Name;
        }

        /// <summary>
        /// Get profile name
        /// </summary>
        /// <param name="profileId">Profile Id to get the profile object.</param>
        /// <returns>Returns the profile name.</returns>
        public string GetProfileName(int profileId)
        {
            ProfileService profileService = new ProfileService();
            return profileService.GetByProfileID(profileId).Name;
        }

        /// <summary>
        /// Get the shipping rules for an option
        /// </summary>
        /// <param name="shippingId">Shipping Id to get the shipping tule dataset.</param>
        /// <returns>Returns the shipping rules dataset.</returns>
        public DataSet GetShippingRules(int shippingId)
        {
            ZNode.Libraries.DataAccess.Custom.ShippingHelper shippingHelper = new ZNode.Libraries.DataAccess.Custom.ShippingHelper();
            DataSet shippingRulesDataSet = shippingHelper.GetShippingRules(shippingId);

            return shippingRulesDataSet;
        }

        /// <summary>
        /// Get a shipping rule
        /// </summary>
        /// <param name="shippingRuleId">Shipping rule type Id to get the shipping rule type object.</param>
        /// <returns>Returns the ShippingRule object.</returns>
        public ShippingRule GetShippingRule(int shippingRuleId)
        {
            ShippingRuleService shippingRuleService = new ShippingRuleService();
            return shippingRuleService.GetByShippingRuleID(shippingRuleId);
        }

        /// <summary>
        /// Get all shipping rule types
        /// </summary>
        /// <returns>Returns the list of ShippingRuleType object.</returns>
        public TList<ShippingRuleType> GetShippingRuleTypes()
        {
            ShippingRuleTypeService shippingRuleTypeService = new ShippingRuleTypeService();
            return shippingRuleTypeService.GetAll();
        }

        /// <summary>
        /// Add a shipping rule
        /// </summary>
        /// <param name="shippingRule">ShippingRule object to add.</param>
        /// <returns>Returns true if inserted otherwise false.</returns>
        public bool AddShippingRule(ShippingRule shippingRule)
        {
            ZNode.Libraries.DataAccess.Service.ShippingRuleService shippingRuleService = new ZNode.Libraries.DataAccess.Service.ShippingRuleService();
            return shippingRuleService.Insert(shippingRule);
        }

        /// <summary>
        /// Update shipping rule
        /// </summary>
        /// <param name="shippingRule">ShippingRule object to update.</param>
        /// <returns>Returns true if updated otherwise false.</returns>
        public bool UpdateShippingRule(ShippingRule shippingRule)
        {
            ZNode.Libraries.DataAccess.Service.ShippingRuleService shippingRuleService = new ZNode.Libraries.DataAccess.Service.ShippingRuleService();
            return shippingRuleService.Update(shippingRule);
        }

        /// <summary>
        /// Delete shipping rule by shipping rule object.
        /// </summary>
        /// <param name="shippingRule">ShippingRule object to delete.</param>
        /// <returns>Returns true if deleted otherwise false.</returns>
        public bool DeleteShippingRule(ShippingRule shippingRule)
        {
            ZNode.Libraries.DataAccess.Service.ShippingRuleService shippingRuleService = new ZNode.Libraries.DataAccess.Service.ShippingRuleService();
            return shippingRuleService.Delete(shippingRule);
        }

        /// <summary>
        /// Gets the shipping type Id.
        /// </summary>
        /// <param name="shippingId">Shipping Id to get the shipping type Id.</param>
        /// <returns>Returns the shipping type Id.</returns>
        public int GetShippingTypeId(int shippingId)
        {
            ZNode.Libraries.DataAccess.Custom.ShippingHelper helper = new ZNode.Libraries.DataAccess.Custom.ShippingHelper();
            int returnId = helper.GetShippingTypeId(shippingId);

            return returnId;
        }

		/// <summary>
		/// Get the shipping type Id by class name.
		/// </summary>
		/// <param name="className">Class name to get the shipping type.</param>
		/// <returns>Returns the top 1 shipping type id. </returns>
		public int GetShippingTypeId(string className)
		{
			TList<ShippingType> shippingTypeList = DataRepository.ShippingTypeProvider.Find("ClassName = '" + className + "'");

			if (shippingTypeList.Count > 0)
			{
				shippingTypeList.Sort("ShippingTypeID Desc");

				return shippingTypeList[0].ShippingTypeID;
			}

			return 0;
		}
        
        /// <summary>
        /// Estimate the package size.
        /// </summary>
        /// <param name="orderItems">List of order line items.</param>
        /// <param name="height">Estimated package height.</param>
        /// <param name="width">Estimated package width.</param>
        /// <param name="length">Estimated package length.</param>
        /// <param name="weight">Estimated package weight.</param>
        public void EstimatePackageSize(TList<OrderLineItem> orderItems, out decimal height, out decimal width, out decimal length, out decimal weight)
        {
            TList<Product> productList = new TList<Product>();
            TList<AddOnValue> addOnValueList = new TList<AddOnValue>();

            foreach (OrderLineItem lineItem in orderItems)
            {
                Product product = this.GetProductByLineItem(lineItem);
                if (product != null)
                {
                    for (int quantityIndex = 1; quantityIndex <= lineItem.Quantity.Value; quantityIndex++)
                    {
                        productList.Add(product);
                    }
                }
                else
                {
                    AddOnValue addOnValue = this.GetAddonValueByLineItem(lineItem);
                    if (addOnValue != null)
                    {
                        for (int quantityIndex = 1; quantityIndex <= lineItem.Quantity.Value; quantityIndex++)
                        {
                            addOnValueList.Add(addOnValue);
                        }
                    }
                }
            }

            ZnodeShippingPackage EstimatedPackage = new ZnodeShippingPackage(productList, addOnValueList);

            // Set the out parameter value.
            height = EstimatedPackage.Height;
            length = EstimatedPackage.Length;
            width = EstimatedPackage.Width;
            weight = EstimatedPackage.Weight;
        }
        
        #endregion

		/// <summary>
		/// Check if the shipping already exists in DB
		/// </summary>
		/// <param name="shippingType"></param>
		/// <param name="profileID"></param>
		/// <param name="shippingServiceCode"></param>
		/// <returns></returns>
		public bool IsDuplicateShipping(int shippingID, int profileID, string shippingServiceCode)
		{
			var shippingService = new ShippingService();
			TList<DataAccess.Entities.Shipping> shippinglist;

			if (profileID > -1)
			{
				shippinglist = shippingService.GetByProfileID(profileID);

					if (shippinglist != null)
					{

						shippinglist.ApplyFilter(shipping => shipping.ProfileID == profileID && shipping.ShippingCode == shippingServiceCode && shipping.ShippingID != shippingID);
					}
			}
			else
			{
				shippinglist = shippingService.GetAll();

				if (shippinglist != null)
				{
					shippinglist.ApplyFilter(shipping => shipping.ProfileID == null && shipping.ShippingCode == shippingServiceCode && shipping.ShippingID != shippingID);
				}
			}

			return shippinglist != null && shippinglist.Count != 0;
		}


        #region Helper Methods
        /// <summary>
        /// Get the product by order line item object.
        /// </summary>
        /// <param name="item">Order line item to get the product object.</param>
        /// <returns>Returns the Product object.</returns>
        private Product GetProductByLineItem(OrderLineItem item)
        {
            Product product = null;
            if (item != null)
            {
                ProductService productService = new ProductService();
                string filterExpression = "ProductNum='" + item.ProductNum + "' AND Name='" + item.Name + "'";
                TList<Product> productList = productService.Find(filterExpression);

                if (productList != null && productList.Count > 0)
                {
                    product = productList[0];
                }
            }

            return product;
        }

        /// <summary>
        /// Get addon value by order line item object.
        /// </summary>
        /// <param name="item">Order line item to get the addon value object.</param>
        /// <returns>Returns the AddOnValue object.</returns>
        private AddOnValue GetAddonValueByLineItem(OrderLineItem item)
        {
            if (item != null)
            {
                ZNode.Libraries.DataAccess.Service.AddOnValueService addonService = new AddOnValueService();
                TList<AddOnValue> listAddOnValue = addonService.GetBySKU(item.SKU);

                if (listAddOnValue.Count > 0)
                {
                    return listAddOnValue[0];
                }
            }

            return null;
        }

        /// <summary>
        /// Update the order line item with tracking and shipping charge.
        /// </summary>
        /// <param name="lineItem">Order line item object to update.</param>
        /// <param name="trackingNumber">Tracking number value to update.</param>
        /// <param name="shippingCharge">Shipping charge value to update.</param>
        private void UpdateLineItem(OrderLineItem lineItem, string trackingNumber, decimal shippingCharge)
        {
            OrderLineItemService service = new OrderLineItemService();
            lineItem.TrackingNumber = trackingNumber;
            lineItem.ShipDate = DateTime.Now;
            lineItem.ShippingCost = shippingCharge;
            lineItem.AutoGeneratedTracking = true;
            service.Update(lineItem);
        }

        #endregion
    }
}
