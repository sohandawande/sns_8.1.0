using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.SEO;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.Admin
{
    /// <summary>
    /// Represents the UrlRedirectAdmin class.
    /// </summary>
    public class UrlRedirectAdmin
    {
        /// <summary>
        /// Get complete collection of url redirect entities.
        /// </summary>
        /// <param name="oldUrl">Old url string.</param>
        /// <param name="newUrl">New url string.</param>
        /// <param name="enabled">Enabled indicates 0 or 1.</param>
        /// <returns>Returns the list of UrlRedirect object.</returns>
        public TList<UrlRedirect> Search(string oldUrl, string newUrl, string enabled)
        {
            UrlRedirectQuery query = new UrlRedirectQuery();
            query.Append(UrlRedirectColumn.OldUrl, "~/" + oldUrl + "%");
            query.Append(UrlRedirectColumn.NewUrl, "~/" + newUrl + "%");

            if (enabled != "0")
            {
                query.Append(UrlRedirectColumn.IsActive, enabled);
            }

            return DataRepository.UrlRedirectProvider.Find(query.GetParameters());
        }

        /// <summary>
        /// Check whether the new url exists or not.
        /// </summary>
        /// <param name="oldUrl">Old url string.</param>
        /// <param name="newUrl">New url string.</param>
        /// <param name="id">Product Id, category Id or content page Id to compare.</param>
        /// <returns>Returns true if exists else false.</returns>
        public bool Exists(string oldUrl, string newUrl, int id)
        {
            UrlRedirectQuery query = new UrlRedirectQuery();
            query.AppendEquals(UrlRedirectColumn.OldUrl, oldUrl);
            query.AppendEquals(UrlRedirectColumn.IsActive, "True");
            UrlRedirectService urlRedirectService = new UrlRedirectService();
            TList<UrlRedirect> urlRedirectList = urlRedirectService.Find(query.GetParameters());

            bool isUrlExists = false;
            if (urlRedirectList.Count > 0)
            {
                if (id > 0)
                {
                    foreach (UrlRedirect urlRedirect in urlRedirectList)
                    {
                        if (urlRedirect.UrlRedirectID != id)
                        {
                            isUrlExists = urlRedirect.IsActive;
                            break;
                        }
                    }
                }
                else
                {
                    isUrlExists = true;
                }
            }

            return isUrlExists;
        }

        /// <summary>
        /// Get all complete collection of url Redirect entities
        /// </summary>        
        /// <returns>Returns the UrlRedirect object.</returns>
        public TList<UrlRedirect> GetAll()
        {
            UrlRedirectService urlRedirectService = new UrlRedirectService();
            return urlRedirectService.GetAll();
        }

        /// <summary>
        /// Get url Redirect object by Id.
        /// </summary> 
        /// <param name="urlRedirectId">Unique Id to retrieve entity object.</param>
        /// <returns>Returns the UrlRedirect object.</returns>
        public UrlRedirect GetById(int urlRedirectId)
        {
            UrlRedirectService urlRedirectService = new UrlRedirectService();
            return urlRedirectService.GetByUrlRedirectID(urlRedirectId);
        }

        /// <summary>
        /// Add a new UrlRedirect
        /// </summary>
        /// <param name="urlRedirect">UrlRedirect object to add.</param>
        /// <returns>Returns true if inserted otherwise false.</returns>
        public bool Add(UrlRedirect urlRedirect)
        {
            UrlRedirectService urlRedirectService = new UrlRedirectService();
            return urlRedirectService.Insert(urlRedirect);
        }

        /// <summary>
        /// Update a UrlRedirect
        /// </summary>
        /// <param name="urlRedirect">UrlRedirect object to add.</param>
        /// <returns>Returns true if updated otherwise false.</returns>
        public bool Update(UrlRedirect urlRedirect)
        {
            UrlRedirectService urlRedirectService = new UrlRedirectService();
            return urlRedirectService.Update(urlRedirect);
        }

        /// <summary>
        /// Delete a UrlRedirect
        /// </summary>
        /// <param name="urlRedirect">UrlRedirect object to add.</param>
        /// <returns>Returns true if deleted otherwise false.</returns>
        public bool Delete(UrlRedirect urlRedirect)
        {
            UrlRedirectService urlRedirectService = new UrlRedirectService();
            return urlRedirectService.Delete(urlRedirect);
        }

        /// <summary>
        /// Delete a UrlRedirect entity by Id.
        /// </summary>
        /// <param name="urlRedirectId">Url redirect Id to delete the UrlRedirect object.</param>
        /// <returns>Returns true if deleted otherwise false.</returns>
        public bool Delete(int urlRedirectId)
        {
            UrlRedirectService urlRedirectService = new UrlRedirectService();
            return urlRedirectService.Delete(urlRedirectId);
        }

        /// <summary>
        /// Checks to see if an SEO URL exists.
        /// </summary>
        /// <param name="seoUrl">SEO url to check.</param>
        /// <returns>Returns true if exist otherwise false.</returns>
        public bool SeoUrlExists(string seoUrl)
        {
            bool isExist = false;

            if (!string.IsNullOrEmpty(seoUrl))
            {
                ProductService productService = new ProductService();
                ProductQuery query = new ProductQuery();
                query.AppendIsNotNull(ProductColumn.SEOURL);

                TList<Product> productList = productService.Find(query.GetParameters());

                foreach (Product product in productList)
                {
                    if (!string.IsNullOrEmpty(product.SEOURL))
                    {
                        string productSeoUrl = ZNodeSEOUrl.MakeURL(product.ProductID.ToString(), SEOUrlType.Product, product.SEOURL);

                        string mvcUrl = ZNodeSEOUrl.MakeURLforMVC(product.ProductID.ToString(), SEOUrlType.Product, product.SEOURL);

                        if (seoUrl.ToLower() == productSeoUrl.ToLower() || seoUrl.ToLower() == mvcUrl.ToLower())
                        {
                            isExist = true;
                            break;
                        }
                    }
                }

                if (!isExist)
                {
                    CategoryQuery categoryQuery = new CategoryQuery();
                    categoryQuery.AppendIsNotNull(CategoryColumn.SEOURL);

                    CategoryService categoryService = new CategoryService();
                    TList<Category> categoryList = categoryService.Find(categoryQuery.GetParameters());

                    foreach (Category category in categoryList)
                    {
                        if (!string.IsNullOrEmpty(category.SEOURL))
                        {
                            string categorySeoUrl = ZNodeSEOUrl.MakeURL(category.CategoryID.ToString(), SEOUrlType.Category, category.SEOURL);

                            string mvcUrl = ZNodeSEOUrl.MakeURLforMVC(category.Name, SEOUrlType.Category, category.SEOURL);

                            if (seoUrl.ToLower() == categorySeoUrl.ToLower() || seoUrl.ToLower() == mvcUrl.ToLower())
                            {
                                isExist = true;
                                break;
                            }
                        }
                    }
                }

                if (!isExist)
                {
                    ContentPageQuery contentPageQuery = new ContentPageQuery();
                    contentPageQuery.AppendIsNotNull(ContentPageColumn.SEOURL);

                    ContentPageService contentPageService = new ContentPageService();
                    TList<ContentPage> contentPageList = contentPageService.Find(contentPageQuery.GetParameters());

                    foreach (ContentPage contentPage in contentPageList)
                    {
                        if (!string.IsNullOrEmpty(contentPage.SEOURL))
                        {
                            string contentPageSEOUrl = ZNodeSEOUrl.MakeURL(contentPage.Name, SEOUrlType.ContentPage, contentPage.SEOURL);

                            if (Equals(seoUrl.ToLower(), contentPageSEOUrl.ToLower()))
                            {
                                isExist = true;
                                break;
                            }
                        }
                    }
                }
            }

            return isExist;
        }

        /// <summary>
        /// Checks to see if an SEO URL exists.
        /// </summary>
        /// <param name="seoUrl">SEO url to check.</param>
        /// <param name="id">Product Id, category Id or content page Id to compare.</param>
        /// <returns>Returns true if SEO url alredy exists else false.</returns>
        public bool SeoUrlExists(string seoUrl, int id)
        {
            return this.SeoUrlExists(seoUrl, id.ToString());
        }

        /// <summary>
        /// Checks to see if an SEO URL exists.
        /// </summary>
        /// <param name="seoUrl">SEO url to check</param>
        /// <param name="id">Product Id, category Id or content page Id to compare.</param>
        /// <returns>Returns true if SEO url alredy exists else false.</returns>
        public bool SeoUrlExists(string seoUrl, string id)
        {
            bool isSEOUrlExist = false;

            if (!string.IsNullOrEmpty(seoUrl))
            {
                ProductService productService = new ProductService();
                TList<Product> productList = productService.GetBySEOURL(seoUrl);

                if (productList.Count > 0)
                {
                    if (!Equals(productList[0].ProductID.ToString(), id))
                    {
                        isSEOUrlExist = true;
                    }
                }

                if (!isSEOUrlExist)
                {
                    CategoryService categoryService = new CategoryService();
                    TList<Category> categoryList = categoryService.GetBySEOURL(seoUrl);

                    if (categoryList.Count > 0)
                    {
                        if (!Equals(categoryList[0].CategoryID.ToString(), id))
                        {
                            isSEOUrlExist = true;
                        }
                    }
                }

                if (!isSEOUrlExist)
                {
                    ContentPageService contentPageService = new ContentPageService();
                    TList<ContentPage> contentPageList = contentPageService.GetBySEOURL(seoUrl);

                    if (contentPageList.Count > 0)
                    {
                        if (!Equals(contentPageList[0].Name, id))
                        {
                            isSEOUrlExist = true;
                        }
                    }
                }
            }

            return isSEOUrlExist;
        }

        /// <summary>
        /// Adds a new entry into the 301 redirect table.
        /// </summary>
        /// <param name="urlType">SEO url type.</param>
        /// <param name="oldSEOUrl">Old SEO url.</param>
        /// <param name="newSEOUrl">New SEO Url</param>
        /// <param name="id">Product Id, category Id or content page Id to compare.</param>
        /// <returns>Returns true if add to URL redirect table otherwise false.</returns>
        public bool AddUrlRedirectTable(SEOUrlType urlType, string oldSEOUrl, string newSEOUrl, string id, bool isMvcPage = false)
        {
            bool isAdded = true;
            UrlRedirect urlRedirect = new UrlRedirect();
            urlRedirect.IsActive = true;

            if (string.IsNullOrEmpty(oldSEOUrl))
            {
                urlRedirect.OldUrl = isMvcPage ? ZNodeSEOUrl.MakeUrlForMvc(id, urlType, string.Empty) : ZNodeSEOUrl.MakeURL(id, urlType, string.Empty);
            }
            else
            {
                urlRedirect.OldUrl = isMvcPage ? ZNodeSEOUrl.MakeUrlForMvc(id, urlType, oldSEOUrl) : ZNodeSEOUrl.MakeURL(id, urlType, oldSEOUrl);
            }

            if (string.IsNullOrEmpty(newSEOUrl))
            {
                urlRedirect.OldUrl = isMvcPage ? ZNodeSEOUrl.MakeUrlForMvc(id, urlType, string.Empty) : ZNodeSEOUrl.MakeURL(id, urlType, string.Empty);
            }
            else
            {
                urlRedirect.NewUrl = isMvcPage ? ZNodeSEOUrl.MakeUrlForMvc(id, urlType, newSEOUrl) : ZNodeSEOUrl.MakeUrlForMvc(id, urlType, newSEOUrl);
            }

            if (string.Compare(newSEOUrl, oldSEOUrl, true) != 0)
            {
                isAdded = this.Add(urlRedirect);
            }

            if (isAdded)
            {
                this.UpdateUrlRedirect(oldSEOUrl);
            }

            return isAdded;
        }

        /// <summary>
        /// Znode Version 8.0
        ///  Adds a new entry into the 301 redirect table for mvc admin.
        /// </summary>
        /// <param name="urlType">SEO url type.</param>
        /// <param name="oldSEOUrl">Old SEO url.</param>
        /// <param name="newSEOUrl">New SEO Url</param>
        /// <param name="id">Product Id, category Id or content page Id to compare.</param>
        /// <param name="categoryName">Category name to display in url.</param>
        /// <returns>Returns true if add to URL redirect table otherwise false.</returns>
        public bool AddUrlRedirectTableForMVCAdmin(SEOUrlType urlType, string oldSEOUrl, string newSEOUrl, string id, string categoryName = "")
        {
            bool isAdded = true;
            UrlRedirect urlRedirect = new UrlRedirect();
            urlRedirect.IsActive = true;

            if (string.IsNullOrEmpty(oldSEOUrl))
            {
                urlRedirect.OldUrl = ZNodeSEOUrl.MakeUrlForMvc(id, urlType, string.Empty, categoryName);
            }
            else
            {
                urlRedirect.OldUrl = ZNodeSEOUrl.MakeUrlForMvc(id, urlType, oldSEOUrl);
            }

            if (string.IsNullOrEmpty(newSEOUrl))
            {
                urlRedirect.OldUrl = ZNodeSEOUrl.MakeUrlForMvc(id, urlType, string.Empty, categoryName);
            }
            else
            {
                urlRedirect.NewUrl = ZNodeSEOUrl.MakeUrlForMvc(id, urlType, newSEOUrl);
            }

            if (string.Compare(newSEOUrl, oldSEOUrl, true) != 0)
            {
                isAdded = this.Add(urlRedirect);
            }

            if (isAdded)
            {
                this.UpdateUrlRedirect(oldSEOUrl);
            }

            return isAdded;
        }

        /// <summary>
        /// Updates an SEO URL Redirect URL.
        /// </summary>        
        /// <param name="seoUrl">Update seoUrl in Url redirect table.</param>        
        /// <returns>Returns true if add to URL redirect table otherwise false.</returns>
        public bool UpdateUrlRedirect(string seoUrl)
        {
            if (!string.IsNullOrEmpty(seoUrl))
            {
                UrlRedirect urlRedirect = this.GetByNewUrl("~/" + seoUrl);

                if (!Equals(urlRedirect, null))
                {
                    urlRedirect.IsActive = false;
                    return this.Update(urlRedirect);
                }
            }

            return true;
        }

        /// <summary>
        /// Updates the SEO memory table and SEO Url redirect tables
        /// </summary>
        /// <param name="urlType">SEO url type.</param>
        /// <param name="oldSEOUrl">Old SEO url.</param>
        /// <param name="seoUrl">New SEO Url</param>
        /// <param name="id">Product Id, category Id or content page Id to compare.</param>
        /// <returns>Returns true if add to URL redirect table otherwise false.</returns>
        public bool UpdateUrlRedirectTable(SEOUrlType urlType, string oldSEOUrl, string seoUrl, string id)
        {
            return this.UpdateUrlRedirectTable(urlType, oldSEOUrl, seoUrl, id, false);
        }

        /// <summary>
        /// Updates seo memory table and SEO Url redirect tables
        /// </summary>
        /// <param name="urlType">SEO url type.</param>
        /// <param name="oldSEOUrl">Old SEO url.</param>
        /// <param name="seoUrl">New SEO Url</param>
        /// <param name="id">Product Id, category Id or content page Id to compare.</param>
        /// <param name="isUrlRedirectEnabled"> Indicates whether url redirect is enbled or not.</param>
        /// <returns>Returns true if add to URL redirect table otherwise false.</returns>
        public bool UpdateUrlRedirectTable(SEOUrlType urlType, string oldSEOUrl, string seoUrl, string id, bool isUrlRedirectEnabled)
        {
            bool isUrlRedirectUpdated = true;

            ZNodeSEOUrl znodeSEOUrl = new ZNodeSEOUrl();
            UrlRedirect urlRedirect = new UrlRedirect();

            if (oldSEOUrl.Length == 0 && !string.IsNullOrEmpty(seoUrl))
            {
                isUrlRedirectUpdated &= znodeSEOUrl.Add(id, urlType, seoUrl, isUrlRedirectEnabled);

                if (isUrlRedirectEnabled && isUrlRedirectUpdated)
                {
                    urlRedirect.OldUrl = ZNodeSEOUrl.MakeURL(id, urlType, string.Empty);
                    urlRedirect.NewUrl = ZNodeSEOUrl.MakeURL(id, urlType, seoUrl);
                    urlRedirect.IsActive = true;

                    isUrlRedirectUpdated &= this.Add(urlRedirect);
                }
            }
            else if (oldSEOUrl.Length > 0)
            {
                string oldSeoUrl = ZNodeSEOUrl.MakeURL(id, urlType, oldSEOUrl);
                urlRedirect = this.GetByNewUrl(oldSeoUrl);

                if (seoUrl == null)
                {
                    isUrlRedirectUpdated &= znodeSEOUrl.Remove(oldSEOUrl);
                }
                else if (oldSEOUrl != seoUrl)
                {
                    // Update newseourl to the memory table
                    isUrlRedirectUpdated &= znodeSEOUrl.Update(oldSEOUrl, seoUrl, isUrlRedirectEnabled);

                    if (isUrlRedirectEnabled && isUrlRedirectUpdated)
                    {
                        urlRedirect.OldUrl = oldSeoUrl;
                        urlRedirect.NewUrl = ZNodeSEOUrl.MakeURL(id, urlType, seoUrl);
                        urlRedirect.IsActive = true;

                        isUrlRedirectUpdated &= this.Add(urlRedirect);
                    }
                }

                if (!Equals(urlRedirect, null))
                {
                    urlRedirect.IsActive = false;
                    isUrlRedirectUpdated &= this.Update(urlRedirect);
                }
            }

            return isUrlRedirectUpdated;
        }

        /// <summary>
        /// Get UrlRedirect for new url string
        /// </summary>
        /// <param name="newUrl">New url string to get Url redirect object.</param>        
        /// <returns>Returns the UrlRedirect object.</returns>
        protected UrlRedirect GetByNewUrl(string newUrl)
        {
            UrlRedirectQuery filters = new UrlRedirectQuery();
            filters.AppendEquals(UrlRedirectColumn.NewUrl, newUrl);
            filters.AppendEquals(UrlRedirectColumn.IsActive, "true");

            UrlRedirectService urlRedirectService = new UrlRedirectService();
            TList<UrlRedirect> urlList = urlRedirectService.Find(filters.GetParameters());

            if (urlList.Count > 0)
            {
                return urlList[0];
            }

            return null;
        }
    }
}
