using System;
using System.Data;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.Admin
{
    /// <summary>
    /// This Class Manages the related to the Order,OrderState,OrderLineItem.
    /// </summary>
    public class OrderAdmin : ZNodeBusinessBase
    {
        #region Order Public Methods

        /// <summary>
        /// Returns all the order for a Portal
        /// </summary>
        /// <param name="portalId">Portal Id to get all orders.</param>
        /// <returns>Returns list of order for a portal.</returns>
        public TList<ZNode.Libraries.DataAccess.Entities.Order> GetAllOrders(int portalId)
        {
            ZNode.Libraries.DataAccess.Service.OrderService orderService = new OrderService();
            TList<ZNode.Libraries.DataAccess.Entities.Order> orderList = orderService.GetByPortalId(portalId);

            return orderList;
        }

        /// <summary>
        /// Returns all the order for portals
        /// </summary>
        /// <param name="portalIds">Comma seperated portal Ids.</param>
        /// <returns>Returns list of order for portals.</returns>
        public TList<ZNode.Libraries.DataAccess.Entities.Order> GetAllOrders(string portalIds)
        {
            ZNode.Libraries.DataAccess.Service.OrderService orderService = new OrderService();
            OrderQuery query = new OrderQuery();
            query.AppendIn(OrderColumn.PortalId, portalIds.Split(','));
            TList<ZNode.Libraries.DataAccess.Entities.Order> orderList = orderService.Find(query.GetParameters());

            return orderList;
        }

        /// <summary>
        ///  Get order for a order Id
        /// </summary>
        /// <param name="orderId">Order Id to get order.</param>
        /// <returns>Returns Order object for the order Id.</returns>
        public Order GetOrderByOrderID(int orderId)
        {
            ZNode.Libraries.DataAccess.Service.OrderService orderService = new OrderService();
            Order orderList = orderService.GetByOrderID(orderId);

            return orderList;
        }

        /// <summary>
        ///  Get Order which includes payment,shipping,account,orderstateInfo for a Order ID
        /// </summary>
        /// <param name="orderId">Order Id to get the deep loaded order.</param>
        /// <returns>Returns the deep loaded Order object for the order Id.</returns>
        public Order DeepLoadByOrderID(int orderId)
        {
            ZNode.Libraries.DataAccess.Service.OrderService orderService = new OrderService();
            Order order = orderService.DeepLoadByOrderID(orderId, true, DeepLoadType.IncludeChildren, typeof(OrderState), typeof(Account), typeof(ZNode.Libraries.DataAccess.Entities.Shipping), typeof(PaymentStatus), typeof(PaymentSetting));

            return order;
        }

        /// <summary>
        /// Returns order for a Account
        /// </summary>
        /// <param name="accountId">Account Id to get the order list for the account.</param>
        /// <returns>Returns list of orders ordered by the account.</returns>
        public TList<Order> GetByAccountID(int accountId)
        {
            ZNode.Libraries.DataAccess.Service.OrderService orderService = new OrderService();
            TList<ZNode.Libraries.DataAccess.Entities.Order> orderList = orderService.GetByAccountID(accountId);

            return orderList;
        }

        /// <summary>
        /// Returns No of Orders for a Account
        /// </summary>
        /// <param name="accountId">Account Id to get the total orders for the account.</param>
        /// <returns>Returns the number of orders ordered by the account.</returns>
        public int GetTotalOrderItemsByAccountId(int accountId)
        {
            int ordersCount = 0;

            OrderService orderService = new OrderService();
            ordersCount = orderService.GetTotalItems("AccountId=" + accountId, out ordersCount);

            return ordersCount;
        }

        /// <summary>
        /// Returns No of Orders for a Account
        /// </summary>
        /// <param name="accountId">Account Id to get the total orders for the account.</param>
        /// <returns>Returns the number of orders ordered by the account.</returns>
        public int GetTotalOrderItemsByPaymentId(int paymentid)
        {
            int ordersCount = 0;

            OrderService orderService = new OrderService();
            ordersCount = orderService.GetTotalItems("PaymentSettingID=" + paymentid, out ordersCount);

            return ordersCount;
        }

        /// <summary>
        /// Returns order for a Account and Portal
        /// </summary>
        /// <param name="accountId">Account Id to get the order dataset.</param>
        /// <param name="portalId">Portal Id to get the order dataset.</param>
        /// <returns>Returns the Orders dataset ordered by account for the portal.</returns>
        public DataSet GetByAccountID(int accountId, int portalId)
        {
            OrderHelper orderHelper = new OrderHelper();
            DataSet orderDataSet = orderHelper.GetByAccountID(accountId, portalId);

            return orderDataSet;
        }

        /// <summary>
        /// Returns Orders based on the specified parameters.
        /// </summary>
        /// <param name="orderId">Order Id to search (nullable)</param>
        /// <param name="billingFirstname">Billing first name to search.</param>
        /// <param name="billingLastname">Billing last name  to search.</param>
        /// <param name="billingCompanyname">Billing company name to search.</param>
        /// <param name="accountId">Account Id to search.</param>
        /// <param name="startDate">Start data to search.</param>
        /// <param name="endDate">Order end date to search.</param>
        /// <param name="orderStateId">Order state Id to search.</param>
        /// <param name="portalId">Portal Id to search.</param>
        /// <param name="portalIds">Comma seperated portal Id to search.</param>
        /// <returns>Returns the Order dataset search result.</returns>
        public DataSet FindOrders(int? orderId, string billingFirstname, string billingLastname, string billingCompanyname, string accountId, DateTime? startDate, DateTime? endDate, int? orderStateId, int? portalId, string portalIds)
        {
            OrderHelper orderHelper = new OrderHelper();
            return orderHelper.SearchOrder(orderId, billingFirstname, billingLastname, billingCompanyname, accountId, startDate, endDate, orderStateId, portalId, portalIds);
        }

        /// <summary>
        /// Returns Vendor Orders based on the specified parameters.
        /// </summary>
        /// <param name="orderId">Order Id to search (nullable)</param>
        /// <param name="billingFirstname">Billing first name to search.</param>
        /// <param name="billingLastname">Billing last name  to search.</param>
        /// <param name="billingCompanyname">Billing company name to search.</param>
        /// <param name="accountId">Account Id to search.</param>
        /// <param name="startDate">Start data to search.</param>
        /// <param name="endDate">Order end date to search.</param>
        /// <param name="orderStateId">Order state Id to search.</param>
        /// <param name="portalId">Portal Id to search.</param>
        /// <param name="portalIds">Comma seperated portal Id to search.</param>
        /// <returns>Returns the Order dataset search result.</returns>
        public DataSet FindVendorOrders(int? orderId, string billingFirstname, string billingLastname, string billingCompanyname, string accountId, DateTime? startDate, DateTime? endDate, int? orderStateId, int? portalId, string portalIds, string loggedAccountId)
        {
            OrderHelper orderHelper = new OrderHelper();
            return orderHelper.SearchVendorOrder(orderId, billingFirstname, billingLastname, billingCompanyname, accountId, startDate, endDate, orderStateId, portalId, portalIds, loggedAccountId);
        }

        /// <summary>
        /// Return the order line item dataset based on from, to and portal Id
        /// </summary>
        /// <param name="fromDate"> Order crated from Date</param>
        /// <param name="toDate">Order crated to Date.</param>
        /// <param name="portalId">Portal Id to get the order line item.</param>
        /// <returns>Returns the OrderLineItem dataset.</returns>
        public DataSet GetOrderLineItems(DateTime fromDate, DateTime toDate, int portalId)
        {
            return this.GetOrderLineItems(string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, fromDate.ToString("MM/dd/yyyy"), toDate.ToString("MM/dd/yyyy"), -1, portalId);
        }

        /// <summary>
        /// Returns the OrderLineItem dataset.
        /// </summary>
        /// <param name="orderId">Order Id to search (nullable)</param>
        /// <param name="billingFirstname">Billing first name to search.</param>
        /// <param name="billingLastname">Billing last name  to search.</param>
        /// <param name="billingCompanyname">Billing company name to search.</param>
        /// <param name="accountId">Account Id to search.</param>
        /// <param name="startDate">Start date to search.</param>
        /// <param name="endDate">Order end date to search.</param>
        /// <param name="orderStateId">Order state Id to search.</param>
        /// <param name="portalId">Portal Id to search.</param>
        /// <returns>Returns the OrderLineItem dataset.</returns>
        public DataSet GetOrderLineItems(string orderId, string billingFirstname, string billingLastname, string billingCompanyname, string accountId, string startDate, string endDate, int orderStateId, int portalId)
        {
            OrderHelper orderHelper = new OrderHelper();
            return orderHelper.GetOrderLineItems(orderId, billingFirstname, billingLastname, billingCompanyname, accountId, startDate, endDate, orderStateId, portalId);
        }

        /// <summary>
        /// Update Order Status
        /// </summary>
        /// <param name="order">Order object to update.</param>
        /// <returns>Returns true if updated else false.</returns>
        public bool Update(Order order)
        {
            ZNode.Libraries.DataAccess.Service.OrderService orderService = new OrderService();
            bool isUpdated = orderService.Update(order);
            return isUpdated;
        }

        /// <summary>
        /// Update Order line item Status
        /// </summary>
        /// <param name="orderLineItem">Order line item status object to update.</param>
        /// <returns>Returns true if updated else false.</returns>
        public bool UpdateOrderLineItem(OrderLineItem orderLineItem)
        {
            ZNode.Libraries.DataAccess.Service.OrderLineItemService orderLineItemService = new OrderLineItemService();
            bool isUpdated = orderLineItemService.Update(orderLineItem);
            return isUpdated;
        }

        /// <summary>
        /// Delete Order by Id
        /// </summary>
        /// <param name="orderId">Order Id to delete the order.</param>
        /// <returns>Returns true if deleted else false.</returns>
        public bool Delete(int orderId)
        {
            ZNode.Libraries.DataAccess.Service.OrderService orderService = new OrderService();
            bool isDeleted = orderService.Delete(orderId);

            return isDeleted;
        }

        /// <summary>
        /// Get Orders based on the OrderId
        /// </summary>
        /// <param name="orderId">Order Id to get order detail dataset.</param>      
        /// <returns>Returns order details dataset.</returns>       
        public DataSet GetOrdersByOrderId(string orderId)
        {
            OrderHelper orderHelper = new OrderHelper();
            return orderHelper.GetOrdersByOrderId(orderId);
        }

        /// <summary>
        /// Get Orders based on the OrderId
        /// </summary>
        /// <param name="orderId">Order Id to get order detail dataset.</param>      
        /// <returns>Returns order details dataset.</returns>       
        public DataSet GetVendorOrdersByOrderId(string orderId, string loggedAccountId)
        {
            OrderHelper orderHelper = new OrderHelper();
            return orderHelper.GetVendorOrdersByOrderId(orderId, loggedAccountId);
        }

        /// <summary>
        /// Get OrderLineItems based on OrderId
        /// </summary>
        /// <param name="orderId">Order Id to get the order line item details dataset.</param>      
        /// <param name="loggedAccountId">Logged Account Id to get the order details.</param>
        /// <returns>Returns OrderLineItem dataset by order Id.</returns>
        public DataSet GetOrderLineItemsByOrderId(string orderId)
        {
            OrderHelper orderHelper = new OrderHelper();
            return orderHelper.GetOrderLineItemsByOrderId(orderId);
        }

        /// <summary>
        /// Get OrderLineItems based on OrderId
        /// </summary>
        /// <param name="orderId">Order Id to get the order line item details dataset.</param>      
        /// <param name="loggedAccountId">Logged Account Id to get the order details.</param>
        /// <returns>Returns OrderLineItem dataset by order Id.</returns>
        public DataSet GetVendorOrderLineItemsByOrderId(string orderId, string loggedAccountId)
        {
            OrderHelper orderHelper = new OrderHelper();
            return orderHelper.GetVendorOrderLineItemsByOrderId(orderId, loggedAccountId);
        }

        /// <summary>
        /// Get the OrderLineItems based on Order Id and Order state.
        /// </summary>
        /// <param name="orderId">Order Id to get the order line item.</param>      
        /// <param name="orderStateId">Order state Id to filter the order line item.</param>
        /// <returns>Returns the OrderLineItem dataset by Order Id and Order State.</returns>
        public DataSet GetOrderLineItemsByOrderId(string orderId, int orderStateId)
        {
            OrderHelper orderHelper = new OrderHelper();
            DataSet orderLineItemDataSet = orderHelper.GetOrderLineItemsByOrderId(orderId, orderStateId);

            return orderLineItemDataSet;
        }

        /// <summary>
        /// Get the next highest OrderId, which not downloaded yet 
        /// </summary>
        /// <returns>Returns the next highest OrderId, which not downloaded yet. </returns>
        public int GetHighestOrderId()
        {
            return this.GetHighestOrderId("0");
        }

        /// <summary>
        /// Get the next highest OrderId, which not downloaded yet for a Portal
        /// </summary>
        /// <param name="portalId">Portal Id to get the highest order Id.</param>
        /// <returns>Returns the next highest order Id which not downloaded yet for a Portal.</returns>
        public int GetHighestOrderId(string portalId)
        {
            OrderHelper orderHelper = new OrderHelper();
            int orderId = orderHelper.GetHighestOrderId(portalId);

            return orderId;
        }
        #endregion

        #region OrderLineItem Public Methods
        /// <summary>
        /// Get all order line items
        /// </summary>        
        /// <returns>Returns list of all OrderLineItem object.</returns>
        public TList<ZNode.Libraries.DataAccess.Entities.OrderLineItem> GetAllOrderLineItems()
        {
            ZNode.Libraries.DataAccess.Service.OrderLineItemService orderLineItemService = new OrderLineItemService();
            TList<OrderLineItem> orderLineItems = orderLineItemService.GetAll();

            return orderLineItems;
        }

        /// <summary>
        /// Get order line items for a orderid
        /// </summary>
        /// <param name="orderId">Order Id to get the order line item for the order.</param>
        /// <returns>Returns the list of OrderLineItem for a order.</returns>
        public TList<ZNode.Libraries.DataAccess.Entities.OrderLineItem> GetOrderLineItemByOrderID(int orderId)
        {
            ZNode.Libraries.DataAccess.Service.OrderLineItemService orderLineItemService = new OrderLineItemService();
            TList<OrderLineItem> orderLineItems = orderLineItemService.GetByOrderID(orderId);
            orderLineItemService.DeepLoad(orderLineItems, true, DeepLoadType.IncludeChildren, typeof(OrderShipment));
            return orderLineItems;
        }

        /// <summary>
        /// Get vendor order line items for a orderid
        /// </summary>
        /// <param name="orderId">Order Id to get the order line item for the order.</param>
        /// <returns>Returns the list of OrderLineItem for a order.</returns>
        public DataSet GetVendorOrderLineItemByOrderID(int orderId, string loggedAccountId)
        {
            OrderHelper orderHelper = new OrderHelper();
            return orderHelper.GetVendorOrderLineItemByOrderID(orderId.ToString(), loggedAccountId);
        }

        /// <summary>
        /// Delete the order line item by Order Id.
        /// </summary>
        /// <param name="orderId">Order Id to delete the order line item.</param>
        public void DeleteByOrderId(int orderId)
        {
            TList<ZNode.Libraries.DataAccess.Entities.OrderLineItem> orderLineItem = this.GetOrderLineItemByOrderID(orderId);
            ZNode.Libraries.DataAccess.Service.OrderLineItemService orderLineItemService = new OrderLineItemService();

            if (orderLineItem.Count > 0)
            {
                OrderLineItem lineItem = orderLineItem[0];

                orderLineItem.Remove(lineItem);

                // Remove Addon Order Line Item first
                orderLineItemService.Delete(orderLineItem);

                // Then Remove Parent/product OrderLine item
                orderLineItemService.Delete(lineItem);
            }
        }
        #endregion

        #region OrderState Public Methods

        /// <summary>
        /// Get All OrderState
        /// </summary>
        /// <returns>Returns list order OrderState object.</returns>
        public TList<ZNode.Libraries.DataAccess.Entities.OrderState> GetAllOrderStates()
        {
            ZNode.Libraries.DataAccess.Service.OrderStateService orderStateService = new OrderStateService();
            TList<ZNode.Libraries.DataAccess.Entities.OrderState> orderStateList = orderStateService.GetAll();

            return orderStateList;
        }

        /// <summary>
        ///  Get the order state by order state Id.
        /// </summary>
        /// <param name="orderStateId">Order state Id to get the order state.</param>
        /// <returns>Returns OrderState object by order Id</returns>
        public OrderState GetByOrderStateID(int orderStateId)
        {
            ZNode.Libraries.DataAccess.Service.OrderStateService orderStateService = new OrderStateService();
            OrderState orderState = orderStateService.GetByOrderStateID(orderStateId);

            return orderState;
        }

        /// <summary>
        ///  Get the PaymentType for a paymentTypeId
        /// </summary>
        /// <param name="paymentTypeId">Payment type Id to get the Payment type object.</param>
        /// <returns>Returns the PaymentType object for the payment type Id.</returns>
        public PaymentType GetByPaymentTypeId(int paymentTypeId)
        {
            ZNode.Libraries.DataAccess.Service.PaymentTypeService paymentTypeService = new PaymentTypeService();
            PaymentType paymentType = paymentTypeService.GetByPaymentTypeID(paymentTypeId);

            return paymentType;
        }

        #endregion

        #region Znode Version 7.2.2 - OrderShipment Public Methods

        /// <summary>
        /// Znode Version 7.2.2
        /// This function find out the ShippingTypeID. on the basis of orderLineItemID
        /// </summary>
        /// <param name="orderLineItemID">Order Line Id </param>
        /// <returns>Returns Order ShippingtypeID</returns>
        public int GetOrderShippingTypeID(int orderLineItemID)
        {
            int shippingTypeID = 0;
            int orderShipmentID = 0;
            int shippingID = 0;

            //Get Order ShipmentID from OrderLineItemID
            OrderLineItemService orderLineItemService = new OrderLineItemService();
            OrderLineItem ordLineItem = orderLineItemService.GetByOrderLineItemID(orderLineItemID);

            orderShipmentID = (ordLineItem.OrderShipmentID.Equals(null) || ordLineItem.OrderShipmentID.ToString().Equals(0)) ? 0 : int.Parse(ordLineItem.OrderShipmentID.ToString());

            if (orderShipmentID > 0)
            {
                //Get ShippingID from OrderShipmentID 
                OrderShipmentService shipmentServeice = new OrderShipmentService();
                OrderShipment shipment = shipmentServeice.GetByOrderShipmentID(orderShipmentID);
                shippingID = (shipment.ShippingID.Equals(null) || shipment.ShippingID.ToString().Equals(0)) ? 0 : int.Parse(shipment.ShippingID.ToString());
            }

            if (shippingID > 0)
            {
                //Get ShippingTypeID from ShippingID
                ShippingService shippingService = new ShippingService();
                Shipping shipping = shippingService.GetByShippingID(shippingID);
                shippingTypeID = (shipping.ShippingTypeID.Equals(null) || shipping.ShippingTypeID.ToString().Equals(0)) ? 0 : int.Parse(shipping.ShippingTypeID.ToString());
            }

            return shippingTypeID;
        }

        #endregion
    }
}
