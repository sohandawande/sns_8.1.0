using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.Admin
{
    /// <summary>
    /// Provides methods to manage Referral Commission
    /// </summary>
    public class ReferralCommissionAdmin : ZNodeBusinessBase
    {
        #region Public methods - Related to Discount Type
        /// <summary>
        /// Get all the referral commission types
        /// </summary>
        /// <returns>Returns list of ReferralCommissionType object.</returns>
        public TList<ZNode.Libraries.DataAccess.Entities.ReferralCommissionType> GetAll()
        {
            ZNode.Libraries.DataAccess.Service.ReferralCommissionTypeService service = new ReferralCommissionTypeService();
            TList<ZNode.Libraries.DataAccess.Entities.ReferralCommissionType> referralCommissionList = service.GetAll();

            return referralCommissionList;
        }

        /// <summary>
        /// Get a referral commission type by Id.
        /// </summary>
        /// <param name="referralCommissionId">Referral commission Id to get the referral commission object.</param>
        /// <returns>Returns ReferralCommissionType object.</returns>
        public ZNode.Libraries.DataAccess.Entities.ReferralCommissionType GetByReferralID(int referralCommissionId)
        {
            ZNode.Libraries.DataAccess.Service.ReferralCommissionTypeService service = new ReferralCommissionTypeService();
            ZNode.Libraries.DataAccess.Entities.ReferralCommissionType referralCommissionList = service.GetByReferralCommissionTypeID(referralCommissionId);

            return referralCommissionList;
        }

        /// <summary>
        /// Get list of referral commission by account Id.        
        /// </summary>       
        /// <param name="accountId">Account Id to get the referral commission object.</param>
        /// <returns>Returns list of ReferralCommission object.</returns>
        public TList<ZNode.Libraries.DataAccess.Entities.ReferralCommission> GetReferralCommissionByAccountID(int accountId)
        {
            ZNode.Libraries.DataAccess.Service.ReferralCommissionService service = new ReferralCommissionService();
            TList<ZNode.Libraries.DataAccess.Entities.ReferralCommission> referralCommissionList = service.Find(string.Format("ReferralAccountID={0}", accountId.ToString()));

            return referralCommissionList;
        }

        /// <summary>
        /// Delete the referral commission by order Id.
        /// </summary>
        /// <param name="orderId">Order Id to delete the referral commission.</param>
        /// <returns>Returns true if deleted otherwise false.</returns>
        public bool DeleteByOrderId(int orderId)
        {
            bool isDeleted = false;
            ZNode.Libraries.DataAccess.Service.ReferralCommissionService service = new ReferralCommissionService();

            // Find the ReferralCommissionID using the order Id
            TList<ZNode.Libraries.DataAccess.Entities.ReferralCommission> referralCommissionList = service.Find(string.Format("OrderID={0}", orderId));

            if (referralCommissionList != null && referralCommissionList.Count == 1)
            {
                isDeleted = service.Delete(referralCommissionList[0].ReferralCommissionID);
            }

            return isDeleted;
        }
        
        /// <summary>
        /// Chech whether the order has referral commission or not.
        /// </summary>        
        /// <param name="orderId">Order Id to check the referral commission.</param>
        /// <returns>Returns true if order has referral commission otherwise false.</returns>
        public bool GetReferralCommissionByOrderID(int orderId)
        {
            ZNode.Libraries.DataAccess.Service.ReferralCommissionService service = new ReferralCommissionService();

            TList<ZNode.Libraries.DataAccess.Entities.ReferralCommission> referralCommissionList = service.Find(string.Format("OrderID={0}", orderId.ToString()));
            if (referralCommissionList != null && referralCommissionList.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion
    }
}
