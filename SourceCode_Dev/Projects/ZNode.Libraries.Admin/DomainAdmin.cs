using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.Admin
{
    public class DomainAdmin : ZNodeBusinessBase
    {
        /// <summary>
        ///  Get all Domains
        /// </summary>
        /// <returns>Returns list of Domain object.</returns>
        public TList<Domain> GetAllDomain()
        {
            ZNode.Libraries.DataAccess.Service.DomainService domainService = new ZNode.Libraries.DataAccess.Service.DomainService();
            TList<ZNode.Libraries.DataAccess.Entities.Domain> domainList = domainService.GetAll();

            return domainList;
        }

        /// <summary>
        /// Returns all Domain by PortalId
        /// </summary>
        /// <param name="portalId">Portal Id to get the domain objects.</param>
        /// <returns>Returns list of Domain objects.</returns>
        public TList<Domain> GetDomainByPortalID(int portalId)
        {
            ZNode.Libraries.DataAccess.Service.DomainService domainService = new ZNode.Libraries.DataAccess.Service.DomainService();
            TList<ZNode.Libraries.DataAccess.Entities.Domain> domainList = domainService.GetByPortalID(portalId);

            return domainList;
        }

        /// <summary>
        /// Returns Domain detail by domain Id
        /// </summary>
        /// <param name="domainId">Domain Id to get the Domain object.</param>
        /// <returns>Returns the Domain object.</returns>
        public Domain GetDomainByDomainId(int domainId)
        {
            ZNode.Libraries.DataAccess.Service.DomainService domainService = new ZNode.Libraries.DataAccess.Service.DomainService();
            Domain domain = domainService.GetByDomainID(domainId);
            return domain;
        }

        /// <summary>
        /// Get the deep loaded domain details by portalId
        /// </summary>
        /// <param name="portalId">Portal Id to ger the deep loaded Domain objects.</param>
        /// <returns>Returns deep loadedd Domain objects.</returns>
        public TList<Domain> DeepLoadDomainsByPortalID(int portalId)
        {
            ZNode.Libraries.DataAccess.Service.DomainService domainService = new ZNode.Libraries.DataAccess.Service.DomainService();
            TList<ZNode.Libraries.DataAccess.Entities.Domain> domainList = domainService.GetByPortalID(portalId);
            domainService.DeepLoad(domainList, true, DeepLoadType.IncludeChildren, typeof(Profile));
            return domainList;
        }

        /// <summary>
        /// Update a Domain
        /// </summary>
        /// <param name="domain">Domain object to update.</param>
        /// <returns>Returns true if updated else false.</returns>
        public bool Update(ZNode.Libraries.DataAccess.Entities.Domain domain)
        {
            ZNode.Libraries.DataAccess.Service.DomainService domainService = new DomainService();
            bool isUpdated = domainService.Update(domain);
            return isUpdated;
        }

        /// <summary>
        /// Insert a new domain.
        /// </summary>     
        /// <param name="domain">Domain object to insert.</param>
        /// <returns>Returns true if inserted else false.</returns>
        public bool Insert(ZNode.Libraries.DataAccess.Entities.Domain domain)
        {
            ZNode.Libraries.DataAccess.Service.DomainService domainService = new DomainService();
            bool isAdded = domainService.Insert(domain);
            return isAdded;
        }

        /// <summary>
        /// Delete Domain details
        /// </summary>
        /// <param name="domainId">Doamin Id to delete from data source.</param>
        /// <returns>Returns true if deleted else false.</returns>
        public bool Delete(int domainId)
        {
            ZNode.Libraries.DataAccess.Service.DomainService domainService = new DomainService();
            bool isDeleted = domainService.Delete(domainId);
            return isDeleted;
        }
    }
}
