﻿using System;
using System.Data;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;

namespace ZNode.Libraries.Admin
{
    public class GiftCardAdmin
    {
        #region Public Methods - Related to Promotions
        /// <summary>
        /// Returns all promotions
        /// </summary>
        /// <returns>Returns list of GiftCard object.</returns>
        public TList<GiftCard> GetAll()
        {
            GiftCardService giftCardService = new GiftCardService();
            TList<GiftCard> giftCardList = giftCardService.GetAll();
            return giftCardList;
        }       

        /// <summary>
        /// Returns all active gift cards.
        /// </summary>
        /// <returns>Retuns list of active gift cards.</returns>
        public TList<GiftCard> GetActiveGiftCards()
        {
            GiftCardService giftCardService = new GiftCardService();
            TList<GiftCard> PromotionList = giftCardService.GetAll();
            PromotionList.ApplyFilter(delegate(GiftCard giftCard) { return System.DateTime.Today.Date >= Convert.ToDateTime(giftCard.ExpirationDate.HasValue).Date && System.DateTime.Today.Date <= Convert.ToDateTime(giftCard.ExpirationDate.HasValue).Date; });

            return PromotionList;
        }

        /// <summary>
        /// Returns the gift card by gift card Id.
        /// </summary>
        /// <param name="giftCardId">Get GiftCard by gift card Id.</param>
        /// <returns>Returns Gift Card object</returns>
        public GiftCard GetByGiftCardId(int giftCardId)
        {
            GiftCardService giftCardService = new GiftCardService();
            return giftCardService.GetByGiftCardId(giftCardId);
        }
        
        /// <summary>
        /// Returns the gift card by gift card number.
        /// </summary>
        /// <param name="giftCardNumber">GiftCard Number.</param>
        /// <returns>Returns Gift Card object</returns>
        public GiftCard GetByGiftCardNumber(string giftCardNumber)
        {
            GiftCardService giftCardService = new GiftCardService();
            return giftCardService.GetByCardNumber(giftCardNumber);
        }

        /// <summary>
        /// Returns the list of gift card by Account Id.
        /// </summary>
        /// <param name="accountId">Account Id.</param>
        /// <returns>Returns list of gift cards for the account.</returns>
        public TList<GiftCard> GetByGiftAccountId(int accountId)
        {
            GiftCardService giftCardService = new GiftCardService();
            string filterExpression = string.Format("{0}={1} AND {3}<>NULL", GiftCardColumn.AccountId.ToString(), accountId.ToString(), GiftCardColumn.AccountId.ToString());

            TList<GiftCard> giftCardList = giftCardService.Find(filterExpression);
            return giftCardList;
        }

        /// <summary>
        /// Get Gift Card History by Account ID.
        /// </summary>
        /// <param name="accountId">Account Id to get the gift card history.</param>
        /// <returns>Returns list of GiftCardHistory object.</returns>
        public TList<GiftCardHistory> GetGiftCardHistoryByAccountID(int accountId)
        {
            return this.GetGiftCardHistoryByAccountID(accountId, 0);
        }

        /// <summary>
        /// Get Gift Card History by Account ID and Order ID.
        /// </summary>
        /// <param name="accountId">Account Id to get the gift card history.</param>
        /// <param name="orderId">Order Id to get the gift card history.</param>
        /// <returns>Returs list of GiftCardHistory object.</returns>
        public TList<GiftCardHistory> GetGiftCardHistoryByAccountID(int accountId, int orderId)
        {
            GiftCardHistoryService giftCardHistoryService = new GiftCardHistoryService();
            OrderService orderService = new OrderService();
            OrderQuery orderQuery = new OrderQuery();
            orderQuery.Append(OrderColumn.AccountID, accountId.ToString());

            if (orderId > 0)
            {
                orderQuery.Append(OrderColumn.OrderID, orderId.ToString());
            }

            TList<Order> orderList = orderService.Find(orderQuery.GetParameters());
            int orderCount = 1;
            string[] orderIdList = null;

            if (orderList.Count > 0)
            {
                orderCount = orderList.Count;
                orderIdList = new string[orderCount];

                for (int index = 0; index <= orderList.Count - 1; index++)
                {
                    orderIdList[index] = orderList[index].OrderID.ToString();
                }
            }

            if (orderIdList != null)
            {
                GiftCardHistoryQuery query = new GiftCardHistoryQuery();
                query.AppendIn(GiftCardHistoryColumn.OrderID, orderIdList);
                TList<GiftCardHistory> giftCardHistoryList = giftCardHistoryService.Find(query.GetParameters());
                return giftCardHistoryList;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Returns the list of gift cards based on the search criteria.
        /// </summary>
        /// <param name="giftCardSearchParam">GiftCardSearchParam objetc.</param>
        /// <returns>Returns list of gift card object as DataSet.</returns>
        public DataSet Search(GiftCardSearchParam giftCardSearchParam)
        {
            GiftCardHelper giftCardHelper = new GiftCardHelper();
            return giftCardHelper.Search(giftCardSearchParam);
        }

        /// <summary>
        /// add new gift card
        /// </summary>
        /// <param name="giftCard">GiftCard entity object.</param>
        /// <returns>Returns true if inserted else false.</returns>
        public bool AddGiftCard(GiftCard giftCard)
        {
            GiftCardService giftCardService = new GiftCardService();
            return giftCardService.Insert(giftCard);
        }
        /// <summary>
        /// add new gift card
        /// </summary>
        /// <param name="giftCard">GiftCard entity object.</param>
        /// <returns>Returns true if inserted else false.</returns>
        public bool AddGiftCard(GiftCard giftCard,out int giftCardID)
        {
            giftCardID = 0;
            GiftCardService giftCardService = new GiftCardService();
            bool isAdded = giftCardService.Insert(giftCard);
            if (!isAdded)
            {
                return false;
            }

            giftCardID = giftCard.GiftCardId;

            // Clear all cache memory.
            CacheManager.Clear();

            return isAdded;
           
        }
        /// <summary>
        /// Update the gift card
        /// </summary>        
        /// <param name="giftCard">GiftCard entity object</param>
        /// <returns>Returns true if inserted else false.</returns>
        public bool UpdateGiftCard(GiftCard giftCard)
        {
            GiftCardService giftCardService = new GiftCardService();
            return giftCardService.Update(giftCard);
        }

        /// <summary>
        /// Delete the gift card by gift card Id.
        /// </summary>
        /// <param name="giftCardId">giftCardId to delete.</param>
        /// <returns>Returns true if deleted else False.</returns>
        public bool DeleteGiftCard(int giftCardId)
        {
            GiftCardService giftCardService = new GiftCardService();
            return giftCardService.Delete(giftCardId);
        }
        #endregion
    }
}
