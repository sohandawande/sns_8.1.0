using System.Data;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.Admin
{
    /// <summary>
    /// Provides method to hold values of the Dash board items
    /// </summary>
    public class DashboardAdmin : ZNodeBusinessBase
    {
        private int _TotalProducts;
        private int _TotalCategories;
        private int _TotalInventory;
        private int _TotalOutOfStock;
        private decimal _YTDRevenue;
        private decimal _MTDRevenue;
        private decimal _TodayRevenue;
        private int _ShippedToday;
        private int _ReturnedToday;
        private int _TotalOrders;
        private int _TotalOrdersMTD;
        private int _TotalNewOrders;
        private int _TotalPaymentPendingOrders;
        private int _TotalSubmittedOrders;
        private int _TotalShippedOrders;
        private int _TotalAccounts;
        private int _TotalAccountsMTD;
        private int _TotalPages;
        private int _TotalShippingOptions;
        private string _PaymentGateway;
        private int _TotalPendingServiceRequests = 0;
        private int _TotalReviewsToApprove = 0;
        private int _TotalAffiliatesToApprove = 0;
        private int _TotalLowInventoryItems = 0;
        private int _TotalFailedLoginsToday = 0;
        private int _TotalDeclinedTransactions = 0;
        private string _StatusMessage;
        private DataTable _PopularSearchKeywords = new DataTable();
        private int _TotalLowInventoryThanReorder = 0;

        /// <summary>
        /// Gets or sets the total products.
        /// </summary>
        public int TotalProducts
        {
            get { return this._TotalProducts; }
            set { this._TotalProducts = value; }
        }

        /// <summary>
        /// Gets or sets the total categories.
        /// </summary>
        public int TotalCategories
        {
            get { return this._TotalCategories; }
            set { this._TotalCategories = value; }
        }       

        /// <summary>
        /// Gets or sets the total inventory items.
        /// </summary>
        public int TotalInventory
        {
            get { return this._TotalInventory; }
            set { this._TotalInventory = value; }
        }
        
        /// <summary>
        /// Gets or sets the total out of stock items.
        /// </summary>
        public int TotalOutOfStock
        {
            get { return this._TotalOutOfStock; }
            set { this._TotalOutOfStock = value; }
        }
        
        /// <summary>
        /// Gets or sets the yeat-to-date revenue.
        /// </summary>
        public decimal YTDRevenue
        {
            get { return this._YTDRevenue; }
            set { this._YTDRevenue = value; }
        }
        
        /// <summary>
        /// Gets or sets the month-todate revenue.
        /// </summary>
        public decimal MTDRevenue
        {
            get { return this._MTDRevenue; }
            set { this._MTDRevenue = value; }
        }
        
        /// <summary>
        /// Gets or sets the today revenue.
        /// </summary>
        public decimal TodayRevenue
        {
            get { return this._TodayRevenue; }
            set { this._TodayRevenue = value; }
        }
        
        /// <summary>
        /// Gets or sets the number of items shipped today.
        /// </summary>
        public int ShippedToday
        {
            get { return this._ShippedToday; }
            set { this._ShippedToday = value; }
        }
        
        /// <summary>
        /// Gets or sets the number of items returned today.
        /// </summary>
        public int ReturnedToday
        {
            get { return this._ReturnedToday; }
            set { this._ReturnedToday = value; }
        }
        
        /// <summary>
        /// Gets or sets the total number of orders.
        /// </summary>
        public int TotalOrders
        {
            get { return this._TotalOrders; }
            set { this._TotalOrders = value; }
        }
        
        /// <summary>
        /// Gets or sets the total number orders for month-to-date.
        /// </summary>
        public int TotalOrdersMTD
        {
            get { return this._TotalOrdersMTD; }
            set { this._TotalOrdersMTD = value; }
        }        

        /// <summary>
        /// Gets or sets the total number of new orders.
        /// </summary>
        public int TotalNewOrders
        {
            get { return this._TotalNewOrders; }
            set { this._TotalNewOrders = value; }
        }
        
        /// <summary>
        /// Gets or sets the total payment pending orders.
        /// </summary>
        public int TotalPaymentPendingOrders
        {
            get { return this._TotalPaymentPendingOrders; }
            set { this._TotalPaymentPendingOrders = value; }
        }
        
        /// <summary>
        /// Gets or sets the total submitted orders.
        /// </summary>
        public int TotalSubmittedOrders
        {
            get { return this._TotalSubmittedOrders; }
            set { this._TotalSubmittedOrders = value; }
        }
        
        /// <summary>
        /// Gets or sets the total shipped orders.
        /// </summary>
        public int TotalShippedOrders
        {
            get { return this._TotalShippedOrders; }
            set { this._TotalShippedOrders = value; }
        }

        /// <summary>
        /// Gets or sets the total accounts.
        /// </summary>
        public int TotalAccounts
        {
            get { return this._TotalAccounts; }
            set { this._TotalAccounts = value; }
        }
                
        /// <summary>
        /// Gets or sets the total accounts month-to-date.
        /// </summary>
        public int TotalAccountsMTD
        {
            get { return this._TotalAccountsMTD; }
            set { this._TotalAccountsMTD = value; }
        }
        
        /// <summary>
        /// Gets or sets the total pages.
        /// </summary>
        public int TotalPages
        {
            get { return this._TotalPages; }
            set { this._TotalPages = value; }
        }
        
        /// <summary>
        /// Gets or sets the total shipping options.
        /// </summary>
        public int TotalShippingOptions
        {
            get { return this._TotalShippingOptions; }
            set { this._TotalShippingOptions = value; }
        }
        
        /// <summary>
        /// Gets or sets the payment gateway name
        /// </summary>
        public string PaymentGateway
        {
            get { return this._PaymentGateway; }
            set { this._PaymentGateway = value; }
        }

        /// <summary>
        /// Gets or sets the total pending service request.
        /// </summary>
        public int TotalPendingServiceRequests
        {
            get { return this._TotalPendingServiceRequests; }
            set { this._TotalPendingServiceRequests = value; }
        }
        
        /// <summary>
        /// Gets or sets the total reviews to approve.
        /// </summary>
        public int TotalReviewsToApprove
        {
            get { return this._TotalReviewsToApprove; }
            set { this._TotalReviewsToApprove = value; }
        }
               
        /// <summary>
        /// Gets or sets the total affiliate orders to approve.
        /// </summary>
        public int TotalAffiliatesToApprove
        {
            get { return this._TotalAffiliatesToApprove; }
            set { this._TotalAffiliatesToApprove = value; }
        }
        
        /// <summary>
        /// Gets or sets the total low inventory items 
        /// </summary>
        public int TotalLowInventoryItems
        {
            get { return this._TotalLowInventoryItems; }
            set { this._TotalLowInventoryItems = value; }
        }
        
        /// <summary>
        /// Gets or sets the total failed logins today.
        /// </summary>
        public int TotalFailedLoginsToday
        {
            get { return this._TotalFailedLoginsToday; }
            set { this._TotalFailedLoginsToday = value; }
        }

	    ///<summary>
	    /// Gets or Sets the Email opt-in Customers
	    ///</summary>
	    public int EmailOptInCustomers { get; set; }
		/// <summary>
		/// Gets or sets the total declined transactions.
		/// </summary>
        public int TotalDeclinedTransactions
        {
            get { return this._TotalDeclinedTransactions; }
            set { this._TotalDeclinedTransactions = value; }
        }
               
        /// <summary>
        /// Gets or sets the license status message.
        /// </summary>
        public string StatusMessage
        {
            get { return this._StatusMessage; }
            set { this._StatusMessage = value; }
        }
              
        /// <summary>
        /// Gets or sets the pupular search keywords data table.
        /// </summary>
        public DataTable PopularSearchKeywords
        {
            get { return this._PopularSearchKeywords; }
            set { this._PopularSearchKeywords = value; }
        }

        /// <summary>
        /// Gets or sets the total low inventory than reorder.
        /// </summary>
        public int TotalLowInventoryThanReorder
        {
            get { return this._TotalLowInventoryThanReorder; }
            set { this._TotalLowInventoryThanReorder = value; }
        }

        /// <summary>
        /// Gets dashboard Item details.
        /// </summary>
        public void GetDashboardItems(int portalID)
        {
            DashboardHelper dash = new DashboardHelper();
            DataSet ds = dash.GetDashboardItemsByPortal(portalID);

            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                try
                {
                    this.TotalProducts = (int)dr["TotalProducts"];
                    this.TotalCategories = (int)dr["TotalCategories"];
                    this.TotalInventory = (int)dr["TotalInventory"];
                    this.TotalOutOfStock = (int)dr["TotalOutOfStock"];

                    this.YTDRevenue = (decimal)dr["YTDRevenue"];
                    this.MTDRevenue = (decimal)dr["MTDRevenue"];
                    this.TodayRevenue = (decimal)dr["TodayRevenue"];
                    this.ShippedToday = (int)dr["ShippedToday"];
                    this.ReturnedToday = (int)dr["ReturnedToday"];
                    this.TotalOrders = (int)dr["TotalOrders"];
                    this.TotalOrdersMTD = (int)dr["TotalOrdersMTD"];

                    this.TotalNewOrders = (int)dr["TotalNewOrders"];
                    this.TotalPaymentPendingOrders = (int)dr["TotalPaymentPendingOrders"];
                    this.TotalSubmittedOrders = (int)dr["TotalSubmittedOrders"];
                    this.TotalShippedOrders = (int)dr["TotalShippedOrders"];
                    this.TotalAccounts = (int)dr["TotalAccounts"];
                    this.TotalAccountsMTD = (int)dr["TotalAccountsMTD"];

                    this.TotalPages = (int)dr["TotalPages"];
                    this.TotalShippingOptions = (int)dr["TotalShippingOptions"];
                    this.PaymentGateway = (string)dr["PaymentGateway"];

                    // Alerts Section
                    this.TotalPendingServiceRequests = (int)dr["TotalPendingServiceRequests"];
                    this.TotalDeclinedTransactions = (int)dr["TotalDeclinedTransactions"];
                    this.TotalReviewsToApprove = (int)dr["TotalReviewstoApprove"];
                    this.TotalAffiliatesToApprove = (int)dr["TotalAffiliatesToApprove"];
                    this.TotalFailedLoginsToday = (int)dr["TotalLoginFailedToday"];
                    this.TotalLowInventoryItems = (int)dr["TotalLowInventoryItems"];
					this.EmailOptInCustomers = (int)dr["EmailOptInCustomers"];
                    this._TotalLowInventoryThanReorder = (int)dr["TotalLowInventorythanReorder"];
                    this.PopularSearchKeywords = ds.Tables[1];

                    // Truncate the data so that it doesn't wrap on the page.
                    foreach (DataRow row in this.PopularSearchKeywords.Rows)
                    {
                        if (row[0].ToString().Length > 20)
                        {
                            row[0] = row[0].ToString().Substring(0, 20) + "...";
                        }
                    }
                }
                catch
                {
                    throw;
                }
            }

            // Get license status
            ZNode.Libraries.Framework.Business.ZNodeLicenseManager licenseManager = new ZNodeLicenseManager();
            licenseManager.Validate();

            this.StatusMessage = licenseManager.GetStatusDescription();
        }
    }
}
