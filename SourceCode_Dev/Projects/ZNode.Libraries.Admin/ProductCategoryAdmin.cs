using System.Data;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.Admin
{
    /// <summary>
    /// Provides methods to manage ProductCategory object
    /// </summary>
    public class ProductCategoryAdmin : ZNodeBusinessBase
    {
        #region Public Methods

        /// <summary>
        /// Get all product categories.
        /// </summary>
        /// <returns>Returns the list of ProductCategory object.</returns>
        public TList<ZNode.Libraries.DataAccess.Entities.ProductCategory> GetAllProductCategory()
        {
            ZNode.Libraries.DataAccess.Service.ProductCategoryService productCategoryService = new ProductCategoryService();
            TList<ProductCategory> productCategoryList = productCategoryService.GetAll();

            return productCategoryList;
        }

        /// <summary>
        /// Get a product category by product category Id.
        /// </summary>
        /// <param name="productCategoryId">Product category Id to get the product category object.</param>
        /// <returns>Returns the ProductCategory object.</returns>
        public ProductCategory GetByProductCategoryID(int productCategoryId)
        {
            ZNode.Libraries.DataAccess.Service.ProductCategoryService productCategoryService = new ProductCategoryService();
            ProductCategory productCategoryList = productCategoryService.GetByProductCategoryID(productCategoryId);

            return productCategoryList;
        }

        /// <summary>
        /// Get a product category by product Id.
        /// </summary>
        /// <param name="productId">Product Id to get the product category dataset.</param>
        /// <returns>Returns the product category dataset.</returns>
        public DataSet GetByProductID(int productId, int? portalId= null)
        {
            ZNode.Libraries.DataAccess.Custom.ProductCategoryHelper _productCategoryHelper = new ProductCategoryHelper();
            return _productCategoryHelper.GetByProductID(productId,portalId);
        }
        
        /// <summary>
        /// Get a product category by category Id
        /// </summary>
        /// <param name="categoryId">Category Id to get the product category dataset.</param>
        /// <returns>Returns the product category dataset.</returns>
        public DataSet GetByCategoryID(int categoryId)
        {
            ZNode.Libraries.DataAccess.Custom.ProductCategoryHelper productCategoryHelper = new ProductCategoryHelper();
            return productCategoryHelper.GetByCategoryID(categoryId);
        }

        /// <summary>
        /// Checks to see if a product exists for the given Category.
        /// </summary>
        /// <param name="productId">Product Id to check the product category existence.</param>
        /// <param name="categoryId">Category Id to check the product category existence.</param>
        /// <returns>Returns true if exists otherwise false.</returns>
        public bool ProductExists(int productId, int categoryId)
        {
            ProductCategoryService productCategoryService = new ProductCategoryService();
            ProductCategoryQuery query = new ProductCategoryQuery();
            query.Append(ProductCategoryColumn.ProductID, productId.ToString());
            query.Append(ProductCategoryColumn.CategoryID, categoryId.ToString());

            TList<ProductCategory> productCategory = productCategoryService.Find(query);
            if (productCategory.Count > 0)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Checks to see if a product exists for the given Category.
        /// </summary>
        /// <param name="productIds">Product Ids to check the product category existence.</param>
        /// <param name="categoryId">Category Id to check the product category existence.</param>
        /// <returns>Returns true if exists otherwise false.</returns>
        public bool ProductExists(string productIds, int categoryId)
        {
            ProductCategoryService productCategoryService = new ProductCategoryService();
            ProductCategoryQuery query = new ProductCategoryQuery();
            string[] productIdsArray = productIds.Split(',');
            query.AppendIn(ProductCategoryColumn.ProductID, productIdsArray);
            query.Append(ProductCategoryColumn.CategoryID, categoryId.ToString());
            TList<ProductCategory> productCategory = productCategoryService.Find(query);
            if (productCategory.Count > 0)
            {               
                return true;
            }

            return false;
        }

        /// <summary>
        /// Add new a product Category
        /// </summary>
        /// <param name="productCategory">ProductCategory object to insert.</param>
        /// <returns>Returns true if added otherwise false.</returns>
        public bool Add(ProductCategory productCategory)
        {
            ZNode.Libraries.DataAccess.Service.ProductCategoryService productCategoryService = new ProductCategoryService();
            return productCategoryService.Insert(productCategory);
        }

        /// <summary>
        /// Insert a Product Category
        /// </summary>
        /// <param name="productId">ProductId to insert.</param>
        /// <param name="categoryId">CategoryId to insert.</param>
        /// <returns>Returns true if inserted otherwise false.</returns>
        public bool Insert(int productId, int categoryId)
        {
            ProductCategoryHelper productCategoryHelper = new ProductCategoryHelper();
            return productCategoryHelper.Insert(productId, categoryId);
        }

        /// <summary>
        /// Removes a product category.
        /// </summary>
        /// <param name="productId">Product Id of the product category.</param>
        /// <param name="categoryId">Category Id of the product category.</param>
        /// <returns>Returns true if removed otherwise false.</returns>
        public bool RemoveProduct(int productId, int categoryId)
        {
            ProductCategoryHelper productCategoryHelper = new ProductCategoryHelper();
            return productCategoryHelper.RemoveProduct(productId, categoryId);
        }

        /// <summary>
        /// Removes a product from a Product Category.
        /// </summary>
        /// <param name="productId">Product Id of the product category.</param>        
        /// <returns>Returns true if removed otherwise false.</returns>
        public bool RemoveProductCategory(int productId)
        {
            ProductCategoryHelper productCategoryHelper = new ProductCategoryHelper();
            return productCategoryHelper.RemoveProductCategory(productId);
        }

        /// <summary>
        /// Update a product category.
        /// </summary>
        /// <param name="productCategory">ProductCategory object to insert.</param>
        /// <returns>Returns true if updated otherwise false.</returns>
        public bool Update(ProductCategory productCategory)
        {
            ZNode.Libraries.DataAccess.Service.ProductCategoryService productCategoryService = new ProductCategoryService();
            return productCategoryService.Update(productCategory);
        }

        /// <summary>
        /// Delete a product category.
        /// </summary>
        /// <param name="productCategory">ProductCategory object to delete.</param>
        /// <returns>Returns true if deleted otherwise false.</returns>
        public bool Delete(ProductCategory productCategory)
        {
            ZNode.Libraries.DataAccess.Service.ProductCategoryService productCategoryService = new ProductCategoryService();
            return productCategoryService.Delete(productCategory);
        }

        /// <summary>
        /// Delete a product category by Id.
        /// </summary>
        /// <param name="productCategoryId">Product category Id to delete.</param>
        /// <returns>Returns true if deleted otherwise false.</returns>
        public bool Delete(int productCategoryId)
        {
            ZNode.Libraries.DataAccess.Service.ProductCategoryService productCategoryService = new ProductCategoryService();
            return productCategoryService.Delete(productCategoryId);
        }
        #endregion
    }
}
