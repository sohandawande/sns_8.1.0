using System;
using System.Data;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;
using System.Web;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.ECommerce.Utilities;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;

namespace ZNode.Libraries.Admin
{
    /// <summary>
    /// Represents the znode product image type enum
    /// </summary>
    public enum ZNodeProductImageType
    {
        /// <summary>
        /// Image is Alternate Image type
        /// </summary>
        AlternateImage = 1,

        /// <summary>
        /// Image is swatch image.
        /// </summary>
        Swatch = 2
    }

    /// <summary>
    /// Provides methods to manage catalog products
    /// </summary>
    public class ProductAdmin : ZNodeBusinessBase
    {        
        #region AddOnValue Inventory
        /// <summary>
        /// Update the SKU quantity.
        /// </summary>
        /// <param name="sku">SKU to update the quantity.</param>
        /// <param name="quantity">Quantity value to udpate.</param>
        /// <param name="reorderLevel">Reorder level of the SKU.</param>
        /// <returns>Returns true if udpated otherwise false.</returns>
        public static bool UpdateQuantity(string sku, int quantity, int reorderLevel)
        {
            try
            {
                if (!string.IsNullOrEmpty(sku))
                {
                    SKUInventoryService skuInventoryService = new SKUInventoryService();
                    SKUInventory skuInventory = skuInventoryService.GetBySKU(sku);

                    if (skuInventory != null)
                    {
                        skuInventory.QuantityOnHand = quantity;
                        skuInventory.ReOrderLevel = reorderLevel;

                        return skuInventoryService.Update(skuInventory);
                    }
                }
            }
            catch (Exception ex)
            {
                ZNodeLogging.LogMessage(ex.Message);
            }

            return false;
        }

        /// <summary>
        /// Get the AddOn inventory.
        /// </summary>
        /// <param name="addOnValue">AddOn value object to get the inventory.</param>
        /// <returns>Returns the SKU inventory object.</returns>
        public static SKUInventory GetAddOnInventory(AddOnValue addOnValue)
        {
            if (addOnValue != null)
            {
                SKUInventoryService skuInventoryService = new SKUInventoryService();

                SKUInventory addOnInventory = null;
                addOnInventory = skuInventoryService.GetBySKU(addOnValue.SKU);

                if (addOnInventory != null)
                {
                    return addOnInventory;
                }
            }

            return null;
        }

        /// <summary>
        /// Get the quantity by addon value.
        /// </summary>
        /// <param name="addOnValue">AddOn value onject to get the SKU inventory.</param>
        /// <returns>Returns the quantity on hand value.</returns>
        public static int GetQuantity(AddOnValue addOnValue)
        {
            if (addOnValue != null)
            {
                return ProductAdmin.GetQuantity(addOnValue.SKU);
            }

            return 0;
        }

        /// <summary>
        /// Get the quantity by sku.
        /// </summary>
        /// <param name="sku">SKU value to get the </param>
        /// <returns>Returns the quantity on hand value.</returns>
        public static int GetQuantity(string sku)
        {
            if (!string.IsNullOrEmpty(sku))
            {
                SKUInventoryService skuInventoryService = new SKUInventoryService();

                SKUInventory addOnInventory = null;
                addOnInventory = skuInventoryService.GetBySKU(sku);

                if (addOnInventory != null)
                {
                    return addOnInventory.QuantityOnHand.GetValueOrDefault(0);
                }
            }

            return 0;
        }

        /// <summary>
        /// Update the SKU inventory value.
        /// </summary>
        /// <param name="addOnValue">AddOn value to get the sku.</param>
        /// <param name="quantity">Quantity value to update.</param>        
        public static void UpdateQuantity(AddOnValue addOnValue, int quantity)
        {
            if (addOnValue != null)
            {
                // Update Quantity into Inventory Table.
                SKUInventoryService skuInventoryService = new SKUInventoryService();

                SKUInventory addOnInventory = null;
                addOnInventory = skuInventoryService.GetBySKU(addOnValue.SKU);

                if (addOnInventory == null)
                {
                    addOnInventory = new SKUInventory();
                    addOnInventory.EntityState = EntityState.Added;
                }
                else
                {
                    addOnInventory.EntityState = EntityState.Changed;
                }

                addOnInventory.SKU = addOnValue.SKU;
                addOnInventory.QuantityOnHand = quantity;

                skuInventoryService.Save(addOnInventory);
            }
        }

        /// <summary>
        /// Update quantity and reorder value.
        /// </summary>
        /// <param name="addOnValue">AddOn value to get the sku.</param>
        /// <param name="quantity">Quantity value to update.</param>        
        /// <param name="reorderLevel">Reorder value to update.</param>
        public static void UpdateQuantity(AddOnValue addOnValue, int quantity, int reorderLevel)
        {
            if (addOnValue != null && !string.IsNullOrEmpty(addOnValue.SKU))
            {
                // Update Quantity into Inventory Table.
                SKUInventoryService skuInventoryService = new SKUInventoryService();

                SKUInventory addOnInventory = null;
                addOnInventory = skuInventoryService.GetBySKU(addOnValue.SKU);

                if (addOnInventory == null)
                {
                    addOnInventory = new SKUInventory();
                    addOnInventory.EntityState = EntityState.Added;
                }
                else
                {
                    addOnInventory.EntityState = EntityState.Changed;
                }

                addOnInventory.SKU = addOnValue.SKU;
                addOnInventory.QuantityOnHand = quantity;

                if (reorderLevel > 0)
                {
                    addOnInventory.ReOrderLevel = reorderLevel;
                }
                else
                {
                    addOnInventory.ReOrderLevel = null;
                }

                skuInventoryService.Save(addOnInventory);
            }
        }

        #endregion

        #region Product Locale

        /// <summary>
        /// Get the Locale information for the product that has other languages.
        /// </summary>
        /// <param name="productNum">ProductNum to get the locale Ids.</param>
        /// <returns>Returns the locale Ids for the productNum</returns>
        public static DataSet GetLocaleIdsByProductId(string productNum)
        {
            ProductHelper productHelper = new ProductHelper();
            return productHelper.GetLocaleIdsByProductId(productNum, ZNodeConfigManager.SiteConfig.PortalID);
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Get all products for a portal
        /// </summary>
        /// <returns>Returns list of Product object.</returns>
        public TList<Product> GetAllProducts()
        {
            ZNode.Libraries.DataAccess.Service.ProductService productService = new ZNode.Libraries.DataAccess.Service.ProductService();
            TList<ZNode.Libraries.DataAccess.Entities.Product> productList = productService.GetAll();

            return productList;
        }

        /// <summary>
        ///  Get a product by product Id
        /// </summary>
        /// <param name="productId">Product Id to get product object.</param>
        /// <returns>Returns the Product object for the product Id.</returns>
        public Product GetByProductId(int productId)
        {
            ZNode.Libraries.DataAccess.Service.ProductService productService = new ZNode.Libraries.DataAccess.Service.ProductService();
            Product product = productService.GetByProductID(productId);

            return product;
        }


		///<summary>
		/// Get OrderLine Item By supplier id
		/// 
		/// </summary>
		public int GetTotalOrderLineItemBySKU(int? supplierId, int portalId)
		{
			ProductHelper productHelper = new ProductHelper();
			return productHelper.GetTotalOrderLineItemBySKU(supplierId, portalId);
			
		}

        /// <summary>
        ///  Get the next available product for review.
        /// </summary>
        /// <param name="reviewState">ZNodeReviewState enum object</param>
        /// <returns>Returns top 1 Product if product found otherwise returns null.</returns>
        public Product GetNextProductToReview(ZNodeProductReviewState reviewState)
        {
            ZNode.Libraries.DataAccess.Service.ProductService productService = new ZNode.Libraries.DataAccess.Service.ProductService();
            int totalCount = 0;
            TList<Product> productList = productService.GetByReviewStateID(Convert.ToInt32(reviewState), 0, 1, out totalCount);
            if (productList.Count > 0)
            {
                return productList[0];
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Search the product.
        /// </summary>
        /// <param name="keyword">Keyword to search.</param>
        /// <param name="manufacturerId">Manufacturere Id to search.</param>
        /// <param name="productTypeId">Product type Id to search.</param>
        /// <param name="categoryId">Category Id to search.</param>
        /// <returns>Returns the product search result dataset.</returns>
        public DataSet SearchProductsByKeyword(string keyword, string manufacturerId, string productTypeId, string categoryId)
        {
            ProductHelper productHelper = new ProductHelper();
            return productHelper.SearchProductsByKeyword(keyword, manufacturerId, productTypeId, categoryId);
        }

        /// <summary>
        /// Search the product.
        /// </summary>
        /// <param name="name">Product name to search.</param>
        /// <param name="productNum">Product number to search.</param>
        /// <param name="sku">Sku to search.</param>
        /// <param name="manufacturerId">Manufacturer Id to search.</param>
        /// <param name="productTypeId">Product Type Id to search.</param>
        /// <param name="categoryId">Category Id to search.</param>        
        /// <param name="catalogId">Catalog Id to search.</param>
        /// <param name="supplierId">Supplier Id (Vendor) to search.</param>
        /// <returns>Returns the product search result dataset.</returns>
        public DataSet SearchProduct(string name, string productNum, string sku, string manufacturerId, string productTypeId, string categoryId, string catalogId, string supplierId)
        {
            ProductHelper productHelper = new ProductHelper();
            return productHelper.SearchProduct(name, productNum, sku, manufacturerId, productTypeId, categoryId, catalogId, supplierId);
        }
        /// <summary>
        /// Search the product.
        /// </summary>
        /// <param name="name">Product name to search.</param>
        /// <param name="productNum">Product number to search.</param>
        /// <param name="sku">Sku to search.</param>
        /// <param name="manufacturerId">Manufacturer Id to search.</param>
        /// <param name="productTypeId">Product Type Id to search.</param>
        /// <param name="categoryId">Category Id to search.</param>        
        /// <param name="catalogId">Catalog Id to search.</param>   
        /// <returns>Returns the product search result dataset.</returns>
        public DataSet SearchProduct(string name, string productNum, string sku, string manufacturerId, string productTypeId, string categoryId, string catalogId)
        {
            ProductHelper productHelper = new ProductHelper();
            return productHelper.SearchProduct(name, productNum, sku, manufacturerId, productTypeId, categoryId, catalogId);
        }
        /// <summary>
        /// Search Bundle product.
        /// </summary>
        /// <param name="name">Product name to search.</param>
        /// <param name="productNum">Product number to search.</param>
        /// <param name="sku">Sku to search.</param>
        /// <param name="manufacturerId">Manufacturer Id to search.</param>
        /// <param name="productTypeId">Product Type Id to search.</param>
        /// <param name="categoryId">Category Id to search.</param>        
        /// <param name="catalogId">Catalog Id to search.</param>
        /// <param name="supplierId">Supplier Id to search.</param>
        /// <param name="portalId">Portal id to search.</param>
        /// <returns>Returns the product search result dataset.</returns>
        //public DataSet SearchBundleProduct(string name, string productNum, string sku, string manufacturerId, string productTypeId, string categoryId, string catalogId, string supplierId, int portalId)
        //{
        //    ProductHelper productHelper = new ProductHelper();
        //   // return productHelper.SearchBundleProduct(name, productNum, sku, manufacturerId, productTypeId, categoryId, catalogId,supplierId,portalId);
        //}

        /// <summary>
        /// Search the product.
        /// </summary>
        /// <param name="name">Product name to search.</param>
        /// <param name="productNum">Product number to search.</param>
        /// <param name="sku">Sku to search.</param>
        /// <param name="manufacturerId">Manufacturer Id to search.</param>
        /// <param name="productTypeId">Product Type Id to search.</param>
        /// <param name="categoryId">Category Id to search.</param>        
        /// <param name="catalogId">Catalog Id to search.</param>
        /// <param name="supplierId">Supplier Id to search.</param>
        /// <param name="portalId">Portal id to search.</param>
        /// <returns>Returns the product search result dataset.</returns>
        public DataSet SearchProduct(string name, string productNum, string sku, string manufacturerId, string productTypeId, string categoryId, string catalogId, string supplierId, int portalId)
        {
            ProductHelper productHelper = new ProductHelper();
            return productHelper.SearchProduct(name, productNum, sku, manufacturerId, productTypeId, categoryId, catalogId, supplierId, portalId);
        }

        #region New function created for product searching in franchise admin
        /// <summary>
        /// Search the product.
        /// </summary>
        /// <param name="name">Product name to search.</param>
        /// <param name="productNum">Product number to search.</param>
        /// <param name="sku">Sku to search.</param>
        /// <param name="manufacturerId">Manufacturer Id to search.</param>
        /// <param name="productTypeId">Product Type Id to search.</param>
        /// <param name="categoryId">Category Id to search.</param>        
        /// <param name="catalogId">Catalog Id to search.</param>
        /// <param name="supplierId">Supplier Id to search.</param>
        /// <param name="portalId">Portal id to search.</param>
        /// <returns>Returns the product search result dataset.</returns>
        public DataSet SearchFranchiseProduct(string name, string productNum, string sku, string manufacturerId, string productTypeId, string categoryId, string catalogId, string supplierId, int portalId)
        {
            ProductHelper productHelper = new ProductHelper();
            return productHelper.FranchiseSearchProduct(name, productNum, sku, manufacturerId, productTypeId, categoryId, catalogId, supplierId, portalId);
        }
        #endregion
        /// <summary>
        /// Search the vendor products.
        /// </summary>
        /// <param name="name">Product name to search.</param>
        /// <param name="productId">Product Id to search.</param>
        /// <param name="vendor">Vendor name to search.</param>
        /// <param name="vendorId">Vendor Id Id to search.</param>
        /// <param name="productState">Product state Id to search.</param>
        /// <param name="coid">2CO product Id.</param>
        /// <returns>Returns the vendor product search result dataset.</returns>
        public DataSet SearchVendorProduct(string name, string productId, string vendor, string vendorId, string productState, string coid)
        {
            ProductHelper productHelper = new ProductHelper();
            DataSet searchResultDataSet = productHelper.SearchVendorProduct(name, productId, vendor, vendorId, productState, coid);

            return searchResultDataSet;
        }

        /// <summary>
        /// Search the product.
        /// </summary>
        /// <param name="name">Product name to search.</param>
        /// <param name="productNum">ProductNum to search.</param>
        /// <param name="sku">Sku to search.</param>
        /// <param name="manufacturerName">Manufacturer name to search.</param>
        /// <param name="productTypeId">Product Type Id to search.</param>
        /// <param name="categoryName">Category name to search.</param>
        /// <param name="portalIds">Comma seperated portal Id to search.</param>
        /// <returns>Returns the product search result dataset.</returns>
        public DataSet SearchProductByNames(string name, string productNum, string sku, string manufacturerName, string productTypeId, string categoryName, string portalIds)
        {
            ProductHelper productHelper = new ProductHelper();
            DataSet searchResultDataSet = productHelper.SearchProductByNames(name, productNum, sku, manufacturerName, productTypeId, categoryName, portalIds);

            return searchResultDataSet;
        }

        /// <summary>
        /// Search the Portals.
        /// </summary>
       /// <returns>Returns the portal search result dataset.</returns>
        public DataSet GetPortalByProduct(string portalIds)
        {
            ProductHelper productHelper = new ProductHelper();
            DataSet searchResultDataSet = productHelper.GetPortalByProduct(portalIds);

            return searchResultDataSet;
        }

        /// <summary>
        /// Get product name by productId
        /// </summary>
        /// <param name="productId">Product Id to get the product object.</param>
        /// <returns>Returns the product name.</returns>
        public string GetProductName(int productId)
        {
            string productName = string.Empty;

            if (productId > 0)
            {
                ProductService productService = new ProductService();
                Product product = productService.GetByProductID(productId);

                if (product != null)
                {
                    productName = product.Name;
                }
            }

            return productName;
        }

        /// <summary>
        /// Get product by catalog Id.
        /// </summary>
        /// <param name="catalogId">Catalog Id to get the products for the catalog.</param>
        /// <returns>Returns the catalog products dataset.</returns>
        public DataSet GetProductsByCatalogID(int catalogId)
        {
            ProductHelper productHelper = new ProductHelper();
            return productHelper.GetProductsByCatalogID(catalogId);
        }

        /// <summary>
        ///  Add a new product to data source.
        /// </summary>
        /// <param name="product">Product object to add.</param>
        /// <param name="productId">Return the added product Id (out)</param>
        /// <returns>Returns true if inserted otherwise false.</returns>
        public bool Add(Product product, out int productId)
        {
            productId = 0;
            ZNode.Libraries.DataAccess.Service.ProductService productService = new ZNode.Libraries.DataAccess.Service.ProductService();

            bool isAdded = productService.Insert(product);
            if (!isAdded)
            {
                return false;
            }

            productId = product.ProductID;

            // Clear all cache memory.
            CacheManager.Clear();

            return isAdded;
        }

        /// <summary>
        ///  Add new product with SKU
        /// </summary>
        /// <param name="product">Product object to add.</param>
        /// <param name="sku">SKU object to add.</param>
        /// <param name="productId">Returns the added product Id.</param>
        /// <param name="skuId">Returns the added SKU Id.</param>
        /// <returns>Returns true if inserted otherwise false.</returns>
        public bool Add(Product product, SKU sku, out int productId, out int skuId)
        {
            productId = 0;
            skuId = 0;

            ZNode.Libraries.DataAccess.Service.ProductService productService = new ZNode.Libraries.DataAccess.Service.ProductService();

            bool isAdded = productService.Insert(product);
            if (!isAdded)
            {
                return false;
            }

            productId = product.ProductID;

            // Add SKU
            sku.ProductID = product.ProductID;
            SKUAdmin skuAdmin = new SKUAdmin();
            isAdded = skuAdmin.Add(sku);
            skuId = sku.SKUID;

            // Clear all cache memory.
            CacheManager.Clear();

            return isAdded;
        }

        /// <summary>
        /// Update the product
        /// </summary>
        /// <param name="product">Product object to update.</param>        
        /// <returns>Returns true if updated otherwise false.</returns>
        public bool Update(Product product)
        {
            ZNode.Libraries.DataAccess.Service.ProductService productService = new ZNode.Libraries.DataAccess.Service.ProductService();
            bool isUpdated = productService.Update(product);

            // Clear all cache memory.
            CacheManager.Clear();

            return isUpdated;
        }

        /// <summary>
        /// Update a product and product SKU
        /// </summary>
        /// <param name="product">Product object to update.</param>
        /// <param name="sku">SKU object to update.</param>
        /// <returns>Returns true if updated otherwise false.</returns>
        public bool Update(Product product, SKU sku)
        {
            ZNode.Libraries.DataAccess.Service.ProductService productService = new ZNode.Libraries.DataAccess.Service.ProductService();
            bool isUpdated = productService.Update(product);

            // Update Product SKU
            SKUAdmin skuAdmin = new SKUAdmin();
            skuAdmin.Update(sku);

            // Clear all cache memory.
            CacheManager.Clear();

            return isUpdated;
        }

        /// <summary>
        ///  Delete a product by product Id.
        /// </summary>
        /// <param name="productId">Product Id to delete the product object.</param>        
        /// <returns>Returns true if deleted otherwise false.</returns>
        public bool Delete(int productId)
        {
            ProductService prodService = new ProductService();
            return prodService.Delete(productId);
        }

        /// <summary>
        ///  Delete a product by product Id and clear the product cache.
        /// </summary>
        /// <param name="productId">Product Id to delete the product object.</param>        
        /// <returns>Returns true if deleted otherwise false.</returns>
        public bool DeleteByProductID(int productId)
        {
            bool isDeleted = false;

            ProductHelper productHelper = new ProductHelper();
            isDeleted = productHelper.DeleteByProductId(productId);

            // Clear all cache memory.
            CacheManager.Clear();

            return isDeleted;
        }

        /// <summary>
        /// Add category for the product
        /// </summary>
        /// <param name="productCategory">Product category onject to add.</param>        
        public void AddProductCategory(ProductCategory productCategory)
        {
            ProductCategoryAdmin productCategoryAdmin = new ProductCategoryAdmin();
            ProductCategoryQuery filters = new ProductCategoryQuery();
            ProductCategoryService prodCategoryService = new ProductCategoryService();

            filters.Append(ProductCategoryColumn.CategoryID, productCategory.CategoryID.ToString());
            filters.Append(ProductCategoryColumn.ProductID, productCategory.ProductID.ToString());

            TList<ProductCategory> prodcategoryList = prodCategoryService.Find(filters.GetParameters());

            if (prodcategoryList.Count == 0)
            {
                productCategoryAdmin.Add(productCategory);
            }
        }

        /// <summary>
        /// Update the product category for the product and clear the cache manager.
        /// </summary>
        /// <param name="productCategory">ProductCategory object to update.</param>
        public void UpdateProductCategory(ProductCategory productCategory)
        {
            ProductCategoryAdmin productCategoryAdmin = new ProductCategoryAdmin();
            productCategoryAdmin.Update(productCategory);

            // Clear all cache memory.
            CacheManager.Clear();
        }

        /// <summary>
        /// Deletes all the categories for a product and clear the cache manager.
        /// </summary>
        /// <param name="productId">Product Id to get the list of product category.</param>        
        public void DeleteProductCategories(int productId)
        {
            ProductCategoryService productCategoryService = new ProductCategoryService();

            TList<ProductCategory> catList = productCategoryService.GetByProductID(productId);

            productCategoryService.Delete(catList);

            // Clear all cache memory.
            CacheManager.Clear();
        }
        
        /// <summary>
        /// Get product details by product Id
        /// </summary>
        /// <param name="productId">Product Id to get the product dataset.</param>
        /// <returns>Returns the product details dataset.</returns>
        public DataSet GetProductDetails(int productId)
        {
            ProductHelper productHelper = new ProductHelper();
            DataSet productDataSet = productHelper.GetProductDetails(productId);

            return productDataSet;
        }

        /// <summary>
        /// Gets attribute names by product type Id.
        /// </summary>
        /// <param name="productTypeId">Product type Id to get the attribute type dataset.</param>
        /// <returns>Returns the attribute name dataset.</returns>
        public DataSet GetAttributeNamesByProductTypeid(int productTypeId)
        {
            AttributeTypeHelper attributeTypeHelper = new AttributeTypeHelper();
            DataSet attributeTypeDataSet = attributeTypeHelper.GetByProductTypeID(productTypeId);

            return attributeTypeDataSet;
        }

        /// <summary>
        /// Get the product category by product Id.
        /// </summary>
        /// <param name="productId">Product Id to get the product category list.</param>
        /// <returns>Returns list of ProductCategory object.</returns>
        public TList<ProductCategory> GetProductCategoriesByProductID(int productId)
        {
            ProductCategoryService productCategoryService = new ProductCategoryService();
            TList<ProductCategory> productcategoryList = productCategoryService.GetByProductID(productId);

            return productcategoryList;
        }

        /// <summary>
        /// Get the product category by product Id.
        /// </summary>
        /// <param name="productId">Product Id to get the product category list.</param>
        /// <returns>Returns list of ProductCategory object.</returns>
        public ProductCategory GetProductCategoryByProductID(int productId)
        {
            ProductCategoryService productCategoryService = new ProductCategoryService();
            TList<ProductCategory> prodcategoryList = productCategoryService.GetByProductID(productId);

            if (prodcategoryList.Count > 0)
            {
                return prodcategoryList[0];
            }

            return null;
        }

        /// <summary>
        /// Get attributes by attribute type Id.
        /// </summary>
        /// <param name="attributeTypeId">Attribute type Id to get the attribute type dataset.</param>
        /// <returns>Returns the attribute dataset.</returns>
        public DataSet GetByAttributeTypeID(int attributeTypeId)
        {
            AttributeTypeAdmin attributeTypeAdmin = new AttributeTypeAdmin();
            DataSet attributeTypeDataSet = attributeTypeAdmin.GetByAttributeTypeID(attributeTypeId).ToDataSet(true);

            return attributeTypeDataSet;
        }

        /// <summary>
        /// Get attribute type by product type Id.
        /// </summary>
        /// <param name="productTypeId">Product type Id to get the attribute type dataset.</param>
        /// <returns>Returns the attribute type dataset.</returns>
        public DataSet GetAttributeTypeByProductTypeID(int productTypeId)
        {
            AttributeTypeHelper attributeTypeHelper = new AttributeTypeHelper();
            DataSet attributeTypeDataSet = attributeTypeHelper.GetAttributeTypesByProductTypeID(productTypeId);

            return attributeTypeDataSet;
        }

        /// <summary>
        /// Get attribute type by product type Id.
        /// </summary>
        /// <param name="attributeTypeId">Attribute type Id to get the attribute type.</param>
        /// <param name="productId">Product Id to get the attribute.</param>
        /// <returns>Returns the attribute type dataset.</returns>
        public DataSet GetAttributesByAttributeTypeIdandProductID(int attributeTypeId, int productId)
        {
            AttributeTypeHelper attributeTypeHelper = new AttributeTypeHelper();
            DataSet attributeTypeDataSet = attributeTypeHelper.GetAttributesByAttributeTypeIdandProductID(attributeTypeId, productId);

            return attributeTypeDataSet;
        }

        /// <summary>
        /// Get category dataset by product Id.
        /// </summary>
        /// <param name="productId">Product Id to get the category dataset.</param>
        /// <returns>Returns the category dataset.</returns>
        public DataSet Get_CategoryByProductID(int productId)
        {
            ProductHelper productHelper = new ProductHelper();
            DataSet categoryDataSet = productHelper.Get_CategoryByProductID(productId);

            return categoryDataSet;
        }

        /// <summary>
        /// Get attributes by product type Id.
        /// </summary>
        /// <param name="productTypeId">Product type Id to get the attribute dataset.</param>
        /// <returns>Returns the attribute dataset.</returns>
        public DataSet GetAttributesByProductTypeID(int productTypeId)
        {
            AttributeTypeHelper attributeTypeHelper = new AttributeTypeHelper();
            DataSet attributeTypeDataSet = attributeTypeHelper.GetAttributesByProductTypeID(productTypeId);

            return attributeTypeDataSet;
        }

        /// <summary>
        /// Get attributes by portal Id
        /// </summary>
        /// <param name="portalId">Portal Id to get the product attribute.</param>
        /// <returns>Returns list of ProductAttribute object.</returns>
        public TList<ProductAttribute> GetAttributesByPortalID(string portalId)
        {
            AttributeTypeAdmin ttributeTypeAdmin = new AttributeTypeAdmin();
            TList<AttributeType> attributeTypeList = ttributeTypeAdmin.GetAttributeTypePortalId(portalId);

            TList<ProductAttribute> aroductAttributeList = new TList<ProductAttribute>();
            ProductAttributeService productAttributeService = new ProductAttributeService();

            foreach (AttributeType attributeType in attributeTypeList)
            {
                aroductAttributeList.AddRange(productAttributeService.GetByAttributeTypeId(attributeType.AttributeTypeId));
            }

            return aroductAttributeList;
        }

        /// <summary>
        /// Get the category hierarchical path (Upto 3 Levels)
        /// </summary>
        /// <param name="categoryName">Root category name.</param>
        /// <param name="parentCategoryName1">Parent category name1.</param>
        /// <param name="parentCategoryName2">Parent category name2.</param>
        /// <returns>Returns the category hierarchical path(upto 3 levels)</returns>
        public string GetCategorypath(string categoryName, string parentCategoryName1, string parentCategoryName2)
        {
            return ProductHelper.GetCategoryPath(categoryName, parentCategoryName1, parentCategoryName2);
        }

        /// <summary>
        /// Get the attributes count by product type Id.
        /// </summary>
        /// <param name="productTypeId">Product type Id to get the attribute type dataset.</param>        
        /// <returns>Returns the attribute count.</returns>
        public int GetAttributeCount(int productTypeId)
        {
            AttributeTypeHelper attributeTypeHelper = new AttributeTypeHelper();
            DataSet attributeTypeDataSet = attributeTypeHelper.GetByProductTypeID(productTypeId);

            int attributeCount = 0;
            if (attributeTypeDataSet != null)
            {
                attributeCount = attributeTypeDataSet.Tables[0].Rows.Count;
            }

            attributeTypeDataSet.Dispose();

            return attributeCount;
        }

        /// <summary>
        /// Get the product Id by name.
        /// </summary>
        /// <param name="name">Product name to get the product Id.</param>
        /// <returns>Returns the produc Id.</returns>
        public int GetProductIdByName(string name)
        {
            if (name.Length > 0)
            {
                ProductService productService = new ProductService();
                ProductQuery query = new ProductQuery();
                query.Append(ProductColumn.Name, name);

                TList<Product> productList = productService.Find(query.GetParameters());

                if (productList.Count > 0)
                {
                    productList.Sort("ProductID Desc");
                    return productList[0].ProductID;
                }
            }

            return 0;
        }

        /// <summary>
        /// Get products by portal Ids.
        /// </summary>
        /// <param name="portalIds">Comma seperated portal Ids.</param>
        /// <returns>Retusns the list of Product object.</returns>
        public TList<Product> GetProductByPortalID(string portalIds)
        {
            ProductService productService = new ProductService();
            ProductQuery query = new ProductQuery();
            query.AppendIn(ProductColumn.PortalID, portalIds.Split(','));
            return productService.Find(query.GetParameters());
        }

        #endregion

        #region Public Methods - Related to Product Tier Pricing
        /// <summary>
        /// Get all product tiers
        /// </summary>
        /// <returns>Returns list of ProductTier object.</returns>
        public TList<ProductTier> GetAllProductTiers()
        {
            ProductTierService productTierService = new ProductTierService();
            return productTierService.GetAll();
        }

        /// <summary>
        /// Get all product tier pricing by product Id
        /// </summary>
        /// <param name="productId">Product Id to get the list of prodiuct tier object.</param>
        /// <returns>Return list of ProductTier object.</returns>
        public TList<ProductTier> GetTieredPricingByProductId(int productId)
        {
            ProductTierService productTierService = new ProductTierService();
            return productTierService.GetByProductID(productId);
        }

        /// <summary>
        /// Get ProductTier by product tier Id.
        /// </summary>
        /// <param name="productTierId">ProductTier Id to get the producttier object.</param>
        /// <returns>Returns the ProiductTier object.</returns>
        public ProductTier GetByProductTierId(int productTierId)
        {
            ProductTierService productTierService = new ProductTierService();
            return productTierService.GetByProductTierID(productTierId);
        }

        /// <summary>
        /// Add new ProductTier
        /// </summary>
        /// <param name="productTier">ProductTier object to insert.</param>
        /// <returns>Returns true if inserted otherwise false.</returns>
        public bool AddProductTier(ProductTier productTier)
        {
            ProductTierService productTierService = new ProductTierService();
            return productTierService.Insert(productTier);
        }

        /// <summary>
        /// Update the ProductTier.
        /// </summary>
        /// <param name="productTier">ProductTier object to update.</param>
        /// <returns>Returns true if update otherwise false.</returns>
        public bool UpdateProductTier(ProductTier productTier)
        {
            ProductTierService productTierService = new ProductTierService();
            return productTierService.Update(productTier);
        }

        /// <summary>
        /// Deletes ProductTier by Id
        /// </summary>
        /// <param name="productTierId">ProductTier Id to delete the ProductTier object.</param>
        /// <returns>Returns true if deleted otherwise false.</returns>
        public bool DeleteProductTierById(int productTierId)
        {
            ProductTierService productTierService = new ProductTierService();
            return productTierService.Delete(productTierId);
        }
        #endregion

        #region Public Methods - Related to Digigtal Asset
        /// <summary>
        /// Get digital asset list for a product
        /// </summary>
        /// <param name="productId">Product Id to get digital asset for the product Id.</param>
        /// <returns>Returns list of DigitalAsset object.</returns>
        public TList<DigitalAsset> GetDigitAssetByProductId(int productId)
        {
            DigitalAssetService DigitalAssetService = new DigitalAssetService();
            return DigitalAssetService.GetByProductID(productId);
        }

        /// <summary>
        /// Add a new digital asset.
        /// </summary>
        /// <param name="digitalAsset">DigitalAsset object to insert.</param>
        /// <returns>Returns true if inserted otherwise false.</returns>
        public bool AddDigitalAsset(DigitalAsset digitalAsset)
        {
            DigitalAssetService DigitalAssetService = new DigitalAssetService();
            bool isAdded = DigitalAssetService.Insert(digitalAsset);

            return isAdded;
        }

        /// <summary>
        /// Update a digital asset 
        /// </summary>
        /// <param name="digitalAsset">DigitalAsset object to update.</param>
        /// <returns>Returns true if updated otherwise false.</returns>
        public bool UpdateDigitalAsset(DigitalAsset digitalAsset)
        {
            DigitalAssetService DigitalAssetService = new DigitalAssetService();
            bool isUpdated = DigitalAssetService.Update(digitalAsset);

            return isUpdated;
        }

        /// <summary>
        /// Delete a digital asset
        /// </summary>
        /// <param name="digitalAssetId">DigitalAsset Id to delete the DigitalAsset object.</param>
        /// <returns>Returns true if deleted otherwise false.</returns>
        public bool DeleteDigitalAsset(int digitalAssetId)
        {
            DigitalAssetService DigitalAssetService = new DigitalAssetService();
            bool isDeleted = DigitalAssetService.Delete(digitalAssetId);

            return isDeleted;
        }
        #endregion

        #region Public Methods - Related to ProductHighlight
        /// <summary>
        ///  Returns a Product highlights by productid
        /// </summary>
        /// <param name="productId">Product Id to get the product highlights.</param>
        /// <returns>Return the deep loaded list if ProductHighlight object.</returns>
        public TList<ProductHighlight> GetHighlightsByProductID(int productId)
        {
            ZNode.Libraries.DataAccess.Service.ProductHighlightService ProdHighlightService = new ZNode.Libraries.DataAccess.Service.ProductHighlightService();

            TList<ProductHighlight> productHighlightList = ProdHighlightService.GetByProductID(productId);
            ProdHighlightService.DeepLoad(productHighlightList, false, DeepLoadType.IncludeChildren, typeof(Highlight));

            return productHighlightList;
        }

        /// <summary>
        /// Add a product highlight
        /// This method will associate the highlight with a product
        /// </summary>
        /// <param name="productHighlight">ProductHighlight object to insert.</param>
        /// <returns>Returns true if inserted otherwise false.</returns>
        public bool AddProductHighlight(ProductHighlight productHighlight)
        {
            ZNode.Libraries.DataAccess.Service.ProductHighlightService ProdHighlightService = new ZNode.Libraries.DataAccess.Service.ProductHighlightService();
            return ProdHighlightService.Insert(productHighlight);
        }

        /// <summary>
        /// Delete a product highlight by Id.
        /// </summary>
        /// <param name="productHighlightId">Product highlight Id to delete the ProductHighLight object.</param>
        /// <returns>Returns true if deleted otherwise false.</returns>
        public bool DeleteProductHighlight(int productHighlightId)
        {
            ZNode.Libraries.DataAccess.Service.ProductHighlightService ProdHighlightService = new ZNode.Libraries.DataAccess.Service.ProductHighlightService();
            return ProdHighlightService.Delete(productHighlightId);
        }

        /// <summary>
        /// Check if this highlight is already assocaited with this product.
        /// </summary>
        /// <param name="productId">Product Id to get the list of associated highlight objects.</param>
        /// <param name="highlightId">Highlight Id to check the product already associated with the highlight.</param>
        /// <returns>Returns true if already associated otherwise false.</returns>
        public bool IsHighlightExists(int productId, int highlightId)
        {
            ZNode.Libraries.DataAccess.Service.ProductHighlightService ProdHighlightService = new ZNode.Libraries.DataAccess.Service.ProductHighlightService();
            TList<ProductHighlight> entityList = ProdHighlightService.GetByProductID(productId);

            if (entityList.Count > 0)
            {
                entityList.Filter = "HighlightID = " + highlightId;

                if (entityList.Count == 1)
                {
                    return true;
                }
            }

            return false;
        }
        #endregion

        #region Delete Catalog Methods
        /// <summary>
        /// Empty catalog data.
        /// </summary>
        /// <param name="userName">Catalog deleted user name. </param>
        /// <returns>Returns true if catalog items deleted otherwise false.</returns>
        public bool EmptyCatalog(string userName)
        {
            ProductHelper productHelper = new ProductHelper();
            return productHelper.EmptyCatalog(userName);
        }
        #endregion       

        #region Copy Product

        /// <summary>
        /// Copy the given product into new one.
        /// </summary>
        /// <param name="productId">Product ID to be copied.</param>
        /// <returns>New product id</returns>
        public int CopyProduct(int productId)
        {
            Product product = GetByProductId(Convert.ToInt32(productId));
            Product copyProduct = (Product)product.Clone();
            copyProduct.Name = string.Format("Copy of {0}", copyProduct.Name);
            copyProduct.ProductID = 0;
            int copyProductId = 0;
            Add(copyProduct, out copyProductId);

            // Copy Skus
            SKUAdmin skuAdmin = new SKUAdmin();
            TList<SKU> skus = skuAdmin.GetByProductID(product.ProductID);
            TList<SKU> copySkus = (TList<SKU>)skus.Clone();
            //copySkus.ForEach(delegate(SKU sku) { sku.SKUID = 0; sku.ProductID = copyProductId; skuAdmin.Add(sku); });
            copySkus.ForEach(delegate(SKU sku)
            {
                string inventorySku = sku.SKU + GetUniqueKey();
                SKUInventory skuInventory = new SKUInventory();
                SKUInventoryService inventoryService = new SKUInventoryService();
                skuInventory = inventoryService.GetBySKU(sku.SKU);             
              
                if (!Equals(skuInventory,null))
                {
                    skuInventory.SKUInventoryID = 0;                    
                    skuInventory.SKU = inventorySku;
                    skuInventory.QuantityOnHand = skuInventory.QuantityOnHand;
                }                
               
                inventoryService.Insert(skuInventory);

                int skuId = sku.SKUID;
                sku.SKU = inventorySku;
                sku.SKUID = 0;
                sku.ProductID = copyProductId;
                skuAdmin.Add(sku);

                SKUAttributeService saService = new SKUAttributeService();
                TList<SKUAttribute> skuAttributes = saService.GetBySKUID(skuId);
                TList<SKUAttribute> copySkuAttributes = (TList<SKUAttribute>)skuAttributes.Clone();
                copySkuAttributes.ForEach(delegate(SKUAttribute skuAttribute)
                {
                    skuAttribute.SKUID = sku.SKUID;
                    skuAttribute.SKUAttributeID = 0;
                    saService.Insert(skuAttribute);
                });
            });


            // Copy AddOns
            ProductAddOnService paService = new ProductAddOnService();
            TList<ProductAddOn> addOns = paService.GetByProductID(product.ProductID);
            TList<ProductAddOn> copyAddOns = (TList<ProductAddOn>)addOns.Clone();
            copyAddOns.ForEach(delegate(ProductAddOn addOn) { addOn.ProductAddOnID = 0; addOn.ProductID = copyProductId; paService.Insert(addOn); });

            // Copy Images
            ProductImageService piService = new ProductImageService();
            TList<ProductImage> images = piService.GetByProductID(product.ProductID);
            TList<ProductImage> copyImages = (TList<ProductImage>)images.Clone();
            copyImages.ForEach(delegate(ProductImage image) { image.ProductImageID = 0; image.ProductID = copyProductId; piService.Insert(image); });

            // Copy Highlights
            ProductHighlightService phService = new ProductHighlightService();
            TList<ProductHighlight> hightlights = phService.GetByProductID(product.ProductID);
            TList<ProductHighlight> copyHightlights = (TList<ProductHighlight>)hightlights.Clone();
            copyHightlights.ForEach(delegate(ProductHighlight hightlight) { hightlight.ProductHighlightID = 0; hightlight.ProductID = copyProductId; phService.Insert(hightlight); });

            // Copy Tier Pricing
            ProductTierService ptService = new ProductTierService();
            TList<ProductTier> tiers = ptService.GetByProductID(product.ProductID);
            TList<ProductTier> copyTiers = (TList<ProductTier>)tiers.Clone();
            copyTiers.ForEach(delegate(ProductTier tier) { tier.ProductTierID = 0; tier.ProductID = copyProductId; ptService.Insert(tier); });

            return copyProductId;
        }

        private static string GetUniqueKey()
        {
            char[] chars = new char[62];
            string strCharacters;
            strCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            chars = strCharacters.ToCharArray();
            int size = 4;
            byte[] data = new byte[1];
            RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
            crypto.GetNonZeroBytes(data);
            size = 4;
            data = new byte[size];
            crypto.GetNonZeroBytes(data);
            StringBuilder result = new StringBuilder(size);
            foreach (byte bytedata in data)
            {
                result.Append(chars[bytedata % (chars.Length - 1)]);
            }
            SetNumberInUniqueKey(result);
            return result.ToString();
        }

        private static void SetNumberInUniqueKey(StringBuilder result)
        {
            Random rnd = new Random();
            int dice = rnd.Next(1, 9);
            result.Append(Convert.ToString(dice));
        }

        #endregion

        #region Update Review state
        /// <summary>
        /// Update Product Review State
        /// </summary>
        /// <param name="product"></param>
        public int? UpdateReviewStateID(Product product, int portalId, int itemId, int accountId=0, string userName="")
        {
            if (product.IsDirty)
            {
                // Modify product status to store default when add or edit.
                Portal portal = ZNode.Libraries.DataAccess.Data.DataRepository.PortalProvider.GetByPortalID(portalId);
                if (!Equals(portal, null))
                {
                    if (!product.IsNew)
                    {
                        // Construct changed column list.
                        System.Text.StringBuilder dirtyColumns = new System.Text.StringBuilder();
                        foreach (string columnName in Enum.GetNames(typeof(ProductColumn)))
                        {
                            if (product.IsPropertyChanged(columnName))
                            {
                                if (dirtyColumns.Length > 0)
                                {
                                    dirtyColumns.Append(", ");
                                }

                                dirtyColumns.Append(columnName);
                            }
                        }

                        this.UpdateReviewHistory(dirtyColumns.ToString(), (ZNodeProductReviewState)portal.DefaultProductReviewStateID.GetValueOrDefault((int)ZNodeProductReviewState.Approved), itemId, accountId, userName);
                    }

                    return portal.DefaultProductReviewStateID.GetValueOrDefault((int)ZNodeProductReviewState.Approved);
                }
            }

            return product.ReviewStateID;
        }

        /// <summary>
        /// Update Product Review State
        /// </summary>
        /// <param name="product"></param>
        public int? UpdateReviewStateID(int portalId, int itemId, string dirtyColumns, int accountId = 0, string userName = "")
        {
                // Modify product status to store default when add or edit.
                Portal portal = ZNode.Libraries.DataAccess.Data.DataRepository.PortalProvider.GetByPortalID(portalId);

                this.UpdateReviewHistory(dirtyColumns, (ZNodeProductReviewState)portal.DefaultProductReviewStateID.GetValueOrDefault((int)ZNodeProductReviewState.Approved), itemId,accountId,userName);

                return portal.DefaultProductReviewStateID.GetValueOrDefault((int)ZNodeProductReviewState.Approved);
        }

        /// <summary>
        /// Update Product Review State
        /// </summary>
        /// <param name="product"></param>
        public int? UpdateReviewStateID(SKU sku, int portalId, int itemId)
        {
            if (sku.IsDirty)
            {
                // Modify product status to store default when add or edit.
                Portal portal = ZNode.Libraries.DataAccess.Data.DataRepository.PortalProvider.GetByPortalID(portalId);

                if (!sku.IsNew)
                {
                    // Construct changed column list.
                    System.Text.StringBuilder dirtyColumns = new System.Text.StringBuilder();
                    foreach (string columnName in Enum.GetNames(typeof(SKUColumn)))
                    {
                        if (sku.IsPropertyChanged(columnName))
                        {
                            if (dirtyColumns.Length > 0)
                            {
                                dirtyColumns.Append(", ");
                            }

                            dirtyColumns.Append(string.Format("SKU.{0}", columnName));
                        }
                    }

                    this.UpdateReviewHistory(dirtyColumns.ToString(), (ZNodeProductReviewState)portal.DefaultProductReviewStateID.GetValueOrDefault((int)ZNodeProductReviewState.Approved), itemId);

                }

                return portal.DefaultProductReviewStateID.GetValueOrDefault((int)ZNodeProductReviewState.Approved);
            }

            return null;
        }

        /// <summary>
        /// Update the product review history
        /// </summary>
        /// <param name="changedFields">Comma seperated changed field.s</param>
        public void UpdateReviewHistory(string changedFields, ZNodeProductReviewState changedState, int itemId, int accountId=0,string userName="")
        {
            // Add entry to Product Review history table.
            ProductReviewHistoryAdmin productReviewHistoryAdmin = new ProductReviewHistoryAdmin();
            ProductReviewHistory productReviewHistory = new ProductReviewHistory();
            productReviewHistory.ProductID = itemId;
            productReviewHistory.VendorID = (!Equals(ZNodeUserAccount.CurrentAccount(), null)) ? ZNodeUserAccount.CurrentAccount().AccountID : accountId;
            productReviewHistory.LogDate = DateTime.Now;
            productReviewHistory.Username = (!string.IsNullOrEmpty(System.Web.HttpContext.Current.User.Identity.Name)) ? System.Web.HttpContext.Current.User.Identity.Name : userName;
            productReviewHistory.Status = changedState.ToString();
            productReviewHistory.Reason = string.Empty;
            productReviewHistory.Description = HttpUtility.HtmlEncode(changedFields);
            bool isSuccess = productReviewHistoryAdmin.Add(productReviewHistory);
        }
        #endregion
    }
}
