using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.Admin
{
    /// <summary>
    /// Provides methods to manage product manufacturers
    /// </summary>
    public class ManufacturerAdmin : ZNodeBusinessBase
    {
        #region Public Methods

        /// <summary>
        /// Get all manufacturers
        /// </summary>        
        /// <returns>Returns list of Manufacturer object.</returns>
        public TList<Manufacturer> GetAll()
        {
            ZNode.Libraries.DataAccess.Service.ManufacturerService manufacturerService = new ManufacturerService();
            TList<ZNode.Libraries.DataAccess.Entities.Manufacturer> manufacturerList = manufacturerService.GetAll();

            return manufacturerList;
        }

        /// <summary>
        /// Get the manufacturer by Id
        /// </summary>
        /// <param name="manufacturerId">Manufacturer Id to get the Manufacturer object</param>
        /// <returns>Returns the Manufacturer object for the Id.</returns>
        public Manufacturer GetByManufactureId(int manufacturerId)
        {
            ZNode.Libraries.DataAccess.Service.ManufacturerService manufacturerService = new ManufacturerService();
            Manufacturer manufacturerList = manufacturerService.GetByManufacturerID(manufacturerId);

            return manufacturerList;
        }

        /// <summary>
        /// Get Manufacturers data.
        /// </summary>
        /// <param name="name">Manufacturer name to search.</param>
        /// <returns>Returns the Manufacturer search data.</returns>
        public DataSet GetManufacturerBySearchData(string name)
        {
            ManufacturerHelper manufacturerHelper = new ManufacturerHelper();
            return manufacturerHelper.GetManufacturerBySearchData(name);
        }

        /// <summary>
        /// Insert a new Manufacturer
        /// </summary>
        /// <param name="manufacturer">Manufacturer object to insert.</param>
        /// <returns>Returns true if inserted else false.</returns>
        public bool Insert(Manufacturer manufacturer)
        {
            ZNode.Libraries.DataAccess.Service.ManufacturerService manufacturerService = new ManufacturerService();
            bool isAdded = manufacturerService.Insert(manufacturer);

            return isAdded;
        }

        /// <summary>
        /// update the Manufacturer
        /// </summary>
        /// <param name="manufacturer">Manufacturer object to update.</param>
        /// <returns>Returns true if updated else false.</returns>
        public bool Update(Manufacturer manufacturer)
        {
            ZNode.Libraries.DataAccess.Service.ManufacturerService manufacturerService = new ManufacturerService();

            bool isUpdated = manufacturerService.Update(manufacturer);
            return isUpdated;
        }

        /// <summary>
        /// Delete the Manufacturer
        /// </summary>
        /// <param name="ManufacturerId">Manufacturer Id to delete the Manufacturer object from data source.</param>
        /// <returns>Returns true if deleted else false.</returns>
        public bool Delete(int ManufacturerId)
        {
            ZNode.Libraries.DataAccess.Service.ManufacturerService manufacturerService = new ManufacturerService();
            bool isDeleted = manufacturerService.Delete(ManufacturerId);

            return isDeleted;
        }

        #endregion
    }
}
