using System.Data;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.Admin
{
    public class FacetAdmin : ZNodeBusinessBase
    {
        /// <summary>
        /// Get all facet control type.
        /// </summary>
        /// <returns>Returns list of FacetControlType object.</returns>
        public TList<FacetControlType> GetAllFacetControlType()
        {
            FacetControlTypeService facetControlTypeService = new FacetControlTypeService();
            TList<FacetControlType> facetControlTypeList = facetControlTypeService.GetAll();

            return facetControlTypeList;
        }
         

        /// <summary>
        /// Get a FacetGroup for this group type
        /// </summary>
        /// <param name="facetGroupId">Facet group Id to get the facet group object.</param>
        /// <returns>Returns the FacetGroup object.</returns>
        public FacetGroup GetByFacetGroupID(int facetGroupId)
        {
            FacetGroupService facetGroupService = new FacetGroupService();
            FacetGroup facetGroup = facetGroupService.GetByFacetGroupID(facetGroupId);
            if (facetGroup != null)
            {
                facetGroup.FacetGroupCategoryCollection = new FacetGroupCategoryService().GetByFacetGroupID(facetGroupId);
            }
            return facetGroup;
        }
         


        /// <summary>
        /// Get a facet by facet Id 
        /// </summary>
        /// <param name="facetId">Facet Id to get the facet object.</param>
        /// <returns>Returns the Facet object.</returns>
        public Facet GetByFacetID(int facetId)
        {
            FacetService facetService = new FacetService();
            Facet facet = facetService.GetByFacetID(facetId);

            return facet;
        }

        /// <summary>
        /// Get a facet group by facet group Id.
        /// </summary>
        /// <param name="facetGroupId">Facet group Id to get the list of associated facets.</param>
        /// <returns>Returns list of Facet object associated with the facet group.</returns>
        public TList<Facet> GetFacetsByFacetGroupID(int facetGroupId)
        {
            FacetService facetService = new FacetService();
            TList<Facet> FacetList = facetService.GetByFacetGroupID(facetGroupId);

            return FacetList;
        }

        /// <summary>
        /// Add a facet to data soruce.
        /// </summary>
        /// <param name="facet">Facet object to insert.</param>
        /// <returns>Returns true if inserted otherwise false.</returns>
        public bool AddFacet(Facet facet)
        {
            FacetService facetService = new FacetService();
            bool isAdded = facetService.Insert(facet);

            return isAdded;
        }

        /// <summary>
        /// Update Facet
        /// </summary>
        /// <param name="facet">Facet object to update.</param>
        /// <returns>Returns true if udpated otherwise false.</returns>
        public bool UpdateFacet(Facet facet)
        {
            FacetService facetService = new FacetService();
            bool isUpdated = facetService.Update(facet);

            return isUpdated;
        }

        /// <summary>
        /// Delete Facet
        /// </summary>
        /// <param name="facet">Facet object to delete.</param>
        /// <returns>Returns true if deleted otherwise false.</returns>
        public bool DeleteFacet(Facet facet)
        {
            FacetService facetService = new FacetService();
            bool isDeleted = facetService.Delete(facet);

            return isDeleted;
        }

        /// <summary>
        /// Add a facet group.
        /// </summary>
        /// <param name="facetGroup">Facet group object to add.</param>
        /// <returns>Returns true if inserted otherwise false.</returns>
        public bool AddFacetGroup(FacetGroup facetGroup)
        {
            FacetGroupService facetGroupService = new FacetGroupService();

            bool isAdded = facetGroupService.Insert(facetGroup);

            return isAdded;
        }

        /// <summary>
        /// Update facet group.
        /// </summary>
        /// <param name="facetGroup">Facet group object to add.</param>
        /// <returns>Returns true if udpated otherwise false.</returns>
        public bool UpdateFacetGroup(FacetGroup facetGroup)
        {
            FacetGroupService facetGroupService = new FacetGroupService();
            bool isUpdated = facetGroupService.Update(facetGroup);

            return isUpdated;
        }

        /// <summary>
        /// Delete facet group by facet group Id.
        /// </summary>
        /// <param name="facetGroup">Facet group object to add.</param>
        /// <returns>Returns true if deleted otherwise false.</returns>
        public bool DeleteFacetGroup(FacetGroup facetGroup)
        {
            FacetGroupService facetGroupService = new FacetGroupService();
            bool isDeleted = facetGroupService.Delete(facetGroup);

            return isDeleted;
        }

        /// <summary>
        /// Add FacetGroupCategory
        /// </summary>
        /// <param name="facetGroupCategory">FacetGroupCategory object to insert.</param>
        /// <returns>Returns true if inserted otherwise false.</returns>
        public bool AddFacetGroupCategory(FacetGroupCategory facetGroupCategory)
        {
            FacetGroupCategoryService facetGroupCategoryService = new FacetGroupCategoryService();
            bool isAdded = facetGroupCategoryService.Insert(facetGroupCategory);

            return isAdded;
        }

        /// <summary>
        /// Delete the FacetGroupCategory by facet group category.
        /// </summary>
        /// <param name="facetGroupCategory">FacetGroupCategory object to insert.</param>
        /// <returns>Returns true if deleted otherwise false.</returns>
        public bool DeleteFacetGroupCategegory(FacetGroupCategory facetGroupCategory)
        {
            FacetGroupCategoryService facetGroupCategoryService = new FacetGroupCategoryService();
            bool isDeleted = facetGroupCategoryService.Delete(facetGroupCategory);

            return isDeleted;
        }

        /// <summary>
        /// Search the facet
        /// </summary>
        /// <param name="name">Facet name to search.</param>
        /// <param name="localeId">Locale Id to search.</param>
        /// <returns>Returns the facet search result dataset.</returns>
        public DataSet GetFacetGroupBySearchData(string name, int localeId)
        {
            FacetHelper facetHelper = new FacetHelper();
			return facetHelper.GetFacetGroupBySearchData(name, localeId);
        }

        /// <summary>
        /// Get locale by associated facets dataset.
        /// </summary>        
        /// <returns>Returns the locale by associated dataset.</returns>
        public DataSet GetLocaleByFacetGroup()
        {
            FacetHelper facetHelper = new FacetHelper();
			return facetHelper.GetLocaleByAssociatedFacets();
        }

        /// <summary>
        /// Get categories by FacetGroup Id
        /// </summary>
        /// <param name="facetGroupId">Facet group Id to search.</param>
        /// <param name="catalogId">Catalog Id to search.</param>
        /// <param name="isFacetGroup">Indicates whether facet group or not.</param>
        /// <returns>Returns the categories dataset.</returns>
        public DataSet GetCategoriesByFacetGroupID(int facetGroupId, int catalogId, bool isFacetGroup)
        {
            FacetHelper facetHelper = new FacetHelper();
			return facetHelper.GetCategoriesByFacetGroupID(facetGroupId, catalogId, isFacetGroup);
        }

        /// <summary>
        /// Get facet product Sku by product Id.
        /// </summary>
        /// <param name="productId">Product Id to get the facet product Sku.</param>
        /// <returns>Returns list of FacetProductSKU object.</returns>
        public TList<FacetProductSKU> GetFacetsByProductId(int productId)
        {
            FacetProductSKUService service = new FacetProductSKUService();
            return service.GetByProductID(productId);
        }
    }
}
