using System.Data;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.Admin
{
    /// <summary>
    /// Provides methods to manage product cross sell items
    /// </summary>
    public class ProductCrossSellAdmin : ZNodeBusinessBase
    {
        #region Public Methods
        /// <summary>
        /// Get all related items by product Id.
        /// </summary>
        /// <param name="productId">Product Id to get related items.</param>
        /// <returns>Returns related items dataset.</returns>
        public DataSet GetByProductID(int productId)
        {
            ProductCrossSellHelper productCrossHelper = new ProductCrossSellHelper();
            return productCrossHelper.GetByProductID(productId);
        }

        /// <summary>
        /// Get the related items to the product.
        /// </summary>
        /// <param name="productId">Product Id</param>
        /// <returns>Returns list of ProductCrossSell object related to the product</returns>
        public TList<ProductCrossSell> GetRelatedItemsByProductId(int productId)
        {
            ProductCrossSellService productCrossSellService = new ProductCrossSellService();
            return productCrossSellService.GetByProductId(productId);
        }

        /// <summary>
        /// Add New related product item for a Product
        /// </summary>
        /// <param name="productId">Master Product Id</param>
        /// <param name="relatedProductId">Related product Id</param>
        /// <returns>Returns true if inserted otherwise false.</returns>
        public bool Insert(int productId, int relatedProductId)
        {
            // Instance for a ProductCrossSellerHelper class
            ProductCrossSellHelper productCrossHelper = new ProductCrossSellHelper();
            bool isAdded = productCrossHelper.Insert(productId, relatedProductId);

            return isAdded;
        }

        /// <summary>
        /// Delete a related product item for a Product
        /// </summary>
        /// <param name="relatedProductId">Relate product Id to detele.</param>
        /// <param name="productId">Product Id to remove the related item.</param>        
        /// <returns>Returns true if deleted otherwise false.</returns>
        public bool Delete(int relatedProductId, int productId)
        {
            ProductCrossSellHelper productCrossSellHelper = new ProductCrossSellHelper();
            bool isDeleted = productCrossSellHelper.RemoveProduct(relatedProductId, productId);
            return isDeleted;
        }
        #endregion
    }
}
