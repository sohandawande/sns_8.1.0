using System;
using System.Data;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;

namespace ZNode.Libraries.Admin
{
    /// <summary>
    /// Provides methods to manage Product highlights
    /// </summary>
    public class HighlightAdmin
    {
        #region Public Methods
        /// <summary>
        ///  Returns all highlights
        /// </summary>
        /// <returns>Returns list of Highlight object.</returns>
        public TList<Highlight> GetAll()
        {
            ZNode.Libraries.DataAccess.Service.HighlightService ProdHighlightService = new ZNode.Libraries.DataAccess.Service.HighlightService();
            TList<Highlight> entityCollection = ProdHighlightService.GetAll();
            return entityCollection;
        }

        /// <summary>
        /// Get a Highlight by highlight id
        /// </summary>
        /// <param name="highlightId">Highlight Id to get the highlight object</param>
        /// <returns>Returns the Highlight object.</returns>
        public Highlight GetByHighlightID(int highlightId)
        {
            ZNode.Libraries.DataAccess.Service.HighlightService ProdHighlightService = new ZNode.Libraries.DataAccess.Service.HighlightService();
            return ProdHighlightService.GetByHighlightID(highlightId);
        }

        /// <summary>
        /// Add a new product Highlight
        /// </summary>
        /// <param name="highlight">Highlight object to add.</param>
        /// <returns>Returns true if inserted else false.</returns>
        public bool Add(Highlight highlight)
        {
            ZNode.Libraries.DataAccess.Service.HighlightService HighlightService = new ZNode.Libraries.DataAccess.Service.HighlightService();

            return HighlightService.Insert(highlight);
        }

        /// <summary>
        /// Update a product Highlight
        /// </summary>
        /// <param name="highlight">Highlight object to update.</param>
        /// <returns>Returns true if updated else false.</returns>
        public bool Update(Highlight highlight)
        {
            ZNode.Libraries.DataAccess.Service.HighlightService HighlightService = new ZNode.Libraries.DataAccess.Service.HighlightService();
            return HighlightService.Update(highlight);
        }

        /// <summary>
        /// Delete a product Highlight
        /// </summary>
        /// <param name="highlightId">Highlight Id to delete from the data source.</param>
        /// <returns>Returns true if deleted else false.</returns>
        public bool Delete(int highlightId)
        {
            try
            {
                ZNode.Libraries.DataAccess.Service.HighlightService HighlightService = new ZNode.Libraries.DataAccess.Service.HighlightService();

                return HighlightService.Delete(highlightId);
            }
            catch (Exception ex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.ToString());
            }

            return false;
        }

        /// <summary>
        /// Get all the HighLightType objects
        /// </summary>
        /// <returns>Returns list of HighLightType object.</returns>
        public TList<ZNode.Libraries.DataAccess.Entities.HighlightType> GetAllHighLightType()
        {
            ZNode.Libraries.DataAccess.Service.HighlightTypeService highLightTypeService = new HighlightTypeService();
            TList<ZNode.Libraries.DataAccess.Entities.HighlightType> highLightTypeList = highLightTypeService.GetAll();

            return highLightTypeList;
        }

        /// <summary>
        /// Get a highlight for Given Inputs
        /// </summary>
        /// <param name="name">Highlight name to search.</param>
        /// <param name="highlightTypeId">Highlight type Id to serach.</param>        
        /// <returns>Returns the Highlight search result data set.</returns>
        public DataSet SearchProductHighlight(string name, int highlightTypeId)
        {
            ProductHelper Helper = new ProductHelper();
            return Helper.SearchProductHighlight(name, highlightTypeId);            
        }

        /// <summary>
        /// Get all the Highlight for this portal
        /// </summary>
        /// <returns>Returns all higlights data set.</returns>
        public DataSet GetAllHighlight()
        {
            ProductHelper Helper = new ProductHelper();
            return Helper.GetHighlightByPortal();
        }

        /// <summary>
        /// Get all the LocaleId associated with Highlights.
        /// </summary>        
        /// <returns>Returns list of Locale object.</returns>
        public TList<Locale> GetAllLocaleId()
        {
            // Get the Locale information with product id.
            LocaleService localeService = new LocaleService();
            LocaleQuery localeQuery = new LocaleQuery();
            localeQuery.AppendInQuery("LocaleId", "SELECT DISTINCT LOCALEID FROM ZnodeHighlight");

            TList<Locale> localeList = localeService.Find(localeQuery.GetParameters());

            if (localeList.Count > 0)
            {
                localeList.Sort("LocaleDescription");
            }

            return localeList;
        }

        #endregion
    }
}
