using System.Data;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.Admin
{
    /// <summary>
    /// Represents the PortalCountryAdmin class.
    /// </summary>
    public class PortalCountryAdmin : ZNodeBusinessBase
    {
        /// <summary>
        ///  Get all portal country List
        /// </summary>
        /// <returns>Returns the PortalCountry object.</returns>
        public TList<PortalCountry> GetAllPortalCountry()
        {
            ZNode.Libraries.DataAccess.Service.PortalCountryService countryService = new ZNode.Libraries.DataAccess.Service.PortalCountryService();
            TList<ZNode.Libraries.DataAccess.Entities.PortalCountry> countryServiceList = countryService.GetAll();

            return countryServiceList;
        }

        /// <summary>
        /// Get all PortalCountry by portal Id.
        /// </summary>
        /// <param name="portalId">Portal Id to get the portal country object.</param>
        /// <returns>Returns list of PortalCountry object by portal Id.</returns>
        public TList<PortalCountry> GetPortalCountryByPortalID(int portalId)
        {
            ZNode.Libraries.DataAccess.Service.PortalCountryService countryService = new ZNode.Libraries.DataAccess.Service.PortalCountryService();
            TList<ZNode.Libraries.DataAccess.Entities.PortalCountry> countryServiceList = countryService.GetByPortalID(portalId);

            countryService.DeepLoad(countryServiceList, true, DeepLoadType.IncludeChildren, typeof(Country));

            return countryServiceList;
        }

        /// <summary>
        /// Get all PortalCountry by country code.
        /// </summary>
        /// <param name="countryCode">Country code to get the portal country.</param>
        /// <returns>Returns list of PortalCountry object for the country code.</returns>
        public TList<PortalCountry> GetPortalCountryByCode(string countryCode)
        {
            ZNode.Libraries.DataAccess.Service.PortalCountryService countryService = new ZNode.Libraries.DataAccess.Service.PortalCountryService();
            TList<ZNode.Libraries.DataAccess.Entities.PortalCountry> countryServiceList = countryService.GetByCountryCode(countryCode);

            return countryServiceList;
        }

        /// <summary>
        /// Returns PortalCountry by CountryID
        /// </summary>
        /// <param name="portalCountryId">Portal country Id to get the portal country object.</param>
        /// <returns>Returns PortalCountry for the portal country Id.</returns>
        public PortalCountry GetPortalCountryByCode(int portalCountryId)
        {
            ZNode.Libraries.DataAccess.Service.PortalCountryService countryService = new ZNode.Libraries.DataAccess.Service.PortalCountryService();
            ZNode.Libraries.DataAccess.Entities.PortalCountry portalCountry = countryService.GetByPortalCountryID(portalCountryId);

            return portalCountry;
        }

        /// <summary>
        /// Update the PortalCountry object
        /// </summary>
        /// <param name="portalCountry">PortalCountry object to update.</param>
        /// <returns>Returns true if udapted else false.</returns>
        public bool Update(ZNode.Libraries.DataAccess.Entities.PortalCountry portalCountry)
        {
            ZNode.Libraries.DataAccess.Service.PortalCountryService countryService = new PortalCountryService();
            bool isUpdated = countryService.Update(portalCountry);

            return isUpdated;
        }

        /// <summary>
        /// Insert a new PortalCountry
        /// </summary>
        /// <param name="portalCountry">PortalCountry object to insert.</param>
        /// <returns>Returns true if inserted else false.</returns>
        public bool Insert(ZNode.Libraries.DataAccess.Entities.PortalCountry portalCountry)
        {
            ZNode.Libraries.DataAccess.Service.PortalCountryService countryService = new PortalCountryService();

            PortalCountryService portalCountryService = new PortalCountryService();
            PortalCountryQuery filters = new PortalCountryQuery();

            filters.Append(PortalCountryColumn.CountryCode, portalCountry.CountryCode);
            filters.Append(PortalCountryColumn.PortalID, portalCountry.PortalID.ToString());

            TList<PortalCountry> portalCountryList = countryService.Find(filters.GetParameters());

            if (portalCountryList.Count == 0)
            {
                return countryService.Insert(portalCountry);
            }

            return false;
        }

        /// <summary>
        /// Delete the PortalCountry.
        /// </summary>
        /// <param name="portalCountryId">Portal country Id to delete the PortalCountry object.</param>
        /// <returns>Returns true if deleted else false.</returns>
        public bool Delete(int portalCountryId)
        {
            ZNode.Libraries.DataAccess.Service.PortalCountryService countryService = new PortalCountryService();
            bool isDeleted = countryService.Delete(portalCountryId);

            return isDeleted;
        }

        /// <summary>
        /// Check whether the portal country is deletable 
        /// </summary>
        /// <param name="countryCode">Country code to check.</param>
        /// <returns>Returns true if portal couyntry deletable otherwise false.</returns>
        public bool IsDeletable(string countryCode)
        {
            bool canDelete = true;

            AddressService addressService = new AddressService();
            AddressQuery filter = new AddressQuery();

            // Check account billing country code.
            filter.AppendEquals(AddressColumn.CountryCode, countryCode);
            TList<Address> addressList = addressService.Find(filter.GetParameters());

            if (addressList.Count > 0)
            {
                // Some accounts associated with this coutry code.
                canDelete = false;
            }
            else
            {
                canDelete = true;
            }

            // Return the result.
            return canDelete;
        }

        /// <summary>
        /// Searches for a country based on Country Code or Country Name.
        /// </summary>
        /// <param name="countryCode">Country code to search.</param>
        /// <param name="countryName">Country name to search.</param>
        /// <returns>Returns the country list dataset.</returns>
        public DataSet SearchCountry(string countryCode, string countryName)
        {
            CountryService CountryService = new CountryService();
            CountryQuery filters = new CountryQuery();

            filters.Append(CountryColumn.Code, countryCode);
            filters.Append(CountryColumn.Name, countryName);

            TList<Country> CountryList = CountryService.Find(filters.GetParameters());

            if (CountryList.Count > 0)
            {
                return CountryList.ToDataSet(false);
            }

            return null;
        }
    }
}
