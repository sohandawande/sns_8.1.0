using System;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.Admin
{
    /// <summary>
    /// Provides methods to manage pages and revisions
    /// </summary>
    public class ContentPageAdmin : ZNodeBusinessBase
    {
        /// <summary>
        /// Locale Id for the content page
        /// </summary>
        private string _LocaleID = string.Empty;

        /// <summary>
        /// Gets or sets the locale code.
        /// </summary>
        public string LocalCode
        {
            get
            {
                LocaleService localservice = new LocaleService();
                Locale locale = localservice.GetByLocaleID(Convert.ToInt32(this._LocaleID));

                return locale.LocaleCode;
            }

            set
            {
                this._LocaleID = value;
            }
        }

        /// <summary>
        /// Add a new content page.
        /// </summary>
        /// <param name="page">Content Page</param>
        /// <param name="html">Html text content</param>
        /// <param name="portalId">Portal Id of the content</param>
        /// <param name="localeId">Locale Id of the content</param>
        /// <param name="updateUser">Updated by the user name</param>
        /// <param name="mappedSEOUrl">Mapped SEO Url name</param>
        /// <param name="isUrlRedirectEnabled">Indicates whether the URL redirect enabled or not.</param>
        /// <returns>Returns true if added else false.</returns>
        /// <exception cref="ApplicationException">Thrown when content page not inserted</exception>
        public bool AddPage(ContentPage page, string html, int portalId, string localeId, string updateUser, string mappedSEOUrl, bool isUrlRedirectEnabled, bool isMvcPage = false)
        {
            TransactionManager transactionManager = ConnectionScope.CreateTransaction(); 
            try
            {
                ContentPageService contentPageService = new ContentPageService();
                bool isSuccess = contentPageService.Insert(page);

                if (!isSuccess)
                {
                    throw new ApplicationException();
                }

                // Update memory table
                isSuccess = this.UpdateUrlMappingTable(page.Name, mappedSEOUrl, page.SEOURL, isUrlRedirectEnabled, isMvcPage);
                if (!isSuccess)
                {
                    throw new ApplicationException();
                }

                // Set LocaleId and get LocaleCode.
                this.LocalCode = localeId;

                // Add HTML update code here
                this.UpdateHtmlFile(page.Name, html, portalId, this.LocalCode);

                // Add Revision update code here
                isSuccess = this.AddPageRevision(page, html, updateUser, "Added Page");
                if (!isSuccess)
                {
                    throw new ApplicationException();
                }

                // Commit the transaction
                transactionManager.Commit();

                return true;
            }
            catch (Exception ex)
            {
                if (!Equals(transactionManager, null) && transactionManager.IsOpen)
                {
                    transactionManager.Rollback();
                }
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.ToString());
                return false;
            }
        }

        /// <summary>
        /// Update the content page.
        /// </summary>
        /// <param name="page">Content page object.</param>
        /// <param name="html">New html content</param>
        /// <param name="oldhtml">Old html content</param>
        /// <param name="portalId">Portal Id of the content.</param>
        /// <param name="localeId">Locale Id of the content.</param>
        /// <param name="updateUser">Updated by the user name.</param>
        /// <param name="mappedSEOUrl">Mapped SEO Url name</param>
        /// <param name="isUrlRedirectEnabled">Indicates whether the URL redirect enabled or not.</param>
        /// <returns>Returns true if inserted else false.</returns>
        public bool UpdatePage(ContentPage page, string html, string oldhtml, int portalId, string localeId, string updateUser, string mappedSEOUrl, bool isUrlRedirectEnabled, bool isMvcPage = false)
        {
            TransactionManager transactionManager = null;
            transactionManager = ConnectionScope.CreateTransaction();

            try
            {
                ContentPageService contentPageService = new ContentPageService();
                bool isSuccess = contentPageService.Update(page);
                if (!isSuccess)
                {
                    throw new ApplicationException();
                }

                // Update urlmappings
                if (page.SEOURL != null)
                {
                    // Update memory table
                    isSuccess = this.UpdateUrlMappingTable(page.Name, mappedSEOUrl, page.SEOURL, isUrlRedirectEnabled, isMvcPage);
                    if (!isSuccess)
                    {
                        throw new ApplicationException();
                    }
                }

                // Set LocaleId and get LocaleCode.
                this.LocalCode = localeId;

                // Add HTML update code here
                this.UpdateHtmlFile(page.Name, html, portalId, this.LocalCode);

                // Add Revision update code here
                isSuccess = this.AddPageRevision(page, oldhtml, updateUser, "Edited Page");
                if (!isSuccess)
                {
                    throw new ApplicationException();
                }

                // Commit the transaction
                transactionManager.Commit();

                return true;
            }
            catch
            {
                if (transactionManager != null && transactionManager.IsOpen)
                {
                    transactionManager.Rollback();
                }
                return false;
            }
        }

        /// <summary>
        /// Updates an Url mapping memory table with new seo url link
        /// </summary>
        /// <param name="pageName">Page name in the URl mapping table.</param>
        /// <param name="mappedSEOUrl">Mapped SEO url name.</param>
        /// <param name="newSEOUrl">New SEO url name.</param>
        /// <param name="isUrlRedirectEnabled">Indicates whether the URL redirect enabled or not.</param>
        /// <returns>Returns true if updated else false.</returns>
        public bool UpdateUrlMappingTable(string pageName, string mappedSEOUrl, string newSEOUrl, bool isUrlRedirectEnabled, bool isMvcPage = false)
        {
            UrlRedirectAdmin urlRedirectAdmin = new UrlRedirectAdmin();
            bool isUpdated = true;

            if (urlRedirectAdmin.SeoUrlExists(newSEOUrl, pageName))
            {
                return false;
            }

            if (isUrlRedirectEnabled)
            {
                isUpdated = urlRedirectAdmin.AddUrlRedirectTable(SEOUrlType.ContentPage, mappedSEOUrl, newSEOUrl, pageName, isMvcPage);
            }

            urlRedirectAdmin.UpdateUrlRedirect(mappedSEOUrl);

            return isUpdated;
        }

        /// <summary>
        /// Returns a list of all the pages by Portal
        /// </summary>
        /// <param name="portalId">Poratl Id to get all content pages.</param>
        /// <returns>Returns the list of content pages belongs to a portal</returns>
        public TList<ContentPage> GetPages(int portalId)
        {
            ContentPageService contentPageService = new ContentPageService();
            return contentPageService.GetByPortalID(portalId);
        }

        /// <summary>
        /// Returns a list of all the pages
        /// </summary>        
        /// <returns>Returns the list of content pages of all portal.</returns>
        public TList<ContentPage> GetAllPages()
        {
            ContentPageService contentPageService = new ContentPageService();
            return contentPageService.GetAll();
        }

        /// <summary>
        /// Gets the content page by Id
        /// </summary>
        /// <param name="contentPageId">Content page Id to get ContentPage object.</param>
        /// <returns>Returns ContentPage object for the content page Id.</returns>
        public ContentPage GetPageByID(int contentPageId)
        {
            ContentPageService contentPageService = new ContentPageService();
            return contentPageService.GetByContentPageID(contentPageId);
        }

        /// <summary>
        /// Check if a page name is available
        /// </summary>
        /// <param name="name">Content page name.</param>
        /// <param name="portalId">Portal of the content page.</param>
        /// <param name="localeId">Locale Id of the content page</param>        
        /// <returns>Returns true if available else false.</returns>
        public bool IsNameAvailable(string name, int portalId, int localeId, int pageId = 0)
        {
            ContentPageService contentPageService = new ContentPageService();
            var page = contentPageService.GetByNamePortalIDLocaleId(name, portalId, localeId);

            if (page == null || page.ContentPageID == pageId)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Returns the HTML content of a page
        /// </summary>
        /// <param name="pageName">Content page name</param>
        /// <param name="portalId">Portal Id of the content page.</param>
        /// <param name="localeId">Locale Id of the content page.</param>
        /// <returns>Return the HTML page content.</returns>
        public string GetPageHTMLByName(string pageName, int portalId, string localeId)
        {
            // Set LocaleId and get LocaleCode.
            this.LocalCode = localeId;

            string filePath = ZNodeConfigManager.EnvironmentConfig.ContentPath + pageName + "_" + portalId + "_" + this.LocalCode + ".htm";
            if (ZNodeStorageManager.Exists(filePath))
            {
                try
                {
                    string html = ZNodeStorageManager.ReadTextStorage(filePath);

                    return html;
                }
                catch
                {
                    return string.Empty;
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// Get page revisions
        /// </summary>
        /// <param name="contentPageId">Content page Id to get revision details.</param>
        /// <returns>Returns list of ContentPageRevision objects.</returns>
        public TList<ContentPageRevision> GetPageRevisions(int contentPageId)
        {
            ContentPageRevisionService contentPageService = new ContentPageRevisionService();
            return contentPageService.GetByContentPageID(contentPageId);
        }

        /// <summary>
        /// Sets a page status to published
        /// </summary>
        /// <param name="contentPage">Content page object to publish.</param>
        public void PublishPage(ContentPage contentPage)
        {
            ContentPageService contentPageService = new ContentPageService();

            if (!contentPage.ActiveInd)
            {
                contentPage.ActiveInd = true;
                contentPageService.Update(contentPage);
            }

            return;
        }

        /// <summary>
        /// Delete a page
        /// </summary>
        /// <param name="contentPage">Cotent page object to delete.</param>
        /// <returns>Returns true if deleted else false.</returns>
        public bool DeletePage(ContentPage contentPage)
        {
            TransactionManager transactionManager = null;
            transactionManager = ConnectionScope.CreateTransaction();

            try
            {
                bool isSuccess = false;

                ContentPageRevisionService contentPageRevisionService = new ContentPageRevisionService();
                ContentPageService contentPageService = new ContentPageService();

                // Delete revisions
                TList<ContentPageRevision> pageRevisionList = contentPageRevisionService.GetByContentPageID(contentPage.ContentPageID);
                contentPageRevisionService.Delete(pageRevisionList);

                isSuccess = contentPageService.Delete(contentPage);
                if (!isSuccess)
                {
                    return false;
                }

                // Delete html
                this.DeletePage(contentPage.Name, contentPage.PortalID, contentPage.LocaleId);

                transactionManager.Commit();
                return true;
            }
            catch
            {
                transactionManager.Rollback();
                return false;
            }
        }

        /// <summary>
        /// Reverts a page to a particular revision
        /// </summary>
        /// <param name="pageRevisionId">Page reviosion Id to revert.</param>
        /// <param name="updateUser">Updated by user name.</param>
        /// <param name="oldhtml">Old Html content.</param>
        /// <returns>Returns true if reverted else false.</returns>
        public bool RevertToRevision(int pageRevisionId, string updateUser, string oldhtml)
        {
            // Get revision
            ContentPageRevisionService contentPageService = new ContentPageRevisionService();
            ContentPageRevision pageRevision = contentPageService.GetByRevisionID(pageRevisionId);

            // Get content page
            ContentPage page = this.GetPageByID(pageRevision.ContentPageID);

            // Revert html to this revision
            bool isSuccess = this.UpdatePage(page, pageRevision.HtmlText, oldhtml, updateUser, page.PortalID, page.LocaleId.ToString());

            return isSuccess;
        }

        #region Private Helper Methods
        /// <summary>
        /// Creates or updates an HTML file
        /// </summary>
        /// <param name="pageName">Page name.</param>
        /// <param name="html">Html content of the page</param>
        /// <param name="portalId">Poratl Id of the content</param>
        /// <param name="localCode">Locale code of the content</param>
        private void UpdateHtmlFile(string pageName, string html, int portalId, string localCode)
        {
            if (ZNode.Libraries.Framework.Business.ZNodeConfigManager.SetSiteConfig() == false)
            {
                return;
            }

            string filePath = ZNodeConfigManager.EnvironmentConfig.ContentPath + pageName + "_" + portalId + "_" + localCode + ".htm";

            ZNodeStorageManager.WriteTextStorage(html, filePath);
        }

        /// <summary>
        /// Adds a new revision
        /// </summary>
        /// <param name="page">Content page object</param>
        /// <param name="html">Html content of the revision.</param>
        /// <param name="updateUser">Updated by the user name.</param>
        /// <param name="description">Revision description.</param>
        /// <returns>Returns true if inserted else false.</returns>
        private bool AddPageRevision(ContentPage page, string html, string updateUser, string description)
        {
            ContentPageRevisionService contentPageService = new ContentPageRevisionService();
            ContentPageRevision pageRevision = new ContentPageRevision();

            pageRevision.HtmlText = html;
            pageRevision.UpdateDate = System.DateTime.Now;
            pageRevision.UpdateUser = updateUser;
            pageRevision.ContentPageID = page.ContentPageID;
            pageRevision.Description = description;

            return contentPageService.Insert(pageRevision);
        }

        /// <summary>
        /// Delete the content page by name, portal Id and locale Id
        /// </summary>
        /// <param name="pageName">Content page name</param>
        /// <param name="portalId">Portal Id of the content page.</param>
        /// <param name="localeId">Locale Id of content page. (Nullable)</param>
        private void DeletePage(string pageName, int portalId, int? localeId)
        {
            // Set LocaleId and get LocaleCode.
            this.LocalCode = localeId.ToString();

            string filePath = ZNodeConfigManager.EnvironmentConfig.ContentPath + pageName + "_" + portalId + "_" + this.LocalCode + ".htm";

            ZNodeStorageManager.DeleteStorage(filePath);
        }

        /// <summary>
        /// Update a content page.
        /// </summary>
        /// <param name="page">Content page object</param>
        /// <param name="html">New html content.</param>
        /// <param name="oldhtml">Old html content.</param>
        /// <param name="updateUser">Updated by the user name</param>
        /// <param name="portalId">Portal Id of the content.</param>
        /// <param name="localeId">Locale Id of the content.</param>
        /// <returns>Returns true if updated else false.</returns>
        private bool UpdatePage(ContentPage page, string html, string oldhtml, string updateUser, int portalId, string localeId)
        {
            TransactionManager transactionManager = null;
            transactionManager = ConnectionScope.CreateTransaction();

            try
            {
                ContentPageService contentPageService = new ContentPageService();
                bool isSuccess = contentPageService.Update(page);

                if (!isSuccess)
                {
                    throw new ApplicationException();
                }

                // Set LocaleId and get LocaleCode.
                this.LocalCode = localeId;

                // Add HTML update code here
                this.UpdateHtmlFile(page.Name, html, portalId, this.LocalCode);

                // Add Revision update code here
                isSuccess = this.AddPageRevision(page, oldhtml, updateUser, "Edited Page");

                if (!isSuccess)
                {
                    throw new ApplicationException();
                }

                // Commit the transaction
                transactionManager.Commit();

                return true;
            }
            catch
            {
                transactionManager.Rollback();

                return false;
            }
        }
        #endregion
    }
}
