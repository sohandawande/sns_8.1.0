using System.Data;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.Admin
{
    /// <summary>
    /// Provides methods to manage product images
    /// </summary>
    public class ProductViewAdmin : ZNodeBusinessBase
    {
        #region Public Method
        /// <summary>
        /// Get the product view dataset by product Id.
        /// </summary>
        /// <param name="productId">Product Id to get the product view.</param>
        /// <returns>Returns the product view dataset.</returns>
        public DataSet GetByProductID(int productId)
        {
            ZNode.Libraries.DataAccess.Service.ProductImageService productImageService = new ProductImageService();
            return productImageService.GetByProductID(productId).ToDataSet(true);
        }

        /// <summary>
        /// Get all the alternate images by product Id.
        /// </summary>
        /// <param name="productId">Product Id to get the all alternate images.</param>
        /// <returns>Returns the product alternate images dataset.</returns>
        public DataSet GetAllAlternateImage(int productId)
        {
            return this.GetAllAlternateImage(productId, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty);
        }

        /// <summary>
        /// Get products alternate and swatch image. All parameters are optional.
        /// </summary>
        /// <param name="productId">Product Id to search.</param>
        /// <param name="productNum">Product Num to search</param>
        /// <param name="productName">Product Name to search</param>
        /// <param name="vendor">Vendor Name to search</param>
        /// <param name="vendorId">Vendor Id to search</param>
        /// <param name="productStateId">Product Review State Id to search</param>
        /// <param name="twoCoId">2Checkout Id to search</param>
        /// <returns>Returns product alternate/swatch images dataset.</returns>
        public DataSet GetAllAlternateImage(int productId, string productNum, string productName, string vendor, string vendorId, string productStateId, string twoCoId)
        {
            ProductHelper productHelper = new ProductHelper();
            return productHelper.GetAlternateImage(productId, productNum, productName, vendor, vendorId, productStateId, twoCoId);
        }

        /// <summary>
        /// Get all the highlight images by product Id.
        /// </summary>
        /// <param name="productId">Product Id to get the product highlight images.</param>
        /// <returns>Returns the product highlight image dataset.</returns>
        public DataSet GetAllHighlightImage(int productId)
        {
            ProductHelper productHelper = new ProductHelper();
            DataSet highlightImageDataSet = productHelper.GetHighlightImages(productId);

            return highlightImageDataSet;
        }

        /// <summary>
        /// Get product images by product Id.
        /// </summary>
        /// <param name="productId">Product Id to get the product image.</param>
        /// <returns>Returns list of ProductImage object.</returns>
        public TList<ProductImage> GetImageByProductID(int productId)
        {
            ZNode.Libraries.DataAccess.Service.ProductImageService Access = new ProductImageService();
            TList<ZNode.Libraries.DataAccess.Entities.ProductImage> productImageList = Access.GetByProductID(productId);

            return productImageList;
        }

        /// <summary>
        /// Update a product image.
        /// </summary>
        /// <param name="productImage">ProductImage object to update.</param>
        /// <returns>Returns true if updated otherwise false.</returns>
        public bool Update(ZNode.Libraries.DataAccess.Entities.ProductImage productImage)
        {
            ZNode.Libraries.DataAccess.Service.ProductImageService productImageService = new ProductImageService();
            bool isUpdated = productImageService.Update(productImage);
            return isUpdated;
        }

        /// <summary>
        /// Insert a new ProductImage
        /// </summary>
        /// <param name="productImage">ProductImage object to update.</param>
        /// <returns>Returns true if inserted otherwise false.</returns>
        public bool Insert(ZNode.Libraries.DataAccess.Entities.ProductImage productImage)
        {
            ZNode.Libraries.DataAccess.Service.ProductImageService productImageService = new ProductImageService();
            bool isAdded = productImageService.Insert(productImage);

            return isAdded;
        }

        /// <summary>
        /// Delete the product image by product image Id.
        /// </summary>
        /// <param name="productImageId">Product image Id to get the product image object.</param>
        /// <returns>Returns true if deleted otherwise false.</returns>
        public bool Delete(int productImageId)
        {
            ZNode.Libraries.DataAccess.Service.ProductImageService productImageService = new ProductImageService();
            bool isDeleted = productImageService.Delete(productImageId);

            return isDeleted;
        }

        /// <summary>
        /// Delete the product image by product Id.
        /// </summary>
        /// <param name="productId">Product Id to get the list of product image object.</param>        
        public void DeleteByProductID(int productId)
        {
            ZNode.Libraries.DataAccess.Service.ProductImageService productImageService = new ProductImageService();
            TList<ZNode.Libraries.DataAccess.Entities.ProductImage> imagelist = productImageService.GetByProductID(productId);
            productImageService.Delete(imagelist);
        }

        /// <summary>
        /// Get the product image by the product image Id.
        /// </summary>
        /// <param name="productImageId">Product image Id to get the product image object.</param>
        /// <returns>Returns the ProductImage object.</returns>
        public ZNode.Libraries.DataAccess.Entities.ProductImage GetByProductImageID(int productImageId)
        {
            ZNode.Libraries.DataAccess.Service.ProductImageService productImageService = new ProductImageService();
            ZNode.Libraries.DataAccess.Entities.ProductImage productImage = productImageService.GetByProductImageID(productImageId);

            return productImage;
        }

        /// <summary>
        /// Get all the product image type.
        /// </summary>
        /// <returns>Returns list of ProductImageType object.</returns>
        public TList<ZNode.Libraries.DataAccess.Entities.ProductImageType> GetImageType()
        {
            ZNode.Libraries.DataAccess.Service.ProductImageTypeService productImageTypeService = new ProductImageTypeService();
            TList<ZNode.Libraries.DataAccess.Entities.ProductImageType> imageTypeList = productImageTypeService.GetAll();

            return imageTypeList;
        }

        #endregion
    }
}
