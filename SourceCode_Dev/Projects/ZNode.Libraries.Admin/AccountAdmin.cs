using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.Admin
{
    /// <summary>
    /// Provides methods to manage Customer Accounts
    /// </summary>
    public class AccountAdmin : ZNodeBusinessBase
    {
        /// <summary>
        /// Return all Customer Details for this portal
        /// </summary>        
        /// <returns>Returns the customer dataset.</returns>
        public DataSet GetAllCustomers()
        {
            AccountHelper accountHelper = new AccountHelper();
            return accountHelper.GetAllCustomers();
        }

        /// <summary>
        /// Return all the Customer Account
        /// </summary>
        /// <returns>Returns the list account objects</returns>
        public TList<Account> GetAllCustomer()
        {
            ZNode.Libraries.DataAccess.Service.AccountService accountService = new AccountService();
            TList<ZNode.Libraries.DataAccess.Entities.Account> AccountList = accountService.GetAll();

            return AccountList;
        }

        /// <summary>
        /// Get account's default shipping address.
        /// </summary>
        /// <param name="accountId">Account Id</param>
        /// <returns>Returns account default shipping address object.</returns>
        public Address GetDefaultShippingAddress(int accountId)
        {
            return this.GetDefaultAddress(false, accountId);
        }

        /// <summary>
        /// Get account's default billing address.
        /// </summary>
        /// <param name="accountId">Account Id</param>
        /// <returns>Returns account default billing address object.</returns>
        public Address GetDefaultBillingAddress(int accountId)
        {
            return this.GetDefaultAddress(true, accountId);
        }

        /// <summary>
        /// Get all billing and shipping address for an account.
        /// </summary>
        /// <param name="accountId">Account Id</param>
        /// <returns>Returns list of billing and shipping address for an account.</returns>
        public TList<Address> GetAllAddressByAccountID(int accountId)
        {
            AddressService addressService = new AddressService();
            return addressService.GetByAccountID(accountId);
        }       

        /// <summary>
        ///  Get a customer account for a Account Id
        /// </summary>
        /// <param name="accountId">Account Id to get the account object</param>
        /// <returns>Returns the Account object</returns>
        public Account GetByAccountID(int accountId)
        {
            ZNode.Libraries.DataAccess.Service.AccountService accountService = new AccountService();
            ZNode.Libraries.DataAccess.Entities.Account accountList = accountService.GetByAccountID(accountId);

            return accountList;
        }

        /// <summary>
        /// Add User Account
        /// </summary>
        /// <param name="userAccount">User Account object to save into database.</param>
        /// <returns>Return true if added else false.</returns>
        public bool Add(Account userAccount)
        {
            ZNode.Libraries.DataAccess.Service.AccountService accountService = new AccountService();
            return accountService.Insert(userAccount);
        }

        /// <summary>
        /// Update User Account
        /// </summary>
        /// <param name="userAccount">UserAccount object update.</param>
        /// <returns>Returns true if updated else false.</returns>
        public bool Update(Account userAccount)
        {
            ZNode.Libraries.DataAccess.Service.AccountService accountService = new AccountService();
            return accountService.Update(userAccount);
        }

        /// <summary>
        /// Delete an user account.
        /// </summary>
        /// <param name="accountId">Account Id to delete the account object.</param>
        /// <returns>Returns true of deleted else false.</returns>
        public bool Delete(int accountId)
        {
            ZNode.Libraries.DataAccess.Service.AccountService accountService = new AccountService();
            return accountService.Delete(accountId);
        }

        /// <summary>
        /// Delete a User Account
        /// </summary>
        /// <param name="account">Account object to delete from datasource.</param>
        /// <returns>Returns true of deleted else false.</returns>
        public bool Delete(Account account)
        {
            ZNode.Libraries.DataAccess.Service.AccountService accountService = new AccountService();
            return accountService.Delete(account);
        }

        /// <summary>
        /// Delete the address.
        /// </summary>
        /// <param name="addressId">Address Id to delete the Address object</param>
        /// <returns>Returns true if deleted else false.</returns>
        public bool DeleteAddress(int addressId)
        {
            ZNode.Libraries.DataAccess.Service.AddressService addressService = new AddressService();
            return addressService.Delete(addressId);
        }

        /// <summary>
        /// Check whether the address is valid or not.
        /// </summary>
        /// <param name="address">Address object to validate using USPS.</param>
        /// <returns>Returns true if address is valid otherwise false.</returns>
        public bool IsAddressValid(Address address)
        {
	        var usps = new Znode.Engine.Shipping.Usps.UspsAgent();
	        return usps.IsAddressValid(address);
        }

        /// <summary>
        /// Insert a new User Account
        /// </summary>
        /// <param name="account">Account object to insert.</param>
        /// <returns>Returns true if inserted else false.</returns>
        public bool Insert(Account account)
        {
            ZNode.Libraries.DataAccess.Service.AccountService accountService = new AccountService();
            return accountService.Insert(account);
        }
        
        /// <summary>
        /// Get the payments by account Id.
        /// </summary>
        /// <param name="accountId">Account Id to get the payment</param>
        /// <returns>Returns a collection of Account Payment objects.</returns>
        public TList<AccountPayment> GetPaymentsByAccountId(int accountId)
        {
            AccountPaymentService paymentService = new AccountPaymentService();
            return paymentService.GetByAccountID(accountId);
        }

        /// <summary>
        /// Add affiliate payment
        /// </summary>
        /// <param name="accountPayment">AccountPayment object to insert.</param>
        /// <returns>Returns true if inserted else false.</returns>
        public bool AddAffiliatePayment(AccountPayment accountPayment)
        {
            AccountPaymentService paymentService = new AccountPaymentService();
            return paymentService.Insert(accountPayment);
        }

        /// <summary>
        /// Update user accounts LastPasswordChangedDate field.
        /// </summary>
        /// <param name="userId">UserId to updated the LastPasswordChangedDate field</param>        
        public void UpdateLastPasswordChangedDate(string userId)
        {
            ZNode.Libraries.DataAccess.Custom.AccountHelper helper = new AccountHelper();
            helper.UpdateLastPasswordChangedDate(userId);
        }

        /// <summary>
        /// Gets a row from the DataSource based on its primary key Column Id.
        /// </summary>
        /// <param name="accountPaymentId">Account Payment Id get the AccountPayment object.</param>
        /// <returns>Returns an instance of the AccountPayment entity object.</returns>
        public AccountPayment GetByAccountPaymentId(int accountPaymentId)
        {
            AccountPaymentService paymentService = new AccountPaymentService();
            return paymentService.GetByAccountPaymentID(accountPaymentId);
        }

        /// <summary>
        /// Get the account by portals
        /// </summary>
        /// <param name="Portals">Portal number to get the accounts</param>
        /// <returns>Returns account object dataset.</returns>
        public DataSet GetAccountByPortal(string Portals)
        {
            ZNode.Libraries.DataAccess.Custom.AccountHelper helper = new AccountHelper();
            return helper.GetAccountByPortal(Portals);
        }

        #region Helper Methods
        /// <summary>
        /// Get accounts default address.
        /// </summary>
        /// <param name="defaultBilling">True to return default billing address, false to return shipping address.</param>
        /// <param name="accountId">Account Id</param>
        /// <returns>Returns account default billing or shipping address object.</returns>
        private Address GetDefaultAddress(bool defaultBilling, int accountId)
        {
            AddressQuery query = new AddressQuery();
            query.Append(AddressColumn.AccountID, accountId.ToString());
            if (defaultBilling)
            {
                query.Append(AddressColumn.IsDefaultBilling, "true");
            }
            else
            {
                query.Append(AddressColumn.IsDefaultShipping, "true");
            }

            AddressService addressService = new AddressService();
            Address defaultBillingAddress = null;
            TList<Address> addressList = addressService.Find(query);
            if (addressList.Count > 0)
            {
                defaultBillingAddress = addressList[0];
            }

            return defaultBillingAddress;
        } 
        #endregion
    }
}