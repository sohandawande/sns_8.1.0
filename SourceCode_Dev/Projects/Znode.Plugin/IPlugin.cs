﻿using System;

namespace Znode.Plugin
{
    public interface IPlugin
    {        /// <summary>
        /// Title of the plugin, can be used as a property to display on the user interface
        /// </summary>
        string Title { get; }

        /// <summary>
        /// Name of the plugin, should be an unique name
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Version of the loaded plugin
        /// </summary>
        Version Version { get; }

        /// <summary>
        /// The order of the plugin
        /// </summary>
        int Order { get; }

        /// <summary>
        /// Install the plugin
        /// </summary>
        void Install();

        /// <summary>
        /// Uninstall the plugin
        /// </summary>
        void Uninstall();

        /// <summary>
        /// Check the plugin is installed or not
        /// </summary>
        bool IsInstalled { get; set; }
    }
}