﻿
namespace Znode.Plugin
{
    public interface IAdminPlugin : IPlugin
    {
        /// <summary>
        /// Entry controller name
        /// </summary>
        string EntryControllerName { get; }
    }
}