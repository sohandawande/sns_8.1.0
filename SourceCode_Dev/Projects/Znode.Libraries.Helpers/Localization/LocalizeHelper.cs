﻿using System;
using System.Reflection;
using System.Resources;

namespace Znode.Libraries.Helpers
{
    /// <summary>
    /// Helper class for localization
    /// </summary>
    public class LocalizeHelper
    {
        #region Private Functions

        //Find Global Assembly
        private static Assembly FindGlobalResAssembly()
        {
            foreach (Assembly asm in AppDomain.CurrentDomain.GetAssemblies())
            {
                if (asm.FullName.StartsWith("App_GlobalResources."))
                    return asm;
            }
            return null;
        }

        //Find Localized Resource Value
        private static string FindLocalizedResourceValue(string ResourceName, string ResourceKey)
        {
            Assembly asm = FindGlobalResAssembly();
            if (asm == null)
                return null;
            string resourceName = string.Format("{0}.{1}", "Resources", ResourceName);
            return new ResourceManager(resourceName, asm).GetObject(ResourceKey).ToString();
        }

        #endregion

        #region Public Functions

        //zToDo - This Function is temprory kept private uptill the final approach of accessing ResourceName gets finalized
        //Get Localized Message by Providing Resource Key only
        private static string GetLocalizedString(string ResourceKey)
        {
            return FindLocalizedResourceValue(string.Empty, ResourceKey);
        }

        //Get Localized Message by Providing Resource Name and Resource Key
        public static string GetLocalizedString(string ResourceName, string ResourceKey)
        {
            return FindLocalizedResourceValue(ResourceName, ResourceKey);
        }

        #endregion
    }
}
