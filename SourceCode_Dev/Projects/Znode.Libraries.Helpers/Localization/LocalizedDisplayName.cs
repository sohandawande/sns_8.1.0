﻿using System.ComponentModel;

namespace Znode.Libraries.Helpers
{
    /// <summary>
    ///  To extend Display name attribute
    ///  getting display name from resource file
    /// </summary>
    public class LocalizedDisplayName : DisplayNameAttribute
    {
        #region Public variable
        public string Name { get; set; }
        public string ResourceType { get; set; }
        #endregion

        #region Constructor

        /// <summary>
        /// Constructor for LocalizedDisplayName
        /// </summary>
        public LocalizedDisplayName()
            : base()
        {

        }
        #endregion

        #region Public Function

        public override string DisplayName
        {
            get
            {
                if (!string.IsNullOrEmpty(this.ResourceType))
                {
                    return LocalizeHelper.GetLocalizedString(ResourceType, Name);
                }
                else if (!string.IsNullOrEmpty(this.Name))
                {
                    return LocalizeHelper.GetLocalizedString("ZnodeResources", Name);
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        #endregion
    }
}
