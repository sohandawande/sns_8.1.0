﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Libraries.Helpers.Constants;

namespace Znode.Libraries.Helpers.Utility
{
    public static class DynamicClause
    {
        /// <summary>
        /// To create where condition dynamically
        /// </summary>
        /// <param name="StoredProc">Dictionary<string, int> StoredProc</param>
        /// <returns>returns string where clause</returns>
        public static string DynamicWhereClause(Dictionary<string, string> StoredProc)
        {
            string whereClause = string.Empty;

            foreach (KeyValuePair<string, string> pair in StoredProc)
            {
                whereClause += StoredProcedureKeys.TildOperator + pair.Key + StoredProcedureKeys.TildEqualToOperator + pair.Value;
                whereClause = whereClause + " " + StoredProcedureKeys.AND + " ";
            }

            whereClause = whereClause.Substring(0, whereClause.Length - 4);
            return whereClause;
        }
    }
}
