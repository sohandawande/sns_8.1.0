﻿using System.Linq;

namespace Znode.Libraries.Helpers
{
    /// <summary>
    /// Helper for FileUpload
    /// </summary>
    public class FileUploadHelper
    {
        #region Create Path     
        
        /// <summary>
        ///  Create and returns directory path for specified input.
        /// </summary>
        /// <param name="virtualPath"></param>
        /// <param name="companyId"></param>
        /// <param name="websiteId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static string CreateFrontDirectoryPath(string virtualPath, int companyId, int websiteId, int userId)
        {
            string path = string.Format(virtualPath, companyId, websiteId, userId);
            return path;
        }

        /// <summary>
        /// Create and return directory path for specified input
        /// </summary>
        /// <param name="virtualPath"></param>
        /// <param name="companyId"></param>
        /// <param name="websiteId"></param>
        /// <returns></returns>
        public static string CreateFrontDirectoryPath(string virtualPath, int companyId, int websiteId)
        {
            string path = string.Format(virtualPath, companyId, websiteId);
            return path;
        }

        /// <summary>
        /// Check upload file extension
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="fileExtensionlistCommaSeperated"></param>
        /// <returns></returns>
        public static bool IsValidFileExtension(string fileName, string fileExtensionlistCommaSeperated)
        {
            string extension = System.IO.Path.GetExtension(fileName).ToLower().Replace(".", "");
            string[] allowedExtension = fileExtensionlistCommaSeperated.ToLower().Split(',');
            if (allowedExtension.Contains(extension))
            {
                return true;
            }
            return false;
        }

        public static bool IsFileExtensionValid(string fileName, string fileExtension)
        {
            fileName = fileName.ToLower();
            string[] allowedExtension = fileExtension.Split(',');

            bool status = false;
            if (allowedExtension.Contains(fileName.Substring(fileName.LastIndexOf('.'))))
            {
                status = true;
            }
            return status;
        }

        /// <summary>
        /// Check file size
        /// </summary>
        /// <param name="fileSize"></param>
        /// <param name="maxFileSize"></param>
        /// <returns></returns>
        public static bool IsValidFileSize(int fileSize, int maxFileSize)
        {
            if (maxFileSize >= fileSize)
            {
                return true;
            }
            return false;
        }
      
        #endregion
    }
}
