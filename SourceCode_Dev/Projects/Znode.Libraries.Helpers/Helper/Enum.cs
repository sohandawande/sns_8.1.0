﻿
namespace Znode.Libraries.Helpers
{
    #region User Types
    /// <summary>
    /// Enum for User Types
    /// </summary>
    public enum UserTypes
    {
        MallAdmin,
        FranchiseAdmin,
        Admin
    }
    #endregion
}
