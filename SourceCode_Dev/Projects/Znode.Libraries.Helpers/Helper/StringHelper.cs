﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Libraries.Helpers.Constants;

namespace Znode.Libraries.Helpers
{
    public static class StringHelper
    {
        /// <summary>
        /// To Generate dynamic where clause for stored procedure.
        /// </summary>
        /// <param name="filters">List<Tuple<string, string, string>> filters</param>
        /// <returns>Return the where clause string.</returns>
        public static string GenerateDynamicWhereClause(List<Tuple<string, string, string>> filters)
        {
            string whereClause = string.Empty;
            if (!Equals(filters, null) && filters.Count > 0)
            {
                foreach (var tuple in filters)
                {
                    var filterKey = tuple.Item1;
                    var filterOperator = tuple.Item2;
                    var filterValue = tuple.Item3;
                    if (!Equals(filterKey, "_") && !Equals(filterKey.ToLower(), "usertype"))
                    {
                        whereClause = (string.IsNullOrEmpty(whereClause)) ? SetQueryParameter(filterKey, filterOperator, filterValue) : whereClause + " " + StoredProcedureKeys.AND + " " + SetQueryParameter(filterKey, filterOperator, filterValue);
                    }
                }
            }
            return whereClause;
        }

        /// <summary>
        /// To Generate dynamic order by clause for stored procedure.
        /// </summary>
        /// <param name="sorts">NameValueCollection for sorting the grid column</param>
        /// <returns>Return the order by clause string.</returns>
        public static string GenerateDynamicOrderByClause(NameValueCollection sorts)
        {
            string orderBy = string.Empty;
            if (!Equals(sorts.AllKeys, null) && sorts.Count > 0)
            {
                orderBy = sorts[StoredProcedureKeys.sortKey] + " " + sorts[StoredProcedureKeys.sortDirKey];
            }

            return orderBy;
        }


        /// <summary>
        /// To Get Query based on Input filter operator.
        /// </summary>
        /// <param name="filterKey">string filterKey</param>
        /// <param name="filterOperator">string filterOperator</param>
        /// <param name="filterValue">string filterValue</param>
        /// <returns>Return the where clause string</returns>
        private static string SetQueryParameter(string filterKey, string filterOperator, string filterValue)
        {
            string query = string.Empty;
            if (Equals(filterOperator, ProcudureFilterOperators.Equals)) return AppendQuery(filterKey, " = ", filterValue);
            if (Equals(filterOperator, ProcudureFilterOperators.Like)) return AppendLikeQuery(filterKey, " like ", filterValue);
            if (Equals(filterOperator, ProcudureFilterOperators.GreaterThan)) return AppendQuery(filterKey, " > ", filterValue);
            if (Equals(filterOperator, ProcudureFilterOperators.GreaterThanOrEqual)) return AppendQuery(filterKey, " >= ", filterValue);
            if (Equals(filterOperator, ProcudureFilterOperators.LessThan)) return AppendQuery(filterKey, " < ", filterValue);
            if (Equals(filterOperator, ProcudureFilterOperators.LessThanOrEqual)) return AppendQuery(filterKey, " <= ", filterValue);
            if (Equals(filterOperator, ProcudureFilterOperators.Contains)) return AppendLikeQuery(filterKey, " like ", filterValue);
            if (Equals(filterOperator, ProcudureFilterOperators.StartsWith)) return GenerateStartwithQuery(filterKey, " like ", filterValue);
            if (Equals(filterOperator, ProcudureFilterOperators.EndsWith)) return GenerateEndwithQuery(filterKey, " like ", filterValue);
            if (Equals(filterOperator, ProcudureFilterOperators.Is)) return AppendQuery(filterKey, " is ", filterValue);
            //PRFT Custpom Code: Start
            if (Equals(filterOperator, ProcudureFilterOperators.NotEquals)) return AppendQuery(filterKey, " is not ", filterValue);
            //PRFT Custom Code: End
            return query;
        }

        /// <summary>
        /// Method Return the genrated query.
        /// </summary>
        /// <param name="filterKey">string filterKey</param>
        /// <param name="filterOperator">string filterOperator</param>
        /// <param name="filterValue">string filterValue</param>
        /// <returns>Return the where clause string</returns>
        private static string AppendQuery(string filterKey, string filterOperator, string filterValue)
        {
            //This Special Case required to Get the Franchisable Product in which Product belongs to Portals & all product having franchisable flat true will comes.
            if (filterKey.Contains("$")) return AppendSpecialOrClauseToQuery(filterKey, filterOperator, filterValue);
            //To generate OR condition with null value for the filter key passed.
            if (filterKey.Contains("|")) return AppendNullOrClauseToQuery(filterKey, filterOperator, filterValue);
            return string.Format("{0}{1}{2}{3}{4}", StoredProcedureKeys.TildOperator, filterKey, StoredProcedureKeys.TildOperator, filterOperator, filterValue);
        }

        /// <summary>
        /// Method return the generated query for special case. forex "like"
        /// </summary>
        /// <param name="filterKey">string filterKey</param>
        /// <param name="filterOperator">string filterOperator</param>
        /// <param name="filterValue">string filterValue</param>
        /// <returns>Return the where clause string</returns>
        private static string AppendLikeQuery(string filterKey, string filterOperator, string filterValue)
        {
            if (filterKey.Contains("|")) return AppendOrClauseToQuery(filterKey, filterOperator, filterValue);
            return GenerateLikeQuery(filterKey, filterOperator, filterValue);
        }

        private static string GenerateLikeQuery(string filterKey, string filterOperator, string filterValue)
        {
            return string.Format("{0}{1}{2}{3}'%{4}%'", StoredProcedureKeys.TildOperator, filterKey, StoredProcedureKeys.TildOperator, filterOperator, filterValue);
        }

        private static string GenerateStartwithQuery(string filterKey, string filterOperator, string filterValue)
        {
            return string.Format("{0}{1}{2}{3}'{4}%'", StoredProcedureKeys.TildOperator, filterKey, StoredProcedureKeys.TildOperator, filterOperator, filterValue);
        }

        private static string GenerateEndwithQuery(string filterKey, string filterOperator, string filterValue)
        {
            return string.Format("{0}{1}{2}{3}'%{4}'", StoredProcedureKeys.TildOperator, filterKey, StoredProcedureKeys.TildOperator, filterOperator, filterValue);
        }

        private static string AppendOrClauseToQuery(string filterKey, string filterOperator, string filterValue)
        {
            string strQuery = string.Empty;
            if (filterKey.Contains("|"))
            {
                string innerQuery = string.Empty;
                string[] strSplit = filterKey.Split('|');
                for (int i = 0; i < strSplit.Length; i++)
                {
                    if (!string.IsNullOrEmpty(strSplit[i]))
                    {
                        innerQuery = (string.IsNullOrEmpty(innerQuery)) ? GenerateLikeQuery(strSplit[i], filterOperator, filterValue) : innerQuery + " OR " + GenerateLikeQuery(strSplit[i], filterOperator, filterValue);
                    }
                }
                strQuery = (string.IsNullOrEmpty(innerQuery)) ? string.Empty : string.Format("({0})", innerQuery);
            }
            return strQuery;
        }

        private static string AppendSpecialOrClauseToQuery(string filterKey, string filterOperator, string filterValue)
        {
            string[] strSplit = filterKey.Split('$');
            return string.Format("({0}{1}{2}={3} OR ({4}{5}{6} is null and {7}{8}{9} = 1 ))", StoredProcedureKeys.TildOperator, strSplit[0], StoredProcedureKeys.TildOperator, filterValue, StoredProcedureKeys.TildOperator, strSplit[0], StoredProcedureKeys.TildOperator, StoredProcedureKeys.TildOperator, strSplit[1], StoredProcedureKeys.TildOperator);
        }

        /// <summary>
        /// The method appends Or condition with null value for the filter key passed with "|" separator.
        /// </summary>
        /// <param name="filterKey">Filter key for which the OR condition needs to be created</param>
        /// <param name="filterOperator">Filter operator which defines the conditions</param>
        /// <param name="filterValue">Filter value, the value for the filter key passed.</param>
        /// <returns>Returns the string with Or condition having null value.</returns>
        private static string AppendNullOrClauseToQuery(string filterKey, string filterOperator, string filterValue)
        {
            string[] strSplit = filterKey.Split('|');
            return string.Format("({0}{1}{2}={3} OR {4}{5}{6} Is NULL)", StoredProcedureKeys.TildOperator, strSplit[0], StoredProcedureKeys.TildOperator, filterValue, StoredProcedureKeys.TildOperator, strSplit[0], StoredProcedureKeys.TildOperator);
        }
    }
}

