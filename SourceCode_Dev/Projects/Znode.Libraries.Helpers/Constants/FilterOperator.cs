﻿
namespace Znode.Libraries.Helpers.Constants
{
    public class ProcudureFilterOperators
    {
        public static string Contains = "cn";
        public static new string Equals = "eq";
        public static string EndsWith = "ew";
        public static string GreaterThan = "gt";
        public static string GreaterThanOrEqual = "ge";
        public static string LessThan = "lt";
        public static string LessThanOrEqual = "le";
        public static string NotEquals = "ne";
        public static string StartsWith = "sw";
        public static string Like="lk";
        public static string Is = "is";
    }
}
