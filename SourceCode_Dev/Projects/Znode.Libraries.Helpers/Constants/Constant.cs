﻿
namespace Znode.Libraries.Helpers.Constants
{
    public class Constant
    {
        public const string ExportInventory = "Inventory";
        public const string ExportPricing = "Pricing";
        public const string ExportProduct = "Product";
        public const string ExportAttribute = "Attribute";
        public const string ExportFacet = "Facet";
        public const string ExportShipping = "Shipping";
        public const string ExportCustomerPricing = "CustomerPricing";
        public const string ExportSku = "Sku";
        public const string ExportZipCode = "ZipCode";
        public const string ExportCustomer = "Customer";
        public const string username = "~~username~~ =";
        public const string ExportState = "State";

        #region User Roles Constant
        public const string RoleAdmin = "ADMIN";
        public const string RoleCatalogEditor = "CATALOG EDITOR";
        public const string RoleContentEditor = "CONTENT EDITOR";
        public const string RoleCustomerServiceRep = "CUSTOMER SERVICE REP";
        public const string RoleExecutive = "EXECUTIVE";
        public const string RoleFranchise = "FRANCHISE";
        public const string RoleOrderApprover = "ORDER APPROVER";
        public const string RoleOrderOnly = "ORDER ONLY";
        public const string RoleReviewer = "REVIEWER";
        public const string RoleSEO = "SEO";
        public const string RoleVendor = "VENDOR";
        public const string RoleReviewerManager = "REVIEWER MANAGER";
        #endregion

        #region License Type Constants
        public const string Marketplace = "Marketplace";
        public const string FreeTrial = "FreeTrial";
        public const string StoreMultiplier = "StoreMultiplier";
        public const string MultiFront = "MultiFront";
        public const string MallAdmin = "MallAdmin";
        #endregion
    }
}
