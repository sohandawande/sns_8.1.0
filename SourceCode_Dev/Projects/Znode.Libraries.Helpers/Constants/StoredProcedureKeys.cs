﻿namespace Znode.Libraries.Helpers.Constants
{
    /// <summary>
    /// StoredProcedureKeys
    /// </summary>
    public class StoredProcedureKeys
    {
        //Sp names
        public const string SpGetAttributeTypesBy = "GetAttributeTypesBy";
        public const string SpProductTypeWiseAttributes = "ProductTypeWiseAttributes";
        public const string SpGetProductCategoryByProductId = "ZNodeGetProductCategoryByProductID";
        public const string SpGetVendorProductList = "ZNodeGetVendorProduct";
        public const string SpGetMallAdminProductList = "ZNodeGetVendorProductforMallAdmin";
        public const string SpGetUnAssociatedCategoryByProductId = "ZNodeGetProductCategoryNonAssociated";
        public const string SpGetAccountDetailsByRoleName = "ZNodeAccountDetails";
        public const string SpZNodeCatalogAssociatedCategory = "ZNodeCategory";
        public const string SpGetCategoryAssociatedProducts = "ZNodeGetProductByCriteria";
        public const string SpGetCategoryProducts = "ZNodeGetDistinctProductsByCategory";
        public const string SpGetProductSKUDetails = "ZNodeProductSKUDetails";
        public const string SpSearchAddOns = "ZNodeSearchAddons";
        public const string SpGetAddOnValues = "ZNodeGetQtyReorderLverl";
        public const string SpSearchCategories = "ZNode_SearchCategories";
        public const string SpGetAllPortalCountries = "ZNodeGetPortalCountry";
        public const string spGetSKUProfileEffective = "ZNodeGetSKUProfileEffective";
        public const string SpGetProductsList = "ZNodeGetDistinctProductsByCriteria";
        public const string SpGetCustomerPricing = "ZNodeGetCustomerBasedPricing";
        public const string SpGetcustReview = "ZNodeGetcustReview";
        public const string SpGetAllOrders = "ZNodeGetOrderDetails";
        public const string SpGetVendorOrders = "ZNode_SearchVendorOrder";
        public const string SpGetFrequentlyBoughtProducts = "ZNode_GetFrequentlyBoughtProduct";
        public const string SpGetProductByCategoryBoost = "ZNodeGetProductByCriteriaWithBoost";
        public const string SpGetSKUFacetGroup = "ZNodeGetSKUFacetGroup";
        public const string SpGetProductByProductBoost ="ZNodeGetProductWithProductBoost";
        public const string SpGetRejectionMessages = "ZNodeGetRejectionMessages";
        public const string SpGetYouMayAlsoProductList = "ZNode_GetCrossSellProductType_5";
        public const string SpGetCaseRequestsLis = "ZNodeSearchCase";
        public const string SpGetCustomerList = "ZNodeFullAccountDetails";
        public const string SpZNodeSearchRMA = "ZNodeSearchRMA";
        public const string SpZNodeGetAssocitedHighLights = "ZNode_GetAssocitedHighLights";
        public const string SpZNodeGetAvailableHighlights = "ZNode_GetAvailableHighlights";
        public const string SpZNodeGetAvailableAddOn = "ZNode_GetAvailableAddOn";
        public const string SpZNodeGetDigitalAsset = "ZNodeGetDigitalAsset";
        public const string SpZNodeGetProductPriceListByPortalID = "ZNode_GetProductPriceListByPortalID";
        public const string SpZNodeGetTaxClasses = "Znode_TaxClass";
        public const string SPGetCustomerPricingProduct = "ZNode_SearchCustomerPricingProduct";
        public const string SPGetAccountAssociateProfile = "ZNode_GetAssociatedProfile";
        public const string SPGetProfileNotAssociateCustomer = "ZNode_GetAvailableProfiles";
        public const string SPGetPromotionProduct = "ZNodeSearchProductsByKeyword";
        public const string SPGetAttributeTypeWiseAttributes = "AttributeTypeWiseAttributes";
        public const string SPZNodeGetProductFacetGroup = "ZNodeGetProductFacetGroup";
        public const string SPZNode_GetAssociatedAddOn = "ZNode_GetAssociatedAddOn";
        public const string SPZNode_GetAvailableAddOn = "ZNode_GetAvailableAddOn";
        public const string SPZNode_GetAssocitedHighLights = "ZNode_GetAssocitedHighLights";
        public const string SPZNode_GetAvailableHighlights = "ZNode_GetAvailableHighlights";
        public const string SPZnodeProductTier = "ZnodeProductTier";
        public const string SPZNodeGetDigitalAsset = "ZNodeGetDigitalAsset";
        public const string SPGetProfilewithSerialNumber = "ZnodeGetProfilewithSerialNumber";
        public const string SPGetShippingList = "ZnodeGetShippingList";
        public const string SPGetChildProduct = "ZnodeGetChildProduct";
        public const string SPGetWebpagesProfile ="ZnodeGetWebpagesProfile";
        public const string ZNodeAdminAccounts = "ZNodeAdminAccounts";
        public const string SPZNode_SearchCategoriesByPortalCatalog = "ZNode_SearchCategoriesByPortalCatalog";
        public const string SpGetCustomerData = "ZNodeExportFullAccountDetails";
        public const string SPZNode_GetAssociatedAddOn_2 = "ZNode_GetAssociatedAddOn_2";

        //PRFT Custom Code:Start
        public const string SpGetCreditApplicationList = "PRFT_CreditApplicationList";
        public const string SpGetInventoryList = "PRFT_InventoryList";
        public const string SpGetCustomerUserMappingList = "PRFT_GetCustomerUserMappingList";
        public const string SpGetNotAssociatedCustomerList = "PRFT_GetNotAssociatedCustomerList";
        public const string SpGetSubUserListList = "PRFT_GetSubUserList";
        //PRFT Custom Code:End

        //Operatod
        public const string AND = "and";
        public const string TildOperator = "~~";
        public const string TildEqualToOperator = "~~=";

        //columns
        public const string ProductTypeId = "producttypeid";
        public const string ProductId = "productid";
        public const string PortalId = "portalId";
        public const string Portalid = "portalid";
        public const string AccountId = "accountid";
        public const string RoleName = "rolename";
        public const string CategoryId = "categoryid";

        public const string SkuId = "skuId";

        //Condition
        public const string RoleNameIsNull = "~~rolename~~ is null";
        public const string RoleNameIsNullWithAnd = " and ~~rolename~~ is null";
        public const string GetAccountId = "~~accountid~~={0}";
        public const string FranchiseRole = "FRANCHISE<br>";
        public const string VendorRole = "VENDOR<br>";
        public const string RejectionMessageFilterCondition = "~~PortalId~~={0} and ~~LocaleId~~={1}";
        //Order By 
        public const string sortKey = "sort";
        public const string sortDirKey = "sortDir";
        public const string defaultSortKey = "displayorder asc";
    }
}


