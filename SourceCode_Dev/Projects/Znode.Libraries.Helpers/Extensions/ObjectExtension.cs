﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Libraries.Helpers
{
    public static class ObjectExtension
    {
        /// <summary>
        ///   Searches for the public property with the specified name.
        /// </summary>
        /// <param id="name">The string containing the name of the public property to get.</param>
        /// <returns> An object representing the public property with the specified name, if found otherwise, null.</returns>
        /// <exception cref="System.Reflection.AmbiguousMatchException">if name already exist. "></exception>
        /// <exception cref="System.ArgumentNullException">name is null.</exception>
        public static object GetProperty(this object obj, string name)
        {
            object propValue = null;
            PropertyInfo propInfo = obj.GetType().GetProperty(name);
            if (!Equals(propInfo, null))
            {
                propValue = propInfo.GetValue(obj, null);
            }
            return propValue;
        }

        /// <summary>
        /// Searches for the specified property in the source object, if found, sets the specified value to it.
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="propertyName"></param>
        /// <param name="value"></param>
        public static void SetPropertyValue(this object obj, string propertyName, object value)
        {
            PropertyInfo propInfo = obj.GetType().GetProperty(propertyName);
            if (!Equals(propInfo, null))
            {
                propInfo.SetValue(obj, value);
            }
        }

        /// <summary>
        /// Return true if current object is not equal to specified object.
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="other"></param>
        /// <returns></returns>
        public static bool NotEquals(this object obj, object other)
        {
            return Equals(obj, other) ? false : true;
        }

        /// <summary>
        ///   Returns a selected value when the source is not null; null otherwise.
        /// </summary>
        /// <typeparam name = "T">Type of the source object.</typeparam>
        /// <typeparam name = "TInner">Type of the object which the selector returns.</typeparam>
        /// <param name = "source">The source for this extension method.</param>
        /// <param name = "selector">A function which given the source object, returns a selected value.</param>
        /// <returns>The selected value when source is not null; null otherwise.</returns>
        public static TInner IfNotNull<T, TInner>(this T source, Func<T, TInner> selector)
            where T : class
        {
            return source != null ? selector(source) : default(TInner);
        }
    }
}
