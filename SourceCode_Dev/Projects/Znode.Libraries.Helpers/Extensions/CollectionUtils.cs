﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Libraries.Helpers.Extensions
{
    public static class CollectionUtils
    {
        public static Collection<T> ToCollection<T>(this List<T> items)
        {
            Collection<T> collection = new Collection<T>();

            for (int i = 0; i < items.Count; i++)
            {
                collection.Add(items[i]);
            }

            return collection;
        }

        public static List<T> ToList<T>(this Collection<T> items)
        {
            List<T> list = new List<T>();

            for (int i = 0; i < items.Count; i++)
            {
                list.Add(items[i]);
            }

            return list;
        }
    }
}
