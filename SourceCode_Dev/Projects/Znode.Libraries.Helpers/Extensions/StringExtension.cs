﻿using System;
using System.Linq;

namespace Znode.Libraries.Helpers.Extensions
{
    public static class StringExtension
    {
        /// <summary>
        /// Extension method; the first parameter takes the "this" keyword and specifies the type for which the method is defined
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns> 
        public static string FirstCharToUpper(this string input)
        {
            if (String.IsNullOrEmpty(input))
                throw new ArgumentException("Input string is null");

            return input.First().ToString().ToUpper() + String.Join("", input.Skip(1));
        }

        /// <summary>
        /// Converts string value to int
        /// </summary>
        /// <param name="stringValue">string value to convert</param>
        /// <returns>int value</returns>
        public static int ConvertToInt(this string stringValue)
        {
            int intValue;
            int.TryParse(stringValue, out intValue);
            return intValue;
        }

    }
}
