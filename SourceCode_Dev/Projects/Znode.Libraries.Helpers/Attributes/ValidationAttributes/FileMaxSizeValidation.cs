﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Znode.Libraries.Helpers.Attributes
{
    public class FileMaxSizeValidation : ValidationAttribute
    {
        private readonly int maxFileSize;
       
        public FileMaxSizeValidation(int maxFileSize)
        {
            this.maxFileSize = maxFileSize;
        }
        public override bool IsValid(object value)
        {
            var file = value as HttpPostedFileBase;
            bool isValid = true;

            if (file != null)
            {
                isValid = file.ContentLength <= maxFileSize;
            }
            else
            {
                HttpPostedFileBase[] files = value as HttpPostedFileBase[];
                if (files == null) return true;

                foreach (var item in files)
                {
                    if (item == null) return true;

                    isValid = item.ContentLength <= maxFileSize ? true : false;

                    if (!isValid) break;
                }
            }
            return isValid;
        }
    }
}
