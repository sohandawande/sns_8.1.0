using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Fulfillment
{
    /// <summary>
    /// Provides information about Credit card and authorization
    /// </summary>
    public class ZNodeCreditCard : ZNodeBusinessBase
    {
        #region Private Member Variables
        private string _Gateway = string.Empty;
        private string _MerchantLogin = string.Empty;
        private string _MerchantPassword = string.Empty;
        private string _TransactionAmount = string.Empty;
        private string _TransactionDesc = string.Empty;
        private string _CardExpMonth = string.Empty;
        private string _CardExpYear = string.Empty;
        private string _CardNumber = string.Empty;
        private string _CustomerAddress = string.Empty;
        private string _CustomerCity = string.Empty;
        private string _CustomerCountry = string.Empty;
        private string _CustomerEmail = string.Empty;
        private string _CustomerFirstName = string.Empty;
        private string _CustomerLastName = string.Empty;
        private string _CustomerState = string.Empty;
        private string _CustomerZip = string.Empty;

        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the payment gateway
        /// </summary>
        public string Gateway
        {
            get { return this._Gateway; }
            set { this._Gateway = value; }
        }

        /// <summary>
        /// Gets or sets the Merchant loginid. 
        /// </summary>
        public string MerchantLogin
        {
            get { return this._MerchantLogin; }
            set { this._MerchantLogin = value; }
        }

        /// <summary>
        /// Gets or sets the Merchant Gateway password.
        /// </summary>
        public string MerchantPassword
        {
            get { return this._MerchantPassword; }
            set { this._MerchantPassword = value; }
        }

        /// <summary>
        /// Gets or sets the transaction amount (Ordered amount).
        /// </summary>
        public string TransactionAmount
        {
            get { return this._TransactionAmount; }
            set { this._TransactionAmount = value; }
        }

        /// <summary>
        /// Gets or sets the transaction description.
        /// </summary>
        public string TransactionDesc
        {
            get { return this._TransactionDesc; }
            set { this._TransactionDesc = value; }
        }

        /// <summary>
        /// Gets or sets the Credit card expiration month property.
        /// </summary>
        public string CardExpMonth
        {
            get { return this._CardExpMonth; }
            set { this._CardExpMonth = value; }
        }

        /// <summary>
        /// Gets or sets the Credit card epiration year property.
        /// </summary>
        public string CardExpYear
        {
            get { return this._CardExpYear; }
            set { this._CardExpYear = value; }
        }

        /// <summary>
        /// Gets or sets the Credit card Number.
        /// </summary>
        public string CardNumber
        {
            get { return this._CardNumber; }
            set { this._CardNumber = value; }
        }

        /// <summary>
        /// Gets or sets the Customer billing address.
        /// </summary>
        public string CustomerAddress
        {
            get { return this._CustomerAddress; }
            set { this._CustomerAddress = value; }
        }

        /// <summary>
        /// Gets or sets the Customer billing city.
        /// </summary>
        public string CustomerCity
        {
            get { return this._CustomerCity; }
            set { this._CustomerCity = value; }
        }

        /// <summary>
        /// Gets or sets the Customer billing country.
        /// </summary>
        public string CustomerCountry
        {
            get { return this._CustomerCountry; }
            set { this._CustomerCountry = value; }
        }

        /// <summary>
        /// Gets or sets the Customer billing email id.
        /// </summary>
        public string CustomerEmail
        {
            get { return this._CustomerEmail; }
            set { this._CustomerEmail = value; }
        }

        /// <summary>
        /// Gets or sets the Customer billing first name.
        /// </summary>
        public string CustomerFirstName
        {
            get { return this._CustomerFirstName; }
            set { this._CustomerFirstName = value; }
        }

        /// <summary>
        /// Gets or sets the Customer billing Last name.
        /// </summary>
        public string CustomerLastName
        {
            get { return this._CustomerLastName; }
            set { this._CustomerLastName = value; }
        }

        /// <summary>
        /// Gets or sets the Customer billing state code.
        /// </summary>
        public string CustomerState
        {
            get { return this._CustomerState; }
            set { this._CustomerState = value; }
        }

        /// <summary>
        /// Gets or sets the Customer billing postal code
        /// </summary>        
        public string CustomerZip
        {
            get { return this._CustomerZip; }
            set { this._CustomerZip = value; }
        } 
        #endregion
    }
}
