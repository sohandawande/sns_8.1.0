using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Fulfillment
{
    /// <summary>
    /// Gets or sets a response to a order submittal
    /// </summary>
    public class ZNodeSubmitOrderResponse : ZNodeBusinessBase
    {
        private string _ResponseCode = string.Empty;
        private string _ResponseText = string.Empty;
        private bool _IsSuccess = false;
        private ZNodePaymentResponse _PaymentResponse;

        /// <summary>
        /// Gets or sets the Gateway Response code 
        /// </summary>
        public string ResponseCode
        {
            get { return this._ResponseCode; }
            set { this._ResponseCode = value; }
        }

        /// <summary>
        /// Gets or sets the Gateway Response Text
        /// </summary>
        public string ResponseText
        {
            get { return this._ResponseText; }
            set { this._ResponseText = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the transaction is success or failure.
        /// </summary>
        public bool IsSuccess
        {
            get { return this._IsSuccess; }
            set { this._IsSuccess = value; }
        }

        /// <summary>
        /// Gets or sets gateway response as a PaymentResponse object
        /// </summary>
        public ZNodePaymentResponse PaymentResponse
        {
            get { return this._PaymentResponse; }
            set { this._PaymentResponse = value; }
        }
    }
}
