using System;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Fulfillment
{
    /// <summary>
    /// Provides methods to manage Orders and OrderLine Items during Checkout
    /// </summary>
    [Serializable()]
    public class ZNodeOrderFulfillment : ZNodeOrder
    {
        #region Private Variables
        private ZNodeShoppingCart _cart = (ZNodeShoppingCart)ZNodeShoppingCart.CreateFromSession(ZNodeSessionKeyType.ShoppingCart);
        private string referralStatus;
        #endregion

        public ZNodeShoppingCart Cart
        {
            get { return _cart; }
        }

        #region Constructor
        /// <summary>
        /// Initializes a new instance of the ZNodeOrderFulfillment class
        /// </summary>
        public ZNodeOrderFulfillment()
        {
        }

        /// <summary>
        /// Initializes a new instance of the ZNodeOrderFulfillment class. This constructor initializes the order and OrderLineItem entity objects.
        /// </summary>
        /// <param name="userAccount">ZNodeUserAccount object.</param>
        /// <param name="shoppingCart">ZNodeShoppingCart object.</param>
        /// <param name="portalId">Portal Id of the store.</param>
        public ZNodeOrderFulfillment(ZNodeUserAccount userAccount, ZNodeShoppingCart shoppingCart, int portalId)
        {
            this._cart = shoppingCart;
            this.PortalId = portalId;
            this.AccountID = userAccount.AccountID;
            this.TaxCost = shoppingCart.TaxCost;
            this.VAT = shoppingCart.VAT;
            this.SalesTax = shoppingCart.SalesTax;
            this.HST = shoppingCart.HST;
            this.PST = shoppingCart.PST;
            this.GST = shoppingCart.GST;

            // This property will return the actual ShippingCost - Discount Amount
            this.ShippingCost = shoppingCart.ShippingCost;
            this.SubTotal = shoppingCart.SubTotal;
            this.Total = shoppingCart.Total;
            this.BillingAddress = userAccount.BillingAddress;
            this.ShippingAddress = userAccount.ShippingAddress;
            // Commenting this code as we need to rewrite the code for #16485
            //this.Order.ShipEmailID = userAccount.EmailID;
            this.Order.BillingEmailId = userAccount.EmailID;

            // Coupon discount
            this.DiscountAmount = shoppingCart.Discount;

            this.referralStatus = userAccount.ReferralStatus;

            foreach (ZNodeCoupon coupon in shoppingCart.Coupons)
            {
                if (coupon.CouponApplied && coupon.CouponValid)
                {
                    this.CouponCode += "," + coupon.Coupon;
                }
            }

            // Loop through cart and add line items
            foreach (ZNodeShoppingCartItem shoppingCartItem in shoppingCart.ShoppingCartItems)
            {
                decimal productPrice = shoppingCartItem.UnitPrice - shoppingCartItem.Product.AddOnPrice;
                productPrice = productPrice < 0 ? 0 : productPrice;

                OrderLineItem orderLineItem = new OrderLineItem();
                orderLineItem.Description = shoppingCartItem.Product.ShoppingCartDescription;
                orderLineItem.Name = shoppingCartItem.Product.Name;
                orderLineItem.SKU = shoppingCartItem.Product.SKU;
                orderLineItem.Quantity = shoppingCartItem.Quantity;
                orderLineItem.ProductNum = shoppingCartItem.Product.ProductNum;

                // Final product price
                orderLineItem.Price = productPrice;
                orderLineItem.DiscountAmount = shoppingCartItem.Product.DiscountAmount;
                orderLineItem.ShipSeparately = shoppingCartItem.Product.ShipSeparately;
                orderLineItem.ParentOrderLineItemID = null;
                orderLineItem.DownloadLink = shoppingCartItem.Product.DownloadLink;
                orderLineItem.HST = shoppingCartItem.Product.HST;
                orderLineItem.GST = shoppingCartItem.Product.GST;
                orderLineItem.PST = shoppingCartItem.Product.PST;
                orderLineItem.VAT = shoppingCartItem.Product.VAT;
                orderLineItem.SalesTax = shoppingCartItem.Product.SalesTax;

                // If product is marked as ship seperately, 
                // then make a shipping cost entry in orderlineItem table.
                if (shoppingCartItem.Product.ShipSeparately)
                {
                    orderLineItem.ShippingCost = shoppingCartItem.Product.ShippingCost;
                }

                // Loop through the Selected addons for each Shopping cartItem
                foreach (ZNodeAddOnEntity AddOn in shoppingCartItem.Product.SelectedAddOns.AddOnCollection)
                {
                    foreach (ZNodeAddOnValueEntity AddOnValue in AddOn.AddOnValueCollection)
                    {
                        OrderLineItem childOrderLineItem = new OrderLineItem();

                        childOrderLineItem.Description = AddOnValue.Name;
                        childOrderLineItem.Name = AddOn.Name;
                        childOrderLineItem.SKU = AddOnValue.SKU;
                        childOrderLineItem.Quantity = shoppingCartItem.Quantity;
                        childOrderLineItem.ProductNum = AddOn.Name;
                        childOrderLineItem.Price = AddOnValue.FinalPrice;
                        childOrderLineItem.DiscountAmount = AddOnValue.DiscountAmount;
                        childOrderLineItem.HST = AddOnValue.HST;
                        childOrderLineItem.GST = AddOnValue.GST;
                        childOrderLineItem.PST = AddOnValue.PST;
                        childOrderLineItem.VAT = AddOnValue.VAT;
                        childOrderLineItem.SalesTax = AddOnValue.SalesTax;

                        // Set new ParentOrderLineItemId to this item
                        childOrderLineItem.ParentOrderLineItemID = orderLineItem.OrderLineItemID;

                        // Insert Product Addons to the OrderLineItem table
                        orderLineItem.OrderLineItemCollection.Add(childOrderLineItem);
                    }
                }

                this.OrderLineItems.Add(orderLineItem);
            }
        }

        public ZNodeOrderFulfillment(ZNodeShoppingCart shoppingcart)
        {
            if (_cart == null)
                _cart = shoppingcart;

            if (shoppingcart != null)
                this.shoppingCart = shoppingcart;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Submits order to database
        /// </summary>
        /// <param name="orderState">ZNodeOrderState object.</param>
        public int AddOrderToDatabase(ZNodeOrderState orderState)
        {
            // Set order state
            this.OrderStateID = (int)orderState;

            // Set order date and status
            this.OrderDate = System.DateTime.Now;

            // Coupon code.
            this.DiscountAmount = DiscountAmount;
            this.CouponCode = CouponCode;

            if (this.ShippingID == 0)
            {
                this.ShippingID = null;
            }

            // General setings
            this.AdditionalInstructions = AdditionalInstructions;

            // Commenting this code as we need to rewrite the code for #16485
            //this.Order.ShipEmailID = Email;
            this.Order.BillingEmailId = Email;

            // Insert order into database
            OrderService orderService = new OrderService();
            orderService.Insert(this.Order);

            // Get new orderId
            int orderId = this.OrderID;

            // Add orderline items to database
            OrderLineItemService orderLineItemService = new OrderLineItemService();

            // Loop through the Order Line Items
            foreach (OrderLineItem orderLineItem in this.OrderLineItems)
            {
                // Set orderid for line items
                orderLineItem.OrderID = orderId;
                if (Convert.ToBoolean(orderLineItem.IsRecurringBilling))
                {
                    orderLineItem.TransactionNumber = _cart.Token;
                }

                foreach (OrderLineItem childItem in orderLineItem.OrderLineItemCollection)
                {
                    // Set orderid for line items
                    childItem.OrderID = orderId;
                    if (Convert.ToBoolean(childItem.IsRecurringBilling))
                    {
                        childItem.TransactionNumber = _cart.Token;
                    }

                    foreach (OrderLineItem subchildItem in childItem.OrderLineItemCollection)
                    {
                        // Set orderid for line items
                        subchildItem.OrderID = orderId;
                        if (Convert.ToBoolean(subchildItem.IsRecurringBilling))
                        {
                            subchildItem.TransactionNumber = _cart.Token;
                        }
                    }
                }

                // Insert parent Order Line Item to the database
                orderLineItemService.DeepSave(orderLineItem, DeepSaveType.IncludeChildren, typeof(TList<OrderLineItem>));
            }
            return orderId;
        }

        /// <summary>
        /// Updates the order status
        /// </summary>
        /// <param name="orderState">ZNodeOrderState object.</param>
        public void UpdateOrderStatus(ZNodeOrderState orderState)
        {
            this.Order.OrderStateID = (int)orderState;
            this.Order.OrderLineItemCollection = this.OrderLineItems;

            OrderService orderService = new OrderService();
            orderService.DeepSave(this.Order, DeepSaveType.IncludeChildren, typeof(TList<OrderLineItem>));
        }

        /// <summary>
        /// Add an order line item to the collection
        /// </summary>
        /// <param name="orderLineItem">OrderLineItem object.</param>
        public void AddOrderLineItem(OrderLineItem orderLineItem)
        {
            this.OrderLineItems.Add(orderLineItem);
        }

        /// <summary>
        /// Add an entry to gift card.
        /// </summary>
        /// <param name="account">ZNodeUserAccount object</param>
        /// <param name="shoppingCart">ZNodeShoppingCart object</param>
        /// <param name="orderFulfillment">ZNodeOrderFulfillment object.</param>
        /// <returns>Returns true if success else false. </returns>
        public bool AddToGiftCard(ZNodeUserAccount account, ZNodeShoppingCart shoppingCart, ZNodeOrderFulfillment orderFulfillment)
        {
            bool isAdded = true;

            ZNode.Libraries.ECommerce.Catalog.ZNodeProduct product = new ZNode.Libraries.ECommerce.Catalog.ZNodeProduct();

            // Check all shopping card items and if product type is gift card then add an entry to ZNodeGiftCard.
            foreach (ZNodeShoppingCartItem currentItem in shoppingCart.ShoppingCartItems)
            {
                int orderLineItemId = 0;
                if (currentItem.Product.GiftCardNumber.Trim() != string.Empty)
                {
                    // Get order line item ID
                    orderFulfillment.OrderLineItems.ApplyFilter(delegate(OrderLineItem li)
                    {
                        // Compare product SKU and gift card number in description field.
                        return li.SKU == currentItem.Product.SKU && li.Description.ToUpper().Contains(currentItem.Product.GiftCardNumber.ToUpper());
                    });

                    if (orderFulfillment.OrderLineItems.Count > 0)
                    {
                        orderLineItemId = orderFulfillment.OrderLineItems[0].OrderLineItemID;
                    }

                    GiftCardService giftCardService = new GiftCardService();
                    GiftCard giftCard = new GiftCard();
                    giftCard.PortalId = ZNodeConfigManager.SiteConfig.PortalID;
                    giftCard.Name = currentItem.Product.Name;
                    giftCard.CardNumber = currentItem.Product.GiftCardNumber;
                    giftCard.CreatedBy = account.AccountID;
                    giftCard.CreateDate = DateTime.Now;
                    giftCard.OrderItemId = orderLineItemId;

                    // If Account Id set to NULL then, card can be used by any customer. Account Id will be set when first time this card used.
                    giftCard.AccountId = null;
                    giftCard.ExpirationDate = product.GetExpirationDate(currentItem.Product.ExpirationPeriod, currentItem.Product.ExpirationFrequency);
                    giftCard.Amount = currentItem.UnitPrice * currentItem.Quantity;

                    isAdded = giftCardService.Insert(giftCard);
                }
            }
            orderFulfillment.OrderLineItems.RemoveFilter();
            return isAdded;
        }

        /// <summary>
        /// Add an entry to gift card history.
        /// </summary>
        /// <param name="orderFulfillment">ZNodeOrderFullfilment object</param>
        /// <returns>Returns true if inserted otherwise false.</returns>
        public bool AddToGiftCardHistory(ZNodeOrderFulfillment orderFulfillment)
        {
            bool isAdded = true;
            if (!string.IsNullOrEmpty(orderFulfillment.GiftCardNumber))
            {
                GiftCardService giftCardService = new GiftCardService();
                GiftCard giftCard = giftCardService.GetByCardNumber(orderFulfillment.GiftCardNumber);

                // Add an entry to Gift Card History to keep record.
                GiftCardHistory giftCardHistory = new GiftCardHistory();
                GiftCardHistoryService giftCardHistoryService = new GiftCardHistoryService();
                giftCardHistory.GiftCardID = giftCard.GiftCardId;
                giftCardHistory.TransactionDate = DateTime.Now;
                giftCardHistory.TransactionAmount = orderFulfillment.GiftCardAmount;
                giftCardHistory.OrderID = orderFulfillment.OrderID;

                isAdded = giftCardHistoryService.Insert(giftCardHistory);
            }

            return isAdded;
        }

        #endregion
    }
}
