﻿using System;
using System.Data;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.SEO
{
    /// <summary>
    /// XML Site Map File.
    /// </summary>
    public class ZNodeXmlSitemap : ZNodeBusinessBase
    {
        #region Private Variables
        private ZNodeUrl znodeUrl = new ZNodeUrl();
        #endregion

        #region Public Methods
        /// <summary>
        /// Gets the product list
        /// For more adding tags for the product, please refere to  http://www.google.com/support/merchants/bin/answer.py?answer=188494
        /// </summary>
        /// <param name="portalId">Portal Id to get the product</param>
        /// <param name="changeFreq">Change Frequency</param>
        /// <param name="priority">Priority of the page</param>
        /// <param name="rootTag">Root tag for the XML</param>
        /// <param name="rootTagValue">Root tag value</param>
        /// <param name="xmlFileName"> XML File Name</param>
        /// <param name="lastModified">Last Modified date</param>
        /// <returns>Returns true if the XML file is created</returns>

        public int GetProductList(string portalId, string rootTagValue, string xmlFileName, string lastModified, string googleFeedTitle, string googleFeedLink, string googleFeedDesc, string feedType)
        {
            ProductHelper productHelper = new ProductHelper();
            int fileCount = 0;
            DataSet datasetProductList;

            //datasetProductList = productHelper.GetProductListXMLSiteMap(portalId);
            datasetProductList = productHelper.GetProductListXMLSiteMap(portalId, feedType);

            if (datasetProductList != null)
            {
                ZNodeRssWriter rssWriter = new ZNodeRssWriter();

                datasetProductList.Tables[0].TableName = "url";

                foreach (DataRow dr in datasetProductList.Tables[0].Rows)
                {
                    int pid = Convert.ToInt16(dr["PortalId"].ToString());
                    if (pid > 0 && string.IsNullOrEmpty(dr["DomainName"].ToString()))
                        break;

                    //string domainname = "www." + dr["DomainName"].ToString();
                    //string domainname = System.Configuration.ConfigurationManager.AppSettings["DemoWebsiteUrl"];
                    string domainname = System.Configuration.ConfigurationManager.AppSettings["DemoWebsiteUrl"].TrimEnd('/');
                    string apiDomainName = string.Concat(System.Web.HttpContext.Current.Request.Url.Scheme, System.Uri.SchemeDelimiter, System.Web.HttpContext.Current.Request.Url.Host);
                    if (System.Web.HttpContext.Current.IsDebuggingEnabled)
                    {
                        apiDomainName = string.Concat(apiDomainName, ":", System.Web.HttpContext.Current.Request.Url.Port);
                    }
                    apiDomainName = apiDomainName.TrimEnd('/');
                    //if (pid > 0)
                    //    domainname = this.znodeUrl.GetWWWDomain();

                    //dr["loc"] = domainname + ZNodeSEOUrl.MakeURL(dr["g:id"].ToString(), SEOUrlType.Product, dr["loc"].ToString()).Substring(1);
                    dr["loc"] = domainname + ZNodeSEOUrl.MakeUrlForMvc(dr["g:id"].ToString(), SEOUrlType.Product, dr["loc"].ToString()).Substring(1);

                    if (lastModified == "None" || string.IsNullOrEmpty(lastModified))
                    {
                        dr["lastmod"] = System.DBNull.Value;
                    }
                    else if ((lastModified != "DB") && (lastModified != "None"))
                    {
                        dr["lastmod"] = Convert.ToDateTime(lastModified);
                    }

                    //dr["link"] = domainname + ZNodeSEOUrl.MakeURL(dr["g:id"].ToString(), SEOUrlType.Product, string.Empty).Substring(1);
                    //dr["link"] = domainname + ZNodeSEOUrl.MakeURLforMVC(dr["g:id"].ToString(), SEOUrlType.Product, string.Empty).Substring(1);
                    dr["link"] = dr["loc"];
                    dr["g:image_link"] = apiDomainName + ZNodeConfigManager.EnvironmentConfig.SmallImagePath.Substring(1) + dr["g:image_link"];
                }

                if (datasetProductList.Tables[0].Columns.Contains("DomainName"))
                    datasetProductList.Tables[0].Columns.Remove("DomainName");
                if (datasetProductList.Tables[0].Columns.Contains("PortalId"))
                    datasetProductList.Tables[0].Columns.Remove("PortalId");
                //fileCount = rssWriter.CreateXMLSiteMap(datasetProductList, rootTag, rootTagValue, xmlFileName);
                fileCount = rssWriter.CreateGoogleSiteMap(datasetProductList, rootTagValue, xmlFileName, googleFeedTitle, googleFeedLink, googleFeedDesc);
            }

            return fileCount;
        }
        public int GetProductList(string portalId, string changeFreq, decimal priority, string rootTag, string rootTagValue, string xmlFileName, string lastModified)
        {
            ProductHelper productHelper = new ProductHelper();
            int fileCount = 0;
            DataSet datasetProductList;

            //datasetProductList = productHelper.GetProductListXMLSiteMap(portalId);
            datasetProductList = productHelper.GetProductListXMLSiteMap(portalId, "Google");

            if (datasetProductList != null)
            {
                ZNodeRssWriter rssWriter = new ZNodeRssWriter();

                datasetProductList.Tables[0].TableName = "url";
                DataColumn dcolChangeFreq = new DataColumn("changefreq");
                dcolChangeFreq.DefaultValue = changeFreq;
                DataColumn dcolPriority = new DataColumn("priority");
                dcolPriority.DefaultValue = priority;

                datasetProductList.Tables[0].Columns.Add(dcolChangeFreq);
                datasetProductList.Tables[0].Columns.Add(dcolPriority);

                foreach (DataRow dr in datasetProductList.Tables[0].Rows)
                {
                    int pid = Convert.ToInt16(dr["PortalId"].ToString());
                    if (pid > 0 && string.IsNullOrEmpty(dr["DomainName"].ToString()))
                        break;

                    //string domainname = "www." + dr["DomainName"].ToString();
                    //string domainname = System.Configuration.ConfigurationManager.AppSettings["DemoWebsiteUrl"];
                    string domainname = System.Configuration.ConfigurationManager.AppSettings["DemoWebsiteUrl"].TrimEnd('/');
                    string apiDomainName = string.Concat(System.Web.HttpContext.Current.Request.Url.Scheme, System.Uri.SchemeDelimiter, System.Web.HttpContext.Current.Request.Url.Host);
                    if (System.Web.HttpContext.Current.IsDebuggingEnabled)
                    {
                        apiDomainName = string.Concat(apiDomainName, ":", System.Web.HttpContext.Current.Request.Url.Port);
                    }
                    apiDomainName = apiDomainName.TrimEnd('/');
                    //if (pid > 0)
                    //    domainname = this.znodeUrl.GetWWWDomain();

                    //dr["loc"] = domainname + ZNodeSEOUrl.MakeURL(dr["g:id"].ToString(), SEOUrlType.Product, dr["loc"].ToString()).Substring(1);
                    dr["loc"] = domainname + ZNodeSEOUrl.MakeUrlForMvc(dr["g:id"].ToString(), SEOUrlType.Product, dr["loc"].ToString()).Substring(1);

                    //if (lastModified == "None")
                    if (Equals(lastModified, ""))
                    {
                        dr["lastmod"] = System.DBNull.Value;
                    }
                    else if ((lastModified != "DB") && (lastModified != "None"))
                    {
                        dr["lastmod"] = Convert.ToDateTime(lastModified);
                    }

                    //dr["link"] = domainname + ZNodeSEOUrl.MakeURL(dr["g:id"].ToString(), SEOUrlType.Product, string.Empty).Substring(1);
                    dr["link"] = dr["loc"];
                    dr["g:image_link"] = apiDomainName + ZNodeConfigManager.EnvironmentConfig.SmallImagePath.Substring(1) + dr["g:image_link"];
                }
                if(datasetProductList.Tables[0].Columns.Contains("DomainName"))
                datasetProductList.Tables[0].Columns.Remove("DomainName");
                if (datasetProductList.Tables[0].Columns.Contains("PortalId"))
                datasetProductList.Tables[0].Columns.Remove("PortalId");
                fileCount = rssWriter.CreateXMLSiteMap(datasetProductList, rootTag, rootTagValue, xmlFileName);
            }
            return fileCount;
        }

        /// <summary>
        /// Gets the category List
        /// </summary>
        /// <param name="portalId">Portal Id to get category</param>
        /// <param name="changeFreq">Change Frequency</param>
        /// <param name="priority">Priority of the page</param>
        /// <param name="rootTag">Root tag for the XML</param>
        /// <param name="rootTagValue">Root tag value</param>
        /// <param name="xmlFileName"> XML File Name</param>
        /// <param name="lastModified">Last Modified date</param>
        /// <returns>Returns true if the XML file is created</returns>
        public int GetCategoryList(string portalId, string changeFreq, decimal priority, string rootTag, string rootTagValue, string xmlFileName, string lastModified)
        {
            CategoryHelper categoryHelper = new CategoryHelper();
            int fileCount = 0;
            DataSet datasetCategoryList;

            datasetCategoryList = categoryHelper.GetCategoryListXMLSiteMap(portalId);

            if (datasetCategoryList != null)
            {
                ZNodeRssWriter rssWriter = new ZNodeRssWriter();

                if (priority.Equals(0.0M)) priority = 1.0M;

                datasetCategoryList.Tables[0].TableName = "url";
                DataColumn dcolChangeFreq = new DataColumn("changefreq");
                dcolChangeFreq.DefaultValue = changeFreq;
                DataColumn dcolPriority = new DataColumn("priority");
                dcolPriority.DefaultValue = priority;

                datasetCategoryList.Tables[0].Columns.Add(dcolChangeFreq);
                datasetCategoryList.Tables[0].Columns.Add(dcolPriority);

                foreach (DataRow dr in datasetCategoryList.Tables[0].Rows)
                {
                    int pid = Convert.ToInt16(dr["PortalId"].ToString());
                    if (pid > 0 && string.IsNullOrEmpty(dr["DomainName"].ToString()))
                        break;

                    //string domainname = "www." + dr["DomainName"].ToString();
                    //PRFT Custom Code: start
                    //string domainname = System.Configuration.ConfigurationManager.AppSettings["DemoWebsiteUrl"];
                    string domainname = System.Configuration.ConfigurationManager.AppSettings["DemoWebsiteUrl"].TrimEnd('/');
                    //PRFT Custom Code: end
                    //if (pid > 0)
                    //    domainname = this.znodeUrl.GetWWWDomain();

                    //dr["loc"] = domainname + ZNodeSEOUrl.MakeURL(dr["CategoryId"].ToString(), SEOUrlType.Category, dr["loc"].ToString()).Substring(1);
                    dr["loc"] = domainname + ZNodeSEOUrl.MakeUrlForMvc(dr["CategoryId"].ToString(), SEOUrlType.Category, dr["loc"].ToString()).Substring(1);
                    
                    if (Equals(lastModified,""))
                    {
                        dr["lastmod"] = System.DBNull.Value;
                    }
                    else if ((!Equals(lastModified, "DB")) && (!Equals(lastModified, "None")))
                    {
                        dr["lastmod"] = Convert.ToDateTime(lastModified);
                    }
                }

                datasetCategoryList.Tables[0].Columns.Remove("CategoryId");
                if (datasetCategoryList.Tables[0].Columns.Contains("DomainName"))
                    datasetCategoryList.Tables[0].Columns.Remove("DomainName");
                if (datasetCategoryList.Tables[0].Columns.Contains("PortalId"))
                    datasetCategoryList.Tables[0].Columns.Remove("PortalId");
                fileCount = rssWriter.CreateXMLSiteMap(datasetCategoryList, rootTag, rootTagValue, xmlFileName);
            }

            return fileCount;
        }

        /// <summary>
        /// Gets the category List
        /// </summary>
        /// <param name="portalId">Portal Id to get the contents</param>
        /// <param name="changeFreq">Change Frequency</param>
        /// <param name="priority">Priority of the page</param>
        /// <param name="rootTag">Root tag for the XML</param>
        /// <param name="rootTagValue">Root tag value</param>
        /// <param name="xmlFileName"> XML File Name</param>
        /// <param name="lastModified">Last Modified date</param>
        /// <returns>Returns true if the XML file is created</returns>
        public int GetContentPagesList(string portalId, string changeFreq, decimal priority, string rootTag, string rootTagValue, string xmlFileName, string lastModified)
        {
            ContentHelper contentHelper = new ContentHelper();
            int fileCount = 0;
            DataSet datasetContentPagesList;

            datasetContentPagesList = contentHelper.GetContentListXMLSiteMap(portalId);

            if (datasetContentPagesList != null)
            {
                ZNodeRssWriter rssWriter = new ZNodeRssWriter();

                datasetContentPagesList.Tables[0].TableName = "url";
                DataColumn dcolChangeFreq = new DataColumn("changefreq");
                dcolChangeFreq.DefaultValue = changeFreq;
                DataColumn dcolPriority = new DataColumn("priority");
                dcolPriority.DefaultValue = priority;

                datasetContentPagesList.Tables[0].Columns.Add(dcolChangeFreq);
                datasetContentPagesList.Tables[0].Columns.Add(dcolPriority);

                foreach (DataRow dr in datasetContentPagesList.Tables[0].Rows)
                {
                    int pid = Convert.ToInt16(dr["PortalId"].ToString());
                    if (pid > 0 && string.IsNullOrEmpty(dr["DomainName"].ToString()))
                        break;

                    //string domainname = "www." + dr["DomainName"].ToString();
                    //string domainname = System.Configuration.ConfigurationManager.AppSettings["DemoWebsiteUrl"];
                    string domainname = System.Configuration.ConfigurationManager.AppSettings["DemoWebsiteUrl"].TrimEnd('/');

                    //if (pid > 0)
                    //    domainname = this.znodeUrl.GetWWWDomain();
                    //dr["loc"] = domainname + ZNodeSEOUrl.MakeURL(dr["Name"].ToString(), SEOUrlType.ContentPage, dr["loc"].ToString()).Substring(1);
                    dr["loc"] = domainname + ZNodeSEOUrl.MakeUrlForMvc(dr["Name"].ToString(), SEOUrlType.ContentPage, dr["loc"].ToString()).Substring(1);
                    
                    //if (lastModified == "None")
                    if (Equals(lastModified, ""))
                    {
                        dr["lastmod"] = System.DBNull.Value;
                    }
                    else if ((lastModified != "DB") && (lastModified != "None"))
                    {
                        dr["lastmod"] = Convert.ToDateTime(lastModified);
                    }
                }

                datasetContentPagesList.Tables[0].Columns.Remove("Name");
                if (datasetContentPagesList.Tables[0].Columns.Contains("DomainName"))
                    datasetContentPagesList.Tables[0].Columns.Remove("DomainName");
                if (datasetContentPagesList.Tables[0].Columns.Contains("PortalId"))
                    datasetContentPagesList.Tables[0].Columns.Remove("PortalId");
                fileCount = rssWriter.CreateXMLSiteMap(datasetContentPagesList, rootTag, rootTagValue, xmlFileName);
            }

            return fileCount;
        }

        public string GenerateGoogleSiteMapIndexFiles(int fileNameCount, string txtXMLFileName)
        {
            ZNodeRssWriter rssWriter = new ZNodeRssWriter();
            return rssWriter.GenerateGoogleSiteMapIndexFiles(fileNameCount, txtXMLFileName);
        }
        #endregion
    }
}
