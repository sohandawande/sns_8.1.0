using ZNode.Libraries.DataAccess.Service;

namespace ZNode.Libraries.ECommerce.SEO
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Web;
    using System.Web.Configuration;
    using ZNode.Libraries.DataAccess.Data;
    using ZNode.Libraries.DataAccess.Entities;
    using ZNode.Libraries.Framework.Business;

    /// <summary>
    ///  Provides methods to rewrite SEO friendly url
    /// </summary>
    public class ZNodeSEOUrl : ZNodeSeoUrlBase
    {
        #region Private Constants
        private const int LocaleID = 43;
        #endregion

        #region Constructor
        public ZNodeSEOUrl()
        {
        }
        #endregion

        #region Public static methods
        /// <summary>
        ///  Returns the internal rewrite path (real path for seo url path)
        /// </summary>
        /// <param name="CurrentUrl"></param>
        /// <param name="localeID"></param>
        /// <returns></returns>
        public static string RewriteUrlPath(string currentUrl, out int localeID)
        {
            string path = System.Web.HttpContext.Current.Request.Url.PathAndQuery.ToLower();
            string queryStrings = System.Web.HttpContext.Current.Request.QueryString.ToString();
            string fileName = currentUrl;
            string realUrlPath = string.Empty;
            bool isRedirect = false;
            int position = fileName.LastIndexOf("/");

            localeID = ZNodeSEOUrl.LocaleID;

            if (position > 0)
            {
                fileName = currentUrl.Substring(position);
            }

            // if product page is requested
            if (path.IndexOf("product.aspx?zpid=") > -1)
            {
                System.Collections.Specialized.NameValueCollection queryStringCollection =
                                                System.Web.HttpContext.Current.Request.QueryString;

                string productId = queryStringCollection["zpid"];

                queryStrings = queryStringCollection.ToString().ToLower().Replace("zpid=" + productId, string.Empty);

                fileName += "?zpid=" + productId;
            }
            else if (path.Contains("category.aspx?zcid="))
            {
                System.Collections.Specialized.NameValueCollection queryStringCollection =
                                                System.Web.HttpContext.Current.Request.QueryString;

                string categoryId = queryStringCollection["zcid"];
                queryStrings = queryStringCollection.ToString().ToLower().Replace("zcid=" + categoryId, string.Empty);

                fileName += "?zcid=" + categoryId;
            }
            else if (path.Contains("content.aspx?page="))
            {
                System.Collections.Specialized.NameValueCollection queryStringCollection =
                                                System.Web.HttpContext.Current.Request.QueryString;

                string pageName = queryStringCollection["page"];
                queryStrings = queryStringCollection.ToString().ToLower().Replace("page=" + pageName, string.Empty);

                fileName += "?page=" + pageName;
            }
            else if (path.Contains("content.aspx?zpgid="))
            {
                System.Collections.Specialized.NameValueCollection queryStringCollection =
                                                System.Web.HttpContext.Current.Request.QueryString;

                string pageName = queryStringCollection["zpgid"];
                queryStrings = queryStringCollection.ToString().ToLower().Replace("zpgid=" + pageName, string.Empty);

                fileName += "?zpgid=" + pageName;
            }
            else
            {
                // Get real url to rewrite the path
                ZNodeSEOUrl seoFriendlyURL = new ZNodeSEOUrl();
                realUrlPath = seoFriendlyURL.SeoUrlToReal("~" + fileName, out isRedirect);
                string applicationPath = System.Web.HttpContext.Current.Request.ApplicationPath.ToLower();

                if (realUrlPath.Length > 0)
                {
                    // Check for proper Url                    
                    if (applicationPath.Equals("/"))
                    {
                        fileName = fileName.Replace("/", string.Empty);
                    }

                    StringBuilder properUrl = new StringBuilder();

                    if (!isRedirect && queryStrings.Length > 0)
                    {
                        queryStrings = "?" + queryStrings;
                    }

                    properUrl.Append(System.Web.HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority));
                    properUrl.Append(applicationPath);
                    properUrl.Append(fileName);
                    properUrl.Append(queryStrings.ToLower());

                    // Don't rewrite if its not a proper URL
                    if (System.Web.HttpContext.Current.Request.Url.AbsoluteUri.ToLower() != properUrl.ToString())
                    {
                        realUrlPath = string.Empty;
                    }
                }

                // 301 redirect section
                if (realUrlPath.Length > 0 && isRedirect)
                {
                    StringBuilder rewriteUrl = new StringBuilder();

                    rewriteUrl.Append(System.Web.HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority));

                    if (!applicationPath.Equals("/"))
                    {
                        rewriteUrl.Append(applicationPath);
                    }

                    rewriteUrl.Append(realUrlPath.Replace("~", string.Empty));
                    rewriteUrl.Append(queryStrings.Replace("&", "?"));

                    System.Web.HttpContext.Current.Response.Clear();

                    // Redirect  
                    System.Web.HttpContext.Current.Response.Status = "301 Moved Permanently";

                    // Rewrite the Header URL
                    System.Web.HttpContext.Current.Response.AddHeader("Location", rewriteUrl.ToString());

                    realUrlPath = string.Empty;
                }
            }

            return realUrlPath;
        }

        /// <summary>
        /// Formats a product/category/content page internal URL link. 
        /// </summary>
        /// <param name="id">The ProductID, CategoryID or PageID.</param>
        /// <param name="type">Prouct, Category or Page.</param>
        /// <param name="seoUrl">An SEO URL to format. Set to an empty string if no SEO URL is available.</param>
        /// <param name="categoryName">Categoty name.</param>
        /// <returns>The complete URL with the proper parameters and extensions.</returns>
        public static string MakeUrlForMvc(string id, SEOUrlType type, string seoUrl, string categoryName = "")
        {
            var url = new StringBuilder();

            if (!String.IsNullOrEmpty(seoUrl))
            {
                //PRFT Commented code: start
                //if (Equals(type, SEOUrlType.Product)) { url.Append(String.Format("~/{0}", seoUrl)); }
                //if (Equals(type, SEOUrlType.Category)) { url.Append(String.Format("~/{0}", seoUrl)); }
                //PRFT Commented code: end

                //PRFT Custom Code : Start
                if (Equals(type, SEOUrlType.Product)) { url.Append(String.Format("~/{0}", seoUrl.ToLower())); }
                if (Equals(type, SEOUrlType.Category)) { url.Append(String.Format("~/{0}", seoUrl.ToLower())); }
                //PRFT Custom Code : End

                if (Equals(type, SEOUrlType.ContentPage))
                {
                    var contentService = new ContentPageService();
                    var content = contentService.GetByNamePortalIDLocaleId(id, ZNodeConfigManager.SiteConfig.PortalID, LocaleID);
                    if (!Equals(content, null))
                    {
                        //url.Append(String.Format("~/content/{0}", content.Name));
                        url.Append(String.Format("~/contentpage/{0}", content.Name.ToLower()));
                    }
                }
            }
            else
            {
                switch (type)
                {
                    case SEOUrlType.Product:
                        url.Append(String.Format("~/product/{0}", id));
                        break;
                    case SEOUrlType.Category:
                        url.Append(String.Format("~/category/{0}/{1}", categoryName, id));
                        break;
                    case SEOUrlType.ContentPage:
                        ZNode.Libraries.DataAccess.Service.ContentPageService objService = new ZNode.Libraries.DataAccess.Service.ContentPageService();
                        ContentPage contentPage = objService.GetByNamePortalIDLocaleId(id, ZNodeConfigManager.SiteConfig.PortalID, LocaleID);
                        if (contentPage != null)
                        {
                            url.Append(String.Format("~/content/{0}", contentPage.ContentPageID));
                        }
                        break;
                }
            }

            return url.ToString();
        }

        /// <summary>
        /// Formats a product/category/content page internal URL link. 
        /// </summary>
        /// <param name="ID">The ProductID, CategoryID or PageID.</param>
        /// <param name="type">Prouct, Category or Page.</param>
        /// <param name="seoURL">An SEO URL to format. Set to an empty string if no SEO URL is available.</param>
        /// <returns>The complete URL with the proper parameters and extensions.</returns>
        public new static string MakeURL(string id, SEOUrlType type, string seoURL)
        {
            StringBuilder mapUrl = new StringBuilder();

            if (!string.IsNullOrEmpty(seoURL))
            {
                switch (type)
                {
                    case SEOUrlType.Product:
                        mapUrl.Append(string.Format("~/{0}.aspx", seoURL));
                        break;
                    case SEOUrlType.Category:
                        mapUrl.Append(string.Format("~/{0}.aspx", seoURL));
                        break;
                    case SEOUrlType.ContentPage:
                        ZNode.Libraries.DataAccess.Service.ContentPageService objService = new ZNode.Libraries.DataAccess.Service.ContentPageService();
                        ContentPage obj = objService.GetByNamePortalIDLocaleId(id, ZNodeConfigManager.SiteConfig.PortalID, LocaleID);
                        if (obj != null)
                        {
                            mapUrl.Append(string.Format("~/{0}.aspx", obj.Name));
                        }

                        break;
                }
            }
            else
            {
                switch (type)
                {
                    case SEOUrlType.Product:
                        mapUrl.Append("~/product.aspx?zpid=" + id);
                        break;
                    case SEOUrlType.Category:
                        mapUrl.Append("~/category.aspx?zcid=" + id);
                        break;
                    case SEOUrlType.ContentPage:
                        ZNode.Libraries.DataAccess.Service.ContentPageService objService = new ZNode.Libraries.DataAccess.Service.ContentPageService();
                        ContentPage obj = objService.GetByNamePortalIDLocaleId(id, ZNodeConfigManager.SiteConfig.PortalID, LocaleID);
                        if (obj != null)
                        {
                            mapUrl.Append("~/content.aspx?zpgid=" + obj.ContentPageID);
                        }

                        break;
                }
            }

            return mapUrl.ToString();
        }

        public static string MakeURLforMVC(string id, SEOUrlType type, string seoURL)
        {
            var mapUrl = new StringBuilder();

            if (!string.IsNullOrEmpty(seoURL))
            {
                switch (type)
                {
                    case SEOUrlType.Product:
                        mapUrl.Append(string.Format("~/product/{0}/{1}", seoURL, id));
                        break;
                    case SEOUrlType.Category:
                        mapUrl.Append(string.Format("~/category/{0}/{1}", seoURL, id));
                        break;
                    case SEOUrlType.ContentPage:
                        ZNode.Libraries.DataAccess.Service.ContentPageService objService = new ZNode.Libraries.DataAccess.Service.ContentPageService();
                        ContentPage obj = objService.GetByNamePortalIDLocaleId(id, ZNodeConfigManager.SiteConfig.PortalID, LocaleID);
                        if (obj != null)
                        {
                            mapUrl.Append(string.Format("~/{0}.aspx", obj.Name));
                        }

                        break;
                }
            }
            else
            {
                switch (type)
                {
                    case SEOUrlType.Product:
                        mapUrl.Append("~/product/" + id);
                        break;
                    case SEOUrlType.Category:
                        mapUrl.Append("~/category/" + id);
                        break;
                    case SEOUrlType.ContentPage:
                        ZNode.Libraries.DataAccess.Service.ContentPageService objService = new ZNode.Libraries.DataAccess.Service.ContentPageService();
                        ContentPage obj = objService.GetByNamePortalIDLocaleId(id, ZNodeConfigManager.SiteConfig.PortalID, LocaleID);
                        if (obj != null)
                        {
                            mapUrl.Append("~/content.aspx?zpgid=" + obj.ContentPageID);
                        }

                        break;
                }
            }

            return mapUrl.ToString();
        }
        #endregion

        #region Private Methods

        private RedirectRecord GetSEORedirectRecord(string seoURL)
        {
            string formattedSEOURL = seoURL.Replace(".aspx", string.Empty);
            formattedSEOURL = formattedSEOURL.Replace("~/", string.Empty);
            RedirectRecord redirect = new RedirectRecord();
            ZNode.Libraries.DataAccess.Service.ProductService ps = new ZNode.Libraries.DataAccess.Service.ProductService();
            ZNode.Libraries.DataAccess.Service.CategoryService cs = new ZNode.Libraries.DataAccess.Service.CategoryService();
            ZNode.Libraries.DataAccess.Service.ContentPageService cps = new ZNode.Libraries.DataAccess.Service.ContentPageService();
            ZNode.Libraries.DataAccess.Service.UrlRedirectService urls = new ZNode.Libraries.DataAccess.Service.UrlRedirectService();
            TList<Product> pl = ps.GetBySEOURL(formattedSEOURL);
            if (pl.Count > 0)
            {
                redirect.IsRedirect = false;
                redirect.MappedURL = string.Format("~/product.aspx?zpid={0}", pl[0].ProductID);
                return redirect;
            }

            TList<Category> cl = cs.GetBySEOURL(formattedSEOURL);
            if (cl.Count > 0)
            {
                redirect.IsRedirect = false;
                redirect.MappedURL = string.Format("~/category.aspx?zcid={0}", cl[0].CategoryID);
                return redirect;
            }

            TList<ContentPage> cpl = cps.GetBySEOURL(formattedSEOURL);

            if (cpl.Count > 0)
            {
                redirect.IsRedirect = false;
                redirect.MappedURL = string.Format("~/content.aspx?page={0}", cpl[0].Name);
                return redirect;
            }

            UrlRedirectQuery filters = new UrlRedirectQuery();
            filters.AppendEquals(UrlRedirectColumn.OldUrl, seoURL);
            filters.AppendEquals(UrlRedirectColumn.IsActive, "true");

            ZNode.Libraries.DataAccess.Service.UrlRedirectService urlRedirectService = new ZNode.Libraries.DataAccess.Service.UrlRedirectService();
            TList<UrlRedirect> urlList = urlRedirectService.Find(filters.GetParameters());

            if (urlList.Count > 0)
            {
                redirect.IsRedirect = true;
                redirect.MappedURL = urlList[0].NewUrl;
                return redirect;
            }

            return null;
        }

        /// <summary>
        /// Returns actual product/category/content page url for this SEO Url
        /// </summary>
        /// <param name="SeoUrl">The friendly URL to translate into a real URL.</param>
        /// <param name="IsRedirect">Set to true if this should be a 301 redirect instead of a rewrite of the URL.</param>
        /// <returns>The real URL or the SEOUrl. If a real URL can't be found and empty string is returned.</returns>
        private new string SeoUrlToReal(string seoUrl, out bool isRedirect)
        {
            // Check Cache for SEO URL
            if (HttpRuntime.Cache[seoUrl] != null)
            {
                RedirectRecord redirect = (RedirectRecord)HttpRuntime.Cache.Get(seoUrl);

                isRedirect = redirect.IsRedirect;

                return redirect.MappedURL;
            }
            else
            {
                RedirectRecord redirect = this.GetSEORedirectRecord(seoUrl);
                if (redirect != null)
                {
                    ZNodeCacheDependencyManager.Insert(seoUrl, redirect, System.Web.Caching.Cache.NoAbsoluteExpiration, TimeSpan.FromMinutes(10),
                                                    "ZNodeProduct", "ZNodeCategory", "ZNodeContentPage", "ZNodeUrlRedirect");

                    isRedirect = redirect.IsRedirect;
                    return redirect.MappedURL;
                }
                else
                {

                    // Create an empty redirect object so we don't constantly check the database for a nonexistant URL.
                    RedirectRecord empty = new RedirectRecord();
                    empty.MappedURL = string.Empty;
                    empty.IsRedirect = false;
                    ZNodeCacheDependencyManager.Insert(seoUrl, empty, System.Web.Caching.Cache.NoAbsoluteExpiration, TimeSpan.FromMinutes(10),
                                                   "ZNodeProduct", "ZNodeCategory", "ZNodeContentPage", "ZNodeUrlRedirect");
                }

                isRedirect = false;
                return string.Empty;
            }
        }

        #endregion
    }
}
