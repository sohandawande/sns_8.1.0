﻿
#region Using Directives
using System;
using System.ComponentModel;
using System.Collections;
using System.Xml.Serialization;
using System.Data;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Entities.Validation;

using ZNode.Libraries.DataAccess.Data;
using Microsoft.Practices.EnterpriseLibrary.Logging;

#endregion

namespace ZNode.Libraries.DataAccess.Service
{		
	/// <summary>
	/// An component type implementation of the 'ZNodeUrlRedirect' table.
	/// </summary>
	/// <remarks>
	/// All custom implementations should be done here.
	/// </remarks>
	[CLSCompliant(true)]
	public partial class UrlRedirectService : ZNode.Libraries.DataAccess.Service.UrlRedirectServiceBase
	{
		#region Constructors
		/// <summary>
		/// Initializes a new instance of the UrlRedirectService class.
		/// </summary>
		public UrlRedirectService() : base()
		{
		}
		#endregion Constructors
		
	}//End Class

} // end namespace
