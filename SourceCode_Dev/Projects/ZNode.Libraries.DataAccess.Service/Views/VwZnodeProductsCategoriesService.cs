﻿
#region Using Directives
using System;
using System.ComponentModel;
using System.Collections;
using System.Xml.Serialization;
using System.Data;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Entities.Validation;

using ZNode.Libraries.DataAccess.Data;
using Microsoft.Practices.EnterpriseLibrary.Logging;

#endregion

namespace ZNode.Libraries.DataAccess.Service
{		
	
	///<summary>
	/// An component type implementation of the 'vw_ZnodeProductsCategories' table.
	///</summary>
	/// <remarks>
	/// All custom implementations should be done here.
	/// </remarks>
	[CLSCompliant(true)]
	public partial class VwZnodeProductsCategoriesService : ZNode.Libraries.DataAccess.Service.VwZnodeProductsCategoriesServiceBase
	{
		/// <summary>
		/// Initializes a new instance of the VwZnodeProductsCategoriesService class.
		/// </summary>
		public VwZnodeProductsCategoriesService() : base()
		{
		}
		
	}//End Class


} // end namespace
