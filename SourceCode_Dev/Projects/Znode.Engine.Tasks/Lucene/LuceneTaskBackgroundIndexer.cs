﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading;
using Lucene.Net.Index;
using log4net;
using log4net.Config;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Search.LuceneSearchProvider.Indexer.Model;
using ZNode.Libraries.Search.LuceneSearchProvider.Indexer.Services;
using ZNode.Libraries.Search.LuceneSearchProvider.Search;
using Znode.Engine.Tasks.Lucene.Configuration;
using Znode.Engine.Tasks.Lucene.Data;
using Znode.Framework.BackgroundTask;

namespace Znode.Engine.Tasks.Lucene
{
	public class LuceneTaskBackgroundIndexer : BackgroundTaskBase
	{
		private LuceneTaskConfiguration _config;
		private readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		public LuceneTaskBackgroundIndexer()
		{
			// Gotta get config first
			_config = (LuceneTaskConfiguration)ConfigurationManager.GetSection("Znode.Engine.Tasks.Lucene");
			if (_config == null)
			{
				throw new ConfigurationErrorsException("Configuration section for Znode.Engine.Tasks.Lucene does not exist.");
			}

			XmlConfigurator.Configure();
		}

		public override void Initialize()
		{
			Thread.Sleep(_config.OnStartWaitTime);
			this.ErrorLogged += LuceneIndexerBackgroundTask_ErrorLogged;

			if (!Directory.Exists(_config.LogFolder))
			{
				Directory.CreateDirectory(_config.LogFolder);
			}

			var results = LuceneTaskSqlProvider.UpsertZNodeLuceneServerConfigurationStatus(_config.ServerName, Environment.MachineName, 1);
			if (results == 1)
			{
				_log.Info("Service is started for server name '" + _config.ServerName + "'. Server configuration table updated with server name '" + Environment.MachineName + "'.");
			}
			else
			{
				_log.Info("Service is started for server name '" + _config.ServerName + "'. Server configuration table update processed failed for the server name '" + Environment.MachineName + "'.");
			}
		}

		private void LuceneIndexerBackgroundTask_ErrorLogged(object sender, LogMessageEventArgs e)
		{
			_log.Error("LuceneIndexerBackgroundTask_ErrorLogged: " + e.Message);
		}

		protected override bool DoChunkOfWork()
		{
			_log.Info("DoChunkOfWork has been called: ");
			long luceneIndexMonitorId = 1;

			try
			{
				// Validate status for the service for the server name provided in config.
				// If configuration table status is false, service will not execute any logic to process monitor table data.

				var serviceStatus = ServiceStatusCheck();

				if (serviceStatus)
				{
					ILuceneProductService productService = new LuceneProductService();
					ILuceneCategoryService categoryService = new LuceneCategoryService();

					var dataset = LuceneTaskSqlProvider.SelectAllZnodeLuceneIndexMonitor(_config.ServerName);
					var dataRows = dataset.Tables[0].Rows;

					var luceneindexer = new ZNode.Libraries.Search.LuceneSearchProvider.Indexer.LuceneSearchIndexer(
						new LuceneProductService(),
						new LuceneDocMappingService(),
						new LuceneCategoryService(),
						_config.LuceneIndexLocation, 1000);

					// Creating writer to process add, update, or delete documents
					if (dataRows.Count > 0)
					{
						if (dataRows[0]["SourceType"].ToString().ToUpper() == "CREATEINDEX")
						{
							IndexWriter createIndexWriter = null;
							try
							{
								luceneIndexMonitorId = (long)dataRows[0]["LuceneIndexMonitorID"];
								LuceneTaskSqlProvider.UpsertZnodeLuceneIndexServerStatus(_config.ServerName, luceneIndexMonitorId, (int)LuceneTaskProcessedStatus.InProcess);

								createIndexWriter = LuceneSearchManager.GetIndexWriter(_config.LuceneIndexLocation, true);
								luceneindexer.CreateIndex(createIndexWriter);

								LuceneTaskSqlProvider.UpsertZnodeLuceneIndexServerStatus(_config.ServerName, luceneIndexMonitorId, (int)LuceneTaskProcessedStatus.Completed);
								_log.Info("LuceneIndexMonitorID Processed: " + luceneIndexMonitorId + " with Complete Status: " + LuceneTaskProcessedStatus.Completed);
							}
							catch (Exception ex)
							{
								_log.Error("DoChunkOfWork : " + ex.Message + " stack Trace" + ex.StackTrace);
								_log.Warn("LuceneIndexMonitorID Processed: " + luceneIndexMonitorId + " with Complete Status: " + LuceneTaskProcessedStatus.Failed);
								LuceneTaskSqlProvider.UpsertZnodeLuceneIndexServerStatus(_config.ServerName, luceneIndexMonitorId, (int)LuceneTaskProcessedStatus.Failed);
							}
							finally
							{
								if (createIndexWriter != null)
								{
									createIndexWriter.Optimize();
									createIndexWriter.Dispose();
								}

								// Reopen the index file once update to index file is completed
								LuceneSearchManager.ReopenIndexReader();
							}
						}
						else
						{
							var writer = LuceneSearchManager.GetIndexWriter(_config.LuceneIndexLocation, false);

							foreach (DataRow dr in dataRows)
							{
								try
								{
									// CompleteStatus = true;
									var productIdList = new List<int>();
									var categoryIdList = new List<int>();
									var deleteCategoriyIdList = new List<int>();

									var sourceType = dr["SourceType"].ToString().ToUpper();
									var sourceId = (int)dr["SourceID"];
									var sourceTransactionType = dr["SourceTransactionType"].ToString().ToUpper();
									var sourceAffectType = dr["AffectedType"].ToString().ToUpper();

									luceneIndexMonitorId = (long)dr["LuceneIndexMonitorID"];
									LuceneTaskSqlProvider.UpsertZnodeLuceneIndexServerStatus(_config.ServerName, luceneIndexMonitorId, (int)LuceneTaskProcessedStatus.InProcess);

									switch (sourceType)
									{
										case "PRODUCT":
											productIdList.Add(sourceId);
											break;
										case "CATEGORY":
											productIdList = LuceneTaskSqlProvider.GetProductIDsByCategoryID(sourceId);
											categoryIdList.Add(sourceId);

											if (sourceAffectType == "CATEGORYNODE" && sourceTransactionType == "DELETE")
											{
												luceneindexer.DeleteCategoryFacetDocuments(new List<LuceneCategoryFacet> { new LuceneCategoryFacet { CategoryId = sourceId } }, writer);
												deleteCategoriyIdList.Add(sourceId);
											}
											break;
										case "TAG":
											productIdList = LuceneTaskSqlProvider.GetProductIDsByTagID(sourceId);
											break;
										case "TAGGROUP":
											productIdList = LuceneTaskSqlProvider.GetProductIDsByTagGroupID(sourceId);
											categoryIdList = LuceneTaskSqlProvider.GertCategoryIDsByTagGroupID(sourceId);
											break;
										case "MANUFACTURER":
											productIdList = LuceneTaskSqlProvider.GetProductIDsByManufacturerID(sourceId);
											break;
										case "SUPPLIER":
											productIdList = LuceneTaskSqlProvider.GetProductIDsBySupplierID(sourceId);
											break;
										case "CATEGORYFACET":
											categoryIdList.Add(sourceId);
											break;
									}

									// Product document update, insert, or delete
									foreach (var productId in productIdList)
									{
										if (productIdList.Count > 0)
										{
											LuceneProduct luceneProduct;
											switch (sourceTransactionType)
											{
												case "UPDATE":
													luceneProduct = productService.GetProductByProductId(productId);

													if (luceneProduct != null)
													{
														var updateProducts = new List<LuceneProduct> { luceneProduct };
														if (luceneProduct.IsActive && luceneProduct.ReviewStateID == 20)
															luceneindexer.UpdateProductDocuments(updateProducts, writer);
														else
															luceneindexer.DeleteProductDocuments(updateProducts, writer);
													}
													break;
												case "INSERT":
													luceneProduct = productService.GetProductByProductId(productId);

													if (luceneProduct != null)
													{
														var insertProducts = new List<LuceneProduct> { luceneProduct };
														luceneindexer.AddProductDocuments(insertProducts, writer);
													}
													break;
												case "DELETE":
													luceneProduct = new LuceneProduct { ProductId = productId };
													var deleteProducts = new List<LuceneProduct> { luceneProduct };
													luceneindexer.DeleteProductDocuments(deleteProducts, writer);
													break;
											}
										}
									}

									// CategoryFacets document always update for the insert, update, or delete trigger
									foreach (var categoryId in categoryIdList.Except(deleteCategoriyIdList))
									{
										var luceneCategoryFacet = categoryService.GetCategoryFacet(categoryId);
										if (luceneCategoryFacet.CategoryId != 0)
										{
											var luceneCategoryFacets = new List<LuceneCategoryFacet>
                                                {
                                                    luceneCategoryFacet
                                                };
											luceneindexer.UpdateCategoryFacetDocuments(luceneCategoryFacets, writer);
										}
									}

									LuceneTaskSqlProvider.UpsertZnodeLuceneIndexServerStatus(_config.ServerName, luceneIndexMonitorId, (int)LuceneTaskProcessedStatus.Completed);
									_log.Info("LuceneIndexMonitorID Processed: " + luceneIndexMonitorId + " with Complete Status: " + LuceneTaskProcessedStatus.Completed);
								}
								catch (Exception ex)
								{
									_log.Error("DoChunkOfWork : " + ex.Message + " stack Trace" + ex.StackTrace);
									_log.Warn("LuceneIndexMonitorID Processed: " + luceneIndexMonitorId + " with Complete Status: " + LuceneTaskProcessedStatus.Failed);
									LuceneTaskSqlProvider.UpsertZnodeLuceneIndexServerStatus(_config.ServerName, luceneIndexMonitorId, (int)LuceneTaskProcessedStatus.Failed);
								}
							}

							// Disposing writer to commit changes
							writer.Dispose();							
						}

					    if (dataRows.Count > 0)
					    {
                            // WebForms application
                            //ReloadLuceneIndex();
                            // API Aplcaition
                            ReloadApiLuceneIndex();
                        }
					    
					}
				}

				return false; // No more work
			}
			catch (Exception ex)
			{
				_log.Warn("LuceneIndexMonitorID Processed: " + luceneIndexMonitorId + " with Complete Status: " + LuceneTaskProcessedStatus.Failed);
				LuceneTaskSqlProvider.UpsertZnodeLuceneIndexServerStatus(_config.ServerName, luceneIndexMonitorId, (int)LuceneTaskProcessedStatus.Failed);
				_log.Error("DoChunkOfWork : " + ex.Message + " stack Trace" + ex.StackTrace);

				return false;
			}
		}

		private bool ServiceStatusCheck()
		{
			var luceneServerConfigurationStatusService = new LuceneServerConfigurationStatusService();
			var listall = luceneServerConfigurationStatusService.GetAll().Select(c => c).ToList();

			if (listall.Any(luceneIndex => luceneIndex.Status == false && luceneIndex.ServerName == _config.ServerName))
			{
				_log.Info("Service status flag is false for server name " + _config.ServerName);
				return false;
			}

			return true;
		}

		protected override BackgroundTaskConfiguration DefaultConfiguration
		{
			get
			{
				return new BackgroundTaskConfiguration(_config.LuceneIndexInterval);
			}
		}

		private void ReloadLuceneIndex()
		{
			try
			{
				var request = (HttpWebRequest)WebRequest.Create(_config.LuceneIndexReaderUrl + "?Key=" + _config.LuceneIndexReaderKey);

				request.ContentLength = 0;
				request.Timeout = 30000;
				request.AllowAutoRedirect = false;
				request.Method = "POST";

				var response = (HttpWebResponse)request.GetResponse();
			}
			catch (Exception ex)
			{
				_log.Error("Exception occured while calling reload page " + ex.Message + " Stack Trace" + ex.StackTrace);
			}
		}

        private void ReloadApiLuceneIndex()
        {
            try
            {
                Uri url = new Uri(_config.LuceneIndexReaderUrl);
                var request = (HttpWebRequest)WebRequest.Create(_config.LuceneIndexReaderUrl + "?Key=" + _config.LuceneIndexReaderKey);

                request.ContentLength = 0;
                request.Timeout = 30000;
                request.AllowAutoRedirect = false;
                request.Method = "POST";

                request.Headers.Add(GetAuthorizationHeader(url.Host.ToString(), _config.LuceneIndexReaderKey));

                var response = (HttpWebResponse)request.GetResponse();
            }
            catch (Exception ex)
            {
                _log.Error("Exception occured while calling API Reload Index " + ex.Message + " Stack Trace" + ex.StackTrace);
            }
        }

        private string GetAuthorizationHeader(string domainName, string domainKey)
        {
            var value = String.Format("{0}|{1}", domainName, domainKey);
            return "Authorization: Basic " + EncodeBase64(value);
        }

        private string EncodeBase64(string value)
        {
            var valueAsBytes = Encoding.UTF8.GetBytes(value);
            return Convert.ToBase64String(valueAsBytes);
        }
	}
}
