﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Linq;
using ZNode.Libraries.DataAccess.Service;

namespace Znode.Engine.Tasks.Lucene.Data
{
    public class LuceneTaskSqlProvider
    {
        /// <summary>
        /// Runs a SQL command.
        /// </summary>
        /// <param name="sql">The SQL command to run.</param>
        /// <returns>Returns the next record ID.</returns>
        private static int RunSqlCommand(string sql)
        {
            var results = 0;
            using (var sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                var command = new SqlCommand(sql, sqlConn);
                command.Connection.Open();
                results = command.ExecuteNonQuery();
                command.Connection.Close();
            }

            return results;
        }

        /// <summary>
        /// Runs a SQL command.
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="sqlParam"></param>
        /// <param name="isStoredProc"></param>
        /// <returns>Whether the SQL command was successful or not.</returns>
        private static int RunSqlCommand(string sql, Dictionary<string, string> sqlParam, bool isStoredProc = true)
        {
            var results = 0;
            using (var sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                var command = new SqlCommand(sql, sqlConn);
                if (isStoredProc)
                {
                    command.CommandType = CommandType.StoredProcedure;

                    foreach (KeyValuePair<string, string> kvp in sqlParam)
                    {
                        command.Parameters.AddWithValue(kvp.Key, kvp.Value);
                    }
                }
                command.Connection.Open();
                results = command.ExecuteNonQuery();
                command.Connection.Close();
            }

            return results;
        }
        
        /// <summary>
        /// Runs a SQL command.
        /// </summary>
        /// <param name="sql">The SQL command to run.</param>
        /// <param name="sqlParam">The parameters for the stored procedure.</param>
        /// <param name="isStoredProc">Flag for whether or not the SQL parameter is a stored procedure.</param>
        /// <returns>A dataset based on the results of the SQL command.</returns>
        private static DataSet RunSqlCommandForResults(string sql, Dictionary<string, string> sqlParam, bool isStoredProc = true)
        {
            var results = new DataSet();
			using (var sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                var command = new SqlCommand(sql, sqlConn);
                if (isStoredProc)
                {
                    command.CommandType = CommandType.StoredProcedure;

                    foreach (KeyValuePair<string, string> kvp in sqlParam)
                    {
                        command.Parameters.AddWithValue(kvp.Key, kvp.Value);
                    }
                }

                command.Connection.Open();
                var da = new SqlDataAdapter(command);
                da.Fill(results);
                command.Connection.Close();
            }

            return results;
        }

        public static int InsertLuceneServerConfiguration(string serverName, String IPAddress, int flag)
        {
            var sql = "INSERT INTO ZNodeLuceneServerConfigurationStatus (ServerName,IP_Address,Status) VALUES ('" +
                      serverName + "','" + IPAddress + "'," + flag + ")";
            return RunSqlCommand(sql);
            
        }

        public static DataSet SelectAllZnodeLuceneIndexMonitor(string serverName)
        {
            var sqlParam = new Dictionary<string, string> { { "@ServerName", serverName } };
            var sql = "Znode_GetDistinctLuceneIndexMonitor";
            return RunSqlCommandForResults(sql, sqlParam);
        }
        
        public static void UpsertZnodeLuceneIndexServerStatus(string serverName, long luceneIndexMonitorId, int status)
        {
            var sqlParam = new Dictionary<string, string>
                               {
                                   {"ServerName", serverName},
                                   {"LuceneIndexMonitorID", luceneIndexMonitorId.ToString()},
                                   {"Status", status.ToString()}
                               };
            var sql = "Znode_UpsertZnodeLuceneIndexServerStatus";

            RunSqlCommandForResults(sql, sqlParam);
        }

        public static int UpsertZNodeLuceneServerConfigurationStatus(string serverName, string IP_Address, int flag)
        {
            var sqlParam = new Dictionary<string, string> 
                               { 
                                   {"ServerName", serverName}, 
                                   {"IP_Address", IP_Address}, 
                                   {"Status", flag.ToString()} 
                               };
            var sql = "Znode_UpsertZNodeLuceneServerConfigurationStatus";

            return RunSqlCommand(sql, sqlParam,true);
        }

        internal static List<int> GetProductIDsByCategoryID(int categoryID)
        {
            var productCategoryService = new ProductCategoryService();
            var byCategoryId = productCategoryService.GetByCategoryID(categoryID);
            var productIdList = byCategoryId.Select(productCategory => productCategory.ProductID).ToList();
            return productIdList;
        }

        internal static List<int> GetProductIDsByTagID(int tagID)
        {
            var sql = "SELECT ProductID FROM ZNodeTagProductSKU WHERE TagID =" + tagID;
            var dataSet = RunSqlCommandForResults(sql, new Dictionary<string, string>(), false);
            var dataRows = dataSet.Tables[0].Rows;
            var productIdList = (from DataRow dr in dataRows select (int)dr["ProductID"]).ToList();
            return productIdList;
        }

        internal static List<int> GetProductIDsByTagGroupID(int tagGroupID)
        {
            var sql = "SELECT ProductID FROM ZNodeTagProductSKU WHERE TagID IN (SELECT TagID FROM ZNodeTag WHERE TagGroupID =" + tagGroupID + ")";
            var dataSet = RunSqlCommandForResults(sql, new Dictionary<string, string>(), false);
            var dataRows = dataSet.Tables[0].Rows;

            // var productIdList1 = (from DataRow dr in dataRows select (int)dr["ProductID"]).Where().ToList();
            return (from DataRow dr in dataRows where !dr["ProductID"].ToString().Equals(string.Empty) select (int) dr["ProductID"]).ToList();
        }        

        internal static List<int> GetProductIDsByManufacturerID(int manufacturerID)
        {
            var productService = new ProductService();
            var productList = productService.GetByManufacturerID(manufacturerID).Select(product => product.ProductID).ToList();
            return productList;
        }

        internal static List<int> GetProductIDsBySupplierID(int supplierID)
        {
            var productService = new ProductService();
            var productList = productService.GetBySupplierID(supplierID).Select(product => product.ProductID).ToList();
            return productList;
        }

        internal static List<int> GertCategoryIDsByTagGroupID(int tagGroupID)
        {
			var tagGroupCategoryService = new FacetGroupCategoryService();
			var categoryList = tagGroupCategoryService.GetByFacetGroupID(tagGroupID).Select(c => c.CategoryID).ToList();
            return categoryList;
        }
    }
}
