﻿using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.MvcAdmin.ViewModels;
namespace Znode.Engine.MvcAdmin.Agents
{
    /// <summary>
    /// Interface for Attributes
    /// </summary>
    interface IAttributesAgent
    {
        /// <summary>
        ///  To Get all Attributes list
        /// </summary>
        /// <returns></returns>
        AttributesListViewModel GetAttributes();       

        /// <summary>
        /// To Get all Attributes list by attributeTypeId
        /// </summary>
        /// <param name="attributeTypeId">int attributeTypeId</param>
        /// <param name="filters">FilterCollection filters</param>
        /// <param name="sortCollection">SortCollection sortCollection</param>
        /// <param name="pageIndex">Nullable int pageIndex</param>
        /// <param name="recordPerPage">Nullable int recordPerPage</param
        /// <returns>Returns list of Attributes</returns>
        AttributesListViewModel GetAttributesByAttributeTypeId(int attributeTypeId, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);
        
        /// <summary>
        /// To Get  Attribute  by attributeId
        /// </summary>
        /// <param name="attributeId"></param>
        /// <returns></returns>
        AttributesViewModel GetAttributesByAttributeId(int attributeId);
        /// <summary>
        /// To save Attributes 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        bool SaveAttributes(AttributesViewModel model);

        /// <summary>
        /// To update Attributes 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        bool UpdateAttributes(AttributesViewModel model);

        /// <summary>
        /// To delete Attributes by attributeId
        /// </summary>
        /// <param name="attributeId">Attribute id </param>
        /// <param name="errorMessage">Error message while deleting any attribute value</param>
        /// <returns>true / false</returns>
        bool DeleteAttributes(int attributeId, out string errorMessage);
    }
}
