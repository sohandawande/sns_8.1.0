﻿using System.Collections.Generic;
using System.Data;
using System.Web.Mvc;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents.IAgents
{
    public interface IRMARequestAgent
    {
        /// <summary>
        /// Gets list of RMA Requests.
        /// </summary>
        /// <param name="filters">Filters for list of RMA request.</param>
        /// <param name="sortCollection">Sorting of list of RMA requests.</param>
        /// <param name="pageIndex">Page index of RMA request list.</param>
        /// <param name="recordPerPage">Count of record per page of list of RMA requests.</param>
        /// <returns>RMARequestListViewModel</returns>
        RMARequestListViewModel GetRMARequests(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// Gets list of Request status.
        /// </summary>
        /// <returns>List of Request status list view model</returns>
        List<RequestStatusViewModel> GetRequstStatusList();

        /// <summary>
        /// Get list of portal view models.
        /// </summary>
        /// <returns>List of all portals.</returns>
        List<PortalViewModel> GetAllPortals();

        /// <summary>
        /// Gets list of RMA Request list Items.
        /// </summary>
        /// <param name="orderId">Order Id.</param>
        /// <param name="rmaId">RMA Id.</param>
        /// <param name="flag">Flag.</param>
        /// <param name="pageIndex">Page index of RMA Request item list.</param>
        /// <param name="recordPerPage">Record per page count of RMA request item.</param>
        /// <param name="mode">Mode of RMA Request Items.</param>
        /// <returns>RMARequestItemList view model.</returns>
        RMARequestItemListViewModel GetRMARequestListItem(int orderId, int rmaId, string flag, int? pageIndex = null, int? recordPerPage = null, string mode = "");

        /// <summary>
        /// Makes an RMA request void.
        /// </summary>
        /// <param name="rmaRequestId">RMA Request ID.</param>
        /// <param name="comments">Comments for RMA request.</param>
        /// <returns>True or false.</returns>
        bool UpdateRMARequest(int rmaRequestId, RMARequestViewModel rmaRequest, string mode = "");

        /// <summary>
        /// Gets an RMA request.
        /// </summary>
        /// <param name="rmaRequestId">RMA request ID.</param>
        /// <returns>RMA request view model.</returns>
        RMARequestViewModel GetRMARequest(int rmaRequestId);

        /// <summary>
        /// Gets RMA request item list to issue gift card.
        /// </summary>
        /// <param name="orderLineItems">Comma seperated string for order line item ids.</param>
        /// <returns>RMA request item list.</returns>
        RMARequestItemListViewModel GetRMARequestItemsForGiftCard(string orderLineItems);

        /// <summary>
        /// Deletes an RMA Request.
        /// </summary>
        /// <param name="rmaRequestId">RMA Request ID.</param>
        /// <returns>Returns true or false.</returns>
        bool DeleteRMARequest(int rmaRequestId);

        /// <summary>
        /// Gets RMA request gift card details.
        /// </summary>
        /// <param name="rmaReqeustItems">RMA Request item list model.</param>
        /// <param name="rmaRequestId">RMA Request ID.</param>
        void RMARequestGiftCardDetails(RMARequestItemListViewModel rmaReqeustItems, int rmaRequestId);

        /// <summary>
        /// Creates an RMA Request.
        /// </summary>
        /// <param name="rmaRequestModel">RMA Request Model.</param>
        /// <param name="errorMessage">out string errorMessage</param>
        /// <returns>Returns true or flase.</returns>
        bool CreateRMARequest(CreateRequestViewModel model, out string errorMessage);

        /// <summary>
        /// Generates RMA Request Number.
        /// </summary>
        /// <returns>RMA Request number.</returns>
        string GenerateRequestNumber();

        /// <summary>
        /// To check RMA is Enable or not
        /// </summary>
        /// <param name="OrderId">int OrderId</param>
        /// <param name="OrderStatus">string OrderStatus</param>
        /// <param name="OrderDate">string OrderDate</param>
        /// <returns>returns enable or disable RMA</returns>
        bool IsRMAEnable(int orderId, string orderStatus, string orderDate);

        /// <summary>
        /// To Bind Reason For Return list
        /// </summary>
        /// <returns></returns>
        List<SelectListItem> BindReasonForReturn();

    }
}
