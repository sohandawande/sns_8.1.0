﻿using System.Collections.Generic;
using System.Web.Mvc;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public interface ICategoryAgent
    {
        /// <summary>
        /// Get list of categories
        /// </summary>
        /// <returns></returns>
        CategoryListViewModel GetCategories();        

        /// <summary>
        /// Gets categories by catalog Id.
        /// </summary>
        /// <param name="catalogId">The Id of catalog.</param>
        /// <returns>Returns categories by catalog Id.</returns>
        CatalogAssociatedCategoriesListViewModel GetCategoryByCataLogId(int catalogId, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// Get Categories based on Catalog Id.
        /// </summary>
        /// <param name="catalogId">The id of catalog</param>
        /// <returns>Return Categories.</returns>
        CategoryListViewModel GetCategoryByCatalogId(int catalogId);

        /// <summary>
        ///  Gets all categories.
        /// </summary>
        /// <param name="filters">FilterCollection filters</param>
        /// <param name="sortCollection">SortCollection sortCollection</param>
        /// <param name="pageIndex">Nullable int pageIndex</param>
        /// <param name="recordPerPage">Nullableint recordPerPage</param>
        /// <returns>Returns List of all categories.</returns>
        CatalogAssociatedCategoriesListViewModel GetAllCategories(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// Get Product Category on the basis of productId and CategoryId
        /// </summary>
        /// <param name="productId">int Id of product</param>
        /// <param name="categoryId">int Id of catalog</param>
        /// <returns></returns>
        ProductCategoryViewModel GetProductCategory(int productId, int categoryId);

        /// <summary>
        /// Binds the parent-category nodes.
        /// </summary>
        /// <param name="catalogId">The id of catalog</param>
        ///  <param name="filters">FilterCollection filters</param>
        /// <returns>List of parent-category nodes.</returns>
        List<SelectListItem> BindParentCategoryNodes(int catalogId, FilterCollection filters = null);

        /// <summary>
        /// Binds theme list.
        /// </summary>
        /// <param name="themeId">Id of theme</param>
        /// <returns>List of theme.</returns>
        List<SelectListItem> BindThemeList(int themeId = 0);

        /// <summary>
        /// Binds the css list.
        /// </summary>
        /// <param name="CssThemeId">Id of css theme</param>
        /// <param name="cssId">Id of css</param>
        /// <returns>List of css.</returns>
        List<SelectListItem> BindCssList(int CssThemeId = 0, int cssId = 0);

        /// <summary>
        /// Binds the master page list.
        /// </summary>
        /// <param name="themeId">The id of theme</param>
        /// <param name="pageType">the type of page</param>
        ///  <param name="masterPageId">The id of master page</param>
        /// <returns>List of master page.</returns>
        List<SelectListItem> BindMasterPageList(int themeId = 0, string pageType = "", int masterPageId = 0);

        /// <summary>
        /// Create new category
        /// </summary>
        /// <param name="model">CategoryViewModel</param>
        /// <returns>Returns whether category was created or not in boolean values.</returns>
        bool CreateCategory(CategoryViewModel model, out string errorMessage);

        /// <summary>
        /// Get category detals based on category Id
        /// </summary>
        /// <param name="categoryId">The id of category</param>
        /// <returns>Category Details</returns>
        CategoryViewModel GetCategory(int categoryId);

        /// <summary>
        /// Update category based on category Id
        /// </summary>
        /// <param name="model">Model of type CategoryViewModel</param>
        /// <param name="errorMessage">return error message as out param</param>
        /// <returns>Returns true or false</returns>
        bool UpdateCategory(CategoryViewModel model, out string errorMessage);

        /// <summary>
        /// Delete Category based on category 
        /// </summary>
        /// <param name="model">The id of category</param>
        /// <param name="message">Out message in case of exception.</param>
        /// <returns>Returns true or false</returns>
        bool DeleteCategory(int categoryId,out string message);

        /// <summary>
        /// Associates an already created category to the catalog.
        /// </summary>
        /// <param name="model">The model of type CategoryNodeViewModel</param>
        /// <returns>Returns true or false.</returns>
        bool AssociateCategory(CategoryNodeViewModel model);

        /// <summary>
        /// Binds the list of profiles.
        /// </summary>
        /// <param name="profileId">The id of profile</param>
        /// <returns>Returns list of type SelectListItem.</returns>
        List<SelectListItem> BindProfiles(int profileId = 0);

        /// <summary>
        /// Gets category by category id.
        /// </summary>
        /// <param name="categoryId">The id of categoryId</param>
        /// <returns>Returns model of type CategoryViewModel.</returns>
        CategoryViewModel GetCategoryByCategoryId(int categoryId);

        /// <summary>
        /// Get profile name.
        /// </summary>
        /// <param name="profileId">the id of profile</param>
        /// <returns>Returns profile name.</returns>
        string GetProfileName(int profileId);



        /// <param name="model">CategoryViewModel</param>
        /// <returns></returns>
        List<SelectListItem> GetCatalogList(CatalogAssociatedCategoriesViewModel model);

        /// <summary>
        /// Get all Products based on categoryId
        /// </summary>
        /// <param name="model">categoryId</param>
        /// <returns>Returns ProductListViewModel.</returns>
        ProductListViewModel GetProductsByCategoryId(int categoryId, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// Get Catalog list
        /// </summary>
        /// <returns>Returns List<CatalogModel>.</returns>
        List<CatalogModel> GetcatalogList();

        /// <summary>
        /// Add Products associated with categories
        /// </summary>
        /// <param name="categoryId">int id of category</param>
        /// <param name="productIds">int id of products</param>
        /// <param name="errorMessage">string errorMessage if products are already associated with the category.</param>
        /// <returns>Returns whether the products were associated with category or not.</returns>
        bool CategoryAssociatedProducts(int categoryId, string productIds, out string errorMessage);

        /// <summary>
        /// To update cotegory SEO Details
        /// </summary>
        /// <param name="model">ManageSEOCategoryViewModel model</param>
        /// <returns>Returns error if fails to update else empty.</returns>
        string UpdateCategorySEODetails(ManageSEOCategoryViewModel model);

        /// <summary>
        /// Get the category based on the category name.
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns>Returns the category as category view model</returns>
        CategoryViewModel GetCategoryByName(string categoryName);

        /// <summary>
        ///  Gets list of categories on the basis of store access.
        /// </summary>
        /// <param name="filters">FilterCollection filters</param>
        /// <param name="sortCollection">SortCollection sortCollection</param>
        /// <param name="pageIndex">Nullable int pageIndex</param>
        /// <param name="recordPerPage">Nullableint recordPerPage</param>
        /// <returns>Returns List of categories on the basis of store access.</returns>
        CatalogAssociatedCategoriesListViewModel GetCategoryList(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// Get the categories based on the catalog.
        /// </summary>
        /// <param name="catalogId">int catalogId for which catalog to show category</param>
        /// <param name="categoryName">striong categoryName</param>
        /// <returns>Returns the categories as category list view model</returns>
        CategoryTreeListViewModel GetCategoryTree(int catalogId, string categoryName);

        /// <summary>
        /// To get product by categoryId
        /// </summary>
        /// <param name="categoryId">int categoryId</param>
        /// <returns>Returns the categories as category list view model</returns>
        CategoryTreeListViewModel GetCategoryProducts(int categoryId);

        CategoryTreeListViewModel AssignCategoryProduct(CategoryTreeListViewModel categories, CategoryTreeListViewModel products, int categoryId);
    }
}
