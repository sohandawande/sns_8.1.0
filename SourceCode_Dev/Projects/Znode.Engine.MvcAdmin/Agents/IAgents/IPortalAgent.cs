﻿using System.Collections.Generic;
using System.Web.Mvc;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    /// <summary>
    /// Interface for Portal Agent
    /// </summary>
    public interface IPortalAgent
    {
        /// <summary>
        /// Delete portal on the basis of portal id.
        /// </summary>
        /// <param name="portalId">portalId</param>
        /// <returns>Returns bool</returns>
        bool DeletePortal(int portalId);

        /// <summary>
        /// Gets a list of all portal in sorted order
        /// </summary>
        /// <param name="filters">filters</param>
        /// <param name="sortCollection">sortCollection</param>
        /// <param name="pageIndex">pageIndex</param>
        /// <param name="recordPerPage">recordPerPage</param>
        /// <returns>Returns PortalListViewModel</returns>
        PortalListViewModel GetPortals(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// Gets portal according to portal Id.
        /// </summary>
        /// <param name="portalId">Portal id.</param>
        /// <returns>This method returns information regarding portal from stored procedure.</returns>
        PortalViewModel GetPortalInformationByPortalId(int portalId);

        /// <summary>
        /// Get all portals
        /// </summary>
        /// <returns>Returns list of portals</returns>
        PortalListViewModel GetAllPortals();

        /// <summary>
        /// Binds the portal information.
        /// </summary>
        /// <param name="portalViewModel">portalviewmodel parameter.</param>
        /// <returns></returns>
        PortalViewModel BindPortalInformation(PortalViewModel portalViewModel);

        /// <summary>
        /// Creates aportal.
        /// </summary>
        /// <param name="portalViewModel">Portal View Model</param>
        /// <param name="notificationMessage">Notification message.</param>
        /// <param name="portalId">Out parameter created portal id.</param>
        /// <returns>True or false.</returns>
        bool CreatePortal(PortalViewModel portalViewModel, out string notificationMessage);

        /// <summary>
        /// Copy stores.
        /// </summary>
        /// <param name="portalId">Portal id of the store to be copied.</param>
        /// <returns>Bool Value if the store is copied or not.</returns>
        bool CopyStore(int portalId);

        /// <summary>
        /// Binds Css list of portal.
        /// </summary>
        /// <param name="cssThemeId">CSS Theme ID.</param>
        /// <returns>List of CSS Select List Items.</returns>
        List<SelectListItem> BindCssList(int? cssThemeId = null);

        /// <summary>
        /// Gets Currency Information
        /// </summary>
        /// <param name="currencyId">Currency id.</param>
        /// <returns>Object</returns>
        object GetCurrencyInformationByCurrencyId(int currencyId);

        /// <summary>
        /// Updates Manage SEO Default Settings.
        /// </summary>
        /// <param name="viewModel">ManageSEOViewModel viewModel</param>
        /// <returns>returns true/false</returns>
        bool UpdatePortalDefaultSettings(ManageSEOViewModel viewModel);

        /// <summary>
        /// Updates the portal according to tabmode.
        /// </summary>
        /// <param name="portalViewModel">Portal model to be updated.</param>
        /// <param name="tabMode">Tabmode according to which portal will be updated.</param>
        /// <returns>Returns updated portal model.</returns>
        PortalViewModel UpdatePortal(PortalViewModel portalViewModel, int tabMode);

        /// <summary>
        /// Returns portalsdropdown view model
        /// </summary>
        /// <returns></returns>
        PortalSelectListViewModel GetPortalSelectList();

        /// <summary>
        /// Gets the Currency Type.
        /// </summary>
        /// <param name="currencyTypeId">Currency Type ID.</param>
        /// <returns>Currency Type Model.</returns>
        CurrencyTypeModel GetCurrencyType(int currencyTypeId);

        /// <summary>
        /// Gets portal by the specified portalid.
        /// </summary>
        /// <param name="portalId">Portal Id by which portal will be retrieved.</param>
        /// <returns>Portal according to the portal ID.</returns>
        PortalViewModel GetPortalByPortalId(int portalId);
    }
}
