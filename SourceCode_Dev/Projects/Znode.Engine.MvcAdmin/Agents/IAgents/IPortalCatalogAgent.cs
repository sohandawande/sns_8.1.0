﻿using System.Collections.Generic;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public interface IPortalCatalogAgent
    {
        /// <summary>
        /// Gets portal pacatlog list.
        /// </summary>
        /// <param name="portalId">Portal id.</param>
        /// <param name="filters">Filters.</param>
        /// <param name="sortCollection">Sort collection</param>
        /// <param name="pageIndex">PageIndex</param>
        /// <param name="recordPerPage">Recordperpage.</param>
        /// <returns>Poral catalog listmessage</returns>
        PortalCatalogListViewModel GetPortalCatalogList(int portalId, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// Gets a specific portal catalog.
        /// </summary>
        /// <param name="portalCatalogId">Portal catalog ID according to which portal catalog will be retrieved.</param>
        /// <returns>Portal Catalog view model.</returns>
        PortalCatalogViewModel GetPortalCatalog(int portalCatalogId);

        /// <summary>
        /// Binds portal catalog information.
        /// </summary>
        /// <param name="portalCatalogViewModel">Portal catalog view model.</param>
        void BindPortalCatalogInformation(PortalCatalogViewModel portalCatalogViewModel);

        /// <summary>
        /// Updates the portal catalog.
        /// </summary>
        /// <param name="portalcatalogViewModel">Takes Portal catalog view model from view.</param>
        /// <returns>Bool value whethe update is successfull.</returns>
        bool UpdatePortalCatalog(PortalCatalogViewModel portalcatalogViewModel);

        /// <summary>
        /// Gets PortalCatalogViewModel with css and theme drop down list.
        /// </summary>
        /// <param name="portalCatalogId">Protal CatalogId.</param>
        /// <returns>Portal Catalog view Model.</returns>
        PortalCatalogViewModel GetPortalCatalogWithDropDowns(int portalCatalogId);

        /// <summary>
        /// Get the catalog list by user name.
        /// </summary>
        /// <returns>list of portal catalog view model.</returns>
        List<PortalCatalogViewModel> GetCatalogListByUserName();
    }
}
