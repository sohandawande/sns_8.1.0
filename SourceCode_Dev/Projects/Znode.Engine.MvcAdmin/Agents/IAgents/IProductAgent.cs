﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;


namespace Znode.Engine.MvcAdmin.Agents
{
    /// <summary>
    ///  Interface for the Products
    /// </summary>
    public interface IProductAgent
    {

        #region Product Details
        /// <summary>
        /// To get Product list
        /// </summary>
        /// <returns></returns>
        ProductListViewModel GetProducts(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// To get Product details by productId
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        ProductViewModel GetProduct(int productId);

        /// <summary>
        /// To check Sku Exist
        /// </summary>
        /// <param name="Sku"></param>
        /// <param name="productId"></param>
        /// <returns></returns>
        bool IsSkuExist(string Sku, int productId);

        /// <summary>
        ///  To check SeoUrl Exist
        /// </summary>
        /// <param name="SeoUrl">string SeoUrl</param>
        /// <param name="productId">int productId</param>
        /// <returns>true/false</returns>
        bool IsSeoUrlExist(int productId, string seoUrl);

        /// <summary>
        /// To save product details
        /// </summary>
        /// <param name="model">ProductViewModel</param>
        /// <param name="errorMessage">out errorMessage</param>
        /// <returns>returns true/false</returns>
        bool SaveProduct(ProductViewModel model, out string errorMessage);

        /// <summary>
        /// To update product details
        /// </summary>
        /// <param name="model">ProductViewModel</param>
        /// <returns>true/false</returns>
        bool UpdateProduct(ProductViewModel model, out string errorMessage);

        /// <summary>
        ///  To delete product details by productId
        /// </summary>
        /// <param name="productId">int productId</param>
        /// <param name="errorMessage">Out parameter error message.</param>
        /// <returns>true/false</returns>
        bool DeleteProduct(int productId, out string errorMessage);

        /// <summary>
        /// To update product setting details
        /// </summary>
        /// <param name="model">ProductViewModel</param>
        /// <returns>true/false</returns>
        bool UpdateProductSetting(ProductViewModel model);

        /// <summary>
        /// To bind all dropdown of product page
        /// </summary>
        /// <param name="model">ProductViewModel model</param>
        /// <param name="portalId">Id of the Portal</param>
        /// <param name="isFranchisable">Flag for Franchisable</param>
        /// <returns>Return the Product Details in ProductViewModel format</returns>
        ProductViewModel BindProductPageDropdown(ProductViewModel model, int portalId, bool isFranchisable = false);

        /// <summary>
        /// To get product attributes by productTypeId
        /// </summary>
        /// <param name="productTypeId">nullable int productTypeId</param>
        /// <returns>returns list of AttributeTypeValueViewModel</returns>
        List<AttributeTypeValueViewModel> GetProductAttributesByProductTypeId(int? productTypeId);

        /// <summary>
        /// To get list of duration
        /// </summary>
        /// <returns></returns>
        List<SelectListItem> GetDurationList();

        #endregion

        #region Product Tags

        /// <summary>
        /// To Get the Product tags.
        /// </summary>
        /// <param name="productId">int productId</param>
        /// <returns>Return product tags.</returns>
        ProductTagsViewModel GetProductTags(int productId);

        /// <summary>
        /// Inserts the Product Tags.
        /// </summary>
        /// <param name="model">ProductTagsViewModel model</param>
        /// <returns>Return true or false</returns>
        bool InsertProductTags(ProductTagsViewModel model);

        /// <summary>
        /// Updates the Product Tags.
        /// </summary>
        /// <param name="model">ProductTagsViewModel model</param>
        /// <returns>Return true or false</returns>
        bool UpdateProductTags(ProductTagsViewModel model);

        /// <summary>
        /// Deletes the Product Tags.
        /// </summary>
        /// <param name="tagId">int tagId</param>
        /// <returns>Return true or false</returns>
        bool DeleteProductTags(int tagId);
        #endregion

        #region Product Facet

        /// <summary>
        /// To Get the associated facets based on product & facetGroupId id.
        /// </summary>
        /// <param name="productId">int productId</param>
        /// <param name="facetGroupId">int facetGroupId = 0</param>
        /// <returns>Return the associated facets.</returns>
        ProductFacetsViewModel GetAssociatedFacetGroup(int productId, int facetGroupId = 0);

        /// <summary>
        /// To Get Associated Product Facets.
        /// </summary>
        /// <param name="productId">int productId</param>
        /// <param name="filters">FilterCollection filters = null</param>
        /// <param name="sortCollection">SortCollection sortCollection = null</param>
        /// <param name="pageIndex">int? pageIndex = null</param>
        /// <param name="recordPerPage">int? recordPerPage = null</param>
        /// <returns>Return Product Facets.</returns>
        ProductFacetListViewModel GetProductFacets(int productId, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);


        /// <summary>
        /// To Associated the Facets to the product.
        /// </summary>
        /// <param name="model">ProductFacetsViewModel model</param>
        /// <returns>Return true or false</returns>
        bool BindAssociatedFacets(ProductFacetsViewModel model);

        /// <summary>
        /// To Delete the product associated facets based on productid & facetgroupid.
        /// </summary>
        /// <param name="productId">int productId</param>
        /// <param name="facetGroupId">int facetGroupId</param>
        /// <returns>Return true or false</returns>
        bool DeleteProductAssociatedFacets(int productId, int facetGroupId);
        #endregion

        #region Product Images
        /// <summary>
        /// To Get all the Product Image Types.
        /// </summary>
        /// <param name="productImageTypeId">int? productImageTypeId = 0</param>
        /// <returns>Return Product image types.</returns>
        List<SelectListItem> GetProductImageTypes(int? productImageTypeId = 0);

        /// <summary>
        /// To Add Product Alternate Image.
        /// </summary>
        /// <param name="model">ProductAlternateImageViewModel model</param>
        /// <returns>Return true or false</returns>
        bool InsertProductAlternateImage(ProductAlternateImageViewModel model);

        /// <summary>
        /// To Update Product Alternate Image.
        /// </summary>
        /// <param name="model">ProductAlternateImageViewModel model</param>
        /// <returns>Return true or false</returns>
        bool UpdateProductAlternateImage(ProductAlternateImageViewModel model);

        /// <summary>
        /// To Delete the product alternate image.
        /// </summary>
        /// <param name="productImageId">int productImageId</param>
        /// <returns>Return true or false</returns>
        bool DeleteProductAlternateImage(int productImageId);

        /// <summary>
        /// To Get all Product Alternate Images.
        /// </summary>
        /// <param name="productId">int productId</param>
        /// <param name="filters">FilterCollection filters = null</param>
        /// <param name="sortCollection">SortCollection sortCollection = null</param>
        /// <param name="pageIndex">int? pageIndex = null</param>
        /// <param name="recordPerPage">int? recordPerPage = null</param>
        /// <returns>Return list of product alternate images..</returns>
        ProductAlternateImageListViewModel GetProductAlternateImages(int productId, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// To Get the Image details based on product image id.
        /// </summary>
        /// <param name="productImageId">int productImageId</param>
        /// <returns>Return the alternate image model.</returns>
        ProductAlternateImageViewModel GetProductAlternateImage(int productImageId);
        #endregion

        #region Product Bundle
        /// <summary>
        /// To Get all Product Bundles list.
        /// </summary>
        /// <param name="productId">int productId</param>
        /// <param name="filters">FilterCollection filters = null</param>
        /// <param name="sortCollection">SortCollection sortCollection = null</param>
        /// <param name="pageIndex">int? pageIndex = null</param>
        /// <param name="recordPerPage">int? recordPerPage = null</param>
        /// <returns>Return the product bundles list.</returns>
        ProductBundlesListViewModel GetProductBundleList(int productId, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// Binds the Catalog, Product Types drop down for Product Search Criteria.
        /// </summary>
        /// <param name="formModel">FilterCollectionDataModel formModel</param>
        /// <param name="isFranchisable">Flag for Franchisable</param>
        /// <returns>Return Product Search criteria details.</returns>
        ProductBundlesViewModel BindProductSearchDropDown(FilterCollectionDataModel formModel, bool isFranchisable = false);

        /// <summary>
        /// To Delete the product alternate image.
        /// </summary>
        /// <param name="parentChildProductID">int parentChildProductID</param>
        /// <returns>Return true or false</returns>
        bool DeleteBundleProduct(int parentChildProductID);

        /// <summary>
        /// To Get the Product List.
        /// </summary>
        /// <param name="filters">FilterCollection filters = null</param>
        /// <param name="sortCollection">SortCollection sortCollection = null</param>
        /// <param name="pageIndex">int? pageIndex = null</param>
        /// <param name="recordPerPage">int? recordPerPage = null</param>
        /// <returns></returns>
        ProductBundlesViewModel GetProductList(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// To Associate Bundle Product.
        /// </summary>
        /// <param name="viewModel">ProductViewModel</param>
        /// <returns>returns true/false</returns>
        bool AssociateBundleProduct(ProductViewModel viewModel);

        #endregion

        #region Product Category

        /// <summary>
        /// To get product categories by productid
        /// </summary>
        /// <param name="productId">int productId</param>
        /// <param name="portalId">int portalId</param>
        /// <param name="filters">FilterCollection filters</param>
        /// <param name="sortCollection">SortCollection sortCollection</param>
        /// <param name="pageIndex">int? pageIndex</param>
        /// <param name="recordPerPage">int? recordPerPage</param>
        /// <returns>returns list of category assigned to product</returns>
        CategoryListViewModel GetProductCategoriesByProductId(int productId, int portalId, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// To get product unassociated category by productid
        /// </summary>
        /// <param name="productId">int productId</param>
        /// <param name="filters">FilterCollection filters</param>
        /// <param name="sortCollection">SortCollection sortCollection</param>
        /// <param name="pageIndex">int? pageIndex</param>
        /// <param name="recordPerPage">int? recordPerPage</param>
        /// <returns>returns list of category unassigned to product</returns>
        CategoryListViewModel GetProductUnAssociatedCategoryByProductId(int productId, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// To Associate Product Category
        /// </summary>
        /// <param name="viewModel">CategoryViewModel</param>
        /// <returns>true/false</returns>
        bool AssociateProductCategory(CategoryViewModel viewModel);

        /// <summary>
        /// To remove Associated Product Category
        /// </summary>
        /// <param name="viewModel">CategoryViewModel</param>
        /// <returns>returns true/false</returns>
        bool UnAssociateProductCategory(CategoryViewModel viewModel);
        #endregion

        #region Product SKU

        /// <summary>
        /// To get Product SKU list by product id
        /// </summary>
        /// <param name="productId">int productId</param>
        /// <param name="filters">FilterCollection filters</param>
        /// <param name="sortCollection">SortCollection sortCollection </param>
        /// <param name="pageIndex">int? pageIndex</param>
        /// <param name="recordPerPage">int? recordPerPage</param>
        /// <returns>returns sku list</returns>
        SkuListViewModel GetProductSkuByProductId(int productId, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// To get Product SKU by skuId
        /// </summary>
        /// <param name="skuId">int skuId</param>
        /// <returns>returns SkuViewModel</returns>
        SkuViewModel GetProductSkuDetailsBySkuId(int skuId);

        /// <summary>
        /// To get product basic details 
        /// </summary>
        /// <param name="productId">int productId</param>
        /// <returns>returns SkuViewModel model</returns>
        SkuViewModel GetProductSkuInfo(int productId);

        /// <summary>
        /// To bind sku drop down list
        /// </summary>
        /// <param name="model"></param>
        /// <returns>returns update model</returns>
        SkuViewModel BindProductSkuDropdown(SkuViewModel model);

        /// <summary>
        /// To save product sku 
        /// </summary>
        /// <param name="model">SkuViewModel model</param>
        /// <returns>returns error message, if success then empty string</returns>
        string SaveProductSku(SkuViewModel model);

        /// <summary>
        /// To update product sku 
        /// </summary>
        /// <param name="model">SkuViewModel model</param>
        /// <returns>returns error message, if success then empty string</returns>
        string UpdateProductSku(SkuViewModel model);

        /// <summary>
        /// To delete sku
        /// </summary>
        /// <param name="skuId">int skuId</param>
        /// <returns>returns true/false</returns>
        bool DeleteSku(int skuId, out string errorMessage);

        /// <summary>
        /// To get Facets list by sku id
        /// </summary>
        /// <param name="skuId">int skuId</param>
        /// <param name="filters">FilterCollection filters</param>
        /// <param name="sortCollection">SortCollection sortCollection</param>
        /// <param name="pageIndex">int? pageIndex</param>
        /// <param name="recordPerPage">int? recordPerPage</param>
        /// <returns></returns>
        ProductFacetListViewModel GetSKUFacets(int skuId, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// To Associate Sku Facets
        /// </summary>
        /// <param name="skuId">int skuId</param>
        /// <param name="associateFacetIds">string associateFacetIds</param>
        /// <param name="unassociateFacetIds">string unassociateFacetIds</param>
        /// <returns>returns true/false</returns>
        bool AssociateSkuFacets(int skuId, string associateFacetIds, string unassociateFacetIds);

        /// <summary>
        /// To Delete Sku Associated Facets
        /// </summary>
        /// <param name="skuId">int skuId</param>
        /// <param name="facetGroupId">int facetGroupId</param>
        /// <returns>returns true/false</returns>
        bool DeleteSkuAssociatedFacets(int skuId, int facetGroupId);

        /// <summary>
        /// To get Get SKU Associated FacetGroup bt sku & facetGroupId
        /// </summary>
        /// <param name="productId">int productId</param>
        /// <param name="skuId">int skuId</param>
        /// <param name="facetGroupId">int facetGroupId </param>
        /// <returns>returns associated facets</returns>
        ProductFacetsViewModel GetSKUAssociatedFacetGroup(int productId, int skuId, int facetGroupId = 0);
        #endregion

        #region AddOns
        /// <summary>
        /// Returns product addons for specified product id. 
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        AddOnListViewModel GetProductAddOns(int productId, FilterCollection filters = null, SortCollection sorts = null, int pageIndex = 1, int pageSize = 10);

        /// <summary>
        /// Remove mapping between addon and current product. 
        /// </summary>
        /// <param name="productAddOnId"></param>
        /// <returns></returns>
        bool RemoveProductAddOn(int productAddOnId);

        /// <summary>
        /// Associate addon with current product.
        /// </summary>
        /// <param name="listViewModel"></param>
        void AssociateAddOns(AddOnListViewModel listViewModel);

        /// <summary>
        /// Returns product addons for specified product id. 
        /// </summary>
        /// <param name="productId">int productId</param>
        /// <param name="portalId">int portalId</param>
        /// <param name="filters">FilterCollection filters</param>
        /// <param name="sorts">SortCollection sorts</param>
        /// <param name="pageIndex">Nullable int pageIndex</param>
        /// <param name="pageSize">Nullable int pageSize</param>
        /// <returns>Return AddOn List</returns>
        AddOnListViewModel GetUnassociatedAddOns(int productId, int portalId, FilterCollection filters = null, SortCollection sorts = null, int? pageIndex = null, int? pageSize = null);
        #endregion

        #region Highlights

        /// <summary>
        /// Returns product highlights for specified product id.
        /// </summary>
        /// <param name="productId">product Id</param>
        /// <param name="filters">filters</param>
        /// <param name="sorts">sorts</param>
        /// <param name="pageIndex">pageIndex</param>
        /// <param name="recordPerPage">recordPerPage</param>
        /// <returns></returns>
        HighlightsListViewModel GetProductHighlights(int productId, FilterCollection filters = null, SortCollection sorts = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// Remove mapping between highlight and current product. 
        /// </summary>
        /// <param name="productHighlightId"></param>
        /// <returns></returns>
        bool RemoveProductHighlight(int productHighlightId);

        /// <summary>
        /// Associate highlight with current product.
        /// </summary>
        /// <param name="listViewModel">Highlights List View Model</param>
        bool AssociateHighlights(HighlightsListViewModel listViewModel);

        /// <summary>
        /// Return unassociated highlights for specified product id.
        /// </summary>
        /// <param name="productId">product Id</param>
        /// <param name="portalId">portal Id</param>
        /// <param name="filters">filters</param>
        /// <param name="sorts">sorts</param>
        /// <param name="pageIndex">pageIndex</param>
        /// <param name="recordPerPage">recordPerPage</param>
        /// <returns></returns>
        HighlightsListViewModel GetUnassociatedHighlights(int productId, int portalId, FilterCollection filters = null, SortCollection sorts = null, int? pageIndex = null, int? recordPerPage = null);

        #endregion

        #region DropDown for Product Search
        /// <summary>
        /// Bind Product Type List
        /// </summary>
        /// <returns>List of ProductTypeModel</returns>
        List<ProductTypeModel> GetProductType();

        /// <summary>
        /// Bind Manufacturer List
        /// </summary>
        /// <returns>List ManufacturerModel </returns>
        List<ManufacturerModel> GetManufacturer();

        /// <summary>
        /// Bind Catolog List
        /// </summary>
        /// <returns>List CatalogModel</returns>
        List<CatalogModel> Getcatalog();

        /// <summary>
        /// Bind category List.
        /// </summary>
        /// <returns>List CategoryModel</returns>
        List<CategoryModel> GetCategories();
        #endregion

        #region Category Associated Products
        /// <summary>
        /// Get all products
        /// </summary>
        /// <param name="filters"></param>
        /// <param name="sorts"></param>
        /// <param name="pageIndex"></param>
        /// <param name="recordPerPage"></param>
        /// <returns></returns>
        CategoryAssociatedProductsListViewModel GetAllProducts(FilterCollection filters, SortCollection sorts, int? pageIndex, int? recordPerPage);
        #endregion

        #region Product Tier Pricing

        /// <summary>
        /// Return product tiers for specified product id.
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="filters"></param>
        /// <param name="sortCollection"></param>
        /// <param name="pageIndex"></param>
        /// <param name="recordPerPage"></param>
        /// <returns></returns>
        ProductTierListViewModel GetProductTiers(int productId, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// Return product tier for specified id.
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="productTierId"></param>
        /// <returns></returns>
        ProductTierPricingViewModel GetProductTier(int productId, int productTierId);

        /// <summary>
        /// Insert a new product tier.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        ProductTierPricingViewModel CreateProductTier(ProductTierPricingViewModel model);

        /// <summary>
        /// Updates specified product tier.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        ProductTierPricingViewModel UpdateProductTier(ProductTierPricingViewModel model);

        /// <summary>
        /// Deletes specified product tier.
        /// </summary>
        /// <param name="productTierId"></param>
        /// <returns></returns>
        bool DeleteProductTier(int productTierId);

        #endregion

        #region Digital Asset

        /// <summary>
        /// Return digital assets for specified product id.
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="filters"></param>
        /// <param name="sortCollection"></param>
        /// <param name="pageIndex"></param>
        /// <param name="recordPerPage"></param>
        /// <returns></returns>
        DigitalAssetListViewModel GetDigitalAssets(int productId, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// Insert a new digital asset.
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        DigitalAssetViewModel AddDigitalAsset(DigitalAssetViewModel viewModel);

        /// <summary>
        /// Delete digital asset.
        /// </summary>
        /// <param name="digitlAssetId">int digitlAssetId</param>
        /// <param name="errorMessage">out string errorMessage</param>
        /// <returns>Returns True or False</returns>
        bool DeleteDigitlAsset(int digitlAssetId, out string errorMessage);

        #endregion

        #region Sku Profile

        /// <summary>
        /// Gets the list of Sku Profile Effective.
        /// </summary>
        /// <param name="filters">FilterCollection filters</param>
        /// <param name="sortCollection">SortCollection sortCollection</param>
        /// <param name="pageIndex">page Index</param>
        /// <param name="recordPerPage">record PerPage</param>
        /// <returns>SkuProfileEffectiveListViewModel</returns>
        SkuProfileEffectiveListViewModel GetSkuProfileEffectivesBySkuId(int skuId, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// Get Sku Profile Effective.
        /// </summary>
        /// <param name="skuProfileEffectiveId">Id of the sku profile effective.</param>
        /// <returns>SkuProfileEffectiveViewModel</returns>
        SkuProfileEffectiveViewModel GetSkuProfileEffective(int skuProfileEffectiveId);

        /// <summary>
        /// Create Sku Profile Effective.
        /// </summary>
        /// <param name="model">model to create.</param>
        /// <returns>Returns True or false.</returns>
        bool CreateSkuProfileEffective(SkuProfileEffectiveViewModel model);

        /// <summary>
        /// Updates the existing Sku Profile Effective.
        /// </summary>
        /// <param name="model">model to update.</param>
        /// <returns>Returns True or false.</returns>
        bool UpdateSkuProfileEffective(SkuProfileEffectiveViewModel model);

        /// <summary>
        /// Deletes the exising sku Profile Effective.
        /// </summary>
        /// <param name="skuProfileEffectiveID">Id of the sku profile Effective.</param>
        /// <returns>Returns True or false.</returns>
        bool DeleteSkuProfileEffective(int skuProfileEffectiveID, out string errorMessgae);

        /// <summary>
        /// Gets the sku profile list.
        /// </summary>
        /// <param name="skuId">Id of the sku.</param>
        /// <param name="skuEffectiveId">Id of the sku profile effective.</param>
        /// <param name="profileId">Id of the profile.</param>
        /// <returns>List of sku profiles.</returns>
        List<SelectListItem> GetSkuProfilesList(int skuId, int skuEffectiveId = 0, int profileId = 0);

        #endregion

        #region Store List Dropdown
        /// <summary>
        /// Bind List for Store 
        /// </summary>
        /// <returns>List of PortalModel</returns>
        List<PortalModel> GetStoreList();
        #endregion

        IEnumerable<ProductViewModel> GetProductByIds(string ids);
        void SetMessages(ProductViewModel model);
        ProductViewModel GetProductDetails(int productId);

        /// <summary>
        /// Copies a product.
        /// </summary>
        /// <param name="productId">Product ID.</param>
        /// <returns>Product View Model.</returns>
        ProductViewModel CopyProduct(int productId);

        /// <summary>
        /// Download Csv file
        /// </summary>
        /// <param name="model">CustomerBasedPricingListViewModel</param>
        /// <param name="response">HttpResponseBase</param>
        /// <param name="fileName">fileName to download</param>
        void DownloadFile(CustomerBasedPricingListViewModel model, HttpResponseBase response, string fileName);

        /// <summary>
        /// To set inventory setting
        /// </summary>
        /// <param name="model">ProductViewModel model</param>
        void SetInventorySettings(ProductViewModel model);

        /// <summary>
        /// To set inventory setting
        /// </summary>
        /// <param name="model">ProductViewModel model</param>
        void GetInventorySettings(ProductViewModel model);

        /// <summary>
        /// To set default value for product details
        /// </summary>
        /// <param name="model">ProductViewModel</param>
        void SetDefaultValues(ProductViewModel model);

        /// <summary>
        /// To Get Product id from form query string collection.
        /// </summary>
        /// <param name="formModel">form model</param>
        /// <returns></returns>
        int GetProductIdFromFormCollection(FilterCollectionDataModel formModel);

        /// <summary>
        ///  To Get SKU id from form query string collection.
        /// </summary>
        /// <param name="formModel">form model</param>
        /// <returns>sku id</returns>
        int GetSkuIdFromFormCollection(FilterCollectionDataModel formModel);

        /// <summary>
        /// Set no image for sku.
        /// </summary>
        /// <param name="model">SkuViewModel</param>
        void SetNoImageForSkuProperty(SkuViewModel model);

        /// <summary>
        /// Gets SelectedSku, Product Price based on selected attributeId
        /// </summary>
        /// <param name="id">Product Id</param>
        /// <param name="attributeId">Attribute Id</param>
        /// <param name="selectedIds">Selected Attribute Ids</param>
        /// <param name="quantity">Quantity</param>
        /// <param name="prodPrice">Product Price</param>
        /// <param name="selectedSku">Selected Sku</param>
        /// <param name="imageMediumPath">Selected Sku imageMediumPath</param>
        /// <returns></returns>
        ProductViewModel GetAttributes(int? id, int? attributeId, string selectedIds, int? quantity, out decimal prodPrice, out string selectedSku, out string imagePath, out string imageMediumPath);

        /// <summary>
        /// Get the extented price
        /// </summary>
        /// <param name="productId">Product ID</param>
        /// <param name="quantity">Quantity</param>
        /// <param name="sku">SKU</param>
        /// <param name="prodPrice"></param>
        /// <returns>Returns the extended price</returns>
        decimal GetExtendedPrice(int productId, int quantity, string sku, decimal prodPrice);

        /// <summary>
        /// Get the add on details
        /// </summary>
        /// <param name="product">View Model Of Product</param>
        /// <param name="selectedAddOnIds">selected AddOn Ids of perticular product</param>
        /// <param name="quantity">selected quantiy of product</param>
        /// <param name="productPrice">total product price excluding Taxes and Discounts</param>
        /// <param name="finalProductPrice">total product price including Taxes and Discounts</param>
        /// <param name="selectedSku">Selected Sku of product</param>
        /// <returns>IEnumerable Type List of addOns</returns>
        IEnumerable<AddOnViewModel> GetAddOnDetails(ProductViewModel product, string selectedAddOnIds, int? quantity, decimal productPrice, out decimal finalProductPrice, string selectedSku = null);

        /// <summary>
        /// Get the Bundled product details 
        /// </summary>
        /// <param name="ids">bundled product ids</param>
        /// <returns>Collection of products</returns>
        BundleDetailsViewModel GetBundleProductsDetails(CartItemViewModel cartItemViewModel);

        /// <summary>
        /// Get the product by IDs
        /// </summary>
        /// <param name="ids">Product IDs</param>
        /// <returns>Returns the list of product as product model</returns>
        Collection<ProductModel> GetProductsByIds(List<string> ids);

        /// <summary>
        /// Returns Bundles for list of prdocuts
        /// </summary>
        /// <returns>Collection of ProductViewModel</returns>
        List<BundleDisplayViewModel> GetBundles(string bundleProductIds);

        /// <summary>
        /// Get the BundleDetails of product
        /// </summary>
        /// <param name="model">Product View Model</param>
        /// <returns>Bundle Details View Model</returns>
        BundleDetailsViewModel GetCartModel(ProductViewModel model);
    
        //PRFT Custom Code:Start
        /// <summary>
        /// To get Product Inventory list from ERP
        /// </summary>
        /// <returns></returns>
        PRFTInventoryListViewModel GetInventoryListFromERP(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);
        //PRFT Custom Code:End

    }
}
