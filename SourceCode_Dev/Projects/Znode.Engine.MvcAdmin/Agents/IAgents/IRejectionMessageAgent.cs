﻿using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public interface IRejectionMessageAgent
    {
        /// <summary>
        /// Get rejection message list.
        /// </summary>
        /// <param name="filters">Filter Collection</param>
        /// <param name="sortCollection">Sort Collection</param>
        /// <param name="pageIndex">Page Index</param>
        /// <param name="recordPerPage">Record Per Page</param>
        /// <returns>RejectionMessageListViewModel</returns>
        RejectionMessageListViewModel GetRejectionMessages(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// Get existing rejection message.
        /// </summary>
        /// <param name="rejectionMessageId">Id of the rejection message.</param>
        /// <returns>RejectionMessageViewModel</returns>
        RejectionMessageViewModel GetRejectionMessage(int rejectionMessageId);

        /// <summary>
        /// Create rejection message.
        /// </summary>
        /// <param name="model">model to create.</param>
        /// <param name="errorMessage">error message</param>
        /// <returns>Returns True or False.</returns>
        bool CreateRejectionMessage(RejectionMessageViewModel model, out string errorMessage);

        /// <summary>
        /// Update the existing rejection message.
        /// </summary>
        /// <param name="model">model to update.</param>
        /// <param name="errorMessage">error message</param>
        /// <returns>Returns True or False.</returns>
        bool UpdateRejectionMessage(RejectionMessageViewModel model, out string errorMessage);

        /// <summary>
        /// Delete the existing rejection message.
        /// </summary>
        /// <param name="rejectionMessageId">Id of the rejection message.</param>
        /// <param name="errorMessage">error message</param>
        /// <returns>Returns True or False.</returns>
        bool DeleteRejectionMessage(int rejectionMessageId, out string errorMessage);
    }
}
