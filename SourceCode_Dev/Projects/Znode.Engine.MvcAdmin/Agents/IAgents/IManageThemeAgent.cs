﻿using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public interface IManageThemeAgent
    {
        /// <summary>
        /// Gets Theme list.
        /// </summary>
        /// <param name="filters">Filter Collection for theme List.</param>
        /// <param name="sorts">Sort collection of Theme List.</param>
        /// <param name="pageIndex">Index of requesting page</param>
        /// <param name="pageSize">Record per page</param>
        /// <returns>List of themes</returns>
        ThemeListViewModel GetThemeList(FilterCollection filters, SortCollection sorts, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// Create new theme
        /// </summary>
        /// <param name="model">Theme View Model</param>
        /// <param name="message">error message if theme not created</param>
        /// <returns>returns boolean value true/false</returns>
        bool CreateTheme(ThemeViewModel model, out string message);

        /// <summary>
        /// Get theme by themeId
        /// </summary>
        /// <param name="themeId">themeId to get the theme</param>
        /// <returns>Theme View Model</returns>
        ThemeViewModel GetThemenById(int themeId);

        /// <summary>
        /// Update the theme
        /// </summary>
        /// <param name="model">Theme View Model</param>
        /// <param name="message">error message if theme not updated</param>
        /// <returns>returns boolean value true/false</returns>
        bool UpdateTheme(ThemeViewModel model, out string message);

        /// <summary>
        /// Delete Theme
        /// </summary>
        /// <param name="themeId">Theme ID to delete Theme</param>
        /// <param name="message">error message if theme not deleted</param>
        /// <returns>boolean value true/false</returns>
        bool DeleteTheme(int themeId, out string message);
    }
}
