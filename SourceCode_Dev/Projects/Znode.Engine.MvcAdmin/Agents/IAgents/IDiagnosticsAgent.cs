﻿using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.MvcAdmin.Agents.IAgents
{
    public interface IDiagnosticsAgent
    {
        /// <summary>
        /// This method calls diagnostics client to check SMTP Account
        /// </summary>
        /// <returns>Returns the DiagnosticsResponse which cntains the status of smtp server</returns>
        DiagnosticsResponse CheckEmailAccount();

        /// <summary>
        /// This method calls diagnostics client to get Version details of product from database
        /// </summary>
        /// <returns>Returns the DiagnosticsResponse which contains the version details</returns>
        DiagnosticsResponse GetProductVersionDetails();

        /// <summary>
        /// This method calls the diagnostics client to sends the diagnostics email
        /// </summary>
        /// <param name="caseNumber">Case number for diagnostics</param>
        /// <returns>Returns the DiagnosticsResponse which contains the email sent status</returns>
        DiagnosticsResponse EmailDiagnostics(DiagnosticsEmailModel model);

        /// <summary>
        /// This method validates the license (whether the license is installed on the calling server or not)
        /// This method calls the ZNode.Libraries.Framework.Business.ZNodeLicenseManager class to validate the license
        /// </summary>
        /// <returns>Returns ZNodeLicenseType which will contain the status true if the license is valid otherwise false</returns>
        ZNodeLicenseType ValidateLicense();

        /// <summary>
        /// This method validates the license (whether the license is installed on the calling server or not)
        /// This method calls the ZNode.Libraries.Framework.Business.ZNodeLicenseManager class to get the license status description
        /// </summary>
        /// <returns>Returns License Status Description</returns>
        string GetLicenseStatusDescription();

        /// <summary>
        /// Check Public Folder for Read + Write Permissions
        /// </summary>
        /// <param name="path">Represents the path </param>
        /// <returns>true if the folder have read+write permission</returns>
        bool CheckFolderPermissions(string path);

        /// <summary>
        /// Check for Database connection String
        /// </summary>
        /// <returns>Returns the status of database connection</returns>
        bool CheckDataBaseConnection();
    }
}