﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public partial interface IManufacturersAgent
    {
        /// <summary>
        /// This method will save the Manufacturer and return Manufacturers view model
        /// </summary>
        /// <param name="model">ManufacturersViewModel model</param>
        /// <returns>Returs true if manufacturer saved</returns>
        ManufacturersViewModel CreateManufacturer(ManufacturersViewModel model);
    }
}