﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
   public interface IManageSearchIndexAgent
    {
       /// <summary>
       /// Get lucene index list
       /// </summary>
       /// <param name="model">filter collection model</param>
        /// <returns>Returns LuceneIndexListViewModel</returns>
       LuceneIndexListViewModel GetLuceneIndexStatusList(FilterCollectionDataModel model);

       /// <summary>
       /// Create Lucene index
       /// </summary>
       /// <returns>true/false</returns>
       bool CreateIndex();

       /// <summary>
       /// Disable/Enable lucene index trigger
       /// </summary>
       /// <param name="flag">int flag</param>
       /// <returns>int flag</returns>
       int DisableTriggers(int flag);

       /// <summary>
       /// Disable/Enable lucene index service
       /// </summary>
       /// <param name="flag">int flag</param>
       /// <returns>int flag</returns>
       int DisableWinservice(int flag);

       /// <summary>
       /// Get Index server log list
       /// </summary>
       /// <param name="indexId">index id</param>
       /// <returns>Returns LuceneIndexServerListViewModel</returns>
       LuceneIndexServerListViewModel GetSearchIndexStatus(int indexId);

    }
}
