﻿using System.Collections.Generic;
using System.Web.Mvc;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public interface ISupplierAgent
    {
        /// <summary>
        /// Get supplier list.
        /// </summary>
        /// <param name="filters">Filter Collection</param>
        /// <param name="sortCollection">Sort Collection</param>
        /// <param name="pageIndex">page Index</param>
        /// <param name="recordPerPage">record Pe rPage</param>
        /// <returns>SupplierListViewModel</returns>
        SupplierListViewModel GetSuppliers(FilterCollection filters, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// Get the existing supplier.
        /// </summary>
        /// <param name="supplierId">Id of the Supplier.</param>
        /// <returns>SupplierViewModel</returns>
        SupplierViewModel GetSupplier(int supplierId);

        /// <summary>
        /// Create Supplier.
        /// </summary>
        /// <param name="model">model to create.</param>
        /// <returns>Returns true or false.</returns>
        bool CreateSupplier(SupplierViewModel model);

        /// <summary>
        /// Update the exsting supplier.
        /// </summary>
        /// <param name="model">updated model.</param>
        /// <returns>Returns true or false.</returns>
        bool UpdateSupplier(SupplierViewModel model);

        /// <summary>
        /// Delete the exsting supplier.
        /// </summary>
        /// <param name="supplierId">Id of the Supplier.</param>
        /// <param name="errorMessage">error message</param>
        /// <returns>Returns true or false.</returns>
        bool DeleteSupplier(int supplierId, out string errorMessage);

        /// <summary>
        /// Get the supplier type list.
        /// </summary>
        /// <returns>List<SupplierTypeModel></returns>
        List<SupplierTypeModel> GetSupplierTypeList(bool isEditAction = false);

        /// <summary>
        /// Get the list of active status.
        /// </summary>
        /// <returns>List<SelectListItem></returns>
        List<SelectListItem> GetStatusList();
    }
}
