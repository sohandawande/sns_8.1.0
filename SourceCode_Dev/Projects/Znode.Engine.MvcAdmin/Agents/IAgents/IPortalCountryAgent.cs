﻿using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public interface IPortalCountryAgent
    {
        /// <summary>
        /// Deletes a portal country if it is not associated with any address.
        /// </summary>
        /// <param name="portalCountryId">Portal Country id.</param>
        /// <returns>Bool value if portal country is deleted or not.</returns>
        bool DeletePortalCountry(int portalCountryId, out string message);

        /// <summary>
        /// Get country list.
        /// </summary>
        /// <param name="filters">filters</param>
        /// <param name="sortCollection">sortCollection</param>
        /// <param name="pageIndex">pageIndex</param>
        /// <param name="recordPerPage">recordPerPage</param>
        /// <returns>Country list.</returns>
        CountryListViewModel GetCountryList(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        ///  Add New Portal Countries.
        /// </summary>
        /// <param name="portalId">Portal ID.</param>
        /// <param name="billableCountryCodes">Billing country codes.</param>
        /// <param name="shipableCountryCodes">Shipping Country codes.</param>
        /// <param name="message">Message from service.</param>
        /// <returns>Returns true or false.</returns>
        bool AddPortalCountries(int portalId, string billableCountryCodes, string shipableCountryCodes, out string message);

        /// <summary>
        /// Get all Portal Countries
        /// </summary>
        /// <param name="PortalId">int PortalId</param>
        /// <param name="filters">FilterCollection filters</param>
        /// <param name="sortCollection">SortCollection sortCollection</param>
        /// <param name="pageIndex">Nullable int pageIndex</param>
        /// <param name="recordPerPage">Nullable int recordPerPage</param>
        /// <returns>Returns Portal Countries List</returns>
        PortalCountriesListViewModel GetAllPortalCountries(int PortalId, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);
    }
}
