﻿using System.Collections.Generic;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public interface IProductSearchSettingAgent
    {
        /// <summary>
        /// Get product level settings
        /// </summary>
        /// <param name="filters">FilterCollection filters</param>
        /// <param name="sortCollection">SortCollection sortCollection</param>
        /// <param name="pageIndex">Nullable int pageIndex</param>
        /// <param name="recordPerPage">Nullable int recordPerPage</param>
        /// <returns>Return Product level setting list view model</returns>
        ProductLevelSettingListViewModel GetProductLevelSettings(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// Get category level settings
        /// </summary>
        ///<param name="filters">FilterCollection filters</param>
        /// <param name="sortCollection">SortCollection sortCollection</param>
        /// <param name="pageIndex">Nullable int pageIndex</param>
        /// <param name="recordPerPage">Nullable int recordPerPage</param>
        /// <returns>Return Category level setting list view model</returns>
        CategoryLevelSettingListViewModel GetCategoryLevelSettings(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// Get field level settings
        /// </summary>
        ///<param name="filters">FilterCollection filters</param>
        /// <param name="sortCollection">SortCollection sortCollection</param>
        /// <param name="pageIndex">Nullable int pageIndex</param>
        /// <param name="recordPerPage">Nullable int recordPerPage</param>
        /// <returns>Return field level setting list view model</returns>
        FieldLevelSettingListViewModel GetFieldLevelSettings(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// Get all Catalogs
        /// </summary>
        /// <returns>Returns catalog model</returns>
        List<CatalogModel> GetCatalogList();

        /// <summary>
        /// Get all categories
        /// </summary>
        /// <returns>Returns catagory model</returns>
        List<CategoryModel> GetCategoryList();

        /// <summary>
        /// Get all Manufacturers
        /// </summary>
        /// <returns>Returns manufacturers model</returns>
        List<ManufacturerModel> GetManufacturerList();
        
        /// <summary>
        /// Get all product type
        /// </summary>
        /// <returns>Returns product type model</returns>
        List<ProductTypeModel> GetProductTypeList();

        /// <summary>
        /// Save Product Search Boost Values for sections like Category, Product and Field.
        /// </summary>
        /// <param name="ids">Product/Categories/Fields Ids</param>
        /// <param name="values">Product/Categories/Fields boost values</param>
        /// <param name="name">Boost Name</param>
        /// <returns>true/false</returns>
        bool SaveProductBoostValues(string[] ids, string[] values, string name);
    }
}
