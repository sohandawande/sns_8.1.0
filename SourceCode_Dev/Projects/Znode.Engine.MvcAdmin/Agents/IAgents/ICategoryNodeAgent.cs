﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public interface ICategoryNodeAgent
    {
        /// <summary>
        /// Edits the category node.
        /// </summary>
        /// <param name="categoryNodeId">The id of the category node</param>
        /// <returns>Returns model of type CategoryNodeViewModel.</returns>
        CategoryNodeViewModel EditCategoryNode(int categoryNodeId);

        /// <summary>
        /// Updates category node.
        /// </summary>
        /// <param name="categoryNodeId">The id of the category node</param>
        /// <param name="model">Model of type CategoryNodeViewModel</param>
        /// <returns>Returns model of type CategoryNodeViewModel</returns>
        CategoryNodeViewModel UpdateCategoryNode(int categoryNodeId, CategoryNodeViewModel model);

        /// <summary>
        /// Associates an already created category to the catalog.
        /// </summary>
        /// <param name="model">The model of type CategoryNodeViewModel.</param>              
        /// <returns>Returns true or false.</returns>
        bool AssociateCategory(CategoryNodeViewModel model);

        /// <summary>
        /// Deletes category node.
        /// </summary>
        /// <param name="categoryNodeId">the id of category node</param>
        /// <returns>Returns model of type ZnodeDeleteViewModel.</returns>
        ZnodeDeleteViewModel DeleteCategoryNode(int categoryNodeId);
    }
}
