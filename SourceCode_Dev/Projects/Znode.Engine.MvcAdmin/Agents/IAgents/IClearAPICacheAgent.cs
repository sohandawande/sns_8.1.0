﻿
namespace Znode.Engine.MvcAdmin.Agents
{
    /// <summary>
    /// This is the interface which will have signatures of clear API cache functionality
    /// </summary>
    public interface IClearAPICacheAgent 
    {
        /// <summary>
        /// This method will clear the API cache
        /// </summary>
        /// <returns>true if cache cleared</returns>
        bool ClearApiCache();
    }
}
