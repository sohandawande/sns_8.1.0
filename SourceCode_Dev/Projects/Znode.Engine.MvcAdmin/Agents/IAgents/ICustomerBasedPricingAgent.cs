﻿using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public interface ICustomerBasedPricingAgent 
    {
        /// <summary>
        /// Get Customer Based Pricing List
        /// </summary>
        /// <param name="productId">Product Id</param>
        /// <param name="filters">Filters for searching customer pricing</param>
        /// <param name="sortCollection">sort collection for customer pricing</param>
        /// <param name="pageIndex">page index for page</param>
        /// <param name="recordPerPage">page customer pricing record per page</param>
        /// <returns>CustomerBasedPricingListViewModel</returns>
        CustomerBasedPricingListViewModel GetCustomerPricingList(int productId, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);
    }
}