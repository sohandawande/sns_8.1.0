﻿using System.Collections.Generic;
using System.Web.Mvc;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public interface IPromotionAgent
    {
        /// <summary>
        /// Get list of all promotions
        /// </summary>
        /// <param name="filters">Filter Collection</param>
        /// <param name="sortCollection">Sort Collection</param>
        /// <param name="pageIndex">Page Index</param>
        /// <param name="recordPerPage">countnumber of records to display</param>
        /// <returns>PromotionsListViewModel</returns>
        PromotionsListViewModel GetPromotions(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// Get Promotion on the basis of promotionId
        /// </summary>
        /// <param name="promotionId">promotion Id</param>
        /// <returns>Promotion related to Id</returns>
        PromotionsViewModel GetPromotionById(int promotionId);

        /// <summary>
        /// Deletes promotion from list on the basis of promotionId
        /// </summary>
        /// <param name="promotionId">promotion Id</param>
        /// <returns>Delete promotion</returns>
        bool DeletePromotion(int promotionId);

        /// <summary>
        /// Update the promtion on the basis of promotionId
        /// </summary>
        /// <param name="model">Promotion View Model</param>
        /// <returns>Update promotion</returns>
        bool UpdatePromotion(PromotionsViewModel model, out string message);

        /// <summary>
        /// Create the new promotion
        /// </summary>
        /// <param name="model">Promotion View Model</param>
        /// <returns>create promotion</returns>
        bool SavePromotion(PromotionsViewModel model, out string message);

        /// <summary>
        /// Get the list of PromotionType
        /// </summary>
        /// <returns>List of all promotiontypes</returns>
        List<PromotionTypeModel> GetPromotionModelTypeList(string userType, bool IsListAction = false);

        /// <summary>
        /// Get the list of Category
        /// </summary>
        /// <returns>List of all categories</returns>
        List<CategoryModel> GetCategoryModelList();

        /// <summary>
        /// Get the list of Manufacturer
        /// </summary>
        /// <returns>List of all manufacturers</returns>
        List<ManufacturerModel> GetManufacturerModelList();

        /// <summary>
        /// Get the list of all products
        /// </summary>
        /// <param name="filters">Filter Collection</param>
        /// <param name="sortCollection">Sort Collection</param>
        /// <param name="pageIndex">Page Index</param>
        /// <param name="recordPerPage">countnumber of records to display</param>
        /// <returns>List of Products</returns>
        ProductListViewModel GetProducts(FilterCollection filters, SortCollection sortCollection, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// Get the list of SKU
        /// </summary>
        /// <param name="productId">productId to retrieve SKUList</param>
        /// <returns>Sku list of type selectListItem</returns>
        List<SelectListItem> GetSkuList(int productId);

        /// <summary>
        /// Get the list of all stores
        /// </summary>
        /// <returns>List of stores of type selectListItem</returns>
        List<SelectListItem> GetPortalList();

        /// <summary>
        /// Get the list of all Manufacturers
        /// </summary>
        /// <returns>List of manufacturer of type selectListItem</returns>
        List<SelectListItem> GetManufacturerList();

        /// <summary>
        /// Get the list of all Catalogs
        /// </summary>
        /// <param name="portalId">portalId to retrieve catalogList</param>
        /// <returns>List of catalog of type selectListItem</returns>
        List<SelectListItem> GetCatalogList(int portalId);

        /// <summary>
        /// Get the list of all Categories       
        /// </summary>
        /// <param name="portalId">portalId to retrieve categoryList</param>
        /// <returns>List of category of type selectListItem</returns>
        List<SelectListItem> GetCategoryList(int portalId);

        /// <summary>
        /// Get the list of all Profiles
        /// </summary>
        /// <param name="portalId">portalId to retrieve profileList(</param>
        /// <returns>List of profiles of type selectListItem</returns>
        List<SelectListItem> GetProfileList(int portalId);

        /// <summary>
        /// Get the Dropndown values of categories, catalogs,manufacturers,profiles for promotions
        /// </summary>
        /// <param name="model">PromotionsViewModel</param>
        /// <returns>PromotionsViewModel</returns>
        PromotionsViewModel GetPromotionInformation(PromotionsViewModel model, bool IsEditAction = false);
        
    }
}
