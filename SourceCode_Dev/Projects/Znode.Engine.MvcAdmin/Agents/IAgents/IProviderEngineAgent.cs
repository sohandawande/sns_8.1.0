﻿using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public interface IProviderEngineAgent
    {
        /// <summary>
        /// Get all Promotion Types
        /// </summary>
        /// <param name="filters">FilterCollection filters</param>
        /// <param name="sortCollection">SortCollection sortCollection</param>
        /// <param name="pageIndex">Nullable int pageIndex</param>
        /// <param name="recordPerPage">Nullable int recordPerPage</param>
        /// <returns>Return list of Promotion Types</returns>
        PromotionTypeListViewModel GetPromotionTypes(FilterCollection filters, SortCollection sortCollection, int? pageIndex, int? recordPerPage);

        /// <summary>
        /// Delete Promotion Type
        /// </summary>
        /// <param name="promotionTypeId">int promotionTypeId</param>
        /// <param name="errorMessage">out string errorMessage</param>
        /// <returns>Return True or false</returns>
        bool DeletePromotionType(int promotionTypeId,out string errorMessage);

        /// <summary>
        /// Update Promotion Type
        /// </summary>
        /// <param name="model">ProviderEngineViewModel model</param>
        /// <returns>Return True or false</returns>
        bool UpdatePromotionType(ProviderEngineViewModel model);

        /// <summary>
        /// Get Promotion Type
        /// </summary>
        /// <param name="PromotionTypeId">int PromotionTypeId</param>
        /// <returns>Return Promotion Type</returns>
        ProviderEngineViewModel GetPromotionType(int PromotionTypeId);

        /// <summary>
        /// Get Promotion Type by class name
        /// </summary>
        /// <param name="className">string className</param>
        /// <returns>Return Promotion Type</returns>
        ProviderEngineViewModel GetPromotionTypeByClassName(string className);

        /// <summary>
        /// Create Promotion Type
        /// </summary>
        /// <param name="model">ProviderEngineViewModel model</param>
        /// <returns>Return True or false</returns>
        bool CreatePromotionType(ProviderEngineViewModel model);

        /// <summary>
        /// Bind all available Promotion Types
        /// </summary>
        /// <param name="Model">ProviderEngineViewModel Model</param>
        /// <returns>Return Provider Engine View Model</returns>
        ProviderEngineViewModel BindAvailablePromotionTypes(ProviderEngineViewModel Model);

        /// <summary>
        /// Get all Tax Rule Type 
        /// </summary>
        /// <param name="filters">FilterCollection filters</param>
        /// <param name="sortCollection">SortCollection sortCollection</param>
        /// <param name="pageIndex">Nullable int pageIndex</param>
        /// <param name="recordPerPage">Nullable int recordPerPage</param>
        /// <returns>Return list of Tax Rule Type</returns>
        TaxRuleTypeListViewModel GetTaxRuleTypes(FilterCollection filters, SortCollection sortCollection, int? pageIndex, int? recordPerPage);

        /// <summary>
        /// Delete Tax Rule Type 
        /// </summary>
        /// <param name="taxRuleTypeId">int taxRuleTypeId</param>
        /// <param name="errorMessage">out string errorMessage</param>
        /// <returns>Return True or false</returns>
        bool DeleteTaxRuleType(int taxRuleTypeId, out string errorMessage);

        /// <summary>
        /// Get Tax Rule Type 
        /// </summary>
        /// <param name="TaxRuleTypeId">int TaxRuleTypeId</param>
        /// <returns>Return Tax Rule Type</returns>
        ProviderEngineViewModel GetTaxRuleType(int TaxRuleTypeId);

        /// <summary>
        /// Update Tax Rule Type
        /// </summary>
        /// <param name="model">ProviderEngineViewModel model</param>
        /// <returns>Return True or false</returns>
        bool UpdateTaxRuleType(ProviderEngineViewModel model);

        /// <summary>
        /// Get Tax Rule Type by class name
        /// </summary>
        /// <param name="className">string className</param>
        /// <returns>Return Tax Rule Type </returns>
        ProviderEngineViewModel GetTaxRuleTypeByClassName(string className);

        /// <summary>
        /// Create Tax Rule Type
        /// </summary>
        /// <param name="model">ProviderEngineViewModel model</param>
        /// <returns>Return True or false</returns>
        bool CreateTaxRuleType(ProviderEngineViewModel model);

        /// <summary>
        /// Bind all available Tax Rule Type
        /// </summary>
        /// <param name="Model">ProviderEngineViewModel Model</param>
        /// <returns>Return Provider Engine View Model</returns>
        ProviderEngineViewModel BindAvailableTaxRuleType(ProviderEngineViewModel Model);

        /// <summary>
        ///  Get all Supplier Type
        /// </summary>
        /// <param name="filters">FilterCollection filters</param>
        /// <param name="sortCollection">SortCollection sortCollection</param>
        /// <param name="pageIndex">Nullable int pageIndex</param>
        /// <param name="recordPerPage">Nullable int recordPerPage</param>
        /// <returns>Return list of Supplier Type</returns>
        SupplierTypeListViewModel GetSupplierTypes(FilterCollection filters, SortCollection sortCollection, int? pageIndex, int? recordPerPage);

        /// <summary>
        /// Delete Supplier Type
        /// </summary>
        /// <param name="supplierTypeId">int supplierTypeId</param>
        /// /// <param name="errorMessage">out string errorMessage</param>
        /// <returns>Return True or false</returns>
        bool DeleteSupplierType(int supplierTypeId, out string errorMessage);

        /// <summary>
        /// Get Supplier Type
        /// </summary>
        /// <param name="SupplierTypeId">int SupplierTypeId</param>
        /// <returns>Return Supplier Type</returns>
        ProviderEngineViewModel GetSupplierType(int SupplierTypeId);

        /// <summary>
        /// Update Supplier Type
        /// </summary>
        /// <param name="model">ProviderEngineViewModel model</param>
        /// <returns>Return True or false</returns>
        bool UpdateSupplierType(ProviderEngineViewModel model);

        /// <summary>
        /// Get Supplier Type by class name
        /// </summary>
        /// <param name="className">string className</param>
        /// <returns>Return Supplier Type</returns>
        ProviderEngineViewModel GetSupplierTypeByClassName(string className);

        /// <summary>
        /// Create Supplier Type
        /// </summary>
        /// <param name="model">ProviderEngineViewModel model</param>
        /// <returns>Return True or false</returns>
        bool CreateSupplierType(ProviderEngineViewModel model);

        /// <summary>
        /// Bind all available Supplier Type
        /// </summary>
        /// <param name="Model">ProviderEngineViewModel Model</param>
        /// <returns>Return Provider Engine View Model</returns>
        ProviderEngineViewModel BindAvailableSupplierType(ProviderEngineViewModel Model);

        /// <summary>
        /// Get all Shipping Type
        /// </summary>
        /// <param name="filters">FilterCollection filters</param>
        /// <param name="sortCollection">SortCollection sortCollection</param>
        /// <param name="pageIndex">Nullable int pageIndex</param>
        /// <param name="recordPerPage">Nullable int recordPerPage</param>
        /// <returns>Return list of Shipping Type</returns>
        ShippingTypeListViewModel GetShippingTypes(FilterCollection filters, SortCollection sortCollection, int? pageIndex, int? recordPerPage);
        
        /// <summary>
        /// Delete Shipping Type
        /// </summary>
        /// <param name="shippingTypeId">int shippingTypeId</param>
        /// <param name="errorMessage">out string errorMessage</param>
        /// <returns>Return True or false</returns>
        bool DeleteShippingType(int shippingTypeId, out string errorMessage);

        /// <summary>
        /// Get Shipping Type
        /// </summary>
        /// <param name="ShippingTypeId">int ShippingTypeId</param>
        /// <returns>Return Shipping Type</returns>
        ProviderEngineViewModel GetShippingType(int ShippingTypeId);

        /// <summary>
        /// Update Shipping Type
        /// </summary>
        /// <param name="model">ProviderEngineViewModel model</param>
        /// <returns>Return True or false</returns>
        bool UpdateShippingType(ProviderEngineViewModel model);

        /// <summary>
        /// Get Shipping Types by class name
        /// </summary>
        /// <param name="className">string className</param>
        /// <returns>Return Shipping Type</returns>
        ProviderEngineViewModel GetShippingTypeByClassName(string className);

        /// <summary>
        /// Create Shipping Type
        /// </summary>
        /// <param name="model">ProviderEngineViewModel model</param>
        /// <returns>Return True or false</returns>
        bool CreateShippingType(ProviderEngineViewModel model);

        /// <summary>
        /// Bind all available Shipping Type
        /// </summary>
        /// <param name="Model">ProviderEngineViewModel Model</param>
        /// <returns></returns>
        ProviderEngineViewModel BindAvailableShippingType(ProviderEngineViewModel Model);
    }
}
