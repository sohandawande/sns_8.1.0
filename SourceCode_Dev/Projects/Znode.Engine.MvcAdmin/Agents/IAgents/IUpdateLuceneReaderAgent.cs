﻿
namespace Znode.Engine.MvcAdmin.Agents
{
    public interface IUpdateLuceneReaderAgent
    {
        /// <summary>
        /// This method Updates the Lucene Reader
        /// </summary>
        void UpdateReader();
    }
}
