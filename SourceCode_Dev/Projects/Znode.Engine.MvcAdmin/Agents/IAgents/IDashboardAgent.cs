﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public interface IDashboardAgent
    {
        /// <summary>
        /// Get Dashboard Items By Portal Id.
        /// </summary>
        /// <param name="portalId">Id of the portal.</param>
        /// <returns>DashboardViewModel</returns>
        DashboardViewModel GetDashboardItemsByPortalId(int portalId);
    }
}
