﻿using System.Collections.Generic;
using System.Web.Mvc;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    /// <summary>
    /// Interface for the Shipping Agent
    /// </summary>
    public interface IShippingAgent
    {
        /// <summary>
        /// Get Shipping Options List
        /// </summary>
        /// <param name="filters">Filter Collection</param>
        /// <param name="sortCollection">Sort Collection</param>
        /// <param name="pageIndex">Index of page</param>
        /// <param name="recordPerPage">Record per page</param>
        /// <returns>Returns ShippingOptionListViewModel</returns>
        ShippingOptionListViewModel GetShippingOptions(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// Get Shipping Options information on the basis of shippingId
        /// </summary>
        /// <param name="shippingOptionId">int shippingId</param>
        /// <param name="filters">Filter Collection</param>
        /// <param name="sortCollection">Sort Collection</param>
        /// <param name="pageIndex">Index of page</param>
        /// <param name="recordPerPage">Record per page</param>
        /// <returns>Returns ShippingOptionViewModel</returns>
        ShippingOptionViewModel GetShippingOption(int shippingOptionId, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// Delete Shipping Rule
        /// </summary>
        /// <param name="shippingRuleId">int shippingRuleId</param>
        /// <returns>Returns true/false</returns>
        bool DeleteShippingRule(int shippingRuleId);

        /// <summary>
        /// Get Shipping rule Types List
        /// </summary>
        /// <returns>Returns ShippingRuleViewModel</returns>
        ShippingRuleViewModel GetShippingRuleTypes();

        /// <summary>
        /// Save new Shipping Rule
        /// </summary>
        /// <param name="model">Model of type ShippingRuleViewModel</param>
        /// <param name="message">message</param>
        /// <returns>Returns true/false</returns>
        bool SaveShippingRule(ShippingRuleViewModel model, out string message);

        /// <summary>
        /// Update Shipping Rule
        /// </summary>
        /// <param name="shippingOptionId">int shippingOptionId</param>
        /// <param name="model">Model of type ShippingRuleViewModel</param>
        /// <param name="message">message</param>
        /// <returns>Returns true/false</returns>
        bool UpdateShippingRule(int shippingOptionId, ShippingRuleViewModel model, out string message);

        /// <summary>
        /// Get Shipping Rule Type
        /// </summary>
        /// <param name="shippingOptionId">int shippingOptionId</param>
        /// <returns>Returns ShippingRuleViewModel</returns>
        ShippingRuleViewModel GetShippingRule(int shippingOptionId);

        /// <summary>
        /// Get Shipping Type List
        /// </summary>
        /// <param name="isEditAction">bool isEditAction parameter to check whether Action is Edit Action or not</param>
        /// <returns>Returns ShippingOptionViewModel</returns>
        ShippingOptionViewModel GetShippingTypes(bool isEditAction = false);

        /// <summary>
        /// Get Profile List
        /// </summary>
        /// <returns>Returns ShippingOptionViewModel</returns>
        ShippingOptionViewModel GetProfileList();

        /// <summary>
        /// Get Country List
        /// </summary>
        /// <returns>Returns ShippingOptionViewModel</returns>
        ShippingOptionViewModel GetCountryList();

        /// <summary>
        /// Get Shipping Service Code List
        /// </summary>
        /// <returns>Returns ShippingOptionViewModel</returns>
        ShippingOptionViewModel GetShippingServiceCodeList();

        /// <summary>
        /// Bind Shipping List 
        /// </summary>
        /// <param name="filters">Filter Collection</param>
        /// <param name="sortCollection">Sort Collection</param>
        /// <returns>Returns Service List</returns>
        List<SelectListItem> BindServiceList(FilterCollection filters = null, SortCollection sortCollection = null);

        /// <summary>
        /// Bind Shipping Options 
        /// </summary>
        /// <param name="filters">Filter Collection</param>
        /// <param name="sortCollection">Sort Collection</param>
        /// <param name="portalId">int nullable portal id</param>
        /// <param name="shippingCountryCode">string shippingCountryCode</param>
        /// <returns>Returns Shipping List</returns>
        List<SelectListItem> BindShippingList(FilterCollection filters = null, SortCollection sortCollection = null, int? portalId = null, string shippingCountryCode = null);

        /// <summary>
        /// Bind Franchise Shipping List 
        /// </summary>
        /// <param name="portalId">nullable int portalId</param>
        /// <param name="filters">Filter Collection</param>
        /// <param name="sortCollection">Sort Collection</param>
        /// <returns>Returns Service List</returns>
        List<SelectListItem> BindFranchiseShippingList(int? portalId = null, FilterCollection filters = null, SortCollection sortCollection = null);

        /// <summary>
        /// Add Shipping Option 
        /// </summary>
        /// <param name="model">Model of type ShippingOptionViewModel</param>
        /// <param name="message">message</param>
        /// <returns>Returns Shipping Option Id</returns>
        int SaveShippingOption(ShippingOptionViewModel model, out string message);

        /// <summary>
        /// Update Shipping Option
        /// </summary>
        /// <param name="shippingOptionId">int shippingOptionId</param>
        /// <param name="model">Model of type ShippingOptionViewModel</param>
        /// <returns>Returns true/false</returns>
        bool UpdateShippingOption(int shippingOptionId, ShippingOptionViewModel model, out string message);

        /// <summary>
        /// Get Shipping Service Code based on shippingServiceCodeId
        /// </summary>
        /// <param name="shippingServiceCodeId">int shippingServiceCodeId</param>
        /// <returns>Returns ShippingServiceCodeViewModel</returns>
        ShippingServiceCodeViewModel GetShippingServiceCode(int shippingServiceCodeId);

        /// <summary>
        /// Get Shipping Options List
        /// </summary>
        /// <param name="filters">Filter Collection</param>
        /// <param name="sortCollection">Sort Collection</param>
        /// <param name="pageIndex">Index of page</param>
        /// <param name="recordPerPage">Record per page</param>
        /// <returns>Returns ShippingOptionListViewModel</returns>
        ShippingOptionListViewModel GetFranchiseShippingOptions(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// Delete Shipping Options
        /// </summary>
        /// <param name="shippingOptionId">int shippingOptionId</param>
        /// <param name="message">message</param>
        /// <returns>Returns true/false</returns>
        bool DeleteShippingOption(int shippingOptionId, out string message);

        /// <summary>
        /// Get Shipping Type Id on the basis of its ClassName
        /// </summary>
        /// <param name="xyz">Shipping Type Class Name</param>
        /// <returns>Returns Shippinf Type Id</returns>
        int GetShippingTypeId(string className);
    }
}