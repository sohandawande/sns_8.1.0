﻿using System.Collections.Generic;
using System.Web.Mvc;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    interface IContentPageAgent
    {
        /// <summary>
        /// Get Content Page list.
        /// </summary>
        /// <returns>returns ContentPageListViewModel</returns>
        ContentPageListViewModel GetContentPages();

        /// <summary>
        /// Get Content Page list.
        /// </summary>
        /// <param name="filters">FilterCollection filters = null</param>
        /// <param name="sortCollection">SortCollection sortCollection = null</param>
        /// <param name="pageIndex">int? pageIndex</param>
        /// <param name="recordPerPage">int? recordPerPage</param>
        /// <returns>returns ContentPageListViewModel</returns>
        ContentPageListViewModel GetContentPages(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        ///  Get Content page details by contentPageId.
        /// </summary>
        /// <param name="contentPageId">int contentPageId</param>
        /// <returns>returns ContentPageViewModel</returns>
        ContentPageViewModel GetContentPage(int? contentPageId);

        /// <summary>
        /// /// <summary>
        /// Save new Content Page.
        /// </summary>
        /// <param name="model">ContentPageViewModel model</param>
        /// <param name="errorMessage">error message</param>
        /// <returns>true or false</returns>
        bool CreateContentPage(ContentPageViewModel model, out string errorMessage);

        /// <summary>
        /// Update Content Page.
        /// </summary>
        /// <param name="model">ContentPageViewModel model</param>
        /// <param name="errorMessage">error message</param>
        /// <returns>true or false</returns>
        bool UpdateContentPage(ContentPageViewModel model, out string errorMessage);

        /// <summary>
        /// Delete Existing Content Page.
        /// </summary>
        /// <param name="contentPageId">int contentPageId</param>
        /// <param name="errorMessage">error message</param>
        /// <returns>true or false</returns>
        bool DeleteContentPage(int contentPageId, out string errorMessage);

        /// <summary>
        /// Check is delete allowed.
        /// </summary>
        /// <param name="contentPageId">Id of the content page</param>
        /// <returns>Returns true or false</returns>
        bool IsAllowDelete(int contentPageId);

        /// <summary>
        /// Reverts the revision to its previous version.
        /// </summary>
        /// <param name="revisionId">Id of the revision.</param>
        /// <param name="contentPageId">Id of the content page.</param>
        /// <param name="errorMessage">error message</param>
        /// <returns>Returns true or false</returns>
        bool RevertRevision(int revisionId, int? contentPageId, out string errorMessage);

        /// <summary>
        /// Bind the Css List.
        /// </summary>
        /// <param name="filters">FilterCollection filters</param>
        /// <returns>Returns List<SelectListItem></returns>
        List<SelectListItem> BindCssList(FilterCollection filters);

        /// <summary>
        /// Bind the master page list.
        /// </summary>
        /// <param name="CssThemeId">Id of the Theme.</param>
        /// <param name="pageType">page type of page.</param>
        /// <returns>Returns List<SelectListItem></returns>
        List<SelectListItem> BindMasterPageList(int CssThemeId, string pageType);

        /// <summary>
        /// Bind the theme list.
        /// </summary>
        /// <returns>Returns list of themes.</returns>
        List<SelectListItem> BindThemeList();

        /// <summary>
        /// Bind the portal list.
        /// </summary>
        /// <returns>Returns list of portals.</returns>
        List<SelectListItem> BindPortalList();

        /// <summary>
        /// Get Content Page Revisions by Id.
        /// </summary>
        /// <param name="contentPageId">Id of the content page.</param>
        /// <param name="filters">Filter Collection of content page.</param>
        /// <param name="sortCollection">Sort Collection of content page.</param>
        /// <param name="pageIndex">int? pageIndex</param>
        /// <param name="recordPerPage">int? recordPerPage</param>
        /// <returns>ContentPageRevisionListViewModel</returns>
        ContentPageRevisionListViewModel GetContentPageRevisionById(int contentPageId, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// Get the portal list.
        /// </summary>
        /// <returns>List of portals.</returns>
        List<PortalViewModel> GetPortalsList();
    }
}
