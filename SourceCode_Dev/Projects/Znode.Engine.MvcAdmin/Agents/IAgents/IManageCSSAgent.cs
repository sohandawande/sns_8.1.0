﻿using System.Collections.Generic;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public interface IManageCSSAgent
    {
        /// <summary>
        /// Gets CSS List.
        /// </summary>
        /// <param name="expands">Expands for Css List.</param>
        /// <param name="filters">Filters for Css List.</param>
        /// <param name="sorts">Sorts for Css List.</param>
        /// <param name="pageIndex">Page index for Css List.</param>
        /// <param name="pageSize">Page Size for Css List.</param>
        /// <returns>CSS List View Model</returns>
        CSSListViewModel GetCSSList(FilterCollection filters, SortCollection sorts, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// Get List of Theme model for dynamic grid search dropdown
        /// </summary>
        /// <returns>List of Theme Model</returns>
        List<ThemeModel> GetThemeModelList();

        /// <summary>
        /// Create new CSS
        /// </summary>
        /// <param name="model">CSS view model</param>
        /// <param name="message">error message if css is not created</param>
        /// <returns>boolean value true/false</returns>
        bool CreateCSS(CSSViewModel model, out string message);

        /// <summary>
        /// Get a CSS by cssID
        /// </summary>
        /// <param name="cssId">cssID to get CSS</param>
        /// <returns>CSS view model</returns>
        CSSViewModel GetCSSById(int cssId);

        /// <summary>
        /// Update the CSS
        /// </summary>
        /// <param name="model">CSS view model</param>
        /// <param name="message">error message if css is not created</param>
        /// <returns>boolean value true/false</returns>
        bool UpdateCSS(CSSViewModel model, out string message);

        /// <summary>
        /// Delete CSS
        /// </summary>
        /// <param name="cssId">CSS ID to delete CSS</param>
        /// <param name="message">error message if CSS is not deleted</param>
        /// <returns>boolean value true/false</returns>
        bool DeleteCSS(int cssId, out string message);
    }
}
