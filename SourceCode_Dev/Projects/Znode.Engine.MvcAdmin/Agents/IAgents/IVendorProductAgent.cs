﻿using System.Collections.Generic;
using System.Web.Mvc;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public interface IVendorProductAgent
    {
        /// <summary>
        /// To Get Vendor Product List
        /// </summary>
        /// <param name="filters">FilterCollection filters</param>
        /// <param name="sortCollection">SortCollection sortCollection</param>
        /// <param name="pageIndex">int? pageIndex</param>
        /// <param name="recordPerPage">int? recordPerPage</param>
        /// <returns>Return Vendor Product List.</returns>
        VendorProductListViewModel GetVendorProductList(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// To Get mall admin Product List
        /// </summary>
        /// <param name="filters">FilterCollection filters</param>
        /// <param name="sortCollection">SortCollection sortCollection</param>
        /// <param name="pageIndex">int? pageIndex</param>
        /// <param name="recordPerPage">int? recordPerPage</param>
        /// <returns>Return Vendor Product List.</returns>
        VendorProductListViewModel GetMallAdminProductList(FilterCollection fromFilters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);
       
        /// <summary>
        /// To Change the Product review state.
        /// </summary>
        /// <param name="model">VendorProductListViewModel model</param>
        /// <param name="state">ZNodeProductReviewState state</param>
        /// <returns>Return true or false</returns>
        bool ChangeProductStatus(VendorProductListViewModel model, ZNodeProductReviewState state);

        /// <summary>
        /// To Get Rejected Product details
        /// </summary>
        /// <param name="productIds">string productIds</param>
        /// <returns>Return rejected product details</returns>
        VendorProductListViewModel BindRejectProduct(string productIds);

        /// <summary>
        /// To Get Vendor Product image List
        /// </summary>
        /// <param name="filters">FilterCollection filters</param>
        /// <param name="sortCollection">SortCollection sortCollection</param>
        /// <param name="pageIndex">int? pageIndex</param>
        /// <param name="recordPerPage">int? recordPerPage</param>
        /// <returns>Return Product image List.</returns>
        ProductAlternateImageListViewModel GetReviewImageList(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// To Change the Product review state.
        /// </summary>
        /// <param name="productIds">string productIds</param>
        /// <param name="state">string state</param>
        /// <returns>Return true or false</returns>
        bool UpdateProductImageStatus(string productIds, string state);


        /// <summary>
        /// To Get Vendor Product Reviews History List
        /// </summary>
        /// <param name="filters">FilterCollection filters</param>
        /// <param name="sortCollection">SortCollection sortCollection</param>
        /// <param name="pageIndex">int? pageIndex</param>
        /// <param name="recordPerPage">int? recordPerPage</param>
        /// <returns>Return Product Review History List.</returns>
        ProductReviewHistoryListViewModel GetProductReviewHistory(int productId, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// To Get Review History details by Id
        /// </summary>
        /// <param name="productReviewHistoryID"></param>
        /// <returns>Return review history details in ProductReviewHistoryViewModel model</returns>
        ProductReviewHistoryViewModel GetProductReviewHistoryById(int productReviewHistoryID);

        /// <summary>
        /// To Update Vendor Product Details.
        /// </summary>
        /// <param name="model">VendorProductViewModel model</param>
        /// <param name="errorMessage"> out string errorMessage to handle exception if any</param>
        /// <returns>eturn true or false.</returns>
        bool UpdateVendorProduct(VendorProductViewModel model, out string errorMessage);

        /// <summary>
        /// To Get Vendor Name based on portal id or account id.
        /// </summary>
        /// <param name="portalId"></param>
        /// <param name="accountId"></param>
        /// <returns>Return vendor name.</returns>
        string GetVendorName(int? portalId, int? accountId);

        /// <summary>
        /// Get Vendor Product Marketing Details.
        /// </summary>
        /// <param name="productId">int productId</param>
        /// <returns>Return Product Marketing Details in VendorProductMarketingViewModel format.</returns>
        VendorProductMarketingViewModel GetVendorProductMarketingDetails(int productId);

        /// <summary>
        /// To Update the Vendor Product Marketing Details.
        /// </summary>
        /// <param name="model">VendorProductMarketingViewModel model</param>
        /// <returns>Return updated marketing view model.</returns>
        VendorProductMarketingViewModel UpdateMarketingDetails(VendorProductMarketingViewModel model);

        /// <summary>
        /// To Get Product details by productId
        /// </summary>
        /// <param name="productId">int productId</param>
        /// <returns>Return Product Details in VendorProductPreviewViewModel format</returns>
        VendorProductPreviewViewModel GetProduct(int productId);

        /// <summary>
        /// To Get the Product associated Category based on Product Id.
        /// </summary>
        /// <param name="productId">Id for the Product</param>
        /// <returns>Return Product associated Category List in ProductCategoryViewModel model</returns>
        List<ProductCategoryViewModel> GetProductCategory(int productId);

        /// <summary>
        /// To Get the Product Category Node based on Product Id.
        /// </summary>
        /// <param name="productId">Id for the Product</param>
        /// <returns>Return Product Category Node List in CategoryNodeListViewModel model</returns>
        CategoryNodeListViewModel GetCategoryNode(int productId);

        /// <summary>
        /// To Delete the product details based on productId.
        /// </summary>
        /// <param name="productId">Id for the product</param>
        /// <returns>Return true or false</returns>
        bool DeleteProduct(int productId);

        /// <summary>
        /// To Get Product Review State List.
        /// </summary>
        /// <returns>Return Product Review State List in ProductReviewStateListViewModel format</returns>
        ProductReviewStateListViewModel GetReviewStates();


        /// <summary>
        /// To bind vendor product page productType drop down list
        /// </summary>
        /// <param name="model">ProductViewModel model</param>
        /// <param name="portalId">int portalId</param>
        /// <returns>returns ProductViewModel with producttype list</returns>
        ProductViewModel BindProductPageDropdown(ProductViewModel model, int portalId);

        /// <summary>
        /// To save vendor product 
        /// </summary>
        /// <param name="model">VendorProductViewModel model</param>
        /// <param name="errorMessage">out string errorMessage</param>
        /// <returns>returns true/false</returns>
        bool SaveVendorProduct(VendorProductViewModel model, out string errorMessage);
    }
}
