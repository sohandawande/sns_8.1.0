﻿using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public interface IReviewAgent
    {
        /// <summary>
        /// To get Customer Review list
        /// </summary>
        /// <returns>Retuns Customer Review List.</returns>
        ReviewViewModel GetReviews(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// Get review based on review id.
        /// </summary>
        /// <param name="reviewId">review Id</param>
        /// <returns>Return review details</returns>
        ReviewItemViewModel GetReview(int reviewId);

        /// <summary>
        /// Method change the review status.
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Return true or false</returns>
        bool UpdateReviewStatus(ReviewItemViewModel model);

        /// <summary>
        /// Method update the review.
        /// </summary>
        /// <param name="model">ReviewItemViewModel</param>
        /// <returns>Return true or false</returns>
        bool UpdateReview(ReviewItemViewModel model);

        /// <summary>
        /// Method delete the review based on id.
        /// </summary>
        /// <param name="reviewId">review Id</param>
        /// <returns>Return true or false</returns>
        bool DeleteReview(int reviewId);

        /// <summary>
        /// Dropdown for Review Status.
        /// </summary>
        /// <returns>ReviewViewModel</returns>
        ReviewViewModel BindStatus();
    }
}
