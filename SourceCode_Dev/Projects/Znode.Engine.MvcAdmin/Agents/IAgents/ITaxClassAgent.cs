﻿using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    /// <summary>
    /// Interface for Tax Class Agent
    /// </summary>
    public interface ITaxClassAgent
    {
        /// <summary>
        /// Gets all tax class list.
        /// </summary>
        /// <param name="filters">Filter collection</param>
        /// <param name="sortCollection">Sort collection</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="recordPerPage">Records per page</param>
        /// <returns>Returns tax list in the model of type TaxClassListViewModel</returns>
        TaxClassListViewModel GetAllTaxClasses(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// Get Tax Class Details
        /// </summary>
        /// <param name="taxClassId">taxClassId</param>
        /// <param name="filters">Filter collection</param>
        /// <param name="sortCollection">Sort collection</param>
        /// <param name="pageIndex">Index of page</param>
        /// <param name="recordPerPage">Records per page</param>
        /// <returns>Returns Tax class details</returns>
        TaxClassViewModel GetTaxClassDetails(int taxClassId, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// Save Tax Class
        /// </summary>
        /// <param name="model">TaxClassViewModel model</param>
        /// <param name="taxId">out int taxId</param>
        /// <param name="message">message</param>
        /// <returns>Returns true/false</returns>
        bool SaveTaxClass(TaxClassViewModel model, out int taxId, out string message);       

        /// <summary>
        /// Update Tax Class
        /// </summary>
        /// <param name="model">TaxClassViewModel model</param>
        /// <param name="taxClassId">tax class id</param>
        /// <param name="message">message</param>
        /// <returns>Returns true/false</returns>
        bool UpdateTaxClass(TaxClassViewModel model, int taxClassId, out string message);
       
        /// <summary>
        /// Delete Tax Class
        /// </summary>
        /// <param name="taxClassId">tax class id</param>
        /// <param name="message">message</param>
        /// <returns>Returns true/false</returns>
        bool DeleteTaxClass(int taxClassId, out string message);       
    }
}
