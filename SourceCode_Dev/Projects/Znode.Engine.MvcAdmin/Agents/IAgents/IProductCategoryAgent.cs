﻿using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    interface IProductCategoryAgent
    {
        /// <summary>
        /// Get Product category on the basis of productCategoryId
        /// </summary>
        /// <param name="productCategoryId"></param>
        /// <returns></returns>
        ProductCategoryViewModel GetProductCategory(int productCategoryId); 
       
        /// <summary>
        /// Add new product associated to category on the basis of product Id and category Id
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        bool CreateProductCategory(ProductCategoryViewModel model);

        /// <summary>
        /// Update productassociated with Category on the basis of product Id
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        bool UpdateProductCategory(ProductCategoryViewModel model);

        /// <summary>
        /// Delete product associated with category on the basis of productId
        /// </summary>
        /// <param name="productCategoryId"></param>
        /// <returns></returns>
        bool DeleteProductCategory(int productCategoryId);
    }
}
