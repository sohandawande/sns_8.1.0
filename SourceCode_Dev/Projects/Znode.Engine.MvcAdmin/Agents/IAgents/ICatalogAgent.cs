﻿using System.Collections.Generic;
using System.Web.Mvc;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public interface ICatalogAgent
    {
        /// <summary>
        /// Gets the list of catalogs.      
        /// </summary>
        /// <param name="filters">Collection of filter</param>
        /// <param name="sortCollection">Collection of Sort</param>
        /// <param name="pageIndex">Index of page</param>
        /// <param name="recordPerPage">Records to be shown per page</param>
        /// <returns>Returns the list of catalogs.</returns>
        CatalogListViewModel GetCatalogs(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// Gets the list of catalogs by catalogId.
        /// </summary>
        /// <param name="catalogId">The Id of catalog.</param>
        /// <returns>Returns the list of catalogs by catalogId.</returns>
        CatalogViewModel GetCatalog(int catalogId);

        /// <summary>
        /// Creates a new catalog.
        /// </summary>
        /// <param name="model">The model of type CatalogViewModel.</param>
        /// <returns>Returns model of type CatalogViewModel.</returns>
        CatalogViewModel CreateCatalog(CatalogViewModel model);

        /// <summary>
        /// Creates a copy of an existing catalog.
        /// </summary>
        /// <param name="model">The model of type CatalogViewModel.</param>
        /// <returns>Returns true or false.</returns>
        bool CopyCatalog(CatalogViewModel model);
        
       /// <summary>
        /// Deletes an existing catalog.
       /// </summary>
        /// <param name="model">The model of type CatalogViewModel.</param>
       /// <param name="message">Return Message for showing result message</param>
       /// <returns>true / false</returns>
        bool DeleteCatalog(CatalogViewModel model, out string message);

        /// <summary>
        /// Updates the catalog model.
        /// </summary>
        /// <param name="catalogId">The id of the catalog</param>
        /// <param name="model">Model of type CatalogViewModel</param>
        /// <returns>Returns true or false.</returns>
        bool UpdateCatalog(int catalogId, CatalogViewModel model);

        /// <summary>
        /// Returns CatalogSelectList view model
        /// </summary>
        /// <param name="portalId"></param>
        /// <returns>Returns CatalogSelectListViewModel.</returns>
        CatalogSelectListViewModel GetCatalogSelectList(int portalId);

        /// <summary>
        /// List of available category profiles.
        /// </summary>
        /// <param name="categoryId">int CategoryId</param>
        /// <param name="categoryProfileId">int categoryProfileId</param>
        /// <param name="profileId">int profileId</param>
        /// <returns>Returns list of List<SelectListItem></returns>
        List<SelectListItem> GetCategoryProfilesList(int categoryId, int categoryProfileId = 0, int profileId = 0);

        /// <summary>
        /// Gets the category profiles data while edit.
        /// </summary>
        /// <param name="profileID">int profileID</param>
        /// <param name="categoryProfileID">int categoryProfileID</param>
        /// <returns>Returns CategoryProfileViewModel.</returns>
        CategoryProfileViewModel GetEditCategoryProfilesData(int profileID, int categoryProfileID, int categoryId);

        /// <summary>
        /// Gets the catalog details while manage.
        /// </summary>
        /// <param name="catalogId">int catalogId</param>
        /// <param name="categoryProfileId">int categoryProfileId</param>
        /// <returns>Returns CatalogViewModel.</returns>
        CatalogViewModel GetCatalogDetails(int catalogId, int categoryProfileId = 0);

        /// <summary>
        /// Binds associated category node data.
        /// </summary>
        /// <param name="catalogId">The id of catalog</param>
        /// <param name="categoryName">The name of category</param>       
        /// <param name="model">Mdoel of type CategoryNodeViewModel.</param>
        void BindAssociatedCategoryNodeData(int catalogId, string categoryName, CategoryNodeViewModel model);

        /// <summary>
        /// Posts category profile data.
        /// </summary>
        /// <param name="model">Model of type CategoryProfileViewModel</param>       
        /// <returns>Returns CategoryProfileViewModel.</returns>
        CategoryProfileViewModel PostCategoryProfileData(CategoryProfileViewModel model);

        /// <summary>
        /// Binds edit associate category data.
        /// </summary>
        /// <param name="categoryNodeId">The id of category node</param>
        /// <param name="categoryProfileId">The id of category profile</param>
        /// <param name="model">Model of type CategoryNodeViewModel</param>
        void BindEditAssociateCategoryData(int categoryNodeId, int categoryProfileId, CategoryNodeViewModel model, FilterCollectionDataModel formModel);

        /// <summary>
        /// Gets the associated category profiles data.
        /// </summary>
        /// <param name="categoryId">int category Id.</param>
        /// <returns>Returns CategoryProfileViewModel.</returns>
        CategoryProfileViewModel GetAssociateCategoryProfilesData(int categoryId);

        /// <summary>
        /// To get first or default Catalog by PortalId
        /// </summary>
        /// <param name="portalId">int portalId</param>
        /// <returns>Catalog Id</returns>
        int? GetCatalogByPortalId(int portalId);
    }
}
