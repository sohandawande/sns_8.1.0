﻿using System.Collections.Generic;
using System.Web.Mvc;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    /// <summary>
    /// Interface for ManageSEO Agent
    /// </summary>
    public interface IManageSEOAgent
    {

        /// <summary>
        /// To get prodtal default setting by portal id
        /// </summary>
        /// <param name="portalId">int portalId</param>
        /// <returns>returns default setting for portal</returns>
        ManageSEOViewModel GetPortalById(int portalId);

        /// <summary>
        /// Binds the portal dropdown list.
        /// </summary>
        /// <returns>return protal SelectListItem</returns>
        List<SelectListItem> BindPortalList();

        /// <summary>
        /// To get product seo details by productId
        /// </summary>
        /// <param name="productId">int productId</param>
        /// <returns>returns Product SEO Details</returns>
        ManageSEOProductViewModel GetProductSEODetails(int productId);

        /// <summary>
        /// To get update product seo details 
        /// </summary>
        /// <param name="viewmodel">ManageSEOProductViewModel viewmodel</param>
        /// <returns>returns true / false</returns>
        bool UpdateProductSEODetails(ManageSEOProductViewModel viewmodel);

        /// <summary>
        /// Updates the Content Page SEO Information.
        /// </summary>
        /// <param name="updatedModel">model to update.</param>       
        /// <param name="errorMessgae">error message.</param>
        /// <returns>Returns True or False.</returns>
        bool UpdateContentPageSEOInformation(ContentPageViewModel updatedModel, out string errorMessgae);

        /// <summary>
        /// To get category seo details
        /// </summary>
        /// <param name="categoryId">int categoryId</param>
        /// <returns>returns category seo details viewmodel</returns>
        ManageSEOCategoryViewModel GetCategory(int categoryId);
    }
}
