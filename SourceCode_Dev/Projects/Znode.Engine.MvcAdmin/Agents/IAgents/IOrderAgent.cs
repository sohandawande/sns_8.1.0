﻿using System.Collections.Generic;
using System.Data;
using System.Web.Mvc;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public interface IOrderAgent
    {
        /// <summary>
        /// Gets Order list from database using stored procedure.       
        /// </summary>
        /// <param name="filters">Filter collection</param>
        /// <param name="sortCollection">Sort collection</param>
        /// <param name="pageIndex">Index of page</param>
        /// <param name="recordPerPage">Records per page</param>
        /// <returns>Returns model of type AdminOrderListView model.</returns>
        AdminOrderListViewModel GetOrderList(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// Gets Order deatils from database using stored procedure.       
        /// </summary>
        /// <param name="filters">Filter collection</param>
        /// <param name="sortCollection">Sort collection</param>
        /// <param name="pageIndex">Index of page</param>
        /// <param name="recordPerPage">Records per page</param>
        /// <returns>Returns model of type OrderView model.</returns>
        OrderViewModel GetOrderDetails(int OrderId, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// Updates existing order status
        /// </summary>
        /// <param name="model">Model of type OrderView model</param>
        /// <param name="errorMessage">string error message</param>
        /// <returns>Returns true or false.</returns>
        bool UpdateOrderStatus(OrderViewModel model, out string errorMessage);

        /// <summary>
        /// Updates order with void payment.
        /// </summary>
        /// <param name="model">Model of type OrderView model</param>
        /// <returns>Returns model of type OrderView model</returns>
        OrderViewModel VoidPayment(OrderViewModel model);

        /// <summary>
        /// Updates order with refund payment.
        /// </summary>
        /// <param name="model">Model of type OrderView model</param>
        /// <returns>Returns model of type OrderView model</returns>
        OrderViewModel RefundPayment(OrderViewModel model);

        /// <summary>
        /// Downloads order data from db using sp.
        /// </summary>
        /// <param name="orderId">Starting order id</param>
        /// <param name="filterCollection">nullable FilterCollection fro mall admin</param>
        /// <returns>Returns Dataset.</returns>
        DataSet DownloadOrderData(string orderId, FilterCollection filterCollection = null);

        /// <summary>
        /// Downloads order line item data from db using sp.
        /// </summary>
        /// <param name="orderId">Starting order id</param>
        /// <param name="filterCollection">nullable FilterCollection fro mall admin</param>
        /// <returns>Returns Dataset.</returns>
        DataSet DownloadOrderLineItemData(string orderId, FilterCollection filterCollection = null);

        /// <summary>
        /// Gets the list of all store for binding dropdown.
        /// </summary>
        /// <returns> List<PortalViewModel></returns>
        List<PortalViewModel> GetAllStores();

        /// <summary>
        /// Gets the list of all order status for binding dropdown.
        /// </summary>
        /// <returns>List<OrderStateViewModel></returns>
        List<OrderStateViewModel> GetAllOrderStatusList();

        /// <summary>
        /// Binds dropdown with order status values.
        /// </summary>
        /// <returns>Retuns List<SelectListItem></returns>
        List<SelectListItem> BindOrderStatus(int orderStateId = 0);

        /// <summary>
        /// Binds month dropdown list.
        /// </summary>
        /// <returns>Returns List<SelectListItem></returns>
        List<SelectListItem> ToMonthSelectListItems();

        /// <summary>
        /// Binds year dropdown list.
        /// </summary>
        /// <returns>Returns List<SelectListItem></returns>
        List<SelectListItem> ToYearSelectListItems();

        /// <summary>
        /// Creates a new customer.
        /// </summary>
        /// <param name="model">Model of type ShippingBillingAddressViewModel</param>
        /// <returns>Return the Response in AddressListModel format.</returns>
        AddressListModel AddNewCustomer(ShippingBillingAddressViewModel model);

        /// <summary>
        /// To Get Product List.
        /// </summary>
        /// <param name="filters">Filter collection</param>
        /// <param name="sortCollection">Sort collection</param>
        /// <param name="pageIndex">Index of page</param>
        /// <param name="recordPerPage">Records per page</param>
        /// <returns>Returns model of type ProductListViewModel model.</returns>
        ProductListViewModel SearchProducts(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// To Get Order Product Details.
        /// </summary>
        /// <param name="productId">Id of the Product</param>
        /// <returns>Returns model of type ProductViewModel model.</returns>
        ProductViewModel GetOrderProduct(int productId);

        /// <summary>
        /// Sets the shipping and billing address of specific order.
        /// </summary>
        /// <param name="orderModel">Model of type OrderView model</param>
        void SetBillingShippingAddresses(OrderViewModel orderModel);

        /// <summary>
        /// Get order line item on the basis of order line item id.
        /// </summary>
        /// <param name="orderLineItemId">int id of order line item</param>
        /// <param name="filters">Filter collection</param>
        /// <param name="sortCollection">Sort collection</param>
        /// <param name="pageIndex">Index of page</param>
        /// <param name="recordPerPage">Records per page</param>
        /// <returns>Returns order line item on the basis of order line item id.</returns>
        OrderLineItemViewModel GetOrderLineItems(int orderLineItemId, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// Update order line item status on the basis of order line item id.
        /// </summary>
        /// <param name="model">Model of type OrderLineItemViewModel</param>
        /// <param name="errorMessage">Error Message</param>
        /// <returns>Returns true/false</returns>
        bool UpdateOrderLineItemStatus(OrderLineItemViewModel model, out string errorMessage);

        /// <summary>
        /// To update customer address.
        /// </summary>
        /// <param name="accountId">customer account id</param>
        /// <param name="viewModel">Model of type ShippingBillingAddressViewModel</param>
        /// <returns>Return the Response in AddressListModel format.</returns>
        AddressListModel UpdateCustomerAddress(int accountId, ShippingBillingAddressViewModel viewModel);

        /// <summary>
        /// To get Catalog Id By Portal Id
        /// </summary>
        /// <param name="portalId">int? portalId</param>
        /// <returns>returns Catalog Id for Portal Id</returns>
        int GetCatalogIdByPortalId(int? portalId);

        /// <summary>
        /// Sends email for the shipped status change and also tracking number of order line item.
        /// </summary>
        /// <param name="errorMessage">Error message if any exceptions gets thrown from service.</param>
        /// <param name="filters">Filter Collection to filter the data on the basis of order values passed.</param>
        /// <returns>Returns string message if the email was sent or not.</returns>
        string SendEmail(out string errorMessage, FilterCollection filters = null);

        /// <summary>
        /// Get Portals according to user name.
        /// </summary>
        /// <returns>Returns PortalSelectListViewModel</returns>
        PortalSelectListViewModel GetPortalSelectList();

        //PRFT Custom Code : Start
        /// <summary>
        /// Resubmit order to ERP 
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        bool ResubmitOrderToERP(int orderId, out string errorMessage);
        //PRFT Custom Code : End
    }
}
