﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public interface IReasonForReturnAgent
    {
        /// <summary>
        /// Get list of ReasonForReturn
        /// </summary>
        /// <param name="filters">Filter Collection</param>
        /// <param name="sortCollection">Sort Collection</param>
        /// <param name="pageIndex">Page Index</param>
        /// <param name="recordPerPage">countnumber of records to display</param>
        /// <returns>list of ReasonForReturn</returns>
        ReasonForReturnListViewModel GetReasonForReturnList(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// Create new ReasonForReturn
        /// </summary>
        /// <param name="model">View Model of ReasonForReturn</param>
        /// <returns>Boolean value true/false</returns>
        bool CreateReaosnForReturn(ReasonForReturnViewModel model);

        /// <summary>
        /// Update an existing ReasonForReturn
        /// </summary>
        /// <param name="model">View Model of ReasonForReturn</param>
        /// <returns>Boolean value true/false</returns>
        bool UpdateReaosnForReturn(ReasonForReturnViewModel model);

        /// <summary>
        /// Gets ReasonForReturn data by reasonForReturn Id
        /// </summary>
        /// <param name="reasonForReturnId">Id of ReasonForReturn</param>
        /// <returns>Returns model of type ReasonForReturnViewModel.</returns>
        ReasonForReturnViewModel GetReasonForReturn(int reasonForReturnId);

        /// <summary>
        /// Delete an existing Reason For Return
        /// </summary>
        /// <param name="reasonForReturnId">ReasonForReturn Id by which Reason For Return will delete</param>
        /// <returns>Boolean value true/false</returns>
        bool DeleteReaosnForReturn(int reasonForReturnId, out string message);
    }
}
