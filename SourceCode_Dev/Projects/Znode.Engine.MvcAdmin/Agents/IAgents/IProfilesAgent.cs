﻿using System.Collections.Generic;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public interface IProfilesAgent
    {
        /// <summary>
        /// Gets the list of all profiles.
        /// </summary>
        /// <param name="filters">Filter collection</param>
        /// <param name="sortCollection">Sort collection</param>
        /// <param name="pageIndex">Index of page</param>
        /// <param name="recordPerPage">Records to shown per page</param>
        /// <returns>Returns model of type profilelistview model.</returns>
        ProfileListViewModel GetListOfProfiles(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// Create a new profile.
        /// </summary>
        /// <param name="model">>Model of type ProfileView model</param>
        /// <param name="profileId">out int profileId</param>
        /// <returns>Returns true or false.</returns>
        bool CreateProfile(ProfileViewModel model, out int profileId);

        /// <summary>
        /// Gets profile data by profile Id
        /// </summary>
        /// <param name="profileId">Id of profile</param>
        /// <returns>Returns model of type ProfileViewModel.</returns>
        ProfileViewModel GetProfileByProfileId(int profileId);

        /// <summary>
        /// Updates an existing profile.
        /// </summary>
        /// <param name="model">Model of type ProfileView model</param>
        /// <returns>Returns true or false.</returns>
        bool UpdateProfile(ProfileViewModel model);

        /// <summary>
        /// Deletes profiles
        /// </summary>
        /// <param name="profileId">Id of profile</param>
        /// <returns>Returns true or false.</returns>
        bool DeleteProfile(int profileId);

        /// <summary>
        /// Get available portals by sku id.
        /// </summary>
        /// <param name="skuId">sku id.</param>
        /// <param name="categoryId">category id.</param>
        /// <param name="skuEffectiveProfileId">sku effective profile id.</param>
        /// <param name="categoryProfileId">category profile id</param>
        /// <returns>List of profiles.</returns>
        List<ProfileModel> GetAvailablePortalsBySkuIdCategoryId(int skuId = 0, int categoryId = 0, int skuEffectiveProfileId = 0, int categoryProfileId = 0);

        /// <summary>
        /// Get profile by portal id.
        /// </summary>
        /// <param name="filters">To get filtered record</param>
        /// <param name="sortCollection">To get sorted record</param>
        /// <param name="pageIndex">current page index</param>
        /// <param name="recordPerPage">total record per page</param>
        /// <returns>ProfileListViewModel</returns>
        ProfileListViewModel GetProfileByPortalId(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);
        }
}
