﻿using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    /// <summary>
    /// Interface for the Manufacturer or brands
    /// </summary>
    public partial interface IManufacturersAgent
    {
        /// <summary>
        /// This function returns the list of all manufacturers
        /// </summary>
        /// <returns>ManufacturersListViewModel list of ManufacturersViewModel</returns>s
        ManufacturersListViewModel GetManufacturers(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// This method will return the manufacturer based on manufacturer id
        /// </summary>
        /// <param name="manufacturerID">integer manufacturer id</param>
        /// <returns>returns edit view model of manufacturer</returns>
        ManufacturersViewModel GetManufacturerByID(int manufacturerID);

        /// <summary>
        /// This method will save the Manufacturer
        /// </summary>
        /// <param name="model">ManufacturersViewModel model</param>
        /// <returns>Returs true if manufacturer saved</returns>
        bool SaveManufacturer(ManufacturersViewModel model);

        /// <summary>
        /// This method will update the Manufacturer
        /// </summary>
        /// <param name="model">ManufacturersViewModel model</param>
        /// <returns>Returns true if manufacturer updated</returns>
        bool UpdateManufacturer(ManufacturersViewModel model);

        /// <summary>
        /// This method will delete the Manufacturer
        /// </summary>
        /// <param name="manufacturerID">integere ManufacturerID</param>
        /// <returns>Returs true if manufacturer deleted</returns>
        bool DeleteManufactuere(int manufacturerID);

    }
}