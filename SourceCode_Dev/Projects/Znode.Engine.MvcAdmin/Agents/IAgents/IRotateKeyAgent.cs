﻿
namespace Znode.Engine.MvcAdmin.Agents
{
    public interface IRotateKeyAgent
    {
        /// <summary>
        /// Generate a Rotation Key
        /// </summary>
        /// <param name="message">Returns Error Message</param>
        /// <returns>Boolean value true/false</returns>
        bool GenerateRotateKey(out string message);
    }
}
