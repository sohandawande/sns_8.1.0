﻿
namespace Znode.Engine.MvcAdmin.Agents
{
    public interface ISearchAgent
    {
        /// <summary>
        /// To Check whether provided Seo Url is restricted or not.
        /// </summary>
        /// <param name="seoUrl">string seoUrl</param>
        /// <returns>Return true or false.</returns>
        bool IsRestrictedSeoUrl(string seoUrl);
    }
}
