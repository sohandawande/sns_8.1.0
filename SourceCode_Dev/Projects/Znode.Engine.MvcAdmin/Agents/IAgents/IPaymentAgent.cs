﻿using System.Collections.Generic;
using System.Web.Mvc;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public interface IPaymentAgent
    {
        /// <summary>
        /// get all payment options list
        /// </summary>
        /// <returns>returns PaymentOptionListViewModel</returns>
        PaymentOptionListViewModel GetPaymentOptions(FilterCollectionDataModel model);

        /// <summary>
        /// Get payment option viewModel for display Add new payment option page
        /// </summary>
        /// <returns>Returns PaymentOptionViewModel</returns>
        PaymentOptionViewModel GetPaymentOptionViewData();

        /// <summary>
        /// Get all payment getways as list
        /// </summary>
        /// <returns>Returns PaymentGatewayListModel</returns>
        PaymentGatewayListModel GetPaymentGetways();

        /// <summary>
        /// Get all payment types as list
        /// </summary>
        /// <returns>Returns PaymentTypeListModel</returns>
        PaymentTypeListModel GetPaymentTypes();

        /// <summary>
        /// Add new payment option to database
        /// </summary>
        /// <param name="model">Payment option view model</param>
        /// <returns>Returns true/false</returns>
        PaymentOptionModel AddPaymentOption(PaymentOptionViewModel model, out string status);

        /// <summary>
        /// Delete payment option
        /// </summary>
        /// <param name="paymentOptionId">Payment option Id</param>
        /// <returns>Returns true/false</returns>
        bool DeletePaymentOption(int paymentOptionId, out string status);

        /// <summary>
        /// Get payment option by payment option Id
        /// </summary>
        /// <param name="paymentOptionId">Payment option Id</param>
        /// <returns>Returns PaymentOptionViewModel</returns>
        PaymentOptionViewModel GetPaymentOption(int paymentOptionId);

        /// <summary>
        /// Update payment option
        /// </summary>
        /// <param name="model">payment option view model</param>
        /// <returns></returns>
        bool UpdatePaymentOption(PaymentOptionViewModel model, out string status);

        /// <summary>
        /// Get Franchise Payment Options List
        /// </summary>
        /// <param name="filterModel">FilterCollectionDataModel filterModel</param>
        /// <returns>Returns Franchise Payment Options List</returns>
        PaymentOptionListViewModel GetFranchisePaymentOptions(FilterCollectionDataModel filterModel);

        /// <summary>
        /// Get profileId by portalId and set value of profileId in PaymentOptionViewModel
        /// </summary>
        /// <param name="portalId">int portalId</param>
        /// <param name="model">PaymentOptionViewModel model</param>
        /// <returns>Return PaymentOptionViewModel with updated profile Id</returns>
        PaymentOptionViewModel SetProfileIdByPortalId(int portalId, PaymentOptionViewModel model);

        List<SelectListItem> GetPaymentTypesListItems();

        List<SelectListItem> GetPaymentOptionListItems();

        /// <summary>
        /// To get all active payment Option
        /// </summary>
        /// <returns>returns list of payment options</returns>
        List<SelectListItem> GetActivePaymentOptionListItems(int? portalId = null);

        /// <summary>
        /// To get paymentgatway name by paymetoptionid
        /// </summary>
        /// <param name="paymetOptionId">int? paymetOptionId</param>
        /// <param name="portalId">int? portalId optional for admin</param>
        /// <returns>returns paymentgatway name</returns>
        string GetPaymentGatwayNameByPaymetOptionId(int? paymetOptionId, out int? paymentProfileId, int? portalId = null);

        /// <summary>
        /// To post PayPal request on Paypal server using Payment API
        /// </summary>
        /// <param name="model">PayPalPaymentModel model</param>
        /// <returns>returns PayPal Response</returns>
        string HttpPayPalPostRequest(PayPalPaymentModel model);

        /// <summary>
        /// This method will call the credit card payment API for order processing.
        /// </summary>
        /// <param name="model">PayPalPaymentModel model</param>
        /// <returns>returns payment Response Response</returns>
        string ProcessPayNow(PayPalPaymentModel model);     
    }
}
