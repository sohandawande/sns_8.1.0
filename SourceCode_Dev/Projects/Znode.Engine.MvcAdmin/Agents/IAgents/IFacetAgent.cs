﻿using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;
using Znode.Engine.MvcAdmin.Models;
using System.Collections.ObjectModel;

namespace Znode.Engine.MvcAdmin.Agents
{
    public interface IFacetAgent
    {
        /// <summary>
        /// To get facet groups list
        /// </summary>
        /// <returns>Retuns facet group List.</returns>
        FacetGroupListViewModel GetFacetGroups(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// Method Bind the default facet group information.
        /// </summary>
        /// <returns>Returns facet group information.</returns>
        FacetGroupViewModel BindFacetGroupInformation();

        /// <summary>
        /// Method gets associated categories based on catalog Id.
        /// </summary>
        /// <param name="catalogId"></param>
        /// <returns>Returns the categories.</returns>
        FacetGroupViewModel GetAssociatedCategories(int catalogId);

        /// <summary>
        /// Method Gets associated Categories based on catalog Id.
        /// </summary>
        /// <param name="catalogId"></param>
        /// <returns>Return the categories.</returns>
        ListViewModel BindAssociatedCategories(int catalogId, Collection<FacetGroupCategoryModel> facetGroupCategories = null);

        /// <summary>
        /// Method Create facet group, facet group categories.
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Returns true or false.</returns>
        bool CreateFacetGroup(FacetGroupViewModel model);

        /// <summary>
        /// Method Gets the facet group details by facet group Id.
        /// </summary>
        /// <param name="facetGroupId"></param>
        /// <returns>Return Facet group details.</returns>
        FacetGroupViewModel GetFacetGroup(int? facetGroupId);

        /// <summary>
        /// Method Updates the Facet Group details with associated categories.
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Returns true or false</returns>
        bool ManageFacetGroup(FacetGroupViewModel model);

        /// <summary>
        /// Method Deletes the Facet Group details with associated categories.
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Returns true or false</returns>
        bool DeleteFacetGroup(int facetGroupId);

        /// <summary>
        /// Method Create facet.
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Returns true or false.</returns>
        FacetViewModel CreateFacet(FacetViewModel model);

        /// <summary>
        /// Method Gets the facet details by facet Id.
        /// </summary>
        /// <param name="facetId"></param>
        /// <returns>Return Facet details.</returns>
        FacetViewModel GetFacet(int? facetId);

        /// <summary>
        /// Method Updates the Facet details.
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Returns updated facet model.</returns>
        FacetViewModel EditFacet(FacetViewModel model);

        /// <summary>
        ///  Method Deletes the Facet details..
        /// </summary>
        /// <param name="facetId"></param>
        /// <returns>Returns true or false</returns>
        bool DeleteFacet(int? facetId);


        /// <summary>
        /// To Get all the facets based on facet group Id.
        /// </summary>
        /// <param name="facetGroupId">int facetGroupId</param>
        /// <param name="formModel">FilterCollectionDataModel formModel</param>
        /// <returns></returns>
        FacetListViewModel GetFacetList(int facetGroupId, FilterCollectionDataModel formModel);
    }
}
