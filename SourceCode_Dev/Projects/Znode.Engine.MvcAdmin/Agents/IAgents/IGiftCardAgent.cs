﻿using System.Collections.Generic;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public interface IGiftCardAgent
    {
        /// <summary>
        /// Get GiftCards
        /// </summary>
        /// <param name="filters">FilterCollection filters</param>
        /// <param name="sortCollection">SortCollection sortCollection</param>
        /// <param name="pageIndex">Nullable int pageIndex</param>
        /// <param name="recordPerPage">Nullable int recordPerPage</param>
        /// <returns>Returns GiftCard list view model</returns>
        GiftCardListViewModel GetGiftCards(FilterCollection filters, SortCollection sortCollection, int? pageIndex, int? recordPerPage);
                
        /// <summary>
        /// Delete GiftCard 
        /// </summary>
        /// <param name="giftCardId">int giftCardId</param>
        /// <returns>Returs true or false</returns>
        bool DeleteGiftCard(int giftCardId);

        /// <summary>
        /// Get GiftCard by giftcardId
        /// </summary>
        /// <param name="GiftCardId">int giftCardId</param>
        /// <returns>Returns Giftcard view model</returns>
        GiftCardViewModel GetGiftCard(int giftCardId);

        /// <summary>
        /// Creates a gift card.
        /// </summary>
        /// <param name="model">Gift Card view Model.</param>
        /// <returns>Gift card view model.</returns>
        GiftCardViewModel Create(GiftCardViewModel model);

        /// <summary>
        /// Update GiftCard 
        /// </summary>
        /// <param name="model">GiftCardViewModel model</param>
        /// <returns>Returs true or false</returns>
        bool Update(GiftCardViewModel model);

        /// <summary>
        /// Check AccountId Valid or Not
        /// </summary>
        /// <param name="accountId">int accountId</param>
        /// <returns>Returs true or false</returns>
        bool IsAccountActive(int accountId);

        /// <summary>
        /// Get Next GiftCard Number
        /// </summary>
        /// <returns>Returns Giftcard view model</returns>
        GiftCardViewModel GetNextGiftCardNumber();

        /// <summary>
        /// Get All Portals
        /// </summary>
        /// <returns>Returns Giftcard view model</returns>
        GiftCardViewModel GetAllPortals();

        /// <summary>
        /// Add RMA Request item.
        /// </summary>
        /// <param name="rmaRequestItem">RMA request item view model.</param>
        /// <returns>Bool value if the RMA Request item is created or not.</returns>
        bool AddRMARequestItem(RMARequestItemViewModel rmaRequestItem);

        /// <summary>
        /// Updates an RMA Request.
        /// </summary>
        /// <param name="model">Gift card View model.</param>
        /// <param name="message">Output status message.</param>
        /// <returns>Returns true or false.</returns>
        bool UpdateRMA(GiftCardViewModel model, out string message);

        /// <summary>
        /// Bind Exclude Expired Dopdown for search
        /// </summary>
        /// <returns>Returns List of exclude expired view model</returns>
        List<ExcludeExpiredViewModel> GetExpirationStatus();

        /// <summary>
        /// Sets RMA Data for the gift card if it is RMA Mode.
        /// </summary>
        /// <param name="mode">Mode of the request.</param>
        /// <param name="item">RMA Request ID of the RMA Request items for which we will issue gift cards.</param>
        /// <param name="items">Comma seperated string of order line items for which gift card is to be issued.</param>
        /// <param name="qty">Comma seperated string of qunatities for the comma seperated order line items respectively.</param>
        /// <param name="amount">Total amount for the gift card. </param>
        /// <param name="model">Gift Card view model where the details will be saved.</param>
        void SetRMAData(string mode, int item, string items, string qty, decimal amount, GiftCardViewModel model);
    }
}