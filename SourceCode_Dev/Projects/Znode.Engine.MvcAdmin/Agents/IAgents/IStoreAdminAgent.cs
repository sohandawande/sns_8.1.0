﻿using System.Collections.Generic;
using System.Web;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    /// <summary>
    /// Interface for the Store Admin functionality
    /// </summary>
    public interface IStoreAdminAgent
    {
        /// <summary>
        /// Get the list of the Store Admins
        /// </summary>
        /// <param name="filters"></param>
        /// <param name="sortCollection"></param>
        /// <param name="pageIndex"></param>
        /// <param name="recordPerPage"></param>
        /// <returns></returns>
        StoreAdminListViewModel GetStoreAdmins(string roleName, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// This function returns the list of the Store Admin By ID
        /// </summary>
        /// <param name="storeAdminID">interger Store Admin ID</param>
        /// <returns></returns>
        StoreAdminViewModel GetStoreAdminByID(int storeAdminID);

        /// <summary>
        /// This method will add the Store Admin
        /// </summary>
        /// <param name="model">StoreAdminViewModel model</param>
        /// <param name="accountId">out int accountId</param>
        /// <param name="errorMessage">out string errorMessage</param>
        /// <returns></returns>
        bool SaveStoreAdmin(StoreAdminViewModel model, out int accountId, out string errorMessage);

        /// <summary>
        /// This method will update the store admin
        /// </summary>
        /// <param name="model">StoreAdminViewModel model</param>
        /// <returns></returns>
        bool UpdateStoreAdmin(int accountId, StoreAdminViewModel model);

        /// <summary>
        /// This method will delete the store admin
        /// </summary>
        /// <param name="storeAdminID">int Store Admin ID</param>
        /// <param name="errorMessage">out string errorMessage</param>
        /// <returns></returns>
        bool DeleteStoreAdmin(int storeAdminID, out string errorMessage);

        /// <summary>
        /// This method will return list of portals/stores
        /// </summary>
        /// <returns></returns>
        List<PortalViewModel> GetAllStores();

        /// <summary>
        /// This method will return list of profiles
        /// </summary>
        /// <returns></returns>
        List<ProfileViewModel> GetAllProfiles();

        /// <summary>
        /// This method will disable the selected store admin
        /// </summary>
        /// <param name="storeAdminID">int store admin id</param>
        /// <returns></returns>
        bool EnableDisableStoreAdmin(int storeAdminID);

        /// <summary>
        /// This method will check the same user is logged in or not
        /// </summary>
        /// <param name="storeAdminID">int Account ID</param>
        /// <param name="currentUserName">string Current user name</param>
        /// <returns>True if current user</returns>
        bool IsCurrentUser(int storeAdminID, string currentUserName);

        /// <summary>
        /// This method will download the file
        /// </summary>
        /// <param name="model"></param>
        void DownloadFile(StoreAdminListViewModel model, HttpResponseBase response, string fileName);
    }
}
