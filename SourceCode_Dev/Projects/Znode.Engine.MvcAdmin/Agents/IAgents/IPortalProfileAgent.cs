﻿using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public interface IPortalProfileAgent
    {
        /// <summary>
        /// Creates a portal profile.
        /// </summary>
        /// <param name="portalProfileViewModel">portalProfileViewModel</param>
        /// <returns>True or false.</returns>
        bool CreatePortalProfile(PortalProfileViewModel portalProfileViewModel);

        /// <summary>
        /// Deletes portal profile.
        /// </summary>
        /// <param name="portalProfileId">portalProfileId</param>
        /// <returns>True or false.</returns>
        bool DeletePortalProfile(int portalProfileId);

        /// <summary>
        /// Binds portal profile information.
        /// </summary>
        /// <param name="portPortalalProfileViewModel">Portal profile view model.</param>
        void BindPortalProfileInformation(PortalProfileViewModel portalProfileViewModel, int portalId);

        /// <summary>
        /// Binds Portal's existing profile information.
        /// </summary>
        /// <param name="profileId">Profile Id.</param>
        /// <param name="portalProfileViewModel">Portal profile view model.</param>
        void BindExistingPortalProfileInformation(int profileId, PortalProfileViewModel portalProfileViewModel);

        /// <summary>
        /// Gets portal Profile
        /// </summary>
        /// <param name="portalProfileId">Portal profile id.</param>
        /// <returns></returns>
        PortalProfileViewModel GetPortalProfile(int portalProfileId);

        /// <summary>
        /// Update portal profile.
        /// </summary>
        /// <param name="portalProfileViewModel">Portal profile view model.</param>
        /// <returns>True or false.</returns>
        bool UpdatePortalProfile(PortalProfileViewModel portalProfileViewModel);

        /// <summary>
        /// Get Portal profiles
        /// </summary>
        /// <param name="portalId">Portal id.</param>
        /// <param name="filterCollection">Fileter Collection.</param>
        /// <param name="sortCollection">Sort Collection.</param>
        /// <param name="pageIndex">Page index.</param>
        /// <param name="recordPerPage">Record per page.</param>
        /// <returns>Portal list view model.</returns>
        PortalProfileListViewModel GetPortalProfiles(int portalId, FilterCollection filterCollection = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);
    }
}
