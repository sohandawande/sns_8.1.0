﻿using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public interface IProfileCommonAgent
    {
        /// <summary>
        /// To Get Store Access list based on user name.
        /// </summary>
        /// <param name="userName">User name of the user.</param>
        /// <returns>Return Stored Access List.</returns>
        ProfileCommonViewModel GetProfileStoreAccess(string userName);
    }
}
