﻿using System.Collections.Generic;
using System.Web.Mvc;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public interface IEmailTemplateAgent
    {
        /// <summary>
        /// Get list of all templates.
        /// </summary>
        /// <param name="filters">FilterCollection</param>
        /// <param name="sortCollection">SortCollection</param>
        /// <param name="pageIndex">index of template page</param>
        /// <param name="recordPerPage">size of template page</param>
        /// <returns>EmailTemplateListViewModel</returns>
        EmailTemplateListViewModel GetTemplateList(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// Create email template.
        /// </summary>
        /// <param name="model">EmailTemplateViewModel</param>
        /// <param name="errorMessage">error message</param>
        /// <returns>true / false</returns>
        bool AddEmailTemplate(EmailTemplateViewModel model, out string errorMessage);

        /// <summary>
        /// Get template details by template name.
        /// </summary>
        /// <param name="templateName">name of template</param>
        /// <param name="extension">extension of template</param>
        /// <returns>EmailTemplateViewModel</returns>
        EmailTemplateViewModel GetTemplatePage(string templateName, string extension);

        /// <summary>
        /// Update existing template.
        /// </summary>
        /// <param name="model">EmailTemplateViewModel</param>
        /// <param name="errorMessgae">Error message</param>
        /// <returns></returns>
        bool UpdateTemplatePage(EmailTemplateViewModel model, out string errorMessgae);

        /// <summary>
        /// Delete Existing Template Page.
        /// </summary>
        /// <param name="templateName">name of template</param>
        /// <param name="extension">extension of template</param>
        /// <param name="errorMessage">error message</param>
        /// <returns>true or false</returns>
        bool DeleteTemplatePage(string templateName,string extension, out string errorMessage);

        /// <summary>
        /// Get deleted templates.
        /// </summary>
        /// <returns>SelectListItem type of list</returns>
        List<SelectListItem> GetAllDeletedTemplates();

        /// <summary>
        /// Get Html content by template name
        /// </summary>
        /// <param name="templateName">name of template file.</param>
        /// <returns>EmailTemplateViewModel</returns>
        EmailTemplateViewModel GetHtmlContent(string templateName);

    }
}