﻿using System.Collections.Generic;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public interface ICaseRequestAgent
    {
        /// <summary>
        /// To get CaseRequest list
        /// </summary>
        /// <param name="filters">FilterCollection filters</param>
        /// <param name="sortCollection">SortCollection sortCollection</param>
        /// <param name="pageIndex">int? pageIndex</param>
        /// <param name="recordPerPage">int? recordPerPage</param>
        /// <returns>returns list of CaseRequest</returns>
        CaseRequestListViewModel GetCaseRequests(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// To get CaseRequests by id
        /// </summary>
        /// <param name="caseRequestId">int caseRequestId</param>
        /// <returns>returns CaseRequest model</returns>
        CaseRequestViewModel GetCaseRequest(int caseRequestId);

        /// <summary>
        /// To Save Case Request
        /// </summary>
        /// <param name="model">CaseRequestViewModel model</param>
        /// <returns>returns CaseRequestViewModel</returns>
        bool SaveCaseRequest(CaseRequestViewModel model);

        /// <summary>
        /// To update Case Request
        /// </summary>
        /// <param name="model">CaseRequestViewModel model</param>
        /// <returns>returns CaseRequestViewModel</returns>
        bool UpdateCaseRequest(CaseRequestViewModel model);

        /// <summary>
        /// To delete Case Request
        /// </summary>
        /// <param name="caseRequestId">int caseRequestId</param>
        /// <returns>returns true/false</returns>
        bool DeleteCaseRequest(int caseRequestId);

        /// <summary>
        /// To save Case Requests notes
        /// </summary>
        /// <param name="model">NotesViewModel model</param>
        /// <returns>returns NotesViewModel</returns>
        bool SaveCaseRequestsNotes(NotesViewModel model);

        /// <summary>
        /// To get all Catalog List
        /// </summary>
        /// <returns>returns List<CatalogModel></returns>
        List<CatalogModel> GetCatalogList();

        /// <summary>
        /// To get all Case Status List
        /// </summary>
        /// <returns></returns>
        List<CaseStatusModel> GetCaseStatusList();

        /// <summary>
        /// To bind page dropdown for create/edit
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        CaseRequestViewModel BindPageDropdown(CaseRequestViewModel model);

        /// <summary>
        /// To Get Notes By Case RequestId
        /// </summary>
        /// <param name="caseRequestId">int caseRequestId</param>
        /// <returns>returns NotesListViewModel</returns>
        NotesListViewModel GetNotesByCaseRequestId(int caseRequestId);

        /// <summary>
        /// To Get Notes By account id.
        /// </summary>
        /// <param name="accountId">id of the account</param>
        /// <returns>returns NotesListViewModel</returns>
        NotesListViewModel GetNotesByAccountId(int accountId);

        /// <summary>
        /// To ReplyCustomer
        /// </summary>
        /// <param name="model">CaseRequestViewModel model</param>
        /// <param name="ErrorMessage">out string ErrorMessage</param>
        /// <returns>returns true/false</returns>
        bool ReplyCustomer(CaseRequestViewModel model, out string ErrorMessage);

    }
}
