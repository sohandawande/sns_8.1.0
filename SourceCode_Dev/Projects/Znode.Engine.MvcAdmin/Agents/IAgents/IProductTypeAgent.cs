﻿using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    /// <summary>
    /// Interface for the Product Type Agent
    /// </summary>
    public interface IProductTypeAgent
    {
        /// <summary>
        /// Get Product Types List.
        /// </summary>
        /// <returns>Returns ProductTypeListViewModel</returns>
        ProductTypeListViewModel GetProductTypes(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// Get existing Product Type.
        /// </summary>
        /// <param name="productTypeId">productTypeId</param>
        /// <returns>Returns ProductTypeViewModel</returns>
        ProductTypeViewModel GetProductType(int productTypeId);
        
        /// <summary>
        /// Create new Product Type.
        /// </summary>
        /// <param name="viewModel">ProductTypeViewModel</param>
        /// <param name="message">Error message</param>
        /// <returns>true / false</returns>
        bool CreateProductType(ProductTypeViewModel viewModel, out string message);

        /// <summary>
        /// Delete an existing  Product Type.
        /// </summary>
        /// <param name="productTypeId">productTypeId</param>
        /// <returns>Return bool</returns>
        bool DeleteProductType(int productTypeId, out string errorMessage);

        /// <summary>
        /// Update existing Product Type.
        /// </summary>
        /// <param name="viewModel">ProductTypeViewModel</param>
        /// <param name="message">Error message</param>
        /// <returns>true/false</returns>
        bool UpdateProductType(ProductTypeViewModel viewModel, out string message);
    }
}