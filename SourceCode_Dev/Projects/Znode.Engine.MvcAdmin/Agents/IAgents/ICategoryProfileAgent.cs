﻿using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public interface ICategoryProfileAgent
    {
        /// <summary>
        /// Gets category profile by category profile id.
        /// </summary>
        /// <param name="categoryProfileId">The id of category profile</param>
        /// <returns>Returns model of type CategoryProfileViewModel.</returns>
        CategoryProfileViewModel GetCategoryProfileByCategoryProfileId(int categoryProfileId = 0);

        /// <summary>
        /// Creates category profile.
        /// </summary>
        /// <param name="model">Model of type CategoryProfileViewModel</param>
        /// <returns>Returns model of type CategoryProfileViewModel.</returns>
        CategoryProfileViewModel CreateCategoryProfile(CategoryProfileViewModel model);

        /// <summary>
        /// Gets category profile by category id.
        /// </summary>
        /// <param name="categoryId">the id of category</param>
        /// <returns>Returns model of type CategoryProfileListViewModel.</returns>
        CategoryProfileListViewModel GetCategoryProfileByCategoryId(int categoryId = 0);

        /// <summary>
        /// This method will update the data of the Category Profile
        /// </summary>
        /// <param name="categoryProfileId">int CategoryProfileId</param>
        /// <param name="model">CategoryProfileListViewModel mode</param>
        /// <returns>Returns True if data updated</returns>
        bool UpdatecategoryProfile(int categoryProfileId, CategoryProfileViewModel model);

        /// <summary>
        /// This method will delete the data of the Category Profile
        /// </summary>
        /// <param name="categoryProfileId">int CategoryProfileId</param>
        /// <returns>Returns True if data deleted</returns>
        bool DeleteCategoryProfile(int categoryProfileId);
    }
}
