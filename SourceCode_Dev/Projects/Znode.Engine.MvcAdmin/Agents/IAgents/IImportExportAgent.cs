﻿using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public interface IImportExportAgent
    {
        /// <summary>
        /// To Get the Download File Type Formats.
        /// </summary>
        /// <returns>Return the File Types in List<SelectListItem> format</returns>
        List<SelectListItem> GetFileTypes();

        /// <summary>
        /// To Get Export Details.
        /// </summary>
        /// <param name="model">Model of the ExportViewModel</param>
        /// <returns>Return Export Details in ExportViewModel format.</returns>
        ExportViewModel GetExportData(ExportViewModel model);

        /// <summary>
        /// To Get the Default Export Details.
        /// </summary>
        /// <param name="filter">Keys for the Export Type</param>
        /// <returns>Return the Export Details in ExportViewModel format</returns>
        ExportViewModel GetDefaultExportDetails(string filter);

        /// <summary>
        /// To Get Attribute Types based on Catalog Id.
        /// </summary>
        /// <param name="catalogId">Id of the Catalog</param>
        /// <returns>Return the Attribute Types in List<SelectListItem> format</returns>
        List<SelectListItem> GetAttributeTypes(int catalogId);

        /// <summary>
        /// To Get the Categories based on Catalog Id.
        /// </summary>
        /// <param name="catalogId">Id of the Catalog</param>
        /// <returns>Return the Categories in List<SelectListItem> format</returns>
        List<SelectListItem> GetCategories(int catalogId);

        /// <summary>
        /// To Get Product Details based on Product Name & Catalog Id.
        /// </summary>
        /// <param name="searchText">Product Name</param>
        /// <param name="catalogId">Id of the Catalog</param>
        /// <returns>Return the Product Details in List<ProductViewModel> format</returns>
        List<ProductViewModel> GetProductDetailsByName(string searchText, int catalogId);

        /// <summary>
        /// To Save the Uploaded file Data.
        /// </summary>
        /// <param name="model">Model of the ImportViewModel</param>
        /// <returns>Return Imported data status in ImportViewModel Format</returns>
        ImportViewModel ImportData(ImportViewModel model);

        /// <summary>
        /// To Get the Upload File Data in Preview Mode.
        /// </summary>
        /// <param name="model">Model of Type ImportViewModel</param>
        /// <returns>Return Uploaded File data in ImportViewModel Format.</returns>
        ImportViewModel PreviewImportData(ImportViewModel model);

        /// <summary>
        /// To Get Import Template File Path.
        /// </summary>
        /// <param name="type">Type of the import Template</param>
        /// <returns>Return the Template File Path.</returns>
        string GetImportTemplateFilePath(string type);

        /// <summary>
        /// To Get Import Type Details based on template type.
        /// </summary>
        /// <param name="type">Type of the import Template</param>
        /// <returns>Return Import Type Details in ImportViewModelFormat</returns>
        ImportViewModel GetImportDefaultData(string type);
    }
}
