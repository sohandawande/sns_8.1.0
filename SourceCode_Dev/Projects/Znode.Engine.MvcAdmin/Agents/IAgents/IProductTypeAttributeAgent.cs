﻿using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public interface IProductTypeAttributeAgent
    {
        /// <summary>
        /// Gets categories by catalog Id.
        /// </summary>
        /// <param name="productTypeAttributeId">The Id of catalog.</param>
        /// <returns>Returns categories by catalog Id.</returns>
        ProductTypeAssociatedAttributeTypesListViewModel GetAttributesByProductTypeId(int productTypeAttributeId, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// To Check is Product Type Associated with Product.
        /// </summary>
        /// <param name="productType">productType</param>
        /// <returns>Returns True/False.</returns>
        bool IsProductAssociatedProductType(int productType);

        /// <summary>
        /// To Delete Existing Attribute Type.
        /// </summary>
        /// <param name="attributeTypeId">attributeTypeId</param>
        /// <returns>Returns True/False.</returns>
        bool DeleteAttributeType(int attributeTypeId);

        /// <summary>
        /// To check is any Attribute is asccoiated with Product Type.
        /// </summary>
        /// <param name="productTypeId">productTypeId</param>
        /// <returns></returns>
        bool IsAttributeAssociatedProductType(int productTypeId);

        /// <summary>
        /// Get List of Attribute Types.
        /// </summary>
        /// <returns>Returns AttributeTypesListViewModel</returns>
        AttributeTypesListViewModel GetAttributeTypes();

        /// <summary>
        /// To Create Product Type.
        /// </summary>
        /// <param name="viewModel">ProductTypeAssociatedAttributeTypesViewModel</param>
        /// <returns>Returns True/False.</returns>
        bool CreateProductType(ProductTypeAssociatedAttributeTypesViewModel viewModel, out string message);
        
    }
}