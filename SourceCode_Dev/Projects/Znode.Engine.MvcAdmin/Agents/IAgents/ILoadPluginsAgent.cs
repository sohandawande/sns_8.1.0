﻿
namespace Znode.Engine.MvcAdmin.Agents.IAgents
{
    public interface ILoadPluginsAgent
    {
        /// <summary>
        /// Load Plugins
        /// </summary>
        /// <returns>Returns Boolean result</returns>
        bool IsPluginsLoaded();
    }
}
