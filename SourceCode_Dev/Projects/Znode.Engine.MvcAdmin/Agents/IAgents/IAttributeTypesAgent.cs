﻿using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    /// <summary>
    /// Interface for the AttributeType
    /// </summary>
    public interface IAttributeTypesAgent
    {
        /// <summary>
        ///To Get all Attributes Type list
        /// </summary>
        /// <param name="filters">FilterCollection filters</param>
        /// <param name="sortCollection">SortCollection sortCollection</param>
        /// <param name="pageIndex">Nullable int pageIndex</param>
        /// <param name="recordPerPage">Nullable int recordPerPage</param>
        /// <returns>Return Attribute Type List</returns>
        AttributeTypesListViewModel GetAttributesType(FilterCollection filters, SortCollection sortCollection, int? pageIndex, int? recordPerPage);
       
        /// <summary>
        /// To Get all Attributes Type list by attributeTypeId
        /// </summary>
        /// <param name="attributeTypeId"></param>
        /// <returns></returns>
        AttributeTypesViewModel GetAttributeTypeById(int attributeTypeId);

        /// <summary>
        /// To save Attributes Type 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        bool SaveAttributeType(AttributeTypesViewModel model);

        /// <summary>
        /// To update Attributes Type 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        bool UpdateAttributeType(AttributeTypesViewModel model);

        /// <summary>
        /// To delete Attributes Type by attributeTypeId
        /// </summary>
        /// <param name="attributeTypeId"></param>
        /// <returns></returns>
        bool DeleteAttributeType(int attributeTypeId, out string errorMessage);

        /// <summary>
        /// To Get All Attribute Types.
        /// </summary>
        /// <returns></returns>
        AttributeTypesViewModel GetAttributeTypeList();
    }
}
