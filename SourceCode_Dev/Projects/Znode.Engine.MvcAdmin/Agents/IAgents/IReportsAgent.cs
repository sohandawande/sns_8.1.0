﻿using System.Collections.Generic;
using System.Web.Mvc;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.MvcAdmin.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents.IAgents
{
    /// <summary>
    /// Interface for Report Agent
    /// </summary>
    public interface IReportsAgent
    {
        /// <summary>
        /// Get Report Data set from API by Report name
        /// </summary>
        /// <param name="reportno">Report Enum Number</param>
        /// <param name="filters">Filter collection</param>
        /// <param name="sortCollection">Sort collection</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="recordPerPage">Page Size</param>
        /// <returns>Returns the entire reports on the basis of reportno</returns>
        ReportDataViewModel GetReportData(int reportno, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null, int mode=0);

        /// <summary>
        /// Get X and Y Axis of graph for display report in graph.
        /// </summary>
        /// <param name="reportModel">Report Model</param>
        void GetGraphAxis(ReportModel reportModel);

        /// <summary>
        /// Method to get list of export file type.
        /// </summary>
        /// <returns>Returns list of export file type.</returns>
        List<SelectListItem> GetFileTypes();
    }
}
