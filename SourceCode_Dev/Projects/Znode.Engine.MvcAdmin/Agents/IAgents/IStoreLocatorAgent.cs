﻿using System.Collections.Generic;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    /// <summary>
    /// Interface for Store Locator
    /// </summary>
    public interface IStoreLocatorAgent 
    {
        /// <summary>
        /// This method will save the Store.
        /// </summary>
        /// <param name="model">StoreLocatorViewModel model</param>
        /// <returns>Returs true if Store saved</returns>
        bool SaveStore(StoreLocatorViewModel model);

        /// <summary>
        /// This method will return the store based on store id
        /// </summary>
        /// <param name="storeId">integer store id</param>
        /// <returns>returns edit view model of store</returns>
        StoreLocatorViewModel GetStoreLocatorByID(int storeId);


        /// <summary>
        /// This method will update existing store locator.
        /// </summary>
        /// <param name="model">StoreLocatorViewModel</param>
        /// <returns>Returs true if Store updated</returns>
        bool UpdateStoreLocator(StoreLocatorViewModel model);

        /// <summary>
        /// This method will delete existing store locator .
        /// </summary>
        /// <param name="storeId">store Id</param>
        /// <returns>Returs true if Store deleted</returns>
        bool DeleteStoreLocator(int storeId);

        /// <summary>
        /// Gets the list of store locator.
        /// </summary>
        /// <param name="filters">filters</param>
        /// <param name="sortCollection">sort collection</param>
        /// <param name="pageIndex">page index</param>
        /// <param name="recordPerPage">record per page</param>
        /// <returns>StoreLocatorListViewModel</returns>
        StoreLocatorListViewModel GetStoreLocators(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// Sets the property of store locator.
        /// </summary>
        /// <param name="model">model to sets the property.</param>
        void SetStoreLocatorProperty(StoreLocatorViewModel model);

        /// <summary>
        /// Gets the list of store locator.
        /// </summary>
        /// <returns>List PortalViewModel</returns>
        List<PortalViewModel> GetPortalsList();
    }
}