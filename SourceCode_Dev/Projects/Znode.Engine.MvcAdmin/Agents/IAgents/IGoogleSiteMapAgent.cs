﻿using System.Collections.Generic;
using System.Web.WebPages.Html;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    /// <summary>
    /// Interface for Google Site Map Agent
    /// </summary>
    public interface IGoogleSiteMapAgent
    {
        /// <summary>
        /// Method to get list of Frequency
        /// </summary>
        /// <returns>Returns List<SelectListItem></returns>
        List<SelectListItem> GetFrequencyList();

        /// <summary>
        /// Method to get list of Priority 
        /// </summary>
        /// <returns>Returns List<SelectListItem></returns>
        List<SelectListItem> GetPriorityList();

        /// <summary>
        /// Method to get list of XML Site Map
        /// </summary>
        /// <returns>Returns List<SelectListItem></returns>
        List<SelectListItem> GetXMLSiteMapList();

        /// <summary>
        /// Method to get list of Last Modification date 
        /// </summary>
        /// <returns>Returns List<SelectListItem></returns>
        List<SelectListItem> GetLastDateList();

        /// <summary>
        /// Method to get list of XML Site Map Type
        /// </summary>
        /// <returns>Returns List<SelectListItem></returns>
        List<SelectListItem> GetXMLSiteMapTypeList();

        /// <summary>
        /// Commomn method to get all lists
        /// </summary>
        /// <returns>Returns GoogleSiteMapViewModel</returns>
        GoogleSiteMapViewModel BindViewModel();

        /// <summary>
        /// Creates Google Site Map for generating XML file
        /// </summary>
        /// <param name="model">Model of type GoogleSiteMapViewModel</param>
        /// <param name="domainName">Domain Name</param>
        /// <param name="message">Error Message</param>
        /// <returns>Returns GoogleSiteMapViewModel</returns>
        GoogleSiteMapViewModel CreateGoogleSiteMap(GoogleSiteMapViewModel model, string domainName, out string message);
    }
}
