﻿using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    /// <summary>
    /// This  will be  the interface for the Permissioin agent
    /// </summary>
    public interface IPermissionAgent
    {
        /// <summary>
        /// This method will fetch the details of Roles and Permissions based on the Account Id
        /// </summary>
        /// <param name="storeAdminID"></param>
        /// <returns></returns>
        PermissionViewModel GetAllRolesAndPermissionsByAccountId(int storeAdminID);

        /// <summary>
        /// This method will update the Roles and Permissions based on the Account Id
        /// </summary>
        /// <param name="StoreIds">int array for Store Ids</param>
        /// <param name="RoleIds">int array for Role Ids</param>
        /// <param name="AccountId">int Account Id</param>
        /// <returns>True if changes updated in Db</returns>
        bool UpdateRolesAndPermissions(int[] StoreIds, int[] RoleIds, int AccountId);
    }
}
