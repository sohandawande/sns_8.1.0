﻿using System.Collections.Generic;
using System.Web.Mvc;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public interface IPersonalizationAgent
    {
        /// <summary>
        /// List of products with frequently bought products.
        /// </summary>
        /// <param name="portalId">Portal Id according to which product list is to be retrieved.</param>
        /// <param name="categoryId">Category id according to which product list is to be retrieved.</param>
        /// <param name="crossSellType">Cross sell type of the product list.</param>
        /// <param name="filters">Filters for list of products with frequently bought products.</param>
        /// <param name="sortCollection">Sorting for list of products with frequently bought products.</param>
        /// <param name="pageIndex">Page Index for list of products with frequently bought products.</param>
        /// <param name="recordPerPage">Record per page count list of products with frequently bought products.</param>
        /// <returns>Returns list of products with frequently bought products.</returns>
        ProductCrossSellListViewModel GetProductListWithAssociatedItems(int portalId, int categoryId, int crossSellType, FilterCollection filters, SortCollection sortCollection, int? pageIndex, int? recordPerPage);

        /// <summary>
        /// Binds cross sell products.
        /// </summary>
        /// <param name="productId">Product id.</param>
        /// <param name="productList">Product list to be filtered for assigned adn unassigned.</param>
        /// <param name="relationTypeId">Relation type of products.</param>
        /// <returns>ListViewModel of cross sell products.</returns>
        ListViewModel BindCrossSellProducts(int productId, ProductCrossSellListViewModel productList, int relationTypeId);

        /// <summary>
        /// Delete Cross Sell Products
        /// </summary>
        /// <param name="listModel">CrossSell list model</param>
        /// <returns>boolean value true/false</returns>
        bool DeleteCrossSellProducts(int productId, int relationTypeId);

        /// <summary>
        /// Gets Cross sell products for a given product and relation type id.
        /// </summary>
        /// <param name="productId">Product id for which cross sell items has to be retrieved.</param>
        /// <param name="relationTypeId">Relation typ id of the associated products.</param>
        /// <returns>Product cross sell list items.</returns>
        ProductCrossSellListViewModel GetCrossSellProducts(int productId, int relationTypeId);

        /// <summary>
        /// Gets the list of portals with selected portal.
        /// </summary>
        /// <param name="portalId">portal id to set the selected.</param>
        /// <returns>Returns the list of portals.</returns>
        List<SelectListItem> BindPortalDropDownList(string portalId);

        /// <summary>
        /// Gets cross sell list.
        /// </summary>
        /// <param name="portalId">Portal ID according to which cross sell items can be fetched.</param>
        /// <param name="categoryId">Category ID according to which cross sell will be fetched,</param>
        /// <param name="productRelation">Product relation type of the proucts.</param>
        /// <returns>Personalization model which contains products along with cross sell items.</returns>
        PersonalizationViewModel GetCrossSellList(int portalId, int categoryId, int productRelation);

        /// <summary>
        /// Gets Personalization view model.
        /// </summary>
        /// <param name="productId">Personalization view model for this product ID.</param>
        /// <param name="portalId">Personalization view model for this portal ID.</param>
        /// <param name="categoryId">Personalization view model for this category ID.</param>
        /// <param name="productName">Personalization view model with this product Name.</param>
        /// <param name="personalizationModel">Personalization view model where the values will be assigned</param>
        /// <param name="productList"></param>
        PersonalizationViewModel GetPersonalizationModel(int? productId, int? portalId, int? categoryId, string productName, string sku, out ProductCrossSellListViewModel productList);

        /// <summary>
        /// Bindscross sell data on manage page.
        /// </summary>
        /// <param name="model">Personalization model where data is bind.</param>
        /// <param name="productCrossSellType">Type of product cross sell.</param>
        void BindManageCrossSellData(PersonalizationViewModel model, int productCrossSellType);

        /// <summary>
        /// Creates product cross sell list.
        /// </summary>
        /// <param name="model">Personalization model from where data for personalization view model will be retrieved.</param>
        /// <param name="productCrossSellType">Type of Product cross sell.</param>
        /// <returns>Bool value if the product cross sell is created or not.</returns>
        bool CreateProductCrossSellList(PersonalizationViewModel model, int productCrossSellType);
    }
}
