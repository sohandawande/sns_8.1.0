﻿using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    /// <summary>
    /// AddOn Value Agent Interface
    /// </summary>
    public interface IAddOnValueAgent 
    {
        /// <summary>
        /// Delete AddOn Value
        /// </summary>
        /// <param name="AddOnValueId">int addOnValueId</param>
        /// <returns>Returns true or false</returns>
        bool Delete(int addOnValueId);

        /// <summary>
        /// Get AddOn Value by addOnId 
        /// </summary>
        /// <param name="AddOnValueId">int addOnValueId</param>
        /// <returns>Returns AddOn value model</returns>
        AddOnValueViewModel GetAddOnValue(int addOnValueId);
        
        /// <summary>
        /// Create AddOn Value
        /// </summary>
        /// <param name="model">AddOnValueViewModel model</param>
        /// <returns>Returns true or false</returns>
        bool Create(AddOnValueViewModel model);
        
        /// <summary>
        /// Update AddOn Value
        /// </summary>
        /// <param name="model">AddOnValueViewModel model</param>
        /// <returns>Returns true or false</returns>
        bool Update(AddOnValueViewModel model);

        /// <summary>
        /// Bind DropDown
        /// </summary>
        /// <param name="model">AddOnValueViewModel model</param>
        /// <returns>Returns AddOn value model</returns>
        AddOnValueViewModel BindDropdownValues(AddOnValueViewModel model);

        /// <summary>
        /// Get AddOn value associated with AddOn Id
        /// </summary>
        /// <param name="addOnId">int addOnId</param>
        /// <param name="filters">FilterCollection filters</param>
        /// <param name="sortCollection">SortCollection sortCollection</param>
        /// <param name="pageIndex">Nullable int pageIndex</param>
        /// <param name="recordPerPage">Nullable int recordPerPage</param>
        /// <returns>Returns AddOn value list view model</returns>
        AddOnValueListViewModel GetAddOnValueByAddOnId(int addOnId, FilterCollection filters, SortCollection sortCollection, int? pageIndex = null, int? recordPerPage = null);
        

        /// <summary>
        /// Set values for Recurring Billing 
        /// </summary>
        /// <param name="model">AddOnValueViewModel model</param>
        /// <returns>Returns AddOn value model</returns>
        AddOnValueViewModel SetRecurringBillingValues(AddOnValueViewModel model);
    }
}
