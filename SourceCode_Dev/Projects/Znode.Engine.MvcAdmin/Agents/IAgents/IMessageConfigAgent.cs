﻿using System.Collections.Generic;
using System.Web.Mvc;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    /// <summary>
    /// Interface for Message Config Agent
    /// </summary>
    public interface IMessageConfigAgent
    {
        /// <summary>
        /// Get Message List
        /// </summary>
        /// <returns>Returns ManufacturersListViewModel</returns>
        ManageMessageListViewModel GetMessageConfigs(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// Get Message on the basis of messageConfigId
        /// </summary>
        /// <param name="messageConfigId">messageConfigId</param>
        /// <returns>Returns ManageMessageViewModel</returns>
        ManageMessageViewModel GetMessageConfig(int messageConfigId);

        /// <summary>
        /// Delete Message on the basis of messageConfigId
        /// </summary>
        /// <param name="messageConfigId">messageConfigId</param>
        /// <returns>Returns bool</returns>
        bool DeleteMessageConfig(int messageConfigId);

        /// <summary>
        ///  Save new Message 
        /// </summary>
        /// <param name="model">Manage Message View Model</param>
        /// <param name="message">message</param>
        /// <returns>Returns true/false</returns>
        bool SaveMessageConfig(ManageMessageViewModel model, out string message);

        /// <summary>
        /// Updates message on the basis of messageConfigId
        /// </summary>
        /// <param name="messageConfigId">messageConfigId</param>
        /// <param name="model">ManageMessageViewModel</param>
        /// <returns>Returns true/false</returns>
        bool UpdateMessageConfig(int messageConfigId, ManageMessageViewModel model);

        /// <summary>
        /// Get Banners List
        /// </summary>
        /// <returns>Returns Banner List.</returns>
        ManageMessageListViewModel GetAllBanners(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// Creates New Banner.
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Return true or false.</returns>
        bool CreateBanner(ManageBannerViewModel model);

        /// <summary>
        /// Method Returns all Banner Keys.
        /// </summary>
        /// <returns>Return all banner keys.</returns>
        List<SelectListItem> GetAllBannerKeys();

        /// <summary>
        /// Get the banner based on banner id.
        /// </summary>
        /// <param name="messageId"></param>
        /// <returns>Return banner details</returns>
        ManageBannerViewModel GetBanner(int messageId);

        /// <summary>
        /// Updates Banner on the basis of messageId
        /// </summary>
        /// <param name="messageConfigId">messageId</param>
        /// <param name="model">ManageBannerViewModel</param>
        /// <returns>Returns true/false</returns>
        bool UpdateBanner(int messageId, ManageBannerViewModel model);

        /// <summary>
        /// Get All the portals, & set it selected based on portal id.
        /// </summary>
        /// <param name="portalId"></param>
        /// <returns>Return portal list.</returns>
        List<SelectListItem> GetPortals(int portalId = 0);

        /// <summary>
        /// Bind Portal Drop Down
        /// </summary>
        /// <returns>Returns Portal List<PortalModel></returns>
        List<PortalModel> BindDropDown();

        /// <summary>
        /// Get Portal list 
        /// </summary>
        /// <returns>Returns list of portals</returns>
        List<PortalViewModel> GetPortalsList();
    }
}
