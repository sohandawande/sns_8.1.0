﻿using Znode.Engine.Api.Models.Responses;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public interface IActivateAgent
    {
        /// <summary>
        /// This method is used to install the License
        /// This creates the .dlsc file in the Licence folder on the server specified the model
        /// This method internally call the ZNode.Libraries.Framework.Business.ZNodeLicenseManager class to install the license
        /// </summary>
        /// <param name="model">ActivatePageApiModel which should contain the type of the license user want to activate, and detail information of the user</param>
        /// <returns>This method returns the ActivateResponse which will contain the status of the activation of license along with error message if present</returns>
        ActivateLicenseResponse InstallLicense(ActivatePageApiModel model);

        /// <summary>
        /// This method validates the license (whether the license is installed on the calling server or not)
        /// This method internaly calls the ZNode.Libraries.Framework.Business.ZNodeLicenseManager class to validate the license
        /// </summary>
        /// <returns>Returns ActivateLicenseResponse which will contain the status true if the license is valid otherwise false</returns>
        ActivateLicenseResponse ValidateLicense();
    }
}
