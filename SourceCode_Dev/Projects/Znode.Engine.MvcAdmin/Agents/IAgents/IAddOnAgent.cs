﻿using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    /// <summary>
    /// AddOn Agent Interface
    /// </summary>
    public interface IAddOnAgent
    {
       /// <summary>
       /// Get Addons
       /// </summary>
        /// <param name="filters">(FilterCollection filters</param>
        /// <param name="sortCollection">SortCollection sortCollection</param>
        /// <param name="pageIndex">Nullable int pageIndex</param>
        /// <param name="recordPerPage">Nullable int recordPerPage</param>
       /// <returns>Returns addon list view model</returns>
        AddOnListViewModel GetAddOns(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);
        
        /// <summary>
        /// Delete AddOn
        /// </summary>
        /// <param name="AddOnId">int AddOnId</param>
        /// <param name="errorMessage">out string errorMessage</param>
        /// <returns>Return true or false</returns>
        bool DeleteAddOn(int AddOnId,out string errorMessage);
        
        /// <summary>
        ///Get AddOn by AddOnId 
        /// </summary>
        /// <param name="AddOnId">int addOnId</param>
        /// <returns>Returns AddOn model</returns>
        AddOnViewModel GetAddOn(int addOnId);

        /// <summary>
        /// Create Add-On
        /// </summary>
        // <param name="model">AddOnViewModel model</param>
        /// <param name="addOnId">out int addOnId</param>
        /// <returns>Return True or False</returns>
        bool Create(AddOnViewModel model,out int addOnId);

        /// <summary>
        /// Update Add-On
        /// </summary>
        /// <param name="model">AddOnViewModel model</param>
        /// <returns>Return True or False</returns>
        bool Update(AddOnViewModel model);

        /// <summary>
        /// Set values of TrackInventory and AllowBackorder by value OutOfStockOption
        /// </summary>
        /// <returns>Returns AddOn model</returns>
        AddOnViewModel SetOutOfStockOption(AddOnViewModel model);

        /// <summary>
        /// Set values of TrackInventory and AllowBackorder by value OutOfStockOption
        /// </summary>
        /// <returns>Returns AddOn model</returns>
        AddOnViewModel GetOutOfStockOption(AddOnViewModel model);

        /// <summary>
        /// Get Invontory Setting
        /// </summary>
        /// <param name="model">AddOnViewModel model</param>
        /// <returns>Returns AddOn model</returns>
        AddOnViewModel GetInventorySetting(AddOnViewModel model);

        /// <summary>
        /// Get the name of lacale Language
        /// </summary>
        /// <param name="localeId">Id of locale</param>
        /// <returns>Name of locale Language</returns>
        string GetLocaleNameByLocaleId(int localeId);
    }
}
