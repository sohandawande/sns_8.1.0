﻿using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public interface IAffiliateTrackingAgent
    {
        /// <summary>
        /// Gets the affilate tracking data.
        /// </summary>
        /// <param name="startDate">string start date</param>
        /// <param name="endDate">string end date</param>
        /// <returns>Returns DataSet</returns>
        DataSet GetAffiliateTrackingData(string startDate, string endDate);

        /// <summary>
        /// Binds the file type dropdown list using enum values.
        /// </summary>
        /// <returns>Returns list of List<SelectListItem></returns>
        List<SelectListItem> FileTypeList();

        /// <summary>
        /// Downloads the file on the basis of data provided.
        /// </summary>
        /// <param name="model">Model of type AffiliateTrackingViewModel</param>
        /// <param name="trackingData">Dataset of affiliate tracking data</param>
        /// <param name="response">HttpResponseBase response</param>
        void DownloadFile(AffiliateTrackingViewModel model, DataSet trackingData, HttpResponseBase response);
    }
}
