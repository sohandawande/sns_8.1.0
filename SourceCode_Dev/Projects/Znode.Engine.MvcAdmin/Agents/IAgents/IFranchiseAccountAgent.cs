﻿using System.Collections.Generic;
using System.Web;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public interface IFranchiseAccountAgent
    {
        /// <summary>
        /// To Get Franchise Account List
        /// </summary>
        /// <param name="filters">FilterCollection filters</param>
        /// <param name="sortCollection">SortCollection sortCollection</param>
        /// <param name="pageIndex">Nullable int pageIndex</param>
        /// <param name="recordPerPage">Nullable int recordPerPage</param>
        /// <returns>Return Franchise Account List.</returns>
        VendorAccountListViewModel GetFranchiseAccountList(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// To Get Franchise Account Data to Download
        /// </summary>
        /// <param name="filters">FilterCollection filters</param>
        /// <param name="sortCollection">SortCollection sortCollection</param>
        /// <param name="pageIndex">Nullable int pageIndex</param>
        /// <param name="recordPerPage">Nullable int recordPerPage</param>
        /// <returns>Return Franchise Account List.</returns>
        VendorAccountDownloadListViewModel GetFranchiseAccountToDownload(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// Create Franchise Account
        /// </summary>
        /// <param name="model">FranchiseAccountViewModel model</param>
        /// <param name="AccountId">out int AccountId</param>
        /// <param name="errorMessage">out string errorMessage</param>
        /// <returns>Returns true or false</returns>
        bool CreateFranchiseAccount(FranchiseAccountViewModel model, out int accountId, out string errorMessage);


        /// <summary>
        /// Bind list for Profile
        /// </summary>
        /// <returns>list of ProfileModel</returns>
        List<ProfileModel> ProfileList();

        /// <summary>
        /// Bind DropDowns
        /// </summary>
        /// <param name="model">FranchiseAccountViewModel model</param>
        /// <returns>Returns Franchise account view model</returns>
        FranchiseAccountViewModel BindPortalInformation(FranchiseAccountViewModel model);

        /// <summary>
        /// Get Franchise by Account Id
        /// </summary>
        /// <param name="accountId">int accountId</param>
        /// <returns>Return Franchise Account View Model</returns>
        FranchiseAccountViewModel GetFranchiseAccountById(int accountId);

        /// <summary>
        /// Update Franchise Account
        /// </summary>
        /// <param name="model">FranchiseAccountViewModel model</param>
        /// <returns>Return true or false</returns>
        bool UpdateFranchiseAccount(FranchiseAccountViewModel model);

        /// <summary>
        /// Reset Account Password
        /// </summary>
        /// <param name="accountId">int accountId</param>
        /// <param name="areaName">string areaName</param>
        /// <returns>Returns Account View Model</returns>
        AccountViewModel ResetPassword(int accountId, string areaName = "");

        /// <summary>
        /// This method will download the file
        /// </summary>
        /// <param name="model"></param>
        void DownloadFile(VendorAccountDownloadListViewModel model, HttpResponseBase response, string fileName);

        /// <summary>
        /// Get Portal list 
        /// </summary>
        /// <returns>Returns list of portals</returns>
        List<PortalViewModel> GetPortalsList();

        /// <summary>
        /// Get Profiels list by portalId
        /// </summary>
        /// <param name="storeId">int storeId</param>
        /// <returns>List of profiles associated wih store</returns>
        ProfileListViewModel GetProfileList(int storeId);
    }
}
