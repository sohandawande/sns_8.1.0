﻿using System.Collections.ObjectModel;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public interface IAccountAgent
    {
        /// <summary>
        /// This method is used to login the user.
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Return the login details.</returns>
        LoginViewModel Login(LoginViewModel model);

        /// <summary>
        /// This method is used to logout the user.
        /// </summary>
        void Logout();

        /// <summary>
        /// Method used to check whether the user is authorized or not.
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="roleName"></param>
        /// <returns>Returns true or false</returns>
        bool IsUserInRole(string userName, string roleName);
        
        /// <summary>
        /// Method checks for the Account key in the session variable.
        /// </summary>
        /// <returns>Returns true or false.</returns>
        bool CheckAccountKey();

        /// <summary>
        /// Function used for Reset admin details for the first default login.
        /// </summary>
        /// <param name="model">AccountModel</param>
        /// <returns>Returns reset admin details status.</returns>
        ResetPasswordModel ResetPassword(ResetPasswordModel model);
        /// <summary>
        /// Function used for forgot password.
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Returns forgot password details.</returns>
        AccountViewModel ForgotPassword(AccountViewModel model);

        /// <summary>      
        /// This function will check the Reset Password Link current status.
        /// </summary>
        /// <param name="model">ChangePasswordViewModel</param>
        /// <returns>Returns Status of Reset Password Link.</returns>
        ResetPasswordStatusTypes CheckResetPasswordLinkStatus(ChangePasswordViewModel model);

        /// <summary>
        /// Function used to Change/Reset the user password.
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Returns the Change/Reset password details.</returns>
        ChangePasswordViewModel ChangePassword(ChangePasswordViewModel model);

        /// <summary>
        /// Get Country Dropdown list.
        /// </summary>
        /// <param name="portalId">portal id</param>
        /// <returns>Collection CountryModel </returns>
        Collection<CountryModel> GetCountries(int portalId);

        /// <summary>
        /// Get All Country Dropdown List.
        /// </summary>
        /// <param name="portalId">int portalId</param>
        /// <returns>Collection CountryModel</returns>
        Collection<CountryModel> GetCountriesByPortalId(int portalId);

        /// <summary>
        /// Gets the account view model from the session
        /// </summary>
        /// <returns></returns>
        AccountViewModel GetAccountViewModel();

        void UpdateAccountViewModel(object model);

        /// <summary>
        /// To check  Address is Valid
        /// </summary>
        /// <param name="shippingAddress">AddressViewModel shippingAddress</param>
        /// <returns>returns true/false</returns>
        bool IsAddressValid(AddressViewModel shippingAddress);

        /// <summary>
        /// To get current login account from session
        /// </summary>
        /// <returns>returns AccountViewModel</returns>
        AccountViewModel GetAccounts();

        /// <summary>
        /// Checks Product already added to Wishlist
        /// </summary>
        /// <param name="productId">ProductId to check exists in wishlist</param>
        /// <param name="accountId">AccountId to check exists in wishlist</param>
        /// <returns>Product exist in wishlist or not</returns>
        int CheckWishListExists(int productId, int accountId);

        /// <summary>
        /// To Get the Menu List based on login user Role.
        /// </summary>
        /// <param name="userName">UserName for the login User.</param>
        /// <returns>Return Role Menu List in RoleMenuListViewModel format.</returns>
        RoleMenuListViewModel GetRoleMenuList(string userName);

        /// <summary>
        /// To Get the account Details based on user account id.
        /// </summary>
        /// <param name="accountId">User accountId</param>
        /// <returns>Return the Account details in AccountViewModel format</returns>
        AccountViewModel GetAccountByAccountId(int accountId);
    }
}
