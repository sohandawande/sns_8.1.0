﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public interface IPRFTCreditApplicationAgent
    {
        /// <summary>
        /// To get Credit Application list
        /// </summary>
        /// <param name="filters">FilterCollection filters</param>
        /// <param name="sortCollection">SortCollection sortCollection</param>
        /// <param name="pageIndex">int? pageIndex</param>
        /// <param name="recordPerPage">int? recordPerPage</param>
        /// <returns>returns list of Credit Application</returns>
        PRFTCreditApplicationListViewModel GetCreditApplications(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// To get Credit Application by id
        /// </summary>
        /// <param name="caseRequestId">int caseRequestId</param>
        /// <returns>returns CaseRequest model</returns>
        PRFTCreditApplicationViewModel GetCreditApplication(int creditApplicationId);

        /// <summary>
        /// To update Credit Application
        /// </summary>
        /// <param name="model">CreditApplicationViewModel model</param>
        /// <returns>returns update status</returns>
        bool UpdateCreditApplication(PRFTCreditApplicationViewModel model);
    }
}
