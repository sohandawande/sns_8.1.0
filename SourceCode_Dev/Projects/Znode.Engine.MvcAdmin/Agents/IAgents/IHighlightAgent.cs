﻿using System.Collections.Generic;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public interface IHighlightAgent
    {
        /// <summary>
        /// Get Highlight list.
        /// </summary>
        /// <returns>returns HighlightsListViewModel</returns>
        HighlightsListViewModel GetHighlights();

        /// <summary>
        ///  Get Highlight details by highlightId.
        /// </summary>
        /// <param name="highlightId">int highlightId</param>
        /// <returns>returns HighlightViewModel</returns>
        HighlightViewModel GetHighlight(int? highlightId);

        /// <summary>
        /// Save new Highlight.
        /// </summary>
        /// <param name="model">HighlightViewModel model</param>
        /// <param name="errorMessage">error message</param>
        /// <returns>true or false</returns>
        bool CreateHighlight(HighlightViewModel model, out string errorMessage);

        /// <summary>
        /// Update Highlight.
        /// </summary>
        /// <param name="model">HighlightViewModel model</param>
        /// <param name="errorMessage">error message</param>
        /// <returns>true or false</returns>
        bool UpdateHighlight(HighlightViewModel model, out string errorMessage);

        /// <summary>
        /// Delete Existing Highlight.
        /// </summary>
        /// <param name="highlightId">int highlightId</param>
        /// <param name="errorMessage">error message</param>
        /// <returns>true or false</returns>
        bool DeleteHighlight(int highlightId, out string errorMessage);

        /// <summary>
        /// Get highlight list
        /// </summary>
        /// <param name="filters">FilterCollection filters</param>
        /// <param name="sortCollection">SortCollection sortCollection</param>
        /// <param name="pageIndex">int? pageIndex</param>
        /// <param name="recordPerPage">int? recordPerPage</param>
        /// <returns>returns HighlightsListViewModel</returns>
        HighlightsListViewModel GetHighlights(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// Check the Associated product with highlight
        /// </summary>
        /// <param name="highlightId">int highlightId</param>
        /// <returns>true or false</returns>
        bool CheckAssociatedProduct(int highlightId);

        /// <summary>
        /// Gets the highlight Type list.
        /// </summary>
        /// <param name="model">IEnumerable<HighlightTypeModel></param>
        /// <returns>List of highlight type.</returns>
        List<HighlightTypeModel> GetHighlightTypeList();

        /// <summary>
        /// Sets Highlight properites.
        /// </summary>
        /// <param name="model">Highlight view model.</param>
        void SetHighlightProperty(HighlightViewModel model);
    }
}
