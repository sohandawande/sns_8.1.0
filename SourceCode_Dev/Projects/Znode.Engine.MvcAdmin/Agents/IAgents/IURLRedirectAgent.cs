﻿using System.Collections.Generic;
using System.Web.Mvc;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public interface IURLRedirectAgent 
    {
        /// <summary>
        /// Get the Url Redirect List.
        /// </summary>
        /// <param name="filters">Filter for Url Redirect.</param>
        /// <param name="sortCollection">Sort Collection for Url Redirect.</param>
        /// <param name="pageIndex">Page Index.</param>
        /// <param name="recordPerPage">Record per page.</param>
        /// <returns>List of url redirects.</returns>
        URLRedirectListViewModel GetUrlRedirects(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// get the existing url redirect.
        /// </summary>
        /// <param name="UrlRedirectId">Id of the url redirect.</param>
        /// <returns>URLRedirectViewModel</returns>
        URLRedirectViewModel GetUrlRedirect(int UrlRedirectId);

        /// <summary>
        /// Create the url Redirect.
        /// </summary>
        /// <param name="model">model to create.</param>
        /// <param name="errorMessage">error message</param>
        /// <returns>Returns True or False.</returns>
        bool CreateUrlRedirect(URLRedirectViewModel model, out string errorMessage);

        /// <summary>
        /// Upadate the Url Redirect.
        /// </summary>
        /// <param name="model">model to update.</param>
        /// <param name="errorMessage">error message</param>
        /// <returns>Returns True or False.</returns>
        bool UpdateUrlRedirect(URLRedirectViewModel model, out string errorMessage);

        /// <summary>
        /// Delete the Url Redirect.
        /// </summary>
        /// <param name="urlredirectId">Id of the Url Redirect.</param>
        /// <param name="errorMessage">error message</param>
        /// <returns>Returns True or False.</returns>
        bool DeleteUrlRedirect(int urlredirectId, out string errorMessage);

        /// <summary>
        /// Get list of status.
        /// </summary>
        /// <returns>List of status.</returns>
        List<SelectListItem> GetStatusList();
    }
}
