﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public partial interface ICustomerAgent
    {
        Collection<PRFTSalesRepresentative> GetSalesRep();

        CustomerListViewModel GetAssociatedCustomerList(int accountId, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// This method will delete the Account Profile
        /// </summary>
        /// <param name="customerusermappingId">integer customerusermappingId</param>
        /// <returns>Returs true if customerusermappingId deleted</returns>
        bool DeleteUserAssociatedCustomer(int customerUserMappingId);

        CustomerListViewModel GetNotAssociatedCustomerList(int accountId, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        bool SaveAssociateCustomer(int accountId, string customerAccountIds);

        bool DeleteAllUserMapping(int userAccountId);
    }
}
