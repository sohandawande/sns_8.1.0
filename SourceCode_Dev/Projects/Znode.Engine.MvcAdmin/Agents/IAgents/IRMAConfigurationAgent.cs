﻿using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public interface IRMAConfigurationAgent
    {
        /// <summary>
        /// Gets RMAConfiguration data by rmaConfig Id
        /// </summary>
        /// <param name="rmaConfigId">Id of RMAConfiguration</param>
        /// <returns>Returns model of type RMAConfigurationViewModel.</returns>
        RMAConfigurationViewModel GetRMAConfigurationsByRMAConfigId(int rmaConfigId);

        /// <summary>
        /// Get the lsit of all RMAConfigurations
        /// </summary>
        /// <returns>ListModel of RMAConfiguration</returns>
        RMAConfigurationListViewModel GetRMAConfigurations();

        /// <summary>
        /// Create new RMAConfiguration
        /// </summary>
        /// <param name="model">View Model of RMAConfiguration</param>
        /// <returns>Boolean value true/false</returns>
        bool CreateRMAConfiguration(RMAConfigurationViewModel model);

        /// <summary>
        /// Update an existing RMAConfiguration
        /// </summary>
        /// <param name="model">View Model of RMAConfiguration</param>
        /// <returns>View Model of RMAConfiguration</returns>
        bool UpdateRMAConfiguration(RMAConfigurationViewModel model);        

        /// <summary>
        /// Get list of RequestStatuses
        /// </summary>
        /// <param name="filters">Filter Collection</param>
        /// <param name="sortCollection">Sort Collection</param>
        /// <param name="pageIndex">Page Index</param>
        /// <param name="recordPerPage">countnumber of records to display</param>
        /// <returns>list of ReasonForReturn</returns>
        RequestStatusListViewModel GetRequestStatusList(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// Gets RequestStatus data by rmaConfig Id
        /// </summary>
        /// <param name="requestStatusId">Id of RequestStatus</param>
        /// <returns>Returns model of type RequestStatusViewModel.</returns>
        RequestStatusViewModel GetRequestStatus(int requestStatusId);

        /// <summary>
        /// Update an existing RequestStatus
        /// </summary>
        /// <param name="model">View Model of RequestStatus</param>
        /// <returns>View Model of RequestStatus</returns>
        bool UpdateRequestStatus(RequestStatusViewModel model);

        /// <summary>
        /// Delete an existing RequestStatus
        /// </summary>
        /// <param name="model">View Model of RequestStatus</param>
        /// <returns>Boolean value true/false</returns>
        bool DeleteRequestStatus(int requestStatusId);
    }
}
