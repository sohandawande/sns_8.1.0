﻿using System.Collections.Generic;
using System.Web;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public interface IVendorAccountAgent
    {
        /// <summary>
        /// To Get Vendor Account List
        /// </summary>
        /// <param name="filters">FilterCollection filters</param>
        /// <param name="sortCollection">SortCollection sortCollection</param>
        /// <param name="pageIndex">int? pageIndex</param>
        /// <param name="recordPerPage">int? recordPerPage</param>
        /// <returns>Return Vendor Account List.</returns>
        VendorAccountListViewModel GetVendorAccountList(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// To Get Vendor Account Data to Download
        /// </summary>
        /// <param name="filters">FilterCollection filters</param>
        /// <param name="sortCollection">SortCollection sortCollection</param>
        /// <param name="pageIndex">int? pageIndex</param>
        /// <param name="recordPerPage">int? recordPerPage</param>
        /// <returns>Return Vendor Account List.</returns>
        VendorAccountDownloadListViewModel GetVendorAccountToDownload(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);
        /// <summary>
        /// Bind List of Store 
        /// </summary>
        /// <returns>list of PortalModel</returns>
        List<PortalModel> BindStoreList();

        /// <summary>
        /// Bind list for Profile
        /// </summary>
        /// <returns>list of ProfileModel</returns>
        List<ProfileModel> BindProfileList();

        /// <summary>
        /// Create new vendor account.
        /// </summary>
        /// <param name="model">VendorAccountViewModel</param>
        /// <returns>returns true/false</returns>
        bool CreateVendorAccount(VendorAccountViewModel model,out int accountId, out string message);

        /// <summary>
        /// Get Vendor account by account id
        /// </summary>
        /// <param name="accountId">account id</param>
        /// <returns>VendorAccountViewModel</returns>
        VendorAccountViewModel GetVendorAccountById(int accountId);

        /// <summary>
        /// Get Vendor Account Addresses bye account id
        /// </summary>
        /// <param name="accountId">account id</param>
        /// <param name="filters">filter collection for vendor account</param>
        /// <param name="sortCollection">sorting for vendor account</param>
        /// <param name="pageIndex">page index for vendor account records</param>
        /// <param name="recordPerPage">record per page</param>
        /// <returns>AddressListViewModel</returns>
        AddressListViewModel GetAddressDetails(int accountId, FilterCollection fromFilters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// Update existing vendor account
        /// </summary>
        /// <param name="model">VendorAccountViewModel</param>
        /// <returns>returns true / false</returns>
        bool UpdateVendorAccount(VendorAccountViewModel model);

        /// <summary>
        /// Get Country List
        /// </summary>
        /// <returns>AddressViewModel</returns>
        AddressViewModel GetCountryList();

        /// <summary>
        /// Create new address.
        /// </summary>
        /// <param name="model">AddressViewModel</param>
        /// <returns>reurns true / false</returns>
        bool CreateAddress(AddressViewModel model);

        /// <summary>
        /// Get address by address id
        /// </summary>
        /// <param name="addressId">address id</param>
        /// <returns>AddressViewModel</returns>
        AddressViewModel GetAddressById(int addressId);

        /// <summary>
        /// Update existing address 
        /// </summary>
        /// <param name="model">AddressViewModel</param>
        /// <returns>returns true / false</returns>
        bool UpdateAddress(AddressViewModel model);

        /// <summary>
        /// Check default address 
        /// </summary>
        /// <param name="addressId">address id </param>
        /// <returns>returns true / false</returns>
        bool CheckDefaultAddress(int addressId);

        /// <summary>
        /// Delete existing address
        /// </summary>
        /// <param name="addressId">address</param>
        /// <returns>returns true / false</returns>
        bool DeleteAddress(int addressId);

        /// <summary>
        /// set default address and update
        /// </summary>
        /// <param name="model">AddressViewModel</param>
        /// <returns>AddressViewModel</returns>
        AddressViewModel UpdateAddressDetails(AddressViewModel model);

        /// <summary>
        /// download vendor account list
        /// </summary>
        /// <param name="model">VendorAccountDownloadListViewModel</param>
        /// <param name="response">HttpResponseBase</param>
        /// <param name="fileName">fileName</param>
        void DownloadFile(VendorAccountDownloadListViewModel model, HttpResponseBase response, string fileName);

        /// <summary>
        /// This function Get Shipping Active countries for portalId.
        /// </summary>
        /// <param name="portalId">int portalId to get countries related to perticular portal</param>
        /// <param name="model">VendorAccountViewModel model</param>
        /// <returns>Returns VendorAccountViewModel</returns>
        VendorAccountViewModel GetShippingActiveCountryByPortalId(int portalId, VendorAccountViewModel model);

        /// <summary>
        /// Get selected portals list asper permissions.
        /// </summary>
        /// <returns>PortalViewModel list</returns>
        List<PortalViewModel> GetPortalsList();
    }
}
