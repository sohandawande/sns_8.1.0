﻿using System.Collections.Generic;
using System.Web.Mvc;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    /// <summary>
    /// Interface for Tax Rule Agent
    /// </summary>
    public interface ITaxRuleAgent
    {
        /// <summary>
        /// Delete Tax Rule
        /// </summary>
        /// <param name="taxRuleId">int taxRuleId</param>
        /// <returns>Returns true/false</returns>
        bool Delete(int taxRuleId);

        /// <summary>
        /// Bind Dropdown Values for tax rule type and country
        /// </summary>
        /// <param name="model">Model of type TaxRulesViewModel</param>
        /// <param name="isEditAction">bool isEditAction to check whether Action is Edit Action or not</param>
        /// <returns>Returns list of tax rule type and country</returns>
        TaxRulesViewModel BindDropdownValues(TaxRulesViewModel model, bool isEditAction = false);       

        /// <summary>
        /// Bind State List
        /// </summary>
        /// <param name="filters">Filter Collection</param>
        /// <param name="stateCode">State Code</param>
        /// <returns>Returns list of states</returns>
        List<SelectListItem> BindStateList(Api.Client.Filters.FilterCollection filters, string stateCode = "");

        /// <summary>
        /// Bind County List
        /// </summary>
        /// <param name="filters">Filter collection</param>
        /// <returns>Returns list of county</returns>
        List<SelectListItem> BindCountyList(Api.Client.Filters.FilterCollection filters, string countyCode = "");

        /// <summary>
        /// Save Tax Rules
        /// </summary>
        /// <param name="model">Model of type TaxRulesViewModel</param>
        /// <returns>Returns true/false</returns>
        bool SaveTaxRules(TaxRulesViewModel model);

        /// <summary>
        /// Update Tax Rule
        /// </summary>
        /// <param name="model">Model of type TaxRulesViewModel</param>
        /// <returns>Returns true/false</returns>
        bool UpdateTaxRules(TaxRulesViewModel model);

        /// <summary>
        /// Get Tax Rules
        /// </summary>
        /// <param name="taxRuleId">int taxRuleId</param>
        /// <returns>Returns Tax Rules based on taxRuleId</returns>
        TaxRulesViewModel GetTaxRules(int taxRuleId);
    }
}
