﻿using System.Linq;
using Znode.Engine.MvcAdmin.Extensions;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public interface ICartAgent
    {
        /// <summary>
        /// Create the shopping cart object and add cart items in the session and cookie.
        /// </summary>
        /// <param name="cartItem">Cart Item</param>
        /// <returns>Returns the shopping cart items as cart view model</returns>
        CartViewModel Create(CartItemViewModel cartItem);

        /// <summary>
        /// Get Cart method to check Session or Cookie to get the existing shopping cart.
        /// </summary>
        /// <returns>Returns the cart items as cart view Model</returns>
        CartViewModel GetCart();

        /// <summary>
        /// Get Cart method to check Session or Cookie to get the existing shopping cart.
        /// </summary>
        /// <returns>Returns the cart items as cart view Model</returns>
        int GetCartCount();

        /// <summary>
        /// Get Cart method to check Session or Cookie to get the existing shopping cart.
        /// </summary>
        /// <returns>Returns the calculated cart items as cart view model</returns>
        CartViewModel Calculate(ShoppingCartModel shoppingCartModel);

        /// <summary>
        /// Removes the items from the shopping cart.
        /// </summary>
        /// <param name="guid">GUID of the cart item</param>
        bool RemoveItem(string guid);

        /// <summary>
        /// Updates the Quantity updated in the shopping cart page.
        /// </summary>
        /// <param name="guid">GUID of the cart item</param>
        /// <param name="qty">Quantity Selected</param>
        bool UpdateItem(string guid, int qty);

        /// <summary>
        /// Merge the cart after login
        /// </summary>
        /// <returns>Returns true/false</returns>
        bool Merge();

        /// <summary>
        /// Finds the couponCode is valid or invalid
        /// </summary>
        /// <param name="couponCode">CouponCode of the cart</param>
        /// <returns>Returns valid coupon or not</returns>
        bool IsCouponValid(string couponCode);

        /// <summary>
        /// Adds the Coupon Code to the Cart
        /// </summary>
        /// <param name="couponCode">couponCode of the Cart</param>
        CartViewModel ApplyCoupon(string couponCode);

        /// <summary>
        /// Set the Coupon Applied success and error message
        /// </summary>
        /// <param name="model">Cart View Model</param>
        /// <param name="success">Coupon applied or not</param>
        void SetMessages(CartViewModel model);

        /// <summary>
        /// Check Inventory for Wishlist product
        /// </summary>
        ///<param name="cartItem">CartItem View model</param>
        /// <returns>Returns the productview model with inventory message</returns>
        ProductViewModel WishlistCheckInventory(CartItemViewModel cartItem);

        /// <summary>
        /// This function will Removes all item of shopping cart from Session & Cookie.
        /// </summary>
        /// <returns>Returns true or false.</returns>
        bool RemoveAllCartItems();

        /// <summary>
        /// Get the cart items based on the cookie mapping id
        /// </summary>
        /// <returns>Returns the shopping cart items Shopping Cart Model</returns>
        ShoppingCartModel GetCartFromCookie();
    }
}