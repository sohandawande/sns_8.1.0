﻿
namespace Znode.Engine.MvcAdmin.Agents
{
    public interface IConfigurationReaderAgent
    {
        /// <summary>
        /// Get dynamic grid configuration XML 
        /// </summary>
        /// <param name="listID">List type</param>
        /// <returns>Returns XMl string</returns>
        string GetFilterConfigurationXML(int listID);
    }
}
