﻿using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public partial interface ICustomerAgent
    {
        /// <summary>
        /// To Get Customer Account Details.
        /// </summary>
        /// <param name="customerId">Account Id of the Customer</param>
        /// <returns>Return Account Details. in AccountViewModel format.</returns>
        AccountViewModel GetCustomerAccountById(int customerId);

        /// <summary>
        /// Get Vendor Account Addresses bye account id
        /// </summary>
        /// <param name="accountId">account id</param>
        /// <param name="filters">filter collection for vendor account</param>
        /// <param name="sortCollection">sorting for vendor account</param>
        /// <param name="pageIndex">page index for vendor account records</param>
        /// <param name="recordPerPage">record per page</param>
        /// <returns>Return Customer Address list in AddressListViewModel format</returns>
        AddressListViewModel GetCustomerAddressDetails(int accountId, FilterCollection fromFilters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// To Update Customer Details.
        /// </summary>
        /// <param name="model">CustomerViewModel model</param>
        /// <returns>Return Updated details or error details in CustomerViewModel format.</returns>
        CustomerViewModel UpdateCustomerAccount(CustomerViewModel model);

        /// <summary>
        /// Get Customer List.
        /// </summary>
        /// <param name="filters">filter collection for Customer</param>
        /// <param name="sortCollection">sorting for Customer</param>
        /// <param name="pageIndex">page index for Customer records</param>
        /// <param name="recordPerPage">record per page</param>
        /// <returns>CustomerListViewModel</returns>
        CustomerListViewModel GetCustomerList(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// Get Franchisee Customer List.
        /// </summary>
        /// <param name="filters">pass filters to get filtered list</param>
        /// <param name="sortCollection">to get sorted list</param>
        /// <param name="pageIndex">page index</param>
        /// <param name="recordPerPage">page per record</param>
        /// <returns>returns list of Franchise Customer</returns>
        CustomerListViewModel GetFranchiseCustomerList(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// Get details about Customer Affiliate
        /// </summary>
        /// <param name="accountId">Account Id by which gets affiliate details</param>
        /// <returns>CustomerAffiliateViewModel</returns>
        CustomerAffiliateViewModel GetCustomerAffiliateByAccountId(int accountId);

        /// <summary>
        /// Get the list of ReferralCommissionType 
        /// </summary>
        /// <returns>List of ReferralCommissionType of type SelectListItem </returns>
        List<SelectListItem> GetReferralCommissionTypeList();

        /// <summary>
        /// Get List Partner status values
        /// </summary>
        /// <returns>List Partner status values</returns>
        List<SelectListItem> GetApprovStatusList();

        /// <summary>
        /// To Update Customer Affiliate Details.
        /// </summary>
        /// <param name="customerAffiliate">CustomerAffiliate View Model</param>
        /// <returns></returns>
        bool UpdateCustomerAffiliate(CustomerAffiliateViewModel customerAffiliate);

        /// <summary>
        /// Create New Customer Account.
        /// </summary>
        /// <param name="model">CustomerViewModel </param>
        /// <returns>Returns true /  false</returns>
        bool CreateCustomerAccount(CustomerViewModel model, out int accountId, out string message);

        /// <summary>
        /// Download Customer List.
        /// </summary>
        /// <param name="filters">Filter collection</param>
        /// <param name="sortCollection">sorting the Customer</param>
        /// <param name="pageIndex">page index for Customer records</param>
        /// <param name="recordPerPage">record per page</param>
        /// <returns>CustomerDownloadListViewModel</returns>
        CustomerDownloadListViewModel GetCustomerToDownload(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// Delete Existing Customer Account.
        /// </summary>
        /// <param name="accountId">accountId</param>
        /// <param name="errorMessage">out string errorMessage</param>
        /// <returns>Returns True / False</returns>
        bool DeleteCustomerAccount(int accountId, out string errorMessage);

        /// <summary>
        /// Get the list of AccountPayment
        /// </summary>
        /// <param name="accountId">Account Id by which list retrieve</param>
        /// <param name="filters">Filter collection</param>
        /// <param name="sortCollection">sorting the Customer</param>
        /// <param name="pageIndex">page index for Customer records</param>
        /// <param name="recordPerPage">record per page</param>
        /// <returns>AccountPaymentListViewModel</returns>
        AccountPaymentListViewModel GetAccountPaymentList(int accountId, FilterCollection filters, SortCollection sorts, int? pageIndex, int? recordPerPage);

        /// <summary>
        /// Create new Account Payment
        /// </summary>
        /// <param name="model">AccountPaymentViewModel</param>
        /// <returns>Boolean value true/false</returns>
        bool CreateAccountPayment(AccountPaymentViewModel model);

        /// <summary>
        /// Get Customer Account Profile.
        /// </summary>
        /// <param name="accountId">AccountId</param>
        /// <param name="filters">Filtering Customer Data</param>
        /// <param name="sortCollection">Sorting Collection</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="recordPerPage">Customer Data Record per Page </param>
        /// <returns>ProfileListViewModel</returns>
        ProfileListViewModel GetCustomerProfileList(int accountId, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// Get Customer that not associated with profiles.
        /// </summary>
        /// <param name="accountId">AccountId</param>
        /// <param name="filters">Filtering Customer Data</param>
        /// <param name="sortCollection">Sorting Collection</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="recordPerPage">Customer Data Record per Page </param>
        /// <returns>ProfileListViewModel</returns>
        ProfileListViewModel GetCustomerNotAssociatedProfileList(int accountId, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// Save Associated profiles with account
        /// </summary>
        /// <param name="accountId">accountId</param>
        /// <param name="profileIds">profileIds</param>
        /// <returns>True / false</returns>
        bool SaveAssociateProfile(int accountId, string profileIds);

        /// <summary>
        /// Get the list of ReferralCommission
        /// </summary>
        /// <param name="accountId">Account Id by which list retrieve</param>
        /// <param name="filters">Filter collection</param>
        /// <param name="sortCollection">sorting the Customer</param>
        /// <param name="pageIndex">page index for Customer records</param>
        /// <param name="recordPerPage">record per page</param>
        /// <returns>ReferralCommissionListViewModel</returns>
        ReferralCommissionListViewModel GetReferralCommissionList(int accountId, FilterCollection filters, SortCollection sorts, int? pageIndex, int? recordPerPage);

        /// <summary>
        /// This method will delete the Account Profile
        /// </summary>
        /// <param name="accountProfileId">integer accountProfileId</param>
        /// <returns>Returs true if accountProfileId deleted</returns>
        bool DeleteAccountAssociatedProfile(int accountProfileId);

        /// <summary>
        /// Get Customer Pricing Product List
        /// </summary>
        /// <param name="accountId">account id</param>
        /// <param name="filters">Filter collection</param>
        /// <param name="sortCollection">sorting the Customer</param>
        /// <param name="pageIndex">page index for Customer records</param>
        /// <param name="recordPerPage">record per page</param>
        /// <returns>CustomerBasedPricingListViewModel </returns>
        CustomerBasedPricingProductListViewModel GetCustomerPricingProductList(int accountId, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// Download csv file
        /// </summary>
        /// <param name="model">CustomerDownloadListViewModel</param>
        /// <param name="response">HttpResponseBase</param>
        /// <param name="fileName">fileName</param>
        void DownloadFile(CustomerDownloadListViewModel model, HttpResponseBase response, string fileName);

        /// <summary>
        /// To download customer pricing product list in csv format.
        /// </summary>
        /// <param name="model">CustomerBasedPricingProductListViewModel</param>
        /// <param name="response">HttpResponseBase</param>
        /// <param name="fileName">File Name</param>
        void DownloadCustomerPricingProductFile(CustomerBasedPricingProductListViewModel model, HttpResponseBase response, string fileName);

        /// <summary>
        /// Bind affiliate approval status list.
        /// </summary>
        /// <returns>CustomerViewModel</returns>
        CustomerViewModel BindStatus();
    }
}
