﻿using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.MvcAdmin.ViewModels;
namespace Znode.Engine.MvcAdmin.Agents
{
    public interface IDomainAgent
    {
        /// <summary>
        /// Get all the domains from specified portal id
        /// </summary>
        /// <param name="portalId">portal id</param>
        /// <param name="filters">filter list across Domains</param>
        /// <param name="sortCollection">sort collection for domains.</param>
        /// <param name="pageIndex">pageIndex for Domain record </param>
        /// <param name="recordPerPage">paging doamin record per page</param>
        /// <returns></returns>
        DomainListViewModel GetDomains(int portalId, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// Create New Domain Url.
        /// </summary>
        /// <param name="model">DomainViewModel</param>
        /// <returns>Returns true / false</returns>
        bool createDomainUrl(DomainViewModel model);

        /// <summary>
        /// Get Existing Domain by portal id.
        /// </summary>
        /// <param name="portalId">portal id.</param>
        /// <returns>Returns DomainViewModel</returns>
        DomainViewModel GetDomain(int portalId);

        /// <summary>
        /// Update Existing Domain url.
        /// </summary>
        /// <param name="viewModel">DomainViewModel</param>
        /// <returns>Returns bool.</returns>
        bool UpdateDomainUrl(DomainViewModel viewModel);

        /// <summary>
        /// Delete Existing Domain Url by domain Id.
        /// </summary>
        /// <param name="domainId">domain Id.</param>
        /// <returns>Returns bool</returns>
        bool DeleteDomainUrl(int domainId);
    }
}
