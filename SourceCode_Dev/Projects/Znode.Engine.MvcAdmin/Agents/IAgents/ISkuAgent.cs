﻿using System;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    /// <summary>
    /// Interface for the SKU
    /// </summary>
    public interface ISkuAgent
    {
        /// <summary>
        /// Get the SKUs Price
        /// </summary>
        /// <param name="productId">Product ID</param>
        /// <param name="attributeId">Attribute ID</param>
        /// <param name="productPrice">Product Price</param>
        /// <param name="selectedSku">Selected SKU</param>
        /// <param name="imageMediumPath">Selected imageMediumPath</param>
        /// <returns>Returns the SKU Price</returns>
        Decimal GetSkusPrice(int productId, int attributeId, decimal productPrice, out string selectedSku, out string imagePath, out string imageMediumPath);

        /// <summary>
        /// Get SKU with inventory for the selected attribute
        /// </summary>
        /// <param name="productId">int productId</param>
        /// <param name="attributeId">int attributeI</param>
        /// <returns>Returns sku model.</returns>
        SkuModel GetSkuInventory(int productId, string attributeId);

        /// <summary>
        /// Get SKU Model with inventory based on given Product Id and SKU
        /// </summary>
        /// <param name="productId">int productId</param>
        /// <param name="sku">string sku</param>
        /// <returns>Returns sku model.</returns>
        SkuModel GetBySku(int productId, string sku);
    }
}
