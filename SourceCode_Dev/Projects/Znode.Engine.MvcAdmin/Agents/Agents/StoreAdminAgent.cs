﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Exceptions;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;
using Znode.Engine.MvcDemo.Maps;

namespace Znode.Engine.MvcAdmin.Agents
{
    /// <summary>
    /// This is the agent for the Store Admin
    /// </summary>
    public class StoreAdminAgent : BaseAgent, IStoreAdminAgent
    {
        #region Private Variables
        private readonly IAccountsClient _accountClient;
        private readonly IProfilesClient _profilesClient;
        private readonly IPermissionsClient _permissionClient;
        private readonly IPortalAgent _portalsAgent;
        private readonly IAddressesClient _addressClient; //PRFT Custom Code
        #endregion

        #region Constructor

        public StoreAdminAgent()
        {
            _accountClient = GetClient<AccountsClient>();
            _profilesClient = GetClient<ProfilesClient>();
            _permissionClient = GetClient<PermissionsClient>();
            _portalsAgent = new PortalAgent();
            _addressClient = new AddressesClient(); //PRFT Custom Code
        }

        #endregion

        #region Public Methods

        public StoreAdminListViewModel GetStoreAdmins(string roleName, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            ReplaceFilterKeyName(ref filters, MvcAdminConstants.AccountNumber, FilterKeys.ExternalAccountNo);
            ReplaceFilterKeyName(ref filters, MvcAdminConstants.StartDate, FilterKeys.CreateDate);
            ReplaceFilterKeyName(ref filters, MvcAdminConstants.EndDate, FilterKeys.CreateDate);

            bool isUserNameFilterAvailable = false;

            foreach (var filter in filters)
            {
                if (filter.Item1.ToLower().Equals(FilterKeys.Username.ToLower()))
                {
                    isUserNameFilterAvailable = true;
                }

                if (filter.Item1.ToLower().Equals(FilterKeys.AccountId.ToLower()))
                {
                    filters.Add(new FilterTuple(FilterKeys.AccountId, FilterOperators.Like, filter.Item3));
                    filters.Remove(filter);
                    break;
                }
            }

            if (!isUserNameFilterAvailable)
            {
                filters.Add(new FilterTuple(FilterKeys.Username, FilterOperators.Equals, string.Format("'{0}'", HttpContext.Current.User.Identity.Name)));
            }

            _accountClient.RefreshCache = true;
            var list = _accountClient.GetAccountDetailsByRoleName(roleName, new ExpandCollection { ExpandKeys.User }, filters, sortCollection, pageIndex, recordPerPage);
            return StoreAdminViewModelMap.ToListViewModel(list);
        }

        public StoreAdminViewModel GetStoreAdminByID(int storeAdminID)
        {
            var account = _accountClient.GetAccount(storeAdminID, new ExpandCollection { ExpandKeys.User });
            var addressModel = GetAddress(storeAdminID);
            account.AddressModel = AddressViewModelMap.ToModel(addressModel);
            return StoreAdminViewModelMap.ToModel(account);
        }

        public bool SaveStoreAdmin(StoreAdminViewModel model, out int accountId, out string errorMessage)
        {
            try
            {
                errorMessage = string.Empty;
                var account = _accountClient.CreateAdminAccount(StoreAdminViewModelMap.ToAdminCreateAccountModel(model));
                accountId = account.AccountId;
                if ((accountId > 0) && account.IsEmailSentFailed)
                {
                    errorMessage = string.Format("{0} {1}", ZnodeResources.ErrorSmptSetting, PortalAgent.CurrentPortal.StoreName);
                    return false;
                }
                return account.AccountId > 0 ? true : false;
            }
            catch (ZnodeException ex)
            {
                accountId = 0;
                errorMessage = ex.ErrorMessage;
                return false;
            }
        }

        public bool UpdateStoreAdmin(int accountId, StoreAdminViewModel model)
        {
            var account = _accountClient.UpdateAccount(accountId, StoreAdminViewModelMap.ToAdminUpdateAccountModel(model));
            //PRFT Custom Code: Start
            var addressModel = GetAddress(accountId);
            addressModel.FirstName = model.FirstName;
            addressModel.LastName = model.LastName;
            addressModel.MiddleName = model.MiddleName;
            var address = _addressClient.UpdateAddress(addressModel.AddressId, AddressViewModelMap.ToModel(addressModel));            
            //PRFT Custom Code: End
            return account.AccountId > 0 && address.AddressId > 0 ? true : false;
        }

        public bool DeleteStoreAdmin(int storeAdminID, out string errorMessage)
        {
            try
            {
                errorMessage = string.Empty;
                return _accountClient.DeleteAccount(storeAdminID);
            }
            catch (ZnodeException ex)
            {
                errorMessage = ex.ErrorMessage;
                return false;
            }
        }

        public List<PortalViewModel> GetAllStores()
        {
            PortalListViewModel model = new PortalListViewModel();
            model = _portalsAgent.GetPortals();
            return model.Portals.ToList();
        }

        public List<ProfileViewModel> GetAllProfiles()
        {
            ProfileListViewModel model = new ProfileListViewModel();
            model = ProfileViewModelMap.ToListViewModel(_profilesClient.GetProfilesAssociatedWithUsers(HttpContext.Current.User.Identity.Name), null, null);
            return model.Profiles.ToList();
        }

        public bool EnableDisableStoreAdmin(int storeAdminID)
        {
            return _accountClient.EnableDisableAdminAccount(storeAdminID);
        }

        public bool IsCurrentUser(int storeAdminID, string currentUserName)
        {
            bool isCurrentUser = false;
            string dbUserName = GetUserNameById(storeAdminID);

            isCurrentUser = dbUserName.ToLower().Equals(currentUserName.ToLower()) ? true : false;
            return isCurrentUser;
        }

        public void DownloadFile(StoreAdminListViewModel model, HttpResponseBase response, string fileName)
        {
            DownloadHelper downloadHelper = new DownloadHelper();
            downloadHelper.Download(model.StoreAdmins, Convert.ToString(Convert.ToInt32(FileTypes.CSV)), response, null, fileName);
        }

        //PRFT Custom Code: Start
        public AddressViewModel GetAddress(int accountId)
        {
            AddressViewModel addressModel = new AddressViewModel();
            FilterCollection fromFilters = new FilterCollection();                       
            fromFilters.Add(new FilterTuple(MvcAdminConstants.AccountId, FilterOperators.Equals, Convert.ToString(accountId)));
            _addressClient.RefreshCache = true;
            var addressList = _addressClient.GetAddresses(fromFilters, new SortCollection(), null, null);
            if (!Equals(addressList, null) && !Equals(addressList.Addresses, null) && addressList.Addresses.Count > 0)
            {
                addressModel = AddressViewModelMap.ToViewModel(addressList.Addresses[0]);                
            }
            return addressModel;
        }
        //PRFT Custom Code: End

        #endregion

        #region Private Methods

        /// <summary>
        /// This method will return the User Name from Account ID
        /// </summary>
        /// <param name="id">int Account ID</param>
        /// <returns>String UserName</returns>
        private string GetUserNameById(int id)
        {
            string userName = string.Empty;
            StoreAdminViewModel model = GetStoreAdminByID(id);
            if (!Equals(model, null) && !string.IsNullOrEmpty(model.LoginName))
            {
                userName = model.LoginName;
            }

            return userName;
        }

        #endregion

    }
}