﻿using System;
using System.Collections.Generic;
using System.Web;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.MvcAdmin.Agents.Agents;
using Znode.Engine.MvcAdmin.Agents.IAgents;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;
namespace Znode.Engine.MvcAdmin.Agents
{
    /// <summary>
    /// Gift Card Agent
    /// </summary>
    public class GiftCardAgent : BaseAgent, IGiftCardAgent
    {
        #region Private Variables

        #region Readonly variables
        private readonly IGiftCardsClient _giftCardsClient;
        private readonly IAccountsClient _accountsClient;
        private readonly IPortalsClient _portalsClient;
        private readonly IRMARequestItemClient _rmaRequestItemClient;
        private readonly IRMARequestAgent _rmaRequestAgent;
        private readonly IRMARequestClient _rmaRequestClient;
        #endregion

        private string ExcludeExpired = MvcAdminConstants.ExcludeExpired;
        private string ExpirationDate = "expirationdate";
        #endregion

        #region Constructor

        /// <summary>
        /// Public Constructor for Gift card.
        /// </summary>
        public GiftCardAgent()
        {
            _giftCardsClient = GetClient<GiftCardsClient>();
            _accountsClient = GetClient<AccountsClient>();
            _portalsClient = GetClient<PortalsClient>();
            _rmaRequestItemClient = GetClient<RMARequestItemClient>();
            _rmaRequestAgent = new RMARequestAgent();
            _rmaRequestClient = GetClient<RMARequestClient>();
        }

        #endregion

        #region Public Methods

        public GiftCardListViewModel GetGiftCards(FilterCollection filters, SortCollection sortCollection, int? pageIndex, int? recordPerPage)
        {
            #region Filter Conversion


            if (filters.Count > 0)
            {
                foreach (FilterTuple tuple in filters)
                {
                    if (Equals(tuple.Item1.ToLower(), ExpirationDate) && !Equals(tuple.Item3.ToLower(), "0"))
                    {
                        filters.Add(new FilterTuple(FilterKeys.ExpirationDate, FilterOperators.GreaterThanOrEqual, tuple.Item3.ToLower()));
                        filters.Remove(tuple);
                        break;
                    }
                    else
                    {
                        if (Equals(tuple.Item1.ToLower(), ExpirationDate) && Equals(tuple.Item3.ToLower(), "0"))
                        {
                            filters.Remove(tuple);
                            break;
                        }
                    }
                }
            }
            else
            {
                filters.Add(new FilterTuple(FilterKeys.ExpirationDate, FilterOperators.GreaterThanOrEqual, DateTime.Now.ToShortDateString()));
            }
            #endregion

            _giftCardsClient.RefreshCache = true;
            GiftCardListModel list = _giftCardsClient.GetGiftCards(null, filters, sortCollection, pageIndex - 1, recordPerPage);
            return Equals(list, null) ? new GiftCardListViewModel() : GiftCardViewModelMap.ToListViewModel(list);
        }

        public bool DeleteGiftCard(int giftCardId)
        {
            return _giftCardsClient.DeleteGiftCard(giftCardId);
        }

        public GiftCardViewModel GetGiftCard(int GiftCardId)
        {
            _giftCardsClient.RefreshCache = true;
            GiftCardModel giftCardModel = _giftCardsClient.GetGiftCard(GiftCardId);
            return (!Equals(giftCardModel, null)) ? GiftCardViewModelMap.ToViewModel(giftCardModel) : new GiftCardViewModel();
        }

        public GiftCardViewModel Create(GiftCardViewModel model)
        {
            string mode = string.Empty;
            if (!string.IsNullOrEmpty(model.Mode))
            {
                mode = model.Mode;
            }
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                AccountModel accountModel = _accountsClient.GetAccountByUser(HttpContext.Current.User.Identity.Name);
                model.CreatedBy = accountModel.AccountId;
            }
            GiftCardViewModel giftCardModel = new GiftCardViewModel();
            giftCardModel = GiftCardViewModelMap.ToViewModel(_giftCardsClient.CreateGiftCard(GiftCardViewModelMap.ToModel(model)));
            giftCardModel.Mode = mode;
            giftCardModel.RMARequestId = model.RMARequestId;
            giftCardModel.QuantityList = model.QuantityList;
            giftCardModel.OrderLineItems = model.OrderLineItems;
            return giftCardModel;
        }

        public bool Update(GiftCardViewModel model)
        {
            _giftCardsClient.RefreshCache = true;
            GiftCardModel updatedGiftCard = _giftCardsClient.UpdateGiftCard(model.GiftCardId, GiftCardViewModelMap.ToModel(model));
            return (!Equals(updatedGiftCard, null) && updatedGiftCard.GiftCardId > 0);
        }

        public bool IsAccountActive(int accountId)
        {
            AccountModel accounts = !Equals(_accountsClient.GetAccount(accountId), null) ? _accountsClient.GetAccount(accountId) : null;
            return (!Equals(accounts, null) && accounts.AccountId.Equals(accountId));
        }

        public GiftCardViewModel GetNextGiftCardNumber()
        {
            return GiftCardViewModelMap.ToViewModel(_giftCardsClient.GetNextGiftCardNumber());
        }

        public GiftCardViewModel GetAllPortals()
        {
            GiftCardViewModel model = new GiftCardViewModel();
            model.PortalList = GiftCardViewModelMap.ToListItems(_portalsClient.GetPortals(null, null, null).Portals);
            return model;
        }

        public List<ExcludeExpiredViewModel> GetExpirationStatus()
        {
            List<ExcludeExpiredViewModel> Expirationdate = new List<ExcludeExpiredViewModel>();
            Expirationdate.Add(new ExcludeExpiredViewModel() { ExpirationDate = DateTime.Now.ToShortDateString(), ExcludeText = ExcludeExpired });
            return Expirationdate;
        }

        public bool AddRMARequestItem(RMARequestItemViewModel rmaRequestItem)
        {
            if (!Equals(rmaRequestItem, null))
            {
                rmaRequestItem = RMARequestItemViewModelMap.ToViewModel(_rmaRequestItemClient.CreateRMARequestItem(RMARequestItemViewModelMap.ToModel(rmaRequestItem)));
                if (!Equals(rmaRequestItem, null) && rmaRequestItem.RMARequestItemID > 0)
                {
                    return true;
                }
            }
            return false;
        }

        public bool UpdateRMA(GiftCardViewModel model, out string message)
        {
            RMARequestViewModel rmaRequestViewModel = _rmaRequestAgent.GetRMARequest(model.RMARequestId);
            rmaRequestViewModel.RequestStatusId = Convert.ToInt32(Znode.Engine.Api.Models.RequestStatusModel.RequestStatus.Returned_Refundable);
            _rmaRequestAgent.UpdateRMARequest(model.RMARequestId, rmaRequestViewModel, string.Empty);
            string[] lineitems = model.OrderLineItems.Split(',');
            string[] qtyItems = model.QuantityList.Split(',');
            bool isRmaRequestItemAdded = true;

            message = string.Empty;

            for (int count = 0; count < lineitems.Length; count++)
            {
                RMARequestItemViewModel rmaRequestitem = new RMARequestItemViewModel();
                int orderlineitemid = Convert.ToInt32(lineitems[count]);
                rmaRequestitem.RMArequestId = model.RMARequestId;
                rmaRequestitem.Quantity = Convert.ToInt32(qtyItems[count]);
                rmaRequestitem.OrderLineItemID = orderlineitemid;
                rmaRequestitem.GiftCardId = model.GiftCardId;
                rmaRequestitem.IsReturnable = true;
                rmaRequestitem.IsReceived = true;
                rmaRequestitem.Price = null;
                isRmaRequestItemAdded &= this.AddRMARequestItem(rmaRequestitem);
            }
            try
            {
                _rmaRequestClient.IsStatusEmailSent(model.RMARequestId);
                if (isRmaRequestItemAdded && model.SendMail)
                {
                    _rmaRequestClient.IsGiftCardMailSent(GiftCardViewModelMap.ToModel(model));
                }
            }
            catch (ZnodeException ex)
            {
                message = ex.ErrorMessage;
            }
            return isRmaRequestItemAdded;
        }

        public void SetRMAData(string mode, int item, string items, string qty, decimal amount, GiftCardViewModel model)
        {
            model.Mode = mode;
            model.RMARequestId = item;
            model.OrderLineItems = items;
            model.QuantityList = qty;
            RMARequestItemListViewModel rmaRequestItemList = _rmaRequestAgent.GetRMARequestItemsForGiftCard(items);
            model.Amount = amount;
            BindRMAData(rmaRequestItemList, model);
        }
        #endregion

        #region Private Methods
        private void BindRMAData(RMARequestItemListViewModel rmaRequestItemList, GiftCardViewModel giftCardModel)
        {
            if (!Equals(rmaRequestItemList, null) && !Equals(rmaRequestItemList.RMARequestItems, null))
            {
                int accountId = rmaRequestItemList.RMARequestItems[0].AccountID;
                int giftCardExpirationPeriod = rmaRequestItemList.RMARequestItems[0].GCExpirationPeriod;
                DateTime expiryDate = DateTime.Now.AddDays(giftCardExpirationPeriod);
                giftCardModel.ExpirationDate = HelperMethods.ViewDateFormat(expiryDate);
                giftCardModel.EnableToCustomerAccount = true;
                giftCardModel.PortalId = PortalAgent.CurrentPortal.PortalId;
                giftCardModel.AccountId = accountId;
            }
        }
        #endregion
    }

}