﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Linq;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Enum;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.Models;
using Znode.Engine.MvcAdmin.ViewModels;
using Znode.Engine.MvcAdmin.Extensions;
namespace Znode.Engine.MvcAdmin.Agents
{
    public class XmlGeneratorAgent : BaseAgent, IXmlGeneratorAgent
    {
        #region Private Variables
        private readonly IXmlGeneratorClient _xmlGeneratorClient;
        private const string ActionModeInsert = "INSERT";
        private const string ActionModeUpdate = "UPDATE";
        private const string ActionModeDelete = "DELETE";
        private const string EntityTypeTable = "Table";
        private const string EntityTypeView = "View";
        private const string EntityTypeProc = "StoredProcedure";
        private const string ObjectName = "ObjectName";
        private const string All = "All";
        private const string Columns = "columns";
        private const string EmptyString = "Empty";
        private const string ColumnName = "ColumnName";


        #endregion

        #region Constructor
        public XmlGeneratorAgent()
        {
            _xmlGeneratorClient = GetClient<XmlGeneratorClient>();
        }
        #endregion

        #region Public Methods

        public ApplicationSettingListViewModel ApplicationsSettinglList(FilterCollectionDataModel filters)
        {
            try
            {
                ApplicationSettingListModel model = _xmlGeneratorClient.GetApplicationSettings(filters.Filters, filters.SortCollection, filters.Page, filters.RecordPerPage);
                return XmlGeneratorModelMap.ToListModel(model);
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public bool SaveXmlConfiguration(string xmlString, string viewOptions, string entityType, string entityName, string frontPageName, string frontObjectName, int id)
        {
            ApplicationSettingModel model = XmlGeneratorModelMap.ToApplicationSettingModel(xmlString, viewOptions, entityType, entityName, frontPageName, frontObjectName, id);
            model.ActionMode = ActionModeInsert;
            return _xmlGeneratorClient.SaveXmlConfiguration(XmlGeneratorModelMap.ToDataModel(model));
        }

        public bool UpdateXmlConfiguration(string xmlString, string viewOptions, string entityType, string entityName, string frontPageName, string frontObjectName, int id)
        {
            ApplicationSettingModel model = XmlGeneratorModelMap.ToApplicationSettingModel(xmlString, viewOptions, entityType, entityName, frontPageName, frontObjectName, id);
            model.ActionMode = ActionModeUpdate;
            return _xmlGeneratorClient.SaveXmlConfiguration(XmlGeneratorModelMap.ToDataModel(model));
        }

        public bool DeleteXmlConfiguration(int id)
        {
            return _xmlGeneratorClient.SaveXmlConfiguration(new ApplicationSettingDataModel() { Id = id, ActionMode = ActionModeDelete });
        }

        public DataSet GetObjectColumnList(string entityType, string entityName)
        {
            return _xmlGeneratorClient.GetColumnList(entityType, string.IsNullOrEmpty(entityName) ? EmptyString : entityName);
        }

        public List<SelectListItem> GetEntityColumnList(string entityType, string entityName)
        {
            List<SelectListItem> columnList = new List<SelectListItem>();

            if (!string.IsNullOrEmpty(entityType) && !string.IsNullOrEmpty(entityName))
            {
                if (Equals(entityType, EntityTypeTable))
                {
                    entityType = GetObjectColumnListParameter.U.ToString();
                }
                else if (Equals(entityType, EntityTypeView))
                {
                    entityType = GetObjectColumnListParameter.V.ToString();
                }
                DataSet dsColumn = new DataSet();
                if (Equals(entityType, EntityTypeProc))
                {
                    dsColumn = GetObjectColumnList(GetObjectColumnListParameter.P.ToString(), entityName);
                }
                else
                {
                    dsColumn = GetObjectColumnList(entityType, entityName);
                }
                if (dsColumn.Tables.Count > 0)
                {
                    if (dsColumn.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow _dataRow in dsColumn.Tables[0].Rows)
                        {
                            columnList.Add(new SelectListItem() { Value = Convert.ToString(_dataRow[ColumnName]), Text = Convert.ToString(_dataRow[ColumnName]) });
                        }
                    }
                    else
                    {
                        foreach (DataColumn column in dsColumn.Tables[0].Columns)
                        {
                            columnList.Add(new SelectListItem() { Value = Convert.ToString(column.ColumnName), Text = Convert.ToString(column.ColumnName) });
                        }
                    }
                }


                columnList.Add(new SelectListItem() { Text = DynamicGridConstants.EditKey, Value = DynamicGridConstants.EditKey });
                columnList.Add(new SelectListItem() { Text = DynamicGridConstants.DeleteKey, Value = DynamicGridConstants.DeleteKey });
                columnList.Add(new SelectListItem() { Text = DynamicGridConstants.ManageKey, Value = DynamicGridConstants.ManageKey });
                columnList.Add(new SelectListItem() { Text = DynamicGridConstants.CopyKey, Value = DynamicGridConstants.CopyKey });
                columnList.Add(new SelectListItem() { Text = DynamicGridConstants.ViewKey, Value = DynamicGridConstants.ViewKey });
                columnList.Add(new SelectListItem() { Text = DynamicGridConstants.ImageKey, Value = DynamicGridConstants.ImageKey });
            }


            return columnList;
        }

        public string CreateXMLFile(List<WebGridColumnModel> list)
        {
            string xmlFile = string.Empty;
            StringWriter sw = new StringWriter();
            XmlTextWriter writer = new XmlTextWriter(sw);
            writer.WriteStartDocument();
            writer.Formatting = System.Xml.Formatting.Indented;
            writer.Indentation = 2;
            GetXMLString(list, writer);
            writer.WriteEndDocument();
            writer.Flush();
            xmlFile = sw.ToString();
            writer.Close();
            return xmlFile;
        }

        public List<WebGridColumnModel> GetListFromXMLString(string xmlString)
        {
            List<WebGridColumnModel> list = new List<WebGridColumnModel>();
            XDocument xmlDoc = XDocument.Parse(xmlString);
            list = (from xml in xmlDoc.Descendants(DynamicGridConstants.columnKey)
                    select new WebGridColumnModel()
                    {
                        Id = Convert.ToInt32(xml.TryGetElementValue(DynamicGridConstants.IdKey)),
                        Name = xml.TryGetElementValue(DynamicGridConstants.nameKey),
                        Headertext = xml.TryGetElementValue(DynamicGridConstants.headertextKey),
                        Width = Convert.ToInt32(xml.TryGetElementValue(DynamicGridConstants.WidthKey)),
                        Datatype = xml.TryGetElementValue(DynamicGridConstants.datatypeKey),
                        Columntype = xml.TryGetElementValue(DynamicGridConstants.ColumntypeKey),
                        Allowsorting = Convert.ToBoolean(xml.TryGetElementValue(DynamicGridConstants.AllowsortingKey)),
                        Allowpaging = Convert.ToBoolean(xml.TryGetElementValue(DynamicGridConstants.AllowpagingKey)),
                        Format = xml.TryGetElementValue(DynamicGridConstants.FormatKey),
                        Isvisible = Convert.ToChar(xml.TryGetElementValue(DynamicGridConstants.isvisibleKey)),
                        Mustshow = Convert.ToChar(xml.TryGetElementValue(DynamicGridConstants.MustshowKey)),
                        Musthide = Convert.ToChar(xml.TryGetElementValue(DynamicGridConstants.MusthideKey)),
                        Maxlength = Convert.ToInt32(xml.TryGetElementValue(DynamicGridConstants.MaxlengthKey, null, "Int")),
                        Isallowsearch = Convert.ToChar(xml.TryGetElementValue(DynamicGridConstants.isallowsearchKey)),
                        Isconditional = Convert.ToChar(xml.TryGetElementValue(DynamicGridConstants.IsconditionalKey)),

                        Isallowlink = Convert.ToChar(xml.TryGetElementValue(DynamicGridConstants.isallowlinkKey)),
                        Islinkactionurl = xml.TryGetElementValue(DynamicGridConstants.islinkactionurlKey),
                        Islinkparamfield = xml.TryGetElementValue(DynamicGridConstants.islinkparamfieldKey),
                        Ischeckbox = Convert.ToChar(xml.TryGetElementValue(DynamicGridConstants.ischeckboxKey, null, "Char")),
                        Checkboxparamfield = xml.TryGetElementValue(DynamicGridConstants.checkboxparamfieldKey),
                        Iscontrol = Convert.ToChar(xml.TryGetElementValue(DynamicGridConstants.iscontrolKey, null, "Char")),
                        Controltype = xml.TryGetElementValue(DynamicGridConstants.controltypeKey),
                        Controlparamfield = xml.TryGetElementValue(DynamicGridConstants.controlparamfieldKey),
                        Displaytext = xml.TryGetElementValue(DynamicGridConstants.displaytextKey),
                        Editactionurl = xml.TryGetElementValue(DynamicGridConstants.editactionurlKey),
                        Editparamfield = xml.TryGetElementValue(DynamicGridConstants.editparamfieldKey),
                        Deleteactionurl = xml.TryGetElementValue(DynamicGridConstants.deleteactionurlKey),
                        Deleteparamfield = xml.TryGetElementValue(DynamicGridConstants.deleteparamfieldKey),
                        Viewactionurl = xml.TryGetElementValue(DynamicGridConstants.viewactionurlKey),
                        Viewparamfield = xml.TryGetElementValue(DynamicGridConstants.viewparamfieldKey),
                        Imageactionurl = xml.TryGetElementValue(DynamicGridConstants.imageactionurlKey),
                        Imageparamfield = xml.TryGetElementValue(DynamicGridConstants.imageparamfieldKey),
                        Manageactionurl = xml.TryGetElementValue(DynamicGridConstants.manageactionurlKey),
                        Manageparamfield = xml.TryGetElementValue(DynamicGridConstants.manageparamfieldKey),
                        Copyactionurl = xml.TryGetElementValue(DynamicGridConstants.copyactionurlKey),
                        Copyparamfield = xml.TryGetElementValue(DynamicGridConstants.copyparamfieldKey),
                        XAxis = Convert.ToChar(xml.TryGetElementValue(DynamicGridConstants.xaxis, null, "Char")),
                        YAxis = Convert.ToChar(xml.TryGetElementValue(DynamicGridConstants.yaxis, null, "Char")),
                        IsAdvanceSearch = Convert.ToChar(xml.TryGetElementValue(DynamicGridConstants.isadvancesearch, null, "Char"))
                    }).ToList();


            return list;
        }



        public IEnumerable<SelectListItem> GetEntityNames(string term = "", string entityType = "")
        {
            IEnumerable<SelectListItem> entityNameList = null;

            DataTable dt = GetbjectName(entityType, All);

            if (dt.Rows.Count > 0)
            {
                entityNameList = (from data in dt.AsEnumerable()
                                  select new SelectListItem
                                  {
                                      Value = data.Field<string>(ObjectName),
                                      Text = data.Field<string>(ObjectName)
                                  }).Distinct();
                if (entityNameList.Count() > 1)
                {
                    var DistinctObjectList = entityNameList.Select(c => c.Value).Where(c => c.Contains(term)).Distinct();

                    entityNameList = (from obj in DistinctObjectList
                                      select new SelectListItem
                                      {
                                          Value = obj,
                                          Text = obj
                                      });
                }

            }
            return entityNameList;

        }

        #endregion

        #region Private Methods
        /// <summary>
        /// Get Names of tables/views/procedures
        /// </summary>
        /// <param name="entityType">Types of entity</param>
        /// <param name="entityName">Name of entity</param>
        /// <returns>Returns DataTable</returns>
        private DataTable GetbjectName(string entityType, string entityName)
        {
            DataTable dt = new DataTable();
            if (!string.IsNullOrEmpty(entityType) && !string.IsNullOrEmpty(entityName))
            {
                if (Equals(entityName, All))
                {
                    entityName = null;
                }
                if (Equals(entityType, EntityTypeTable))
                {
                    entityType = GetObjectColumnListParameter.U.ToString();
                }
                else if (Equals(entityType, EntityTypeView))
                {
                    entityType = GetObjectColumnListParameter.V.ToString();
                }
                else
                {
                    entityType = GetObjectColumnListParameter.P.ToString();
                }

                DataSet dsColumn = GetObjectColumnList(entityType, entityName);
                if (dsColumn.Tables.Count > 0)
                {
                    if (dsColumn.Tables[0].Rows.Count > 0)
                    {
                        dt = dsColumn.Tables[0];
                    }
                }
            }
            return dt;
        }

        /// <summary>
        /// Create XMl string from WebGridColumnModel list
        /// </summary>
        /// <param name="List">WebGridColumnModel List</param>
        /// <param name="writer">XmlTextWriter</param>
        /// <returns>Returns XML String</returns>
        private string GetXMLString(List<WebGridColumnModel> List, XmlTextWriter writer)
        {
            string xmlString = string.Empty;
            if (List != null)
            {
                var xEle = new XElement(Columns,
                      from column in List
                      select new XElement(DynamicGridConstants.columnKey,
                          new XElement(DynamicGridConstants.IdKey, column.Id),
                          new XElement(DynamicGridConstants.nameKey, column.Name),
                          new XElement(DynamicGridConstants.headertextKey, column.Headertext),
                          new XElement(DynamicGridConstants.WidthKey, column.Width),
                          new XElement(DynamicGridConstants.datatypeKey, column.Datatype),
                          new XElement(DynamicGridConstants.ColumntypeKey, column.Columntype),
                          new XElement(DynamicGridConstants.AllowsortingKey, column.Allowsorting),
                          new XElement(DynamicGridConstants.AllowpagingKey, column.Allowpaging),
                          new XElement(DynamicGridConstants.FormatKey, column.Format),
                          new XElement(DynamicGridConstants.isvisibleKey, column.Isvisible),
                          new XElement(DynamicGridConstants.MustshowKey, column.Mustshow),
                          new XElement(DynamicGridConstants.MusthideKey, column.Musthide),
                          new XElement(DynamicGridConstants.MaxlengthKey, column.Maxlength),
                          new XElement(DynamicGridConstants.isallowsearchKey, column.Isallowsearch),
                          new XElement(DynamicGridConstants.IsconditionalKey, column.Isconditional),
                          new XElement(DynamicGridConstants.isallowlinkKey, column.Isallowlink),
                          new XElement(DynamicGridConstants.islinkactionurlKey, column.Islinkactionurl),
                          new XElement(DynamicGridConstants.islinkparamfieldKey, column.Islinkparamfield),
                          new XElement(DynamicGridConstants.ischeckboxKey, column.Ischeckbox),
                          new XElement(DynamicGridConstants.checkboxparamfieldKey, column.Checkboxparamfield),
                          new XElement(DynamicGridConstants.iscontrolKey, column.Iscontrol),
                          new XElement(DynamicGridConstants.controltypeKey, column.Controltype),
                          new XElement(DynamicGridConstants.controlparamfieldKey, column.Controlparamfield),//
                          new XElement(DynamicGridConstants.displaytextKey, column.Displaytext),
                          new XElement(DynamicGridConstants.editactionurlKey, column.Editactionurl),
                          new XElement(DynamicGridConstants.editparamfieldKey, column.Editparamfield),
                          new XElement(DynamicGridConstants.deleteactionurlKey, column.Deleteactionurl),
                          new XElement(DynamicGridConstants.deleteparamfieldKey, column.Deleteparamfield),
                          new XElement(DynamicGridConstants.viewactionurlKey, column.Viewactionurl),
                          new XElement(DynamicGridConstants.viewparamfieldKey, column.Viewparamfield),
                          new XElement(DynamicGridConstants.imageactionurlKey, column.Imageactionurl),
                          new XElement(DynamicGridConstants.imageparamfieldKey, column.Imageparamfield),
                          new XElement(DynamicGridConstants.manageactionurlKey, column.Manageactionurl),
                          new XElement(DynamicGridConstants.manageparamfieldKey, column.Manageparamfield),
                          new XElement(DynamicGridConstants.copyactionurlKey, column.Copyactionurl),
                          new XElement(DynamicGridConstants.copyparamfieldKey, column.Copyparamfield),
                          new XElement(DynamicGridConstants.xaxis, column.XAxis),
                          new XElement(DynamicGridConstants.yaxis, column.YAxis),
                          new XElement(DynamicGridConstants.isadvancesearch, column.IsAdvanceSearch)

                                 ));

                xEle.WriteTo(writer);

            }
            return xmlString;
        }
        #endregion

    }
}