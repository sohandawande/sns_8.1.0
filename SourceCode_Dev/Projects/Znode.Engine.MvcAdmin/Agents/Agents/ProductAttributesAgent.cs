﻿using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public class ProductAttributesAgent : BaseAgent, IAttributesAgent
    {
        #region Private Variabls
        private readonly IProductAttributesClient _productAttributesClient;
        #endregion

        #region Constructors
        public ProductAttributesAgent()
        {
            _productAttributesClient = GetClient<ProductAttributesClient>();
        }
        #endregion

        #region Public Methods

        public AttributesListViewModel GetAttributes()
        {
            var list = _productAttributesClient.GetAttributes(null, null);
            return AttributesViewModelMap.ToListViewModel(list);
        }

        public AttributesListViewModel GetAttributesByAttributeTypeId(int attributeTypeId, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            _productAttributesClient.RefreshCache = true;
            if (Equals(filters, null))
            {
                filters = new FilterCollection();
            }
            filters.Add(new FilterTuple(FilterKeys.AttributeTypeId, FilterOperators.Equals, attributeTypeId.ToString()));
            
            ProductAttributeListModel list = _productAttributesClient.GetAttributesByAttributeTypeId(filters, sortCollection, pageIndex, recordPerPage);
            return Equals(list, null) ? new AttributesListViewModel() : AttributesViewModelMap.ToListViewModel(list);
        }

        public AttributesViewModel GetAttributesByAttributeId(int attributeId)
        {
            var attributes = _productAttributesClient.GetAttributes(attributeId);

            if (Equals(attributes, null))
            {
                return null;
            }
            var attributeViewModel = AttributesViewModelMap.ToViewModel(attributes);
            return attributeViewModel;
        }

        public bool SaveAttributes(AttributesViewModel model)
        {
            model.IsActive = true;
            var attribute = _productAttributesClient.CreateAttributes(AttributesViewModelMap.ToModel(model));
            return (attribute.AttributeId > 0) ? true : false;
        }

        public bool UpdateAttributes(AttributesViewModel model)
        {
            var attribute = _productAttributesClient.UpdateAttributes(model.AttributeId, AttributesViewModelMap.ToModel(model));
            return (attribute.AttributeId > 0) ? true : false;
        }

        public bool DeleteAttributes(int attributeId, out string errorMessage)
        {
            try
            {
                errorMessage = string.Empty;
                return _productAttributesClient.DeleteAttributes(attributeId); ;
            }
            catch (ZnodeException ex)
            {
                errorMessage = ex.ErrorMessage;
                return false;
            }
            
        }

        #endregion
    }
}