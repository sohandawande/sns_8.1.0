﻿using Resources;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;
using Znode.Engine.MvcDemo.Maps;

namespace Znode.Engine.MvcAdmin.Agents
{
    public class OrderAgent : BaseAgent, IOrderAgent
    {
        #region Private Variables
        #region Private Client Variables
        private readonly IOrdersClient _orderClient;
        private readonly IOrderStateClient _orderStateClient;
        private readonly IPortalsClient _portalClient;
        private readonly IProductsClient _productsClient;
        private readonly ICatalogsClient _catalogClient;

        private readonly IPortalAgent _portalAgent;
        #endregion

        #region Private Constant Variables
        public string emptySpace = " ";
        public string breakTag = "<br>";
        public string constComma = ", ";
        public string constTel = "Tel: ";
        public string constEmail = "Email: ";
        public string constRefundMonthStartDate = "RefundMonthStartDate";
        public string constRefundMonthFormat = "RefundMonthFormat";
        public string constRefundYearCounter = "RefundYearCounter";
        public const string guid = "guid";
        public const string quantity = "quantity";

        #endregion
        #endregion

        #region Constructor
        public OrderAgent()
        {
            _orderClient = GetClient<OrdersClient>();
            _orderStateClient = GetClient<OrderStateClient>();
            _portalClient = GetClient<PortalsClient>();
            _productsClient = GetClient<ProductsClient>();
            _catalogClient = GetClient<CatalogsClient>();

            _portalAgent = new PortalAgent();
        }
        #endregion

        #region Public Methods
        public AdminOrderListViewModel GetOrderList(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            _orderClient.RefreshCache = true;
            filters = SetFilterForOrderList(filters);
            AdminOrderListModel orderList = _orderClient.GetOrderList(null, filters, sortCollection, pageIndex, recordPerPage);
            if (!Equals(orderList, null) && !Equals(orderList.OrderList, null))
            {
                return OrderViewModelMap.ToAdminOrderListViewModel(orderList, pageIndex, recordPerPage);
            }
            else
            {
                return new AdminOrderListViewModel();
            }
        }

        public OrderViewModel GetOrderDetails(int orderId, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            _orderClient.RefreshCache = true;
            filters = new FilterCollection();
            filters.Add(new FilterTuple(FilterKeys.OrderId, FilterOperators.Equals, orderId.ToString()));
            filters.Add(new FilterTuple(FilterKeys.Username, FilterOperators.Equals, HttpContext.Current.User.Identity.Name));
            OrderModel orderModel = _orderClient.GetOrderDetails(null, filters, sortCollection, pageIndex, recordPerPage);
            if (!Equals(orderModel, null))
            {
                return OrderViewModelMap.ToViewModel(orderModel);
            }
            return null;
        }

        public List<SelectListItem> BindOrderStatus(int orderStateId = 0)
        {
            _orderStateClient.RefreshCache = true;
            OrderStateListModel orderStateList = _orderStateClient.GetOrderStates(null, Filters, null);
            if (orderStateId > 0)
            {
                return Equals(orderStateList, null) ? new List<SelectListItem>() : OrderViewModelMap.ToOrderStateSelectListItems(orderStateList.OrderStates, orderStateId);
            }
            return Equals(orderStateList, null) ? new List<SelectListItem>() : OrderViewModelMap.ToOrderStateSelectListItems(orderStateList.OrderStates);
        }

        public bool UpdateOrderStatus(OrderViewModel model, out string errorMessage)
        {
            try
            {
                errorMessage = string.Empty;
                OrderModel orderModel = _orderClient.UpdateOrderStatus(model.OrderId, OrderViewModelMap.ToOrderModel(model));
                return (!Equals(orderModel, null)) ? true : false;
            }
            catch (ZnodeException ex)
            {
                errorMessage = ex.ErrorMessage;
                return false;
            }
        }

        public OrderViewModel VoidPayment(OrderViewModel model)
        {
            var updatedModel = _orderClient.VoidPayment(model.OrderId, OrderViewModelMap.ToOrderModel(model));
            if (!Equals(updatedModel, null))
            {
                if (!string.IsNullOrEmpty(updatedModel.ErrorMessage))
                {
                    OrderModel orderModel = new OrderModel();
                    orderModel.ErrorMessage = updatedModel.ErrorMessage;
                    OrderViewModel updatedErrorModel = OrderViewModelMap.ToViewModel(orderModel);
                    return updatedErrorModel;
                }
                else
                {
                    return OrderViewModelMap.ToViewModel(updatedModel.Order);
                }
            }
            return null;
        }

        public OrderViewModel RefundPayment(OrderViewModel model)
        {
            var updatedModel = _orderClient.RefundPayment(model.OrderId, OrderViewModelMap.ToOrderModel(model));
            if (!Equals(updatedModel, null))
            {
                if (!string.IsNullOrEmpty(updatedModel.ErrorMessage))
                {
                    OrderModel orderModel = new OrderModel();
                    orderModel.ErrorMessage = updatedModel.ErrorMessage;
                    OrderViewModel updatedErrorModel = OrderViewModelMap.ToViewModel(orderModel);
                    return updatedErrorModel;
                }
                else
                {
                    return OrderViewModelMap.ToViewModel(updatedModel.Order);
                }
            }
            return null;
        }

        public List<SelectListItem> ToMonthSelectListItems()
        {
            List<SelectListItem> monthList = new List<SelectListItem>(11);
            SelectListItem item = new SelectListItem();
            DateTime month = Convert.ToDateTime(ConfigurationManager.AppSettings[constRefundMonthStartDate].ToString());
            for (int index = 0; index < 12; index++)
            {
                DateTime nextMonth = month.AddMonths(index);
                item = new SelectListItem
                {
                    Text = nextMonth.ToString(ConfigurationManager.AppSettings[constRefundMonthFormat]),
                    Value = nextMonth.Month.ToString()
                };
                monthList.Add(item);
            }
            return monthList;
        }

        public List<SelectListItem> ToYearSelectListItems()
        {
            List<SelectListItem> yearList = new List<SelectListItem>();
            SelectListItem item = new SelectListItem();
            int currentYear = System.DateTime.Now.Year;
            int counter = Convert.ToInt32(ConfigurationManager.AppSettings[constRefundYearCounter]);

            do
            {
                string itemtext = currentYear.ToString();
                item = new SelectListItem
                {
                    Text = itemtext,
                    Value = itemtext
                };
                yearList.Add(item);
                currentYear = currentYear + 1;

                counter = counter - 1;
            }
            while (counter > 0);

            return yearList;
        }

        public List<PortalViewModel> GetAllStores()
        {
            _portalClient.RefreshCache = true;
            PortalListViewModel model = PortalViewModelMap.ToListViewModel(_portalClient.GetPortals(Expands, Filters, new SortCollection()), null, null);
            return model.Portals.ToList();
        }

        public List<OrderStateViewModel> GetAllOrderStatusList()
        {
            _orderStateClient.RefreshCache = true;
            OrderStateListViewModel model = OrderStateViewModelMap.ToListViewModel(_orderStateClient.GetOrderStates(null, Filters, null));
            return model.OrderStateList.ToList();
        }

        public DataSet DownloadOrderData(string orderId, FilterCollection filterCollection = null)
        {
            _orderClient.RefreshCache = true;
            DataSet downloadOrderData = null;
            if (Equals(filterCollection, null))
            {
                FilterCollection filters = new FilterCollection();
                filters.Add(new FilterTuple(FilterKeys.OrderId, FilterOperators.Equals, orderId));
                downloadOrderData = _orderClient.DownloadOrderData(filters);
            }
            else
            {
                downloadOrderData = _orderClient.DownloadOrderData(filterCollection);
            }
            return !Equals(downloadOrderData, null) ? downloadOrderData : null;
        }

        public DataSet DownloadOrderLineItemData(string orderId, FilterCollection filterCollection = null)
        {
            _orderClient.RefreshCache = true;
            DataSet downloadOrderLineData = null;
            if (Equals(filterCollection, null))
            {
                FilterCollection filters = new FilterCollection();
                filters.Add(new FilterTuple(FilterKeys.OrderId, FilterOperators.Equals, orderId));
                downloadOrderLineData = _orderClient.DownloadOrderLineItemData(filters);
            }
            else
            {
                downloadOrderLineData = _orderClient.DownloadOrderLineItemData(filterCollection);
            }
            return !Equals(downloadOrderLineData, null) ? downloadOrderLineData : null;
        }

        public AddressListModel AddNewCustomer(ShippingBillingAddressViewModel viewModel)
        {
            AddressListModel listModel = new AddressListModel();

            AddressViewModel shippingViewModel = viewModel.ShippingAddressModel;
            AddressViewModel billingViewModel = viewModel.BillingAddressModel;

            AddressModel shippingModel = AddressViewModelMap.ToModel(shippingViewModel);
            AddressModel billingModel = AddressViewModelMap.ToModel(billingViewModel);

            listModel.Addresses.Add(shippingModel);
            listModel.Addresses.Add(billingModel);
            listModel.PortalId = Convert.ToInt32(viewModel.PortalId);
            listModel.Email = viewModel.EmailAddress;
            listModel.IsSameAsBillingAddress = viewModel.ShippingAddressModel.UseSameAsBillingAddress;
            AddressListResponse response = _orderClient.AddNewCustomer(listModel);
            return response.AddressListModel;
        }

        public AddressListModel UpdateCustomerAddress(int accountId, ShippingBillingAddressViewModel viewModel)
        {
            AddressListModel listModel = new AddressListModel();

            AddressViewModel shippingViewModel = viewModel.ShippingAddressModel;
            AddressViewModel billingViewModel = viewModel.BillingAddressModel;

            AddressModel shippingModel = AddressViewModelMap.ToModel(shippingViewModel);
            AddressModel billingModel = AddressViewModelMap.ToModel(billingViewModel);

            listModel.Addresses.Add(shippingModel);
            listModel.Addresses.Add(billingModel);
            listModel.PortalId = Convert.ToInt32(viewModel.PortalId);
            listModel.Email = viewModel.EmailAddress;
            listModel.IsSameAsBillingAddress = viewModel.ShippingAddressModel.UseSameAsBillingAddress;
            AddressListResponse response = _orderClient.UpdateCustomerAddress(accountId, listModel);
            return response.AddressListModel;
        }

        public ProductListViewModel SearchProducts(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            _productsClient.RefreshCache = true;
            ReplaceFilterKeyName(ref filters, FilterKeys.ManufacturerName, FilterKeys.Brand);
            ReplaceFilterKeyName(ref filters, FilterKeys.CategoryName, FilterKeys.Category);
            ReplaceFilterKeyName(ref filters, FilterKeys.ProductNumber, FilterKeys.ProductNum);

            this.RemoveFilters(filters, quantity);
            this.RemoveFilters(filters, guid);
            ProductListModel productList = _productsClient.SearchProduct(null, filters, sortCollection, pageIndex, recordPerPage);
            if (!Equals(productList, null) && !Equals(productList.Products, null))
            {
                return ProductViewModelMap.ToListViewModel(productList, pageIndex, recordPerPage);
            }
            return new ProductListViewModel();
        }

        public ProductViewModel GetOrderProduct(int productId)
        {
            _productsClient.RefreshCache = true;
            ProductModel productModel = _productsClient.GetOrderProduct(productId);
            Dictionary<string, List<SelectListItem>> dropDowns = new Dictionary<string, List<SelectListItem>>();

            foreach (var key in productModel.ProductAddOns)
            {
                List<SelectListItem> List = new List<SelectListItem>();
                foreach (var value in key.Value)
                {
                    List.Add(new SelectListItem() { Text = value.Key, Value = value.Value });
                }
                dropDowns.Add(key.Key, List);
            }


            foreach (var key in productModel.ProductAttributes)
            {
                List<SelectListItem> List = new List<SelectListItem>();
                foreach (var value in key.Value)
                {
                    List.Add(new SelectListItem() { Text = value.Key, Value = value.Value });
                }
                dropDowns.Add(key.Key, List);
            }


            foreach (var key in productModel.BundleProductDict)
            {
                List<SelectListItem> List = new List<SelectListItem>();
                foreach (var value in key.Value)
                {
                    foreach (var innervalues in value.Value)
                    {
                        List.Add(new SelectListItem() { Text = innervalues.Key, Value = innervalues.Value });
                    }
                    dropDowns.Add(value.Key, List);
                }
            }
            if (!Equals(productModel, null))
            {
                ProductViewModel returnModel = ProductViewModelMap.ToViewModel(productModel);
                returnModel.dropDownsList = dropDowns;
                return returnModel;
            }
            return new ProductViewModel();
        }

        public void SetBillingShippingAddresses(OrderViewModel orderModel)
        {
            StringBuilder billingAddress = new StringBuilder();
            billingAddress.Append(this.CheckNull(orderModel.BillingFirstName) + emptySpace);
            billingAddress.Append(this.CheckNull(orderModel.BillingLastName) + breakTag);
            billingAddress.Append(this.CheckNull(orderModel.BillingCompanyName) + breakTag);
            billingAddress.Append(this.CheckNull(orderModel.BillingStreetAddress1) + emptySpace);
            billingAddress.Append(this.CheckNull(orderModel.BillingStreetAddress2) + breakTag);
            billingAddress.Append(this.CheckNull(orderModel.BillingCity) + constComma);
            billingAddress.Append(this.CheckNull(orderModel.BillingStateCode) + emptySpace);
            billingAddress.Append(this.CheckNull(orderModel.BillingPostalCode) + breakTag);
            billingAddress.Append(this.CheckNull(orderModel.BillingCountryCode) + breakTag);
            billingAddress.Append(constTel + this.CheckNull(orderModel.BillingPhoneNumber) + breakTag);
            billingAddress.Append(constEmail + this.CheckNull(orderModel.BillingEmail));
            orderModel.BillingStreetAddress1 = billingAddress.ToString();
            StringBuilder shippingAddress = new StringBuilder();
            for (int index = 0; index < orderModel.OrderLineItemList.OrderLineItems.Count; index++)
            {
                for (int innerIndex = index + 1; innerIndex < orderModel.OrderLineItemList.OrderLineItems.Count; innerIndex++)
                {
                    if (!Equals(orderModel.OrderLineItemList.OrderLineItems[index].OrderShipmentId, orderModel.OrderLineItemList.OrderLineItems[innerIndex].OrderShipmentId))
                    {
                        orderModel.isMultipleShipping = true;
                        break;
                    }
                }
            }
            if (!orderModel.isMultipleShipping)
            {
                shippingAddress.Append(this.CheckNull(orderModel.OrderShipment.ShipToFirstName) + emptySpace);
                shippingAddress.Append(this.CheckNull(orderModel.OrderShipment.ShipToLastName) + breakTag);
                shippingAddress.Append(this.CheckNull(orderModel.OrderShipment.ShipToCompanyName) + breakTag);
                shippingAddress.Append(this.CheckNull(orderModel.OrderShipment.ShipToStreetAddress1) + emptySpace);
                shippingAddress.Append(this.CheckNull(orderModel.OrderShipment.ShipToStreetAddress2) + breakTag);
                shippingAddress.Append(this.CheckNull(orderModel.OrderShipment.ShipToCity) + constComma);
                shippingAddress.Append(this.CheckNull(orderModel.OrderShipment.ShipToStateCode) + emptySpace);
                shippingAddress.Append(this.CheckNull(orderModel.OrderShipment.ShipToPostalCode) + breakTag);
                shippingAddress.Append(this.CheckNull(orderModel.OrderShipment.ShipToCountryCode) + breakTag);
                shippingAddress.Append(constTel + this.CheckNull(orderModel.OrderShipment.ShipToPhoneNumber) + breakTag);
                shippingAddress.Append(constEmail + this.CheckNull(orderModel.OrderShipment.ShipToEmail));
                orderModel.OrderShipment.ShipToStreetAddress1 = shippingAddress.ToString();
            }
            else
            {
                orderModel.OrderLineItemShippingAddressList = new List<string>();
                foreach (OrderLineItemViewModel item in orderModel.OrderLineItemList.OrderLineItems)
                {
                    orderModel.OrderLineItemShippingAddressList.Add(item.ShippingAddress);
                }
                orderModel.OrderShipment.ShipToStreetAddress1 = ZnodeResources.ShipToMultipleShipping;
            }
        }

        public OrderLineItemViewModel GetOrderLineItems(int orderLineItemId, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            _orderClient.RefreshCache = true;
            filters = new FilterCollection();
            filters.Add(new FilterTuple(FilterKeys.OrderLineItemId, FilterOperators.Equals, orderLineItemId.ToString()));
            OrderLineItemModel orderLineItemModel = _orderClient.GetOrderLineItems(orderLineItemId);
            if (!Equals(orderLineItemModel, null))
            {
                return OrderLineItemViewModelMap.ToViewModel(orderLineItemModel);
            }
            return null;
        }

        public bool UpdateOrderLineItemStatus(OrderLineItemViewModel model, out string errorMessage)
        {
            try
            {
                errorMessage = string.Empty;
                OrderLineItemModel orderLineItemModel = _orderClient.UpdateOrderLineItemStatus(model.OrderLineItemId, OrderLineItemViewModelMap.ToModel(model));
                return (!Equals(orderLineItemModel, null)) ? true : false;
            }
            catch (ZnodeException ex)
            {
                errorMessage = ex.ErrorMessage;
                return false;
            }
        }

        public int GetCatalogIdByPortalId(int? portalId)
        {
            int catalogId = 0;

            _catalogClient.RefreshCache = true;

            FilterCollection filters = new FilterCollection();
            filters.Add(new FilterTuple(FilterKeys.PortalId, FilterOperators.Equals, portalId.ToString()));

            CatalogListModel catalogList = _catalogClient.GetCatalogs(filters, null);

            if (!(Equals(catalogList, null)) && !(Equals(catalogList.Catalogs, null)))
            {
                catalogId = catalogList.Catalogs[0].CatalogId;
            }

            return catalogId;
        }

        public string SendEmail(out string errorMessage, FilterCollection filters = null)
        {
            try
            {
                errorMessage = string.Empty;
                return _orderClient.SendEmail(filters);
            }
            catch (ZnodeException ex)
            {
                errorMessage = ex.ErrorMessage;
                return string.Empty;
            }
        }

        public PortalSelectListViewModel GetPortalSelectList()
        {
            PortalListViewModel portals = _portalAgent.GetPortals();
            return new PortalSelectListViewModel() { PortalSelectList = PortalViewModelMap.ToSelectListItems(portals.Portals) };
        }
        #endregion

        #region Private Methods

        /// <summary>
        /// Check Null Method
        /// </summary>
        /// <param name="value">The value of sValue</param>
        /// <returns>Returns the string </returns>
        private string CheckNull(string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                return value;
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Sets the filters for searching order data according to the vendor or admin user type.
        /// </summary>
        /// <param name="filters">FilterCollection containing data related to orders.</param>
        /// <returns>Returns the filter collection as required for filtering the order list.</returns>
        private FilterCollection SetFilterForOrderList(FilterCollection filters)
        {
            if (!Equals(filters, null) && filters.Count > 0)
            {
                bool isVendor = false;
                foreach (FilterTuple filter in filters)
                {
                    if (filter.Item1.Equals(FilterKeys.UserType))
                    {
                        isVendor = true;
                        break;
                    }
                }
                foreach (FilterTuple filter in filters)
                {
                    if (isVendor)
                    {
                        if (filter.Item1.ToLower().Equals(FilterKeys.BeginDate))
                        {
                            ReplaceFilterKeyName(ref filters, FilterKeys.BeginDate, FilterKeys.StartDate);
                        }

                    }
                    else
                    {
                        ReplaceFilterKeyName(ref filters, FilterKeys.BeginDate, FilterKeys.OrderDate);
                        ReplaceFilterKeyName(ref filters, FilterKeys.EndDate, FilterKeys.OrderDate);
                        break;
                    }
                }
            }
            return filters;
        }

        /// <summary>
        /// Remove the filter name from filterCollection which is spacified in compare parameter
        /// </summary>
        /// <param name="filters">FilterCollection filters</param>
        /// <param name="compare">string compare</param>
        /// <returns>Returns Updated Filters</returns>
        private FilterCollection RemoveFilters(FilterCollection filters, string compare)
        {
            if (!Equals(filters, null) && ((filters.Count > 0)))
            {
                foreach (var item in filters)
                {
                    if (Equals(item.Item1.ToLower(), compare))
                    {
                        filters.Remove(item);
                        break;
                    }
                }
            }
            return filters;
        }
        #endregion

        #region PRFT Custom Methods

        public bool ResubmitOrderToERP(int orderId, out string errorMessage)
        {
            try
            {
                errorMessage = string.Empty;
                var status = _orderClient.ResubmitOrderToERP(orderId);
                if (!status.IsSubmitted)
                {
                    errorMessage = ZnodeResources.ReOrderFailedMessage + ": " + status.ErrorMessage;
                }

                return status.IsSubmitted;
            }
            catch (ZnodeException ex)
            {
                errorMessage = ex.ErrorMessage;
                return false;
            }
        }

        #endregion

    }
}