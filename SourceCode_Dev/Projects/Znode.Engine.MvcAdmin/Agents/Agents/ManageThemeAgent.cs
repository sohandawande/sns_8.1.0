﻿using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;
using Znode.Libraries.Helpers;

namespace Znode.Engine.MvcAdmin.Agents
{
    public class ManageThemeAgent : BaseAgent, IManageThemeAgent
    {
        private readonly ThemeClient _themeClient;

        public ManageThemeAgent()
        {
            _themeClient = GetClient<ThemeClient>();
        }

        public ThemeListViewModel GetThemeList(FilterCollection filters, SortCollection sorts, int? pageIndex = null, int? recordPerPage = null)
        {
            _themeClient.RefreshCache = true;
            ReplaceFilterKeyName(ref filters, FilterKeys.Name, FilterKeys.ThemeName);
            ThemeListViewModel themeList =  ThemeViewModelMap.ToListViewModel(_themeClient.GetThemes(filters, sorts, pageIndex - 1, recordPerPage));
            return (!Equals(themeList, null) && !Equals(themeList.ThemeList, null)) ? themeList : new ThemeListViewModel(); 
        }

        public bool CreateTheme(ThemeViewModel model, out string message)
        {
            try
            {
                message = string.Empty;
                ThemeModel theme = _themeClient.CreateTheme(ThemeViewModelMap.ToModel(model));
                return true;
            }
            catch (ZnodeException ex)
            {
                message = ex.ErrorMessage;
                return false;
            }
        }

        public ThemeViewModel GetThemenById(int themeId)
        {
            _themeClient.RefreshCache = true;
            var themeModel = _themeClient.GetTheme(themeId);
            return !Equals(themeModel, null) ? ThemeViewModelMap.ToViewModel(themeModel) : new ThemeViewModel();
        }

        public bool UpdateTheme(ThemeViewModel model, out string message)
        {
            try
            {
                message = string.Empty;
                ThemeModel theme = _themeClient.UpdateTheme(model.ThemeId, ThemeViewModelMap.ToModel(model));
                return true;
            }
            catch (ZnodeException ex)
            {
                message = ex.ErrorMessage;
                return false;
            }
        }

        public bool DeleteTheme(int themeId, out string message)
        {
            try
            {
                message = string.Empty;
                return _themeClient.DeleteTheme(themeId);
            }
            catch (ZnodeException ex)
            {
                message = ex.ErrorMessage;
                return false;
            }
        }
    }
}