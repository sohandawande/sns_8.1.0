﻿using Znode.Engine.Api.Client;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    /// <summary>
    /// This is the class for perminssion agent
    /// </summary>
    public class PermissionAgent : BaseAgent, IPermissionAgent
    {

        #region Private Variables
        private readonly IPermissionsClient _permissionClient;
        #endregion

        #region Constructor
        /// <summary>
        /// This is the default constructor
        /// </summary>
        public PermissionAgent()
        {
            _permissionClient = GetClient<PermissionsClient>();
        }

        #endregion

        #region Pulic Properties
        public PermissionViewModel GetAllRolesAndPermissionsByAccountId(int storeAdminID)
        {
            var permissions = _permissionClient.GetAllRolesAndPermissionsByAccountId(storeAdminID);
            return PermissionsViewModelMap.ToViewModel(permissions);
        }

        public bool UpdateRolesAndPermissions(int[] StoreIds, int[] RoleIds, int AccountId)
        {
            return _permissionClient.UpdateRolesAndPermissions(PermissionsViewModelMap.ToModel(AccountId, StoreIds, RoleIds));
        }
        #endregion
    }
}