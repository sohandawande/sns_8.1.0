﻿using Resources;
using System.Web;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    /// <summary>
    /// Tax Class Agent
    /// </summary>
    public class TaxClassAgent : BaseAgent, ITaxClassAgent
    {
        #region Private members

        private readonly ITaxClassesClient _taxClassesClient;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor for the Tax Class
        /// </summary>
        public TaxClassAgent()
        {
            _taxClassesClient = GetClient<TaxClassesClient>();
        }

        #endregion

        #region Public Methods

        public TaxClassListViewModel GetAllTaxClasses(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            _taxClassesClient.RefreshCache = true;
            if (Equals(filters, null))
            {
                filters = new FilterCollection();
            }
            filters.Add(new FilterTuple(FilterKeys.Username, FilterOperators.Equals, "\'"+HttpContext.Current.User.Identity.Name+"\'"));
            TaxClassListModel taxClassList = _taxClassesClient.GetTaxClassList(filters, sortCollection, pageIndex, recordPerPage);
            return (!Equals(taxClassList, null) && !Equals(taxClassList.TaxClasses, null)) ? TaxClassViewModelMap.ToListViewModel(taxClassList, pageIndex, recordPerPage) : new TaxClassListViewModel();
        }

        public TaxClassViewModel GetTaxClassDetails(int taxClassId, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            _taxClassesClient.RefreshCache = true;
            if (Equals(filters, null))
            {
                filters = new FilterCollection();
            }
            filters.Add(new FilterTuple(FilterKeys.TaxClassId, FilterOperators.Equals, taxClassId.ToString()));
            TaxClassModel taxClass = _taxClassesClient.GetTaxClassDetails(filters, sortCollection, pageIndex - 1, recordPerPage);

            foreach (var item in taxClass.TaxRuleList.TaxRules)
            {
                if (string.IsNullOrEmpty(item.CountryCode))
                {
                    item.CountryCode = ZnodeResources.LabelAll;

                }
                if (string.IsNullOrEmpty(item.StateCode))
                {
                    item.StateCode = ZnodeResources.LabelAll;

                }
                if (string.IsNullOrEmpty(item.CountyFips))
                {
                    item.CountyFips = ZnodeResources.LabelAll;
                }
            }
            return (!Equals(taxClass, null)) ? TaxClassViewModelMap.ToViewModel(taxClass) : null;
        }

        public bool SaveTaxClass(TaxClassViewModel model, out int taxId, out string message)
        {
            taxId = 0;
            try
            {
                message = string.Empty;
                var taxClass = _taxClassesClient.CreateTaxClass(TaxClassViewModelMap.ToModel(model));
                taxId = taxClass.TaxClassId;
                return (!Equals(taxClass, null) && (taxClass.TaxClassId > 0));
            }
            catch (ZnodeException ex)
            {
                message = ex.ErrorMessage;
                return false;
            }
        }

        public bool UpdateTaxClass(TaxClassViewModel model, int taxClassId, out string message)
        {
            taxClassId = 0;
            try
            {
                message = string.Empty;
                var updateTaxClass = _taxClassesClient.UpdateTaxClass(model.TaxClassId, TaxClassViewModelMap.ToModel(model));
                return (!Equals(updateTaxClass, null) && updateTaxClass.TaxClassId > 0);
            }
            catch (ZnodeException ex)
            {
                message = ex.ErrorMessage;
                return false;
            }
        }

        public bool DeleteTaxClass(int taxClassId, out string message)
        {
            try
            {
                message = string.Empty;
                return _taxClassesClient.DeleteTaxClass(taxClassId);
            }
            catch (ZnodeException ex)
            {
                message = ZnodeResources.DeleteTaxClassErrorMessage;
                return false;
            }
        }

        #endregion
    }
}