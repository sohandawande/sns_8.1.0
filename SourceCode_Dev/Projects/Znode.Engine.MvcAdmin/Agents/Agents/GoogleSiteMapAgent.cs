﻿using Resources;
using System.Collections.Generic;
using System.Web.WebPages.Html;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    /// <summary>
    /// Google Site Map Agent
    /// </summary>
    public class GoogleSiteMapAgent : BaseAgent, IGoogleSiteMapAgent
    {
        #region Private Variables

        private readonly IGoogleSiteMapClient _googleSiteMapClient;
        private readonly IPermissionAgent _permissionAgent;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor for Google Site Map Agent
        /// </summary>
        public GoogleSiteMapAgent()
        {
            _googleSiteMapClient = GetClient<GoogleSiteMapClient>();
            _permissionAgent = new PermissionAgent();
        }

        #endregion

        #region Public Methods

        #region Frequency List

        public List<SelectListItem> GetFrequencyList()
        {
            return GoogleSiteMapViewModelMap.ToFrequencyListItems();
        }
 
        #endregion

        #region Priority List

        public List<SelectListItem> GetPriorityList()
        {
            return GoogleSiteMapViewModelMap.ToPriorityListItems();
        } 

        #endregion

        #region XML Site Map List

        public List<SelectListItem> GetXMLSiteMapList()
        {
            return GoogleSiteMapViewModelMap.ToXMLSiteMapListItems();
        } 

        #endregion

        #region Last Modified Date List

        public List<SelectListItem> GetLastDateList()
        {
            return GoogleSiteMapViewModelMap.ToLastModificationListItems();
        } 

        #endregion

        #region XML Site Map Type List 

        public List<SelectListItem> GetXMLSiteMapTypeList()
        {
            return GoogleSiteMapViewModelMap.ToXMLSiteMapTypeListItems();
        }

        #endregion

        #region Bind All List

        public GoogleSiteMapViewModel BindViewModel()
        {
            GoogleSiteMapViewModel model = new GoogleSiteMapViewModel();
            model.Stores = new ProfilePortalViewModel();

            model.FrequencyList = GetFrequencyList();
            model.PriorityList = GetPriorityList();
            model.PriorityList.Insert(0, new SelectListItem() { Value = "0", Text = @ZnodeResources.GoogleSiteMapSelectPriority });
            model.XMLSiteMapList = GetXMLSiteMapList();
            model.LastModificationList = GetLastDateList();
            model.XMLSiteMapTypeList = GetXMLSiteMapTypeList();

            model.Stores.PortalList = _permissionAgent.GetAllRolesAndPermissionsByAccountId(0).Stores.PortalList;
            return model;
        } 

        #endregion

        public GoogleSiteMapViewModel CreateGoogleSiteMap(GoogleSiteMapViewModel model, string domainName, out string message)
        {
            try
            {
                message = string.Empty;
                model.SuccessXMLGenerationMessage = domainName;                
                GoogleSiteMapModel googleSiteMap = _googleSiteMapClient.CreateGoogleSiteMap(GoogleSiteMapViewModelMap.ToModel(model));   
                return GoogleSiteMapViewModelMap.ToViewModel(googleSiteMap);
            }
            catch (ZnodeException ex)
            {
                message = ex.ErrorMessage;
                return model;
            }
        } 

        #endregion
    }
}