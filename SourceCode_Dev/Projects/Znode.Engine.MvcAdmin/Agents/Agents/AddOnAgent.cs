﻿using System.Linq;
using System.Web;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    /// <summary>
    /// AddOn Agent
    /// </summary>
    public class AddOnAgent : BaseAgent, IAddOnAgent
    {
        #region Private Variables

        private readonly IAddOnClient _addOnClient;
        private readonly ILocaleClient _localeClient;
        #endregion

        #region Constructor
        public AddOnAgent()
        {
            _addOnClient = GetClient<AddOnClient>();
            _localeClient = GetClient<LocaleClient>();
        }
        #endregion

        #region Public Methods
        public AddOnListViewModel GetAddOns(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            _addOnClient.RefreshCache = true;
            if (Equals(filters, null))
            {
                filters = new FilterCollection();
            }
            filters.Add(new FilterTuple(FilterKeys.Username, FilterOperators.Equals, string.Format("'{0}'", HttpContext.Current.User.Identity.Name)));
            AddOnListModel list = _addOnClient.GetAddOns(new ExpandCollection { ExpandKeys.AddOns }, filters, sortCollection, pageIndex, recordPerPage);
            return Equals(list, null) ? new AddOnListViewModel() : AddOnViewModelMap.ToListViewModel(list);
        }

        public bool DeleteAddOn(int addOnId, out string errorMessage)
        {
            try
            {
                errorMessage = string.Empty;
                return _addOnClient.DeleteAddOn(addOnId);
            }
            catch (ZnodeException ex)
            {
                errorMessage = ex.ErrorMessage;
                return false;
            }
        }

        public AddOnViewModel GetAddOn(int addOnId)
        {
            _addOnClient.RefreshCache = true;
            AddOnModel addOnModel = _addOnClient.GetAddOn(addOnId);
            return (!Equals(addOnModel, null)) ? AddOnViewModelMap.ToViewModel(addOnModel) : null;
        }

        public bool Create(AddOnViewModel model, out int addonId)
        {
            AddOnModel addOnModel = _addOnClient.CreateAddOn(AddOnViewModelMap.ToModel(model));
            addonId = addOnModel.AddOnId;
            return (!Equals(addOnModel, null) && (addOnModel.AddOnId > 0));
        }

        public bool Update(AddOnViewModel model)
        {
            AddOnModel updatedAddOn = _addOnClient.UpdateAddOn(model.AddOnId, AddOnViewModelMap.ToModel(model));
            return (!Equals(updatedAddOn, null) && updatedAddOn.AddOnId > 0);
        }

        public AddOnViewModel SetOutOfStockOption(AddOnViewModel model)
        {
            if (Equals(model.OutofStockOptions, InventoryOptionsState.DisablePurchasingForOutOfStockProducts))
            {
                model.TrackInventory = true;
                model.AllowBackOrder = false;
            }
            else if (Equals(model.OutofStockOptions, InventoryOptionsState.AllowBackOrderingOfProducts))
            {
                model.TrackInventory = true;
                model.AllowBackOrder = true;
            }
            else
            {
                model.TrackInventory = false;
                model.AllowBackOrder = false;
            }
            return model;
        }

        public AddOnViewModel GetOutOfStockOption(AddOnViewModel model)
        {
            model.OutofStockOptions = (Equals(model.TrackInventory, true) && Equals(model.AllowBackOrder, false)) ? InventoryOptionsState.DisablePurchasingForOutOfStockProducts :
                (Equals(model.TrackInventory, true) && Equals(model.AllowBackOrder, true)) ? InventoryOptionsState.AllowBackOrderingOfProducts : InventoryOptionsState.DontTrackInventory;
            return model;
        }

        public AddOnViewModel GetInventorySetting(AddOnViewModel model)
        {
            if (Equals(model.TrackInventory, true) && Equals(model.AllowBackOrder, false))
            {
                model.InventoryEnabled = true;
                model.AllowBackOrder = false;
                model.TrackInventory = false;
            }
            else if (Equals(model.TrackInventory, true) && Equals(model.AllowBackOrder, true))
            {
                model.InventoryEnabled = false;
                model.AllowBackOrder = true;
                model.TrackInventory = false;
            }
            else
            {
                model.InventoryEnabled = false;
                model.AllowBackOrder = false;
                model.TrackInventory = true;
            }
            return model;
        }

        public string GetLocaleNameByLocaleId(int localeId)
        {
            FilterCollection filters = new FilterCollection();
            filters.Add(new FilterTuple(FilterKeys.LocaleId, FilterOperators.Equals, localeId.ToString()));
            LocaleListModel localeList = _localeClient.GetLocales(null, filters, null);
            if (!Equals(localeList, null) && !Equals(localeList.Locales, null))
            {
                return localeList.Locales.FirstOrDefault().LocaleDescription;
            }
            return string.Empty;
        }


        #endregion
    }
}