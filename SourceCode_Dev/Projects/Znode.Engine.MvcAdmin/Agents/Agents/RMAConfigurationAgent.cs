﻿using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public class RMAConfigurationAgent : BaseAgent, IRMAConfigurationAgent
    {
        #region Private Variables
        private readonly IRMAConfigurationClient _rmaConfigurationClient;
        private readonly IRequestStatusClient _requestStatusClient;
        #endregion

        #region Constructor
        public RMAConfigurationAgent()
        {
            _rmaConfigurationClient = GetClient<RMAConfigurationClient>();
            _requestStatusClient = GetClient<RequestStatusClient>();
        }
        #endregion

        #region Public Region

        #region RMA Configuration
        public RMAConfigurationViewModel GetRMAConfigurationsByRMAConfigId(int rmaConfigId)
        {
            RMAConfigurationViewModel model = RMAConfigurationViewModelMap.ToViewModel(_rmaConfigurationClient.GetRMAConfiguration(rmaConfigId));
            return !Equals(model, null) ? model : new RMAConfigurationViewModel();
        }


        public RMAConfigurationListViewModel GetRMAConfigurations()
        {
            _rmaConfigurationClient.RefreshCache = true;
            RMAConfigurationListViewModel rmaConfigurations = RMAConfigurationViewModelMap.ToListViewModel(_rmaConfigurationClient.GetRMAConfigurations());
            return (!Equals(rmaConfigurations, null) && !Equals(rmaConfigurations.RMAConfigurations, null) ? rmaConfigurations : new RMAConfigurationListViewModel());
        }

        public bool CreateRMAConfiguration(RMAConfigurationViewModel model)
        {
            RMAConfigurationViewModel rmaConfiguration = RMAConfigurationViewModelMap.ToViewModel(_rmaConfigurationClient.CreateRMAConfiguration(RMAConfigurationViewModelMap.ToModel(model)));
            return (rmaConfiguration.RMAConfigId > 0) ? true : false;
        }

        public bool UpdateRMAConfiguration(RMAConfigurationViewModel model)
        {
            RMAConfigurationViewModel rmaConfiguration = RMAConfigurationViewModelMap.ToViewModel(_rmaConfigurationClient.UpdateRMAConfiguration(model.RMAConfigId, RMAConfigurationViewModelMap.ToModel(model)));
            return (!Equals(rmaConfiguration, null)) ? true : false;
        }
        #endregion

        #region Request Status
        public RequestStatusListViewModel GetRequestStatusList(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            _requestStatusClient.RefreshCache = true;
            RequestStatusListViewModel requestStatusList = RMAConfigurationViewModelMap.ToListViewModel(_requestStatusClient.GetRequestStatus(null, filters, sortCollection, pageIndex - 1, recordPerPage));
            return (!Equals(requestStatusList, null) && !Equals(requestStatusList.RequestStatusList, null)) ? requestStatusList : new RequestStatusListViewModel();

        }

        public RequestStatusViewModel GetRequestStatus(int requestStatusId)
        {
            _requestStatusClient.RefreshCache = true;
            RequestStatusViewModel requestStatus = RequestStatusViewModelMap.ToViewModel(_requestStatusClient.GetRequestStatus(requestStatusId));
            return !Equals(requestStatus, null) ? requestStatus : new RequestStatusViewModel();
        }

        public bool UpdateRequestStatus(RequestStatusViewModel model)
        {
            RequestStatusViewModel requestStatus = RequestStatusViewModelMap.ToViewModel(_requestStatusClient.UpdateRequestStatus(model.RequestStatusID, RequestStatusViewModelMap.ToModel(model)));
            return (!Equals(requestStatus, null)) ? true : false;
        }

        public bool DeleteRequestStatus(int requestStatusId)
        {
            return _requestStatusClient.DeleteRequestStatus(requestStatusId);
        }
        #endregion

        #endregion
    }
}