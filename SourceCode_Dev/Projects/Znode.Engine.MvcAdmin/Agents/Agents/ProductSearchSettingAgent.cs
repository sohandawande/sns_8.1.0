﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public class ProductSearchSettingAgent : BaseAgent, IProductSearchSettingAgent
    {
        #region Private Variables
        private readonly IProductSearchSettingClient _productSearchSettingClient;
        private readonly ICategoriesClient _categoryClient;
        private readonly ICatalogsClient _catalogClient;
        private readonly IManufacturersClient _manufacturersClient;
        private readonly IProductTypeClient _productTypeClient;

        #endregion

        #region Constructor
        public ProductSearchSettingAgent()
        {
            _categoryClient = GetClient<CategoriesClient>();
            _productSearchSettingClient = GetClient<ProductSearchSettingClient>();
            _catalogClient = GetClient<CatalogsClient>();
            _manufacturersClient = GetClient<ManufacturersClient>();
            _productTypeClient = GetClient<ProductTypeClient>();
        }
        #endregion

        #region public Methods

        public ProductLevelSettingListViewModel GetProductLevelSettings(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            _productSearchSettingClient.RefreshCache = true;
            ProductLevelSettingListModel list = _productSearchSettingClient.GetProductLevelSettings(null, filters, sortCollection, pageIndex, recordPerPage);
            if (!Equals(list, null))
            {
                ProductLevelSettingListViewModel productLevelSettingListViewModel = ProductSearchSettingViewModelMap.ToListViewModel(list);
                HttpContext.Current.Session[MvcAdminConstants.BoostValuesListCache] = productLevelSettingListViewModel;
                return productLevelSettingListViewModel;
            }
            else
            {
                return new ProductLevelSettingListViewModel();
            }
        }


        public CategoryLevelSettingListViewModel GetCategoryLevelSettings(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            _productSearchSettingClient.RefreshCache = true;
            CategoryLevelSettingListModel list = _productSearchSettingClient.GetCategoryLevelSettings(null, filters, sortCollection, pageIndex, recordPerPage);
            if (!Equals(list, null))
            {
                CategoryLevelSettingListViewModel categoryLevelSettingListViewModel = ProductSearchSettingViewModelMap.ToListViewModel(list);
                HttpContext.Current.Session[MvcAdminConstants.BoostValuesListCache] = categoryLevelSettingListViewModel;
                return categoryLevelSettingListViewModel;
            }
            else
            {
                return new CategoryLevelSettingListViewModel();
            }
        }

        public FieldLevelSettingListViewModel GetFieldLevelSettings(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            if (Equals(sortCollection, null))
            {
                sortCollection = new SortCollection();
                sortCollection.Add(SortDirections.Descending, SortKeys.Boost);
            }
            filters.Add(new FilterTuple(FilterKeys.FieldBoostable, FilterOperators.Equals, "True"));
            _productSearchSettingClient.RefreshCache = true;
            FieldLevelSettingListModel list = _productSearchSettingClient.GetFieldLevelSettings(null, filters, sortCollection, pageIndex - 1, recordPerPage);
            if (!Equals(list, null))
            {
                FieldLevelSettingListViewModel fieldLevelSettingListViewModel = ProductSearchSettingViewModelMap.ToListViewModel(list);
                HttpContext.Current.Session[MvcAdminConstants.BoostValuesListCache] = fieldLevelSettingListViewModel;
                return fieldLevelSettingListViewModel;
            }
            else
            {
                return new FieldLevelSettingListViewModel();
            }
        }

        public bool SaveProductBoostValues(string[] ids, string[] values, string name)
        {
            var model = ProductSearchSettingViewModelMap.ToBoostModel(ids, values, name);
           
            var result = _productSearchSettingClient.SaveBoostValues(model);

            return result;
        }

        #endregion

        #region Bind Dropdowns
        public List<CatalogModel> GetCatalogList()
        {
            _catalogClient.RefreshCache = true;
            CatalogListModel catalogList = _catalogClient.GetCatalogs(null, null);
            return (!Equals(catalogList, null) && !Equals(catalogList.Catalogs, null)) ? catalogList.Catalogs.ToList() : new List<CatalogModel>();
        }

        public List<CategoryModel> GetCategoryList()
        {
            _categoryClient.RefreshCache = true;
            CategoryListModel categoryList = _categoryClient.GetCategories(null, null, null);
            return (!Equals(categoryList, null) && !Equals(categoryList.Categories, null)) ? categoryList.Categories.ToList() : new List<CategoryModel>();
        }

        public List<ManufacturerModel> GetManufacturerList()
        {
            _manufacturersClient.RefreshCache = true;
            ManufacturerListModel manufacturerList = _manufacturersClient.GetManufacturers(null, null);
            return (!Equals(manufacturerList, null) && !Equals(manufacturerList.Manufacturers, null)) ? manufacturerList.Manufacturers.ToList() : new List<ManufacturerModel>();
        }

        public List<ProductTypeModel> GetProductTypeList()
        {
            _productTypeClient.RefreshCache = true;
            ProductTypeListModel productTypeList = _productTypeClient.GetProductTypes(null, null, null);
            return (!Equals(productTypeList, null) && !Equals(productTypeList.ProductTypes, null)) ? productTypeList.ProductTypes.ToList() : new List<ProductTypeModel>();
        }

        #endregion
    }
}