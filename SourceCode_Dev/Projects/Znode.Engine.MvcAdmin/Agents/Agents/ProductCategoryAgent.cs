﻿using Znode.Engine.Api.Client;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public class ProductCategoryAgent : BaseAgent, IProductCategoryAgent
    {
        #region Private variables
        private readonly IProductCategoriesClient _productCategoryClient; 
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor for ProductCategoryAgent
        /// </summary>
        public ProductCategoryAgent()
        {
            _productCategoryClient = GetClient<ProductCategoriesClient>();
        }  
        #endregion       

        #region Public Methods
        public ProductCategoryViewModel GetProductCategory(int productCategoryId)
        {
            ProductCategoryViewModel model = new ProductCategoryViewModel();

            model = ProductCategoryViewModelMap.ToViewModel(_productCategoryClient.GetProductCategory(productCategoryId));
            return (!Equals(model, null)) ? model : null;
        }

        public bool CreateProductCategory(ProductCategoryViewModel model)
        {
            if (!Equals(model, null))
            {
                var value = _productCategoryClient.CreateProductCategory(ProductCategoryViewModelMap.ToModel(model));
                return true;
            }
            else
                return false;
        }

        public bool UpdateProductCategory(ProductCategoryViewModel model)
        {
            var productCategory = _productCategoryClient.UpdateProductCategory(model.ProductCategoryId, ProductCategoryViewModelMap.ToModel(model));

            if (!Equals(productCategory, null) && productCategory.ProductCategoryId > 0)
            {
                return true;
            }
            return false;
        }

        public bool DeleteProductCategory(int productCategoryId)
        {
            return _productCategoryClient.DeleteProductCategory(productCategoryId);
        } 
        #endregion
    }
}