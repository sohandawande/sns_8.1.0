﻿using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public class ProductTypeAttributeAgent : BaseAgent , IProductTypeAttributeAgent
    {
        #region Public Variables
        private readonly IProductTypeAttributeClient _productTypeAttributeClient;
        private readonly IProductsClient _productsClient;
        private readonly IAttributeTypesClient _attributeTypesClient; 
        #endregion

        #region Public Constructor
        public ProductTypeAttributeAgent()
        {
            _productTypeAttributeClient = GetClient<ProductTypeAttributeClient>();
            _productsClient = GetClient<ProductsClient>();
            _attributeTypesClient = GetClient<AttributeTypesClient>();
        } 
        #endregion

        #region Public Methods
        public ProductTypeAssociatedAttributeTypesListViewModel GetAttributesByProductTypeId(int productTypeAttributeId, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            filters.Add(new FilterTuple(FilterKeys.ProductTypeId, FilterOperators.Equals, productTypeAttributeId.ToString()));
            _productTypeAttributeClient.RefreshCache = true;
            ProductTypeAssociatedAttributeTypesListModel modelList = _productTypeAttributeClient.GetAttributeTypesByProductTypeIdService(productTypeAttributeId,null, filters, sortCollection, pageIndex, recordPerPage);
            
            return Equals(modelList, null) ? null : ProductTypeAttributeViewModelMap.ToListViewModel(modelList, pageIndex, recordPerPage, modelList.TotalPages, modelList.TotalResults);
        }

        public bool IsProductAssociatedProductType(int productTypeId)
        {
            _productsClient.RefreshCache = true;
            ProductListModel products = _productsClient.GetProducts(null, new FilterCollection() { new FilterTuple(FilterKeys.ProductTypeId, FilterOperators.Equals, productTypeId.ToString()) }, null);
            return !Equals(products, null) && !Equals(products.Products, null) && products.Products.Count > 0;
        }

        public bool IsAttributeAssociatedProductType(int productTypeId)
        {
            _productTypeAttributeClient.RefreshCache = true;
            ProductTypeAttributeListModel isAssociated = _productTypeAttributeClient.GetAttributeTypes(null, new FilterCollection() { new FilterTuple(FilterKeys.ProductTypeId, FilterOperators.Equals, productTypeId.ToString()) }, null);
            return !Equals(isAssociated, null) && !Equals(isAssociated.ProductTypeAttribute, null) && isAssociated.ProductTypeAttribute.Count > 0;
        }

        public AttributeTypesListViewModel GetAttributeTypes()
        {
            AttributeTypesListViewModel model = new AttributeTypesListViewModel();
            _attributeTypesClient.RefreshCache = true;
            model.AttributeTypeList = AttributeTypesViewModelMap.ToListItems(_attributeTypesClient.GetAttributeTypes(Filters, Sorts,null,null).AttributeTypes);
            return Equals(model,null) ? null : model;
        }

        public bool DeleteAttributes(int attributeTypeId)
        {
            return _attributeTypesClient.DeleteAttributeType(attributeTypeId);
        }

        public bool DeleteAttributeType(int attributeTypeId)
        {
            return _productTypeAttributeClient.DeleteAttributeType(attributeTypeId);
        }

        public bool CreateProductType(ProductTypeAssociatedAttributeTypesViewModel viewModel, out string message)
        {
            try
            {
                message = string.Empty;
                ProductTypeAssociatedAttributeTypesModel model = ProductTypeAttributeViewModelMap.ToModel(viewModel);
                ProductTypeAttributeModel productType = _productTypeAttributeClient.CreateAttributeType(model);
                return (productType.ProductTypeId > 0);
            }
            catch (ZnodeException ex)
            {
                message = ex.ErrorMessage;
                return false;
            }
        }
        #endregion
    }
}