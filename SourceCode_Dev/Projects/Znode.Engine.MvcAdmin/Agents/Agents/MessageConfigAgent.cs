﻿using Resources;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;
using Znode.Engine.MvcDemo.Maps;

namespace Znode.Engine.MvcAdmin.Agents
{
    /// <summary>
    ///  Message Config Agent
    /// </summary>
    public class MessageConfigAgent : BaseAgent, IMessageConfigAgent
    {
        #region Private Variables

        private readonly IMessageConfigsClient _messageConfigClient;
        private readonly IPortalsClient _portalClient;
        private readonly IPortalAgent _portalAgent;

        #endregion

        #region Constructors

        /// <summary>
        /// Default Constructor.
        /// </summary>
        public MessageConfigAgent()
        {
            _messageConfigClient = GetClient<MessageConfigsClient>();
            _portalClient = GetClient<PortalsClient>();
            _portalAgent = new PortalAgent();
        }

        #endregion

        #region Public Methods

        #region Manage Message

        public ManageMessageListViewModel GetMessageConfigs(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            _messageConfigClient.RefreshCache = true;
            if (Equals(filters, null))
            {
                filters = new FilterCollection();
            }
            filters.Add(new FilterTuple(FilterKeys.Username, FilterOperators.Contains, HttpContext.Current.User.Identity.Name));
            filters.Add(FilterKeys.MessageTypeId, FilterOperators.Equals, ((int)MessageType.MessageBlock).ToString());
            MessageConfigListModel _messageList = _messageConfigClient.GetMessageConfigs(new ExpandCollection { ExpandKeys.Portal }, filters, sortCollection, pageIndex - 1, recordPerPage);

            ManageMessageListViewModel manageMessageList = ManageMessageViewModelMap.ToListViewModel(_messageList, pageIndex, recordPerPage);

            manageMessageList.Portals = ManageMessageViewModelMap.ToListItems(_portalClient.GetPortals(Expands, Filters, new SortCollection()).Portals);

            return manageMessageList;
        }

        public ManageMessageViewModel GetMessageConfig(int messageConfigId)
        {
            _messageConfigClient.RefreshCache = true;
            ManageMessageViewModel manageMessageDetails = ManageMessageViewModelMap.ToViewModel(_messageConfigClient.GetMessageConfig(messageConfigId));
            return manageMessageDetails;
        }

        public bool DeleteMessageConfig(int messageConfigId)
        {
            return _messageConfigClient.DeleteMessageConfig(messageConfigId);
        }

        public bool SaveMessageConfig(ManageMessageViewModel model, out string message)
        {
            try
            {
                message = string.Empty;
                MessageConfigModel messageConfigModel = _messageConfigClient.CreateMessageConfig(ManageMessageViewModelMap.ToModel(model));
                return (messageConfigModel.MessageConfigId > 0) ? true : false;
            }
            catch (ZnodeException ex)
            {
                message = ex.ErrorMessage;
                return false;
            }
        }

        public bool UpdateMessageConfig(int messageConfigId, ManageMessageViewModel model)
        {
            MessageConfigModel messageConfigModel = _messageConfigClient.UpdateMessageConfig(model.MessageConfigId, ManageMessageViewModelMap.ToModel(model));
            return Equals(messageConfigModel, null) ? false : true;
        }

        #endregion

        #region Manage Banner

        public List<SelectListItem> GetAllBannerKeys()
        {
            List<SelectListItem> lstBannerKeys = new List<SelectListItem>();

            //Set the filter condition for banner.
            Filters = new FilterCollection { { FilterKeys.MessageTypeId, FilterOperators.Equals, ((int)MessageType.Banner).ToString() } };
            _messageConfigClient.RefreshCache = true;
            //Get all the banners.
            var messageList = _messageConfigClient.GetMessageConfigs(null, Filters, null);

            if (!Equals(messageList, null) && !Equals(messageList.MessageConfigs, null) && messageList.MessageConfigs.Count > 0)
            {
                //Gets the distinct keys from all banner to be display in dropdown.
                var bannerKeys = (from item in messageList.MessageConfigs
                                  where !string.IsNullOrEmpty(item.Key)
                                  select item.Key).Distinct().ToList();

                lstBannerKeys = (from item in bannerKeys
                                 select new SelectListItem
                                 {
                                     Text = item,
                                     Value = item,
                                 }).ToList();
            }
            //Insert default items in Location drop down. 
            if (lstBannerKeys.Count > 0)
            {
                lstBannerKeys.Insert(0, new SelectListItem { Text = ZnodeResources.LabelBannerDoNotShowText, Value = MvcAdminConstants.BannerDoNotShowValue });
                lstBannerKeys.Add(new SelectListItem { Text = ZnodeResources.LabelBannerAddNewLocationText, Value = MvcAdminConstants.BannerAddNewLocationValue });
            }

            return lstBannerKeys;
        }

        public ManageMessageListViewModel GetAllBanners(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            _messageConfigClient.RefreshCache = true;
            filters.Add(new FilterTuple(FilterKeys.Username, FilterOperators.Contains, HttpContext.Current.User.Identity.Name));
            filters.Add(FilterKeys.MessageTypeId, FilterOperators.Equals, ((int)MessageType.Banner).ToString());

            //Get All the Banners
            var messageList = _messageConfigClient.GetMessageConfigs(new ExpandCollection { ExpandKeys.Portal }, filters, sortCollection, pageIndex - 1, recordPerPage);
            ManageMessageListViewModel manageViewModels = ManageMessageViewModelMap.ToListViewModel(messageList);

            manageViewModels.Portal = PortalViewModelMap.ToListViewModel(_portalClient.GetPortals(null, null, null), null, null).Portals;

            return manageViewModels;
        }

        public bool CreateBanner(ManageBannerViewModel model)
        {
            //Create the banner & return its details.
            var message = _messageConfigClient.CreateMessageConfig(ManageBannerViewModelMap.ToModel(model));
            return (!Equals(message, null) && message.MessageConfigId > 0);
        }

        public ManageBannerViewModel GetBanner(int messageId)
        {
            _messageConfigClient.RefreshCache = true;
            //Get the banner based on messageId.
            ManageBannerViewModel model = ManageBannerViewModelMap.ToViewModel(_messageConfigClient.GetMessageConfig(messageId));

            //Bind all portals.
            model.Portal = GetPortals(model.PortalID);

            //Bind all banner keys, if exists.
            model.BannerKeys = GetAllBannerKeys();
            model.BannerKeys.All(x => x.Selected = Equals(x.Value, model.Key));
            return model;
        }

        public bool UpdateBanner(int messageId, ManageBannerViewModel model)
        {
            //Updates the banner & return its details.
            var banner = _messageConfigClient.UpdateMessageConfig(messageId, ManageBannerViewModelMap.ToModel(model));
            return (!Equals(banner, null) && banner.MessageConfigId > 0);
        }

        #endregion

        public List<SelectListItem> GetPortals(int portalId = 0)
        {
            //Get the portals & Map with view model.
            return ManageBannerViewModelMap.GetSelectListItem(_portalAgent.GetPortals(null, null).Portals.ToList(), portalId);
        }

        public List<PortalModel> BindDropDown()
        {
            return _portalClient.GetPortals(null, null, null).Portals.ToList();
        }

        public List<PortalViewModel> GetPortalsList()
        {
            PortalListViewModel portalListModel = new PortalListViewModel();
            _portalClient.RefreshCache = true;

            portalListModel = _portalAgent.GetPortals();

            return !Equals(portalListModel, null) && !Equals(portalListModel.Portals, null) ? (portalListModel.Portals.ToList()) : new List<PortalViewModel>();
        }

        #endregion
    }
}