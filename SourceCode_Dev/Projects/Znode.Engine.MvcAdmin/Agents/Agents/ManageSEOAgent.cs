﻿using Resources;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    /// <summary>
    /// Agent for ManageSEO
    /// </summary>
    public class ManageSEOAgent : BaseAgent, IManageSEOAgent
    {
        #region Private Variables
        private readonly IPortalsClient _portalsClient;
        private readonly IProductsClient _productsClient;
        private readonly ICatalogsClient _catalogClient;
        private readonly IContentPageClient _contentPageClient;
        private readonly ICategoriesClient _categoryClient;
        private readonly IPortalAgent _portalAgent;
        private readonly ISearchClient _searchClient;
        private int localeId = MvcAdminConstants.LocaleId;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor for portal agent.
        /// </summary>
        public ManageSEOAgent()
        {
            _portalsClient = GetClient<PortalsClient>();
            _productsClient = GetClient<ProductsClient>();
            _catalogClient = GetClient<CatalogsClient>();
            _contentPageClient = GetClient<ContentPageClient>();
            _categoryClient = GetClient<CategoriesClient>();
            _portalAgent = new PortalAgent();
            _searchClient = GetClient<SearchClient>();
        }
        #endregion

        #region Public Properties

        #region Default Settings

        public ManageSEOViewModel GetPortalById(int portalId)
        {
            _portalsClient.RefreshCache = true;
            PortalModel model = _portalsClient.GetPortal(portalId);
            return model != null ? ManageSEOViewModelMap.ToViewModel(model) : null;
        }

        public List<SelectListItem> BindPortalList()
        {
            _portalsClient.RefreshCache = true;
            PortalListViewModel list = _portalAgent.GetPortals();
            return !Equals(list,null) ? ManageSEOViewModelMap.ToSelectListItems( list.Portals) : new List<SelectListItem>();
        }

        #endregion

        #region Products
        public ManageSEOProductViewModel GetProductSEODetails(int productId)
        {
            _portalsClient.RefreshCache = true;
            var product = _productsClient.GetProductDetailsByProductId(productId);
            if (Equals(product, null))
            {
                return null;
            }
            var viewModel = ManageSEOViewModelMap.ToViewModel(product);
            return viewModel;
        }

        public bool UpdateProductSEODetails(ManageSEOProductViewModel viewmodel)
        {
            _portalsClient.RefreshCache = true;
            ProductModel model = _productsClient.GetProduct(viewmodel.ProductId);
            model.AdditionalInfo = Equals(viewmodel.ShippingInformation, null) ? string.Empty : HttpUtility.HtmlEncode(viewmodel.ShippingInformation);
            model.Specifications = Equals(viewmodel.ProductSpecifications, null) ? string.Empty : HttpUtility.HtmlEncode(viewmodel.ProductSpecifications);
            model.Description = Equals(viewmodel.LongDescription, null) ? string.Empty : HttpUtility.HtmlEncode(viewmodel.LongDescription);
            model.FeaturesDescription = Equals(viewmodel.FeaturesDescription, null) ? string.Empty : HttpUtility.HtmlEncode(viewmodel.FeaturesDescription);
            model.ShortDescription = Equals(viewmodel.ShortDescription, null) ? string.Empty : HttpUtility.HtmlEncode(viewmodel.ShortDescription);
            model.SeoDescription = Equals(viewmodel.SeoDescription, null) ? string.Empty : HttpUtility.HtmlEncode(viewmodel.SeoDescription);
            model.SeoKeywords = Equals(viewmodel.SeoKeywords, null) ? string.Empty : HttpUtility.HtmlEncode(viewmodel.SeoKeywords);
            model.SeoTitle = Equals(viewmodel.SeoTitle, null) ? string.Empty : HttpUtility.HtmlEncode(viewmodel.SeoTitle);
            model.SeoUrl = Equals(viewmodel.SeoPageUrl, null) ? string.Empty : HttpUtility.HtmlEncode(viewmodel.SeoPageUrl);
            model.Name = Equals(viewmodel.ProductName, null) ? string.Empty : viewmodel.ProductName;
            model.RedirectUrlInd = viewmodel.RedirectUrlInd;

            var product = _productsClient.UpdateProductSEOInformation(viewmodel.ProductId, model);
            return product.ProductId > 0 ? true : false;
        }

        #endregion

        #region Category

        public ManageSEOCategoryViewModel GetCategory(int categoryId)
        {
            _categoryClient.RefreshCache = true;
            var category = _categoryClient.GetCategory(categoryId);
            return Equals(category, null) ? null : ManageSEOViewModelMap.ToViewModel(category);
        }

        #endregion

        #region Content Page

        public bool UpdateContentPageSEOInformation(ContentPageViewModel updatedModel, out string errorMessage)
        {
            errorMessage = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(updatedModel.SEOFriendlyPageName) && _searchClient.IsRestrictedSeoUrl(updatedModel.SEOFriendlyPageName))
                {
                    errorMessage = ZnodeResources.ErrorRestrictedSeoUrlExists;
                    return false;
                }
                _contentPageClient.RefreshCache = true;
                ContentPageViewModel orignalModel = ContentPageViewModelMap.ToViewModel(_contentPageClient.GetContentPage(updatedModel.ContentPageId));

                orignalModel.UpdatedUser = HttpContext.Current.User.Identity.Name;
                orignalModel.LocaleId = localeId;
                orignalModel.AllowDelete = true;
                orignalModel.ActiveInd = true;

                orignalModel.SEOFriendlyPageName = updatedModel.SEOFriendlyPageName;
                orignalModel.SEOTitle = updatedModel.SEOTitle;
                orignalModel.SEOKeywords = updatedModel.SEOKeywords;
                orignalModel.SEODescription = updatedModel.SEODescription;
                orignalModel.PageTitle = updatedModel.PageTitle;
                orignalModel.Html = updatedModel.Html;

                return _contentPageClient.UpdateContentPage(orignalModel.ContentPageId, ContentPageViewModelMap.ToModel(orignalModel));
            }
            catch (ZnodeException ex)
            {
                errorMessage = ex.ErrorMessage;
                return false;
            }
            catch (Exception ex)
            {
            }
            return false;
        }

        #endregion

        #endregion
    }
}