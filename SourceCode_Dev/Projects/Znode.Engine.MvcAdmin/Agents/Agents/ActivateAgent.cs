﻿using Resources;
using System;
using System.Web;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.MvcAdmin.Agents.IAgents;
using Znode.Engine.MvcAdmin.ViewModels;
using Znode.Libraries.Helpers.Constants;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.MvcAdmin.Agents
{
    public class ActivateAgent : BaseAgent, IActivateAgent
    {
        #region Install license

        /// <summary>
        /// This method is used to install the License
        /// This creates the .dlsc file in the Licence folder on the server specified the model
        /// This method internally call the ZNode.Libraries.Framework.Business.ZNodeLicenseManager class to install the license
        /// </summary>
        /// <param name="model">ActivatePageApiModel which should contain the type of the license user want to activate, and detail information of the user</param>
        /// <returns>This method returns the ActivateResponse which will contain the status of the activation of license along with error message if present</returns>
        public ActivateLicenseResponse InstallLicense(ActivatePageApiModel model)
        {
            string domainName = string.Concat(HttpContext.Current.Request.Url.Host, HttpContext.Current.Request.ApplicationPath);
            string errorMessage = string.Empty;
            string customerIpAddress = HttpContext.Current.Request.UserHostAddress;
            ZNodeLicenseManager licenseMgr = new ZNodeLicenseManager();
            ActivateLicenseResponse response = new ActivateLicenseResponse();
            try
            {
                response.HasError = !licenseMgr.InstallLicense(GetLicenseType(model.LicenseType.ToString()), model.SerialNumber, model.FullName, model.Email, out errorMessage);
                response.ErrorMessage = errorMessage;
                if (!response.HasError)
                {
                    ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.ActivationSuccess, customerIpAddress, domainName, model.FullName, model.Email, RZnodeResources.LicenseActivatedSuccess);
                }
                else
                {
                    ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.ActivationFailed, customerIpAddress, domainName, model.FullName, model.Email, RZnodeResources.LicenseActivatedError);
                }
            }
            catch (Exception ex)
            {
                response.HasError = true;
                response.ErrorMessage = ex.Message;
            }
            return response;
        }

        #endregion

        #region Validate License

        /// <summary>
        /// This method validates the license (whether the license is installed on the calling server or not)
        /// This method internaly calls the ZNode.Libraries.Framework.Business.ZNodeLicenseManager class to validate the license
        /// </summary>
        /// <returns>Returns ActivateLicenseResponse which will contain the status true if the license is valid otherwise false</returns>
        public ActivateLicenseResponse ValidateLicense()
        {
            ActivateLicenseResponse response = new ActivateLicenseResponse();
            ZNodeLicenseManager licenseMgr = new ZNodeLicenseManager();
            ZNodeLicenseType licenseType = licenseMgr.Validate();

            if (Equals(licenseType, ZNodeLicenseType.Invalid))
            {
                response.HasError = true;
            }
            return response;
        }

        #endregion

        #region Helper Method
        /// <summary>
        /// This method gets the type of the license (ZNodeLicenseType) from the string value supplied
        /// </summary>
        /// <param name="licenseType">type of the license in string format</param>
        /// <returns>returns the ZNodeLicenseType which matches with the string value supplied</returns>
        private ZNodeLicenseType GetLicenseType(string licenseType)
        {
            switch (licenseType)
            {
                case Constant.Marketplace:
                    return ZNodeLicenseType.Marketplace;
                case Constant.MultiFront:
                    return ZNodeLicenseType.Multifront;
                case Constant.MallAdmin:
                    return ZNodeLicenseType.MallAdmin;
                default:
                    return ZNodeLicenseType.Trial;
            }
        }
        #endregion
    }
}