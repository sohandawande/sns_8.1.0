﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;
using Znode.Engine.MvcAdmin.Agents.Agents;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.Exceptions;
using Znode.Libraries.Helpers;


namespace Znode.Engine.MvcAdmin.Agents
{
    public class PromotionAgent : BaseAgent, IPromotionAgent
    {
        #region Private members

        #region Readonly Variables
        private readonly IPromotionsClient _promotionsClient;
        private readonly IPortalsClient _portalsClient;
        private readonly IPromotionTypesClient _promotionTypeClient;
        private readonly IManufacturersClient _manufacturerClient;
        private readonly ICatalogsClient _catalogClient;
        private readonly ICategoriesClient _catagoriesClient;
        private readonly IProductsClient _productsClient;
        private readonly IProfilesClient _profileClient;
        private readonly ISkusClient _skuClient;
        private readonly IPortalCatalogsClient _portalCatalogClient;
        #endregion

        #region Private Variables
        private const string trueValue = "true";
        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor for the Promotions
        /// </summary>
        public PromotionAgent()
        {
            _promotionsClient = GetClient<PromotionsClient>();
            _portalsClient = GetClient<PortalsClient>();
            _promotionTypeClient = GetClient<PromotionTypesClient>();
            _manufacturerClient = GetClient<ManufacturersClient>();
            _catalogClient = GetClient<CatalogsClient>();
            _catagoriesClient = GetClient<CategoriesClient>();
            _productsClient = GetClient<ProductsClient>();
            _profileClient = GetClient<ProfilesClient>();
            _skuClient = GetClient<SkusClient>();
            _portalCatalogClient = GetClient<PortalCatalogsClient>();
        }
        #endregion

        #region Public methods

        public PromotionsListViewModel GetPromotions(FilterCollection filters, SortCollection sortCollection, int? pageIndex = null, int? recordPerPage = null)
        {
            ReplaceFilterKeyName(ref filters, FilterKeys.Coupons, FilterKeys.CouponCode);
            _promotionsClient.RefreshCache = true;
            var modelList = _promotionsClient.GetPromotions(new ExpandCollection { ExpandKeys.Portal, ExpandKeys.Profiles, ExpandKeys.PromotionType }, filters, sortCollection, pageIndex - 1, recordPerPage);
            return Equals(modelList, null) ? null : PromotionViewModelMap.ToListViewModel(modelList);
        }

        public PromotionsViewModel GetPromotionById(int promotionId)
        {
            _promotionsClient.RefreshCache = true;
            var promotionModel = _promotionsClient.GetPromotion(promotionId, new ExpandCollection { ExpandKeys.Portal, ExpandKeys.Profiles, ExpandKeys.PromotionType });
            return (!Equals(promotionModel, null) ? PromotionViewModelMap.ToViewModel(promotionModel) : null);
        }

        public bool SavePromotion(PromotionsViewModel model, out string message)
        {
            try
            {
                message = string.Empty;
                if (Equals(model.UserType, UserTypes.FranchiseAdmin.ToString()))
                {
                    _profileClient.RefreshCache = true;
                    ProfileListModel profiles = _profileClient.GetProfileListByPortalId(model.PortalId.Value);

                    int profileId = profiles.Profiles.FirstOrDefault().ProfileId;
                    model.ProfileId = profileId;
                }
                PromotionModel promotion = _promotionsClient.CreatePromotion(PromotionViewModelMap.ToModel(model));
                return true;
            }
            catch (ZnodeException ex)
            {
                message = ex.ErrorMessage;
                return false;
            }
        }

        public bool UpdatePromotion(PromotionsViewModel model, out string message)
        {
            try
            {
                message = string.Empty;
                PromotionModel promotion = _promotionsClient.UpdatePromotion(model.PromotionId, PromotionViewModelMap.ToModel(model));
                return true;
            }
            catch (ZnodeException ex)
            {
                message = ex.ErrorMessage;
                return false;
            }
        }

        public bool DeletePromotion(int promotionId)
        {
            return _promotionsClient.DeletePromotion(promotionId);
        }

        public ProductListViewModel GetProducts(FilterCollection filters, SortCollection sortCollection, int? pageIndex = null, int? recordPerPage = null)
        {
            _promotionsClient.RefreshCache = true;
            ReplaceSortKeyName(sortCollection, FilterKeys.ProductCode, FilterKeys.ProductNum);
            ProductListModel productList = _promotionsClient.GetProductList(new ExpandCollection(), filters, sortCollection, pageIndex, recordPerPage);
            ReplaceSortKeyName(sortCollection, FilterKeys.ProductNum, FilterKeys.ProductCode);
            return (!Equals(productList, null) && !Equals(productList.Products, null)) ? ProductViewModelMap.ToListViewModel(productList.Products, productList.TotalResults) : new ProductListViewModel();
        }

        public List<SelectListItem> GetSkuList(int productId)
        {
            _skuClient.RefreshCache = true;
            List<SelectListItem> skuList = PromotionViewModelMap.ToListItems(_skuClient.GetSKUListByProductId(productId).Skus);
            return Equals(skuList, null) ? new List<SelectListItem>() : skuList;
        }

        public List<SelectListItem> GetPortalList()
        {
            List<SelectListItem> portalList = new List<SelectListItem>();
            _portalsClient.RefreshCache = true;
            portalList = PromotionViewModelMap.ToListItems(_portalsClient.GetPortals(Expands, Filters, new SortCollection()).Portals);
            return Equals(portalList, null) ? null : portalList;
        }

        public List<SelectListItem> GetManufacturerList()
        {
            List<SelectListItem> manufacturerList = new List<SelectListItem>();
            _manufacturerClient.RefreshCache = true;
            manufacturerList = PromotionViewModelMap.ToListItems(_manufacturerClient.GetManufacturers(Filters, new SortCollection()).Manufacturers);
            return Equals(manufacturerList, null) ? null : manufacturerList;
        }

        public List<SelectListItem> GetCatalogList(int portalId)
        {
            List<SelectListItem> catalogList = new List<SelectListItem>();
            _catalogClient.RefreshCache = true;
            if (!Equals(portalId, 0))
            {
                catalogList = PromotionViewModelMap.ToListItems(_catalogClient.GetCatalogListByPortalId(portalId).Catalogs, false);
            }
            else
            {
                catalogList = PromotionViewModelMap.ToListItems(_catalogClient.GetCatalogs(null, null).Catalogs, false);
            }
            return Equals(catalogList, null) ? null : catalogList;
        }

        public List<SelectListItem> GetCategoryList(int portalId)
        {
            List<SelectListItem> categoryList = new List<SelectListItem>();

            CatalogViewModel catalogViewModel = new CatalogViewModel();
            if (Equals(portalId, 0))
            {
                categoryList = PromotionViewModelMap.ToListItems(_catagoriesClient.GetCategories(Expands, Filters, new SortCollection()).Categories);
            }
            else
            {
                _portalCatalogClient.RefreshCache = true;
                PortalCatalogListViewModel model = PortalCatalogViewModelMap.ToListViewModel(_portalCatalogClient.GetPortalCatalogsByPortalId(portalId), null, null);

                if (!Equals(model, null) && !Equals(model.PortalCatalogs, null) && model.PortalCatalogs.Count > 0)
                {
                    _catagoriesClient.RefreshCache = true;
                    categoryList = PromotionViewModelMap.ToListItems(_catagoriesClient.GetCategoriesByCatalog(model.PortalCatalogs[0].CatalogId, Expands, Filters, new SortCollection()).Categories);
                }
            }
            return categoryList;
        }

        public List<SelectListItem> GetProfileList(int portalId)
        {
            List<SelectListItem> profileList = new List<SelectListItem>();
            _profileClient.RefreshCache = true;
            profileList = PromotionViewModelMap.ToListItems((Equals(portalId, 0)) ? _profileClient.GetProfiles(Expands, Filters, new SortCollection()).Profiles : _profileClient.GetProfileListByPortalId(portalId).Profiles);
            return Equals(profileList, null) ? null : profileList;
        }

        public PromotionsViewModel GetPromotionInformation(PromotionsViewModel model, bool IsEditAction = false)
        {
            FilterCollection filters = new FilterCollection();
            if (!IsEditAction)
            {
                filters.Add(new FilterTuple(FilterKeys.IsActive, FilterOperators.Equals, trueValue));
            }

            if (Equals(model.UserType, UserTypes.FranchiseAdmin.ToString()))
            {
                _promotionTypeClient.RefreshCache = true;
                model.PromotionTypeList = PromotionViewModelMap.ToListItems(_promotionTypeClient.GetFranchisePromotionTypes(filters, Sorts, null, null).PromotionTypes.OrderBy(x => x.Name));
            }
            else
            {
                _portalsClient.RefreshCache = true;
                model.PortalList = PromotionViewModelMap.ToListItems(_portalsClient.GetPortals(Expands, Filters, new SortCollection()).Portals);

                _promotionTypeClient.RefreshCache = true;
                List<SelectListItem> list = PromotionViewModelMap.ToListItems(_promotionTypeClient.GetPromotionTypes(filters, Sorts).PromotionTypes);
                if (Equals(list, null))
                {
                    model.PromotionTypeList = new List<SelectListItem>();
                }
                else
                {
                    model.PromotionTypeList = list.OrderBy(x => x.Text).ToList();
                }
                _profileClient.RefreshCache = true;
                model.ProfileList = PromotionViewModelMap.ToListItems(_profileClient.GetProfiles(Expands, Filters, new SortCollection()).Profiles);

                _manufacturerClient.RefreshCache = true;
                model.ManufacturerList = PromotionViewModelMap.ToListItems(_manufacturerClient.GetManufacturers(Filters, new SortCollection()).Manufacturers);

                _catalogClient.RefreshCache = true;
                model.CatalogList = PromotionViewModelMap.ToListItems(_catalogClient.GetCatalogs(Filters, new SortCollection()).Catalogs, false);

                _catagoriesClient.RefreshCache = true;
                model.CategoryList = PromotionViewModelMap.ToListItems(_catagoriesClient.GetCategories(Expands, Filters, new SortCollection()).Categories);
            }
            return model;
        }
        #endregion

        #region DropDowns For Search

        public List<PromotionTypeModel> GetPromotionModelTypeList(string userType, bool IsListAction = false)
        {
            FilterCollection filters = new FilterCollection();
            if (!IsListAction)
            {
                filters.Add(new FilterTuple(FilterKeys.IsActive, FilterOperators.Equals, trueValue));
            }

            PromotionTypeListModel promotionTypeList = new PromotionTypeListModel();
            _promotionTypeClient.RefreshCache = true;
            if (Equals(userType, UserTypes.FranchiseAdmin.ToString()))
            {
                promotionTypeList = _promotionTypeClient.GetFranchisePromotionTypes(filters, null, null, null);
            }
            else
            {
                promotionTypeList = _promotionTypeClient.GetPromotionTypes(filters, null);
            }
            if (Equals(promotionTypeList, null) || Equals(promotionTypeList.PromotionTypes, null))
            {
                return new List<PromotionTypeModel>();
            }
            else
            {
                var list = promotionTypeList.PromotionTypes.OrderBy(x => x.Name);
                return (!Equals(promotionTypeList, null) && !Equals(list, null)) ? list.ToList() : new List<PromotionTypeModel>();
            }
        }

        public List<ManufacturerModel> GetManufacturerModelList()
        {
            _manufacturerClient.RefreshCache = true;
            ManufacturerListModel manufacturers = _manufacturerClient.GetManufacturers(null, null);
            return (!Equals(manufacturers, null) && !Equals(manufacturers.Manufacturers, null)) ? manufacturers.Manufacturers.ToList() : new List<ManufacturerModel>();
        }

        public List<CategoryModel> GetCategoryModelList()
        {
            _catagoriesClient.RefreshCache = true;
            CategoryListModel categories = _catagoriesClient.GetCategories(null, null, null);
            return (!Equals(categories, null) && !Equals(categories.Categories, null)) ? categories.Categories.ToList() : new List<CategoryModel>();
        }
        #endregion
    }
}
