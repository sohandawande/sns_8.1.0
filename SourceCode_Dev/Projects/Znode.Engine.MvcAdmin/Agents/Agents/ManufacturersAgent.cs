﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public partial class ManufacturersAgent : BaseAgent, IManufacturersAgent
    {
        #region Private Variabls
        private readonly IManufacturersClient _manufacturerClient;
        #endregion

        #region Constructors

        /// <summary>
        /// Constructor for the Manufacturer or Brands.
        /// </summary>
        public ManufacturersAgent()
        {
            _manufacturerClient = GetClient<ManufacturersClient>();
        }

        #endregion

        #region Public Methods

        public ManufacturersListViewModel GetManufacturers(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            _manufacturerClient.RefreshCache = true;
            var list = _manufacturerClient.GetManufacturers(filters, sortCollection, pageIndex - 1, recordPerPage);
            return ManufacturersViewModelMap.ToListViewModel(list);
        }

        public ManufacturersViewModel GetManufacturerByID(int manufacturerID)
        {
            _manufacturerClient.RefreshCache = true;
            ManufacturerModel model = _manufacturerClient.GetManufacturer(manufacturerID);
            return model != null ? ManufacturersViewModelMap.ToViewModel(model) : null;
        }

        public bool SaveManufacturer(ManufacturersViewModel model)
        {
            var manufacturer = _manufacturerClient.CreateManufacturer(ManufacturersViewModelMap.ToModel(model));
            return (manufacturer.ManufacturerId > 0) ? true : false;
        }

        public bool UpdateManufacturer(ManufacturersViewModel model)
        {
            ManufacturerModel updateModel = ManufacturersViewModelMap.ToModel(model);
            var manufacturer = _manufacturerClient.UpdateManufacturer(updateModel.ManufacturerId, updateModel);
            return Equals(manufacturer, null) ? false : true;
        }

        public bool DeleteManufactuere(int manufacturerID)
        {
            try
            {
                return _manufacturerClient.DeleteManufacturer(manufacturerID);
            }
            catch (ZnodeException)
            {
                return false;
            }
            catch (Exception)
            {
                return false;
            }
            
        }

        #endregion

    }
}