﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    /// <summary>
    /// Agent of Highlight
    /// </summary>
    public class HighlightAgent : BaseAgent, IHighlightAgent
    {
        #region Private Variables
        private readonly IHighlightsClient _highlightsClient;
        private readonly IHighlightTypesClient _highlightTypesClient;
        private readonly IProductsClient _productsClient;
        #endregion

        #region Constructor
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public HighlightAgent()
        {
            _highlightsClient = GetClient<HighlightsClient>();
            _highlightTypesClient = GetClient<HighlightTypesClient>();
            _productsClient = GetClient<ProductsClient>();
        }
        #endregion

        #region Public Methods

        public HighlightsListViewModel GetHighlights(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            _highlightsClient.RefreshCache = true;
            HighlightListModel list = _highlightsClient.GetHighlights(new ExpandCollection { ExpandKeys.HighlightType }, filters, sortCollection, pageIndex - 1, recordPerPage);
            return Equals(list, null) ? null : HighlightViewModelMap.ToListViewModel(list);
        }

        public HighlightsListViewModel GetHighlights()
        {
            _highlightsClient.RefreshCache = true;
            HighlightListModel list = _highlightsClient.GetHighlights(new ExpandCollection { ExpandKeys.HighlightType }, null, null);
            HighlightsListViewModel highlightViewModels = HighlightViewModelMap.ToListViewModel(list);
            return highlightViewModels;
        }

        public HighlightViewModel GetHighlight(int? highlightId)
        {
            _highlightsClient.RefreshCache = true;
            HighlightViewModel highlightViewModel = (highlightId.HasValue) ? HighlightViewModelMap.ToViewModel(_highlightsClient.GetHighlight(highlightId.Value)) : null;
            return highlightViewModel;
        }

        public bool CreateHighlight(HighlightViewModel model, out string errorMessage)
        {
            HighlightModel highlight = new HighlightModel();
            errorMessage = string.Empty;
            try
            {
                highlight = _highlightsClient.CreateHighlight(HighlightViewModelMap.ToModel(model));
                model.HighlightId = highlight.HighlightId;

                if (!Equals(highlight, null) && highlight.HighlightId > 0)
                {
                    this.SaveImage(model);
                }
            }
            catch (ZnodeException ex)
            {
                errorMessage = ex.ErrorMessage;
            }
            catch (Exception e)
            {
            }
            return (!Equals(highlight, null) && highlight.HighlightId > 0) ? true : false;
        }

        public bool UpdateHighlight(HighlightViewModel model, out string errorMessage)
        {
            HighlightModel highlight = new HighlightModel();
            errorMessage = string.Empty;
            try
            {
                highlight = _highlightsClient.UpdateHighlight(model.HighlightId, HighlightViewModelMap.ToModel(model));
                if (!Equals(highlight, null) && highlight.HighlightId > 0)
                {
                    this.SaveImage(model);
                }
            }
            catch (ZnodeException ex)
            {
                errorMessage = ex.ErrorMessage;
            }
            return (!Equals(highlight, null) && (highlight.HighlightId > 0));
        }

        public bool DeleteHighlight(int highlightId, out string errorMessage)
        {
            errorMessage = string.Empty;
            try
            {
                return _highlightsClient.DeleteHighlight(highlightId);
            }
            catch (ZnodeException ex)
            {
                errorMessage = ex.ErrorMessage;
                return false;
            }
        }

        public bool CheckAssociatedProduct(int highlightId)
        {
            return HighlightViewModelMap.ToViewModel(_highlightsClient.CheckAssociatedProduct(highlightId)).IsAssociatedProduct;
        }

        public List<HighlightTypeModel> GetHighlightTypeList()
        {
            HighlightTypeListModel highlightTypeList = new HighlightTypeListModel();
            highlightTypeList = _highlightTypesClient.GetHighlightTypes(null, null);
            return highlightTypeList.HighlightTypes.ToList();
        }

        public void SetHighlightProperty(HighlightViewModel model)
        {
            if (!Equals(model.HighlightImage, null))
            {
                if (model.IsNewImageFileName.Equals(1))
                {
                    model.ImageFile = model.NewImageFileName;
                }
                else if (model.IsNewImageFileName.Equals(0))
                {
                    model.NewImageFileName = string.Empty;
                }
            }
            else
            {
                model.ImageFile = model.ImageFile;
            }
            model.ImageFile = model.NoImage ? null : model.ImageFile;
            model.LocaleId = MvcAdminConstants.LocaleId;
            model.HyperlinkNewWindow = (!Equals(model.Hyperlink, null)) ? true : false;
        }

        #endregion

        #region Private Methods
        /// <summary>
        /// To save image by Id in data folder and update image name in database table
        /// </summary>
        /// <param name="model">ProductViewModel model</param>
        private void SaveImage(HighlightViewModel model)
        {
            if (!Equals(model.HighlightImage, null))
            {
                //to save image in data folder
                ImageHelper imageHelper = new ImageHelper();
                string validExtension = Convert.ToString(ConfigurationManager.AppSettings["ImageExtension"]);
                string imagePath = imageHelper.SaveImageToPhysicalPathByName(null, null, validExtension, model.NewImageFileName, model.HighlightImage, null);

                if (!string.IsNullOrEmpty(imagePath) && imagePath != MvcAdminConstants.FileUploadErrorCode)
                {
                    //to update image path in database by id
                    UpdateImageModel imageModel = new UpdateImageModel();
                    imageModel.Id = model.HighlightId;
                    imageModel.EntityName = Convert.ToString(EntityName.Highlight);
                    imageModel.ImagePath = imagePath;
                    _productsClient.UpdateImage(imageModel);
                }
            }
        }
        #endregion
    }
}