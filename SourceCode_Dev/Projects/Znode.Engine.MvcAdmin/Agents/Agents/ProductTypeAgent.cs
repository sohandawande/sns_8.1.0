﻿using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public class ProductTypeAgent : BaseAgent, IProductTypeAgent
    {
        #region Private Variables
        private readonly IProductTypeClient _productTypeClient;
        private readonly IProductsClient _productsClient;
        #endregion

        #region Constructor

        /// <summary>
        /// Constructor for ProductTypeAgent
        /// </summary>
        public ProductTypeAgent()
        {
            _productTypeClient = new ProductTypeClient();
            _productsClient = new ProductsClient();
        }
        #endregion

        #region Public Methods

        public ProductTypeListViewModel GetProductTypes(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            _productTypeClient.RefreshCache = true;
            ProductTypeListModel list = _productTypeClient.GetProductTypes(new ExpandCollection { ExpandKeys.Catalog, ExpandKeys.PortalCountries }, filters, sortCollection, pageIndex - 1, recordPerPage);
            return Equals(list.ProductTypes, null) ? null : ProductTypeViewModelMap.ToListViewModel(list);
        }

        public ProductTypeViewModel GetProductType(int productTypeId)
        {
            _productTypeClient.RefreshCache = true;
            ProductTypeViewModel productTypeViewModel = ProductTypeViewModelMap.ToViewModel(_productTypeClient.GetProductType(productTypeId));
            return Equals(productTypeViewModel,null) ? null : productTypeViewModel;
        }

        public bool CreateProductType(ProductTypeViewModel viewModel,out string message)
        {
            try
            {
                message = string.Empty;
                ProductTypeModel productType = _productTypeClient.CreateProductType(ProductTypeViewModelMap.ToModel(viewModel));
                return (productType.ProductTypeId > 0);
            }
            catch (ZnodeException ex)
            {
                message = ex.ErrorMessage;
                return false;
            }
        }

        public bool DeleteProductType(int productTypeId, out string errorMessage)
        {
            try
            {
                errorMessage = string.Empty;
                ProductTypeModel productType = _productTypeClient.GetProductType(productTypeId);
                return !Equals(productType, null) ? _productTypeClient.DeleteProductType(productTypeId) : false;
            }
            catch (ZnodeException ex)
            {
                errorMessage = ex.ErrorMessage;
                return false;
            }
        }

        public bool UpdateProductType(ProductTypeViewModel viewModel, out string message)
        {
            try
            {
                message = string.Empty;
                ProductTypeModel productType = _productTypeClient.UpdateProductType(viewModel.ProductTypeId, ProductTypeViewModelMap.ToModel(viewModel));
                return !Equals(productType, null);
            }
            catch (ZnodeException ex)
            {
                message = ex.ErrorMessage;
                return false;
            }
        }
        #endregion
    }
}