﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public class PortalCountryAgent : BaseAgent, IPortalCountryAgent
    {
        #region Private Variables
        private readonly IPortalCountriesClient _portalCountriesClient;
        private readonly ICountriesClient _countryClient;
        #endregion

        #region Constructor
        public PortalCountryAgent()
        {
            _portalCountriesClient = GetClient<PortalCountriesClient>();
            _countryClient = GetClient<CountriesClient>();
        }
        #endregion

        #region Public Methods
        public bool DeletePortalCountry(int portalCountryId, out string errorMessage)
        {
            try
            {
                errorMessage = string.Empty;
                return _portalCountriesClient.DeletePortalCountryIfNotAssociated(portalCountryId);
            }
            catch (ZnodeException ex)
            {
                errorMessage = ex.ErrorMessage;
                return false;
            }
        }

        public CountryListViewModel GetCountryList(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            List<CountryViewModel> countryList = new List<CountryViewModel>();
            _countryClient.RefreshCache = true;
            countryList = CountryViewModelMap.ToListViewModel(_countryClient.GetCountries(Expands, filters, sortCollection)).Countries.ToList();
            CountryListViewModel countrListViewModel = new CountryListViewModel();
            if (countryList.Count > 1 && countryList.Contains(countryList.Where(countryItem => countryItem.Code.Equals(MvcAdminConstants.DefaultCountry)).SingleOrDefault()))
            {
                CountryViewModel country = countryList.Where(countryItem => countryItem.Code.Equals(MvcAdminConstants.DefaultCountry)).SingleOrDefault();
                countryList.Remove(country);
                countryList.Insert(0, country);
            }
            countrListViewModel.Countries.InsertRange(0, countryList);
            return (!Equals(countrListViewModel, null)) ? countrListViewModel : null;
        }

        public bool AddPortalCountries(int portalId, string billableCountryCodes, string shipableCountryCodes, out string message)
        {
            try
            {
                message = string.Empty;

                return _portalCountriesClient.CreatePortalCountries(portalId, billableCountryCodes, shipableCountryCodes);
            }
            catch (ZnodeException ex)
            {
                message = ex.ErrorMessage;
                return false;
            }
        }

        public PortalCountriesListViewModel GetAllPortalCountries(int PortalId, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            if (PortalId > 0 && !Equals(filters, null))
            {
                filters.Add(new FilterTuple(FilterKeys.PortalId, FilterOperators.Equals, PortalId.ToString()));
                _portalCountriesClient.RefreshCache = true;
                var list = _portalCountriesClient.GetAllPortalCountries(null, filters, sortCollection, pageIndex, recordPerPage);
                return Equals(list, null) ? null : PortalCountriesViewModelMap.ToListViewModel(list);
            }
            return new PortalCountriesListViewModel();
        }

        #endregion

        #region Private Methods
        private PortalCountriesViewModel GetPortalCountryModel(CountryModel countryModel, int portalId)
        {
            PortalCountriesViewModel portalCountry = new PortalCountriesViewModel();
            portalCountry.PortalId = portalId;
            portalCountry.CountryCode = countryModel.Code;
            return portalCountry;
        }
        #endregion
    }
}