﻿using Znode.Engine.Api.Client;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public class DashboardAgent : BaseAgent, IDashboardAgent
    {
        #region Private Variables
        private readonly IDashboardClient _dashboardClient;
        private readonly IPortalAgent _portalAgent;
        #endregion

        #region Constructor
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public DashboardAgent()
        {
            _dashboardClient = GetClient<DashboardClient>();
            _portalAgent = new PortalAgent();
        }
        #endregion

        #region Public Methods
        public DashboardViewModel GetDashboardItemsByPortalId(int portalId)
        {
            DashboardViewModel dashboardViewModel = new DashboardViewModel();

            if(portalId > 0)
            {
                DashboardModel dashboardModel = _dashboardClient.GetDashboardItemsByPortalId(portalId);
                PortalViewModel portalViewModel = _portalAgent.GetPortalByPortalId(portalId);
                if (!Equals(dashboardModel, null))
                {
                   dashboardViewModel = DashboardViewModelMap.ToViewModel(dashboardModel);
                }

                if(!Equals(portalViewModel, null))
                {
                    dashboardViewModel.StoreName = !Equals(portalViewModel, null) ? portalViewModel.StoreName : string.Empty;
                }
            }

            return (!Equals(dashboardViewModel, null)) ? dashboardViewModel : new DashboardViewModel();
        } 
        #endregion
    }
}