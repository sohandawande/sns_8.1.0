﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public class SuppliersAgent : BaseAgent, ISupplierAgent
    {
        #region Private Variables
        private readonly ISuppliersClient _suppliersClient;
        private readonly ISupplierTypesClient _supplierTypesClient;

        private string isactive = "isactive";
        private string value = "Value";
        private string trueValue = "true";
        private string falseValue = "false";


        #endregion

        #region Constructor
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public SuppliersAgent()
        {
            _suppliersClient = GetClient<SuppliersClient>();
            _supplierTypesClient = GetClient<SupplierTypesClient>();
        }
        #endregion

        #region Public Methods
        public SupplierListViewModel GetSuppliers()
        {
            _suppliersClient.RefreshCache = true;
            var list = _suppliersClient.GetSuppliers(new ExpandCollection { ExpandKeys.Portal, ExpandKeys.Profiles, ExpandKeys.PromotionType }, null, null);
            return (!Equals(list, null)?SupplierViewModelMap.ToListViewModel(list):new SupplierListViewModel());              
        }

        public SupplierListViewModel GetSuppliers(FilterCollection filters, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            #region Update Filter value
            foreach (var tuple in filters)
            {
                if (Equals(tuple.Item1.ToLower(), FilterKeys.Value))
                {
                    var activeValue = tuple.Item3;
                    filters.Add(new FilterTuple(FilterKeys.IsActive, FilterOperators.Equals, Equals(activeValue, "1") ? trueValue : falseValue));
                    filters.Remove(tuple);
                    break;
                }
            }
            #endregion

            _suppliersClient.RefreshCache = true;
            SupplierListModel list = _suppliersClient.GetSuppliers(new ExpandCollection { ExpandKeys.SupplierType }, filters, sortCollection, pageIndex - 1, recordPerPage);
            
            #region Undo Filter value
            foreach (var tuple in filters)
            {
                if (Equals(tuple.Item1.ToLower(), isactive) && (Equals(tuple.Item3.ToLower(), trueValue)))
                {
                    filters.Add(new FilterTuple(value, FilterOperators.Equals, "1"));
                    filters.Remove(tuple);
                    break;
                }
                else if (Equals(tuple.Item1.ToLower(), isactive) && (Equals(tuple.Item3.ToLower(), falseValue)))
                {
                    filters.Add(new FilterTuple(value, FilterOperators.Equals, "0"));
                    filters.Remove(tuple);
                    break;
                }
            }
            #endregion

            return Equals(list, null) ? new SupplierListViewModel() : SupplierViewModelMap.ToListViewModel(list);
        }

        public SupplierViewModel GetSupplier(int supplierId)
        {
            _suppliersClient.RefreshCache = true;
            SupplierModel model = new SupplierModel();
            if (supplierId > 0)
            {
                _suppliersClient.RefreshCache = true;
                 model = _suppliersClient.GetSupplier(supplierId);
                return SupplierViewModelMap.ToViewModel(model);
            }
            else
            {
                return null;
            }
        }

        public bool CreateSupplier(SupplierViewModel model)
        {
            SupplierModel createdModel = new SupplierModel();
            if (!Equals(model, null))
            {
                model.EnableEmailNotification = true;
                createdModel = _suppliersClient.CreateSupplier(SupplierViewModelMap.ToModel(model));                
            }
            return createdModel.SupplierId > 0;
        }

        public bool UpdateSupplier(SupplierViewModel model)
        {
            SupplierModel updatedModel = new SupplierModel();
            _suppliersClient.RefreshCache = true;
            if (!Equals(model, null))
            {
                model.EnableEmailNotification = true;
                updatedModel = _suppliersClient.UpdateSupplier(model.SupplierId, SupplierViewModelMap.ToModel(model));
            } 
            return updatedModel.SupplierId > 0;
        }

        public bool DeleteSupplier(int supplierId, out string errorMessage)
        {
            SupplierViewModel viewModel = new SupplierViewModel();
            errorMessage = string.Empty;
            try
            {
               return supplierId > 0 ? _suppliersClient.DeleteSupplier(supplierId) : false;
            }
            catch (ZnodeException ex)
            {
                errorMessage = ex.ErrorMessage;
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        #endregion

        #region Dropdown for Supplier Type
        public List<SupplierTypeModel> GetSupplierTypeList(bool isEditAction=false)
        {
            _supplierTypesClient.RefreshCache = true;
            FilterCollection filters = new FilterCollection();
            if (!isEditAction)
            {
                filters.Add(new FilterTuple(FilterKeys.IsActive, FilterOperators.Equals, trueValue));
            }
            SupplierTypeListModel supplierTypeList = _supplierTypesClient.GetSupplierTypes(filters, null);
            if (!Equals(supplierTypeList, null) && !Equals(supplierTypeList.SupplierTypes, null))
            {
                return supplierTypeList.SupplierTypes.ToList();
            }
            return new List<SupplierTypeModel>();
        }

        public List<SelectListItem> GetStatusList()
        {
            return SupplierViewModelMap.ToListItems();
        }
        #endregion
    }
}