﻿using Resources;
using System.Web.Mvc;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public class ProviderEngineAgent : BaseAgent, IProviderEngineAgent
    {
        #region Private Variables
        private readonly IPromotionTypesClient _promotionTypesClient;
        private readonly ITaxRuleTypesClient _taxRuleTypesClient;
        private readonly ISupplierTypesClient _supplierTypesClient;
        private readonly IShippingTypesClient _shippingTypesClient;
        #endregion

        #region Constructor
        public ProviderEngineAgent()
        {
            _promotionTypesClient = GetClient<PromotionTypesClient>();
            _taxRuleTypesClient = GetClient<TaxRuleTypesClient>();
            _supplierTypesClient = GetClient<SupplierTypesClient>();
            _shippingTypesClient = GetClient<ShippingTypesClient>();
        }
        #endregion

        #region Promotion Type
        public PromotionTypeListViewModel GetPromotionTypes(FilterCollection filters, SortCollection sortCollection, int? pageIndex, int? recordPerPage)
        {
            _promotionTypesClient.RefreshCache = true;
            PromotionTypeListModel list = _promotionTypesClient.GetPromotionTypes(filters, sortCollection, pageIndex - 1, recordPerPage);
            return Equals(list, null) ? new PromotionTypeListViewModel() : PromotionTypeViewModelMap.ToListViewModel(list);
        }

        public bool DeletePromotionType(int promotionTypeId,out string errorMessage)
        {
            try
            {
                errorMessage = string.Empty;
                return _promotionTypesClient.DeletePromotionType(promotionTypeId);
            }
            catch (ZnodeException ex)
            {
                errorMessage = ex.ErrorMessage;
                return false;
            }
        }

        public ProviderEngineViewModel GetPromotionType(int promotionTypeId)
        {
            _promotionTypesClient.RefreshCache = true;
            PromotionTypeModel promotionTypeModel = _promotionTypesClient.GetPromotionType(promotionTypeId);
            return (!Equals(promotionTypeModel, null)) ? ProviderEngineViewModelMap.ToViewModel(promotionTypeModel) : null;
        }

        public bool UpdatePromotionType(ProviderEngineViewModel model)
        {
            _promotionTypesClient.RefreshCache = true;
            PromotionTypeModel promotionTypeModel = _promotionTypesClient.UpdatePromotionType(model.Id, ProviderEngineViewModelMap.ToPromotionTypeModel(model));
            return (!Equals(promotionTypeModel, null) && promotionTypeModel.PromotionTypeId > 0);
        }

        public ProviderEngineViewModel GetPromotionTypeByClassName(string Name)
        {
            ProviderEngineViewModel providerEngineViewModel = new ProviderEngineViewModel();
            _promotionTypesClient.RefreshCache = true;
            PromotionTypeListViewModel promotionTypeListViewModel = PromotionTypeViewModelMap.ToListViewModel(_promotionTypesClient.GetAllPromotionTypesNotInDatabase(null, null, null, null));
            foreach(var promotionType in  promotionTypeListViewModel.PromotionTypes )
            {
                if(string.Equals(promotionType.Name.ToLower(),Name.ToLower()))
                {
                    providerEngineViewModel.ClassName=promotionType.ClassName;
                    providerEngineViewModel.Name=promotionType.Name;
                    providerEngineViewModel.Description=promotionType.Description;
                    providerEngineViewModel.ClassType = promotionType.ClassType;
                    break;
                }
            }
            return providerEngineViewModel;
        }

        public ProviderEngineViewModel BindAvailablePromotionTypes(ProviderEngineViewModel Model)
        {
            _promotionTypesClient.RefreshCache = true;
            Model.AvailablePromotionTypes = PromotionTypeViewModelMap.ToListItems(_promotionTypesClient.GetAllPromotionTypesNotInDatabase(null, null, null, null).PromotionTypes);
            Model.AvailablePromotionTypes.Insert(0, new SelectListItem() { Value = "0", Text = ZnodeResources.DdlSelectPromotion });
            return Model;
        }

        public bool CreatePromotionType(ProviderEngineViewModel model)
        {
            PromotionTypeModel promotionTypeModel = _promotionTypesClient.CreatePromotionType(ProviderEngineViewModelMap.ToPromotionTypeModel(model));
            return (!Equals(promotionTypeModel, null) && (promotionTypeModel.PromotionTypeId > 0));
        }

        #endregion

        #region Tax Rule Type
        public TaxRuleTypeListViewModel GetTaxRuleTypes(FilterCollection filters, SortCollection sortCollection, int? pageIndex, int? recordPerPage)
        {
            _taxRuleTypesClient.RefreshCache = true;
            TaxRuleTypeListModel list = _taxRuleTypesClient.GetTaxRuleTypes(filters, sortCollection, pageIndex - 1, recordPerPage);
            return Equals(list, null) ? new TaxRuleTypeListViewModel() : TaxRulesViewModelMap.ToListViewModel(list);
        }

        public bool DeleteTaxRuleType(int taxRuleTypeId, out string errorMessage)
        {
            try
            {
                errorMessage = string.Empty;
                return _taxRuleTypesClient.DeleteTaxRuleType(taxRuleTypeId);
            }
            catch (ZnodeException ex)
            {
                errorMessage = ex.ErrorMessage;
                return false;
            }
        }

        public ProviderEngineViewModel GetTaxRuleType(int TaxRuleTypeId)
        {
            _taxRuleTypesClient.RefreshCache = true;
            TaxRuleTypeModel taxRuleTypeModel = _taxRuleTypesClient.GetTaxRuleType(TaxRuleTypeId);
            return (!Equals(taxRuleTypeModel, null)) ? ProviderEngineViewModelMap.ToViewModel(taxRuleTypeModel) : null;
        }

        public bool UpdateTaxRuleType(ProviderEngineViewModel model)
        {
            TaxRuleTypeModel promotionTypeModel = _taxRuleTypesClient.UpdateTaxRuleType(model.Id, ProviderEngineViewModelMap.ToTaxRuleTypeModel(model));
            return (!Equals(promotionTypeModel, null) && promotionTypeModel.TaxRuleTypeId > 0);
        }

        public ProviderEngineViewModel GetTaxRuleTypeByClassName(string name)
        {
            ProviderEngineViewModel providerEngineViewModel = new ProviderEngineViewModel();
            _taxRuleTypesClient.RefreshCache = true;
            TaxRuleTypeListViewModel taxRuleTypeListViewModel = TaxRulesViewModelMap.ToListViewModel(_taxRuleTypesClient.GetAllTaxRuleTypesNotInDatabase(null, null, null, null));
            foreach (var taxRuleType in taxRuleTypeListViewModel.TaxRuleType)
            {
                if (string.Equals(taxRuleType.Name.ToLower(), name.ToLower()))
                {
                    providerEngineViewModel.ClassName = taxRuleType.ClassName;
                    providerEngineViewModel.Name = taxRuleType.Name;
                    providerEngineViewModel.Description = taxRuleType.Description;
                    break;
                }
            }
            return providerEngineViewModel;
        }

        public bool CreateTaxRuleType(ProviderEngineViewModel model)
        {
            TaxRuleTypeModel promotionTypeModel = _taxRuleTypesClient.CreateTaxRuleType(ProviderEngineViewModelMap.ToTaxRuleTypeModel(model));
            return (!Equals(promotionTypeModel, null) && promotionTypeModel.TaxRuleTypeId > 0);
        }

        public ProviderEngineViewModel BindAvailableTaxRuleType(ProviderEngineViewModel Model)
        {
            Model.AvailablePromotionTypes = PromotionTypeViewModelMap.ToListItems(_taxRuleTypesClient.GetAllTaxRuleTypesNotInDatabase(null, null, null, null).TaxRuleTypes);
            Model.AvailablePromotionTypes.Insert(0, new SelectListItem() { Value = "0", Text =ZnodeResources.DdlSelectTax });
            return Model;
        }

        #endregion

        #region Supplier Type
        public SupplierTypeListViewModel GetSupplierTypes(FilterCollection filters, SortCollection sortCollection, int? pageIndex, int? recordPerPage)
        {
            _supplierTypesClient.RefreshCache = true;
            SupplierTypeListModel list = _supplierTypesClient.GetSupplierTypes(filters, sortCollection, pageIndex - 1, recordPerPage);
            return Equals(list, null) ? new SupplierTypeListViewModel() : SupplierTypeViewModelMap.ToListViewModel(list);
        }

        public bool DeleteSupplierType(int supplierTypeId, out string errorMessage)
        {
            try
            {
                errorMessage = string.Empty;
                return _supplierTypesClient.DeleteSupplierType(supplierTypeId);
            }
            catch (ZnodeException ex)
            {
                errorMessage = ex.ErrorMessage;
                return false;
            }
        }

        public ProviderEngineViewModel GetSupplierType(int SupplierTypeId)
        {
            _supplierTypesClient.RefreshCache = true;
            SupplierTypeModel supplierTypeModel = _supplierTypesClient.GetSupplierType(SupplierTypeId);
            return (!Equals(supplierTypeModel, null)) ? ProviderEngineViewModelMap.ToViewModel(supplierTypeModel) : null;
        }

        public bool UpdateSupplierType(ProviderEngineViewModel model)
        {
            SupplierTypeModel supplierTypeModel = _supplierTypesClient.UpdateSupplierType(model.Id, ProviderEngineViewModelMap.ToSupplierTypeModel(model));
            return (!Equals(supplierTypeModel, null) && supplierTypeModel.SupplierTypeId > 0);
        }

        public ProviderEngineViewModel GetSupplierTypeByClassName(string name)
        {
            ProviderEngineViewModel providerEngineViewModel = new ProviderEngineViewModel();
            _supplierTypesClient.RefreshCache = true;
            SupplierTypeListViewModel supplierTypeListViewModel = SupplierTypeViewModelMap.ToListViewModel(_supplierTypesClient.GetAllSupplierTypesNotInDatabase(null, null, null, null));
            foreach (var supplierType in supplierTypeListViewModel.SupplierType)
            {
                if (string.Equals(supplierType.Name.ToLower(), name.ToLower()))
                {
                    providerEngineViewModel.ClassName = supplierType.ClassName;
                    providerEngineViewModel.Name = supplierType.Name;
                    providerEngineViewModel.Description = supplierType.Description;
                    break;
                }
            }
            return providerEngineViewModel;
        }

        public bool CreateSupplierType(ProviderEngineViewModel model)
        {
            SupplierTypeModel supplierTypeModel = _supplierTypesClient.CreateSupplierType(ProviderEngineViewModelMap.ToSupplierTypeModel(model));
            return (!Equals(supplierTypeModel, null) && supplierTypeModel.SupplierTypeId > 0);
        }

        public ProviderEngineViewModel BindAvailableSupplierType(ProviderEngineViewModel Model)
        {
            Model.AvailablePromotionTypes = PromotionTypeViewModelMap.ToListItems(_supplierTypesClient.GetAllSupplierTypesNotInDatabase(null, null, null, null).SupplierTypes);
            Model.AvailablePromotionTypes.Insert(0, new SelectListItem() { Value = "0", Text =ZnodeResources.DdlSelectSupplier });
            return Model;
        }
        #endregion

        #region Shipping Type
        public ShippingTypeListViewModel GetShippingTypes(FilterCollection filters, SortCollection sortCollection, int? pageIndex, int? recordPerPage)
        {
            _shippingTypesClient.RefreshCache = true;
            ShippingTypeListModel list = _shippingTypesClient.GetShippingTypes(filters, sortCollection, pageIndex - 1, recordPerPage);
            return Equals(list, null) ? new ShippingTypeListViewModel() : ShippingTypeViewModelMap.ToListViewModel(list);
        }

        public bool DeleteShippingType(int shippingTypeId,out string errorMessage)
        {
            try
            {
                errorMessage = string.Empty;
                return _shippingTypesClient.DeleteShippingType(shippingTypeId);
            }
            catch (ZnodeException ex)
            {
                errorMessage = ex.ErrorMessage;
                return false;
            }
        }

        public ProviderEngineViewModel GetShippingType(int ShippingTypeId)
        {
            _shippingTypesClient.RefreshCache = true;
            ShippingTypeModel shippingTypeModel = _shippingTypesClient.GetShippingType(ShippingTypeId);
            return (!Equals(shippingTypeModel, null)) ? ProviderEngineViewModelMap.ToViewModel(shippingTypeModel) : null;
        }

        public bool UpdateShippingType(ProviderEngineViewModel model)
        {
            ShippingTypeModel shippingTypeModel = _shippingTypesClient.UpdateShippingType(model.Id, ProviderEngineViewModelMap.ToShippingTypeModel(model));
            return (!Equals(shippingTypeModel, null) && shippingTypeModel.ShippingTypeId > 0);
        }

        public ShippingTypeListViewModel GetAllShippingTypesNotInDatabase()
        {
            ShippingTypeListModel list = _shippingTypesClient.GetAllShippingTypesNotInDatabase(null, null, null, null);
            return Equals(list, null) ? new ShippingTypeListViewModel() : ShippingTypeViewModelMap.ToListViewModel(list);
        }

        public ProviderEngineViewModel GetShippingTypeByClassName(string name)
        {
            ProviderEngineViewModel providerEngineViewModel = new ProviderEngineViewModel();
            _shippingTypesClient.RefreshCache = true;
            ShippingTypeListViewModel shippingTypeListViewModel = ShippingTypeViewModelMap.ToListViewModel(_shippingTypesClient.GetAllShippingTypesNotInDatabase(null, null, null, null));
            foreach (var shippingType in shippingTypeListViewModel.ShippingType)
            {
                if (string.Equals(shippingType.Name.ToLower(), name.ToLower()))
                {
                    providerEngineViewModel.ClassName = shippingType.ClassName;
                    providerEngineViewModel.Name = shippingType.Name;
                    providerEngineViewModel.Description = shippingType.Description;
                    break;
                }
            }
            return providerEngineViewModel;
        }

        public bool CreateShippingType(ProviderEngineViewModel model)
        {
            ShippingTypeModel shippingTypeModel = _shippingTypesClient.CreateShippingType(ProviderEngineViewModelMap.ToShippingTypeModel(model));
            return (!Equals(shippingTypeModel, null) && shippingTypeModel.ShippingTypeId > 0);
        }

        public ProviderEngineViewModel BindAvailableShippingType(ProviderEngineViewModel Model)
        {
            _shippingTypesClient.RefreshCache = true;
            Model.AvailablePromotionTypes = PromotionTypeViewModelMap.ToListItems(_shippingTypesClient.GetAllShippingTypesNotInDatabase(null, null, null, null).ShippingTypes);
            Model.AvailablePromotionTypes.Insert(0, new SelectListItem() { Value = "0", Text = ZnodeResources.DdlSelectShipping });
            return Model;
        }
        #endregion
    }
}