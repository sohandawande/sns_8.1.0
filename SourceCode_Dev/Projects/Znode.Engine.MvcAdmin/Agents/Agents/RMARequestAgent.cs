﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.MvcAdmin.Agents.IAgents;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;
using Znode.Engine.MvcDemo.Maps;

namespace Znode.Engine.MvcAdmin.Agents.Agents
{
    public class RMARequestAgent : BaseAgent, IRMARequestAgent
    {
        #region Private variables
        #region Readonly variables
        private readonly IRMARequestClient _rmaRequestClient;
        private readonly IRequestStatusClient _requestStatusClient;
        private readonly IPortalsClient _portalsClient;
        private readonly IRMARequestItemClient _rmaRequestItemClient;
        private readonly IRMAConfigurationClient _rmaConfigurationClient;
        private readonly IReasonForReturnClient _reasonForReturnClient;
        #endregion

        #region Constant Variables
        private const string AppendFlag = "APPEND";
        private const string VoidMode = "void";
        private const string ViewFlag = "view";
        #endregion
        #endregion

        #region Constructor

        /// <summary>
        /// Public constructor for RMARequestAgent. 
        /// </summary>
        public RMARequestAgent()
        {
            _rmaRequestClient = GetClient<RMARequestClient>();
            _requestStatusClient = GetClient<RequestStatusClient>();
            _portalsClient = GetClient<PortalsClient>();
            _rmaRequestItemClient = GetClient<RMARequestItemClient>();
            _rmaConfigurationClient = GetClient<RMAConfigurationClient>();
            _reasonForReturnClient = GetClient<ReasonForReturnsClient>();
        }
        #endregion

        #region Public Methods

        public RMARequestListViewModel GetRMARequests(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            filters = ApplyRMARequestsFilters(filters);

            RMARequestListViewModel rmaRequests = RMARequestViewModelMap.ToListViewModel(_rmaRequestClient.GetRMARequestList(null, filters, sortCollection, pageIndex, recordPerPage));
            foreach (RMARequestViewModel rmaRequest in rmaRequests.RMARequestList)
            {
                rmaRequest.Flag = (rmaRequest.RequestStatusId.Equals(Convert.ToInt32(RequestStatusModel.RequestStatus.Returned_Refundable))) ? AppendFlag : string.Empty;
            }
            return rmaRequests;
        }

        public List<RequestStatusViewModel> GetRequstStatusList()
        {
            RequestStatusListViewModel requestStatus = RequestStatusViewModelMap.ToListViewModel(_requestStatusClient.GetRequestStatus(null, null, null));
            return (!(Equals(requestStatus, null)) && !(Equals(requestStatus.RequestStatusList, null))) ? requestStatus.RequestStatusList.ToList() : new List<RequestStatusViewModel>();
        }

        public List<PortalViewModel> GetAllPortals()
        {
            PortalListViewModel model = PortalViewModelMap.ToListViewModel(_portalsClient.GetPortals(Expands, Filters, new SortCollection()), null, null);
            return (!Equals(model, null) && !Equals(model.Portals, null)) ? model.Portals.ToList() : new List<PortalViewModel>();
        }

        public RMARequestItemListViewModel GetRMARequestListItem(int orderId, int rmaId, string flag, int? pageIndex = null, int? recordPerPage = null, string mode = "")
        {
            CreateFiltersForRMARequestItem(orderId, rmaId, flag);
            RMARequestItemListViewModel rmaRequestItemListViewModel = RMARequestItemViewModelMap.ToListViewModel(_rmaRequestItemClient.GetRMARequestItemList(Expands, Filters, new SortCollection(), pageIndex - 1, recordPerPage));
            if (!Equals(rmaRequestItemListViewModel, null) && !Equals(rmaRequestItemListViewModel.RMARequestItems, null))
            {
                int rowCount = 0;
                foreach (RMARequestItemViewModel rmaRequestItem in rmaRequestItemListViewModel.RMARequestItems)
                {
                    int maxQuantity, rmaMaxQuantity, rmaQuantity;
                    SetMaxQuantity(rmaRequestItem, out maxQuantity, out rmaMaxQuantity, out rmaQuantity);

                    maxQuantity = (string.IsNullOrEmpty(mode)) ? (rmaRequestItem.MaxQuantity.GetValueOrDefault() - rmaMaxQuantity) : (rmaMaxQuantity - rmaQuantity);

                    if (maxQuantity.Equals(0) && flag.Equals(AppendFlag))
                    {
                        maxQuantity = rmaMaxQuantity;
                        rmaRequestItem.QuantityDropDownEnabled = false;
                        rmaRequestItem.IsReturnable = true;
                        rmaRequestItem.IsReturnableCheckboxEnabled = false;
                    }
                    rmaRequestItem.RmaQuantitySelectList = CreateQuantityDropDown(maxQuantity);
                    rmaRequestItem.RowCount = rowCount;
                    rowCount++;

                    rmaRequestItem.Quantity = maxQuantity;
                    rmaRequestItem.Price = rmaRequestItem.PriceWithoutCurrencySymbol < 0 ? string.Format("({0})", rmaRequestItem.Price.Replace("-", "")) : rmaRequestItem.Price;
                    rmaRequestItem.TotalPrice = Convert.ToDecimal(rmaRequestItem.PriceWithoutCurrencySymbol.GetValueOrDefault()) * rmaRequestItem.MaxQuantity.GetValueOrDefault();
                    rmaRequestItem.TotalPriceString = MvcAdminUnits.GetCurrencyValue(rmaRequestItem.TotalPrice).Replace("-", "");
                }
                rmaRequestItemListViewModel.Total = rmaRequestItemListViewModel.SubTotal + rmaRequestItemListViewModel.Tax;
                return rmaRequestItemListViewModel;
            }

            return new RMARequestItemListViewModel();
        }

        public bool UpdateRMARequest(int rmaRequestId, RMARequestViewModel rmaRequest, string mode = "")
        {
            if (!Equals(rmaRequest, null))
            {
                if (mode.ToLower().Equals(VoidMode))
                {
                    rmaRequest.RequestStatusId = Convert.ToInt32(RequestStatusModel.RequestStatus.Void);
                }
                rmaRequest = RMARequestViewModelMap.ToViewModel(_rmaRequestClient.UpdateRMARequest(rmaRequestId, RMARequestViewModelMap.ToModel(rmaRequest)));
            }
            return (Equals(rmaRequest, null) && rmaRequest.RMARequestID < 1) ? false : true;
        }

        public RMARequestViewModel GetRMARequest(int rmaRequestId)
        {
            RMARequestViewModel rmaRequestViewModel = new RMARequestViewModel();
            if (rmaRequestId > 0)
            {
                rmaRequestViewModel = RMARequestViewModelMap.ToViewModel(_rmaRequestClient.GetRMARequest(rmaRequestId));
            }
            return rmaRequestViewModel;
        }

        public RMARequestItemListViewModel GetRMARequestItemsForGiftCard(string orderLineItems)
        {
            RMARequestItemListViewModel rmaRequestItemListViewModel = new RMARequestItemListViewModel();
            if (!string.IsNullOrEmpty(orderLineItems))
            {
                rmaRequestItemListViewModel = RMARequestItemViewModelMap.ToListViewModel(_rmaRequestItemClient.GetRMARequestItemsForGiftCard(orderLineItems));
            }
            return rmaRequestItemListViewModel;
        }

        public bool DeleteRMARequest(int rmaRequestId)
        {
            try
            {
                return _rmaRequestClient.DeleteRMARequest(rmaRequestId);
            }
            catch
            {
                return false;
            }
        }

        public void RMARequestGiftCardDetails(RMARequestItemListViewModel rmaRequestItems, int rmaRequestId)
        {
            IssuedGiftCardListModel giftCardDetails = _rmaRequestClient.GetRMARequestGiftCardDetails(rmaRequestId);
            rmaRequestItems.GiftCardsIssued = IssuedGiftCardViewModelMap.ToListViewModel(giftCardDetails);
        }

        public bool CreateRMARequest(CreateRequestViewModel model, out string errorMessage)
        {
            try
            {
                errorMessage = string.Empty;
                RMARequestModel request = _rmaRequestClient.CreateRMARequest(RMARequestViewModelMap.ToModel(model));
                model.RMARequestId = request.RMARequestID;
                return model.RMARequestId > 0;
            }
            catch (ZnodeException ex)
            {
                errorMessage = ex.ErrorMessage;
                return false;
            }
        }

        public string GenerateRequestNumber()
        {
            //This method will generate RMA Request number in following format: [store id]-[year]-[alphaCharacters]-[alphaNumericCharacters]: . 

            string alphaNumericCharactersSet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            int alphaCharactersLength = 3;
            int alphaNumericCharactersLength = 5;

            Random random = new Random();
            string alphaCharacters = new string(
                Enumerable.Repeat(alphaNumericCharactersSet, alphaCharactersLength)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());

            string alphaNumericCharacters = new string(
                 Enumerable.Repeat(alphaNumericCharactersSet, alphaNumericCharactersLength)
              .Select(s => s[random.Next(s.Length)])
              .ToArray());

            return string.Concat(PortalAgent.CurrentPortal.PortalId.ToString(), "-", DateTime.Now.ToString("yy"), "-", alphaCharacters, "-", alphaNumericCharacters);
        }

        public bool IsRMAEnable(int orderId, string orderStatus, string orderDate)
        {
            bool enable = false;
            //To check order state
            if (Equals(orderStatus.ToLower(), Convert.ToString(OrderState.Cancelled).ToLower()) || Equals(orderStatus.ToLower(), Convert.ToString(OrderState.Returned).ToLower()))
            {
                return false;
            }
            //Check rma config
            RMAConfigurationListModel rmaConfig = _rmaConfigurationClient.GetAllRMAConfigurations();
            int rmaPeriod = 0;
            if (!Equals(rmaConfig, null) && !Equals(rmaConfig.RMAConfigurations, null) && rmaConfig.RMAConfigurations.Count > 0)
            {
                if (rmaConfig.RMAConfigurations[0].MaxDays.HasValue)
                {
                    rmaPeriod = rmaConfig.RMAConfigurations[0].MaxDays.Value;
                }
                DateTime orderdate = Convert.ToDateTime(orderDate).AddDays(rmaPeriod);
                if (orderdate.Date < DateTime.Now.Date)
                { return false; }
            }
            //To check rma orderflag
            if (!Equals(orderId, 0))
            {
                enable = _rmaRequestClient.GetOrderRMAFlag(orderId);
            }
            return enable;
        }

        public List<SelectListItem> BindReasonForReturn()
        {
            FilterCollection filters = new FilterCollection();
            filters.Add(new FilterTuple(FilterKeys.IsActive, FilterOperators.Equals, FilterKeys.ActiveIndTrue));
            SortCollection sort = new SortCollection();
            sort.Add(SortKeys.Name, SortDirections.Ascending);
            _reasonForReturnClient.RefreshCache = true;
            return RMARequestViewModelMap.ToSelectListItems(_reasonForReturnClient.GetListOfReasonForReturn(null, filters, sort).ReasonsForReturn);
        }
        #endregion

        #region Private Methods And Enum
        /// <summary>
        /// Creates Quantity drop down.
        /// </summary>
        /// <param name="filters">Filter to be created for RMA Request.</param>
        /// <returns>Filter collection for RMA Request.</returns>
        private FilterCollection ApplyRMARequestsFilters(FilterCollection filters)
        {
            filters = (Equals(filters, null)) ? new FilterCollection() : filters;
            foreach (FilterTuple filterItem in filters)
            {
                if (filterItem.Item1.ToString().ToLower().Equals(FilterKeys.RequestDate))
                {
                    filters.Add(new FilterTuple(filterItem.Item1, FilterOperators.Equals, filterItem.Item3));
                    filters.Remove(filterItem);
                    break;
                }
            }
            filters.Add(new FilterTuple(FilterKeys.Username, FilterOperators.Equals, string.Format("'{0}'", HttpContext.Current.User.Identity.Name)));
            ReplaceFilterKeyName(ref filters, FilterKeys.BeginDate, FilterKeys.RequestDate);
            ReplaceFilterKeyName(ref filters, FilterKeys.EndDate, FilterKeys.RequestDate);
            ReplaceFilterKeyName(ref filters, FilterKeys.FirstName, FilterKeys.BillingFirstName);
            ReplaceFilterKeyName(ref filters, FilterKeys.LastName, FilterKeys.BillingLastName);
            return filters;
        }

        /// <summary>
        /// Creates Quantity Drop down for RMA Requdst Items.
        /// </summary>
        /// <param name="maxQuantity">Max quantity limit for RMA Request items.</param>
        /// <returns>Quantity select list items for drop down.</returns>
        private static List<SelectListItem> CreateQuantityDropDown(int maxQuantity)
        {
            List<SelectListItem> rmaQuantityList = new List<SelectListItem>();
            for (int count = 0; count < maxQuantity; count++)
            {
                rmaQuantityList.Add(new SelectListItem { Text = (count + 1).ToString(), Value = (count + 1).ToString(), Selected = Equals(maxQuantity, (count + 1)) });
            }
            return rmaQuantityList;
        }

        /// <summary>
        /// Sets the max quantity of an RMA request item.
        /// </summary>
        /// <param name="rmaRequestItem">RMA Request Item model.</param>
        /// <param name="maxQuantity">Max quantity of an RMA Request item.</param>
        /// <param name="rmaMaxQuantity">RMA max quantity of RMA request item.</param>
        /// <param name="rmaQuantity">RMA Quantity of RMA Request item.</param>
        private static void SetMaxQuantity(RMARequestItemViewModel rmaRequestItem, out int maxQuantity, out int rmaMaxQuantity, out int rmaQuantity)
        {
            maxQuantity = 0;
            rmaMaxQuantity = rmaRequestItem.RMAMaxQuantity;
            rmaQuantity = rmaRequestItem.RMAQuantity;

            rmaRequestItem.IsReturnableCheckboxEnabled = true;
            rmaRequestItem.QuantityDropDownEnabled = true;
        }

        /// <summary>
        /// Creates filter for RMA Request item.
        /// </summary>
        /// <param name="orderId">Order ID filter for RMA request item.</param>
        /// <param name="rmaId">RMA ID filter for RMA Request item.</param>
        /// <param name="flag">Flag filter for RMA request item.</param>
        private void CreateFiltersForRMARequestItem(int orderId, int rmaId, string flag)
        {
            Filters = new FilterCollection();
            Filters.Add(new FilterTuple(FilterKeys.OrderId, FilterOperators.Equals, orderId.ToString()));
            Filters.Add(new FilterTuple(FilterKeys.RMAId, FilterOperators.Equals, rmaId.ToString()));
            Filters.Add(new FilterTuple(FilterKeys.Flag, FilterOperators.Equals, flag));
        }
        #endregion
    }
}