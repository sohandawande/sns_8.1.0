﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public class ManageSearchIndexAgent : BaseAgent, IManageSearchIndexAgent
    {
        #region Private Variables
        private readonly IManageSearchIndexClient _searchIndex;
        #endregion

        #region Constructor
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public ManageSearchIndexAgent()
        {
            _searchIndex = GetClient<ManageSearchIndexClient>();
        }
        #endregion

        #region Public Methods
        public LuceneIndexListViewModel GetLuceneIndexStatusList(FilterCollectionDataModel filters)
        {
            LuceneIndexListViewModel model = LuceneIndexModelMap.ToListViewModel(_searchIndex.GetLuceneIndexStatus(filters.Filters, filters.SortCollection, filters.Page-1, filters.RecordPerPage));
            return model;
        }

        public bool CreateIndex()
        {
            _searchIndex.CreateIndex();
            return true;
        }

        public int DisableTriggers(int flag)
        {
            _searchIndex.DisableTriggers(flag);
            return flag;
        }

        public int DisableWinservice(int flag)
        {
            _searchIndex.DisableWinservice(flag);
            return flag;
        }

        public LuceneIndexServerListViewModel GetSearchIndexStatus(int indexId)
        {
            var model = _searchIndex.GetSearchIndexStatus(indexId);
            return LuceneIndexModelMap.ToServerListViewModel(model);
        }
        #endregion


    }
}