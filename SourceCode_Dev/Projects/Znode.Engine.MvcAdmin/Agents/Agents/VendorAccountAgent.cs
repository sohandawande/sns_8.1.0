﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public class VendorAccountAgent : BaseAgent, IVendorAccountAgent
    {
        #region Private Variables

        private readonly ISuppliersClient _supplierClient;
        private readonly IVendorAccountClient _vendorAccountClient;
        private readonly IPortalsClient _portalsClient;
        private readonly IProfilesClient _profileClient;
        private readonly IAddressesClient _addressClient;
        private readonly ICountriesClient _countryClient;
        private readonly IAccountsClient _accountClient;
        private readonly ICountriesClient _countriesClient;
        private readonly IPortalAgent _portalAgent;
        private const string PhoneNo = "vendorcontact.phonenumber";
        private const string Email = "vendorcontact.email";
        #endregion

        #region Constructor

        public VendorAccountAgent()
        {
            _supplierClient = GetClient<SuppliersClient>();
            _vendorAccountClient = GetClient<VendorAccountClient>();
            _portalsClient = GetClient<PortalsClient>();
            _profileClient = GetClient<ProfilesClient>();
            _addressClient = GetClient<AddressesClient>();
            _countryClient = GetClient<CountriesClient>();
            _accountClient = GetClient<AccountsClient>();
            _countriesClient = GetClient<CountriesClient>();
            _portalAgent = new PortalAgent();
        }

        #endregion

        #region Public Methods

        public VendorAccountListViewModel GetVendorAccountList(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            VendorAccountListViewModel model = new VendorAccountListViewModel();

            filters.Add(new FilterTuple(FilterKeys.RoleName, FilterOperators.Like, FilterKeys.Vendor));
            filters.Add(new FilterTuple(FilterKeys.Username, FilterOperators.Equals, string.Format("'{0}'", HttpContext.Current.User.Identity.Name)));

            ReplaceFilterKeyName(ref filters, PhoneNo, MvcAdminConstants.PhoneNumber);
            ReplaceFilterKeyName(ref filters, Email, MvcAdminConstants.Email);
            ReplaceFilterKeyName(ref filters, MvcAdminConstants.StartDate, FilterKeys.CreateDate);
            ReplaceFilterKeyName(ref filters, MvcAdminConstants.EndDate, FilterKeys.CreateDate);

            ReplaceSortKeyName(sortCollection, PhoneNo, MvcAdminConstants.PhoneNumber);
            ReplaceSortKeyName(sortCollection, Email, MvcAdminConstants.Email);

            foreach (var filter in filters)
            {
                if (filter.Item1.ToLower().Equals(FilterKeys.AccountId.ToLower()))
                {
                    filters.Add(new FilterTuple(FilterKeys.AccountId, FilterOperators.Like, filter.Item3));
                    filters.Remove(filter);
                    break;
                }
            }

            _vendorAccountClient.RefreshCache = true;
            VendorAccountListModel accountList = _vendorAccountClient.GetVendorAccountList(null, filters, sortCollection, pageIndex, recordPerPage);

            return (Equals(accountList, null) && (Equals(accountList.VendorAccount, null))) ? new VendorAccountListViewModel() : VendorAccountViewModelMap.ToListViewModel(accountList);
        }

        public VendorAccountDownloadListViewModel GetVendorAccountToDownload(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            VendorAccountDownloadListViewModel model = new VendorAccountDownloadListViewModel();
            ReplaceFilterKeyName(ref filters, PhoneNo, MvcAdminConstants.PhoneNumber);
            ReplaceFilterKeyName(ref filters, Email, MvcAdminConstants.Email);
            ReplaceFilterKeyName(ref filters, MvcAdminConstants.StartDate, FilterKeys.CreateDate);
            ReplaceFilterKeyName(ref filters, MvcAdminConstants.EndDate, FilterKeys.CreateDate);

            ReplaceSortKeyName(sortCollection, PhoneNo, MvcAdminConstants.PhoneNumber);
            ReplaceSortKeyName(sortCollection, Email, MvcAdminConstants.Email);
            _vendorAccountClient.RefreshCache = true;
            var accountList = _vendorAccountClient.GetVendorAccountList(null, filters, sortCollection, pageIndex, recordPerPage);
            return (Equals(accountList, null) && (Equals(accountList.VendorAccount, null))) ? new VendorAccountDownloadListViewModel() : VendorAccountViewModelMap.ToDownload(accountList);
        }

        public List<PortalModel> BindStoreList()
        {
            PortalListModel list = new PortalListModel();
            list = _portalsClient.GetPortals(null, null, null);
            return list.Portals.ToList();
        }

        public List<ProfileModel> BindProfileList()
        {
            ProfileListModel list = new ProfileListModel();

            List<ProfileModel> profileList = new List<ProfileModel>();
            list = _profileClient.GetProfilesAssociatedWithUsers(HttpContext.Current.User.Identity.Name);

            if (!Equals(list.Profiles, null))
            {
                profileList = list.Profiles.ToList();
            }
            return profileList;
        }

        public List<PortalViewModel> GetPortalsList()
        {
            PortalListViewModel portalListModel = _portalAgent.GetPortals();
            return !Equals(portalListModel, null) && !Equals(portalListModel.Portals, null) ? (portalListModel.Portals.ToList()) : new List<PortalViewModel>();
        }

        public bool CreateVendorAccount(VendorAccountViewModel model, out int accountId, out string message)
        {
            accountId = 0;
            try
            {
                message = string.Empty;
                model.CompanyName = model.VendorContact.CompanyName;
                AccountModel vendorAccount = _vendorAccountClient.CreateVendorAccount(VendorAccountViewModelMap.ToAccountModel(model));
                accountId = vendorAccount.AccountId;
                if ((accountId > 0) && (vendorAccount.IsEmailSentFailed))
                {
                    message = string.Format("{0} {1}", ZnodeResources.ErrorSmptSetting, PortalAgent.CurrentPortal.StoreName);
                    return false;
                }
                return (vendorAccount.AccountId > 0) ? true : false;
            }
            catch (ZnodeException ex)
            {
                message = ex.ErrorMessage;
                return false;
            }
        }

        public bool CreateAddress(AddressViewModel model)
        {
            _addressClient.RefreshCache = true;
            var address = _addressClient.CreateAddress(AddressViewModelMap.ToModel(model));
            return (address.AddressId > 0) ? true : false;
        }

        public VendorAccountViewModel GetVendorAccountById(int accountId)
        {
            _vendorAccountClient.RefreshCache = true;

            var vendorAccount = _vendorAccountClient.GetVendorAccountById(accountId);
            if (Equals(vendorAccount, null))
            {
                return new VendorAccountViewModel();
            }
            var vendors = VendorAccountViewModelMap.ToViewModel(vendorAccount);

            return vendors;
        }

        public AddressViewModel GetAddressById(int addressId)
        {
            _addressClient.RefreshCache = true;
            var address = _addressClient.GetAddress(addressId, true);
            return AddressViewModelMap.ToViewModel(address);
        }

        public bool UpdateAddress(AddressViewModel model)
        {
            var address = _addressClient.UpdateAddress(model.AddressId, AddressViewModelMap.ToModel(model));
            return address.AddressId > 0;
        }

        //new method
        public AddressViewModel UpdateAddressDetails(AddressViewModel model)
        {
            AddressViewModel viewModel = new AddressViewModel();
            try
            {
                var defaultAddress = _addressClient.UpdateDefaultAddress(model.AddressId, AddressViewModelMap.ToModel(model));
                viewModel = AddressViewModelMap.ToViewModel(defaultAddress);
            }
            catch (ZnodeException ex)
            {
                switch (ex.ErrorCode)
                {
                    case ErrorCodes.DefaultBillingAddressNoSet:
                        return new AddressViewModel { HasError = true, ErrorMessage = ZnodeResources.ErrorDefaultBillingAddress };
                    case ErrorCodes.DefaultShippingNoSet:
                        return new AddressViewModel { HasError = true, ErrorMessage = ZnodeResources.ErrorDefaultShippingAddress };
                    case ErrorCodes.NoShippingFacility:
                        return new AddressViewModel { HasError = true, ErrorMessage = ZnodeResources.ErrorCountryNoShippingFacility };
                    default:
                        return viewModel;
                }
            }
            return viewModel;
        }

        public AddressListViewModel GetAddressDetails(int accountId, FilterCollection fromFilters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            AddressListViewModel model = new AddressListViewModel();
            fromFilters.Add(new FilterTuple(MvcAdminConstants.AccountId, FilterOperators.Equals, accountId.ToString()));
            _addressClient.RefreshCache = true;
            var addressList = _addressClient.GetAddresses(fromFilters, sortCollection, pageIndex - 1, recordPerPage);
            if (!Equals(addressList, null) && !Equals(addressList.Addresses, null) && addressList.Addresses.Count > 0)
            {
                model = VendorAccountViewModelMap.ToAccountAddressList(addressList.Addresses, addressList.TotalResults);
            }
            return model;
        }

        public bool UpdateVendorAccount(VendorAccountViewModel model)
        {
            var account = _vendorAccountClient.UpdateVendorAccount(model.AccountId, VendorAccountViewModelMap.ToAccountModel(model));
            return account.AccountId > 0;
        }

        public AddressViewModel GetCountryList()
        {
            AddressViewModel model = new AddressViewModel();
            model.CountryList = VendorAccountViewModelMap.ToListItems(_countryClient.GetCountries(Expands, Filters, Sorts).Countries);
            return model;
        }

        public bool CheckDefaultAddress(int addressId)
        {
            var address = _addressClient.GetAddress(addressId, true);
            if (!Equals(address, null))
            {
                if (address.IsDefaultBilling == true || address.IsDefaultShipping == true)
                {
                    return true;
                }
                return false;
            }
            return true;
        }

        public bool DeleteAddress(int addressId)
        {
            return _addressClient.DeleteAddress(addressId);
        }

        public void DownloadFile(VendorAccountDownloadListViewModel model, HttpResponseBase response, string fileName)
        {
            DownloadHelper downloadHelper = new DownloadHelper();
            downloadHelper.Download(model.VendorAccountDownloadList, Convert.ToString(Convert.ToInt32(FileTypes.CSV)), response, null, fileName);
        }

        public VendorAccountViewModel GetShippingActiveCountryByPortalId(int portalId, VendorAccountViewModel model)
        {
            model.VendorContact.CountryList = VendorAccountViewModelMap.ToListItems(_countryClient.GetShippingActiveCountryByPortalId(portalId).Countries);
            return model;
        }
        #endregion
    }
}