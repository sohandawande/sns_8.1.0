﻿using Resources;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;
using Znode.Libraries.Helpers;
namespace Znode.Engine.MvcAdmin.Agents
{
    /// <summary>
    /// Shipping Agent
    /// </summary>
    public class ShippingAgent : BaseAgent, IShippingAgent
    {
        #region Private Variables

        private readonly IShippingOptionsClient _shippingOptionsClient;
        private readonly IShippingTypesClient _shippingTypeClient;
        private readonly IShippingRulesClient _shippingRuleClient;
        private readonly IShippingRuleTypesClient _shippingRuleTypesClient;
        private readonly IProfilesClient _profileClient;
        private readonly ICountriesClient _countriesClient;
        private readonly IPortalProfileClient _portalProfileClient;
        private readonly IShippingServiceCodesClient _shippingServiceCodesClient;
        private readonly ICategoryAgent _categoryAgent;
        private readonly IAccountAgent _accountAgent;

        private const string trueValue = "true";
        private const string selectZeroIndex = "0";
        private const string ClassName = "ClassName";
        #endregion

        #region Constructors

        /// <summary>
        /// Default Constructor.
        /// </summary>
        public ShippingAgent()
        {
            _shippingOptionsClient = GetClient<ShippingOptionsClient>();
            _shippingTypeClient = GetClient<ShippingTypesClient>();
            _shippingRuleClient = GetClient<ShippingRulesClient>();
            _shippingRuleTypesClient = GetClient<ShippingRuleTypesClient>();
            _profileClient = GetClient<ProfilesClient>();
            _countriesClient = GetClient<CountriesClient>();
            _shippingServiceCodesClient = GetClient<ShippingServiceCodesClient>();
            _categoryAgent = new CategoryAgent();
            _portalProfileClient = GetClient<PortalProfileClient>();
            _accountAgent = new AccountAgent();
        }

        #endregion

        #region Public Methods

        #region Shipping Option

        public ShippingOptionListViewModel GetShippingOptions(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            _shippingOptionsClient.RefreshCache = true;
            if (Equals(filters, null))
            {
                filters = new FilterCollection();
            }
            filters.Add(new FilterTuple(FilterKeys.Username, FilterOperators.Contains, HttpContext.Current.User.Identity.Name));
            ShippingOptionListModel list = _shippingOptionsClient.GetShippingOptions(new ExpandCollection { ExpandKeys.ShippingType }, filters, sortCollection, pageIndex - 1, recordPerPage);
            ShippingOptionListViewModel model = ShippingOptionViewModelMap.ToListViewModel(list);

            //Check country code, if it is null then assign to All.
            foreach (ShippingOptionViewModel shippingOption in model.ShippingOptions)
            {
                if (string.IsNullOrEmpty(shippingOption.CountryCode))
                {
                    shippingOption.CountryCode = ZnodeResources.LabelAll;
                }
            }
            return model;
        }

        public ShippingOptionListViewModel GetFranchiseShippingOptions(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            _shippingOptionsClient.RefreshCache = true;
            ShippingOptionListModel franchiseList = new ShippingOptionListModel();
            ShippingOptionListModel list = _shippingOptionsClient.GetFranchiseShippingOptionList(new ExpandCollection { ExpandKeys.ShippingType }, filters, sortCollection, pageIndex, recordPerPage);
            ShippingOptionListViewModel model = ShippingOptionViewModelMap.ToListViewModel(list);

            //Check country code, if it is null then assign to All.
            foreach (ShippingOptionViewModel shippingOption in model.ShippingOptions)
            {
                if (string.IsNullOrEmpty(shippingOption.CountryCode))
                {
                    shippingOption.CountryCode = ZnodeResources.LabelAll;
                }
            }
            return model;
        }

        public ShippingOptionViewModel GetShippingOption(int shippingOptionId, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            _shippingOptionsClient.RefreshCache = true;
            ShippingOptionModel shippingOption = _shippingOptionsClient.GetShippingOption(shippingOptionId, new ExpandCollection { ExpandKeys.ShippingType });
            ShippingRuleListViewModel model = new ShippingRuleListViewModel();
            if (Equals(filters, null))
            {
                filters = new FilterCollection();
            }
            filters.Add(new FilterTuple(FilterKeys.ShippingOptionId, FilterOperators.Equals, shippingOptionId.ToString()));

            _shippingRuleClient.RefreshCache = true;
            var data = _shippingRuleClient.GetShippingRules(new ExpandCollection { ExpandKeys.ShippingType }, filters, sortCollection, pageIndex - 1, recordPerPage);
            if (!Equals(data, null))
            {
                foreach (var item in data)
                {
                    if (item.ShippingOptionId.Equals(shippingOptionId))
                    {
                        ShippingRuleTypeModel type = _shippingRuleTypesClient.GetShippingRuleType(item.ShippingRuleTypeId);
                        model.ShippingRule.Add(new ShippingRuleViewModel()
                        {
                            ShippingRuleId = item.ShippingRuleId,
                            BaseCostWithDollar = Equals(item.BaseCost, null) ? string.Empty : MvcAdminUnits.GetCurrencyValue(item.BaseCost),
                            PerItemCostWithDollar = Equals(item.PerItemCost, null) ? string.Empty : MvcAdminUnits.GetCurrencyValue(item.PerItemCost),
                            UpperLimit = item.UpperLimit,
                            LowerLimit = item.LowerLimit,
                            ShippingRuleType = new ShippingRuleTypeViewModel()
                            {
                                Name = type.Name,
                                Description = type.Description
                            },
                            RuleTypeName = type.Name
                        });
                    }
                }
            }

            shippingOption.ProfileName = Equals(shippingOption.ProfileId, null) ? ZnodeResources.LabelAllProfiles : _categoryAgent.GetProfileName(int.Parse(shippingOption.ProfileId.ToString()));
            ShippingOptionViewModel shippingOptionViewModel = ShippingOptionViewModelMap.ToViewModel(shippingOption);
            shippingOptionViewModel.ShippingRuleList = model;
            return shippingOptionViewModel;
        }

        public int SaveShippingOption(ShippingOptionViewModel model, out string message)
        {
            try
            {
                message = string.Empty;
                if (Equals(model.UserType, UserTypes.FranchiseAdmin.ToString()))
                {
                    ProfileListModel profiles = _profileClient.GetProfileListByPortalId(model.PortalId);
                    int profileId = profiles.Profiles.FirstOrDefault().ProfileId;
                    model.ProfileId = profileId;
                }
                model.ShippingTypeId = GetShippingTypeId(model.ShippingType.ClassName);

                ShippingOptionModel shippingOption = _shippingOptionsClient.CreateShippingOption(ShippingOptionViewModelMap.ToModel(model));
                return shippingOption.ShippingId;
            }
            catch (ZnodeException ex)
            {
                message = ex.ErrorMessage;
                return 0;
            }
        }

        public bool UpdateShippingOption(int shippingOptionId, ShippingOptionViewModel model, out string message)
        {
            try
            {
                message = string.Empty;
                ShippingOptionModel shippingOption = _shippingOptionsClient.UpdateShippingOption(model.ShippingOptionId, ShippingOptionViewModelMap.ToModel(model));
                return true;
            }
            catch (ZnodeException ex)
            {
                message = ex.ErrorMessage;
                return false;
            }
        }

        public bool DeleteShippingOption(int shippingOptionId, out string message)
        {
            try
            {
                message = string.Empty;
                return _shippingOptionsClient.DeleteShippingOption(shippingOptionId);
            }
            catch (ZnodeException ex)
            {
                message = ZnodeResources.DeleteShippingOptionMessage;
                return false;
            }
        }
        #endregion

        #region Shipping Rule

        public ShippingRuleViewModel GetShippingRule(int shippingRuleId)
        {
            _shippingRuleClient.RefreshCache = true;
            ShippingRuleViewModel shippingRuleViewModel = ShippingRuleViewModelMap.ToViewModel(_shippingRuleClient.GetShippingRule(shippingRuleId));
            return shippingRuleViewModel;
        }

        public bool SaveShippingRule(ShippingRuleViewModel model, out string message)
        {
            try
            {
                message = string.Empty;
                ShippingRuleModel shippingRule = _shippingRuleClient.CreateShippingRule(ShippingRuleViewModelMap.ToModel(model));
                return (shippingRule.ShippingRuleId > 0) ? true : false;
            }
            catch (ZnodeException ex)
            {
                message = ex.ErrorMessage;
                return false;
            }
        }

        public bool UpdateShippingRule(int shippingRuleId, ShippingRuleViewModel model, out string message)
        {
            try
            {
                message = string.Empty;
                _shippingRuleClient.RefreshCache = true;
                ShippingRuleModel shippingRule = _shippingRuleClient.UpdateShippingRule(model.ShippingRuleId, ShippingRuleViewModelMap.ToModel(model));
                return Equals(shippingRule, null) ? false : true;
            }
            catch (ZnodeException ex)
            {
                message = ex.ErrorMessage;
                return false;
            }
        }

        public bool DeleteShippingRule(int shippingRuleId)
        {
            return _shippingRuleClient.DeleteShippingRule(shippingRuleId);
        }

        #endregion

        #region Shipping Rule Types

        public ShippingRuleViewModel GetShippingRuleTypes()
        {
            ShippingRuleViewModel model = new ShippingRuleViewModel();
            _shippingRuleTypesClient.RefreshCache = true;
            model.ShippingRuleTypeList = ShippingRuleViewModelMap.ToListItems(_shippingRuleTypesClient.GetShippingRuleTypes(Filters, Sorts).ShippingRuleTypes);
            return model;
        }

        #endregion

        #region Shipping Types

        public ShippingOptionViewModel GetShippingTypes(bool isEditAction = false)
        {
            ShippingOptionViewModel model = new ShippingOptionViewModel();
            _shippingTypeClient.RefreshCache = true;
            FilterCollection filters = new FilterCollection();
            if (!isEditAction)
            {
                filters.Add(new FilterTuple(FilterKeys.IsActive, FilterOperators.Equals, trueValue));
            }
            model.ShippingTypeList = ShippingOptionViewModelMap.ToListItems(_shippingTypeClient.GetShippingTypes(filters, Sorts).ShippingTypes);
            return model;
        }

        #endregion

        #region Profile List

        public ShippingOptionViewModel GetProfileList()
        {
            ShippingOptionViewModel model = new ShippingOptionViewModel();
            model.ProfileList = ShippingOptionViewModelMap.ToListItems(_profileClient.GetProfilesAssociatedWithUsers(HttpContext.Current.User.Identity.Name).Profiles);
            return model;
        }

        #endregion

        #region Country List

        public ShippingOptionViewModel GetCountryList()
        {
            ShippingOptionViewModel model = new ShippingOptionViewModel();
            _countriesClient.RefreshCache = true;
            model.CountryList = ShippingOptionViewModelMap.ToListItems(_countriesClient.GetCountries(Expands, Filters, Sorts).Countries);
            model.CountryList.Insert(0, new SelectListItem() { Text = ZnodeResources.LabelAllCountries, Value = selectZeroIndex });
            return model;
        }

        #endregion

        #region Shipping Service Code

        public ShippingOptionViewModel GetShippingServiceCodeList()
        {
            ShippingOptionViewModel model = new ShippingOptionViewModel();
            model.ShippingServiceCodeList = ShippingOptionViewModelMap.ToListItems(_shippingServiceCodesClient.GetShippingServiceCodes(Expands, Filters, Sorts).ShippingServiceCodes);
            return model;
        }

        public ShippingServiceCodeViewModel GetShippingServiceCode(int shippingServiceCodeId)
        {
            var model = ShippingOptionViewModelMap.ToViewModel(_shippingServiceCodesClient.GetShippingServiceCode(shippingServiceCodeId));
            ShippingServiceCodeViewModel shippingServiceCodeViewModel = new ShippingServiceCodeViewModel();
            shippingServiceCodeViewModel.Code = model.Code;
            shippingServiceCodeViewModel.Description = model.Description;

            return shippingServiceCodeViewModel;
        }

        #endregion

        #region Bind Service List

        public List<SelectListItem> BindServiceList(FilterCollection filters = null, SortCollection sortCollection = null)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            ShippingServiceCodeListModel serviceModel = _shippingServiceCodesClient.GetShippingServiceCodes(null, filters, sortCollection);
            SaveInSession(ZnodeResources.ShippingServiceCodeList, serviceModel);
            return Equals(serviceModel, null) ? null : ShippingOptionViewModelMap.ToListItems(serviceModel.ShippingServiceCodes);
        }

        #endregion

        #region Bind Shipping List

        public List<SelectListItem> BindShippingList(FilterCollection filters = null, SortCollection sortCollection = null, int? portalId = null, string shippingCountryCode = null)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            AddressViewModel shippingCountry = GetShippingAddess();
            shippingCountryCode = shippingCountry.CountryCode;
            _shippingOptionsClient.RefreshCache = true;
            filters = new FilterCollection();
            filters.Add(new FilterTuple(FilterKeys.IsActive, FilterOperators.Equals, FilterKeys.ActiveIndTrue));
            filters.Add(new FilterTuple(FilterKeys.CountryCode, FilterOperators.Equals, shippingCountryCode));
            sortCollection = new SortCollection();
            sortCollection.Add(SortKeys.DisplayOrder, SortDirections.Ascending);
            var model = _shippingOptionsClient.GetShippingOptions(new ExpandCollection { ExpandKeys.ShippingType }, filters, sortCollection, null, null).ShippingOptions;
            return Equals(model, null) ? new List<SelectListItem>() : ShippingOptionViewModelMap.ToListItems(model);
        }

        public List<SelectListItem> BindFranchiseShippingList(int? portalId = null, FilterCollection filters = null, SortCollection sortCollection = null)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            _shippingOptionsClient.RefreshCache = true;
            filters = new FilterCollection();
            if (!Equals(portalId, null))
            {
                ProfileListModel profiles = _profileClient.GetProfileListByPortalId(portalId.Value);
                int profileId = profiles.Profiles.FirstOrDefault().ProfileId;
                filters.Add(new FilterTuple(FilterKeys.ProfileId, FilterOperators.Equals, profileId.ToString()));
            }
            filters.Add(new FilterTuple(FilterKeys.IsActive, FilterOperators.Equals, FilterKeys.ActiveIndTrue));
            sortCollection = new SortCollection();
            sortCollection.Add(SortKeys.DisplayOrder, SortDirections.Ascending);
            var model = _shippingOptionsClient.GetShippingOptions(new ExpandCollection { ExpandKeys.ShippingType }, filters, sortCollection, null, null).ShippingOptions;
            return Equals(model, null) ? new List<SelectListItem>() : ShippingOptionViewModelMap.ToListItems(model);
        }

        #endregion


        public int GetShippingTypeId(string className)
        {
            FilterCollection filters = new FilterCollection();
            filters.Add(new FilterTuple(ClassName, FilterOperators.Equals, className));
            ShippingTypeListModel shippingTypeList = _shippingTypeClient.GetShippingTypes(filters, null);
            return shippingTypeList.ShippingTypes.FirstOrDefault().ShippingTypeId;
        }
        #endregion

        #region Private Method
        private AddressViewModel GetShippingAddess()
        {
            var model = GetFromSession<AddressViewModel>(MvcAdminConstants.ShippingAddressKey);
            if (Equals(model, null))
            {
                var accountModel = _accountAgent.GetAccountViewModel();
                if (!Equals(accountModel, null))
                    model = accountModel.Addresses.FirstOrDefault(x => x.IsDefaultShipping);
            }
            return model;
        }
        #endregion
    }
}