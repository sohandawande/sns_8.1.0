﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public class PRFTCreditApplicationAgent : BaseAgent, IPRFTCreditApplicationAgent
    {
        #region Private Variables
        private readonly IPRFTCreditApplicationClient _crditApplicationClient;
        #endregion

        #region Constructor

        /// <summary>
        /// Constructor for the CaseRequest.
        /// </summary>
        public PRFTCreditApplicationAgent()
        {
            _crditApplicationClient = new PRFTCreditApplicationClient();
        }

        #endregion

        #region Public Methods

        public PRFTCreditApplicationListViewModel GetCreditApplications(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            if (Equals(filters, null))
            {
                filters = new FilterCollection();
            }
            
            filters.Add(new FilterTuple(FilterKeys.Username, FilterOperators.Equals, string.Format("'{0}'", HttpContext.Current.User.Identity.Name)));
            _crditApplicationClient.RefreshCache = true;
            var list = _crditApplicationClient.GetCreditApplications(null, filters, sortCollection, pageIndex, recordPerPage);
            if (Equals(list, null))
            {
                return null;
            }
            return PRFTCreditApplicationViewModelMap.ToListViewModel(list);
        }

        public PRFTCreditApplicationViewModel GetCreditApplication(int creditApplicationId)
        {
            _crditApplicationClient.RefreshCache = true;
            PRFTCreditApplicationModel model = _crditApplicationClient.GetCreditApplication(creditApplicationId);
            PRFTCreditApplicationViewModel viewModel = PRFTCreditApplicationViewModelMap.ToViewModel(model);
            
            return model != null ? viewModel : null;
        }

        public bool UpdateCreditApplication(PRFTCreditApplicationViewModel model)
        {
            PRFTCreditApplicationModel updateModel = PRFTCreditApplicationViewModelMap.ToModel(model);
            var creditApplication = _crditApplicationClient.UpdateCreditApplication(updateModel.CreditApplicationID, updateModel);
            return (creditApplication.CreditApplicationID > 0) ? true : false;
        }

        #endregion
    }
}