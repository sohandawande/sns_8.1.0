﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    /// <summary>
    /// Case Request Agent
    /// </summary>
    public class CaseRequestAgent : BaseAgent, ICaseRequestAgent
    {
        #region Private Variables

        private readonly ICaseRequestsClient _caseRequestsClient;
        private readonly ICatalogsClient _catalogClient;
        private readonly IPortalsClient _portalClient;
        private readonly IAccountsClient _accountsClient;

        private readonly IPortalAgent _portalAgent;
        #endregion

        #region Constructor

        /// <summary>
        /// Constructor for the CaseRequest.
        /// </summary>
        public CaseRequestAgent()
        {
            _caseRequestsClient = GetClient<CaseRequestsClient>();
            _catalogClient = GetClient<CatalogsClient>();
            _accountsClient = GetClient<AccountsClient>();
            _portalClient = GetClient<PortalsClient>();

            _portalAgent = new PortalAgent();
        }

        #endregion

        #region Public Methods

        public CaseRequestListViewModel GetCaseRequests(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            if (Equals(filters, null))
            {
                filters = new FilterCollection();
            }
            ReplaceFilterKeyName(ref filters, FilterKeys.CatalogId, FilterKeys.PortalId);
            ReplaceFilterKeyName(ref filters, FilterKeys.FormatedCreateDate, FilterKeys.CreateDate);
            ReplaceSortKeyName(sortCollection, FilterKeys.CatalogId, FilterKeys.PortalId);
            ReplaceSortKeyName(sortCollection, FilterKeys.FormatedCreateDate, FilterKeys.CreateDate);
            filters.Add(new FilterTuple(FilterKeys.Username, FilterOperators.Equals, string.Format("'{0}'", HttpContext.Current.User.Identity.Name)));
            _caseRequestsClient.RefreshCache = true;
            var list = _caseRequestsClient.GetCaseRequests(null, filters, sortCollection, pageIndex, recordPerPage);
            if (Equals(list, null))
            {
                return null;
            }
            return CaseRequestViewModelMap.ToListViewModel(list);
        }

        public CaseRequestViewModel GetCaseRequest(int caseRequestId)
        {
            _caseRequestsClient.RefreshCache = true;
            CaseRequestModel model = _caseRequestsClient.GetCaseRequest(caseRequestId);
            CaseRequestViewModel viewModel = CaseRequestViewModelMap.ToViewModel(model);
            if (!Equals(viewModel, null))
            {
                viewModel.CaseNoteList = this.GetNotesByCaseRequestId(caseRequestId);
            }
            return model != null ? viewModel : null;
        }

        public bool SaveCaseRequest(CaseRequestViewModel model)
        {
            model.CreateUser = System.Web.HttpContext.Current.User.Identity.Name;
            var caseRequest = _caseRequestsClient.CreateCaseRequest(CaseRequestViewModelMap.ToModel(model));
            return (caseRequest.CaseRequestId > 0) ? true : false;
        }

        public bool UpdateCaseRequest(CaseRequestViewModel model)
        {
            CaseRequestModel updateModel = CaseRequestViewModelMap.ToModel(model);
            var caseRequest = _caseRequestsClient.UpdateCaseRequest(updateModel.CaseRequestId, updateModel);
            return (caseRequest.CaseRequestId > 0) ? true : false;
        }

        public bool DeleteCaseRequest(int caseRequestId)
        {
            return _caseRequestsClient.DeleteCaseRequest(caseRequestId);
        }

        public bool SaveCaseRequestsNotes(NotesViewModel model)
        {
            model.CreateUser = System.Web.HttpContext.Current.User.Identity.Name;
            var caseRequest = _caseRequestsClient.CreateCaseNote(CaseRequestViewModelMap.ToModel(model));
            return (caseRequest.NoteId > 0) ? true : false;
        }

        public List<CatalogModel> GetCatalogList()
        {
            //to sort by name
            var sort = new SortCollection();
            sort.Add(SortKeys.Name, SortDirections.Ascending);
            _catalogClient.RefreshCache = true;
            CatalogListModel catalogList = _catalogClient.GetCatalogs(null, sort);

            if (!(Equals(catalogList, null)) && !(Equals(catalogList.Catalogs, null)))
            {
                return catalogList.Catalogs.ToList();
            }
            return new List<CatalogModel>();
        }

        public List<CaseStatusModel> GetCaseStatusList()
        {
            _caseRequestsClient.RefreshCache = true;
            CaseStatusListModel statusList = _caseRequestsClient.GetCaseStatus(null, null, null);

            if (!(Equals(statusList, null)) && !(Equals(statusList.CaseStatuses, null)))
            {
                return statusList.CaseStatuses.ToList();
            }
            return new List<CaseStatusModel>();
        }

        public CaseRequestViewModel BindPageDropdown(CaseRequestViewModel model)
        {
            //to sort by name
            var sort = new SortCollection();
            sort.Add(SortKeys.StoreName, SortDirections.Ascending);

            model.PortalList = CaseRequestViewModelMap.ToListItems(_portalAgent.GetPortals().Portals);

            model.CasePriorityList = CaseRequestViewModelMap.ToListItems(_caseRequestsClient.GetCasePriority(null, null, sort).CasePriorities);

            model.CaseStatusList = CaseRequestViewModelMap.ToListItems(_caseRequestsClient.GetCaseStatus(null, null, sort).CaseStatuses);

            return model;
        }

        public NotesListViewModel GetNotesByCaseRequestId(int caseRequestId)
        {
            //to sort by name
            var sort = new SortCollection();
            sort.Add(SortKeys.NoteId, SortDirections.Descending);

            var list = _caseRequestsClient.GetCaseNote(null, new FilterCollection()
                                                            {
                                                                new FilterTuple(FilterKeys.CaseId, FilterOperators.Equals,caseRequestId.ToString())
                                                            }, sort);
            return CaseRequestViewModelMap.ToNoteListViewModel(list);
        }

        public NotesListViewModel GetNotesByAccountId(int accountId)
        {
            SortCollection sort = new SortCollection();
            sort.Add(SortKeys.NoteId, SortDirections.Descending);
            _caseRequestsClient.RefreshCache = true;
            var list = _caseRequestsClient.GetCaseNote(null, new FilterCollection()
                                                            {
                                                                new FilterTuple(FilterKeys.AccountId, FilterOperators.Equals, accountId.ToString())
                                                            }, sort);
            return CaseRequestViewModelMap.ToCustomerNoteListViewModel(list);
        }

        public bool ReplyCustomer(CaseRequestViewModel model, out string ErrorMessage)
        {
            ErrorMessage = string.Empty;
            string notificationMessage = string.Empty;
            if (!this.SendEmail(model, out notificationMessage))
            {
                ErrorMessage = notificationMessage;
                return false;
            }
            NotesViewModel note = new NotesViewModel();
            note.CaseId = model.CaseRequestId;
            note.NoteTitle = string.Join(model.EmailSubject, ZnodeResources.TitleEmailReplySubjectTo, ")");
            note.NoteBody = model.EmailMessage;
            bool status = SaveCaseRequestsNotes(note);
            return status;
        }

        #endregion

        #region SendEmail
        /// <summary>
        /// Send Email 
        /// </summary>
        /// <param name="model">Model of type CaseRequestViewModel</param>
        /// <param name="errorMessage">out string errorMessage</param>
        /// <returns>Returns true or false</returns>
        private bool SendEmail(CaseRequestViewModel model, out string errorMessage)
        {
            try
            {
                PortalModel portal = _portalClient.GetPortalInformationByPortalId(model.PortalId);
                
                //PRFT Custom Code :Start
                var fromEmail = string.IsNullOrEmpty(portal.SmtpUsername) ? portal.CustomerServiceEmail : portal.SmtpUsername;
                
                //if (string.IsNullOrEmpty(portal.SmtpUsername) || string.IsNullOrEmpty(portal.SmtpPassword) || string.IsNullOrEmpty(portal.SmtpServer))
                if (string.IsNullOrEmpty(fromEmail) || string.IsNullOrEmpty(portal.SmtpServer))
                {
                    errorMessage = ZnodeResources.ErrorEmailSending;
                    return false;
                }

                //MailMessage EmailContent = new MailMessage(portal.SmtpUsername, model.EmailId);
                MailMessage EmailContent = new MailMessage(fromEmail, model.EmailId);
                EmailContent.Subject = model.EmailSubject;
                EmailContent.Body = model.EmailMessage;
                EmailContent.IsBodyHtml = true;

                // Attachment Steps
                // Get the file name 
                string strFileName = string.Empty;
                strFileName = (!Equals(model.Attachments,null) &&!Equals(model.Attachments.FileName,null)) ?model.Attachments.FileName:string.Empty;

                if (!strFileName.Equals(string.Empty))
                {
                    // Email Attachment
                    Attachment attach = new Attachment(model.Attachments.InputStream, strFileName);

                    // Attach the created email attachment 
                    EmailContent.Attachments.Add(attach);
                }
               
                string SMTPServer = portal.SmtpServer;
                string SMTPUsername = portal.SmtpUsername;
                string SMTPPassword = portal.SmtpPassword;


                errorMessage = string.Empty;
                // Create mail client and send email
                System.Net.Mail.SmtpClient emailClient = new System.Net.Mail.SmtpClient();
                emailClient.Host = SMTPServer;
                emailClient.EnableSsl = portal.EnableSslForSmtp;
                emailClient.Credentials = new NetworkCredential(SMTPUsername, SMTPPassword);

                // Send MailContent
                emailClient.Send(EmailContent);

                EmailContent.Attachments.Dispose();
                return true;
            }
            catch (Exception)
            {
                errorMessage = ZnodeResources.ErrorEmailSending;
                return false;
            }
        }
        #endregion
    }
}