﻿using Znode.Engine.Api.Client;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public class CategoryProfileAgent : BaseAgent, ICategoryProfileAgent
    {
        #region Private Variables
        private readonly ICategoryProfileClient _categoryProfileClient;
        #endregion

        #region Constructor
        public CategoryProfileAgent()
        {
            _categoryProfileClient = GetClient<CategoryProfileClient>();
        }
        #endregion

        #region Public Methods
        public CategoryProfileViewModel GetCategoryProfileByCategoryProfileId(int categoryProfileId = 0)
        {
            _categoryProfileClient.RefreshCache = true;
            CategoryProfileModel model = _categoryProfileClient.GetCategoryProfile(categoryProfileId);
            return Equals(model, null) ? null : CategoryProfileViewModelMap.ToViewModel(model);
        }

        public CategoryProfileListViewModel GetCategoryProfileByCategoryId(int categoryId = 0)
        {
            _categoryProfileClient.RefreshCache = true;
            CategoryProfileListModel models = _categoryProfileClient.GetCategoryProfileByCategoryId(categoryId);
            return Equals(models, null) ? null : CategoryProfileViewModelMap.ToListViewModel(models);
        }

        public CategoryProfileViewModel CreateCategoryProfile(CategoryProfileViewModel model)
        {
            CategoryProfileModel modelSaved = _categoryProfileClient.CreateCategoryProfile(CategoryProfileViewModelMap.ToModel(model));
            return !Equals(modelSaved, null) ? CategoryProfileViewModelMap.ToViewModel(modelSaved) : null;
        }

        public bool UpdatecategoryProfile(int categoryProfileId, CategoryProfileViewModel model)
        {
            CategoryProfileModel updateModel = CategoryProfileViewModelMap.ToModel(model);
            return _categoryProfileClient.UpdateCategoryProfile(categoryProfileId, updateModel);
        }

        public bool DeleteCategoryProfile(int categoryProfileId)
        {
            return _categoryProfileClient.DeleteCategoryProfile(categoryProfileId);
        }
        #endregion

    }
}