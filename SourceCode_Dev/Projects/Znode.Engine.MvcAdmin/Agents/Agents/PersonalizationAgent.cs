﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.Models;
using Znode.Engine.MvcAdmin.ViewModels;
using Znode.Engine.MvcDemo.Maps;

namespace Znode.Engine.MvcAdmin.Agents
{
    /// <summary>
    /// Personalization agent.
    /// </summary>
    public class PersonalizationAgent : BaseAgent, IPersonalizationAgent
    {
        #region Private Variables
        private readonly IPortalsClient _portalClient;
        private readonly ICatalogsClient _catalogClient;
        private readonly IPortalCatalogsClient _portalCatalogClient;
        private readonly ICategoriesClient _categoryClient;
        private readonly IProductsClient _productClient;
        private readonly IPersonalizationClient _personalizationClient;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor for PersonalizationAgent
        /// </summary>
        public PersonalizationAgent()
        {
            _portalClient = GetClient<PortalsClient>();
            _portalCatalogClient = GetClient<PortalCatalogsClient>();
            _catalogClient = GetClient<CatalogsClient>();
            _categoryClient = GetClient<CategoriesClient>();
            _productClient = GetClient<ProductsClient>();
            _personalizationClient = GetClient<PersonalizationClient>();
        }
        #endregion

        #region Public Methods
        public ProductCrossSellListViewModel GetProductListWithAssociatedItems(int portalId, int categoryId, int crossSellType, FilterCollection filters, SortCollection sortCollection, int? pageIndex, int? recordPerPage)
        {
            if (Equals(filters, null))
            {
                filters = new FilterCollection();
            }

            if (!filters.Any(t => Equals(t.Item1.ToLower(), FilterKeys.PortalId)))
            {
                filters.Add(new FilterTuple(FilterKeys.PortalId, FilterOperators.Equals, portalId.ToString()));
            }
            if (!filters.Any(t => Equals(t.Item1.ToLower(), FilterKeys.CategoryId)) && categoryId > 0)
            {
                filters.Add(new FilterTuple(FilterKeys.CategoryId, FilterOperators.Equals, categoryId.ToString()));
            }
            filters.Add(new FilterTuple(FilterKeys.CrossSellType, FilterOperators.Equals, crossSellType.ToString()));

            _personalizationClient.RefreshCache = true;
            return ProductCrossSellViewModelMap.ToCrossSellItemListViewModel(_personalizationClient.GetFrequentlyBoughtProducts(Expands, filters, sortCollection, pageIndex, recordPerPage), pageIndex, recordPerPage, portalId, categoryId);
        }

        public ListViewModel BindCrossSellProducts(int productId, ProductCrossSellListViewModel productList, int relationTypeId)
        {
            ListViewModel listViewModel = new ListViewModel();

            List<SelectListItem> crossSellProductList = ProductCrossSellViewModelMap.GetSelectListItem(productList.CrossSellProducts, MvcAdminConstants.LabelProductId, MvcAdminConstants.LabelProductName);

            crossSellProductList = (from crossSellProduct in crossSellProductList
                                    where !Equals(crossSellProduct.Value, productId.ToString())
                                    select crossSellProduct).ToList();

            Filters = new FilterCollection();
            Filters.Add(new FilterTuple(FilterKeys.ProductId, FilterOperators.Equals, productId.ToString()));
            Filters.Add(new FilterTuple(FilterKeys.RelationTypeId, FilterOperators.Equals, relationTypeId.ToString()));

            _personalizationClient.RefreshCache = true;
            ProductCrossSellListViewModel crossSellProductListViewModel = ProductCrossSellViewModelMap.ToListViewModel(_personalizationClient.GetProductCrossSellByProductId(Filters, new SortCollection()));


            crossSellProductListViewModel.CrossSellProducts = crossSellProductListViewModel.CrossSellProducts.OrderBy(x => x.DisplayOrder).ToList();

            foreach (ProductCrossSellViewModel crossSellItem in crossSellProductListViewModel.CrossSellProducts)
            {
                _productClient.RefreshCache = true;
                ProductModel product = _productClient.GetProduct(crossSellItem.RelatedProductId);
                crossSellItem.RelatedProductName = product.Name;
            }

            List<SelectListItem> crossSellItems = ProductCrossSellViewModelMap.GetSelectListItem(crossSellProductListViewModel.CrossSellProducts, MvcAdminConstants.LabelRelatedProductId, MvcAdminConstants.RelatedProductName);

            string associatedProductIds = string.Empty;
            associatedProductIds = (string.Join(",", crossSellProductListViewModel.CrossSellProducts.Select(x => x.RelatedProductId.ToString()).ToArray()));
            string[] associatedProductIdsArray = associatedProductIds.Split(',');

            listViewModel.UnAssignedList = (from item in crossSellProductList
                                            where !associatedProductIdsArray.Contains(item.Value)
                                            select item).ToList();


            listViewModel.AssignedList = (from item in crossSellItems
                                          select item).ToList();
            return listViewModel;
        }

        public bool DeleteCrossSellProducts(int productId, int relationTypeId)
        {
            return _personalizationClient.DeleteCrossSellProduct(productId, relationTypeId);
        }

        public ProductCrossSellListViewModel GetCrossSellProducts(int productId, int relationTypeId)
        {
            Filters = new FilterCollection();
            Filters.Add(new FilterTuple(FilterKeys.ProductId, FilterOperators.Equals, productId.ToString()));
            Filters.Add(new FilterTuple(FilterKeys.RelationTypeId, FilterOperators.Equals, relationTypeId.ToString()));

            _personalizationClient.RefreshCache = true;
            ProductCrossSellListViewModel crossSellProducts = ProductCrossSellViewModelMap.ToListViewModel(_personalizationClient.GetProductCrossSellByProductId(Filters, new SortCollection()));
            return (!Equals(crossSellProducts, null) && crossSellProducts.CrossSellProducts.Count > 0) ? crossSellProducts : new ProductCrossSellListViewModel();
        }

        public List<SelectListItem> BindPortalDropDownList(string portalId)
        {
            List<SelectListItem> portalItems = new List<SelectListItem>();
            _portalClient.RefreshCache = true;
            IEnumerable<PortalModel> model = _portalClient.GetPortals(null, null, null).Portals;

            if (!Equals(model, null))
            {
                portalItems = (from item in model
                               select new SelectListItem
                               {
                                   Text = item.StoreName,
                                   Value = item.PortalId.ToString(),
                                   Selected = Equals(item.PortalId.ToString(), portalId) ? true : false
                               }).ToList();
            }
            return (!Equals(portalItems, null) && portalItems.Count > 0) ? portalItems : new List<SelectListItem>();
        }

        public PersonalizationViewModel GetCrossSellList(int portalId, int categoryId, int productRelation)
        {
            PersonalizationViewModel personalizationModel = new PersonalizationViewModel();
            personalizationModel.PortalList = BindPortalDropDownList();
            personalizationModel.PortalId = portalId > 0 ? portalId : Convert.ToInt32(personalizationModel.PortalList[0].Value); ;

            CatalogViewModel catalog = GetCatalogByPortalId(personalizationModel.PortalId);
            if (!Equals(catalog, null))
            {
                personalizationModel.CatalogName = catalog.Name;
                personalizationModel.CatalogId = catalog.CatalogId;

                //Get category list according to catalog id.
                personalizationModel.CategoryList = GetCategoryList(catalog.CatalogId);
            }
            return personalizationModel;
        }

        public PersonalizationViewModel GetPersonalizationModel(int? productId, int? portalId, int? categoryId, string productName, string sku, out ProductCrossSellListViewModel productList)
        {
            PersonalizationViewModel personalizationModel = new PersonalizationViewModel();

            personalizationModel.PortalList = BindPortalDropDownList();

            personalizationModel.PortalId = (!Equals(portalId, null) && portalId > 0) ? portalId.Value : Convert.ToInt32(personalizationModel.PortalList[0].Value);
            CatalogViewModel catalog = GetCatalogByPortalId(personalizationModel.PortalId);

            if (!Equals(catalog, null))
            {
                personalizationModel.CatalogName = catalog.Name;
                personalizationModel.CatalogId = catalog.CatalogId;

                //Get category list according to catalog id.
                personalizationModel.CategorySelectList = GetCategorySelectList(catalog.CatalogId);
            }
            //Get list of products according to category id.
            FilterCollection productlistFilter = new FilterCollection();
            if (!string.IsNullOrEmpty(productName))
            {
                productlistFilter.Add(new FilterTuple(FilterKeys.ProductName, FilterOperators.Contains, productName));
            }

            if (!string.IsNullOrEmpty(sku))
            {
                productlistFilter.Add(new FilterTuple(FilterKeys.Sku, FilterOperators.Contains, sku));
            }

            productList = GetProductListWithAssociatedItems(personalizationModel.PortalId, categoryId.Value, (int)ProductRelationCode.FrequentlyBoughtTogether, productlistFilter, new SortCollection(), 1, int.MaxValue);

            personalizationModel.ProductTitle = !Equals(productId.Value, null) ? _productClient.GetProduct(productId.Value).Name : string.Empty;
            personalizationModel.ProductId = productId.Value;
            personalizationModel.PortalId = portalId.Value;
            personalizationModel.CategoryId = categoryId.Value;

            return personalizationModel;
        }

        public void BindManageCrossSellData(PersonalizationViewModel model, int productCrossSellType)
        {
            model.CategorySelectList = GetCategorySelectList(model.CatalogId);
            ProductCrossSellListViewModel productList = GetProductListWithAssociatedItems(model.PortalId, model.CategoryId, productCrossSellType, new FilterCollection(), new SortCollection(), 1, int.MaxValue);
            model.CrossSellProductList = BindCrossSellProducts(model.ProductId, productList, productCrossSellType);

            model.CrossSellType = productCrossSellType;
        }

        public bool CreateProductCrossSellList(PersonalizationViewModel model, int productCrossSellType)
        {
            if (!Equals(model, null))
            {
                ProductCrossSellListViewModel productCrossSellListViewModel = ProductCrossSellViewModelMap.PersonalizationViewModelToProductCrossSellListViewModel(model, productCrossSellType);
                return CreateCrossSellProducts(productCrossSellListViewModel);
            }
            return false;
        }


        #endregion

        #region Private Methods
        private List<SelectListItem> BindPortalDropDownList()
        {
            SortCollection sort = new SortCollection();
            sort.Add(SortKeys.StoreName, SortDirections.Ascending);
            _portalClient.RefreshCache = true;
            List<SelectListItem> portalList = PortalViewModelMap.ToSelectListItems(_portalClient.GetPortals(null, null, sort).Portals);
            return (Equals(portalList, null) ? null : portalList);
        }

        private CatalogViewModel GetCatalogByPortalId(int portalId)
        {
            CatalogViewModel catalogViewModel = new CatalogViewModel();
            _portalCatalogClient.RefreshCache = true;
            PortalCatalogListViewModel model = PortalCatalogViewModelMap.ToListViewModel(_portalCatalogClient.GetPortalCatalogsByPortalId(portalId), null, null);
            if (!Equals(model, null) && !Equals(model.PortalCatalogs, null) && model.PortalCatalogs.Count > 0)
            {
                _catalogClient.RefreshCache = true;
                catalogViewModel = CatalogViewModelMap.ToViewModel(_catalogClient.GetCatalog(model.PortalCatalogs[0].CatalogId));
            }
            return catalogViewModel;
        }

        private List<CategoryModel> GetCategoryList(int catalogId)
        {
            _categoryClient.RefreshCache = true;
            CategoryListModel categoryList = _categoryClient.GetCategoriesByCatalog(catalogId, null, null, null);

            return (!(Equals(categoryList, null)) && !(Equals(categoryList.Categories, null))) ? categoryList.Categories.ToList() : new List<CategoryModel>();
        }

        private bool CreateCrossSellProducts(ProductCrossSellListViewModel model)
        {
            return (!Equals(model, null)) ? _personalizationClient.CreateCrossSellProducts(ProductCrossSellViewModelMap.ToModel(model)) : false;
            //if (!Equals(model, null))
            //{
            //    return _personalizationClient.CreateCrossSellProducts(ProductCrossSellViewModelMap.ToModel(model));
            //}
            //return false;
        }

        private List<SelectListItem> GetCategorySelectList(int catalogId)
        {
            _categoryClient.RefreshCache = true;
            CategoryListViewModel categoryList = CategoryViewModelMap.ToListViewModel(_categoryClient.GetCategoriesByCatalog(catalogId, null, null, null));
            List<SelectListItem> categorySelectList = new List<SelectListItem>();

            if (!(Equals(categoryList, null)) && !(Equals(categoryList.Categories, null)))
            {
                categorySelectList = ProductCrossSellViewModelMap.GetSelectListItem(categoryList.Categories, "CategoryId", "Name", true);
            }
            return categorySelectList;
        }
        #endregion
    }
}