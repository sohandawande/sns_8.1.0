﻿using Resources;
using System.Collections.Generic;
using System.Linq;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;
using Znode.Engine.MvcDemo.Maps;
using Znode.Libraries.Helpers.Extensions;
using Resources;
using System.Configuration;
using System;
using System.Web;
namespace Znode.Engine.MvcAdmin.Agents
{
    public class VendorProductAgent : BaseAgent, IVendorProductAgent
    {
        #region Private Variables

        private readonly IVendorProductClient _vendorProductClient;
        private readonly IProductReviewStateClient _productReviewStateClient;
        private readonly IProductReviewHistoryClient _productReviewHistoryClient;
        private readonly IPortalsClient _portalClient;
        private readonly IAddressesClient _addressClient;
        private readonly IProductsClient _productClient;
        private readonly IProductCategoriesClient _productCategoryClient;
        private readonly IProductTypeClient _productTypeClient;
        #endregion

        #region Constructor

        public VendorProductAgent()
        {
            _vendorProductClient = GetClient<VendorProductClient>();
            _productReviewStateClient = GetClient<ProductReviewStateClient>();
            _productReviewHistoryClient = GetClient<ProductReviewHistoryClient>();
            _portalClient = GetClient<PortalsClient>();
            _addressClient = GetClient<AddressesClient>();
            _productClient = GetClient<ProductsClient>();
            _productCategoryClient = GetClient<ProductCategoriesClient>(); 
            _productTypeClient = GetClient<ProductTypeClient>();
        }

        #endregion

        #region Public Methods

        public VendorProductListViewModel GetMallAdminProductList(FilterCollection fromFilters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            VendorProductListViewModel model = new VendorProductListViewModel();
            _vendorProductClient.RefreshCache = true;
            ReplaceFilterKeyName(ref fromFilters, FilterKeys.VendorName, FilterKeys.Vendor);
            ReplaceFilterKeyName(ref fromFilters, FilterKeys.Name, FilterKeys.ProductName);
            ReplaceFilterKeyName(ref fromFilters, FilterKeys.AccountId, FilterKeys.VendorId);
            ReplaceSortKeyName(sortCollection, SortKeys.DisplayRetailPrice, SortKeys.RetailPrice);
            ReplaceSortKeyName(sortCollection, SortKeys.VendorName, SortKeys.Vendor);

            var productList = _vendorProductClient.GetMallAdminProductList(null, fromFilters, sortCollection, pageIndex, recordPerPage);
            if (!Equals(productList, null) && !Equals(productList.Products, null) && productList.Products.Count > 0)
            {
                model = ProductViewModelMap.ToVendorProductListViewModel(productList.Products, productList.TotalResults);
            }
            var reviewStateList = _productReviewStateClient.GetProductReviewStates(null, null, null);
            if (!Equals(reviewStateList, null) && !Equals(reviewStateList.ProductReviewStates, null) && reviewStateList.ProductReviewStates.Count > 0)
            {
                model.ReviewStates = ProductViewModelMap.ToProductReviewListModel(reviewStateList);
            }
            return model;
        }

        public VendorProductListViewModel GetVendorProductList(FilterCollection fromFilters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            VendorProductListViewModel model = new VendorProductListViewModel();
            _vendorProductClient.RefreshCache = true;
            ReplaceFilterKeyName(ref fromFilters, FilterKeys.VendorName, FilterKeys.Vendor);
            ReplaceFilterKeyName(ref fromFilters, FilterKeys.Name, FilterKeys.ProductName);
            ReplaceFilterKeyName(ref fromFilters, FilterKeys.AccountId, FilterKeys.VendorId);
            ReplaceSortKeyName(sortCollection, SortKeys.VendorName, SortKeys.Vendor);
            ReplaceSortKeyName(sortCollection, SortKeys.DisplayRetailPrice, SortKeys.RetailPrice);
            if (Equals(fromFilters,null))
            {
                fromFilters = new FilterCollection();
            }
            fromFilters.Add(new FilterTuple(FilterKeys.Username, FilterOperators.Equals, string.Format("'{0}'", HttpContext.Current.User.Identity.Name)));

            var productList = _vendorProductClient.GetVendorProductList(null, fromFilters, sortCollection, pageIndex, recordPerPage);
            ReplaceSortKeyName(sortCollection, SortKeys.Vendor, SortKeys.VendorName);
            if (!Equals(productList, null) && !Equals(productList.Products, null) && productList.Products.Count > 0)
            {
                model = ProductViewModelMap.ToVendorProductListViewModel(productList.Products, productList.TotalResults);
            }
            var reviewStateList = _productReviewStateClient.GetProductReviewStates(null, null, null);
            if (!Equals(reviewStateList, null) && !Equals(reviewStateList.ProductReviewStates, null) && reviewStateList.ProductReviewStates.Count > 0)
            {
                model.ReviewStates = ProductViewModelMap.ToProductReviewListModel(reviewStateList);
            }
            return model;
        }

        public bool ChangeProductStatus(VendorProductListViewModel model, ZNodeProductReviewState state)
        {
            return _vendorProductClient.ChangeProductStatus(VendorProductViewModelMap.ToModel(model), state.ToString());
        }

        public VendorProductListViewModel BindRejectProduct(string productIds)
        {
            var data = _vendorProductClient.BindRejectProduct(productIds);
            return VendorProductViewModelMap.ToViewModel(data.RejectProduct);
        }

        public ProductAlternateImageListViewModel GetReviewImageList(FilterCollection fromFilters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            ProductAlternateImageListViewModel model = new ProductAlternateImageListViewModel();

            ReplaceFilterKeyName(ref fromFilters, MvcAdminConstants.VendorName, FilterKeys.Vendor);
            ReplaceFilterKeyName(ref fromFilters, MvcAdminConstants.ProductSkuName, FilterKeys.Sku);
            ReplaceFilterKeyName(ref fromFilters, MvcAdminConstants.ReviewStateID, FilterKeys.ProductStatus);

            var productImageList = _vendorProductClient.GetReviewImageList(null, fromFilters, sortCollection, pageIndex, recordPerPage);
            if (!Equals(productImageList, null) && !Equals(productImageList.ProductImages, null) && productImageList.ProductImages.Count > 0)
            {
                model = ProductImageTypeModelMap.ToListViewModel(productImageList.ProductImages, productImageList.TotalResults);
            }
            var reviewStateList = _productReviewStateClient.GetProductReviewStates(null, null, null);
            if (!Equals(reviewStateList, null) && !Equals(reviewStateList.ProductReviewStates, null) && reviewStateList.ProductReviewStates.Count > 0)
            {
                model.ImageReviewStates = ProductViewModelMap.ToProductReviewListModel(reviewStateList);
            }
            return model;
        }

        public bool UpdateProductImageStatus(string productIds, string state)
        {
            return _vendorProductClient.UpdateProductImageStatus(productIds, state);
        }


        public ProductReviewHistoryListViewModel GetProductReviewHistory(int productId, FilterCollection fromFilters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            ProductReviewHistoryListViewModel model = new ProductReviewHistoryListViewModel();
            fromFilters.Add(new FilterTuple(MvcAdminConstants.ProductId, FilterOperators.Equals, productId.ToString()));
            var productList = _productReviewHistoryClient.GetProductReviewHistory(null, fromFilters, sortCollection, pageIndex - 1, recordPerPage);
            if (!Equals(productList, null) && !Equals(productList.ProductReviewHistory, null) && productList.ProductReviewHistory.Count > 0)
            {
                model = VendorProductViewModelMap.ToProductReviewList(productList.ProductReviewHistory, productList.TotalResults);
            }
            return model;
        }

        public ProductReviewHistoryViewModel GetProductReviewHistoryById(int productReviewHistoryID)
        {
            ProductReviewHistoryViewModel model = new ProductReviewHistoryViewModel();
            var historyDetails = _productReviewHistoryClient.GetReviewHistoryById(productReviewHistoryID);
            if (!Equals(historyDetails, null))
            {
                model = VendorProductViewModelMap.ToProductReviewView(historyDetails);
            }
            return model;
        }

        public bool UpdateVendorProduct(VendorProductViewModel model, out string errorMessage)
        {
            try
            {
                errorMessage = string.Empty;
            var product = _vendorProductClient.UpdateVendorProduct(model.ProductModel.ProductId, ProductViewModelMap.ToModel(model.ProductModel));
                if (product.ProductId > 0)
                {
                    SaveImage(model.ProductModel);
        }
                return product.ProductId > 0 ? true : false;
            }
            catch (ZnodeException ex)
            {
                errorMessage = ex.ErrorMessage;
                return false;
            }
        }

        public bool SaveVendorProduct(VendorProductViewModel model, out string errorMessage)
        {
            try
            {
                errorMessage = string.Empty;
                model.ProductModel.AccountID = GetAccountIdFromSession();
                var product = _productClient.CreateProduct(ProductViewModelMap.ToModel(model.ProductModel));
                model.ProductModel.ProductId = product.ProductId;
                if (product.ProductId > 0)
                {
                    SaveImage(model.ProductModel);
                }
                return model.ProductModel.ProductId > 0 ? true : false;
            }
            catch (ZnodeException ex)
            {
                errorMessage = ex.ErrorMessage;
                return false;
            }
        }

        public string GetVendorName(int? portalId, int? accountId)
        {
            string vendorName = string.Empty;
            //// For Franchise Admin
            if (!Equals(portalId, null) && portalId > 0)
            {
                var portal = _portalClient.GetPortalInformationByPortalId((int)portalId);
                if (!Equals(portal, null))
                {
                    vendorName = portal.CompanyName;
                }
            }
            //// For Mall Admin
            else if (!Equals(accountId, null) && accountId > 0)
            {
                FilterCollection filters = new FilterCollection();
                filters.Add(new FilterTuple(FilterKeys.AccountId, FilterOperators.Equals, accountId.ToString()));
                var address = _addressClient.GetAddresses(filters, null);
                if (!Equals(address, null) && !Equals(address.Addresses, null))
                {
                    vendorName = string.Format("{0} {1} {2}", address.Addresses[0].FirstName, address.Addresses[0].LastName, address.Addresses[0].CompanyName);
                }
            }
            return vendorName;
        }

        public VendorProductMarketingViewModel GetVendorProductMarketingDetails(int productId)
        {
            VendorProductMarketingViewModel model = new VendorProductMarketingViewModel();
            var product = _productClient.GetProductDetailsByProductId(productId);
            if (!Equals(product, null)) 
            {
                model = VendorProductViewModelMap.ToMarketingViewModel(product);
            }
            return model;
        }

        public VendorProductMarketingViewModel UpdateMarketingDetails(VendorProductMarketingViewModel model)
        {
            VendorProductMarketingViewModel viewModel = new VendorProductMarketingViewModel();
            try
            {
                var product = _vendorProductClient.UpdateMarketingDetails(VendorProductViewModelMap.ToProductModel(model));
                viewModel = VendorProductViewModelMap.ToMarketingViewModel(product);
            }
            catch (ZnodeException ex)
            {
                switch (ex.ErrorCode)
                {
                    case ErrorCodes.SEOUrlAlreadExists:
                        return new VendorProductMarketingViewModel { HasError = true, ErrorMessage = ZnodeResources.ErrorSeoUrlAlreadyExist };
                    default:
                        return viewModel;
                }
            }
            return viewModel;
        }

        public VendorProductPreviewViewModel GetProduct(int productId)
        {
            VendorProductPreviewViewModel productViewModel = new VendorProductPreviewViewModel();
            Expands = new ExpandCollection();
            Expands.Add(ExpandKeys.AddOns);
            Expands.Add(ExpandKeys.Images);
            Expands.Add(ExpandKeys.YouMayAlsoLike);
            Expands.Add(ExpandKeys.FrequentlyBoughtTogether);
            _productClient.RefreshCache = true;
            var product = _productClient.GetProduct(productId, Expands);

            if (Equals(product, null)) return productViewModel;

            productViewModel = VendorProductViewModelMap.ToViewModel(product);

            if (!string.IsNullOrEmpty(productViewModel.YMALProductsIds))
            {
                _productClient.RefreshCache = true;
                var yMALProductDetails = _productClient.GetProductsByProductIds(productViewModel.YMALProductsIds, null, null);
                productViewModel.YMALProductList = ProductViewModelMap.ToListViewModel(yMALProductDetails.Products, yMALProductDetails.TotalResults);
            }

            if (!string.IsNullOrEmpty(productViewModel.FBTProductsIds))
            {
                _productClient.RefreshCache = true;
                var fBTProductDetails = _productClient.GetProductsByProductIds(productViewModel.FBTProductsIds, null, null);
                productViewModel.FBTProductList = ProductViewModelMap.ToListViewModel(fBTProductDetails.Products, fBTProductDetails.TotalResults);
            }


            // Populate the product Alternate Image
            if (!Equals(productViewModel.Images, null) && productViewModel.Images.Any())
            {
                //Filter All Product images to get the swatch iamges based on ProductImageTypeId.
                VendorProductPreviewViewModel swatchImageModel = productViewModel;
                productViewModel.SwatchImages = swatchImageModel.Images.Where(x => x.ProductImageTypeID == (int)ProductImageType.Swatch && x.ActiveInd).OrderBy(x => x.DisplayOrder).ToList();
                if (!Equals(productViewModel.SwatchImages, null) && productViewModel.SwatchImages.Any())
                {
                    ImageHelper imageHelper = new ImageHelper();
                    productViewModel.SwatchImages.Add(new ProductAlternateImageViewModel { ImageAltText = productViewModel.ProductModel.ImageAltTag, ImageSmallThumbnailPath = HelperMethods.GetImageRelativePath(productViewModel.ProductModel.ImageSmallThumbnailPath), ImageMediumPath = HelperMethods.GetImageRelativePath(productViewModel.ProductModel.ImageMediumPath), ImageLargePath = HelperMethods.GetImageRelativePath(productViewModel.ProductModel.ImageLargePath) });
                }
                productViewModel.Images = productViewModel.Images.Where(x => x.ProductImageTypeID == (int)ProductImageType.Alternate && x.ActiveInd).OrderBy(x => x.DisplayOrder).ToList();
            }
            return productViewModel;
        }

        public List<ProductCategoryViewModel> GetProductCategory(int productId)
        {
            FilterCollection filters = new FilterCollection();
            filters.Add(new FilterTuple(FilterKeys.ProductId, FilterOperators.Equals, productId.ToString()));
            List<ProductCategoryViewModel> model = VendorProductViewModelMap.ToListViewModel(_productCategoryClient.GetProductCategories(null, filters, null).ProductCategories);
            return (!Equals(model, null)) ? model : null;
        }

        public CategoryNodeListViewModel GetCategoryNode(int productId)
        {
            var data = _vendorProductClient.GetCategoryNode(productId);
            return VendorProductViewModelMap.ToListViewModel(data.CategoryNodes);
        }

        public bool DeleteProduct(int productId)
        {
            return _vendorProductClient.DeleteProduct(productId);
        }

        public ProductViewModel BindProductPageDropdown(ProductViewModel model, int portalId)
        {
            FilterCollection filters = new FilterCollection();
            filters.Add(new FilterTuple(FilterKeys.Franchisable, FilterOperators.Equals, FilterKeys.ActiveIndTrue));

            int accountId = GetAccountIdFromSession();
            if (!Equals(accountId, 0))
            {
                filters.Add(new FilterTuple(FilterKeys.VendorId, FilterOperators.Equals, accountId.ToString()));
            }
            //to sort by name
            var sort = new SortCollection();
            sort.Add(SortKeys.DisplayOrder, SortDirections.Ascending);
            model.ProductTypeList = ProductViewModelMap.ToListItems(_productTypeClient.GetProductTypes(null, filters, sort).ProductTypes);

            model.ExpirationFrequencyList = ProductViewModelMap.ToListItems();

            return model;
        }

        public ProductReviewStateListViewModel GetReviewStates()
        {
            ProductReviewStateListViewModel model = new ProductReviewStateListViewModel();
            ProductReviewStateListModel reviewStateList = _productReviewStateClient.GetProductReviewStates(null, null, null);
            if (!Equals(reviewStateList, null) && !Equals(reviewStateList.ProductReviewStates, null) && reviewStateList.ProductReviewStates.Count > 0)
            {
                model = ProductViewModelMap.ToProductReviewListModel(reviewStateList);
            }
            return model;
        }

        #endregion

        #region Private Method

        ///// <summary>
        ///// To save image by Id in data folder and update image name in database table
        ///// </summary>
        ///// <param name="model">ProductViewModel model to get image and product details</param>      
        private void SaveImage(ProductViewModel model)
        {
            if (!Equals(model.ProductImage, null))
            {
                //to save image in data folder
                ImageHelper imageHelper = new ImageHelper();
                string validExtension = Convert.ToString(ConfigurationManager.AppSettings["ImageExtension"]);
                string imagePath = imageHelper.SaveImageToPhysicalPathById(model.PortalID, model.AccountID, model.ProductId, validExtension, model.ProductImage);
                if (!string.IsNullOrEmpty(imagePath) && !Equals(imagePath, MvcAdminConstants.FileUploadErrorCode))
                {
                    //to update image path in database by id
                    UpdateImageModel imageModel = new UpdateImageModel();
                    imageModel.Id = model.ProductId;
                    imageModel.EntityName = Convert.ToString(EntityName.Product);
                    imageModel.ImagePath = imagePath;
                    _productClient.UpdateImage(imageModel);
                }
            }
        }

        private int GetAccountIdFromSession()
        {
            int accountId = 0;
            var accountViewModel = GetFromSession<AccountViewModel>(MvcAdminConstants.UserAccountSessionKey);
            if (!Equals(accountViewModel, null))
            {
                accountId = accountViewModel.AccountId;
            }
            return accountId;
        }
        #endregion
    }
}