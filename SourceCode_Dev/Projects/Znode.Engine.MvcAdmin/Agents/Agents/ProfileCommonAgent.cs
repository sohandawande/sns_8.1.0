﻿using Znode.Engine.Api.Client;
using Znode.Engine.MvcAdmin.ViewModels;
using Znode.Engine.MvcAdmin.Maps;


namespace Znode.Engine.MvcAdmin.Agents
{
    public class ProfileCommonAgent :BaseAgent, IProfileCommonAgent
    {
        #region Private Variables
        private readonly IProfileCommonClient _profileCommonClient;
        #endregion

        #region Constructor
        public ProfileCommonAgent()
        {
            _profileCommonClient = GetClient<ProfileCommonClient>();
        }
        #endregion

        #region Public Methods
        public ProfileCommonViewModel GetProfileStoreAccess(string userName)
        {
            ProfileCommonViewModel model = new ProfileCommonViewModel();
            var profileDetails = _profileCommonClient.GetProfileStoreAccess(userName);
            if (!Equals(profileDetails, null) && !Equals(profileDetails.Profiles,null))
            {
                model = ProfileCommonViewModelMap.ToViewModel(profileDetails.Profiles);
            }
            return model;
        }
        #endregion
    }
}