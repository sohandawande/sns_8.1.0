﻿using ZNode.Libraries.ECommerce.Catalog;

namespace Znode.Engine.MvcAdmin.Agents
{
    public class UpdateLuceneReaderAgent : IUpdateLuceneReaderAgent
    {
        /// <summary>
        /// This method updates the lucene reader
        /// </summary>
        public void UpdateReader()
        {
            try
            {
                ZNodeSearchEngine search = new ZNodeSearchEngine();
                search.UpdateReader();
            }
            catch
            {
                throw;
            }
        }
    }
}