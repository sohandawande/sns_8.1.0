﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Linq;
using System.Web.Mvc;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;
using Znode.Engine.Api.Client.Expands;
using Znode.Libraries.Helpers.Constants;
using Znode.Engine.Exceptions;
using System.Data;
using System.IO;
using System.Text;
using System.Dynamic;
using Resources;
using ZNode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.MvcAdmin.Agents
{
    public class ImportExportAgent : BaseAgent, IImportExportAgent
    {
        #region Private Variables
        private readonly IImportExportClient _importExportClient;
        private readonly ICatalogsClient _catalogClient;
        private readonly IProfileCommonAgent _profileCommonAgent;
        private readonly IPortalCatalogsClient _portalCatalogClient;
        private readonly IAttributeTypesClient _attributeTypesClient;
        private readonly ICategoriesClient _categoryClient;
        private readonly IProductsClient _productsClient;
        private readonly IEnvironmentConfigClient _environmentConfigClient;
        private readonly ICountriesClient _countriesClient;
        #endregion

        #region Constructor
        public ImportExportAgent()
        {
            _importExportClient = GetClient<ImportExportClient>();
            _catalogClient = GetClient<CatalogsClient>();
            _profileCommonAgent = new ProfileCommonAgent();
            _portalCatalogClient = GetClient<PortalCatalogsClient>();
            _attributeTypesClient = GetClient<AttributeTypesClient>();
            _categoryClient = GetClient<CategoriesClient>();
            _productsClient = GetClient<ProductsClient>();
            _environmentConfigClient = GetClient<EnvironmentConfigClient>();
            _countriesClient = GetClient<CountriesClient>();
        }
        #endregion

        #region Public Methods

        public List<SelectListItem> GetFileTypes()
        {
            DownloadHelper downloadHelper = new DownloadHelper();
            List<SelectListItem> fileTypes = new List<SelectListItem>();
            fileTypes.Add(new SelectListItem { Value = Convert.ToInt32(FileTypes.Excel).ToString(), Text = downloadHelper.ToEnumString(FileTypes.Excel) });
            fileTypes.Add(new SelectListItem { Value = Convert.ToInt32(FileTypes.CSV).ToString(), Text = downloadHelper.ToEnumString(FileTypes.CSV) });
            return fileTypes;
        }

        public ExportViewModel GetExportData(ExportViewModel model)
        {
            ExportViewModel viewModel = new ExportViewModel();
            try
            {
                return ImportExportViewModelMap.ToViewModel(_importExportClient.GetExportData(ImportExportViewModelMap.ToModel(model)));
            }
            catch (ZnodeException ex)
            {
                switch (ex.ErrorCode)
                {
                    case ErrorCodes.ExportError:
                        return new ExportViewModel { HasError = true, ErrorMessage = ex.ErrorMessage };
                    default:
                        return viewModel;
                }
            }
        }

        public ExportViewModel GetDefaultExportDetails(string filter)
        {
            ExportViewModel viewModel = new ExportViewModel();
            if (!string.IsNullOrEmpty(filter))
            {
                viewModel.Type = filter;
                viewModel.FileTypes = GetFileTypes();
                switch (filter)
                {
                    case Constant.ExportProduct:
                        viewModel.Catalogs = GetAllCatalogs();
                        break;
                    case Constant.ExportAttribute:
                        viewModel = GetDefaultProductAttributes();
                        viewModel.Type = filter;
                        break;
                    case Constant.ExportFacet:
                        viewModel = GetDefaultFacets();
                        viewModel.Type = filter;
                        break;
                    case Constant.ExportSku:
                        viewModel.Catalogs = GetAllCatalogs();
                        break;
                    case Constant.ExportState:
                        viewModel.Countries = GetAllCountries();
                        break;
                    default:
                        break;
                }
            }
            return viewModel;
        }

        public List<SelectListItem> GetAttributeTypes(int catalogId)
        {
            List<SelectListItem> lstAttributeTypes = new List<SelectListItem>();
            if (catalogId > 0)
            {
                var attributes = _attributeTypesClient.GetAttributesByCatalogID(catalogId);
                if (!Equals(attributes, null) && !Equals(attributes.AttributeTypes, null) && attributes.AttributeTypes.Count > 0)
                {
                    lstAttributeTypes = ImportExportViewModelMap.GetSelectListItem(attributes.AttributeTypes, MvcAdminConstants.AttributeTypeId, MvcAdminConstants.Name, true);
                }
            }
            else
            {
                var attributes = _attributeTypesClient.GetAttributeTypes(null, null,null,null);
                if (!Equals(attributes, null) && !Equals(attributes.AttributeTypes, null) && attributes.AttributeTypes.Count > 0)
                {
                    lstAttributeTypes = ImportExportViewModelMap.GetSelectListItem(attributes.AttributeTypes, MvcAdminConstants.AttributeTypeId, MvcAdminConstants.Name, true);
                }
            }
            return lstAttributeTypes;
        }

        public List<SelectListItem> GetCategories(int catalogId)
        {
            List<SelectListItem> lstCategories = new List<SelectListItem>();
            if (catalogId > 0)
            {
                var categories = _categoryClient.GetCategoriesByCatalog(catalogId, null, null, null);
                if (!Equals(categories, null) && !Equals(categories.Categories, null) && categories.Categories.Count > 0)
                {
                    lstCategories = ImportExportViewModelMap.GetSelectListItem(categories.Categories, MvcAdminConstants.CategoryId, MvcAdminConstants.Name, true);
                }
                else
                {
                    lstCategories.Insert(0, new SelectListItem { Text = "All", Value = "0" });
                }
            }
            else
            {
                var categories = _categoryClient.GetCategories(null, null, null);
                if (!Equals(categories, null) && !Equals(categories.Categories, null) && categories.Categories.Count > 0)
                {
                    lstCategories = ImportExportViewModelMap.GetSelectListItem(categories.Categories, MvcAdminConstants.CategoryId, MvcAdminConstants.Name, true);
                }
                else
                {
                    lstCategories.Insert(0, new SelectListItem { Text = "All", Value = "0" });
                }
            }
            return lstCategories;
        }

        public List<ProductViewModel> GetProductDetailsByName(string searchText, int catalogId)
        {
            List<ProductViewModel> lstProducts = new List<ProductViewModel>();
            FilterCollection filter = new FilterCollection();
            if (!string.IsNullOrEmpty(searchText))
            {
                filter.Add(new FilterTuple(FilterKeys.ProductName, FilterOperators.Like, searchText.ToLower()));
            }
            if (catalogId > 0)
            {
                filter.Add(new FilterTuple(FilterKeys.CatalogId, FilterOperators.Equals, catalogId.ToString()));
            }

            var productList = _productsClient.GetProducts(new ExpandCollection { ExpandKeys.Catalogs, ExpandKeys.Categories }, filter, null);
            if (!Equals(productList, null) && !Equals(productList.Products, null) && productList.Products.Count > 0)
            {
                lstProducts = ProductViewModelMap.ToListViewModel(productList.Products, productList.TotalResults).Products;
            }
            return lstProducts;
        }

        public ImportViewModel ImportData(ImportViewModel model)
        {
            try
            {
                //ZNodeLogging.LogMessage("Step3 - ImportExportAgent In MVC Admin - Start");
                EnvironmentConfigModel environmentConfigModel = _environmentConfigClient.GetEnvironmentConfig();
                //ZNodeLogging.LogMessage("Step4 - ImportExportAgent In MVC Admin - GetEnvironmentConfig() is success and API client is called.");
                return ImportExportViewModelMap.ToImportViewModel(_importExportClient.ImportData(ImportExportViewModelMap.ToImportModel(model, environmentConfigModel)));
            }
            catch (ZnodeException ex)
            {
                ZNodeLogging.LogMessage("ImportExportAgent In MVC Admin. ZNodeException is returned from API Client. Exception="+ex.ToString());
                switch (ex.ErrorCode)
                {
                    case ErrorCodes.ImportError:
                        return new ImportViewModel { HasError = true, ErrorMessage = ex.ErrorMessage };
                    default:
                        return new ImportViewModel { HasError = true, ErrorMessage = ZnodeResources.ImportError };
                }
            }
            catch (Exception ex)
            {
                ZNodeLogging.LogMessage("ImportExportAgent In MVC Admin. General Exception is returned from API Client. Exception=" + ex.ToString());
                return new ImportViewModel { HasError = true, ErrorMessage = ZnodeResources.ImportError };
            }
        }

        public ImportViewModel PreviewImportData(ImportViewModel formModel)
        {
            ImportViewModel model = new ImportViewModel();
            try
            {
                EnvironmentConfigModel environmentConfigModel = _environmentConfigClient.GetEnvironmentConfig();
                var dataTable = ImportExportViewModelMap.DataTableToList(formModel.ImportFile, environmentConfigModel, formModel.Type);
                model.DataTableList = dataTable;
                model.WebGridColumn = ImportExportViewModelMap.MapListToViewModel(dataTable);

            }
            catch (Exception ex)
            {
                
                model.HasError = true;
                model.ErrorMessage = ZnodeResources.ImportExportErrorMessage;
            }
            return model;
        }

        public string GetImportTemplateFilePath(string type)
        {
            string filePath = string.Empty;
            if (!string.IsNullOrEmpty(type))
            {
                DownloadHelper downloadHelper = new DownloadHelper();
                ImportFileTypes fileType = (ImportFileTypes)Enum.Parse(typeof(ImportFileTypes), type, true);
                filePath = downloadHelper.ToEnumString(fileType);
            }
            return filePath;
        }

        public ImportViewModel GetImportDefaultData(string type)
        {
            ImportViewModel model = new ImportViewModel();
            model = ImportExportViewModelMap.ToImportViewModel(_importExportClient.GetImportDefaultData(type));
            return model;
        }
        #endregion

        #region Private Method

        private List<SelectListItem> GetAllCatalogs()
        {
            List<SelectListItem> lstItems = new List<SelectListItem>();
            CatalogListModel catalogList = _catalogClient.GetCatalogs(null, null);
            if (!Equals(catalogList, null) && !Equals(catalogList.Catalogs, null) && catalogList.Catalogs.Count > 0)
            {
                ProfileCommonViewModel profile = _profileCommonAgent.GetProfileStoreAccess(HttpContext.Current.User.Identity.Name);
                if (!Equals(profile, null) && !string.IsNullOrEmpty(profile.ProfileStoreAccess) && !Equals(profile.ProfileStoreAccess, "0"))
                {
                    var portalCatalogs = _portalCatalogClient.GetPortalCatalogs(null, null, null);
                    List<int> vendorProductIds = profile.ProfileStoreAccess.Split(',').Select(int.Parse).ToList();
                    var catalogIds = portalCatalogs.PortalCatalogs.ToList().FindAll(x => vendorProductIds.Contains(x.PortalId)).Select(y => y.CatalogId).ToList();
                    lstItems = ImportExportViewModelMap.ToListItems(catalogList.Catalogs).FindAll(z => catalogIds.Contains(Convert.ToInt32(z.Value)));
                }
                else
                {
                    lstItems = ImportExportViewModelMap.ToListItems(catalogList.Catalogs);
                    lstItems.Insert(0, new SelectListItem { Text = "All", Value = "0" });
                }
            }
            return lstItems;
        }

        private ExportViewModel GetDefaultProductAttributes()
        {
            ExportViewModel model = new ExportViewModel();
            model.FileTypes = GetFileTypes();
            model.Catalogs = GetAllCatalogs();
            model.AttributeTypes = GetAttributeTypes(0);
            return model;
        }

        private ExportViewModel GetDefaultFacets()
        {
            ExportViewModel model = new ExportViewModel();
            model.FileTypes = GetFileTypes();
            model.Catalogs = GetAllCatalogs();
            model.Categories = GetCategories(0);
            return model;
        }

        private List<SelectListItem> GetAllCountries()
        {
            List<SelectListItem> lstItems = new List<SelectListItem>();
            CountryListModel countriesList = _countriesClient.GetCountries(null, null, null);
            if (!Equals(countriesList, null) && !Equals(countriesList.Countries, null) && countriesList.Countries.Count > 0)
            {
                lstItems = ImportExportViewModelMap.GetSelectListItem(countriesList.Countries, MvcAdminConstants.CountryCode, MvcAdminConstants.CountryName, true);  
            }
            return lstItems;
        }
        #endregion

    }
}