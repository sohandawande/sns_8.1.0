﻿using Newtonsoft.Json;
using Resources;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;
using WebGrease.Css.Extensions;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.Models;
using Znode.Engine.MvcAdmin.ViewModels;
using Znode.Libraries.Helpers;

namespace Znode.Engine.MvcAdmin.Agents
{
    public class CheckoutAgent : BaseAgent, ICheckoutAgent
    {
        #region Private Variables
        private readonly IShippingAgent _shippingAgent;
        private readonly ICartAgent _cartAgent;
        private readonly IAccountAgent _accountAgent;
        private readonly IPaymentOptionsClient _paymentOptionsClient;
        private readonly IOrdersClient _orderClient;
        private readonly IAccountsClient _accountClient;
        private readonly IAddressesClient _addressClient;
        private readonly IPaymentAgent _paymentAgent;
        #endregion

        #region Constructor
        /// <summary>
        /// Checkout Agent Constructor
        /// </summary>
        public CheckoutAgent()
        {
            _shippingAgent = GetClient<ShippingAgent>();
            _cartAgent = new CartAgent();
            _accountAgent = new AccountAgent();
            _paymentOptionsClient = GetClient<PaymentOptionsClient>();
            _orderClient = GetClient<OrdersClient>();
            _accountClient = GetClient<AccountsClient>();
            _addressClient = GetClient<AddressesClient>();
            _paymentAgent = GetClient<PaymentAgent>();
        }
        #endregion

        #region Public Methods
        public void SaveAccounts(ShippingBillingAddressViewModel viewModel)
        {
            Expands = new ExpandCollection();
            Expands.Add(ExpandKeys.Addresses);
            Expands.Add(ExpandKeys.User);
            _addressClient.RefreshCache = true;
            var accounts = _accountClient.GetAccount(viewModel.AccountId, Expands);
            viewModel.EmailAddress = accounts.Email;

            var cart = GetFromSession<ShoppingCartModel>(MvcAdminConstants.CartModelSessionKey) ??
                      _cartAgent.GetCartFromCookie() ?? new ShoppingCartModel();
            SaveInSession(MvcAdminConstants.ShoppingCartUserAccountSessionKey, accounts);
        }

        public void SaveShippingAddess(AddressViewModel model)
        {
            var cart = GetFromSession<ShoppingCartModel>(MvcAdminConstants.CartModelSessionKey) ??
                       _cartAgent.GetCartFromCookie() ?? new ShoppingCartModel();
            cart.ShippingAddress = AddressViewModelMap.ToModel(model);
            SaveInSession(MvcAdminConstants.CartModelSessionKey, cart);
            SaveInSession(MvcAdminConstants.ShippingAddressKey, model);
        }

        public ShippingBillingAddressViewModel SaveBillingAddess(AddressViewModel model, bool validModel, bool useShippingAddress = false)
        {
            if (!useShippingAddress || string.IsNullOrEmpty(model.Email))
            {
                if (!validModel || string.IsNullOrEmpty(model.Email))
                {
                    return new ShippingBillingAddressViewModel()
                    {
                        BillingAddressModel = model,
                        ShippingAddressModel = GetShippingAddess(),
                        UseSameAsShippingAddress = useShippingAddress,
                        HasError = true
                    };
                }
            }

            var cart = GetFromSession<ShoppingCartModel>(MvcAdminConstants.CartModelSessionKey) ??
                       _cartAgent.GetCartFromCookie() ?? new ShoppingCartModel();
            cart.Payment = new PaymentModel();
            if (useShippingAddress)
            {
                var billAddress = GetShippingAddess();
                billAddress.Email = model.Email;
                billAddress.UseSameAsBillingAddress = true;
                billAddress.IsDefaultBilling = true;
                model = billAddress;
            }

            cart.Payment.BillingAddress = AddressViewModelMap.ToModel(model);
            SaveInSession(MvcAdminConstants.CartModelSessionKey, cart);
            SaveInSession(MvcAdminConstants.BillingAddressKey, model);

            return new ShippingBillingAddressViewModel() { HasError = false };
        }

        public AddressViewModel GetShippingAddess()
        {
            var model = GetFromSession<AddressViewModel>(MvcAdminConstants.ShippingAddressKey);
            if (Equals(model, null))
            {
                var accountModel = _accountAgent.GetAccountViewModel();
                if (!Equals(accountModel, null))
                    model = accountModel.Addresses.FirstOrDefault(x => x.IsDefaultShipping);
            }

            return model;
        }

        public AddressViewModel GetBillingAddessFromSession()
        {
            var model = GetFromSession<AddressViewModel>(MvcAdminConstants.BillingAddressKey);
            if (Equals(model, null))
            {
                var accountModel = _accountAgent.GetAccountViewModel();
                if (!Equals(accountModel, null))
                {
                    if (accountModel.Addresses.Any())
                    {
                        model = accountModel.Addresses.FirstOrDefault(x => x.IsDefaultBilling);
                    }
                    else
                    {
                        model = new AddressViewModel();
                    }
                    model.Email = accountModel.EmailAddress;
                }
            }

            return model;
        }

        public ShippingBillingAddressViewModel GetBillingAddress()
        {
            var viewModel = new ShippingBillingAddressViewModel()
            {
                BillingAddressModel = GetBillingAddessFromSession() ?? new AddressViewModel(),
                ShippingAddressModel = GetShippingAddess(),
            };

            return viewModel;
        }

        public AddressViewModel GetAddressByAddressID(AddressViewModel address)
        {
            AccountViewModel accountViewModel = GetFromSession<AccountViewModel>(MvcAdminConstants.AccountKey);

            if (!Equals(accountViewModel, null) && accountViewModel.Addresses.Any())
            {
                var addressViewModel = accountViewModel.Addresses.FirstOrDefault(x => Equals(x.AddressId, address.AddressId));

                if (!Equals(addressViewModel, null))
                {
                    addressViewModel.Email = accountViewModel.EmailAddress;
                }

                return addressViewModel;
            }

            return new AddressViewModel();
        }

        public OrderViewModel SubmitOrder(PaymentOptionViewModel paymentInformationViewModel, int shippingId, string purchaseOrderNumber, string additionalInfo = "", string token = "", int? portalId = null)
        {
            //shopping cart
            ShoppingCartModel shoppingCart = GetFromSession<ShoppingCartModel>(MvcAdminConstants.CartModelSessionKey);
            if (Equals(shoppingCart, null))
            {
                ClearShoppingCart();
                return new OrderViewModel() { HasError = true, ErrorMessage = ZnodeResources.ErrorNoItemsShoppingCart };
            }

            //add token generated by payment app
            if (!token.ToLower().Equals("null"))
            {
                shoppingCart.Token = token;
            }

            //user account
            AccountModel account = GetFromSession<AccountModel>(MvcAdminConstants.ShoppingCartUserAccountSessionKey);
            if (Equals(account, null))
            {
                ClearShoppingCart();
                return new OrderViewModel() { HasError = true, ErrorMessage = ZnodeResources.ErrorCustomerNotExistForOrder };
            }
            if (Equals(account.AccountId, 0))
            {
                try
                {
                    account = _accountClient.CreateAccount(account);
                }
                catch (ZnodeException ex)
                {
                    ClearShoppingCart();
                    return new OrderViewModel() { HasError = true, ErrorMessage = ex.ErrorMessage };
                }
            }
            shoppingCart.Account = account;

            if (!Equals(portalId, null))
            {
                shoppingCart.PortalId = portalId;
            }
            //ShippingOption 
            if (shippingId > 0)
            {
                shoppingCart.Shipping.ShippingOptionId = shippingId;
            }

            AddressViewModel _shippingAddress = GetShippingAddess();

            if (Equals(_shippingAddress, null))
            {
                ClearShoppingCart();
                return new OrderViewModel() { HasError = true, ErrorMessage = ZnodeResources.ErrorCustomerShippingAddress };
            }

            if (GetBillingAddessFromSession() != null)
            {
                account.Email = GetBillingAddessFromSession().Email;
            }

            
            if (!IsValidAddressForCheckout(_shippingAddress))
            {
                ClearShoppingCart();
                return new OrderViewModel() { HasError = true, ErrorMessage = ZnodeResources.ErrorCustomerShippingAddress };
            }

            PaymentModel paymentInformationModel = PaymentViewModelMap.ToViewModel(paymentInformationViewModel);
            if (!Equals(paymentInformationModel, null) && !Equals(paymentInformationModel.PaymentOption, null) && Equals(paymentInformationModel.PaymentOption.PaymentTypeId, (int)EnumPaymentType.PaypalExpressCheckout))
            {
                string baseUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + HttpContext.Current.Request.ApplicationPath;

                shoppingCart.ReturnUrl = baseUrl + MvcAdminConstants.RouteCheckOutPaypal + "?PaymentOptionId=" +
                                         paymentInformationModel.PaymentOption.PaymentOptionId;
                shoppingCart.CancelUrl = baseUrl + MvcAdminConstants.RouteShoppingCart;
            }

            if (!string.IsNullOrEmpty(additionalInfo))
            {
                shoppingCart.AdditionalInstructions = additionalInfo;
            }

            shoppingCart.Payment = paymentInformationModel;
            shoppingCart.Payment.BillingAddress =
                AddressViewModelMap.ToModel(GetBillingAddessFromSession() ?? _shippingAddress);
            shoppingCart.ShippingAddress = AddressViewModelMap.ToModel(_shippingAddress);

            ShoppingCartModel checkout = CartViewModelMap.AddAccountDetailsToShoppingCartModel(shoppingCart.Account, shoppingCart, purchaseOrderNumber);

            OrderModel order = null;

            try
            {
                _orderClient.AccountId = account.AccountId;
                order = _orderClient.CreateOrder(checkout);

                if (order.IsOffsitePayment)
                {
                    HttpContext.Current.Response.Redirect(order.ReceiptHtml);
                    return new OrderViewModel() { HasError = true };
                }
            }
            catch (ZnodeException exception)
            {
                return new OrderViewModel() { HasError = true, ErrorMessage = exception.ErrorMessage };
            }

            // reset the cart
            RemoveCookie(MvcAdminConstants.CartCookieKey);
            SaveInSession(MvcAdminConstants.CartModelSessionKey, new ShoppingCartModel());
            ClearShoppingCart();
            return new OrderViewModel { EmailId = shoppingCart.Account.Email, OrderId = order.OrderId, ReceiptHtml = order.ReceiptHtml };
        }

        public CartViewModel ApplyGiftCard(string giftCard)
        {
            var shoppingCart = GetFromSession<ShoppingCartModel>(MvcAdminConstants.CartModelSessionKey);
            shoppingCart.GiftCardNumber = giftCard;

            var cartModel = _cartAgent.Calculate(shoppingCart);

            if (cartModel.GiftCardApplied)
            {
                cartModel.SuccessMessage = ZnodeResources.ValidGiftCard;
            }
            else
            {
                cartModel.HasError = true;
                cartModel.ErrorMessage = ZnodeResources.ErrorGiftCard;
            }
            return cartModel;
        }

        public CartViewModel GetShippingChargesById(int shippingId)
        {
            ShippingOptionViewModel selectedShippingOption = _shippingAgent.GetShippingOption(shippingId);
            ShoppingCartModel cart = GetFromSession<ShoppingCartModel>(MvcAdminConstants.CartModelSessionKey);
            cart.Shipping = new ShippingModel
            {
                ShippingOptionId = selectedShippingOption.ShippingOptionId,
                ShippingName = selectedShippingOption.Description
            };

            cart.ShippingAddress = AddressViewModelMap.ToModel(GetShippingAddess());
            cart.ShoppingCartItems.ForEach(item => item.InsufficientQuantity = false);
            CartViewModel cartViewModel = _cartAgent.Calculate(cart);
            cart = GetFromSession<ShoppingCartModel>(MvcAdminConstants.CartModelSessionKey);
            SaveInSession(MvcAdminConstants.CartModelSessionKey, cart);
            return cartViewModel;
        }

        public string ProcessPayPalCheckout(int paymentOptionId, string returnUrl, string cancelUrl)
        {

            ShoppingCartModel shoppingCart = GetFromSession<ShoppingCartModel>(MvcAdminConstants.CartModelSessionKey);
            if (Equals(shoppingCart, null))
            {
                return ZnodeResources.ErrorNoItemsShoppingCart;
            }

            AccountModel account = GetFromSession<AccountModel>(MvcAdminConstants.ShoppingCartUserAccountSessionKey);
            if (Equals(account, null))
            {
                return ZnodeResources.ErrorCustomerNotExistForOrder;
            }

            AddressViewModel _shippingAddress = GetShippingAddess();
            if (Equals(_shippingAddress, null))
            {
                return ZnodeResources.ErrorCustomerShippingAddress;
            }

            if (!IsValidAddressForCheckout(_shippingAddress))
            {
                return ZnodeResources.ErrorCustomerShippingAddress;
            }

            AddressViewModel addressFromSession = GetBillingAddessFromSession();

            if (Equals(addressFromSession, null))
            {
                addressFromSession = _shippingAddress;
            }

            PayPalPaymentModel model = PaymentViewModelMap.ToModel(shoppingCart, addressFromSession);
            if (!Equals(model, null))
            {
                model.ReturnUrl = returnUrl;
                model.CancelUrl = cancelUrl;
                model.PaymentSettingId = paymentOptionId;

                return _paymentAgent.HttpPayPalPostRequest(model);
            }
            else
            {
                return string.Empty;
            }
        }

        public List<AddressViewModel> GetAllAddesses()
        {
            List<AddressViewModel> addressList = new List<AddressViewModel>();
            var model = GetFromSession<AddressViewModel>(MvcAdminConstants.ShippingAddressKey);

            if (!Equals(model, null))
                addressList.Add(model);

            var accountModel = _accountAgent.GetAccountViewModel();
            if (!Equals(accountModel, null))
            {
                foreach (var adr in accountModel.Addresses)
                {
                    if (!addressList.Exists(element => element.AddressId.Equals(adr.AddressId)))
                    {
                        addressList.Add(adr);
                    }

                }
            }

            return addressList;
        }

        public List<CartItemViewModel> GetMultipleShipingViewModel()
        {
            var cartViewModel = _cartAgent.GetCart();
            var shoppingCart = GetFromSession<ShoppingCartModel>(MvcAdminConstants.CartModelSessionKey);
            List<OrderShipmentModel> _MultipleShipToAddress = new List<OrderShipmentModel>();
            shoppingCart.ShoppingCartItems.ForEach(cartitems =>
            {
                _MultipleShipToAddress = cartitems.MultipleShipToAddress;
            });
            var shippingAddress = GetAllAddesses();

            List<CartItemViewModel> cartitemList = new List<CartItemViewModel>();

            cartViewModel.Items.ToList().ForEach(item =>
            {
                int count = item.Quantity;
                for (int index = 0; index < count; index++)
                {
                    CartItemViewModel objCartItem = new CartItemViewModel();
                    item.Quantity = 1;
                    objCartItem = item.Clone() as CartItemViewModel;
                    objCartItem.Addresses = shippingAddress;
                    cartitemList.Add(objCartItem);
                }
            });

            return cartitemList;
        }

        public void ClearShoppingCart()
        {
            RemoveInSession(MvcAdminConstants.CartModelSessionKey);
            RemoveInSession(MvcAdminConstants.ShoppingCartUserAccountSessionKey);
            RemoveInSession(MvcAdminConstants.BillingAddressKey);
            RemoveInSession(MvcAdminConstants.ShippingAddressKey);
        }

        public OrderViewModel SubmitCustomOrder(string token, int paymentOptionId, int shippingId, string additionalNotes)
        {
            PaymentOptionViewModel paymentOption = paymentOptionId > 0 ? _paymentAgent.GetPaymentOption(paymentOptionId) : null;
            return SubmitOrder(paymentOption, shippingId, string.Empty, additionalNotes, token);
        }

        public string CaptureVoidRefundPayment(string token, int orderId, Paymentoptions actionType, decimal? amount = null)
        {
            string status = string.Empty;
            string paymentStatusOptions = string.Empty;
            string paymentAppUrl = ConfigurationManager.AppSettings["PaymentApplicationUrl"].ToString();
            if (!string.IsNullOrEmpty(paymentAppUrl) && !paymentAppUrl.Trim().EndsWith("/"))
            {
                paymentAppUrl = string.Format("{0}/", paymentAppUrl);
            }
            string sucessMessage = string.Empty;
            string failureMessage = ZnodeResources.PaymentAppUrlError;

            if (!string.IsNullOrEmpty(paymentAppUrl))
            {
                switch (actionType)
                {
                    case Paymentoptions.PaymentCapture:
                        paymentAppUrl = string.Concat(paymentAppUrl, "payment/capture?token=", token);
                        paymentStatusOptions = "CC_CAPTURED";
                        sucessMessage = ZnodeResources.PaymentAppCaptureSuccessMessage;
                        failureMessage = ZnodeResources.PaymentAppCaptureErrorMessage;
                        break;
                    case Paymentoptions.PaymentRefund:
                        string paymentAppQueryString = string.Format("payment/Refund?token={0}&amount={1}", token, amount);
                        paymentAppUrl = string.Concat(paymentAppUrl, paymentAppQueryString);
                        paymentStatusOptions = "CC_REFUNDED";
                        sucessMessage = ZnodeResources.PaymentAppRefundSuccessMessage;
                        failureMessage = ZnodeResources.PaymentAppRefundErrorMessage;
                        break;
                    case Paymentoptions.PaymentVoid:
                        paymentAppUrl = string.Concat(paymentAppUrl, "payment/void?token=", token);
                        paymentStatusOptions = "CC_VOIDED";
                        sucessMessage = ZnodeResources.PaymentAppVoidSuccessMessage;
                        failureMessage = ZnodeResources.PaymentAppVoidErrorMessage;
                        break;
                }

                using (WebClient client = new WebClient())
                {
                    status = client.DownloadString(paymentAppUrl);
                }

                if (status.ToLower().Contains("success"))
                {
                    bool sucess = _orderClient.UpdateOrderPaymentStatus(orderId, paymentStatusOptions);
                    status = sucess ? sucessMessage : failureMessage;
                }
                else
                {
                    status = failureMessage;
                }
            }
            return status;
        }

        public bool IsValidAddressForCheckout(AddressViewModel shippingAddress)
        {
            // Validate the customer shipping address if address validation enabled.
            if (!Equals(PortalAgent.CurrentPortal.EnableAddressValidation, null) && (PortalAgent.CurrentPortal.EnableAddressValidation.Value))
            {
                bool isAddressValid = _accountAgent.IsAddressValid(shippingAddress);
                // Do not allow the customer to go to next page if valid shipping address required is enabled. 
                if (!isAddressValid && !Equals(PortalAgent.CurrentPortal.RequireValidatedAddress, null) && (PortalAgent.CurrentPortal.RequireValidatedAddress.Value))
                {
                    return isAddressValid;
                }

                return isAddressValid;
            }
            return true;
        }


        public string CallTwoCoPaymentMethod(int? paymentOptionId, string token)
        {
            ShoppingCartModel shoppingCart = GetFromSession<ShoppingCartModel>(MvcAdminConstants.CartModelSessionKey);
            if (Equals(shoppingCart, null))
            {
                return ZnodeResources.ErrorNoItemsShoppingCart;
            }

            AccountModel account = GetFromSession<AccountModel>(MvcAdminConstants.ShoppingCartUserAccountSessionKey);
            if (Equals(account, null))
            {
                return ZnodeResources.ErrorCustomerNotExistForOrder;
            }

            AddressViewModel _shippingAddress = GetShippingAddess();
            if (Equals(_shippingAddress, null))
            {
                return ZnodeResources.ErrorCustomerShippingAddress;
            }

            if (!IsValidAddressForCheckout(_shippingAddress))
            {
                return ZnodeResources.ErrorCustomerShippingAddress;
            }

            AddressViewModel addressFromSession = GetBillingAddessFromSession();

            if (Equals(addressFromSession, null))
            {
                addressFromSession = _shippingAddress;
            }

            PayPalPaymentModel model = PaymentViewModelMap.ToModel(shoppingCart, addressFromSession);
            if (!Equals(model, null))
            {
                string baseUrl = GetDomainUrl();

                model.ReturnUrl = string.Concat(baseUrl, "/orders/custom?PaymentOptionId=", paymentOptionId.Value, "&shippingId=", shoppingCart.Shipping.ShippingOptionId);
                model.CancelUrl = string.Concat(baseUrl, "/orders/create");
                model.PaymentSettingId = paymentOptionId.Value;
                model.TwoCOToken = token;

                return _paymentAgent.ProcessPayNow(model);
            }
            else
            {
                return string.Empty;
            }
        }

        public string ProcessCreditCardPayment(int paymentSettingId, string customerProfileId, string customerPaymentId)
        {
            ShoppingCartModel shoppingCart = GetFromSession<ShoppingCartModel>(MvcAdminConstants.CartModelSessionKey);
            if (Equals(shoppingCart, null))
            {
                return ZnodeResources.ErrorNoItemsShoppingCart;
            }

            AccountModel account = GetFromSession<AccountModel>(MvcAdminConstants.ShoppingCartUserAccountSessionKey);
            if (Equals(account, null))
            {
                return ZnodeResources.ErrorCustomerNotExistForOrder;
            }

            AddressViewModel _shippingAddress = GetShippingAddess();
            if (Equals(_shippingAddress, null))
            {
                return ZnodeResources.ErrorCustomerShippingAddress;
            }

            if (!IsValidAddressForCheckout(_shippingAddress))
            {
                return ZnodeResources.ErrorCustomerShippingAddress;
            }

            AddressViewModel addressFromSession = GetBillingAddessFromSession();

            if (Equals(addressFromSession, null))
            {
                addressFromSession = _shippingAddress;
            }

            PayPalPaymentModel model = PaymentViewModelMap.ToModel(shoppingCart, addressFromSession);
            if (!Equals(model, null))
            {
                string baseUrl = GetDomainUrl();

                model.ReturnUrl = string.Concat(baseUrl, "orders/custom?PaymentOptionId=", paymentSettingId, "&shippingId=", shoppingCart.Shipping.ShippingOptionId);
                model.CancelUrl = string.Concat(baseUrl, "orders/create");
                model.PaymentSettingId = paymentSettingId;
                model.CustomerProfileId = customerProfileId;
                model.CustomerPaymentProfileId = customerPaymentId;
                return _paymentAgent.ProcessPayNow(model);
            }
            else
            {
                return string.Empty;
            }
        }

        #endregion

        #region Private
        /// <summary>
        /// To Get the Domain Url
        /// </summary>
        /// <returns>Return the Domain Url.</returns>
        private string GetDomainUrl()
        {
            string baseDomainUrl = (!string.IsNullOrEmpty(HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority))) ? HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) : string.Empty;
            string url = HttpContext.Current.Request.Url.AbsoluteUri;
            string areaName = string.Empty;
            if (url.ToLower().Contains("franchise"))
            {
                areaName = string.Concat(Convert.ToString(UserTypes.FranchiseAdmin).ToLower(), "/franchise");
            }
            return (string.IsNullOrEmpty(areaName)) ? string.Concat(baseDomainUrl, "/") : string.Format("{0}/{1}", baseDomainUrl, areaName);
        }
        #endregion
    }
}