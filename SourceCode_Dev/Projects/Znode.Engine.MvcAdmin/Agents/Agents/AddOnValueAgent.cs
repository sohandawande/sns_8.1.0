﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;
using Znode.Libraries.Helpers;

namespace Znode.Engine.MvcAdmin.Agents
{
    /// <summary>
    /// AddOn Value Agent
    /// </summary>
    public class AddOnValueAgent : BaseAgent, IAddOnValueAgent
    {
        #region Private Variables

        private string NULL = "null";

        private readonly IAddOnValueClient _addOnValueClient;
        private readonly ISuppliersClient _suppliersClient;
        private readonly ITaxClassesClient _taxClassesClient;
        private readonly IShippingRuleTypesClient _shippingRuleTypesClient;
        #endregion

        #region Constructor
        public AddOnValueAgent()
        {
            _addOnValueClient = GetClient<AddOnValueClient>();
            _suppliersClient = GetClient<SuppliersClient>();
            _taxClassesClient = GetClient<TaxClassesClient>();
            _shippingRuleTypesClient = GetClient<ShippingRuleTypesClient>();


        }
        #endregion

        #region Public Methods

        public bool Delete(int addOnValueId)
        {
            return _addOnValueClient.DeleteAddOnValue(addOnValueId);
        }

        public AddOnValueViewModel GetAddOnValue(int addOnValueId)
        {
            _addOnValueClient.RefreshCache = true;
            AddOnValueModel addOnValueModel = _addOnValueClient.GetAddOnValue(addOnValueId);
            return (!Equals(addOnValueModel, null)) ? AddOnValueViewModelMap.ToViewModel(addOnValueModel) : null;
        }

        public AddOnValueListViewModel GetAddOnValueByAddOnId(int addOnId, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            _addOnValueClient.RefreshCache = true;
            if (Equals(filters, null))
            {
                filters = new FilterCollection();
            }
            filters.Add(new FilterTuple(FilterKeys.AddOnId, FilterOperators.Equals, addOnId.ToString()));
            AddOnValueListModel modelList = _addOnValueClient.GetAddOnValueByAddOnId(null, filters, sortCollection, pageIndex, recordPerPage);
            return Equals(modelList, null) ? new AddOnValueListViewModel() : AddOnValueViewModelMap.ToListViewModel(modelList, pageIndex, recordPerPage, modelList.TotalPages, modelList.TotalResults);
        }

        public bool Create(AddOnValueViewModel model)
        {
            AddOnValueModel addOnValueModel = _addOnValueClient.CreateAddOnValue(AddOnValueViewModelMap.ToModel(model));
            return (!Equals(addOnValueModel, null) && (addOnValueModel.AddOnValueId > 0));
        }

        public bool Update(AddOnValueViewModel model)
        {
            AddOnValueModel addOnValue = _addOnValueClient.UpdateAddOnValue(model.AddOnValueId, AddOnValueViewModelMap.ToModel(model));
            return (!Equals(addOnValue, null) && (addOnValue.AddOnValueId > 0));
        }

        public AddOnValueViewModel BindDropdownValues(AddOnValueViewModel model)
        {
            FilterCollection filters = new FilterCollection();
            filters.Add(new FilterTuple(FilterKeys.IsActive, FilterOperators.Equals, true.ToString()));
            model.SupplierList = AddOnValueViewModelMap.ToListItems(_suppliersClient.GetSuppliers(null, filters, null).Suppliers);
            model.SupplierList.Insert(0, new SelectListItem() { Value = "0", Text = ZnodeResources.DdlSupplier });
            if (Equals(model.UserTypes, UserTypes.FranchiseAdmin))
            {
                filters.Add(model.Filters.FirstOrDefault());
                model.TaxClassList = AddOnValueViewModelMap.ToListItems(_taxClassesClient.GetTaxClasses(filters, null).TaxClasses);
            }
            else
            {
                filters.Add(FilterKeys.PortalId, FilterOperators.Equals, NULL);
                model.TaxClassList = AddOnValueViewModelMap.ToListItems(_taxClassesClient.GetTaxClasses(filters, null).TaxClasses);
            }
            model.ShippingRuleTypeList = AddOnValueViewModelMap.ToListItems(_shippingRuleTypesClient.GetShippingRuleTypes(null, null).ShippingRuleTypes);
            return model;
        }

        public AddOnValueViewModel SetRecurringBillingValues(AddOnValueViewModel model)
        {
            if (model.EnableRecurringBilling)
            {
                model.RecurringBillingFrequency = "1";
                model.RecurringBillingTotalCycles = 0;
            }
            else
            {
                model.RecurringBillingInstallmentInd = false;
                model.BillingPeriod = null;
            }
            return model;
        }
        #endregion
    }
}