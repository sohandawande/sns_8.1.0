﻿using Znode.Engine.Api.Client;
using Znode.Engine.Exceptions;
using Resources;

namespace Znode.Engine.MvcAdmin.Agents
{
    public class RotateKeyAgent : BaseAgent, IRotateKeyAgent
    {
        #region Private Variables
        private readonly IRotateKeyClient _rotateKeyClient; 
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor for RoateKeyAgent
        /// </summary>
        public RotateKeyAgent()
        {
            _rotateKeyClient = GetClient<RotateKeyClient>();
        } 
        #endregion

        #region Public Method
        public bool GenerateRotateKey(out string message)
        {
            try
            {
                message = string.Empty;
                return _rotateKeyClient.GenerateRotateKey();
            }
            catch (ZnodeException ex)
            {
                message = ZnodeResources.RotateKeyErrorMessage;
                return false;
            }                       
        } 
        #endregion
    }
}