﻿using Znode.Engine.Api.Client;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public class CategoryNodeAgent : BaseAgent, ICategoryNodeAgent
    {
        #region Private Variables
        private readonly ICategoryNodesClient _categoryNodeClient;
        #endregion

        #region Constructor
        public CategoryNodeAgent()
        {
            _categoryNodeClient = GetClient<CategoryNodesClient>();
        }
        #endregion

        #region Public Methods
        public CategoryNodeViewModel EditCategoryNode(int categoryNodeId)
        {
            _categoryNodeClient.RefreshCache = true;
            var model = _categoryNodeClient.GetCategoryNode(categoryNodeId);
            return CategoryNodeViewModelMap.ToViewModel(model);
        }

        public CategoryNodeViewModel UpdateCategoryNode(int categoryNodeId, CategoryNodeViewModel model)
        {
            var updatedModel = _categoryNodeClient.UpdateCategoryNode(categoryNodeId, CategoryNodeViewModelMap.ToModel(model));
            if (!Equals(updatedModel, null))
            {
                if (!string.IsNullOrEmpty(updatedModel.ErrorMessage))
                {
                    CategoryNodeModel categoryNodeModel = new CategoryNodeModel();
                    categoryNodeModel.ErrorMessage = updatedModel.ErrorMessage;
                    CategoryNodeViewModel updatedErrorModel = CategoryNodeViewModelMap.ToViewModel(categoryNodeModel);
                    return updatedErrorModel;
                }
                else
                {
                    return CategoryNodeViewModelMap.ToViewModel(updatedModel.CategoryNode);
                }
            }
            return null;
        }

        public ZnodeDeleteViewModel DeleteCategoryNode(int categoryNodeId)
        {
            ZnodeDeleteModel model = _categoryNodeClient.DeleteAssociatedCategoryNode(categoryNodeId);
            return (!Equals(model, null) ? ZnodeDeleteViewModelMap.ToViewModel(model) : null);
        }

        public bool AssociateCategory(CategoryNodeViewModel model)
        {
            CategoryNodeModel returnModel = _categoryNodeClient.CreateCategoryNode(CategoryNodeViewModelMap.ToModel(model));
            return returnModel.IsCategoryAssociated;
        } 
        #endregion

    }
}