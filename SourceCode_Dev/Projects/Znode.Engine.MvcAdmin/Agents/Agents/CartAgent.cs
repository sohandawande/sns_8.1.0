﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.MvcAdmin.Agents
{
    public class CartAgent : BaseAgent, ICartAgent
    {
        #region Private Variables
        private readonly IShoppingCartsClient _shoppingCartsClient;
        private readonly IPromotionsClient _promotionsClient;
        private readonly ISkuAgent _skuAgent;
        private readonly IProductAgent _productAgent;
        private readonly IAccountAgent _accountAgent;
        #endregion

        #region Constructor
        /// <summary>
        /// Cart Agent Constructor
        /// </summary>
        public CartAgent()
        {
            _shoppingCartsClient = GetClient<ShoppingCartsClient>();
            _promotionsClient = new PromotionsClient();
            _skuAgent = new SkuAgent();
            _productAgent = new ProductAgent();
            _accountAgent = new AccountAgent();
        }
        #endregion

        #region Public Methods
        public ProductViewModel WishlistCheckInventory(CartItemViewModel cartItem)
        {
            ProductViewModel product = new ProductViewModel();

            if (!string.IsNullOrEmpty(cartItem.ProductId.ToString()))
            {
                string selectedSku = string.Empty;
                string imagePath = string.Empty;
                string imageMediumPath = string.Empty;
                decimal productPrice;
                decimal finalProductPrice;

                if (!Equals(cartItem, null) && !Equals(cartItem.AttributeIds, null))
                {
                    // Get the product
                    product = _productAgent.GetAttributes(cartItem.ProductId, Convert.ToInt32(cartItem.Attributes[0]), cartItem.Attributes, cartItem.Quantity, out productPrice, out selectedSku, out imagePath, out imageMediumPath);

                    if (!Equals(product.AddOns, null) && !string.IsNullOrEmpty(cartItem.AddOnValueIds))
                    {
                        product.AddOns = _productAgent.GetAddOnDetails(product, cartItem.AddOnValueIds, cartItem.Quantity,
                                                                       productPrice, out finalProductPrice);
                    }
                    else
                    {
                        finalProductPrice = productPrice;
                    }
                }
                else
                {
                   product = _productAgent.GetProductDetails(cartItem.ProductId);
                }
            }
            return product;
        }

        public CartViewModel Create(CartItemViewModel cartItem)
        {
            var cart = GetFromSession<ShoppingCartModel>(MvcAdminConstants.CartModelSessionKey) ??
                       GetCartFromCookie() ?? new ShoppingCartModel();

            //PRFT Custom Code:start
            cartItem.ERPUnitPrice = cartItem.UnitPrice;
            cartItem.ERPExtendedPrice = cartItem.ExtendedPrice;
            //PRFT custom code:End
            UpdateAccountInfo(cart);
            cart.PortalId = cartItem.PortalId;
            cart.ShoppingCartItems.Add(CartItemViewModelMap.ToModel(cartItem));
            HttpContext.Current.Session[ZNodeSessionKeyType.ShoppingCart.ToString()] = cart.ShoppingCartItems;
            if (!Equals(cart.Account, null) && cart.Account.AccountId > 0)
            {
                _shoppingCartsClient.AccountId = cart.Account.AccountId;
            }
            var shoppingCartModel = _shoppingCartsClient.CreateCart(cart);

            if (!Equals(shoppingCartModel, null))
            {
                AddCouponsToShoppingCart(cart, shoppingCartModel);
                shoppingCartModel.GiftCardNumber = cart.GiftCardNumber;
                shoppingCartModel.GiftCardValid = cart.GiftCardValid;
                shoppingCartModel.GiftCardMessage = cart.GiftCardMessage;
                shoppingCartModel.GiftCardApplied = cart.GiftCardApplied;
                shoppingCartModel.GiftCardAmount = cart.GiftCardAmount;
                shoppingCartModel.GiftCardBalance = cart.GiftCardBalance;
                shoppingCartModel.PortalId = cartItem.PortalId;
                foreach (var cartItems in shoppingCartModel.ShoppingCartItems)
                {
                    foreach (var shoppingCartItems in cart.ShoppingCartItems)
                    {
                        if (!Equals(cartItems, null) && Equals(cartItems.ProductId, shoppingCartItems.ProductId) && Equals(cartItems.CartDescription, shoppingCartItems.CartDescription))
                        {
                            cartItems.AddOnValuesCustomText = shoppingCartItems.AddOnValuesCustomText;
                        }
                    }
                }
                if (!Equals(cart.Shipping, null))
                {
                    shoppingCartModel.ShippingCost = cart.ShippingCost;
                }
                SaveInSession(MvcAdminConstants.CartModelSessionKey, shoppingCartModel);

                // if persistent cart disabled, we need not call below method, need to check with portal record.
                if ((Equals(shoppingCartModel.Account, null) || Equals(shoppingCartModel.Account.AccountId, 0)) && PortalAgent.CurrentPortal.PersistentCartEnabled)
                {
                    SaveInCookie(MvcAdminConstants.CartCookieKey, shoppingCartModel.CookieId.ToString());
                }

                return CartViewModelMap.ToViewModel(shoppingCartModel);
            }

            return new CartViewModel() { HasError = true, ErrorMessage = string.Empty };
        }

        public CartViewModel GetCart()
        {
            ShoppingCartModel cart = GetFromSession<ShoppingCartModel>(MvcAdminConstants.CartModelSessionKey) ??
                       GetCartFromCookie();

            if (Equals(cart, null))
            {
                return new CartViewModel()
                    {
                        HasError = true
                    };
            }

            //PRFT Custom Code : Start
            for (int i = 0; i < cart.ShoppingCartItems.Count(); i++)
            {
                cart.ShoppingCartItems[i].ERPUnitPrice = cart.ShoppingCartItems[i].UnitPrice;
            }
            //PRFT Custom Code : End
            cart.ShippingAddress = AddressViewModelMap.ToModel(this.GetShippingAddess());

            CartViewModel cartModel = Calculate(cart);

            if (GetFromSession<bool>(MvcAdminConstants.CartCookieMerged))
            {
                cartModel.ErrorMessage = string.Empty;
                cartModel.HasError = true;
                SaveInSession(MvcAdminConstants.CartCookieMerged, false);
            }

            if (!Equals(cart.Shipping, null))
            {
                cartModel.Shipping = cart.Shipping;
                cartModel.ShippingServiceCodeId = cart.Shipping.ShippingOptionId;
            }

            return cartModel;
        }

        public int GetCartCount()
        {
            var cart = GetFromSession<ShoppingCartModel>(MvcAdminConstants.CartModelSessionKey) ??
                       GetCartFromCookie();

            if (!Equals(cart, null)) return cart.ShoppingCartItems.Count;

            return 0;
        }

        public CartViewModel Calculate(ShoppingCartModel shoppingCartModel)
        {
            ShoppingCartModel calculatedModel = shoppingCartModel;
            _shoppingCartsClient.RefreshCache = true;
            UpdateAccountInfo(shoppingCartModel);
            if (!shoppingCartModel.ShoppingCartItems.Any(item => item.InsufficientQuantity))
            {
                if (!Equals(shoppingCartModel.Account, null) && shoppingCartModel.Account.AccountId > 0)
                {
                    _shoppingCartsClient.AccountId = shoppingCartModel.Account.AccountId;
                }
                calculatedModel = _shoppingCartsClient.Calculate(shoppingCartModel);
            }

            if (!Equals(calculatedModel, null) && calculatedModel.ShoppingCartItems.Count > 0)
            {
                string ids = string.Join(",", calculatedModel.ShoppingCartItems.Select(x => x.ProductId.ToString()));
                IEnumerable<ProductViewModel> productsViewModel = _productAgent.GetProductByIds(ids);
                CartViewModel cartViewModel = CartViewModelMap.ToViewModel(calculatedModel);

                productsViewModel.ToList().ForEach(productModel =>
                {
                    IEnumerable<CartItemViewModel> items = cartViewModel.Items.Where(itemModel => Equals(itemModel.ProductId, productModel.ProductId));
                    items.ToList().ForEach(itemModel => itemModel.Product = productModel);
                });
                //Update SKU Image Path with Product Main Image in Case no image found -Start
                foreach (CartItemViewModel data in cartViewModel.Items)
                {
                    if (Path.GetFileName(data.ImagePath).Equals(MvcAdminConstants.NoImageName))
                    {
                        data.ImagePath = data.Product.ImageLargePath;
                    }
                }
                //Update SKU Image Path with Product Main Image in Case no image found -Start

                // Set Account Id or Cookie Id.
                if (Equals(shoppingCartModel.Account, null) || Equals(shoppingCartModel.Account.AccountId, 0))
                    calculatedModel.CookieId = shoppingCartModel.CookieId;
                else
                    calculatedModel.Account = shoppingCartModel.Account;

                if (!Equals(calculatedModel, null) && !Equals(calculatedModel.Coupons, null))
                {
                    foreach (CouponModel coupon in calculatedModel.Coupons)
                    {
                        if (!Equals(coupon, null) && !coupon.CouponApplied)
                        {
                            cartViewModel.HasError = true;
                            break;
                        }
                    }
                }

                // Model into Session 
                calculatedModel.PortalId = shoppingCartModel.PortalId;
                SaveInSession(MvcAdminConstants.CartModelSessionKey, calculatedModel);
                return cartViewModel;
            }
            return new CartViewModel() { HasError = true };
        }

        public bool RemoveItem(string guid)
        {
            // Get cart from Session.
            var cart = GetFromSession<ShoppingCartModel>(MvcAdminConstants.CartModelSessionKey);
            if (Equals(cart, null) || !cart.ShoppingCartItems.Any()) return false;

            // Check if item exists.
            var item = cart.ShoppingCartItems.FirstOrDefault(x => Equals(x.ExternalId, guid));
            if (Equals(item, null)) return false;

            UpdateAccountInfo(cart);

            // Remove item and update the cart in Session and API.
            cart.ShoppingCartItems.Remove(item);
            if (!Equals(cart.Account, null) && cart.Account.AccountId > 0)
            {
                _shoppingCartsClient.AccountId = cart.Account.AccountId;
            }

            var shoppingCartModel = _shoppingCartsClient.CreateCart(cart);

            if (!Equals(shoppingCartModel, null))
            {
                AddCouponsToShoppingCart(cart, shoppingCartModel);

                shoppingCartModel.GiftCardNumber = cart.GiftCardNumber;
                shoppingCartModel.GiftCardValid = cart.GiftCardValid;
                shoppingCartModel.GiftCardMessage = cart.GiftCardMessage;
                shoppingCartModel.GiftCardApplied = cart.GiftCardApplied;
                shoppingCartModel.GiftCardAmount = cart.GiftCardAmount;
                shoppingCartModel.GiftCardBalance = cart.GiftCardBalance;
                shoppingCartModel.PortalId = cart.PortalId;
                if (!Equals(cart.Shipping, null))
                {
                    shoppingCartModel.ShippingCost = cart.ShippingCost;
                }
            }
            SaveInSession(MvcAdminConstants.CartModelSessionKey, shoppingCartModel);
            return true;
        }

        public bool UpdateItem(string guid, int qty)
        {
            // Get cart from Session.
            var cart = GetFromSession<ShoppingCartModel>(MvcAdminConstants.CartModelSessionKey);
            if (Equals(cart, null) || !cart.ShoppingCartItems.Any()) return false;

            // Check if item exists.
            var item = cart.ShoppingCartItems.FirstOrDefault(x => Equals(x.ExternalId, guid));
            if (Equals(item, null)) return false;

            //Check Inventory
            item.InsufficientQuantity = false;
            //commented temporary            
            var skumodel = _skuAgent.GetBySku(item.ProductId, item.Sku);
            var product = _productAgent.GetProduct(item.ProductId);
            if (skumodel.Inventory.QuantityOnHand < qty &&
                (!product.AllowBackOrder.GetValueOrDefault() && product.TrackInventory.GetValueOrDefault()))
            {
                item.InsufficientQuantity = true;
                SaveInSession(MvcAdminConstants.CartModelSessionKey, cart);
                return false;
            }
            // update item and update the cart in Session and API.
            item.Quantity = qty;

            UpdateAccountInfo(cart);
            if (!Equals(cart.Account, null) && cart.Account.AccountId > 0)
            {
                _shoppingCartsClient.AccountId = cart.Account.AccountId;
            }
            ShoppingCartModel shoppingCartModel = _shoppingCartsClient.CreateCart(cart);

            if (!Equals(shoppingCartModel, null))
            {
                AddCouponsToShoppingCart(cart, shoppingCartModel);
                shoppingCartModel.GiftCardNumber = cart.GiftCardNumber;
                shoppingCartModel.GiftCardValid = cart.GiftCardValid;
                shoppingCartModel.GiftCardMessage = cart.GiftCardMessage;
                shoppingCartModel.GiftCardApplied = cart.GiftCardApplied;
                shoppingCartModel.GiftCardAmount = cart.GiftCardAmount;
                shoppingCartModel.GiftCardBalance = cart.GiftCardBalance;
                shoppingCartModel.PortalId = cart.PortalId;
                if (!Equals(shoppingCartModel.Shipping, null))
                {
                    shoppingCartModel.Shipping = cart.Shipping;
                    shoppingCartModel.ShippingCost = cart.ShippingCost;
                }
            }

            SaveInSession(MvcAdminConstants.CartModelSessionKey, shoppingCartModel);
            return true;
        }

        public bool Merge()
        {
            bool returnStatus = false;

            var cart = GetFromSession<ShoppingCartModel>(MvcAdminConstants.CartModelSessionKey);

            var accountId = GetFromSession<AccountViewModel>(MvcAdminConstants.UserAccountSessionKey).AccountId;
            var model = _shoppingCartsClient.GetCartByAccount(accountId);

            if (!Equals(model, null))
            {
                returnStatus = model.ShoppingCartItems.Any() && !Equals(cart, null) && cart.ShoppingCartItems.Any();
                if (!Equals(cart, null))
                    cart.ShoppingCartItems.ToList().ForEach(model.ShoppingCartItems.Add);
            }
            else
            {
                if (Equals(cart, null)) return false;
                model = cart;
            }

            if (PortalAgent.CurrentPortal.PersistentCartEnabled)
                model.Account = new AccountModel() { AccountId = accountId };

            try
            {
                if (!Equals(cart.Account, null) && cart.Account.AccountId > 0)
                {
                    _shoppingCartsClient.AccountId = cart.Account.AccountId;
                }

                model = _shoppingCartsClient.CreateCart(model);
                if (!Equals(cart, null) && !Equals(cart.Coupons, null))
                {
                    for (int couponIndex = 0; couponIndex < cart.Coupons.Count; couponIndex++)
                    {
                        model.Coupons[couponIndex].Coupon = cart.Coupons[couponIndex].Coupon;
                        model.Coupons[couponIndex].CouponApplied = cart.Coupons[couponIndex].CouponApplied;
                        model.Coupons[couponIndex].CouponValid = cart.Coupons[couponIndex].CouponValid;
                        model.Coupons[couponIndex].CouponMessage = cart.Coupons[couponIndex].CouponMessage;
                    }
                }
            }
            catch (ZnodeException)
            {
                return false;
            }

            if (!Equals(cart.Shipping, null))
            {
                model.ShippingCost = cart.ShippingCost;
            }

            if (PortalAgent.CurrentPortal.PersistentCartEnabled)
                model.Account = new AccountModel() { AccountId = accountId };

            if (returnStatus) SaveInSession(MvcAdminConstants.CartCookieMerged, true);
            SaveInSession(MvcAdminConstants.CartModelSessionKey, model);

            RemoveCookie(MvcAdminConstants.CartCookieKey);

            return returnStatus;
        }

        public bool IsCouponValid(string couponCode)
        {
            _promotionsClient.RefreshCache = true;
            var promotions = _promotionsClient.GetPromotions(null, null, null);

            if (Equals(promotions, null) || !promotions.Promotions.Any())
            {
                return false;
            }

            var now = DateTime.Now;

            return promotions.Promotions.Any(model => model.CouponCode.Equals(couponCode) && model.StartDate <= now && model.EndDate >= now);
        }

        public CartViewModel ApplyCoupon(string couponCode)
        {
            List<CouponModel> promoCodes = new List<CouponModel>();
            List<CouponViewModel> invalidCoupons = new List<CouponViewModel>();

            var cart = GetFromSession<ShoppingCartModel>(MvcAdminConstants.CartModelSessionKey) ??
                       GetCartFromCookie();

            if (Equals(cart, null))
                return new CartViewModel()
                {
                    HasError = true
                };

            if (!Equals(cart.Coupons, null))
            {
                foreach (CouponModel coupon in cart.Coupons)
                {
                    if (coupon.CouponApplied)
                    {
                        promoCodes.Add(coupon);
                    }
                }
            }

            if (!string.IsNullOrEmpty(couponCode))
            {
                if (PortalAgent.CurrentPortal.IsMultipleCouponCodeAllowed)
                {

                    bool isCouponNotAvailableInCart = Equals(cart.Coupons.Where(p => Equals(p.Coupon, couponCode)).ToList().Count, 0);
                    if (isCouponNotAvailableInCart)
                    {
                        CouponModel newCoupon = new CouponModel { Coupon = couponCode };
                        cart.Coupons.Add(newCoupon);
                    }
                }
                else
                {
                    cart.Coupons.Clear();
                    CouponModel newCoupon = new CouponModel { Coupon = couponCode };
                    cart.Coupons.Add(newCoupon);
                }
            }
            else
            {
                invalidCoupons.Add(new CouponViewModel { Coupon = couponCode, CouponMessage = "Coupon Code cannot be empty.", CouponApplied = false, CouponValid = false });
            }

            var cartModel = Calculate(cart);

            bool cartCouponsApplied = (invalidCoupons.Count > 0 ? false : true);

            foreach (CouponViewModel couponViewModel in cartModel.Coupons)
            {
                if (couponViewModel.CouponApplied)
                {
                    cartCouponsApplied &= true;
                }
                else
                {
                    cartCouponsApplied &= false;
                    invalidCoupons.Add(couponViewModel);
                }
            }
            if (!cartCouponsApplied)
            {

                cart.Coupons.Clear();
                foreach (CouponModel couponModel in promoCodes)
                {
                    cart.Coupons.Add(couponModel);
                }
                cartModel = Calculate(cart);
                cartModel.HasError = true;
                foreach (CouponViewModel invalidCoupon in invalidCoupons)
                {
                    cartModel.Coupons.Add(invalidCoupon);
                }
            }
            return cartModel;
        }

        public void SetMessages(CartViewModel model)
        {
            SetStatusMessage(model);
        }

        public ShoppingCartModel GetCartFromCookie()
        {
            if (!PortalAgent.CurrentPortal.PersistentCartEnabled)
                return null;

            var cookieValue = GetFromCookie(MvcAdminConstants.CartCookieKey);
            if (!string.IsNullOrEmpty(cookieValue))
            {
                int cookieId;
                if (int.TryParse(cookieValue, out cookieId))
                {
                    try
                    {
                        var shoppingCartModel = _shoppingCartsClient.GetCartByCookie(cookieId);

                        if (!Equals(shoppingCartModel, null))
                        {
                            return shoppingCartModel;
                        }
                    }
                    catch (Exception)
                    {

                    }

                    return null;
                }
            }

            return null;
        }

        public bool RemoveAllCartItems()
        {
            RemoveCookie(MvcAdminConstants.CartCookieKey);
            var cart = new ShoppingCartModel();
            UpdateAccountInfo(cart);
            if (!Equals(cart.Account, null) && cart.Account.AccountId > 0)
            {
                _shoppingCartsClient.AccountId = cart.Account.AccountId;
            }

            var shoppingCartModel = _shoppingCartsClient.CreateCart(cart);
            SaveInSession(MvcAdminConstants.CartModelSessionKey, new ShoppingCartModel());
            return true;
        }
        #endregion

        #region Private Methods

        private void UpdateAccountInfo(ShoppingCartModel cart)
        {
            if (Equals(cart, null))
            {
                return;
            }

            AccountModel account = GetFromSession<AccountModel>(MvcAdminConstants.ShoppingCartUserAccountSessionKey);
            if (!Equals(account, null))
            {
                cart.Account = account;
            }
            else
            {
                AccountViewModel currentAccount = GetFromSession<AccountViewModel>(MvcAdminConstants.UserAccountSessionKey);
                cart.Account = !Equals(currentAccount, null) ? AccountViewModelMap.ToAccountModel(currentAccount) : null;
            }
        }

        /// <summary>
        /// To Get Shipping Addess from session
        /// </summary>
        /// <returns>returns Shipping Addess</returns>
        private AddressViewModel GetShippingAddess()
        {
            AddressViewModel model = GetFromSession<AddressViewModel>(MvcAdminConstants.ShippingAddressKey);
            if (Equals(model, null))
            {
                AccountViewModel accountModel = _accountAgent.GetAccountViewModel();
                if (!Equals(accountModel, null))
                {
                    model = accountModel.Addresses.FirstOrDefault(x => x.IsDefaultShipping);
                }
            }

            return model;
        }

        /// <summary>
        /// Adds coupons to shopping cart.
        /// </summary>
        /// <param name="cart">Cart where coupons will be applied.</param>
        /// <param name="shoppingCartModel">coupons transferred from cart to shopping cart.</param>
        private static void AddCouponsToShoppingCart(ShoppingCartModel cart, ShoppingCartModel shoppingCartModel)
        {
            foreach (CouponModel coupon in cart.Coupons)
            {
                shoppingCartModel.Coupons.Add(coupon);
            }
        }
        #endregion
    }
}