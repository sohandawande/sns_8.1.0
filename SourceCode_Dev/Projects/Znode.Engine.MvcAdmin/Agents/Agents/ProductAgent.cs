﻿using Resources;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;
using Znode.Libraries.Helpers;
using Znode.Libraries.Helpers.Constants;
using Znode.Libraries.Helpers.Extensions;

namespace Znode.Engine.MvcAdmin.Agents
{
    public class ProductAgent : BaseAgent, IProductAgent
    {
        #region Private Variables

        private readonly IProductsClient _productsClient;
        private readonly IProductTypeClient _productTypeClient;
        private readonly IManufacturersClient _manufacturersClient;
        private readonly ISuppliersClient _suppliersClient;
        private readonly IPromotionTypesClient _promotionTypesClient;
        private readonly ITaxClassesClient _taxClassesClient;
        private readonly IShippingRuleTypesClient _shippingRuleTypesClient;
        private readonly ISkusClient _skuClient;
        private readonly IEnvironmentConfigClient _environmentConfigClient;
        private readonly ICatalogsClient _catalogClient;
        private readonly ICategoriesClient __categoriesClient;
        private readonly ISkuProfileEffectiveClient _skuProfileEffectiveClient;
        private readonly IPortalsClient _portalsClient;
        private readonly IAccountAgent _accountAgent;
        private readonly ICategoryAgent _categoryAgent;
        private readonly ISkuAgent _skuAgent;
        private readonly IProfilesClient _profilesClient;
        private readonly IProfilesAgent _profilesAgent;
        private readonly IShoppingCartsClient _shoppingCartsClient;

        private readonly ExpandCollection expands = new ExpandCollection()
            {
                ExpandKeys.Images,
                ExpandKeys.Skus,
                ExpandKeys.Promotions,
                ExpandKeys.Reviews,
                ExpandKeys.Facets,
                ExpandKeys.Categories,
                ExpandKeys.Attributes,
                ExpandKeys.AddOns
            };

        #endregion

        #region Constructor

        public ProductAgent()
        {
            _productsClient = GetClient<ProductsClient>();
            _productTypeClient = GetClient<ProductTypeClient>();
            _manufacturersClient = GetClient<ManufacturersClient>();
            _suppliersClient = GetClient<SuppliersClient>();
            _promotionTypesClient = GetClient<PromotionTypesClient>();
            _taxClassesClient = GetClient<TaxClassesClient>();
            _shippingRuleTypesClient = GetClient<ShippingRuleTypesClient>();
            _skuClient = GetClient<SkusClient>();
            _environmentConfigClient = GetClient<EnvironmentConfigClient>();
            _catalogClient = GetClient<CatalogsClient>();
            __categoriesClient = GetClient<CategoriesClient>();
            _skuProfileEffectiveClient = GetClient<SkuProfileEffectiveClient>();
            _portalsClient = GetClient<PortalsClient>();
            _accountAgent = new AccountAgent();
            _categoryAgent = new CategoryAgent();
            _skuAgent = new SkuAgent();
            _profilesClient = GetClient<ProfilesClient>();
            _profilesAgent = new ProfilesAgent();
            _shoppingCartsClient = GetClient<ShoppingCartsClient>();
        }

        #endregion

        #region Public Methods

        #region Product Details

        public ProductListViewModel GetProducts(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            ProductListViewModel model = new ProductListViewModel();

            ReplaceFilterKeyName(ref filters, FilterKeys.Name, FilterKeys.ProductName);
            ReplaceFilterKeyName(ref filters, FilterKeys.ProductCode, FilterKeys.ProductNumber);
            ReplaceSortKeyName(sortCollection, FilterKeys.ProductCode, FilterKeys.ProductNumber);

            _productsClient.RefreshCache = true;
            var productList = _productsClient.GetProducts(new ExpandCollection { ExpandKeys.Catalogs, ExpandKeys.Categories }, filters, sortCollection, pageIndex, recordPerPage);


            return model = ProductViewModelMap.ToListViewModel(productList.Products, productList.TotalResults);
        }

        public ProductViewModel GetProduct(int productId)
        {
            _productsClient.RefreshCache = true;
            var product = _productsClient.GetProductDetailsByProductId(productId);
            if (Equals(product, null))
            {
                return null;
            }

            var productViewModel = ProductViewModelMap.ToViewModel(product);
            return productViewModel;
        }

        public bool IsSkuExist(string Sku, int productId)
        {
            _skuClient.RefreshCache = true;
            var skuCount = _skuClient.GetSkus(null, new FilterCollection()
                                                            {
                                                                new FilterTuple(FilterKeys.Sku, FilterOperators.Equals,Sku),
                                                                 new FilterTuple(FilterKeys.ProductId, FilterOperators.NotEquals,productId.ToString())
                                                            }, null);

            return Equals(skuCount.Skus, null) ? false : true;
        }

        public bool IsSeoUrlExist(int productId, string seoUrl)
        {
            if (string.IsNullOrEmpty(seoUrl))
            {
                return false;
            }
            _productsClient.RefreshCache = true;
            var seoUrlCount = _productsClient.IsSeoUrlExist(productId, seoUrl);
            return Equals(seoUrlCount, null) ? false : seoUrlCount;
        }

        public bool SaveProduct(ProductViewModel model, out string errorMessage)
        {
            try
            {
                errorMessage = string.Empty;
                SetDefaultValues(model);
                var product = _productsClient.CreateProduct(ProductViewModelMap.ToModel(model));
                model.ProductId = product.ProductId;
                if (product.ProductId > 0)
                {
                    SaveImage(model);
                }
                return model.ProductId > 0 ? true : false;
            }
            catch (ZnodeException ex)
            {
                errorMessage = ex.ErrorMessage;
                return false;
            }
        }

        public bool UpdateProduct(ProductViewModel model, out string errorMessage)
        {
            try
            {
                errorMessage = string.Empty;
                SetDefaultValues(model);
                var product = _productsClient.UpdateProduct(model.ProductId, ProductViewModelMap.ToModel(model));
                if (product.ProductId > 0)
                {
                    SaveImage(model);
                }
                return product.ProductId > 0 ? true : false;
            }
            catch (ZnodeException ex)
            {
                errorMessage = ex.ErrorMessage;
                return false;
            }
        }

        public bool DeleteProduct(int productId, out string errorMessage)
        {
            try
            {
                errorMessage = string.Empty;
                return _productsClient.DeleteProductByProductId(productId);
            }
            catch (ZnodeException ex)
            {
                errorMessage = ex.ErrorMessage;
                return false;
            }
        }

        public ProductViewModel BindProductPageDropdown(ProductViewModel model, int portalId, bool isFranchisable = false)
        {
            FilterCollection filters = new FilterCollection();
            filters.Add(new FilterTuple(FilterKeys.IsActive, FilterOperators.Equals, FilterKeys.ActiveIndTrue));
            _productTypeClient.RefreshCache = true;
            _manufacturersClient.RefreshCache = true;
            _shippingRuleTypesClient.RefreshCache = true;
            //to sort by name
            var sort = new SortCollection();
            sort.Add(SortKeys.DisplayOrder, SortDirections.Ascending);

            _productTypeClient.RefreshCache = true;
            Collection<ProductTypeModel> productTypes = _productTypeClient.GetProductTypes(null, null, sort).ProductTypes;
            if (!Equals(productTypes, null) && productTypes.Count > 0 && isFranchisable)
            {
                productTypes = productTypes.ToList().FindAll(x => Equals(x.IsFranchiseable, true)).ToCollection();
            }
            model.ProductTypeList = ProductViewModelMap.ToListItems(productTypes);

            //to sort by name
            sort = new SortCollection();
            sort.Add(SortKeys.Name, SortDirections.Ascending);

            _manufacturersClient.RefreshCache = true;
            model.ManufacturerList = ProductViewModelMap.ToListItems(_manufacturersClient.GetManufacturers(filters, sort, null, null).Manufacturers);
            _suppliersClient.RefreshCache = true;
            model.SupplierList = ProductViewModelMap.ToListItems(_suppliersClient.GetSuppliers(null, filters, sort).Suppliers);
            _taxClassesClient.RefreshCache = true;
            if (isFranchisable)
            {
                filters.Add(new FilterTuple(FilterKeys.PortalId, FilterOperators.Equals, portalId.ToString()));
                model.TaxClassList = ProductViewModelMap.ToListItems(_taxClassesClient.GetTaxClassList(filters, sort, null, null).TaxClasses);
            }
            else
            {
                model.TaxClassList = ProductViewModelMap.ToListItems(_taxClassesClient.GetActiveTaxClassByPortalId(portalId).TaxClasses);
            }

            //to sort by name
            sort = new SortCollection();
            sort.Add(SortKeys.ShippingRuleId, SortDirections.Ascending);
            _shippingRuleTypesClient.RefreshCache = true;
            model.ShippingRuleTypeList = ProductViewModelMap.ToListItems(_shippingRuleTypesClient.GetShippingRuleTypes(filters, sort).ShippingRuleTypes);
            model.ExpirationFrequencyList = ProductViewModelMap.ToListItems();

            return model;
        }

        public List<SelectListItem> GetDurationList()
        {
            return ProductViewModelMap.ToListItems();
        }

        public List<AttributeTypeValueViewModel> GetProductAttributesByProductTypeId(int? productTypeId)
        {
            FilterCollection filters = new FilterCollection();
            filters.Add(new FilterTuple(FilterKeys.ProductTypeId, FilterOperators.Equals, productTypeId.Value.ToString()));
            _productTypeClient.RefreshCache = true;
            var list = ProductViewModelMap.ToListViewModel(_productTypeClient.GetProductAttributesByProductTypeId(null, filters, null, null, null));
            return list;
        }

        #endregion

        #region Product Setting

        public bool UpdateProductSetting(ProductViewModel model)
        {
            if (Equals(model.RecurringBillingInd, false))
            {
                model.RecurringBillingInitialAmount = null;
                model.RecurringBillingFrequency = null;
            }
            GetInventorySettings(model);
            model.NewProductInd = Equals(model.NewProductInd, null) ? false : true;
            var product = _productsClient.UpdateProductSettings(model.ProductId, ProductViewModelMap.ToModel(model));
            return (product.ProductId > 0) ? true : false;
        }

        #endregion

        #region Product Tags

        public ProductTagsViewModel GetProductTags(int productId)
        {
            _productsClient.RefreshCache = true;
            var productTags = _productsClient.GetProductTags(productId);
            return ProductTagsViewModelMap.ToViewModel(productTags);
        }

        public bool InsertProductTags(ProductTagsViewModel model)
        {
            var productTags = _productsClient.CreateProductTags(ProductTagsViewModelMap.ToModel(model));
            return productTags.TagId > 0;

        }

        public bool UpdateProductTags(ProductTagsViewModel model)
        {
            var productTags = _productsClient.UpdateProductTags(model.TagId, ProductTagsViewModelMap.ToModel(model));
            return productTags.TagId > 0;
        }

        public bool DeleteProductTags(int tagId)
        {
            return _productsClient.DeleteProductTags(tagId);
        }

        #endregion

        #region Product Facets

        public ProductFacetListViewModel GetProductFacets(int productId, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            filters.Add(new FilterTuple(FilterKeys.ProductId, FilterOperators.Equals, productId.ToString()));
            _productsClient.RefreshCache = true;
            var productFacetsList = _productsClient.GetProductFacets(null, filters, sortCollection, pageIndex, recordPerPage);
            if (!Equals(productFacetsList, null) && !Equals(productFacetsList.FacetGroups, null))
            {
                return ProductFacetsViewModelMap.ToListViewModel(productFacetsList.FacetGroups, productFacetsList.TotalResults, productId);
            }
            return null;
        }

        public ProductFacetsViewModel GetAssociatedFacetGroup(int productId, int facetGroupId)
        {
            _productsClient.RefreshCache = true;
            var productFacets = _productsClient.GetAssociatedFacets(productId, facetGroupId);
            return ProductFacetsViewModelMap.ToViewModel(productFacets, facetGroupId);
        }

        public bool BindAssociatedFacets(ProductFacetsViewModel model)
        {
            return _productsClient.BindAssociatedFacets(ProductFacetsViewModelMap.ToModel(model));
        }

        public bool DeleteProductAssociatedFacets(int productId, int facetGroupId)
        {
            return _productsClient.DeleteProductAssociatedFacets(productId, facetGroupId);
        }

        #endregion

        #region Product Alternate Images
        public List<SelectListItem> GetProductImageTypes(int? ProductImageTypeId)
        {
            _productsClient.RefreshCache = true;
            var productImageTypeList = _productsClient.GetProductImageTypes();
            if (!Equals(productImageTypeList, null) && !Equals(productImageTypeList.ProductImageTypes, null))
            {
                return ProductImageTypeModelMap.ToListItemsView(productImageTypeList.ProductImageTypes, ProductImageTypeId);
            }
            return null;
        }

        public ProductAlternateImageListViewModel GetProductAlternateImages(int productId, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            filters.Add(new FilterTuple(FilterKeys.ProductId, FilterOperators.Equals, productId.ToString()));
            ReplaceSortKeyName(sortCollection, MvcAdminConstants.Id, MvcAdminConstants.ProductImageId);
            _productsClient.RefreshCache = true;
            var productImageList = _productsClient.GetProductAlternateImages(null, filters, sortCollection, pageIndex - 1, recordPerPage);
            if (!Equals(productImageList, null) && !Equals(productImageList.ProductImages, null))
            {
                return ProductImageTypeModelMap.ToListViewModel(productImageList.ProductImages, productImageList.TotalResults);
            }
            return new ProductAlternateImageListViewModel();
        }

        public bool InsertProductAlternateImage(ProductAlternateImageViewModel model)
        {
            string strImageFile = string.Empty;
            BindImagePath(model, out strImageFile);
            model.ImageFile = strImageFile;
            if (Equals(model.UserType, UserTypes.MallAdmin.ToString()))
            {
                model.ReviewStateID = Convert.ToInt32(ZNodeProductReviewState.PendingApproval);
            }
            else
            {
                model.ReviewStateID = Convert.ToInt32(ZNodeProductReviewState.Approved);
            }
            var productImage = _productsClient.InsertProductAlternateImage(ProductImageTypeModelMap.ToModel(model));
            return productImage.ProductImageID > 0;
        }

        public bool UpdateProductAlternateImage(ProductAlternateImageViewModel model)
        {
            if (Equals(model.SelectedImageStatus, SelectImageStatus.NewImage))
            {
                string strImageFile = string.Empty;
                BindImagePath(model, out strImageFile);
                model.ImageFile = strImageFile;
            }
            if (Equals(model.UserType, UserTypes.MallAdmin.ToString()) && Equals(model.ReviewStateID, Convert.ToInt32(ZNodeProductReviewState.PendingApproval)))
            {
                model.ReviewStateID = Convert.ToInt32(ZNodeProductReviewState.PendingApproval);
            }
            else
            {
                model.ReviewStateID = Convert.ToInt32(ZNodeProductReviewState.Approved);
            }
            var productImage = _productsClient.UpdateProductAlternateImage(model.ProductImageId, ProductImageTypeModelMap.ToModel(model));
            return productImage.ProductImageID > 0;
        }

        public bool DeleteProductAlternateImage(int productImageId)
        {
            return _productsClient.DeleteProductAlternateImage(productImageId);
        }

        public ProductAlternateImageViewModel GetProductAlternateImage(int productImageId)
        {
            _productsClient.RefreshCache = true;
            var productImage = _productsClient.GetProductAlternateImageByProductImageId(productImageId);
            if (!Equals(productImage, null))
            {
                EnvironmentConfigModel environmentConfigModel = _environmentConfigClient.GetEnvironmentConfig();
                productImage.ImagePath = environmentConfigModel.OriginalImagePath + "/" + productImage.ImageFile;
                return ProductImageTypeModelMap.ToViewModel(productImage);
            }
            return new ProductAlternateImageViewModel();
        }

        #endregion

        #region Product Bundles

        public ProductBundlesListViewModel GetProductBundleList(int productId, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            filters.Add(new FilterTuple(FilterKeys.ParentProductID, FilterOperators.Equals, productId.ToString()));
            _productsClient.RefreshCache = true;
            var productBundlesList = _productsClient.GetProductBundleList(null, filters, sortCollection, pageIndex, recordPerPage);
            if (!Equals(productBundlesList, null) && !Equals(productBundlesList.ProductBundles, null))
            {
                return ProductBundlesModelMap.ToListViewModel(productBundlesList.ProductBundles, productBundlesList.TotalResults);
            }
            return null;
        }

        public ProductBundlesViewModel BindProductSearchDropDown(FilterCollectionDataModel formModel, bool isFranchisable = false)
        {
            ProductBundlesViewModel model = new ProductBundlesViewModel();
            FilterCollection filters = formModel.Filters;
            ReplaceFilterKeyName(ref filters, FilterKeys.Name, FilterKeys.ProductName);
            ReplaceFilterKeyName(ref filters, FilterKeys.ProductCode, FilterKeys.ProductNumber);
            //to sort by name
            var sort = new SortCollection();
            sort.Add(SortKeys.DisplayOrder, SortDirections.Ascending);
            _productTypeClient.RefreshCache = true;
            ProductTypeListModel productTypes = _productTypeClient.GetProductTypes(null, null, sort);
            if (!Equals(productTypes, null) && !Equals(productTypes.ProductTypes, null) && productTypes.ProductTypes.Count > 0 && isFranchisable)
            {
                productTypes.ProductTypes = productTypes.ProductTypes.ToList().FindAll(x => Equals(x.IsFranchiseable, true)).ToCollection();
            }
            model.ProductTypes = ProductTypeViewModelMap.ToListViewModel(productTypes);
            _catalogClient.RefreshCache = true;
            model.Catalogs = CatalogViewModelMap.ToListViewModel(_catalogClient.GetCatalogs(null, null), 0, 0);
            _productsClient.RefreshCache = true;
            var productList = _productsClient.GetProductList(null, (filters.Count > 0) ? filters : formModel.Filters, formModel.SortCollection, formModel.Page, formModel.RecordPerPage);
            model.Products = ProductViewModelMap.ToListViewModel(productList.Products, productList.TotalResults);
            return model;
        }

        public bool DeleteBundleProduct(int parentChildProductID)
        {
            return _productsClient.DeleteBundleProduct(parentChildProductID);
        }

        public ProductBundlesViewModel GetProductList(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            ProductBundlesViewModel model = new ProductBundlesViewModel();
            _productsClient.RefreshCache = true;
            var productList = _productsClient.GetProductList(null, filters, sortCollection, pageIndex, recordPerPage);
            model.Products = ProductViewModelMap.ToListViewModel(productList.Products, productList.TotalResults);
            return model;
        }


        public bool AssociateBundleProduct(ProductViewModel viewModel)
        {
            return _productsClient.AssociateBundleProduct(ProductViewModelMap.ToModel(viewModel));
        }

        #endregion



        #endregion

        #region Private Methods
        private bool IsValidPath(string filePath)
        {
            bool status = false;
            if (System.IO.Directory.Exists(filePath))
            {
                status = true;
            }
            else
            {
                CreateDirectory(filePath);
                status = true;
            }
            return status;
        }

        private void CreateDirectory(string filePath)
        {
            Directory.CreateDirectory(filePath);
        }

        private string GetFullName(string fileName, string userType, string portalID, string accountID, string _AppendToFileName, out string folderPath)
        {
            string imagePath = Path.GetFileNameWithoutExtension(fileName) + _AppendToFileName + Path.GetExtension(fileName);
            EnvironmentConfigModel environmentConfigModel = _environmentConfigClient.GetEnvironmentConfig();
            if (Equals(userType, "sadmin"))
            {
                folderPath = environmentConfigModel.OriginalImagePath + "Turnkey/" + portalID + "/";
                return environmentConfigModel.OriginalImagePath + "Turnkey/" + portalID + "/" + imagePath;
            }
            else if (Equals(userType, "madmin"))
            {
                folderPath = environmentConfigModel.OriginalImagePath + "Mall/" + accountID + "/";
                return environmentConfigModel.OriginalImagePath + "Mall/" + accountID + "/" + imagePath;
            }
            else if (Equals(userType, "fadmin"))
            {
                folderPath = environmentConfigModel.OriginalImagePath + "Turnkey/" + portalID + "/";
                return environmentConfigModel.OriginalImagePath + "Turnkey/" + portalID + "/" + imagePath;
            }
            else
            {
                folderPath = environmentConfigModel.OriginalImagePath;
                return environmentConfigModel.OriginalImagePath + imagePath;
            }
        }

        private void BindImagePath(ProductAlternateImageViewModel model, out string imagePath)
        {
            _productsClient.RefreshCache = true;
            var productDetails = _productsClient.GetProduct(model.ProductId);
            imagePath = string.Empty;
            string _AppendToFileName = string.Format("-{0}", model.ProductId);
            string fileNameWithProductIdExtension = Path.GetFileNameWithoutExtension(model.ProductImage.FileName) + _AppendToFileName + Path.GetExtension(model.ProductImage.FileName);

            if (!Equals(model.ProductImage, null))
            {
                //to save image in data folder
                ImageHelper imageHelper = new ImageHelper();
                string validExtension = Convert.ToString(ConfigurationManager.AppSettings["ImageExtension"]);
                imagePath = imageHelper.SaveImageToPhysicalPathByName(model.PortalId, productDetails.AccountId, validExtension, fileNameWithProductIdExtension, model.ProductImage, null);
            }
        }

        /// <summary>
        /// To save image by Id in data folder and update image name in database table
        /// </summary>
        /// <param name="model">ProductViewModel model</param>
        private void SaveImage(ProductViewModel model)
        {
            if (!Equals(model.ProductImage, null))
            {
                //to save image in data folder
                ImageHelper imageHelper = new ImageHelper();
                string validExtension = Convert.ToString(ConfigurationManager.AppSettings["ImageExtension"]);
                string imagePath = imageHelper.SaveImageToPhysicalPathById(model.PortalID, model.AccountID, model.ProductId, validExtension, model.ProductImage);
                if (!string.IsNullOrEmpty(imagePath) && !Equals(imagePath, MvcAdminConstants.FileUploadErrorCode))
                {
                    //to update image path in database by id
                    UpdateImageModel imageModel = new UpdateImageModel();
                    imageModel.Id = model.ProductId;
                    imageModel.EntityName = Convert.ToString(EntityName.Product);
                    imageModel.ImagePath = imagePath;
                    _productsClient.UpdateImage(imageModel);
                }
            }
        }

        private void SaveImage(SkuViewModel model)
        {
            if (!Equals(model.SkuImage, null))
            {
                //to save image in data folder
                ImageHelper imageHelper = new ImageHelper();
                string validExtension = Convert.ToString(ConfigurationManager.AppSettings["ImageExtension"]);
                string imagePath = imageHelper.SaveImageToPhysicalPathByName(null, null, validExtension, string.Empty, model.SkuImage, null);
                if (!string.IsNullOrEmpty(imagePath) && !Equals(imagePath, MvcAdminConstants.FileUploadErrorCode))
                {
                    //to update image path in database by id
                    UpdateImageModel imageModel = new UpdateImageModel();
                    imageModel.Id = model.Id;
                    imageModel.EntityName = Convert.ToString(EntityName.SKU);
                    imageModel.ImagePath = imagePath;
                    _productsClient.UpdateImage(imageModel);
                }
            }
        }
        #endregion

        #region Product Category

        public CategoryListViewModel GetProductCategoriesByProductId(int productId, int portalId, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            filters.Add(FilterKeys.ProductId, FilterOperators.Equals, productId.ToString());
            if (!Equals(portalId, 0))
            {
                filters.Add(FilterKeys.PortalId, FilterOperators.Equals, portalId.ToString());
            }
            _productsClient.RefreshCache = true;
            var list = _productsClient.GetProductCategoryByProductId(null, filters, sortCollection, pageIndex, recordPerPage);
            if (!Equals(list, null))
            {
                return CategoryViewModelMap.ToListModel(list, productId);
            }
            return null;
        }

        public CategoryListViewModel GetProductUnAssociatedCategoryByProductId(int productId, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            _productsClient.RefreshCache = true;
            var list = _productsClient.GetProductUnAssociatedCategoryByProductId(productId, null, filters, sortCollection, pageIndex, recordPerPage);
            return CategoryViewModelMap.ToListModel(list);
        }

        public bool AssociateProductCategory(CategoryViewModel viewModel)
        {
            return _productsClient.AssociateProductCategory(CategoryViewModelMap.ToModel(viewModel));
        }

        public bool UnAssociateProductCategory(CategoryViewModel viewModel)
        {
            return _productsClient.UnAssociateProductCategory(CategoryViewModelMap.ToModel(viewModel));
        }

        #endregion

        #region Product Sku

        public SkuListViewModel GetProductSkuByProductId(int productId, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            SkuListViewModel model = new SkuListViewModel();
            ReplaceSortKeyName(sortCollection, MvcAdminConstants.Id, MvcAdminConstants.SkuId);
            filters.Add(FilterKeys.ProductId, FilterOperators.Equals, productId.ToString());
            _productsClient.RefreshCache = true;
            var list = _productsClient.GetProductSkuDetails(null, filters, sortCollection, pageIndex, recordPerPage);
            ReplaceSortKeyName(sortCollection, MvcAdminConstants.SkuId, MvcAdminConstants.Id);
            if (!Equals(list, null))
            {
                model = SkuViewModelMap.ToListModel(list);
                return model;
            }
            return null;
        }

        public SkuViewModel GetProductSkuDetailsBySkuId(int skuId)
        {
            _productsClient.RefreshCache = true;
            var list = _productsClient.GetProductSkuDetails(null, new FilterCollection()
                                                            {
                                                                new FilterTuple(FilterKeys.SkuId, FilterOperators.Equals,skuId.ToString())
                                                            }, null, null, null);
            if (!Equals(list, null))
            {
                return SkuViewModelMap.ToViewModel(list);
            }
            return null;
        }

        public SkuViewModel GetProductSkuInfo(int productId)
        {
            if (!Equals(productId, 0))
            {
                _productsClient.RefreshCache = true;
                var product = _productsClient.GetProductDetailsByProductId(productId);
                if (Equals(product, null))
                {
                    return null;
                }
                return SkuViewModelMap.ToViewModel(product);
            }
            else
            {
                return null;
            }
        }

        public SkuViewModel BindProductSkuDropdown(SkuViewModel model)
        {
            //to sort by active suppliers only
            FilterCollection filters = new FilterCollection();
            filters.Add(new FilterTuple(FilterKeys.IsActive, FilterOperators.Equals, FilterKeys.ActiveIndTrue));

            //to sort by name
            var sort = new SortCollection();
            sort.Add(SortKeys.Name, SortDirections.Ascending);
            _suppliersClient.RefreshCache = true;
            model.SupplierList = ProductViewModelMap.ToListItems(_suppliersClient.GetSuppliers(null, filters, sort).Suppliers);

            return model;
        }

        public string SaveProductSku(SkuViewModel model)
        {
            var sku = _skuClient.CreateSku(SkuViewModelMap.ToModel(model));
            if (sku.SkuId > 0)
            {
                model.Id = sku.SkuId;
                SaveImage(model);
            }
            return sku.Error;
        }

        public string UpdateProductSku(SkuViewModel model)
        {
            try
            {
                var sku = _skuClient.UpdateSku(model.Id, SkuViewModelMap.ToModel(model));
                if (sku.SkuId > 0)
                {
                    SaveImage(model);
                }
                return sku.Error;
            }
            catch (ZnodeException ex)
            {
                return ex.ErrorMessage.ToString();
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }

        public bool DeleteSku(int skuId, out string errorMessage)
        {
            try
            {
                errorMessage = string.Empty;
                return _skuClient.DeleteSku(skuId); ;
            }
            catch (ZnodeException ex)
            {
                errorMessage = ex.ErrorMessage.ToString();
                return false;
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message.ToString();
                return false;
            }
        }

        public ProductFacetListViewModel GetSKUFacets(int skuId, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {

            foreach (var tuple in filters)
            {
                if (tuple.Item1.ToLower().Equals(FilterKeys.ProductId))
                {
                    filters.Remove(tuple);
                    break;
                }
            }
            _productsClient.RefreshCache = true;
            var productFacetsList = _productsClient.GetSkuFacets(null, filters, sortCollection, pageIndex, recordPerPage);
            if (!Equals(productFacetsList, null) && !Equals(productFacetsList.FacetGroups, null))
            {
                return ProductFacetsViewModelMap.ToListViewModel(productFacetsList.FacetGroups, skuId, productFacetsList.TotalResults);
            }
            return null;
        }

        public bool AssociateSkuFacets(int skuId, string associateFacetIds, string unassociateFacetIds)
        {
            if (string.IsNullOrEmpty(unassociateFacetIds))
            {
                unassociateFacetIds = "0";
            }
            if (string.IsNullOrEmpty(associateFacetIds))
            {
                associateFacetIds = "0";
            }
            return _productsClient.AssociateSkuFacets(skuId, associateFacetIds, unassociateFacetIds);
        }

        public bool DeleteSkuAssociatedFacets(int skuId, int facetGroupId)
        {
            return _productsClient.DeleteSkuAssociatedFacets(skuId, facetGroupId);
        }

        public ProductFacetsViewModel GetSKUAssociatedFacetGroup(int productId, int skuId, int facetGroupId = 0)
        {
            _productsClient.RefreshCache = true;
            var productFacets = _productsClient.GetSkuAssociatedFacets(productId, skuId, facetGroupId);
            return ProductFacetsViewModelMap.ToViewModel(productFacets, facetGroupId);
        }

        #endregion

        #region AddOns

        public AddOnListViewModel GetProductAddOns(int productId, FilterCollection filters = null, SortCollection sorts = null, int pageIndex = 1, int pageSize = 10)
        {
            _productsClient.RefreshCache = true;
            filters.Add(new FilterTuple(FilterKeys.ProductId, FilterOperators.Equals, productId.ToString()));
            _productsClient.RefreshCache = true;
            AddOnListModel model = _productsClient.GetProductAddOns(productId, null, filters, sorts, pageIndex, pageSize);
            return AddOnViewModelMap.ToListViewModel(model);
        }

        public bool RemoveProductAddOn(int productAddOnId)
        {
            return _productsClient.RemoveProductAddOn(productAddOnId);
        }

        public void AssociateAddOns(AddOnListViewModel listViewModel)
        {
            List<AddOnModel> addOnModels = AddOnViewModelMap.ToAddOnModels(listViewModel);
            _productsClient.AssociateAddOns(addOnModels);
        }

        public AddOnListViewModel GetUnassociatedAddOns(int productId, int portalId, FilterCollection filters = null, SortCollection sorts = null, int? pageIndex = null, int? pageSize = null)
        {
            _productsClient.RefreshCache = true;
            ReplaceFilterKeyName(ref filters, MvcAdminConstants.SkuOrProductNum, MvcAdminConstants.SkuOrProductNumWithPipeOperator);
            _productsClient.RefreshCache = true;
            AddOnListModel model = _productsClient.GetUnassociatedAddOns(productId, portalId, null, filters, sorts, pageIndex, pageSize);
            var viewModel = AddOnViewModelMap.ToListViewModel(model);
            viewModel.ProductId = productId;
            return viewModel;
        }

        #endregion

        #region Highlights

        public HighlightsListViewModel GetProductHighlights(int productId, FilterCollection filters = null, SortCollection sorts = null, int? pageIndex = null, int? recordPerPage = null)
        {
            _productsClient.RefreshCache = true;
            filters.Add(new FilterTuple(FilterKeys.ProductId, FilterOperators.Equals, productId.ToString()));
            _productsClient.RefreshCache = true;
            HighlightListModel model = _productsClient.GetProductHighlights(productId, null, filters, sorts, pageIndex, recordPerPage);
            return HighlightViewModelMap.ToListViewModels(model);
        }

        public bool RemoveProductHighlight(int productHighlightId)
        {
            bool status = _productsClient.RemoveProductHighlight(productHighlightId);
            return status;
        }

        public bool AssociateHighlights(HighlightsListViewModel listViewModel)
        {
            List<HighlightModel> highlightModels = HighlightViewModelMap.ToHighlightModels(listViewModel);
            return Equals(_productsClient.AssociateHighlights(highlightModels), null) ? false : true;

        }

        public HighlightsListViewModel GetUnassociatedHighlights(int productId, int portalId, FilterCollection filters = null, SortCollection sorts = null, int? pageIndex = null, int? recordPerPage = null)
        {
            _productsClient.RefreshCache = true;
            HighlightListModel model = _productsClient.GetUnassociatedHighlights(productId, portalId, null, filters, sorts, pageIndex, recordPerPage);
            var viewModel = HighlightViewModelMap.ToListViewModels(model);
            viewModel.ProductId = productId;
            return viewModel;
        }

        #endregion

        #region DropDown For Product Search

        public List<ProductTypeModel> GetProductType()
        {
            ProductTypeListModel list = new ProductTypeListModel();
            _productTypeClient.RefreshCache = true;
            list = _productTypeClient.GetProductTypes(null, null, null);
            return list.ProductTypes.ToList();
        }

        public List<ManufacturerModel> GetManufacturer()
        {
            ManufacturerListModel manufacturerTypeList = new ManufacturerListModel();
            _manufacturersClient.RefreshCache = true;
            manufacturerTypeList = _manufacturersClient.GetManufacturers(null, null);
            return manufacturerTypeList.Manufacturers.ToList();
        }

        public List<CatalogModel> Getcatalog()
        {
            FilterCollection filters = new FilterCollection();
            CatalogListModel catalogList = new CatalogListModel();
            filters.Add(new FilterTuple(FilterKeys.Username, FilterOperators.Contains, HttpContext.Current.User.Identity.Name));
            _catalogClient.RefreshCache = true;
            catalogList = _catalogClient.GetCatalogs(filters, null);

            return catalogList.Catalogs.ToList();
        }

        public List<CategoryModel> GetCategories()
        {
            CategoryListModel categoryList = new CategoryListModel();
            __categoriesClient.RefreshCache = true;
            categoryList = __categoriesClient.GetCategories(null, null, null);

            return categoryList.Categories.ToList();
        }
        #endregion

        #region Category Associated Products
        public CategoryAssociatedProductsListViewModel GetAllProducts(FilterCollection filters, SortCollection sorts, int? pageIndex, int? recordPerPage)
        {
            ReplaceFilterKeyName(ref filters, FilterKeys.Name, FilterKeys.ProductName);
            _productsClient.RefreshCache = true;
            var list = _productsClient.GetAllProducts(null, filters, sorts, pageIndex, recordPerPage);
            CategoryAssociatedProductsListViewModel modelList = ProductViewModelMap.ToListViewModel(list.ProductList, list.TotalResults);

            return !Equals(modelList, null) ? modelList : null;
        }
        #endregion

        #region Product Pricing Tier

        public ProductTierPricingViewModel GetProductTier(int productId, int productTierId)
        {
            _productsClient.RefreshCache = true;
            var productTier = (_productsClient.GetProduct(productId, new ExpandCollection() { ExpandKeys.Tiers }).Tiers.ToList()).Where(x => x.ProductTierId.Equals(productTierId)).FirstOrDefault();
            return ProductViewModelMap.ToProductTierViewModel(productTier);
        }

        public ProductTierPricingViewModel CreateProductTier(ProductTierPricingViewModel viewModel)
        {
            ProductTierPricingModel model = ProductViewModelMap.ToProductTierModel(viewModel);
            ProductTierPricingModel productTier = _productsClient.CreateProductTier(model);
            return ProductViewModelMap.ToProductTierViewModel(productTier);
        }

        public ProductTierPricingViewModel UpdateProductTier(ProductTierPricingViewModel viewModel)
        {
            ProductTierPricingModel model = ProductViewModelMap.ToProductTierModel(viewModel);
            ProductTierPricingModel productTier = _productsClient.UpdateProductTier(model);
            return ProductViewModelMap.ToProductTierViewModel(productTier);
        }

        public bool DeleteProductTier(int productTierId)
        {
            bool isDeleted = _productsClient.DeleteProductTier(productTierId);
            return isDeleted;
        }

        public ProductTierListViewModel GetProductTiers(int productId, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            if (!Equals(filters, null))
            {
                filters.Add(new FilterTuple(FilterKeys.ProductId, FilterOperators.Equals, productId.ToString()));
            }
            _productsClient.RefreshCache = true;
            TierListModel productTiers = _productsClient.GetProductTiers(productId, null, filters, sortCollection, pageIndex, recordPerPage);
            return ProductViewModelMap.ToProductTierListViewModel(productTiers, pageIndex, recordPerPage);
        }

        #endregion

        #region Digital Asset

        public DigitalAssetListViewModel GetDigitalAssets(int productId, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            filters.Add(FilterKeys.ProductId, FilterOperators.Equals, productId.ToString());
            _productsClient.RefreshCache = true;
            var digitalAssets = _productsClient.GetProductDigitalAssets(productId, null, filters, sortCollection, pageIndex, recordPerPage);
            return (Equals(digitalAssets, null) && (Equals(digitalAssets.DigitalAssets, null))) ? new DigitalAssetListViewModel() : ProductViewModelMap.ToDigitalAssetListViewModel(digitalAssets, pageIndex, recordPerPage);
        }

        public DigitalAssetViewModel AddDigitalAsset(DigitalAssetViewModel viewModel)
        {
            DigitalAssetModel model = ProductViewModelMap.ToDigitalAssetModel(viewModel);
            DigitalAssetModel digitalAsset = _productsClient.CreateDigitalAsset(model);
            return ProductViewModelMap.ToDigitalAssetViewModel(digitalAsset);
        }


        public bool DeleteDigitlAsset(int digitlAssetId, out string errorMessage)
        {
            try
            {
                errorMessage = string.Empty;
                return _productsClient.DeleteDigitlAsset(digitlAssetId);
            }
            catch (ZnodeException ex)
            {
                errorMessage = ex.ErrorMessage;
                return false;
            }
        }
        #endregion

        #region Sku Profile

        public SkuProfileEffectiveViewModel GetSkuProfileEffective(int skuProfileEffectiveId)
        {
            _skuProfileEffectiveClient.RefreshCache = true;
            SkuProfileEffectiveViewModel highlightViewModel = !Equals(skuProfileEffectiveId, null) ? SkuProfileEffectiveViewModelMap.ToViewModel(_skuProfileEffectiveClient.GetSkuProfileEffective(skuProfileEffectiveId)) : null;
            return highlightViewModel;
        }

        public bool CreateSkuProfileEffective(SkuProfileEffectiveViewModel model)
        {
            var skuProfileEffective = _skuProfileEffectiveClient.CreateSkuProfileEffective(SkuProfileEffectiveViewModelMap.ToModel(model));
            return (skuProfileEffective.SkuProfileEffectiveID > 0) ? true : false;
        }

        public bool UpdateSkuProfileEffective(SkuProfileEffectiveViewModel model)
        {
            var skuProfileEffective = _skuProfileEffectiveClient.UpdateSkuProfileEffective(model.SkuProfileEffectiveID, SkuProfileEffectiveViewModelMap.ToModel(model));
            return (!Equals(skuProfileEffective, null)) ? true : false;
        }

        public bool DeleteSkuProfileEffective(int skuProfileEffectiveID, out string errorMessgae)
        {
            errorMessgae = string.Empty;
            try
            {
                return _skuProfileEffectiveClient.DeleteSkuProfileEffective(skuProfileEffectiveID);
            }
            catch (ZnodeException ex)
            {
                errorMessgae = ex.ErrorMessage;
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public SkuProfileEffectiveListViewModel GetSkuProfileEffectivesBySkuId(int skuId, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            if (skuId > 0)
            {
                if (Equals(filters, null))
                {
                    filters = new FilterCollection();
                }
                filters.Add(new FilterTuple(FilterKeys.SkuId, FilterOperators.Equals, skuId.ToString()));
                _skuProfileEffectiveClient.RefreshCache = true;
                var list = _skuProfileEffectiveClient.GetSkuProfileEffectiveBySkuId(skuId, filters, null, pageIndex, recordPerPage);
                return Equals(list, null) ? null : SkuProfileEffectiveViewModelMap.ToListViewModel(list);
            }
            return new SkuProfileEffectiveListViewModel();
        }

        public List<SelectListItem> GetSkuProfilesList(int skuId, int skuEffectiveId = 0, int profileId = 0)
        {
            List<ProfileModel> profileList = _profilesAgent.GetAvailablePortalsBySkuIdCategoryId(skuId, 0, skuEffectiveId, 0);

            return ProductViewModelMap.ToSelectList(profileList, profileId);
        }

        #endregion

        #region Product Details
        public ProductViewModel CopyProduct(int productId)
        {
            ProductViewModel productViewModel = new ProductViewModel();
            if (productId > 0)
            {
                _productsClient.RefreshCache = true;
                productViewModel = ProductViewModelMap.ToViewModel(_productsClient.CopyProduct(productId));
            }
            return productViewModel;
        }
        #endregion

        #region Bind Dropdown for store
        public List<PortalModel> GetStoreList()
        {
            PortalListModel list = new PortalListModel();
            _portalsClient.RefreshCache = true;
            list = _portalsClient.GetPortals(null, null, null);
            return list.Portals.ToList();
        }
        #endregion

        #region Shopping Cart

        /// <summary>
        /// Get the product details by Ids
        /// </summary>
        /// <param name="ids">Product IDs</param>
        /// <returns>Returns the list of products as product view model</returns>
        public IEnumerable<ProductViewModel> GetProductByIds(string ids)
        {
            if (string.IsNullOrEmpty(ids))
            {
                return null;
            }
            Expands = new ExpandCollection();
            Expands.Add(ExpandKeys.Skus);
            Expands.Add(ExpandKeys.Attributes);
            Expands.Add(ExpandKeys.Promotions);
            IEnumerable<ProductViewModel> products = new Collection<ProductViewModel>();
            var idString = Equals((ids.Split(',').Length), 1) ? ids + "," : ids;
            _productsClient.RefreshCache = true;
            var list = _productsClient.GetProductsByProductIds(idString, Expands, new SortCollection());
            products = products.Union(ProductViewModelMap.ToViewModels(list.Products));
            return products;
        }

        /// <summary>
        /// Set the wishlist success and error message
        /// </summary>
        /// <param name="model">Product View Model</param>
        /// <param name="success">wishlist added or not</param>
        public void SetMessages(ProductViewModel model)
        {
            SetStatusMessage(model);
        }

        /// <summary>
        /// Get the product based on the product id
        /// </summary>
        /// <param name="productId">Product ID</param>
        /// <returns>Returns the product details as product view model</returns>
        public ProductViewModel GetProductDetails(int productId)
        {
            Expands = new ExpandCollection();
            Expands.Add(ExpandKeys.Skus);
            Expands.Add(ExpandKeys.Attributes);
            Expands.Add(ExpandKeys.Promotions);
            Expands.Add(ExpandKeys.Categories);
            Expands.Add(ExpandKeys.AddOns);
            Expands.Add(ExpandKeys.Reviews);
            Expands.Add(ExpandKeys.Images);
            Expands.Add(ExpandKeys.BundleItems);
            Expands.Add(ExpandKeys.FrequentlyBoughtTogether);
            Expands.Add(ExpandKeys.YouMayAlsoLike);

            _productsClient.RefreshCache = true;
            var product = _productsClient.GetProduct(productId, Expands);

            if (Equals(product, null))
            {
                return null;
            }
            // If the product is not active redirect to home page
            if (!product.IsActive)
            {
                return null;
            }

            var categories = _categoryAgent.GetCategories();

            // Check if it is this store product.
            var isStoreProduct = product.Categories.Any(x => categories.Categories.Any(y => Equals(x.CategoryId, y.CategoryId)));

            if (!isStoreProduct)
                return null;

            var productViewModel = ProductViewModelMap.ToDetailsViewModel(product);


            // Populate the product attributes
            if (productViewModel.ProductsAttributes.Attributes.Any())
            {
                int attributeId = productViewModel.ProductsAttributes.Attributes.FirstOrDefault().ProductAttributes.FirstOrDefault().AttributeId;
                string selectedSku = string.Empty;
                string imagePath = string.Empty;
                //ZNode Version 7.2.2 - Add imageMediumPath Out parameter
                string imageMediumPath = string.Empty;
                decimal price = MvcAdminUnits.ConvertCurrencyToDecimalValue(productViewModel.ProductPrice.ToString());
                productViewModel.UnitPrice = _skuAgent.GetSkusPrice(productId, attributeId, price, out selectedSku, out imagePath, out imageMediumPath);
                productViewModel.Sku = selectedSku;

                productViewModel.ProductsAttributes = UpdateProductAttributes(productViewModel, attributeId, string.Empty);

                if (!Equals(Path.GetFileName(imagePath), MvcAdminConstants.NoImageName))
                {
                    productViewModel.ImageLargePath = imagePath;
                }

            }

            // Check the inventory
            if (!productViewModel.IsCallForPricing)
            {
                IEnumerable<int> attributeId = new List<int>();

                if (productViewModel.ProductsAttributes.Attributes.Any())
                {
                    if (productViewModel.ProductsAttributes.Attributes.First().ProductAttributes.Any())
                        attributeId = productViewModel.ProductsAttributes.Attributes.Select(x => x.ProductAttributes.First(y => y.Available).AttributeId);
                }

                CheckInventory(productViewModel, string.Join(",", attributeId), productViewModel.SelectedQuantity);
            }
            else
            {
                productViewModel.InventoryMessage = MvcAdminConstants.CallForPricing;
                productViewModel.ShowAddToCart = false;
            }

            // Populate the product add-on
            if (!Equals(productViewModel.AddOns, null) && productViewModel.AddOns.Any())
            {
                SetAddOnPrice(productViewModel);

                decimal finalProductPrice;
                productViewModel.AddOns = GetAddOnDetails(productViewModel, string.Join(",", productViewModel.AddOns.Where(x => !Equals(x.SelectedAddOnValue, null)).SelectMany(x => x.SelectedAddOnValue)), 1, productViewModel.ProductPrice, out finalProductPrice);

                productViewModel.OriginalPrice = finalProductPrice;
            }

            // For Displaying the category name in the product page
            var categoryName = GetFromSession<string>(MvcAdminConstants.LabelCategoryName);

            productViewModel.CategoryName = !string.IsNullOrEmpty(categoryName) ? categoryName : product.Categories.FirstOrDefault().Name;


            productViewModel.Title =
                 (!string.IsNullOrEmpty(product.SeoTitle)) ? product.SeoTitle : product.Name;

            return productViewModel;
        }

        /// <summary>
        /// Get the product by IDs
        /// </summary>
        /// <param name="ids">Product IDs</param>
        /// <returns>Returns the list of product as product model</returns>
        public Collection<ProductModel> GetProductsByIds(List<string> ids)
        {
            var productIds = ids.Distinct().ToList();
            _productsClient.RefreshCache = true;
            if (productIds.Count() == 1)
            {
                var singleProduct = _productsClient.GetProduct(int.Parse(productIds.First()), expands);
                return new Collection<ProductModel>() { singleProduct };
            }

            IEnumerable<ProductModel> products = new Collection<ProductModel>();

            var page = 1;
            const int pagesize = 10;
            do
            {
                var currentProductIds = productIds.Skip((page - 1) * pagesize).Take(pagesize).ToList();

                if (currentProductIds.Count() == 1)
                {
                    var singleProduct = _productsClient.GetProduct(int.Parse(currentProductIds.First()), expands);
                    products = products.Union(new Collection<ProductModel>() { singleProduct });
                }
                else
                {
                    var idString = String.Join(",", currentProductIds);
                    var list = _productsClient.GetProductsByProductIds(idString, expands, new SortCollection());
                    products = products.Union(list.Products);
                }

                page++;
            } while (page <= Math.Ceiling(productIds.Count() / (decimal)pagesize));

            return new Collection<ProductModel>(productIds.Select(x => products.FirstOrDefault(y => y.ProductId.ToString() == x)).ToList());
        }

        /// <summary>
        /// Update product attributes availablity based on selected attributes.
        /// </summary>
        /// <param name="product"></param>
        /// <param name="attributeId"></param>
        /// <param name="selectedAttributeIds"></param>
        /// <returns></returns>
        public ProductAttributesViewModel UpdateProductAttributes(ProductViewModel product, int? attributeId, string selectedAttributeIds)
        {
            // to keep already selected attribute item
            var attributeIds = selectedAttributeIds.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries)
                                    .Select(x => int.Parse(x));

            // select all the dropdowns ids that are selected, 
            var selectedAttributes = attributeIds.TakeWhile(x => !Equals(x, attributeId)).Union(new List<int> { attributeId.GetValueOrDefault(0) });

            // Get distinct SKU Ids for selected attribute dropdown values.
            var selectedAttributeSkuIds = product.ProductsAttributes.Attributes.Select(attributeType =>
                {
                    var attributeViewModel =
                        attributeType.ProductAttributes.FirstOrDefault(
                            attribute => selectedAttributes.Any(x => Equals(x, attribute.AttributeId)));

                    if (!Equals(attributeViewModel, null))
                        return new { attributeType.AttributeTypeId, attributeViewModel.SkuIds };

                    // return empty collction if no attribute found.
                    return null;

                }).Where(x => !Equals(x, null)).Distinct();

            // Update "Available" and "Selected" flag only based on SKU Id combination that is based on selected dropdown options. 
            // e.g. 2nd dropdown should populate 1st dropdown selection, 3rd dropdown based on 2nd dropdown selection.
            // if 1st dropdown selected, then it will populate all other dropdowns based on first dropdown only.
            // if 2nd dropdown selected, 
            //      then 1st dropdown will populate as it is, 
            //      then 2nd dropdown will populate based on 1st dropdown selection only. 
            //      all other dropdowns based on combination of 1st and 2nd dropdown dropdown only.

            // Loop all attribute types and its attributes to set Available and Selected flag value.
            var toBeFiltered = false;
            foreach (var attributeType in product.ProductsAttributes.Attributes)
            {
                attributeType.ToBeFiltered = toBeFiltered;
                foreach (var attribute in attributeType.ProductAttributes)
                {
                    // select all dropdown values upto currently selected one.
                    var skuIds = selectedAttributeSkuIds.TakeWhile(x => !Equals(x.AttributeTypeId, attribute.AttributeTypeId));

                    // set first one if not.
                    if (!skuIds.Any())
                        skuIds = skuIds.Union(new[] { selectedAttributeSkuIds.FirstOrDefault() });

                    // Set available flag only it matches all the selected dropdown values
                    attribute.Available = skuIds.All(x => x.SkuIds.Any(v => attribute.SkuIds.Any(skuid => Equals(v, skuid))));

                    // Set the selected flag to repopulate to all dropdowns.
                    attribute.Selected = attributeIds.Any(attId => Equals(attId, attribute.AttributeId));
                }

                toBeFiltered = true;

                // Set which is currently selected dropdown.
                attributeType.IsSelectedOption = attributeType.ProductAttributes.Any(x => Equals(x.AttributeId, attributeId));
            }

            return product.ProductsAttributes;
        }

        /// <summary>
        /// Check Inventory for the selected product sku and displays the error message based on Quantity on hand.
        /// </summary>
        /// <param name="model">Product Model</param>
        /// <param name="attributeId">Selected attribute id</param>
        public void CheckInventory(ProductViewModel model, string attributeId, int? quantity)
        {
            var selectedSku = _skuAgent.GetSkuInventory(model.ProductId, attributeId);

            if (Equals(selectedSku, null))
            {
                model.InventoryMessage = ZnodeResources.SelectAttributeTypeError; ;
                model.ShowAddToCart = false;
                return;
            }

            if (!Equals(selectedSku, null))
            {
                int selectedQuantity = quantity.HasValue && quantity.Value > 0 ? quantity.Value : model.MinQuantity.GetValueOrDefault();

                int cartQuantity = GetOrderedItemQuantity(selectedSku.Sku);

                int combinedQuantity = selectedQuantity + cartQuantity;

                if (selectedSku.Inventory.QuantityOnHand < combinedQuantity && (!model.AllowBackOrder.GetValueOrDefault()) && model.TrackInventory.GetValueOrDefault())
                {
                    model.InventoryMessage = !string.IsNullOrEmpty(model.OutOfStockMessage) ? model.OutOfStockMessage : string.Format(ZnodeResources.TextErrorFormat, ZnodeResources.OutOfStockError);
                    model.ShowAddToCart = false;
                    return;
                }
                else if (selectedSku.Inventory.QuantityOnHand < combinedQuantity && model.AllowBackOrder.GetValueOrDefault() && model.TrackInventory.GetValueOrDefault())
                {
                    model.InventoryMessage = !string.IsNullOrEmpty(model.BackOrderMessage) ? model.BackOrderMessage : ZnodeResources.TextBackOrderMessage;
                    model.ShowAddToCart = true;
                    return;
                }

                if (combinedQuantity > model.MaxQuantity)
                {
                    model.InventoryMessage = string.Format(ZnodeResources.TextErrorFormat,
                        ZnodeResources.InsufficientQuantityError);
                    model.ShowAddToCart = false;
                    return;
                }
            }
            model.InventoryMessage = !string.IsNullOrEmpty(model.InStockMessage) ? model.InStockMessage : ZnodeResources.InstockMessage;
            model.ShowAddToCart = true;
        }

        /// <summary>
        /// Sets the addon price for the addons
        /// </summary>
        /// <param name="productViewModel"></param>
        private void SetAddOnPrice(ProductViewModel productViewModel)
        {
            productViewModel.AddOns.ToList().ForEach(x => x.AddOnValues.ToList().ForEach(y => y.AddOnFinalPrice = GetAddOnPrice(y)));
        }

        /// <summary>
        /// Gets the addon prices for an addonvalue
        /// </summary>
        /// <param name="addOnValuesViewModel"></param>
        /// <returns></returns>
        private decimal GetAddOnPrice(AddOnValueViewModel addOnValuesViewModel)
        {
            decimal finalPrice = 0;
            finalPrice = addOnValuesViewModel.RetailPrice;

            if (addOnValuesViewModel.SalePrice.HasValue)
            {
                finalPrice = addOnValuesViewModel.SalePrice.GetValueOrDefault(0);
            }

            if (System.Web.HttpContext.Current.User.Identity.IsAuthenticated && addOnValuesViewModel.WholesalePrice.HasValue)
            {
                var accountViewModel = _accountAgent.GetAccountViewModel();

                if (!Equals(accountViewModel, null) && accountViewModel.UseWholeSalePricing)
                {
                    finalPrice = addOnValuesViewModel.WholesalePrice.GetValueOrDefault(0);
                }
            }

            return finalPrice;
        }

        /// <summary>
        /// Get the add on details
        /// </summary>
        /// <param name="product"></param>
        /// <param name="selectedAddOnIds"></param>
        /// <param name="quantity"></param>
        /// <param name="productPrice"></param>
        /// <param name="finalProductPrice"></param>
        /// <returns></returns>
        public IEnumerable<AddOnViewModel> GetAddOnDetails(ProductViewModel product, string selectedAddOnIds, int? quantity, decimal productPrice, out decimal finalProductPrice, string selectedSku = null)
        {
            // To DO Check the inventory for the addon
            CheckAddOnInventory(product, selectedAddOnIds, quantity.GetValueOrDefault(1), selectedSku);

            finalProductPrice = productPrice;
            string[] selectedAddOn = selectedAddOnIds.Split(',');

            foreach (var addonvalueId in selectedAddOn)
            {
                if (!string.IsNullOrEmpty(addonvalueId))
                {
                    var addOnValuesViewModel =
                        product.AddOns.SelectMany(
                            y => y.AddOnValues.Where(x => Equals(x.AddOnValueId, int.Parse(addonvalueId)))).FirstOrDefault();
                    if (Equals(addOnValuesViewModel, null)) continue;
                    var price = addOnValuesViewModel.AddOnFinalPrice;
                    finalProductPrice += price * quantity.GetValueOrDefault(1);
                }
            }

            return product.AddOns;
        }

        /// <summary>
        /// Get ordered items quantity by the given sku.
        /// </summary>
        /// <param name="sku"></param>
        /// <returns></returns>
        public int GetOrderedItemQuantity(string sku)
        {
            var cart = GetFromSession<ShoppingCartModel>(MvcAdminConstants.CartModelSessionKey);
            var cartQuantity = default(int);

            if (!Equals(cart, null) && cart.ShoppingCartItems.Any(model => model.Sku.Equals(sku)))
            {
                var shoppingCartItemModel = cart.ShoppingCartItems.FirstOrDefault(model => model.Sku.Equals(sku));
                if (!Equals(shoppingCartItemModel, null))
                    cartQuantity = shoppingCartItemModel.Quantity;
            }
            return cartQuantity;
        }

        /// <summary>
        /// Check Inventory for the selected addon and displays the error message based on Quantity on hand.
        /// </summary>
        /// <param name="model">Product Model</param>
        /// <param name="selectedAddOnId">Selected addon id</param>
        /// <param name="quantity"></param>
        public void CheckAddOnInventory(ProductViewModel model, string selectedAddOnId, int? quantity, string selectedSku = null)
        {
            string[] selectedAddOn = selectedAddOnId.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);

            foreach (var addOn in model.AddOns)
            {
                addOn.IsOutOfStock = false;
                addOn.OutOfStockMessage = string.Empty;
            }

            foreach (var addonvalueId in selectedAddOn)
            {
                AddOnViewModel addOn = null;
                if (!string.IsNullOrEmpty(addonvalueId))
                {
                    addOn =
                        model.AddOns.FirstOrDefault(
                            x => x.AddOnValues.Any(y => Equals(y.AddOnValueId, int.Parse(addonvalueId))));
                }

                if (!Equals(addOn, null))
                {
                    var addOnValue = addOn.AddOnValues.FirstOrDefault(y => Equals(y.AddOnValueId, int.Parse(addonvalueId)));

                    if (!Equals(addOnValue, null))
                    {
                        var selectedQuantity = quantity.HasValue && quantity.Value > 0 ? quantity.Value : model.MinQuantity;

                        var cartQuantity = GetOrderedItemQuantity(selectedSku, Convert.ToInt32(addonvalueId));

                        var combinedQuantity = selectedQuantity + cartQuantity;

                        if (combinedQuantity > model.MaxQuantity)
                        {
                            addOn.OutOfStockMessage = string.Format(ZnodeResources.TextErrorFormat,
                                ZnodeResources.InsufficientQuantityError);
                            addOn.IsOutOfStock = true;
                            model.ShowAddToCart = false;
                            return;
                        }

                        if (addOnValue.QuantityOnHand < combinedQuantity && (!addOn.AllowBackOrder) && addOn.TrackInventory)
                        {
                            addOn.OutOfStockMessage = string.Format(ZnodeResources.TextAddOnOutofStock, addOn.Name);
                            addOn.IsOutOfStock = true;
                            model.InventoryMessage = string.Empty;
                            model.ShowAddToCart = false;
                            return;
                        }
                        else if (addOnValue.QuantityOnHand < combinedQuantity && addOn.AllowBackOrder && addOn.TrackInventory)
                        {
                            addOn.OutOfStockMessage = !string.IsNullOrEmpty(addOn.BackOrderMessage) ? addOn.BackOrderMessage : ZnodeResources.TextBackOrderMessage;
                            model.ShowAddToCart = true;
                            return;
                        }

                        if (model.MaxQuantity < combinedQuantity && model.AllowBackOrder.GetValueOrDefault() && model.TrackInventory.GetValueOrDefault())
                        {
                            addOn.OutOfStockMessage = string.Format(ZnodeResources.TextErrorFormat,
                            ZnodeResources.InsufficientQuantityError);
                            model.InventoryMessage = string.Empty;
                            model.ShowAddToCart = false;
                            return;
                        }

                        if (!Equals(model.InventoryMessage, ZnodeResources.OutOfStockError) && !Equals(model.InventoryMessage, model.OutOfStockMessage) && !model.IsCallForPricing && combinedQuantity <= model.MaxQuantity)
                        {
                            addOn.OutOfStockMessage = !string.IsNullOrEmpty(addOn.InStockMessage) ? addOn.InStockMessage : ZnodeResources.InstockMessage;
                            model.ShowAddToCart = true;
                        }
                    }
                }
            }

            if (string.IsNullOrEmpty(model.InventoryMessage) && model.ShowAddToCart)
            {
                model.InventoryMessage = !string.IsNullOrEmpty(model.InStockMessage) ? model.InStockMessage : ZnodeResources.InstockMessage;
                model.ShowAddToCart = true;
            }
        }

        public void DownloadFile(CustomerBasedPricingListViewModel model, HttpResponseBase response, string fileName)
        {
            DownloadHelper downloadHelper = new DownloadHelper();
            downloadHelper.Download(model.CustomerBasedPricing, Convert.ToString(Convert.ToInt32(FileTypes.CSV)), response, null, fileName);
        }

        public void SetInventorySettings(ProductViewModel model)
        {
            if (!Equals(model.AllowBackOrder, null) && !Equals(model.TrackInventory, null))
            {
                if (Equals(model.AllowBackOrder, false) && Equals(model.TrackInventory, true))
                {
                    model.DisablePurchasingForOutOfStockProducts = true;
                    model.SelectedInventoryOptionsState = InventoryOptionsState.DisablePurchasingForOutOfStockProducts;
                }
                else if (Equals(model.AllowBackOrder, true) && Equals(model.TrackInventory, true))
                {
                    model.AllowBackOrderingOfProducts = true;
                    model.SelectedInventoryOptionsState = InventoryOptionsState.AllowBackOrderingOfProducts;
                }
                else if (Equals(model.AllowBackOrder, false) && Equals(model.TrackInventory, false))
                {
                    model.DontTrackInventory = true;
                    model.SelectedInventoryOptionsState = InventoryOptionsState.DontTrackInventory;
                }
            }
        }

        public void GetInventorySettings(ProductViewModel model)
        {
            switch (model.SelectedInventoryOptionsState)
            {
                case InventoryOptionsState.DisablePurchasingForOutOfStockProducts:
                    model.DisablePurchasingForOutOfStockProducts = true;
                    break;
                case InventoryOptionsState.AllowBackOrderingOfProducts:
                    model.AllowBackOrderingOfProducts = true;
                    break;
                case InventoryOptionsState.DontTrackInventory:
                    model.DontTrackInventory = true;
                    break;
                default:
                    break;
            }

            if (!Equals(model.DisablePurchasingForOutOfStockProducts, null) && !Equals(model.AllowBackOrderingOfProducts, null) && !Equals(model.DontTrackInventory, null))
            {
                if (Equals(model.DisablePurchasingForOutOfStockProducts, true))
                {
                    model.AllowBackOrder = false;
                    model.TrackInventory = true;
                }
                else if (Equals(model.AllowBackOrderingOfProducts, true))
                {
                    model.AllowBackOrder = true;
                    model.TrackInventory = true;
                }
                else if (Equals(model.DontTrackInventory, true))
                {
                    model.AllowBackOrder = false;
                    model.TrackInventory = false;
                }
            }

        }

        public void SetDefaultValues(ProductViewModel model)
        {
            model.IsActive = (Equals(model.UserType, Constant.RoleVendor)) ? model.IsActive : true;
            model.ReviewStateId = (int)ZNodeProductReviewState.Approved;
            model.FreeShipping = Equals(model.FreeShipping, null) ? false : true;
        }

        public int GetProductIdFromFormCollection(FilterCollectionDataModel formModel)
        {
            int productId = 0;
            foreach (var x in formModel.Params)
            {
                if (x.Key.ToLower().Equals("productid"))
                {
                    productId = int.Parse(x.Value);
                }
            }
            return productId;
        }

        public int GetSkuIdFromFormCollection(FilterCollectionDataModel formModel)
        {
            int productId = 0;
            foreach (var x in formModel.Params)
            {
                if (x.Key.ToLower().Equals("skuid"))
                {
                    productId = int.Parse(x.Value);
                }
            }
            return productId;
        }

        public void SetNoImageForSkuProperty(SkuViewModel model)
        {
            if (!Equals(model.SkuImage, null))
            {
                model.SKUPicturePath = (!Equals(model.SkuImage, null)) ? model.SkuImage.FileName : null;
            }
            else
            {
                model.SKUPicturePath = model.SKUPicturePath;
            }
            model.SKUPicturePath = model.NoImage ? null : model.SKUPicturePath;
        }

        public List<BundleDisplayViewModel> GetBundles(string bundleProductIds)
        {
            if (!Equals(bundleProductIds, null))
            {
                var items = bundleProductIds.Split(',');
                var model = GetProductsByIds(items.ToList());

                if (model != null && model.Count > 0)
                {
                    var activeBundleProducts = model.Where(x => x.IsActive);

                    if (model.Count == activeBundleProducts.Count())
                    {
                        return ProductViewModelMap.GetBundleDisplayViewModel(model);
                    }
                }
            }

            return null;
        }

        public ProductViewModel GetAttributes(int? id, int? attributeId, string selectedIds, int? quantity, out decimal prodPrice, out string selectedSku, out string imagePath, out string imageMediumPath)
        {
            var product = GetProductDetails(id.Value);

            // update product attributes based on selection.
            product.ProductsAttributes = UpdateProductAttributes(product, attributeId, selectedIds);

            var updatedSelectedIds = product.ProductsAttributes.Attributes.Select(x =>
            {
                var attributeViewModel =
                    x.ProductAttributes.FirstOrDefault(y => (y.Available && y.Selected));

                if (attributeViewModel == null)
                    attributeViewModel = x.ProductAttributes.FirstOrDefault(y => (y.Available));

                return attributeViewModel != null ? attributeViewModel.AttributeId : 0;
            }).Where(x => x > 0);

            if (!(product.IsCallForPricing))
            {
                CheckInventory(product, string.Join(",", updatedSelectedIds), quantity.GetValueOrDefault(1));
            }
            //ZNode Version 7.2.2 - Add imageMediumPath Out parameter
            prodPrice = _skuAgent.GetSkusPrice(id.Value, attributeId.GetValueOrDefault(0), product.ProductPrice, out  selectedSku, out imagePath, out imageMediumPath);

            prodPrice = GetExtendedPrice(id.Value, quantity.GetValueOrDefault(1), selectedSku, prodPrice);

            //Set Product Main Image Path in case no image found.
            if (Path.GetFileName(imagePath).Equals(MvcAdminConstants.NoImageName))
            {
                imagePath = product.ImageLargePath;
                imageMediumPath = product.ImageMediumPath;
            }
            return product;
        }

        public decimal GetExtendedPrice(int productId, int quantity, string sku, decimal prodPrice)
        {
            var cartModel = new ShoppingCartModel();
            cartModel.ShoppingCartItems.Add(CartItemViewModelMap.ToShoppingCartModel(productId, quantity, sku));

            var shoppingCartModel = _shoppingCartsClient.Calculate(cartModel);

            // Check if item exists after API call.
            if (shoppingCartModel != null && shoppingCartModel.ShoppingCartItems.Any())
            {
                var item = shoppingCartModel.ShoppingCartItems.FirstOrDefault(x => x.ProductId == productId);
                if (item == null) return 0;
                return item.ExtendedPrice;
            }
            return quantity * prodPrice;
        }

        public BundleDetailsViewModel GetCartModel(ProductViewModel model)
        {
            BundleDetailsViewModel bundle = new BundleDetailsViewModel();
            if (!Equals(model, null))
            {
                CartItemViewModel cartItem = new CartItemViewModel();
                cartItem.BundleItemsIds = model.BundleItemsIds;
                cartItem.ProductId = model.ProductId;
                cartItem.Quantity = 1;
                cartItem.AddOnValueIds = !Equals(model.AddOns, null) ? string.Join(",", model.AddOns.Select(x => x.AddOnValues.FirstOrDefault().AddOnValueId)) : string.Empty;
                cartItem.Sku = model.Sku;
                bundle = GetBundleProductsDetails(cartItem);
            }
            return bundle;
        }

        public BundleDetailsViewModel GetBundleProductsDetails(CartItemViewModel cartItemViewModel)
        {
            var bundleViewModel = new BundleDetailsViewModel();
            bundleViewModel.SelectedAttributesIds = cartItemViewModel.AttributeIds;
            bundleViewModel.SelectedProductId = cartItemViewModel.ProductId;
            bundleViewModel.SelectedQuantity = cartItemViewModel.Quantity;
            bundleViewModel.SelectedAddOnValueIds = cartItemViewModel.AddOnValueIds;
            bundleViewModel.SelectedSku = cartItemViewModel.Sku;
            bundleViewModel.ProductName = cartItemViewModel.ProductName;

            var items = cartItemViewModel.BundleItemsIds.Split(',');

            var productsModel = this.GetProductsByIds(items.ToList());

            if (productsModel != null && productsModel.Count > 0)
            {
                bundleViewModel.Bundles = productsModel.Where(x => x.IsActive).Select(ProductViewModelMap.ToDetailsViewModel).ToList();

                foreach (var bundle in bundleViewModel.Bundles)
                {
                    bundle.ShowAddToCart = true;
                    IEnumerable<int> attributeIds = new List<int>();

                    // Initial Load of the dropdown
                    if (bundle.ProductsAttributes.Attributes.Any())
                    {
                        int attributeId =
                            bundle.ProductsAttributes.Attributes.FirstOrDefault()
                                  .ProductAttributes.FirstOrDefault()
                                  .AttributeId;


                        string selectedSku = string.Empty;

                        bundle.Sku = selectedSku;
                        bundle.ProductsAttributes = UpdateProductAttributes(bundle, attributeId, string.Empty);
                        attributeIds = bundle.ProductsAttributes.Attributes.Select(x => x.ProductAttributes.First(y => y.Available).AttributeId);
                    }

                    CheckInventory(bundle, string.Join(",", attributeIds), bundle.SelectedQuantity);

                    if (bundle.AddOns != null && bundle.AddOns.Any())
                    {
                        var selectedAddOnValue = string.Join(",", bundle.AddOns.Where(x => x.SelectedAddOnValue != null).SelectMany(x => x.SelectedAddOnValue).ToArray());
                        CheckAddOnInventory(bundle, selectedAddOnValue, bundle.SelectedQuantity);
                    }

                    bundle.ShowQuantity = false;
                }
            }

            return bundleViewModel;
        }

        public int GetOrderedItemQuantity(string selectedSku, int addonValueId)
        {
            var cart = GetFromSession<ShoppingCartModel>(MvcAdminConstants.CartModelSessionKey);
            var cartQuantity = default(int);

            if (!Equals(cart, null) && !Equals(cart.ShoppingCartItems, null))
            {
                return (
                    from ShoppingCartItemModel item in cart.ShoppingCartItems
                    where !Equals(item, null) && !Equals(item.AddOnValueIds, null)
                    where item.Sku == selectedSku
                    from int itemAddOn in item.AddOnValueIds
                    where itemAddOn == addonValueId
                    select item.Quantity
                    ).Sum();
            }
            return cartQuantity;
        }

        #endregion

        //PRFT Custom Code:Start
        /// <summary>
        /// To get Product Inventory list from ERP
        /// </summary>
        /// <returns></returns>
        public PRFTInventoryListViewModel GetInventoryListFromERP(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {

            if (Equals(filters, null))
            {
                filters = new FilterCollection();
            }

            filters.Add(new FilterTuple(FilterKeys.Username, FilterOperators.Equals, string.Format("'{0}'", HttpContext.Current.User.Identity.Name)));
            var list = _productsClient.GetInventoryFromERP(null, filters, sortCollection, pageIndex, recordPerPage);
            if (Equals(list, null))
            {
                return null;
            }
            return PRFTInventoryViewModelMap.ToListViewModel(list);
            
        }
        //PRFT Custom Code:End
    }
}