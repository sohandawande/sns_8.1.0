﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public partial class ManufacturersAgent : BaseAgent, IManufacturersAgent
    {
        public ManufacturersViewModel CreateManufacturer(ManufacturersViewModel model)
        {
            ManufacturerModel manufacturerModel = _manufacturerClient.CreateManufacturer(ManufacturersViewModelMap.ToModel(model));

            return manufacturerModel != null ? ManufacturersViewModelMap.ToViewModel(manufacturerModel) : null;
        }
    }
}