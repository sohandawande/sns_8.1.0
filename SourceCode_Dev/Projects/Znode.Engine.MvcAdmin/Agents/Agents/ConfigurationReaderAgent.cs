﻿using System;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Models.Enum;

namespace Znode.Engine.MvcAdmin.Agents
{
    public class ConfigurationReaderAgent : BaseAgent, IConfigurationReaderAgent
    {
        #region Private Variables
        private readonly IConfigurationReaderClient _configurationReader;

        #endregion

        #region Constructor
        public ConfigurationReaderAgent()
        {
            _configurationReader = GetClient<ConfigurationReaderClient>();
        }

        #endregion

        #region Public Methods
        public string GetFilterConfigurationXML(int listID)
        {
            string itemName = Convert.ToString((ListType)listID);
            return _configurationReader.GetFilterConfigurationXML(itemName);
        }
        #endregion

    }
}