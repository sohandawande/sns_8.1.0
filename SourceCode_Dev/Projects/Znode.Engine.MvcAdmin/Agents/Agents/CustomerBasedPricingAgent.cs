﻿using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public class CustomerBasedPricingAgent : BaseAgent, ICustomerBasedPricingAgent
    {
        #region Private Variables
        private readonly ICustomerBasedPricingClient _customerBasedPricingClient;
        #endregion

        #region Public Constructor
        public CustomerBasedPricingAgent()
        {
            _customerBasedPricingClient = GetClient<CustomerBasedPricingClient>();
        }
        #endregion

        #region Public Methods
        public CustomerBasedPricingListViewModel GetCustomerPricingList(int productId, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            filters.Add(new FilterTuple(FilterKeys.ProductId, FilterOperators.Equals, productId.ToString()));
            
            ReplaceFilterKeyName(ref filters, FilterKeys.ExternalAccountId, FilterKeys.ExternalAccountNo);
            ReplaceSortKeyName(sortCollection, FilterKeys.ExternalAccountId, FilterKeys.ExternalAccountNo);

            CustomerBasedPricingListModel cutomerBasedPricingList = _customerBasedPricingClient.GetCustomerBasedPricing(productId, null, filters, sortCollection, pageIndex, recordPerPage);
            if (!Equals(cutomerBasedPricingList, null))
            {
                cutomerBasedPricingList.ProductId = productId;
                return CustomerBasedPricingViewModelMap.ToListViewModel(cutomerBasedPricingList, pageIndex, recordPerPage, cutomerBasedPricingList.TotalPages, cutomerBasedPricingList.TotalResults);
            }
            return null;
        }
        #endregion
    }
}