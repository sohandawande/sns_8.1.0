﻿using Resources;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public class EmailTemplateAgent : BaseAgent, IEmailTemplateAgent
    {
        #region Private Variables
        private readonly IEmailTemplateClient _emailTemplateClient;
        #endregion

        #region Constructors

        /// <summary>
        /// Constructor for Email Template.
        /// </summary>
        public EmailTemplateAgent()
        {
            _emailTemplateClient = GetClient<EmailTemplateClient>();
        }

        #endregion

        #region Public methods
        public EmailTemplateListViewModel GetTemplateList(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            _emailTemplateClient.RefreshCache = true;
            EmailTemplateListModel list = _emailTemplateClient.GetTemplates(filters, sortCollection, pageIndex - 1, recordPerPage);
            return EmailTemplateViewModelMap.ToListViewModel(list);
        }

        public bool AddEmailTemplate(EmailTemplateViewModel model, out string errorMessage)
        {
            errorMessage = string.Empty;
            try
            {
                if (!Equals(model, null))
                {
                    //To check whether template is newly created or not, true if newly created.
                    if (!model.IsNewAdd)
                    {
                        // key contains deleted template name.
                        model.TemplateName = model.Key;
                    }
                    EmailTemplateModel emailTemplate = _emailTemplateClient.CreateTemplatePage(EmailTemplateViewModelMap.ToModel(model));
                    return true;
                }
            }
            catch (ZnodeException ex)
            {
                errorMessage = ex.ErrorMessage;
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
            return false;
        }

        public EmailTemplateViewModel GetTemplatePage(string templateName, string extension)
        {
            EmailTemplateViewModel model = new EmailTemplateViewModel();
            _emailTemplateClient.RefreshCache = true;
            if (!string.IsNullOrEmpty(templateName) && !string.IsNullOrEmpty(extension))
            {

                EmailTemplateModel templatePage = _emailTemplateClient.GetTemplatePage(templateName, extension);
                
                if (!Equals(templatePage, null))
                {
                    templatePage.Extension = extension;
                    model = EmailTemplateViewModelMap.ToViewModel(templatePage);

                }
            }
            return model;
        }

        public bool UpdateTemplatePage(EmailTemplateViewModel model, out string errorMessage)
        {
            errorMessage = string.Empty;
            try
            {
                return _emailTemplateClient.UpdateTemplatePage(EmailTemplateViewModelMap.ToModel(model));
            }
            catch (ZnodeException ex)
            {
                errorMessage = ex.ErrorMessage;
                return false;
            }
        }

        public bool DeleteTemplatePage(string templateName, string extension, out string errorMessage)
        {
            errorMessage = string.Empty;
            try
            {
                return _emailTemplateClient.DeleteTemplatePage(templateName, extension);
            }
            catch (ZnodeException ex)
            {
                errorMessage = ex.ErrorMessage;
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public List<SelectListItem> GetAllDeletedTemplates()
        {
            List<SelectListItem> lstTemplateKeys = new List<SelectListItem>();

            _emailTemplateClient.RefreshCache = true;
            //Get all the template list.
            EmailTemplateListModel templateList = _emailTemplateClient.GetDeletedTemplates(null, Filters, null);

            if (!Equals(templateList, null) && !Equals(templateList.EmailTemplates, null) && templateList.EmailTemplates.Count > 0)
            {
                lstTemplateKeys = (from item in templateList.EmailTemplates
                                 select new SelectListItem
                                 {
                                     Text = item.TemplateName.ToString(),
                                     Value = item.DeleteTemplate.ToString()
                                 }).ToList();
            }
            //Insert default items in Location drop down. 
            if (lstTemplateKeys.Count > 0)
            {
                lstTemplateKeys.Insert(0, new SelectListItem { Text = ZnodeResources.LabelSelectTemplates, Value = MvcAdminConstants.SelectTemplatesValue });
                lstTemplateKeys.Add(new SelectListItem { Text = ZnodeResources.LabelAddNewTemplate, Value = MvcAdminConstants.AddNewTemplateValue });
            }

            return lstTemplateKeys;
        }

        public EmailTemplateViewModel GetHtmlContent(string templateName)
        {
            string extension = Path.GetExtension(templateName).Replace(MvcAdminConstants.Dot, string.Empty);
            string fileName = Path.GetFileNameWithoutExtension(templateName);

            EmailTemplateViewModel model = new EmailTemplateViewModel();
            _emailTemplateClient.RefreshCache = true;
            if (!string.IsNullOrEmpty(templateName))
            {
                EmailTemplateModel templatePage = _emailTemplateClient.GetTemplatePage(fileName, extension);

                if (!Equals(templatePage, null))
                {
                    model = EmailTemplateViewModelMap.ToViewModel(templatePage);
                }
            }
            return model;
        }
        #endregion
    }
}