﻿using System;
using System.Web;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    /// <summary>
    /// Agent 
    /// </summary>
    public class RejectionMessageAgent : BaseAgent, IRejectionMessageAgent
    {
        #region Private Variables
        private readonly IRejectionMessageClient _rejectionMessageClient;
        #endregion

        #region Constructor
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public RejectionMessageAgent()
        {
            _rejectionMessageClient = GetClient<RejectionMessageClient>();
        }
        #endregion

        #region Public Methods

        public RejectionMessageListViewModel GetRejectionMessages(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            RejectionMessageListModel list = new RejectionMessageListModel();

            if (Equals(filters, null))
            {
                filters = new FilterCollection();
            }

            filters.Add(new FilterTuple(FilterKeys.Username, FilterOperators.Equals, "\'" + HttpContext.Current.User.Identity.Name + "\'"));

            list = _rejectionMessageClient.GetRejectionMessages(new ExpandCollection { ExpandKeys.AddOns }, filters, sortCollection, pageIndex, recordPerPage);
            return Equals(list, null) ? new RejectionMessageListViewModel() : RejectionMessageViewModelMap.ToListViewModel(list);
        }

        public RejectionMessageViewModel GetRejectionMessage(int rejectionMessageId)
        {
            RejectionMessageModel rejectionMessageModel = _rejectionMessageClient.GetRejectionMessage(rejectionMessageId);
            return (!Equals(rejectionMessageModel, null)) ? RejectionMessageViewModelMap.ToViewModel(rejectionMessageModel) : null;
        }

        public bool CreateRejectionMessage(RejectionMessageViewModel model, out string errorMessage)
        {
            errorMessage = string.Empty;
            RejectionMessageModel rejectionMessageModel = new RejectionMessageModel();
            try
            {
                model.LocaleId = MvcAdminConstants.LocaleId;
                rejectionMessageModel = _rejectionMessageClient.CreateRejectionMessage(RejectionMessageViewModelMap.ToModel(model));
                return (!Equals(rejectionMessageModel, null) && (rejectionMessageModel.RejectionMessagesId > 0));
            }
            catch (ZnodeException ex)
            {
                errorMessage = ex.ErrorMessage;
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool UpdateRejectionMessage(RejectionMessageViewModel model, out string errorMessage)
        {
            errorMessage = string.Empty;
            try
            {
                model.LocaleId = MvcAdminConstants.LocaleId;
                RejectionMessageModel updatedmodel = _rejectionMessageClient.UpdateRejectionMessage(model.RejectionMessagesId, RejectionMessageViewModelMap.ToModel(model));
                return (!Equals(updatedmodel, null) && updatedmodel.RejectionMessagesId > 0);
            }
            catch (ZnodeException ex)
            {
                errorMessage = ex.ErrorMessage;
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool DeleteRejectionMessage(int rejectionMessageId, out string errorMessage)
        {
            errorMessage = string.Empty;
            try
            {
                return _rejectionMessageClient.DeleteRejectionMessage(rejectionMessageId);
            }
            catch (ZnodeException ex)
            {
                errorMessage = ex.ErrorMessage;
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        #endregion
    }
}