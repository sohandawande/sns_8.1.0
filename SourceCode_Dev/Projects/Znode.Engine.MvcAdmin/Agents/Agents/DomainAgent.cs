﻿using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public class DomainAgent : BaseAgent, IDomainAgent
    {
        #region Private Variables
        private readonly IDomainsClient _domainClient;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor for Domain agent.
        /// </summary>
        public DomainAgent()
        {
            _domainClient = GetClient<DomainsClient>();
        }
        #endregion

        #region Public Methods

        public DomainListViewModel GetDomains(int portalId=0, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            _domainClient.RefreshCache = true;
            if (Equals(filters, null))
            {
                filters = new FilterCollection();
            }
            if (!Equals(portalId, 0))
            {                
                filters.Add(new FilterTuple(FilterKeys.PortalId, FilterOperators.Equals, portalId.ToString()));
            }
            DomainListModel list = _domainClient.GetDomains(filters, sortCollection, pageIndex - 1, recordPerPage);
            return DomainViewModelMap.ToListViewModel(list.Domains, list.TotalResults, list.PageSize, list.PageIndex, list.TotalPages);
        }

        public DomainViewModel GetDomain(int portalId)
        {
            _domainClient.RefreshCache = true;
            DomainViewModel domainViewModel = DomainViewModelMap.ToViewModel(_domainClient.GetDomain(portalId));
            return domainViewModel;
        }

        public bool createDomainUrl(DomainViewModel model)
        {
            DomainModel domain = _domainClient.CreateDomain(DomainViewModelMap.ToModel(model));
            return (domain.DomainId > 0);
        }

        public bool UpdateDomainUrl(DomainViewModel viewModel)
        {
            DomainModel productType = _domainClient.UpdateDomain(viewModel.DomainId, DomainViewModelMap.ToModel(viewModel));
            return (viewModel.DomainId > 0);
        }

        public bool DeleteDomainUrl(int domainId)
        {
            return _domainClient.DeleteDomain(domainId);
        }
        #endregion
    }
}