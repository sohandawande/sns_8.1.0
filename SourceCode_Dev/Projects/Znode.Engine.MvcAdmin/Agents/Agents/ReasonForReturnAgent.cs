﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Exceptions;
using Znode.Engine.MvcAdmin.Maps;
using Resources;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public class ReasonForReturnAgent : BaseAgent, IReasonForReturnAgent
    {
        #region Private Variables
        private readonly IReasonForReturnClient _reasonForReturnClient; 
        #endregion

        #region Constructor
        public ReasonForReturnAgent()
        {
            _reasonForReturnClient = GetClient<ReasonForReturnsClient>();
        }
        #endregion

        #region Public Methods
        public ReasonForReturnViewModel GetReasonForReturn(int reasonForReturnId)
        {
            _reasonForReturnClient.RefreshCache = true;
            ReasonForReturnViewModel model = ReasonForReturnViewModelMap.ToViewModel(_reasonForReturnClient.GetReasonForReturn(reasonForReturnId));
            return !Equals(model, null) ? model : null;
        }

        public ReasonForReturnListViewModel GetReasonForReturnList(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            _reasonForReturnClient.RefreshCache = true;
            ReasonForReturnListViewModel reasonForReturnList = RMAConfigurationViewModelMap.ToListViewModel(_reasonForReturnClient.GetListOfReasonForReturn(Expands, filters, sortCollection, pageIndex - 1, recordPerPage));
            if (!Equals(reasonForReturnList, null) && !Equals(reasonForReturnList.ReasonsForReturns, null))
            {
                return reasonForReturnList;
            }
            return null;
        }

        public bool CreateReaosnForReturn(ReasonForReturnViewModel model)
        {
            ReasonForReturnViewModel reasonForReturn = ReasonForReturnViewModelMap.ToViewModel(_reasonForReturnClient.CreateReasonForReturn(ReasonForReturnViewModelMap.ToModel(model)));
            return (reasonForReturn.ReasonForReturnId > 0) ? true : false;
        }

        public bool UpdateReaosnForReturn(ReasonForReturnViewModel model)
        {
            ReasonForReturnViewModel reasonForReturn = ReasonForReturnViewModelMap.ToViewModel(_reasonForReturnClient.UpdateReasonForReturn(model.ReasonForReturnId, ReasonForReturnViewModelMap.ToModel(model)));
            return (reasonForReturn.ReasonForReturnId > 0) ? true : false;
        }

        public bool DeleteReaosnForReturn(int reasonForReturnId, out string message)
        {
            try
            {
                message = string.Empty;
                ReasonForReturnViewModel model = new ReasonForReturnViewModel();
                return _reasonForReturnClient.DeleteReasonForReturn(reasonForReturnId);
            }
            catch (ZnodeException ex)
            {
                message = ZnodeResources.ReasonForReturnErrorMessage;
                return false;
            } 
        }
        #endregion
    }
}