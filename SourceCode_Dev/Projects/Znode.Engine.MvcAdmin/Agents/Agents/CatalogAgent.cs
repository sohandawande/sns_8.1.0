﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Enum;
using Znode.Engine.Exceptions;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public class CatalogAgent : BaseAgent, ICatalogAgent
    {
        #region Private Variables
        #region Private Clients Variables
        private readonly ICatalogsClient _catalogClient;
        #endregion

        #region Private Agent Variables
        private readonly IProfilesAgent _profilesAgent;
        private readonly ICategoryAgent _categoryAgent;
        private readonly ICategoryProfileAgent _categoryProfileAgent;
        #endregion

        private string categoryPageType = "Category";
        private CategoryViewModel categoryViewModel;

        #endregion

        #region Constructor
        public CatalogAgent()
        {
            _catalogClient = GetClient<CatalogsClient>();
            _profilesAgent = new ProfilesAgent();
            _categoryAgent = new CategoryAgent();
            _categoryProfileAgent = new CategoryProfileAgent();
        }
        #endregion

        #region Public Methods

        public CatalogListViewModel GetCatalogs(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            _catalogClient.RefreshCache = true;
            if (Equals(filters, null))
            {
                filters = new FilterCollection();
            }
            filters.Add(new FilterTuple(FilterKeys.Username, FilterOperators.Contains, HttpContext.Current.User.Identity.Name));
            CatalogListModel catalogList = _catalogClient.GetCatalogs(filters, sortCollection, pageIndex - 1, recordPerPage);
            if (!Equals(catalogList, null) && !Equals(catalogList.Catalogs, null))
            {
                return CatalogViewModelMap.ToListViewModel(catalogList, pageIndex, recordPerPage);
            }
            return new CatalogListViewModel();

        }

        public CatalogViewModel GetCatalog(int catalogId)
        {
            _catalogClient.RefreshCache = true;
            CatalogModel catalog = _catalogClient.GetCatalog(catalogId);
            return Equals(catalog, null) ? null : CatalogViewModelMap.ToViewModel(catalog);
        }

        public CatalogViewModel CreateCatalog(CatalogViewModel model)
        {
            model.IsActive = true;
            CatalogModel modelSaved = _catalogClient.CreateCatalog(CatalogViewModelMap.ToModel(model));
            return Equals(modelSaved, null) ? null : CatalogViewModelMap.ToViewModel(modelSaved);
        }

        public bool CopyCatalog(CatalogViewModel model)
        {
            model.IsActive = true;
            try
            {
                return _catalogClient.CopyCatalog(CatalogViewModelMap.ToModel(model));
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool DeleteCatalog(CatalogViewModel model, out string message)
        {

            try
            {
                message = string.Empty;
                return _catalogClient.DeleteCatalog(model.CatalogId, model.PreserveCategories);
            }
            catch (ZnodeException ex)
            {
                message = ex.ErrorMessage;
                return false;
            }
        }

        public bool UpdateCatalog(int catalogId, CatalogViewModel model)
        {
            model.IsActive = true;
            CatalogModel modelUpdated = _catalogClient.UpdateCatalog(catalogId, CatalogViewModelMap.ToModel(model));
            return Equals(modelUpdated, null) ? false : true;
        }

        public CatalogSelectListViewModel GetCatalogSelectList(int portalId)
        {
            _catalogClient.RefreshCache = true;
            FilterCollection filters = new FilterCollection();
            filters.Add(new FilterTuple(FilterKeys.PortalId, FilterOperators.Equals, portalId.ToString()));
            CatalogListViewModel catalogs = CatalogViewModelMap.ToViewModels(_catalogClient.GetCatalogListByPortalId(portalId).Catalogs);
            if (!Equals(catalogs, null))
            {
                return new CatalogSelectListViewModel() { CatalogSelectList = PortalCatalogViewModelMap.ToSelectListItems(catalogs.Catalogs) };
            }

            return new CatalogSelectListViewModel();
        }

        public List<SelectListItem> GetCategoryProfilesList(int categoryId, int categoryProfileId = 0, int profileId = 0)
        {
            List<ProfileModel> profileList = _profilesAgent.GetAvailablePortalsBySkuIdCategoryId(0, categoryId, 0, categoryProfileId);

            return CatalogViewModelMap.GetAvailableProfiles(profileList, profileId);
        }

        public CatalogViewModel GetCatalogDetails(int catalogId, int categoryProfileId = 0)
        {
            CatalogViewModel catalogModel = GetCatalog(catalogId);
            catalogModel.CategoryProfileId = categoryProfileId;
            catalogModel.CatalogId = catalogId;
            return catalogModel;
        }

        public CategoryProfileViewModel GetEditCategoryProfilesData(int profileID, int categoryProfileID, int categoryId)
        {
            CategoryProfileViewModel model = new CategoryProfileViewModel();
            model = _categoryProfileAgent.GetCategoryProfileByCategoryProfileId(categoryProfileID);
            model.CategoryProfile = GetCategoryProfilesList(categoryId, categoryProfileID, profileID);
            return model;
        }

        public CategoryProfileViewModel GetAssociateCategoryProfilesData(int categoryId)
        {
            CategoryProfileViewModel model = new CategoryProfileViewModel();
            model.CategoryProfile = GetCategoryProfilesList(categoryId);
            model.EffectiveDate = HelperMethods.ViewDateFormat(DateTime.Now);
            return model;
        }

        public void BindAssociatedCategoryNodeData(int catalogId, string categoryName, CategoryNodeViewModel model)
        {
            BindParentCategoryAndThemeList(catalogId, model);
            model.CssList = _categoryAgent.BindCssList();
            model.MasterPageList = _categoryAgent.BindMasterPageList(0, categoryPageType);
            model.Catalog = GetCatalog(catalogId);
            model.CatalogName = model.Catalog.Name;
            model.Category = new CategoryViewModel();
            model.Category.Name = categoryName;
            model.CatalogId = catalogId;
            model.IsActive = true;
            model.DisplayOrder = 99;
        }

        public void BindEditAssociateCategoryData(int categoryNodeId, int categoryProfileId, CategoryNodeViewModel model, FilterCollectionDataModel formModel)
        {
            model.ParentCategoryNodeId = model.ParentCategoryNodeId;
            model.Catalog = GetCatalog(Convert.ToInt32(model.CatalogId));
            model.Category = Equals(categoryViewModel, null) ? _categoryAgent.GetCategoryByCategoryId(model.CategoryId) : categoryViewModel;
            model.Category.CategoryProfiles = _categoryProfileAgent.GetCategoryProfileByCategoryId(model.CategoryId).CategoryProfiles;
            model.CategoryProfiles = BindCategoryProfileData(categoryNodeId, model, formModel);
            model.AssociatedCategoryProfiles = FilterHelpers.GetDynamicGridModel(formModel, model.Category.CategoryProfiles, Convert.ToInt32(ListType.AssociateCategoryProfiles));
            model.AssociatedCategoryProfiles.TotalRecordCount = model.Category.CategoryProfiles.Count;
            model.CategoryProfileId = categoryProfileId;
            model.Category.Name = model.Category.Name;
            model.CatalogName = model.Catalog.Name;
            BindParentCategoryAndThemeList(Convert.ToInt32(model.CatalogId), model);
            model.CssList = _categoryAgent.BindCssList(Convert.ToInt32(model.ThemeId), Convert.ToInt32(model.CssId));
            model.MasterPageList = _categoryAgent.BindMasterPageList(Convert.ToInt32(model.MasterPageId), categoryPageType, !Equals(model.MasterPageId, null) ? Convert.ToInt32(model.MasterPageId) : 0);
            model.RecordPerPage = 10;
            model.TotalResults = Equals(model.Category.CategoryProfiles, null) ? 0 : model.Category.CategoryProfiles.Count;
        }

        public CategoryProfileViewModel PostCategoryProfileData(CategoryProfileViewModel model)
        {
            CategoryProfileViewModel profileModel = new CategoryProfileViewModel();
            profileModel = _categoryProfileAgent.GetCategoryProfileByCategoryProfileId(model.CategoryProfileID);
            if (!Equals(profileModel, null))
            {
                profileModel.CategoryID = model.CategoryID;
                profileModel.ProfileID = model.ProfileID;
                profileModel.EffectiveDate = model.EffectiveDate;
                profileModel = _categoryProfileAgent.CreateCategoryProfile(profileModel);
            }
            else
            {
                profileModel = _categoryProfileAgent.CreateCategoryProfile(model);
            }
            profileModel.ProfileName = _categoryAgent.GetProfileName(model.ProfileID);
            categoryViewModel = _categoryAgent.GetCategoryByCategoryId(model.CategoryID);
            categoryViewModel.CategoryProfiles = new List<CategoryProfileViewModel>();
            categoryViewModel.CategoryProfiles.Add(profileModel);
            return profileModel;
        }

        public int? GetCatalogByPortalId(int portalId)
        {
            _catalogClient.RefreshCache = true;
            FilterCollection filters = new FilterCollection();
            filters.Add(new FilterTuple(FilterKeys.PortalId, FilterOperators.Equals, portalId.ToString()));
            filters.Add(new FilterTuple(FilterKeys.IsActive, FilterOperators.Equals, FilterKeys.ActiveIndTrue));
            var catalogs = _catalogClient.GetCatalogListByPortalId(portalId).Catalogs;

            if (!Equals(catalogs, null))
            {
                return catalogs[0].CatalogId;
            }

            return null;
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Binds parent category and theme list.
        /// </summary>
        /// <param name="catalogId">The id of catalog</param>
        /// <param name="model">Model of type CategoryNodeViewModel</param>
        private void BindParentCategoryAndThemeList(int catalogId, CategoryNodeViewModel model)
        {
            FilterCollection filter = new FilterCollection();
            if (!Equals(model.CategoryId, null) && model.CategoryId > 0)
            {
                filter.Add(new FilterTuple(FilterKeys.CategoryId, FilterOperators.Equals, model.CategoryId.ToString()));
            }
            model.ParentCategoryNodes = _categoryAgent.BindParentCategoryNodes(catalogId, filter.Count > 0 ? filter : null);
            model.ThemeList = _categoryAgent.BindThemeList(!Equals(model.ThemeId, null) ? Convert.ToInt32(model.ThemeId) : 0);
        }

        /// <summary>
        /// Binds category profile data.
        /// </summary>
        /// <param name="categoryNodeId">The id of category node</param>
        /// <param name="model">Model of type CategoryNodeViewModel</param>
        private CategoryProfileViewModel BindCategoryProfileData(int categoryNodeId, CategoryNodeViewModel model, FilterCollectionDataModel formModel)
        {
            CategoryProfileViewModel categoryProfileViewModel = new CategoryProfileViewModel();
            categoryProfileViewModel.CategoryProfile = GetCategoryProfilesList(model.Category.CategoryId);
            categoryProfileViewModel.CategoryID = model.Category.CategoryId;
            categoryProfileViewModel.CategoryNodeId = categoryNodeId;
            categoryProfileViewModel.EffectiveDate = HelperMethods.ViewDateFormat(DateTime.Now);
            return categoryProfileViewModel;
        }
        #endregion

    }
}