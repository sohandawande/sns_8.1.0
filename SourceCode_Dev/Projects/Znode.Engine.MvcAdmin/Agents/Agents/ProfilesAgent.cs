﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public class ProfilesAgent : BaseAgent, IProfilesAgent
    {
        #region Private Variables
        private readonly IProfilesClient _profilesClient;

        #endregion

        #region Constructor
        public ProfilesAgent()
        {
            _profilesClient = GetClient<ProfilesClient>();
        }
        #endregion

        #region Public Methods

        public ProfileListViewModel GetListOfProfiles(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            _profilesClient.RefreshCache = true;
            ProfileListModel listModel = _profilesClient.GetProfiles(null, filters, sortCollection, pageIndex - 1, recordPerPage);
            if (!Equals(listModel, null) && !Equals(listModel.Profiles, null))
            {
                return ProfileViewModelMap.ToListViewModel(listModel, pageIndex, recordPerPage);
            }
            return new ProfileListViewModel();
        }

        public bool CreateProfile(ProfileViewModel model, out int profileId)
        {
            ProfileModel modelSaved = _profilesClient.CreateProfile(ProfileViewModelMap.ToModel(model));
            profileId = modelSaved.ProfileId;
            return Equals(modelSaved, null) ? false : true;
        }

        public ProfileListViewModel GetProfileByPortalId(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            int portalId = 0;
            foreach (var filter in filters)
            {
                if (filter.Item1.ToLower().Equals(FilterKeys.PortalId.ToLower()))
                {
                    portalId = Convert.ToInt32(filter.Item3);
                }
            }
            ProfileListModel profileList = _profilesClient.GetProfileListByPortalId(portalId);
            if (!Equals(profileList, null) && !Equals(profileList.Profiles, null))
            {
                return ProfileViewModelMap.ToListViewModel(profileList, pageIndex, recordPerPage);
            }
            return new ProfileListViewModel();
        }

        public ProfileViewModel GetProfileByProfileId(int profileId)
        {
            _profilesClient.RefreshCache = true;
            ProfileModel modelSaved = _profilesClient.GetProfile(profileId);
            return Equals(modelSaved, null) ? null : ProfileViewModelMap.ToViewModel(modelSaved);
        }


        public bool UpdateProfile(ProfileViewModel model)
        {
            ProfileModel modelSaved = _profilesClient.UpdateProfile(model.ProfileId, ProfileViewModelMap.ToModel(model));
            return Equals(modelSaved, null) ? false : true;
        }

        public bool DeleteProfile(int profileId)
        {
            ProfileViewModel model = new ProfileViewModel();
            return _profilesClient.DeleteProfile(profileId);
        }

        public List<ProfileModel> GetAvailablePortalsBySkuIdCategoryId(int skuId = 0, int categoryId = 0, int skuEffectiveProfileId = 0, int categoryProfileId = 0)
        {
            _profilesClient.RefreshCache = true;
            AssociatedSkuCategoryProfileModel model = new AssociatedSkuCategoryProfileModel();
            model.UserName = HttpContext.Current.User.Identity.Name;
            model.CategoryId = categoryId;
            model.CategoryProfileId = categoryProfileId;
            model.SkuId = skuId;
            model.SkuEffectiveId = skuEffectiveProfileId;
            var list = _profilesClient.GetAvailableProfilesBySkuIdCategoryId(model);
            if (!Equals(list, null))
            {
                return list.Profiles.ToList();
            }
            else
            {
                return new List<ProfileModel>();
            }
        }
        #endregion
    }
}