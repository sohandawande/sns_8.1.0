﻿using Znode.Engine.Api.Client;

namespace Znode.Engine.MvcAdmin.Agents
{
    /// <summary>
    /// This is the agent which deals with the clear API cache functionality
    /// </summary>
    public class ClearAPICacheAgent : BaseAgent, IClearAPICacheAgent
    {
        #region Private Variables
        private readonly IClearCacheClient _clearCaheClient;
        #endregion

        #region Constructor
        public ClearAPICacheAgent()
        {
            _clearCaheClient = GetClient<ClearCacheClient>();
        }
        #endregion

        #region Public Methods
        
        public bool ClearApiCache()
        {
            return _clearCaheClient.ClearAPICache();
        }

        #endregion
    }
}