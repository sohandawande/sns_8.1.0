﻿using Resources;
using System.Collections.Generic;
using System.Web.Mvc;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    /// <summary>
    /// Tax Rule Agent
    /// </summary>
    public class TaxRuleAgent : BaseAgent, ITaxRuleAgent
    {
        #region Private members
        private readonly ITaxRulesClient _taxRuleClient;
        private readonly ITaxRuleTypesClient _taxRuleTypeClient;
        private readonly ICountriesClient _countryClient;
        private readonly IStatesClient _stateClient;
        private readonly IZipCodeClient _zipCodeClient;
        private const string selectZeroIndex = "0";
        private const string trueValue = "true";
        #endregion

        #region Constructor

        /// <summary>
        /// Constructor for the Tax Class
        /// </summary>
        public TaxRuleAgent()
        {
            _taxRuleClient = GetClient<TaxRulesClient>();
            _taxRuleTypeClient = GetClient<TaxRuleTypesClient>();
            _countryClient = GetClient<CountriesClient>();
            _stateClient = GetClient<StatesClient>();
            _zipCodeClient = GetClient<ZipCodeClient>();
        }

        #endregion

        #region Public Methods

        public TaxRulesViewModel GetTaxRules(int taxRuleId)
        {
            _taxRuleClient.RefreshCache = true;
            TaxRuleModel taxRuleModel = _taxRuleClient.GetTaxRule(taxRuleId);            
            return (!Equals(taxRuleModel, null)) ? TaxRulesViewModelMap.ToViewModel(taxRuleModel) : null;
        }

        public bool SaveTaxRules(TaxRulesViewModel model)
        {
            TaxRuleModel taxRuleModel = _taxRuleClient.CreateTaxRule(TaxRulesViewModelMap.ToModel(model));
            return (!Equals(taxRuleModel, null) && (taxRuleModel.TaxRuleId > 0));
        }

        public bool UpdateTaxRules(TaxRulesViewModel model)
        {
            TaxRuleModel taxRuleModel = _taxRuleClient.UpdateTaxRule(model.TaxRuleId, TaxRulesViewModelMap.ToModel(model));
            return (!Equals(taxRuleModel, null) && (taxRuleModel.TaxRuleId > 0));
        }

        public bool Delete(int taxRuleId)
        {
            return _taxRuleClient.DeleteTaxRule(taxRuleId);
        }

        #region Bind Tax Rule Type List and Country List

        public TaxRulesViewModel BindDropdownValues(TaxRulesViewModel model, bool isEditAction=false)
        {
            _countryClient.RefreshCache = true;

            FilterCollection filters = new FilterCollection();
            if(!isEditAction)
            {
                filters.Add(new FilterTuple(FilterKeys.IsActive, FilterOperators.Equals, trueValue));
            }           
            model.TaxRuleTypeList = TaxRulesViewModelMap.ToListItems(_taxRuleTypeClient.GetTaxRuleTypes(filters, null).TaxRuleTypes);
            model.TaxRuleTypeCollection = _taxRuleTypeClient.GetTaxRuleTypes(filters, null).TaxRuleTypes;//PRFT Custom Code
            SortCollection sort = new SortCollection();
            sort.Add(SortKeys.Name, SortDirections.Ascending);
            model.CountryList = TaxRulesViewModelMap.ToListItems(_countryClient.GetCountries(null, null, sort).Countries);
            model.CountryList.Insert(0, new SelectListItem() { Text = ZnodeResources.LabelAllCountries, Value = selectZeroIndex });
            return model;
        }

        #endregion

        #region Bind State List

        public List<SelectListItem> BindStateList(Api.Client.Filters.FilterCollection filters, string stateCode = "")
        {
            _stateClient.RefreshCache = true;
            if (!Equals(filters, null))
            {
                var list = TaxRulesViewModelMap.ToListItems(_stateClient.GetStates(filters, null, null, null).States);
                list.Insert(0, new SelectListItem() { Value = selectZeroIndex, Text = ZnodeResources.DefaultStateDropDown });
                return list;
            }
            else if (Equals(filters, null) && !string.IsNullOrEmpty(stateCode))
            {
                var list = TaxRulesViewModelMap.ToListItems(_stateClient.GetStates(filters, null, null, null).States, stateCode);
                list.Insert(0, new SelectListItem() { Value = selectZeroIndex, Text = ZnodeResources.DefaultStateDropDown });
                return list;
            }
            else
            {
                List<SelectListItem> List = new List<SelectListItem>();
                List.Add(new SelectListItem() { Value = selectZeroIndex, Text = ZnodeResources.DefaultStateDropDown });
                return List;
            }
        }

        #endregion

        #region Bind County List

        public List<SelectListItem> BindCountyList(Api.Client.Filters.FilterCollection filters, string countyCode = "")
        {
            _zipCodeClient.RefreshCache = true;
            if (!Equals(filters, null))
            {
                SortCollection sort = new SortCollection();
                sort.Add(SortKeys.CountyName, SortDirections.Ascending);
                var list = TaxRulesViewModelMap.ToListItems(_zipCodeClient.GetZipCodes(filters, sort, null, null).ZipCode);
                if (!Equals(list, null))
                {
                    if (list.Count > 1)
                        list.Insert(0, new SelectListItem() { Value = selectZeroIndex, Text = ZnodeResources.DefaultCountyDropDown });
                }
                else
                {
                    list = new List<SelectListItem>();
                    list.Add(new SelectListItem() { Value = selectZeroIndex, Text = ZnodeResources.DefaultCountyDropDown });
                }

                return list;
            }
            else if (Equals(filters, null) && !string.IsNullOrEmpty(countyCode))
            {
                var list = TaxRulesViewModelMap.ToListItems(_zipCodeClient.GetZipCodes(filters, null, null, null).ZipCode, countyCode);
                list.Insert(0, new SelectListItem() { Value = selectZeroIndex, Text = ZnodeResources.DefaultCountyDropDown });
                return list;
            }
            else
            {
                List<SelectListItem> List = new List<SelectListItem>();
                List.Add(new SelectListItem() { Value = selectZeroIndex, Text = ZnodeResources.DefaultCountyDropDown });
                return List;
            }
        }

        #endregion

        #endregion
    }
}