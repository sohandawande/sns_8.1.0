﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    /// <summary>
    /// Agent of Content Page.
    /// </summary>
    public class ContentPageAgent : BaseAgent, IContentPageAgent
    {
        #region Private Variables

        private readonly IContentPageClient _contentPageClient;
        private readonly ICSSClient _cssClient;
        private readonly IThemeClient _themeClient;
        private readonly IPortalsClient _portalClient;
        private readonly IMasterPageClient _masterPageClient;
        private readonly IContentPageRevisionClient _contentPageRevisionClient;
        private readonly IProfileCommonAgent _profileCommonAgent;
        private readonly IPortalAgent _portalAgent;
        private readonly ISearchClient _searchClient;
        private readonly string selectZeroIndex = "0";
        private readonly string pageType = MvcAdminConstants.PageType;

        #endregion

        #region Default Constructor
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public ContentPageAgent()
        {
            _contentPageClient = GetClient<ContentPageClient>();
            _cssClient = GetClient<CSSClient>();
            _themeClient = GetClient<ThemeClient>();
            _portalClient = GetClient<PortalsClient>();
            _masterPageClient = GetClient<MasterPageClient>();
            _contentPageRevisionClient = GetClient<ContentPageRevisionClient>();
            _profileCommonAgent = new ProfileCommonAgent();
            _portalAgent = new PortalAgent();
            _searchClient = GetClient<SearchClient>();
        }

        #endregion

        #region Public Methods

        public ContentPageListViewModel GetContentPages()
        {
            var list = _contentPageClient.GetContentPages(null, null, null);
            return ContentPageViewModelMap.ToListViewModel(list);
        }

        public ContentPageListViewModel GetContentPages(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            _contentPageClient.RefreshCache = true;
            if (Equals(filters, null))
            {
                filters = new FilterCollection();
            }
            filters.Add(new FilterTuple(FilterKeys.Username, FilterOperators.Contains, HttpContext.Current.User.Identity.Name));
            var list = _contentPageClient.GetContentPages(new ExpandCollection { ExpandKeys.Portal, ExpandKeys.MasterPage }, filters, sortCollection, pageIndex - 1, recordPerPage);
            return Equals(list, null) ? new ContentPageListViewModel() : ContentPageViewModelMap.ToListViewModel(list);
        }

        public ContentPageViewModel GetContentPage(int? contentPageId)
        {
            ContentPageViewModel model = new ContentPageViewModel();
            _contentPageClient.RefreshCache = true;
            if (!Equals(contentPageId, null) && contentPageId > 0)
            {
                ContentPageModel contentPage = _contentPageClient.GetContentPage((int)contentPageId);

                if (!Equals(contentPage, null))
                {
                    model = ContentPageViewModelMap.ToViewModel(contentPage);
                    model.OldHtml = model.Html;
                    if (model.ThemeID.HasValue && model.ThemeID > 0)
                    {
                        FilterCollection filters = new FilterCollection();
                        filters.Add(new FilterTuple(FilterKeys.CSSThemeId, FilterOperators.Equals, model.ThemeID.ToString()));

                        model.CSSList = this.BindCssList(filters);
                        model.MasterPageList = this.BindMasterPageList(model.ThemeID.Value, pageType);
                    }
                    else
                    {
                        model.CSSList = this.BindCssList(null);
                        model.MasterPageList = this.BindMasterPageList(0, pageType);
                    }
                }
            }
            return model;
        }

        public bool CreateContentPage(ContentPageViewModel model, out string errorMessage)
        {
            errorMessage = string.Empty;
            try
            {
                if (!Equals(model, null))
                {
                    if (!string.IsNullOrEmpty(model.SEOFriendlyPageName) && _searchClient.IsRestrictedSeoUrl(model.SEOFriendlyPageName))
                    {
                        errorMessage = ZnodeResources.ErrorRestrictedSeoUrlExists;
                        return false;
                    }
                    model.UpdatedUser = HttpContext.Current.User.Identity.Name;
                    model.LocaleId = MvcAdminConstants.LocaleId;
                    model.AllowDelete = true;
                    model.ActiveInd = true;
                    model.OldHtml = string.Empty;
                    ContentPageModel createdModel = _contentPageClient.CreateContentPage(ContentPageViewModelMap.ToModel(model));
                    return Equals(createdModel.ContentPageID, null) ? false : true;
                }
            }
            catch (ZnodeException ex)
            {
                errorMessage = ex.ErrorMessage;
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
            return false;
        }

        public bool UpdateContentPage(ContentPageViewModel model, out string errorMessage)
        {
            errorMessage = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(model.SEOFriendlyPageName) && _searchClient.IsRestrictedSeoUrl(model.SEOFriendlyPageName))
                {
                    errorMessage = ZnodeResources.ErrorRestrictedSeoUrlExists;
                    return false;
                }
                ContentPageViewModel contentPageViewModel = this.GetContentPage(model.ContentPageId);
                model.UpdatedUser = HttpContext.Current.User.Identity.Name;
                bool contentPage = _contentPageClient.UpdateContentPage(model.ContentPageId, ContentPageViewModelMap.ToModel(model));
                return contentPage;
            }
            catch (ZnodeException ex)
            {
                errorMessage = ex.ErrorMessage;
                return false;
            }
            catch (Exception ex)
            {
            }
            return false;
        }

        public bool DeleteContentPage(int contentPageId, out string errorMessgae)
        {
            errorMessgae = string.Empty;
            try
            {
                return _contentPageClient.DeleteContentPage((int)contentPageId);
            }
            catch (ZnodeException ex)
            {
                errorMessgae = ex.ErrorMessage;
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool IsAllowDelete(int contentPageId)
        {
            ContentPageViewModel model = new ContentPageViewModel();
            var contentPage = _contentPageClient.GetContentPage((int)contentPageId);
            return model.AllowDelete;
        }

        public bool RevertRevision(int revisionId, int? contentPageId, out string errorMessage)
        {
            errorMessage = string.Empty;
            try
            {
                ContentPageViewModel model = this.GetContentPage(contentPageId);
                ContentPageRevisionViewModel contentPageRevision = new ContentPageRevisionViewModel();
                contentPageRevision.UpdatedUser = HttpContext.Current.User.Identity.Name;
                contentPageRevision.ContentPageName = model.PageName;
                contentPageRevision.OldHtml = model.Html;
                return _contentPageRevisionClient.RevertRevision(revisionId, ContentPageRevisionViewModelMap.ToModel(contentPageRevision));
            }
            catch (ZnodeException ex)
            {
                errorMessage = ex.ErrorMessage;
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public List<SelectListItem> BindThemeList()
        {
            _themeClient.RefreshCache = true;
            var list = ContentPageViewModelMap.ToListItems(_themeClient.GetThemes(null, null).Themes);
            list.Insert(0, new SelectListItem() { Value = selectZeroIndex, Text = ZnodeResources.DropdownThemeDefault });
            return list;
        }

        public List<SelectListItem> BindPortalList()
        {
            _portalClient.RefreshCache = true;
            return ContentPageViewModelMap.ToListItems(_portalClient.GetPortals(null, null, null).Portals);
        }

        public List<SelectListItem> BindCssList(Api.Client.Filters.FilterCollection filters)
        {
            _cssClient.RefreshCache = true;
            var list = ContentPageViewModelMap.ToListItems(_cssClient.GetCSSs(null, filters, null).CSSs);
            list.Insert(0, new SelectListItem() { Value = selectZeroIndex, Text = ZnodeResources.DropdownThemeDefault });
            return list;
        }

        public List<SelectListItem> BindMasterPageList(int CssThemeId, string pageType)
        {
            _masterPageClient.RefreshCache = true;
            var list = ContentPageViewModelMap.ToListItems(_masterPageClient.GetMasterPageByThemeId(CssThemeId, pageType).MasterPages);
            list.Insert(0, new SelectListItem() { Value = selectZeroIndex, Text = ZnodeResources.DropdownMasterDefault });
            return list;
        }

        public ContentPageRevisionListViewModel GetContentPageRevisionById(int contentPageId, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            _contentPageRevisionClient.RefreshCache = true;
            if (Equals(filters, null))
            {
                filters = new FilterCollection();
            }
            filters.Add(FilterKeys.ContentPageId, FilterOperators.Equals, contentPageId.ToString());
            ContentPageRevisionListModel modelList = _contentPageRevisionClient.GetContentPageRevisions(null, filters, sortCollection, pageIndex - 1, recordPerPage);

            var list = ContentPageRevisionViewModelMap.ToListViewModel(modelList, pageIndex, recordPerPage, modelList.TotalPages, modelList.TotalResults);
            if (!Equals(list, null) && !Equals(list.ContentPageRevisions, null))
            {
                foreach (var item in list.ContentPageRevisions)
                {
                    item.ContentPageId = contentPageId;
                }
                list.contentPageViewModel = this.GetContentPage(contentPageId);
            }
            return list;
        }

        public List<PortalViewModel> GetPortalsList()
        {
            PortalListViewModel portalListModel = new PortalListViewModel();
            _portalClient.RefreshCache = true;

            portalListModel = _portalAgent.GetPortals();

            return !Equals(portalListModel, null) && !Equals(portalListModel.Portals, null) ? (portalListModel.Portals.ToList()) : new List<PortalViewModel>();
        }

        #endregion
    }
}