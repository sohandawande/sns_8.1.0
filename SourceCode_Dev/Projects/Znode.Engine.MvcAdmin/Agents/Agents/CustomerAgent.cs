﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;
using Znode.Libraries.Helpers;

namespace Znode.Engine.MvcAdmin.Agents
{
    public partial class CustomerAgent : BaseAgent, ICustomerAgent
    {
        #region Private Variables
        private readonly IAccountsClient _accountsClient;
        private readonly IAddressesClient _addressClient;
        private readonly ICustomerClient _customerClient;
        private readonly IProfilesClient _profilesClient;
        private readonly IAccountsProfilesClient _accountProfileClient;
        private readonly IProductCategoriesClient _productCategoriesClient;
        private const string Email = "Address.Email";
        public const string Guid = "guid";
        public const string Quantity = "quantity";
        private readonly ICustomerUserMappingClient _customerUserMappingClient; //PRFT Custom Code
        #endregion

        #region Constructor
        public CustomerAgent()
        {
            _accountsClient = GetClient<AccountsClient>();
            _addressClient = GetClient<AddressesClient>();
            _customerClient = GetClient<CustomerClient>();
            _profilesClient = GetClient<ProfilesClient>();
            _accountProfileClient = GetClient<AccountsProfilesClient>();
            _productCategoriesClient = GetClient<ProductCategoriesClient>();
            _customerUserMappingClient = GetClient<CustomerUserMappingClient>();

        }
        #endregion

        #region Public Methods

        public CustomerListViewModel GetCustomerList(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            CustomerListViewModel model = new CustomerListViewModel();

            filters.Add(new FilterTuple(FilterKeys.RoleName, FilterOperators.Is, FilterKeys.Null));
            filters.Add(new FilterTuple(FilterKeys.Username, FilterOperators.Equals, string.Format("'{0}'", HttpContext.Current.User.Identity.Name)));

            ReplaceFilterKeyName(ref filters, MvcAdminConstants.AddressFirstName, FilterKeys.FirstName);
            ReplaceFilterKeyName(ref filters, MvcAdminConstants.AddressLastName, FilterKeys.LastName);
            ReplaceFilterKeyName(ref filters, MvcAdminConstants.AddressPortalCode, FilterKeys.PostalCode);
            ReplaceFilterKeyName(ref filters, MvcAdminConstants.StartDate, FilterKeys.CreateDate);
            ReplaceFilterKeyName(ref filters, MvcAdminConstants.EndDate, FilterKeys.CreateDate);
            ReplaceFilterKeyName(ref filters, MvcAdminConstants.LabelValue, FilterKeys.ReferralStatus);
            ReplaceFilterKeyName(ref filters, Email, MvcAdminConstants.Email);
            ReplaceSortKeyName(sortCollection, MvcAdminConstants.LoginName, MvcAdminConstants.UserName);
            ReplaceFilterKeyName(ref filters, MvcAdminConstants.ExternalId, FilterKeys.ExternalAccountId);
            this.RemoveFilters(filters, Quantity);
            this.RemoveFilters(filters, Guid);

            //PRFT Custom Code : Start
            if (CheckIScustomerAndSuperUser(filters))
            {
                if (CheckIsCustomerFilters(filters))
                {
                    //filters.Add(new FilterTuple(FilterKeys.UserId, FilterOperators.Is, FilterKeys.Null));
                    filters.Add(new FilterTuple(FilterKeys.Custom2, FilterOperators.Equals, "0"));
                    //filters.Add(new FilterTuple(FilterKeys.Custom2, FilterOperators.Is, FilterKeys.Null));

                }
                if (CheckIsSuperUserFilters(filters))
                {
                    filters.Add(new FilterTuple(FilterKeys.Custom2, FilterOperators.Equals, "1"));
                }
            }
            this.RemoveFilters(filters, "iscustomer");
            this.RemoveFilters(filters, "issuperuser");
            //PRFT Custom Code : End

            _customerClient.RefreshCache = true;
            var accountList = _customerClient.GetCustomerList(null, filters, sortCollection, pageIndex, recordPerPage);
            ReplaceSortKeyName(sortCollection, MvcAdminConstants.UserName, MvcAdminConstants.LoginName);
            if (!Equals(accountList, null))
            {
                model = CustomerViewModelMap.ToListViewModel(accountList.CustomerList, accountList.TotalResults, accountList.PageSize, accountList.PageIndex, accountList.TotalPages);
            }
            return model;
        }

        public CustomerListViewModel GetFranchiseCustomerList(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            CustomerListViewModel model = new CustomerListViewModel();

            ReplaceFilterKeyName(ref filters, MvcAdminConstants.UserName, FilterKeys.LoginName);
            ReplaceFilterKeyName(ref filters, MvcAdminConstants.AddressFirstName, FilterKeys.FirstName);
            ReplaceFilterKeyName(ref filters, MvcAdminConstants.AddressLastName, FilterKeys.LastName);
            ReplaceFilterKeyName(ref filters, MvcAdminConstants.AddressPortalCode, FilterKeys.PostalCode);
            ReplaceFilterKeyName(ref filters, MvcAdminConstants.StartDate, FilterKeys.CreateDate);
            ReplaceFilterKeyName(ref filters, MvcAdminConstants.EndDate, FilterKeys.CreateDate);
            ReplaceFilterKeyName(ref filters, MvcAdminConstants.LabelValue, FilterKeys.ReferralStatus);
            ReplaceFilterKeyName(ref filters, Email, MvcAdminConstants.Email);
            filters.Add(new FilterTuple(FilterKeys.Username, FilterOperators.Equals, string.Format("'{0}'", HttpContext.Current.User.Identity.Name)));
            ReplaceSortKeyName(sortCollection, MvcAdminConstants.LoginName, MvcAdminConstants.UserName);
            ReplaceFilterKeyName(ref filters, MvcAdminConstants.ExternalId, FilterKeys.ExternalAccountId);

            this.RemoveFilters(filters, Quantity);
            this.RemoveFilters(filters, Guid);

            _customerClient.RefreshCache = true;
            var accountList = _customerClient.GetCustomerList(null, filters, sortCollection, pageIndex, recordPerPage);
            ReplaceSortKeyName(sortCollection, MvcAdminConstants.UserName, MvcAdminConstants.LoginName);
            if (!Equals(accountList, null))
            {
                model = CustomerViewModelMap.ToListViewModel(accountList.CustomerList, accountList.TotalResults, accountList.PageSize, accountList.PageIndex, accountList.TotalPages);
            }
            return model;
        }

        public AccountViewModel GetCustomerAccountById(int customerId)
        {
            AccountViewModel model = new AccountViewModel();
            if (customerId > 0)
            {
                _accountsClient.RefreshCache = true;
                var accountModel = _accountsClient.GetAccount(customerId, new ExpandCollection { ExpandKeys.User });
                if (!Equals(accountModel, null))
                {
                    model = AccountViewModelMap.ToAccountViewModel(accountModel);
                }
            }
            return model;
        }

        public AddressListViewModel GetCustomerAddressDetails(int accountId, FilterCollection fromFilters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            string EmailID = string.Empty;
            AddressListViewModel model = new AddressListViewModel();
            fromFilters.Add(new FilterTuple(MvcAdminConstants.AccountId, FilterOperators.Equals, accountId.ToString()));
            _addressClient.RefreshCache = true;
            var addressList = _addressClient.GetAddresses(fromFilters, sortCollection, pageIndex - 1, recordPerPage);
            if (!Equals(addressList, null) && !Equals(addressList.Addresses, null) && addressList.Addresses.Count > 0)
            {
                EmailID = _accountsClient.GetAccount(accountId).Email;
                model = CustomerViewModelMap.ToCustomerAddressList(addressList.Addresses, EmailID, addressList.TotalResults);
            }
            return model;
        }

        public CustomerViewModel UpdateCustomerAccount(CustomerViewModel model)
        {
            CustomerViewModel viewModel = new CustomerViewModel();
            try
            {
                var account = _accountsClient.UpdateCustomerAccount(CustomerViewModelMap.ToAccountModel(model));
                viewModel.CustomerAccount = AccountViewModelMap.ToAccountViewModel(account);
            }
            catch (ZnodeException ex)
            {
                switch (ex.ErrorCode)
                {
                    case ErrorCodes.CustomerAccountError:
                        return new CustomerViewModel { HasError = true, ErrorMessage = ex.ErrorMessage };
                    default:
                        return viewModel;
                }
            }
            return viewModel;
        }

        public bool CreateCustomerAccount(CustomerViewModel model, out int accountId, out string message)
        {
            accountId = 0;
            try
            {
                message = string.Empty;

                AccountModel customerAccount = new AccountModel();
                if (Equals(model.UserType, UserTypes.FranchiseAdmin))
                {
                    model.CustomerAccount.PortalId = model.PortalId;
                    ProfileListModel profileList = _profilesClient.GetProfileListByPortalId(model.PortalId.Value);
                    model.CustomerAccount.ProfileId = (!Equals(profileList, null) && !Equals(profileList.Profiles, null)) ? profileList.Profiles.FirstOrDefault().ProfileId : 0;
                }
                customerAccount = _accountsClient.CreateCustomerAccount(CustomerViewModelMap.ToAccountModel(model));
                accountId = customerAccount.AccountId;

                return (customerAccount.AccountId > 0) ? true : false;
            }
            catch (ZnodeException ex)
            {
                message = ex.ErrorMessage;
                return false;
            }
        }


        public CustomerDownloadListViewModel GetCustomerToDownload(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            CustomerDownloadListViewModel model = new CustomerDownloadListViewModel();

            ReplaceFilterKeyName(ref filters, MvcAdminConstants.StartDate, FilterKeys.CreateDate);
            ReplaceFilterKeyName(ref filters, MvcAdminConstants.EndDate, FilterKeys.CreateDate);
            ReplaceFilterKeyName(ref filters, Email, MvcAdminConstants.Email);

            var accountList = _customerClient.GetCustomerList(null, filters, sortCollection, pageIndex, recordPerPage);
            if (!Equals(accountList, null))
            {
                model = CustomerViewModelMap.ToDownload(accountList.CustomerList, accountList.TotalResults, accountList.PageSize, accountList.PageIndex, accountList.TotalPages);
            }
            return model;
        }

        public bool DeleteCustomerAccount(int accountId, out string errorMessage)
        {
            try
            {
                errorMessage = string.Empty;
                return _accountsClient.DeleteAccount(accountId);
            }
            catch (ZnodeException ex)
            {
                errorMessage = ex.ErrorMessage;
                return false;
            }
        }

        public ReferralCommissionListViewModel GetReferralCommissionList(int accountId, FilterCollection filters, SortCollection sorts, int? pageIndex, int? recordPerPage)
        {
            ReferralCommissionListViewModel referralCommissionList = CustomerViewModelMap.ToListViewModel(_customerClient.GetReferralCommissionList(accountId, new ExpandCollection() { ExpandKeys.ReferralCommissionType }, filters, sorts, pageIndex, recordPerPage));

            return !Equals(referralCommissionList, null) ? referralCommissionList : new ReferralCommissionListViewModel();
        }


        public ProfileListViewModel GetCustomerProfileList(int accountId, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            filters.Add(new FilterTuple(FilterKeys.AccountId, FilterOperators.Equals, accountId.ToString()));

            ProfileListModel accountProfileList = _profilesClient.GetCustomerProfile(accountId, null, filters, sortCollection, pageIndex, recordPerPage);
            if (!Equals(accountProfileList, null) && !Equals(accountProfileList.Profiles, null))
            {
                return ProfileViewModelMap.ToListViewModel(accountProfileList, pageIndex, recordPerPage);
            }
            return new ProfileListViewModel();
        }

        public ProfileListViewModel GetCustomerNotAssociatedProfileList(int accountId, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            filters.Add(new FilterTuple(FilterKeys.AccountId, FilterOperators.Equals, accountId.ToString()));
            ProfileListModel accountProfileList = _profilesClient.GetCustomerNotAssociatedProfiles(null, filters, sortCollection, pageIndex, recordPerPage);
            if (!Equals(accountProfileList, null) && !Equals(accountProfileList.Profiles, null))
            {
                return ProfileViewModelMap.ToListViewModel(accountProfileList, pageIndex, recordPerPage);
            }
            return new ProfileListViewModel();
        }


        public bool DeleteAccountAssociatedProfile(int accountProfileId)
        {
            return _accountProfileClient.DeleteAccountProfile(accountProfileId);
        }

        public bool SaveAssociateProfile(int accountId, string profileIds)
        {
            return Equals(_accountProfileClient.AccountAssociatedProfiles(accountId, profileIds), null) ? false : true;
        }

        public CustomerBasedPricingProductListViewModel GetCustomerPricingProductList(int accountId, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            CustomerBasedPricingListModel cutomerBasedPricingList = _customerClient.GetCustomerBasedPricing(accountId, null, filters, sortCollection, pageIndex, recordPerPage);
            if (!Equals(cutomerBasedPricingList, null) && !Equals(cutomerBasedPricingList.CustomerBasedPricing, null))
            {
                cutomerBasedPricingList.ProductId = accountId;
                return CustomerBasedPricingViewModelMap.ToCustomerPricingListViewModel(cutomerBasedPricingList, pageIndex, recordPerPage, cutomerBasedPricingList.TotalPages, cutomerBasedPricingList.TotalResults);
            }
            return new CustomerBasedPricingProductListViewModel();
        }

        public void DownloadFile(CustomerDownloadListViewModel model, HttpResponseBase response, string fileName)
        {
            DownloadHelper downloadHelper = new DownloadHelper();
            downloadHelper.Download(model.customerList, Convert.ToString(Convert.ToInt32(FileTypes.CSV)), response, null, fileName);
        }

        public void DownloadCustomerPricingProductFile(CustomerBasedPricingProductListViewModel model, HttpResponseBase response, string fileName)
        {
            DownloadHelper downloadHelper = new DownloadHelper();
            downloadHelper.Download(model.CustomerBasedPricingProduct, Convert.ToString(Convert.ToInt32(FileTypes.CSV)), response, MvcAdminConstants.CommaSeparator, fileName);
        }

        public CustomerViewModel BindStatus()
        {
            CustomerViewModel model = new CustomerViewModel();
            model.GetAffilateApprovalStatusList = CustomerViewModelMap.GetAffiliateApprovalStatus(string.Empty);
            return model;
        }
        #endregion

        #region Customer Affiliate
        public CustomerAffiliateViewModel GetCustomerAffiliateByAccountId(int accountId)
        {
            CustomerAffiliateViewModel customerAffiliate = CustomerViewModelMap.ToCustomerAffiliateViewModel(_customerClient.GetCustomerAffiliate(accountId, new ExpandCollection { ExpandKeys.ReferralCommissionType }));

            return !Equals(customerAffiliate, null) ? customerAffiliate : new CustomerAffiliateViewModel();
        }

        public List<SelectListItem> GetReferralCommissionTypeList()
        {
            List<SelectListItem> listItems = CustomerViewModelMap.ToListItems(_customerClient.GetReferralCommissionTypeList(Expands, Filters, new SortCollection(), null, null).ReferralCommissionTypes);
            return !Equals(listItems, null) ? listItems : new List<SelectListItem>();
        }

        public bool UpdateCustomerAffiliate(CustomerAffiliateViewModel customerAffiliate)
        {
            AccountModel account = _accountsClient.GetAccount(customerAffiliate.AccountId);
            if (account.AccountId > 0)
            {
                customerAffiliate.ReferalStatus = customerAffiliate.Status;
                account.ReferralCommissionTypeId = customerAffiliate.ReferralCommisionTypeId;
                account.ReferralCommission = customerAffiliate.ReferralCommission;
                account.TaxId = customerAffiliate.TaxID;
                account.ReferralStatus = customerAffiliate.ReferalStatus;
            }
            account = _accountsClient.UpdateAccount(account.AccountId, account);
            return Equals(account, null) ? false : true;
        }
        #endregion

        #region Approval Status
        public List<SelectListItem> GetApprovStatusList()
        {
            List<SelectListItem> listItems = new List<SelectListItem>();
            listItems = CustomerViewModelMap.GetApprovalStatus(string.Empty);
            return listItems;
        }
        #endregion

        #region Customer Account Payment
        public AccountPaymentListViewModel GetAccountPaymentList(int accountId, FilterCollection filters, SortCollection sorts, int? pageIndex, int? recordPerPage)
        {
            AccountPaymentListViewModel accountPaymentList = CustomerViewModelMap.ToListViewModel(_accountsClient.GetAccountPaymentList(accountId, new ExpandCollection(), filters, sorts, pageIndex - 1, recordPerPage));

            return !Equals(accountPaymentList, null) ? accountPaymentList : new AccountPaymentListViewModel();
        }

        public bool CreateAccountPayment(AccountPaymentViewModel model)
        {
            AccountPaymentViewModel customerAccountPayment = CustomerViewModelMap.ToAccountPaymentViewModel(_accountsClient.CreateAccountPayment(CustomerViewModelMap.ToAccountPaymentModel(model)));
            return customerAccountPayment.AccountPaymentId > 0;
        }
        #endregion

        #region private method

        /// <summary>
        /// Remove Guid and Quantity Filters from filterCollection
        /// </summary>
        /// <param name="filters">FilterCollection filters</param>
        /// <param name="compair">string to compair with</param>
        /// <returns>Returns Updated Filters</returns>
        private FilterCollection RemoveFilters(FilterCollection filters, string compair)
        {
            if (!Equals(filters, null) && ((filters.Count > 0)))
            {
                foreach (var item in filters)
                {
                    if (Equals(item.Item1.ToLower(), compair))
                    {
                        filters.Remove(item);
                        break;
                    }
                }
            }
            return filters;
        }

        #endregion

        #region PRFT Custom Method
        private bool CheckIScustomerAndSuperUser(FilterCollection filters)
        {
            if (!Equals(filters, null) && ((filters.Count > 0)))
            {
                foreach (var item in filters)
                {
                    if (Equals(item.Item1.ToLower(), "iscustomer"))
                    {
                        if (item.Item3.Contains("true"))
                        {
                            foreach (var itemSuperUser in filters)
                            {
                                if (Equals(itemSuperUser.Item1.ToLower(), "issuperuser"))
                                {
                                    if (itemSuperUser.Item3.Contains("true"))
                                    {
                                        return false;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return true;
        }


        private bool CheckIsCustomerFilters(FilterCollection filters)
        {
            if (!Equals(filters, null) && ((filters.Count > 0)))
            {
                foreach (var item in filters)
                {
                    if (Equals(item.Item1.ToLower(), "iscustomer"))
                    {
                        if (item.Item3.Contains("true"))
                        {
                            return true;
                        }
                        else
                        { return false; }
                    }
                }
            }
            return false;
        }
        private bool CheckIsSuperUserFilters(FilterCollection filters)
        {
            if (!Equals(filters, null) && ((filters.Count > 0)))
            {
                foreach (var item in filters)
                {
                    if (Equals(item.Item1.ToLower(), "issuperuser"))
                    {
                        if (item.Item3.Contains("true"))
                        {
                            return true;
                        }
                        else
                        { return false; }
                    }
                }
            }
            return false;
        }
        #endregion
    }
}