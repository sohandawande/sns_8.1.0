﻿using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;
using Znode.Engine.MvcDemo.Maps;
using System.Linq;

namespace Znode.Engine.MvcAdmin.Agents
{
    public class PortalCatalogAgent : BaseAgent, IPortalCatalogAgent
    {
        #region Private Variables
        private readonly IPortalCatalogsClient _portalCatalogsClient;
        private readonly ICSSClient _cssClient;
        private readonly IThemeClient _themeClient;
        private readonly ICatalogsClient _catalogsClient;
        private readonly IPortalAgent _portalAgent;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor for portal agent.
        /// </summary>
        public PortalCatalogAgent()
        {
            _portalCatalogsClient = GetClient<PortalCatalogsClient>();
            _cssClient = GetClient<CSSClient>();
            _themeClient = GetClient<ThemeClient>();
            _catalogsClient = GetClient<CatalogsClient>();
            _portalAgent = new PortalAgent();
        }
        #endregion

        #region Public Methods
        public PortalCatalogListViewModel GetPortalCatalogList(int portalId, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            PortalCatalogListViewModel portalCatalogListViewModel = new PortalCatalogListViewModel();
            if (Equals(filters, null))
            {
                filters = new FilterCollection();                
            }
            FilterTuple portalCatalogFilter = new FilterTuple(FilterKeys.PortalId, FilterOperators.Equals, portalId.ToString());
            filters.Add(portalCatalogFilter);

            Expands = new ExpandCollection();
            Expands.Add(ExpandKeys.Portal);
            Expands.Add(ExpandKeys.Catalog);

            _portalCatalogsClient.RefreshCache = true;
            PortalCatalogListModel list = _portalCatalogsClient.GetPortalCatalogs(Expands, filters, sortCollection, pageIndex - 1, recordPerPage);

            if (!Equals(list.PortalCatalogs, null) && list.PortalCatalogs.Count > 0)
            {
                portalCatalogListViewModel = PortalCatalogViewModelMap.ToListViewModel(list, pageIndex, recordPerPage);
            }
            return portalCatalogListViewModel;
        }

        public PortalCatalogViewModel GetPortalCatalog(int portalCatalogId)
        {
            PortalCatalogViewModel portalCatalogViewModel = new PortalCatalogViewModel();

            Expands = new ExpandCollection();
            Expands.Add(ExpandKeys.Portal);
            Expands.Add(ExpandKeys.Catalog);

            _portalCatalogsClient.RefreshCache = true;
            var portalCatalog = _portalCatalogsClient.GetPortalCatalog(portalCatalogId, Expands);

            if (!Equals(portalCatalog, null))
            {
                portalCatalogViewModel = PortalCatalogViewModelMap.ToViewModel(portalCatalog);
                portalCatalogViewModel.Portal = _portalAgent.GetPortalInformationByPortalId(portalCatalogViewModel.Portal.PortalId);
                portalCatalogViewModel.LocaleId = portalCatalogViewModel.Portal.LocaleId;
                portalCatalogViewModel.PortalId = portalCatalogViewModel.Portal.PortalId;
            }

            return portalCatalogViewModel;
        }

        public PortalCatalogViewModel GetPortalCatalogWithDropDowns(int portalCatalogId)
        {
            if (portalCatalogId > 0)
            {
                PortalCatalogViewModel portalCatalogViewModel = GetPortalCatalog(portalCatalogId);
                if (!Equals(portalCatalogViewModel, null))
                {
                    BindPortalCatalogInformation(portalCatalogViewModel);
                }
                return portalCatalogViewModel;
            }
            return new PortalCatalogViewModel();
        }

        public void BindPortalCatalogInformation(PortalCatalogViewModel portalCatalogViewModel)
        {
            portalCatalogViewModel.Portal.Themes = PortalViewModelMap.ToSelectListItems(_themeClient.GetThemes(Filters, new SortCollection()).Themes);
            portalCatalogViewModel.Portal.CSS = this.BindCssList();
            portalCatalogViewModel.Portal.Catalogs = PortalViewModelMap.ToSelectListItems(_catalogsClient.GetCatalogs(Filters, new SortCollection()).Catalogs);
        }

        public bool UpdatePortalCatalog(PortalCatalogViewModel portalcatalogViewModel)
        {
            var portalCatalogModel = _portalCatalogsClient.UpdatePortalCatalog(portalcatalogViewModel.PortalCatalogId, PortalCatalogViewModelMap.ToModel(portalcatalogViewModel));
            if (!Equals(portalCatalogModel, null) && portalCatalogModel.PortalCatalogId > 0)
            {
                return true;
            }
            return false;
        }

        public List<PortalCatalogViewModel> GetCatalogListByUserName()
        {
            FilterCollection filterCollection = new FilterCollection();
            filterCollection.Add(new FilterTuple(FilterKeys.Username, FilterOperators.Contains, HttpContext.Current.User.Identity.Name));

            Expands = new ExpandCollection();
            Expands.Add(ExpandKeys.Catalog);

            PortalCatalogListViewModel list = PortalCatalogViewModelMap.ToListViewModel(_portalCatalogsClient.GetPortalCatalogs(Expands, filterCollection, null), null, null);
            return (!Equals(list, null) && !Equals(list.PortalCatalogs, null)) ? list.PortalCatalogs = list.PortalCatalogs.GroupBy(x => x.CatalogId).Select(x => x.First()).ToList() : new List<PortalCatalogViewModel>();
        }
        #endregion

        #region Private Methods
        private List<SelectListItem> BindCssList(FilterCollection filters = null, SortCollection sortCollection = null)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            if (!Equals(filters, null))
            {
                _cssClient.RefreshCache = true;
                CSSListModel cssModel = _cssClient.GetCSSs(null, filters, sortCollection);
                return PortalViewModelMap.ToSelectListItems(cssModel.CSSs);
            }
            return list;
        }
        #endregion
    }
}