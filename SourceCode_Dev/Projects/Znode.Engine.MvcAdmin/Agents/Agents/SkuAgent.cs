﻿using Resources;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Web.Mvc;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;
using Znode.Engine.MvcAdmin.Helpers;

namespace Znode.Engine.MvcAdmin.Agents
{
    public class SkuAgent : BaseAgent, ISkuAgent
    {
        #region Private Variables
        private readonly ISkusClient _client; 
        #endregion

        #region Constructor
        public SkuAgent()
        {
            _client = GetClient<SkusClient>();
        } 
        #endregion

        #region Public Methods
        public Decimal GetSkusPrice(int productId, int attributeId, decimal productPrice, out string selectedSku, out string imagePath, out string imageMediumPath)
        {
            decimal SKUPrice = productPrice;

            Expands = new ExpandCollection();
            Expands.Add(ExpandKeys.Attributes);
            Expands.Add(ExpandKeys.Promotions);
            Filters = new FilterCollection();
            Filters.Add(FilterKeys.ProductId, FilterOperators.Equals, productId.ToString());
            var list = _client.GetSkus(Expands, Filters, null, null, null);

            if (list != null)
            {
                foreach (var sku in list.Skus)
                {
                    var attributeSelected =
                        sku.Attributes.FirstOrDefault(x => x.AttributeId.Equals(attributeId));
                    if (attributeSelected != null)
                    {
                        selectedSku = sku.Sku;
                        imagePath = HelperMethods.GetImagePath(sku.ImageLargePath);
                        //ZNode Version 7.2.2 - Assign SkU Medium image Path
                        imageMediumPath = HelperMethods.GetImagePath(sku.ImageMediumPath);

                        if (sku.PromotionPrice.HasValue)
                        {
                            SKUPrice = sku.PromotionPrice.Value;
                            return SKUPrice;
                        }

                        if (sku.SalePriceOverride.HasValue)
                        {
                            SKUPrice = sku.SalePriceOverride.Value;
                            return SKUPrice;
                        }

                        if (sku.RetailPriceOverride.HasValue)
                        {
                            SKUPrice = sku.RetailPriceOverride.Value;
                            return SKUPrice;
                        }

                        return SKUPrice;
                    }
                }
            }

            selectedSku = (list != null) ? list.Skus.First().Sku : string.Empty;
            imagePath = (list != null) ? list.Skus.First().ImageLargePath : string.Empty;
            //ZNode Version 7.2.2 - Add imageMediumPath parameter
            imageMediumPath = (list != null) ? list.Skus.First().ImageMediumPath : string.Empty;
            return productPrice;
        }

        public SkuModel GetSkuInventory(int productId, string attributeId)
        {
            Expands = new ExpandCollection { ExpandKeys.Inventory, ExpandKeys.Attributes };

            Filters = new FilterCollection { { FilterKeys.ProductId, FilterOperators.Equals, productId.ToString() } };

            var list = _client.GetSkus(Expands, Filters, null, null, null);

            if (list.Skus.Count == 1 && !list.Skus[0].Attributes.Any())
            {
                return list.Skus.First();
            }

            var attributeIds = attributeId.Split(',');

            return (from sku in list.Skus
                    where attributeIds.All(x => sku.Attributes.Any(y => int.Parse(x) == y.AttributeId))
                    select sku).FirstOrDefault();

        }

        public SkuModel GetBySku(int productId, string sku)
        {
            Expands = new ExpandCollection { ExpandKeys.Inventory, ExpandKeys.Attributes };

            Filters = new FilterCollection
            {
                {FilterKeys.ProductId, FilterOperators.Equals, productId.ToString()},
                {FilterKeys.Sku, FilterOperators.Equals, sku}
            };

            var list = _client.GetSkus(Expands, Filters, null, null, null);

            return (list != null && list.Skus.Any()) ? list.Skus.First() : null;
        } 
        #endregion
    }
}