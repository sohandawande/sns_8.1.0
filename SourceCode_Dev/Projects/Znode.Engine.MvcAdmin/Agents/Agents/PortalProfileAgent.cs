﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;
using Znode.Engine.MvcDemo.Maps;

namespace Znode.Engine.MvcAdmin.Agents
{
    public class PortalProfileAgent : BaseAgent, IPortalProfileAgent
    {
        #region Private Variables
        private readonly IPortalProfileClient _portalProfileClient;
        private readonly IProfilesClient _profileClient;
        private readonly IPortalAgent _portalAgent;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor for portal profile agent.
        /// </summary>
        public PortalProfileAgent()
        {
            _portalProfileClient = GetClient<PortalProfileClient>();
            _profileClient = GetClient<ProfilesClient>();
            _portalAgent = new PortalAgent();
        }
        #endregion

        #region Public Methods
        public PortalProfileListViewModel GetPortalProfiles(int portalId, FilterCollection filterCollection = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            if(Equals(filterCollection,null))
            {
                filterCollection = new FilterCollection();
            }
            filterCollection.Add(new FilterTuple(FilterKeys.PortalId, FilterOperators.Equals, portalId.ToString()));

            PortalProfileListViewModel portalProfileListModel = PortalProfileViewModelMap.ToListViewModel(_portalProfileClient.GetPortalProfilesDetails(Expands, filterCollection, sortCollection, pageIndex, recordPerPage), pageIndex, recordPerPage);
            PortalViewModel portal = _portalAgent.GetPortalInformationByPortalId(portalId);

            foreach (var portalProfile in portalProfileListModel.PortalProfiles)
            {
                portalProfile.IsCurrentAnonymousProfile = (portal.DefaultAnonymousProfileId.Equals(portalProfile.ProfileID));
                portalProfile.IsCurrentRegisteredProfile = (portal.DefaultRegisteredProfileId.Equals(portalProfile.ProfileID));
            }
            return portalProfileListModel;
        }

        public bool CreatePortalProfile(PortalProfileViewModel portalProfileViewModel)
        {
            if (!Equals(portalProfileViewModel, null))
            {
                var portalProfile = _portalProfileClient.CreatePortalProfile(PortalProfileViewModelMap.ToModel(portalProfileViewModel));
                if (!Equals(portalProfile, null) && portalProfile.PortalProfileID > 0)
                {
                    return true;
                }
            }
            return false;
        }

        public bool DeletePortalProfile(int portalProfileId)
        {
            if (portalProfileId > 0)
            {
                return _portalProfileClient.DeletePortalProfile(portalProfileId);
            }
            return false;
        }

        public bool UpdatePortalProfile(PortalProfileViewModel portalProfileViewModel)
        {
            PortalProfileViewModel model = new PortalProfileViewModel();
            if (portalProfileViewModel.PortalProfileID > 0)
            {
                model = portalProfileViewModel = PortalProfileViewModelMap.ToViewModel(_portalProfileClient.UpdatePortalProfile(model.PortalProfileID, PortalProfileViewModelMap.ToModel(portalProfileViewModel)));
            }

            if (model.PortalProfileID > 0 && !Equals(model, null))
            {
                return true;
            }
            return false;
        }

        public PortalProfileViewModel GetPortalProfile(int portalProfileId)
        {
            _portalProfileClient.RefreshCache = true;
            var portalProfile = _portalProfileClient.GetPortalProfile(portalProfileId);
            if (!Equals(portalProfile, null))
            {
                return PortalProfileViewModelMap.ToViewModel(portalProfile);
            }
            return null;
        }

        public void BindPortalProfileInformation(PortalProfileViewModel portalProfileViewModel, int profileId)
        {
            portalProfileViewModel.Profiles = PortalViewModelMap.ToSelectListItems(_profileClient.GetProfilesByProfileId(profileId).Profiles);
        }

        public void BindExistingPortalProfileInformation(int profileId, PortalProfileViewModel portalProfileViewModel)
        {
            ProfileViewModel profile = ProfileViewModelMap.ToViewModel(_profileClient.GetProfile(profileId));
            SelectListItem item = new SelectListItem { Text = profile.Name, Value = profile.ProfileId.ToString() };
            List<SelectListItem> profiles = new List<SelectListItem>();
            profiles.Add(item);
            portalProfileViewModel.Profiles = profiles;
        }

        #endregion
    }
}