﻿using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public class AttributeTypesAgent : BaseAgent, IAttributeTypesAgent
    {
        #region Private Variables
        private readonly IAttributeTypesClient _attributeTypeClient;
        #endregion

        #region Constructors
        public AttributeTypesAgent()
        {
            _attributeTypeClient = GetClient<AttributeTypesClient>();
        }
        #endregion

        #region Public Methods

        public AttributeTypesListViewModel GetAttributesType(FilterCollection filters, SortCollection sortCollection, int? pageIndex, int? recordPerPage)
        {
            _attributeTypeClient.RefreshCache = true;
            AttributeTypeListModel list = _attributeTypeClient.GetAttributeTypes(filters, sortCollection, pageIndex - 1, recordPerPage);
            return AttributeTypesViewModelMap.ToListViewModel(list);
        }

        public AttributeTypesViewModel GetAttributeTypeById(int attributeTypeId)
        {
            _attributeTypeClient.RefreshCache = true;
            var attributesType = _attributeTypeClient.GetAttributeType(attributeTypeId);

            return Equals(attributesType, null) ? null : AttributeTypesViewModelMap.ToViewModel(attributesType);
        }

        public bool SaveAttributeType(AttributeTypesViewModel model)
        {
            var attributeType = _attributeTypeClient.CreateAttributeType(AttributeTypesViewModelMap.ToModel(model));
            return (attributeType.AttributeTypeId > 0);
        }

        public bool UpdateAttributeType(AttributeTypesViewModel model)
        {
            var attributeType = _attributeTypeClient.UpdateAttributeType(model.AttributeTypeId, AttributeTypesViewModelMap.ToModel(model));
            return (attributeType.AttributeTypeId > 0);
        }

        public bool DeleteAttributeType(int attributeTypeId, out string errorMessage)
        {
            try
            {
                errorMessage = string.Empty;
                return _attributeTypeClient.DeleteAttributeType(attributeTypeId);
            }
            catch (ZnodeException ex)
            {
                errorMessage = ex.ErrorMessage;
                return false;
            }
        }

        public AttributeTypesViewModel GetAttributeTypeList()
        {
            AttributeTypesViewModel model = new AttributeTypesViewModel();
            _attributeTypeClient.RefreshCache = true;
            model.GetAttributesList = AttributeTypesViewModelMap.ToListItems(_attributeTypeClient.GetAttributeTypes(Filters, Sorts, null, null).AttributeTypes);
            return model;
        }

        #endregion
    }
}