﻿using System.Collections.ObjectModel;
using System.Configuration;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public partial class CustomerAgent : BaseAgent, ICustomerAgent
    {
        #region Private Readonly Variables
        private readonly string salesRepRoleName = ConfigurationManager.AppSettings["SalesRepRoleName"];                
        #endregion        

        public Collection<PRFTSalesRepresentative> GetSalesRep()
        {

            var list = _accountsClient.GetSalesRepInfoByRoleName(salesRepRoleName);
            return list;
        }

        #region CustomerUserMapping
        public CustomerListViewModel GetAssociatedCustomerList(int accountId, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            filters.Add(new FilterTuple(FilterKeys.AccountId, FilterOperators.Equals, accountId.ToString()));           
            var customerList = _customerClient.GetAssociatedCustomer(accountId, null, filters, sortCollection, pageIndex, recordPerPage);
            if (!Equals(customerList, null))
            {
                return CustomerViewModelMap.ToListViewModel(customerList.CustomerList, customerList.TotalResults, customerList.PageSize, customerList.PageIndex, customerList.TotalPages);
            }           
            return new CustomerListViewModel();
        }

        public bool DeleteUserAssociatedCustomer(int customerUserMappingId)
        {
            return _customerUserMappingClient.DeleteUserAssociatedCustomer(customerUserMappingId);
        }

        public CustomerListViewModel GetNotAssociatedCustomerList(int accountId, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            filters.Add(new FilterTuple(FilterKeys.AccountId, FilterOperators.Equals, accountId.ToString()));

            ReplaceFilterKeyName(ref filters, MvcAdminConstants.AddressFirstName, FilterKeys.FirstName);
            ReplaceFilterKeyName(ref filters, MvcAdminConstants.AddressLastName, FilterKeys.LastName);
            ReplaceFilterKeyName(ref filters, MvcAdminConstants.AddressPortalCode, FilterKeys.PostalCode);
            ReplaceFilterKeyName(ref filters, MvcAdminConstants.StartDate, FilterKeys.CreateDate);
            ReplaceFilterKeyName(ref filters, MvcAdminConstants.EndDate, FilterKeys.CreateDate);
            ReplaceFilterKeyName(ref filters, MvcAdminConstants.LabelValue, FilterKeys.ReferralStatus);
            ReplaceFilterKeyName(ref filters, Email, MvcAdminConstants.Email);
            ReplaceSortKeyName(sortCollection, MvcAdminConstants.LoginName, MvcAdminConstants.UserName);
            ReplaceFilterKeyName(ref filters, MvcAdminConstants.ExternalId, FilterKeys.ExternalAccountId);
            this.RemoveFilters(filters, Quantity);
            this.RemoveFilters(filters, Guid);

            var customerList = _customerClient.GetNotAssociatedCustomer(accountId, null, filters, sortCollection, pageIndex, recordPerPage);
            if (!Equals(customerList, null))
            {
                return CustomerViewModelMap.ToListViewModel(customerList.CustomerList, customerList.TotalResults, customerList.PageSize, customerList.PageIndex, customerList.TotalPages);
            }
            return new CustomerListViewModel();
        }

        public bool SaveAssociateCustomer(int accountId, string customerAccountIds)
        {
            return Equals(_customerUserMappingClient.UserAssociatedCustomers(accountId, customerAccountIds), null) ? false : true;
        }

        public bool DeleteAllUserMapping(int userAccountId)
        {
            return Equals(_customerUserMappingClient.DeleteAllUserMapping(userAccountId), null) ? false : true;
        }
        #endregion
    }
}