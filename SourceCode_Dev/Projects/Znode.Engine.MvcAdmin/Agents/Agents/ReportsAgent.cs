﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Enum;
using Znode.Engine.MvcAdmin.Agents.IAgents;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Models;
using Znode.Engine.MvcAdmin.ViewModels;
namespace Znode.Engine.MvcAdmin.Agents
{
    public class ReportsAgent : BaseAgent, IReportsAgent
    {
        #region Private Variables
        private readonly IReportsClient _reportsClient;
        private readonly IOrderAgent _orderAgent;
        private readonly ISupplierAgent _supplierAgent;
        private readonly IStoreAdminAgent _storeAdminAgent;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public ReportsAgent()
        {
            _reportsClient = GetClient<ReportsClient>();
            _orderAgent = new OrderAgent();
            _supplierAgent = new SuppliersAgent();
            _storeAdminAgent = new StoreAdminAgent();
        }
        #endregion

        #region Public Methods

        public ReportDataViewModel GetReportData(int reportNo, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null, int mode = 0)
        {
            string reportName = Convert.ToString((ListType)reportNo);
            SetDefaultDate(reportNo, filters, mode);
            if (Equals(filters, null))
            {
                filters = new FilterCollection();
            }
            if (!Equals(pageIndex, null) && !Equals(recordPerPage, null))
            {
                filters.Add(new FilterTuple(FilterKeys.Username, FilterOperators.Equals, "\'" + HttpContext.Current.User.Identity.Name + "\'"));
            }
            ReportDataViewModel model = ToViewModel(_reportsClient.GetReportData(reportName, null, filters, sortCollection, pageIndex, recordPerPage));

            // Bind Store List 
            if (IsDropDownShow(MvcAdminConstants.StoreName, reportNo))
            {
                model.StoreList = _storeAdminAgent.GetAllStores();
            }

            //Bind Suppliers List
            if (IsDropDownShow(MvcAdminConstants.Suppliers, reportNo))
            {
                model.SupplierList = (_supplierAgent.GetSuppliers(new FilterCollection(), null, null, null));
            }
            //Bind Order State Name List
            if (IsDropDownShow(MvcAdminConstants.OrderStateName, reportNo))
            {
                model.OrderStateList = CreateOrderStateDropDown();
            }
            //Bind Duration Name List
            if (IsDropDownShow(MvcAdminConstants.DurationName, reportNo))
            {
                model.DurationList = CreateDurationDropDown();
            }
            //Bind Category Name List
            if (IsDropDownShow(MvcAdminConstants.LabelCategoryName, reportNo))
            {
                model.CategoryList = CreateCategoryStateDropDown();
            }
            //Bind Group By Name List
            if (IsDropDownShow(MvcAdminConstants.GroupByName, reportNo))
            {
                model.GroupByList = CreateGroupByStateDropDown();
            }
            //Set report title

            DownloadHelper downloadHelper = new DownloadHelper();
            model.ReportTitle = downloadHelper.ToEnumString((ListType)reportNo);
            return model;
        }

        public static List<dynamic> GetSubRecords(string parentRecordId)
        {
            ReportsClient _objreport = new ReportsClient();
            string reportName = Convert.ToString((ListType)HttpContext.Current.Session[DynamicGridConstants.listTypeSesionKey]);
            var record = _objreport.GetOrderDetails(int.Parse(parentRecordId), reportName);
            return record.RecordSet;
        }

        public void GetGraphAxis(ReportModel reportModel)
        {
            List<dynamic> gridList = (List<dynamic>)HttpContext.Current.Session[DynamicGridConstants.ColumnListSesionKey];
            reportModel.XAxisFields = new List<SelectListItem>();
            reportModel.YAxisFields = new List<SelectListItem>();
            gridList.FindAll(x => x.xaxis.Equals("y")).ForEach(y =>
            {
                reportModel.XAxisFields.Add(new SelectListItem() { Text = y.headertext, Value = y.name });
            });

            gridList.FindAll(x => x.yaxis.Equals("y")).ForEach(y =>
            {
                reportModel.YAxisFields.Add(new SelectListItem() { Text = y.headertext, Value = y.name });
            });
        }
        #endregion

        #region Private Methods

        /// <summary>
        /// Set default date to report filers. 
        /// </summary>
        /// <param name="reportNo">Report No</param>
        /// <param name="filters">Filter collection</param>
        /// <param name="mode">mode to identify user</param>
        private void SetDefaultDate(int reportNo, FilterCollection filters, int mode)
        {
            if (mode > 0)
            {
                ReportCases(reportNo, filters);
            }
            else
            {
                ReportCasesSiteAdmin(reportNo, filters);
            }
        }

        /// <summary>
        /// Set default filers cases for site admin user.
        /// </summary>
        /// <param name="reportNo">Report No</param>
        /// <param name="filters">Filter collection</param>
        private void ReportCasesSiteAdmin(int reportNo, FilterCollection filters)
        {
            if ((!Equals(filters, null) && filters.Count.Equals(0)))
            {
                switch (reportNo)
                {
                    case (int)ListType.ZNode_ReportsCustom:
                        GetStartEndDate(filters);
                        break;
                    case (int)ListType.ZNode_ReportsActivityLog:
                        filters.Add(new FilterTuple(MvcAdminConstants.StartDateKey, FilterOperators.GreaterThanOrEqual, string.Format("{0}{1}{2}", "'", DateTime.Now.ToString(HelperMethods.GetStringDateFormat()), "'")));
                        break;
                    case (int)ListType.ZNode_ReportsAccounts:
                        GetStartEndDate(filters);
                        break;
                    case (int)ListType.ZNode_ReportsRecurring:
                        GetStartEndDate(filters);
                        break;
                    case (int)ListType.ZNode_ReportsSupplierList:
                        GetStartEndDate(filters);
                        break;
                    case (int)ListType.ZNode_ReportVendorRevenue:
                        GetStartEndDate(filters);
                        break;
                    case (int)ListType.ZNode_ReportVendorProductRevenue:
                        GetStartEndDate(filters);
                        break;
                    case (int)ListType.ZNode_ReportsProductSoldOnVendorSites:
                        GetStartEndDate(filters);
                        break;
                }
            }
            else if (reportNo.Equals((int)ListType.ZNode_ReportsPopularFiltered) || reportNo.Equals((int)ListType.ZNode_ReportsSEOFiltered) || reportNo.Equals((int)ListType.ZNode_ReportsTopEarningProduct) || reportNo.Equals((int)ListType.ZNode_ReportsTaxFiltered_All) || reportNo.Equals((int)ListType.ZNode_ReportsFrequentFiltered) || reportNo.Equals((int)ListType.ZNodeReportsCouponFiltered) || reportNo.Equals((int)ListType.ZNode_ReportsAffiliateFiltered) || reportNo.Equals((int)ListType.ZNode_ReportsVolumeFiltered))
            {
                GetReportDuration(filters);
            }
        }

        /// <summary>
        /// Set default filers cases for Mall/franchise admin user.
        /// </summary>
        /// <param name="reportNo">Report No</param>
        /// <param name="filters">Filter collection</param>
        private void ReportCases(int reportNo, FilterCollection filters)
        {
            if ((!Equals(filters, null)))
            {
                switch (reportNo)
                {
                    case (int)ListType.ZNode_ReportsCustom:
                        GetStartEndDate(filters);
                        break;
                    case (int)ListType.ZNode_ReportsActivityLog:
                        filters.Add(new FilterTuple(MvcAdminConstants.StartDateKey, FilterOperators.GreaterThanOrEqual, string.Format("{0}{1}{2}", "'", DateTime.Now.ToString(HelperMethods.GetStringDateFormat()), "'")));
                        break;
                    case (int)ListType.ZNode_ReportsAccounts:
                        GetStartEndDate(filters);
                        break;
                    case (int)ListType.ZNode_ReportsRecurring:
                        GetStartEndDate(filters);
                        break;
                    case (int)ListType.ZNode_ReportsSupplierList:
                        GetStartEndDate(filters);
                        break;
                    case (int)ListType.ZNode_ReportVendorRevenue:
                        GetStartEndDate(filters);
                        break;
                    case (int)ListType.ZNode_ReportVendorProductRevenue:
                        GetStartEndDate(filters);
                        break;
                    case (int)ListType.ZNode_ReportsProductSoldOnVendorSites:
                        GetStartEndDate(filters);
                        break;
                }
            }
            else if (reportNo.Equals((int)ListType.ZNode_ReportsPopularFiltered) || reportNo.Equals((int)ListType.ZNode_ReportsTopEarningProduct) || reportNo.Equals((int)ListType.ZNode_ReportsAffiliateFiltered) || reportNo.Equals((int)ListType.ZNode_ReportsFrequentFiltered) || reportNo.Equals((int)ListType.ZNode_ReportsVolumeFiltered))
            {
                GetReportDuration(filters);
            }
        }

        /// <summary>
        /// Get start date in filters
        /// </summary>
        /// <param name="filters"></param>
        private void GetStartDate(FilterCollection filters)
        {
            if (!Equals(filters, null))
            {
                bool isFilterPresent = false;
                filters.ToList().ForEach(filterItem =>
                {
                    if (filterItem.FilterName.Equals(MvcAdminConstants.StartDateKey))
                    {
                        isFilterPresent = true;
                    }
                });
                if (!isFilterPresent)
                {
                    filters.Add(new FilterTuple(MvcAdminConstants.StartDateKey, FilterOperators.GreaterThanOrEqual, string.Format("{0}{1}{2}", "'", DateTime.Now.AddDays(-7).ToString(HelperMethods.GetStringDateFormat()), "'")));
                }
            }
        }

        /// <summary>
        /// Get End Date in filters
        /// </summary>
        /// <param name="filters"></param>
        private void GetEndDate(FilterCollection filters)
        {
            if (!Equals(filters, null))
            {
                bool isFilterPresent = false;
                filters.ToList().ForEach(filterItem =>
                {
                    if (filterItem.FilterName.Equals(MvcAdminConstants.EndDate))
                    {
                        isFilterPresent = true;
                    }
                });
                if (!isFilterPresent)
                {
                    filters.Add(new FilterTuple(MvcAdminConstants.EndDate, FilterOperators.LessThanOrEqual, string.Format("{0}{1}{2}", "'", DateTime.Now.ToString(HelperMethods.GetStringDateFormat()), "'")));
                }
            }
        }

        /// <summary>
        /// Get start/End Date in filters
        /// </summary>
        /// <param name="filters"></param>
        private void GetStartEndDate(FilterCollection filters)
        {
            GetStartDate(filters);
            GetEndDate(filters);
        }

        /// <summary>
        /// Get report duration 
        /// </summary>
        /// <param name="filters">Filter collection</param>
        private void GetReportDuration(FilterCollection filters)
        {
            if (!Equals(filters, null))
            {
                foreach (var item in filters)
                {
                    if (item.Item1.Equals("durationid", StringComparison.InvariantCultureIgnoreCase))
                    {
                        switch (int.Parse(item.Item3))
                        {
                            case 1:
                                filters.Add(new FilterTuple(MvcAdminConstants.StartDateKey, FilterOperators.GreaterThanOrEqual, string.Format("{0}{1}{2}", "'", DateTime.Now.AddDays(-1).ToString(HelperMethods.GetStringDateFormat()), "'")));
                                filters.Add(new FilterTuple(MvcAdminConstants.EndDate, FilterOperators.LessThanOrEqual, string.Format("{0}{1}{2}", "'", DateTime.Now.ToString(HelperMethods.GetStringDateFormat()), "'")));
                                break;
                            case 2:
                                filters.Add(new FilterTuple(MvcAdminConstants.StartDateKey, FilterOperators.GreaterThanOrEqual, string.Format("{0}{1}{2}", "'", DateTime.Now.AddDays(-7).ToString(HelperMethods.GetStringDateFormat()), "'")));
                                filters.Add(new FilterTuple(MvcAdminConstants.EndDate, FilterOperators.LessThanOrEqual, string.Format("{0}{1}{2}", "'", DateTime.Now.ToString(HelperMethods.GetStringDateFormat()), "'")));
                                break;
                            case 3:
                                filters.Add(new FilterTuple(MvcAdminConstants.StartDateKey, FilterOperators.GreaterThanOrEqual, string.Format("{0}{1}{2}", "'", DateTime.Now.AddMonths(-1).ToString(HelperMethods.GetStringDateFormat()), "'")));
                                filters.Add(new FilterTuple(MvcAdminConstants.EndDate, FilterOperators.LessThanOrEqual, string.Format("{0}{1}{2}", "'", DateTime.Now.ToString(HelperMethods.GetStringDateFormat()), "'")));
                                break;
                            case 4:
                                filters.Add(new FilterTuple(MvcAdminConstants.StartDateKey, FilterOperators.GreaterThanOrEqual, string.Format("{0}{1}{2}", "'", DateTime.Now.AddMonths(-3).ToString(HelperMethods.GetStringDateFormat()), "'")));
                                filters.Add(new FilterTuple(MvcAdminConstants.EndDate, FilterOperators.LessThanOrEqual, string.Format("{0}{1}{2}", "'", DateTime.Now.ToString(HelperMethods.GetStringDateFormat()), "'")));
                                break;
                            case 5:
                                filters.Add(new FilterTuple(MvcAdminConstants.StartDateKey, FilterOperators.GreaterThanOrEqual, string.Format("{0}{1}{2}", "'", DateTime.Now.AddYears(-1).ToString(HelperMethods.GetStringDateFormat()), "'")));
                                filters.Add(new FilterTuple(MvcAdminConstants.EndDate, FilterOperators.LessThanOrEqual, string.Format("{0}{1}{2}", "'", DateTime.Now.ToString(HelperMethods.GetStringDateFormat()), "'")));
                                break;
                        }

                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Create Order State drop down list 
        /// </summary>
        /// <returns></returns>
        private List<OrderStateViewModel> CreateOrderStateDropDown()
        {
            List<OrderStateViewModel> orderState = new List<OrderStateViewModel>();
            orderState.Add(new OrderStateViewModel() { OrderStateName = MvcAdminConstants.Submitted, OrderStateId = 10 });
            orderState.Add(new OrderStateViewModel() { OrderStateName = MvcAdminConstants.Shipped, OrderStateId = 20 });
            orderState.Add(new OrderStateViewModel() { OrderStateName = MvcAdminConstants.Returned, OrderStateId = 30 });
            orderState.Add(new OrderStateViewModel() { OrderStateName = MvcAdminConstants.Cancelled, OrderStateId = 40 });
            orderState.Add(new OrderStateViewModel() { OrderStateName = MvcAdminConstants.LabelPendingApproval, OrderStateId = 50 });
            return orderState;
        }

        /// <summary>
        /// Create duration drop down list
        /// </summary>
        /// <returns></returns>
        private List<DurationViewModel> CreateDurationDropDown()
        {
            List<DurationViewModel> duration = new List<DurationViewModel>();
            duration.Add(new DurationViewModel() { DurationName = MvcAdminConstants.Day, DurationId = 1 });
            duration.Add(new DurationViewModel() { DurationName = MvcAdminConstants.Week, DurationId = 2 });
            duration.Add(new DurationViewModel() { DurationName = MvcAdminConstants.Month, DurationId = 3 });
            duration.Add(new DurationViewModel() { DurationName = MvcAdminConstants.Quarter, DurationId = 4 });
            duration.Add(new DurationViewModel() { DurationName = MvcAdminConstants.Year, DurationId = 5 });
            return duration;
        }

        /// <summary>
        /// Create category state drop down list
        /// </summary>
        /// <returns></returns>
        private List<CategoryStateViewModel> CreateCategoryStateDropDown()
        {
            List<CategoryStateViewModel> categoryState = new List<CategoryStateViewModel>();
            categoryState.Add(new CategoryStateViewModel() { Category = MvcAdminConstants.Error, CategoryId = 1 });
            categoryState.Add(new CategoryStateViewModel() { Category = MvcAdminConstants.Notice, CategoryId = 2 });
            categoryState.Add(new CategoryStateViewModel() { Category = MvcAdminConstants.Search, CategoryId = 3 });
            categoryState.Add(new CategoryStateViewModel() { Category = MvcAdminConstants.Warning, CategoryId = 4 });
            return categoryState;
        }

        /// <summary>
        /// Create groupby state drop down
        /// </summary>
        /// <returns></returns>
        private List<DurationViewModel> CreateGroupByStateDropDown()
        {
            List<DurationViewModel> groupBy = new List<DurationViewModel>();
            groupBy.Add(new DurationViewModel() { DurationName = MvcAdminConstants.Month, DurationId = 3 });
            groupBy.Add(new DurationViewModel() { DurationName = MvcAdminConstants.Quarter, DurationId = 4 });
            return groupBy;
        }

        /// <summary>
        /// Map report data model to view model.
        /// </summary>
        /// <param name="model">Report data model.</param>
        /// <returns>Returns ReportDataViewModel</returns>
        private ReportDataViewModel ToViewModel(ReportsDataModel model)
        {
            if (!Equals(model.RecordSet, null))
            {
                return new ReportDataViewModel()
                {
                    ReportDataSet = model.RecordSet,
                    RecordPerPage = (int)model.PageSize,
                    Page = (int)model.PageIndex,
                    TotalResults = (int)model.TotalResults
                };
            }
            else
            {
                return new ReportDataViewModel();
            }
        }

        /// <summary>
        /// Check report to show drop down list in filters.
        /// </summary>
        /// <param name="dropDownName">Name of drop down</param>
        /// <param name="reportNo">Report No.</param>
        /// <returns>Returns true/false</returns>
        private bool IsDropDownShow(string dropDownName, int reportNo)
        {
            bool flag = false;
            switch (dropDownName)
            {
                case MvcAdminConstants.StoreName:
                    if (!Equals(reportNo, 114) && !Equals(reportNo, 115) && !Equals(reportNo, 117))
                        flag = true;
                    break;
                case MvcAdminConstants.OrderStateName:
                    if (Equals(reportNo, 98) || Equals(reportNo, 99))
                        flag = true;
                    break;
                case MvcAdminConstants.DurationName:
                    if (Equals(reportNo, 101) || Equals(reportNo, 107) || Equals(reportNo, 105) || Equals(reportNo, 106) || Equals(reportNo, 110) || Equals(reportNo, 112) || Equals(reportNo, 117))
                        flag = true;
                    break;
                case MvcAdminConstants.LabelCategoryName:
                    if (Equals(reportNo, 109))
                        flag = true;
                    break;
                case MvcAdminConstants.GroupByName:
                    if (Equals(reportNo, 111))
                        flag = true;
                    break;
                case MvcAdminConstants.Suppliers:
                    if (Equals(reportNo, 113))
                        flag = true;
                    break;
                default:
                    flag = false;
                    break;
            }
            return flag;
        }

        public List<SelectListItem> GetFileTypes()
        {
            DownloadHelper downloadHelper = new DownloadHelper();
            List<SelectListItem> fileTypes = new List<SelectListItem>();
            fileTypes.Add(new SelectListItem { Value = Convert.ToInt32(FileTypes.Excel).ToString(), Text = downloadHelper.ToEnumString(FileTypes.Excel) });
            fileTypes.Add(new SelectListItem { Value = Convert.ToInt32(FileTypes.CSV).ToString(), Text = downloadHelper.ToEnumString(FileTypes.CSV) });
            return fileTypes;
        }

        #endregion
    }
}