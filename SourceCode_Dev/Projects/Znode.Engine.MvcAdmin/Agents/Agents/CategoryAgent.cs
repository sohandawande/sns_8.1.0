﻿using Resources;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;


namespace Znode.Engine.MvcAdmin.Agents
{
    public class CategoryAgent : BaseAgent, ICategoryAgent
    {
        #region Private Variables
        private readonly ICategoriesClient _categoryClient;
        private readonly ICategoryNodesClient _categoryNodeClient;
        private readonly ICatalogsClient _catalogClient;
        private readonly IThemeClient _themeClient;
        private readonly ICSSClient _cssClient;
        private readonly IMasterPageClient _masterPageClient;
        private readonly IProfilesClient _profileClient;
        private readonly IProductsClient _productClient;
        private readonly IProductCategoriesClient _productCategoryClient;
        private readonly ISearchClient _searchClient;
        private string imageExtension = "ImageExtension";
        private bool isProcessComplete = false;
        #endregion

        #region Constructor
        public CategoryAgent()
        {
            _categoryClient = GetClient<CategoriesClient>();
            _categoryNodeClient = GetClient<CategoryNodesClient>();
            _catalogClient = GetClient<CatalogsClient>();
            _themeClient = GetClient<ThemeClient>();
            _cssClient = GetClient<CSSClient>();
            _masterPageClient = GetClient<MasterPageClient>();
            _profileClient = GetClient<ProfilesClient>();
            _productClient = GetClient<ProductsClient>();
            _productCategoryClient = GetClient<ProductCategoriesClient>();
            _searchClient = GetClient<SearchClient>();
        }
        #endregion

        #region Public Methods
        public CategoryListViewModel GetCategories()
        {
            _categoryClient.RefreshCache = true;
            var list = _categoryClient.GetCategories(new ExpandCollection { ExpandKeys.Portal, ExpandKeys.Profiles, ExpandKeys.PromotionType }, null, null);
            return (!Equals(list, null)) ? CategoryViewModelMap.ToListViewModel(list) : new CategoryListViewModel();
        }

        public CatalogAssociatedCategoriesListViewModel GetCategoryList(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            if (Equals(filters, null))
            {
                filters = new FilterCollection();
            }
            filters.Add(new FilterTuple(FilterKeys.Username, FilterOperators.Contains, HttpContext.Current.User.Identity.Name));
            _categoryClient.RefreshCache = true;
            CatalogAssociatedCategoriesListModel modelList = _categoryClient.GetAllCategories(null, filters, sortCollection, pageIndex, recordPerPage);
            return (Equals(modelList, null)) ? new CatalogAssociatedCategoriesListViewModel() : CategoryViewModelMap.ToListViewModel(modelList);
        }


        public List<SelectListItem> GetCatalogList(CatalogAssociatedCategoriesViewModel model)
        {
            _catalogClient.RefreshCache = true;
            List<SelectListItem> catalogList = CategoryViewModelMap.ToListItems(_catalogClient.GetCatalogs(Filters, new SortCollection()).Catalogs);
            return (!Equals(catalogList, null)) ? catalogList : new List<SelectListItem>();
        }

        public CatalogAssociatedCategoriesListViewModel GetCategoryByCataLogId(int catalogId, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            _categoryClient.RefreshCache = true;
            filters.Add(new FilterTuple(FilterKeys.CatalogId, FilterOperators.Equals, catalogId.ToString()));
            CatalogAssociatedCategoriesListModel modelList = _categoryClient.GetCategoryByCatalogIdFromCustomService(catalogId, filters, sortCollection, pageIndex, recordPerPage);
            return (Equals(modelList, null)) ? new CatalogAssociatedCategoriesListViewModel() : CategoryViewModelMap.ToListViewModel(modelList);
        }

        public CategoryListViewModel GetCategoryByCatalogId(int catalogId)
        {
            _categoryClient.RefreshCache = true;
            //Gets the categories on catalog Id.

            var data = Equals(catalogId, 0) ? _categoryClient.GetCategories(null, null, null) : _categoryClient.GetCategoriesByCatalog(catalogId, new ExpandCollection(), Filters, new SortCollection());
            return CategoryViewModelMap.ToViewModel(data.Categories);
        }

        public CategoryViewModel GetCategoryByCategoryId(int categoryId)
        {
            _categoryClient.RefreshCache = true;
            var data = _categoryClient.GetCategory(categoryId, new ExpandCollection { ExpandKeys.CategoryProfiles });
            return CategoryViewModelMap.ToViewModel(data);
        }

        public CategoryTreeListViewModel GetCategoryTree(int catalogId, string categoryName)
        {
            _categoryClient.RefreshCache = true;

            var list = _categoryClient.GetCategoryTree(new ExpandCollection() { ExpandKeys.CategoryNodes, ExpandKeys.CategoryProfiles }, new FilterCollection()
                        {
                            new FilterTuple(FilterKeys.CatalogId, FilterOperators.Equals,Convert.ToString(catalogId)),
                            new FilterTuple(FilterKeys.CategoryName, FilterOperators.Equals,categoryName),

                        }, null, null, null);


            if (list.Categories != null)
            {
                var tcategories =
                    list.Categories.Where(
                        p =>
                        p.CategoryProfiles.Any(y => y.ProfileID == 1 && y.EffectiveDate > System.DateTime.Now))
                              .ToList();


                CategoryTreeListViewModel Categorylist = CategoryViewModelMap.ToViewModel(list.Categories.Except(tcategories), catalogId);
                Categorylist = CategoryProductList(Categorylist, list);
                return Categorylist;
            }
            return new CategoryTreeListViewModel();
        }

        public CategoryTreeListViewModel GetCategoryProducts(int categoryId)
        {
            _categoryClient.RefreshCache = true;
            var list = this.GetProductsByCategoryId(categoryId, null, null, 1, null);
            if (!Equals(list, null))
            {
                ImageHelper imageHelper = new ImageHelper();
                CategoryTreeListViewModel viewModel = new CategoryTreeListViewModel()
                {
                    Categories = (from item in list.Products
                                  select new CategoryHeaderViewModel()
                                  {
                                      id = item.ProductId,
                                      text = item.Name,
                                      icon = item.ImageFile,
                                      children = null
                                  }).ToList()
                };
                return viewModel;
            }
            return new CategoryTreeListViewModel();
        }

        public CatalogAssociatedCategoriesListViewModel GetAllCategories(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            _categoryClient.RefreshCache = true;
            CatalogAssociatedCategoriesListModel modelList = _categoryClient.GetAllCategories(null, filters, sortCollection, pageIndex, recordPerPage);
            return (Equals(modelList, null)) ? new CatalogAssociatedCategoriesListViewModel() : CategoryViewModelMap.ToListViewModel(modelList);
        }

        public ProductCategoryViewModel GetProductCategory(int productId, int categoryId)
        {
            _productCategoryClient.RefreshCache = true;
            var data = _productCategoryClient.GetProductCategory(productId, categoryId);
            return (Equals(data, null)) ? new ProductCategoryViewModel() : ProductCategoryViewModelMap.ToViewModel(data);
        }

        public List<SelectListItem> BindParentCategoryNodes(int catalogId, FilterCollection filters = null)
        {
            _categoryNodeClient.RefreshCache = true;
            CategoryNodeListModel categoryModel = _categoryNodeClient.GetParentCategoryNodes(catalogId, filters, null, 1, 100);
            return (Equals(categoryModel, null)) ? new List<SelectListItem>() : CategoryViewModelMap.ToParentCategorySelectListItems(categoryModel.CategoryNodes);
        }

        public List<SelectListItem> BindThemeList(int themeId = 0)
        {
            _themeClient.RefreshCache = true;
            ThemeListModel themeModel = _themeClient.GetThemes(Filters, Sorts);
            return (!Equals(themeModel, null) && !Equals(themeModel.Themes, null)) ? CategoryViewModelMap.ToThemeSelectListItems(themeModel.Themes, themeId) : new List<SelectListItem>();
        }

        public List<SelectListItem> BindCssList(int CssThemeId = 0, int cssId = 0)
        {
            _cssClient.RefreshCache = true;
            FilterCollection Filters = new FilterCollection();
            if (CssThemeId > 0)
                Filters.Add(new FilterTuple(FilterKeys.CSSThemeId, FilterOperators.Equals, CssThemeId.ToString()));

            CSSListModel cssModel = _cssClient.GetCSSs(null, Filters, Sorts);
            return (Equals(cssModel, null)) ? new List<SelectListItem>() : CategoryViewModelMap.ToCssSelectListItems(cssModel.CSSs, cssId);
        }

        public List<SelectListItem> BindMasterPageList(int themeId = 0, string pageType = "", int masterPageId = 0)
        {
            _masterPageClient.RefreshCache = true;
            MasterPageListModel masterPageList = _masterPageClient.GetMasterPageByThemeId(themeId, pageType);
            return Equals(masterPageList, null) ? new List<SelectListItem>() : CategoryViewModelMap.ToMasterPageSelectListItems(masterPageList.MasterPages, masterPageId);
        }

        public List<SelectListItem> BindProfiles(int profileId = 0)
        {
            _profileClient.RefreshCache = true;
            ProfileListModel profileList = _profileClient.GetProfiles(null, Filters, null);
            return (Equals(profileList, null)) ? new List<SelectListItem>() : CategoryViewModelMap.ToProfileSelectListItems(profileList.Profiles, profileId);
        }

        public string GetProfileName(int profileId)
        {
            _profileClient.RefreshCache = true;
            ProfileModel profileModel = _profileClient.GetProfile(profileId);
            return Equals(profileModel, null) ? null : profileModel.Name;
        }

        public bool CreateCategory(CategoryViewModel model, out string errorMessage)
        {
            try
            {
                errorMessage = string.Empty;
                if (!string.IsNullOrEmpty(model.SeoUrl) && _searchClient.IsRestrictedSeoUrl(model.SeoUrl))
                {
                    errorMessage = ZnodeResources.ErrorRestrictedSeoUrlExists;
                    return false;
                }

                CategoryModel category = _categoryClient.CreateCategory(CategoryViewModelMap.ToModel(model));
                if (!Equals(category, null) && category.CategoryId > 0)
                {
                    model.CategoryId = category.CategoryId;
                    this.SaveImage(model);
                }
                return category.CategoryId > 0;
            }
            catch (ZnodeException ex)
            {
                errorMessage = ex.ErrorMessage;
                return false;
            }
        }

        public CategoryViewModel GetCategory(int categoryId)
        {
            _categoryClient.RefreshCache = true;
            var category = _categoryClient.GetCategory(categoryId);
            return (Equals(category, null)) ? new CategoryViewModel() : CategoryViewModelMap.ToViewModel(category);
        }

        public bool UpdateCategory(CategoryViewModel model, out string errorMessage)
        {
            try
            {
                errorMessage = string.Empty;
                if (!string.IsNullOrEmpty(model.SeoUrl) && _searchClient.IsRestrictedSeoUrl(model.SeoUrl))
                {
                    errorMessage = ZnodeResources.ErrorRestrictedSeoUrlExists;
                    return false;
                }

                var category = _categoryClient.UpdateCategory(model.CategoryId, CategoryViewModelMap.ToModel(model));
                model.CategoryId = category.CategoryId;
                if (!Equals(category, null) && category.CategoryId > 0)
                    this.SaveImage(model);

                return category.CategoryId > 0;
            }
            catch (ZnodeException ex)
            {
                errorMessage = ex.ErrorMessage;
                return false;
            }
        }

        public bool DeleteCategory(int categoryId, out string message)
        {
            try
            {
                message = string.Empty;
                return _categoryClient.DeleteCategory(categoryId);
            }
            catch (ZnodeException ex)
            {
                message = ex.ErrorMessage;
                return false;
            }
        }

        public bool AssociateCategory(CategoryNodeViewModel model)
        {
            return Equals(_categoryNodeClient.CreateCategoryNode(CategoryNodeViewModelMap.ToModel(model)), null) ? false : true;
        }

        public bool CategoryAssociatedProducts(int categoryId, string productIds, out string errorMessage)
        {
            try
            {
                errorMessage = string.Empty;
                return _categoryClient.CategoryAssociatedProducts(categoryId, productIds);
            }
            catch (ZnodeException ex)
            {
                errorMessage = ex.ErrorMessage;
                return false;
            }
        }

        public ProductListViewModel GetProductsByCategoryId(int categoryId, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            _categoryClient.RefreshCache = true;
            if (Equals(filters, null))
            {
                filters = new FilterCollection();
            }
            filters.Add(FilterKeys.CategoryId, FilterOperators.Equals, categoryId.ToString());
            return ProductViewModelMap.ToListViewModel(_categoryClient.GetCategoryAssociatedProducts(new ExpandCollection(), filters, sortCollection, pageIndex, recordPerPage));
        }

        public CategoryViewModel GetCategoryByName(string categoryName)
        {
            var categoryList = _categoryClient.GetCategories(null, new FilterCollection()
                        {
                            new FilterTuple(FilterKeys.Name, FilterOperators.Equals,categoryName),

                        }, null);

            if (!Equals(categoryList, null) && !Equals(categoryList.Categories, null))
            {
                var categoryDetails = CategoryViewModelMap.ToCategoryViewModel(categoryList.Categories.First());
                if (categoryDetails != null)
                {
                    categoryDetails.Title =
                          !string.IsNullOrEmpty(categoryDetails.SeoTitle)
                                 ? categoryDetails.SeoTitle
                                 : categoryName;
                    return categoryDetails;
                }
            }

            return new CategoryViewModel() { HasError = true, ErrorMessage = ZnodeResources.ErrorNoCategoryFound };
        }
        #endregion

        #region DropDown For Catalog Search
        public List<CatalogModel> GetcatalogList()
        {
            FilterCollection filters = new FilterCollection();
            filters.Add(new FilterTuple(FilterKeys.Username, FilterOperators.Contains, HttpContext.Current.User.Identity.Name));
            filters.Add(new FilterTuple(FilterKeys.IsCategory, FilterOperators.Equals, "true"));
            _catalogClient.RefreshCache = true;
            CatalogListModel catalogList = _catalogClient.GetCatalogs(filters, null);
            return (!Equals(catalogList, null) && !Equals(catalogList.Catalogs, null)) ? catalogList.Catalogs.ToList() : new List<CatalogModel>();
        }
        #endregion

        #region SEO Category

        public string UpdateCategorySEODetails(ManageSEOCategoryViewModel model)
        {
            var category = _categoryClient.UpdateCategorySEODetails(model.CategoryId, ManageSEOViewModelMap.ToModel(model));
            return category.Custom1;
        }

        #endregion

        #region Private Methods
        /// <summary>
        /// To save image by Id in data folder and update image name in database table
        /// </summary>
        /// <param name="model">ProductViewModel model</param>
        private void SaveImage(CategoryViewModel model)
        {
            if (!Equals(model.SelectImage, null))
            {
                //to save image in data folder
                ImageHelper imageHelper = new ImageHelper();
                string validExtension = Convert.ToString(ConfigurationManager.AppSettings[imageExtension]);
                string imagePath = imageHelper.SaveImageToPhysicalPathByName(null, null, validExtension, null, model.SelectImage, null);

                if (!string.IsNullOrEmpty(imagePath) && !Equals(imagePath, MvcAdminConstants.FileUploadErrorCode))
                {
                    //to update image path in database by id
                    UpdateImageModel imageModel = new UpdateImageModel();
                    imageModel.Id = model.CategoryId;
                    imageModel.EntityName = Convert.ToString(EntityName.Category);
                    imageModel.ImagePath = imagePath;
                    _productClient.UpdateImage(imageModel);
                }
            }
        }
        #endregion

        public CategoryTreeListViewModel AssignCategoryProduct(CategoryTreeListViewModel categories, CategoryTreeListViewModel products, int categoryId)
        {
            isProcessComplete = false;
            foreach (var category in categories.Categories)
            {
                if (isProcessComplete)
                {
                    return categories;
                }
                if (category.id.Equals(categoryId))
                {
                    if (Equals(category.children, null))
                    {
                        category.children = new List<CategorySubHeaderViewModel>();
                    }
                    foreach (var item in products.Categories)
                    {
                        category.children.Add(new CategorySubHeaderViewModel { id = item.id, text = item.text, icon = "/Content/themes/base/images/product.png" });
                    }
                }
                else if (!Equals(category.children, null) && category.children.Count > 0)
                {
                    this.FindChildCategory(category.children, products, categoryId);
                }

            }
            return categories;
        }

        private void FindChildCategory(List<CategorySubHeaderViewModel> subCategory, CategoryTreeListViewModel products, int categoryId)
        {
            foreach (var childCategory in subCategory)
            {
                if (childCategory.id.Equals(categoryId))
                {
                    if (Equals(childCategory.children, null))
                    {
                        childCategory.children = new List<CategorySubHeaderViewModel>();
                    }
                    foreach (var item in products.Categories)
                    {
                        childCategory.children.Add(new CategorySubHeaderViewModel { id = item.id, text = item.text, icon = "/Content/themes/base/images/product.png" });
                    }
                    isProcessComplete = true;
                    break;
                }
                else if (!Equals(childCategory.children, null) && childCategory.children.Count > 0)
                {
                    FindChildCategory(childCategory.children, products, categoryId);
                }
            }
        }

        private CategoryTreeListViewModel CategoryProductList(CategoryTreeListViewModel categories, CategoryListModel list)
        {
            if (Equals(list, null) || Equals(list.Categories, null))
                return new CategoryTreeListViewModel();

            foreach (var category in list.Categories)
            {
                if (category.ProductCount > 0)
                {
                    CategoryTreeListViewModel products = GetCategoryProducts(category.CategoryId);
                    categories = AssignCategoryProduct(categories, products, category.CategoryId);
                }
            }
            return categories;
        }
    }
}