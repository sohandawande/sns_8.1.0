﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.Models;
using Znode.Engine.MvcAdmin.ViewModels;
using Znode.Libraries.Helpers.Extensions;

namespace Znode.Engine.MvcAdmin.Agents
{
    public class PaymentAgent : BaseAgent, IPaymentAgent
    {
        #region Private Variables
        private readonly IPaymentOptionsClient _paymentOptionClient;
        private readonly ICategoryAgent _categoryAgent;
        private readonly IPaymentTypesClient _paymentTypesClient;
        private readonly IPaymentGatewaysClient _paymentGatewaysClient;
        private readonly IProfilesClient _profileClient;
        private readonly IStoreAdminAgent _storeAdminAgent;
        private readonly IAccountAgent _accountAgent;
        #endregion

        #region Private Constants
        private string paymentAppUrl = ConfigurationManager.AppSettings["PaymentApplicationUrl"].ToString();
        private const string configurePaymentUrl = "payment/ConfigurePaymentSettings";
        private const string getPaymentSettingDetails = "payment/GetPaymentSettingDetails?paymentSettingId={0}";
        private const string deletePaymentUrl = "payment/deletepaymentsettings?paymentSettingId={0}";
        private const string jsonContentType = "application/json";
        private const string acceptType = "text/xml";
        private const string paymentAppSuccessMessage = "success";
        private const string payPalUrl = "payment/paypal?callback=?";
        private const string payNowUrl = "payment/paynow?callback=?";
        #endregion

        #region Constructor

        public PaymentAgent()
        {
            _paymentOptionClient = GetClient<PaymentOptionsClient>();
            _paymentTypesClient = GetClient<PaymentTypesClient>();
            _paymentGatewaysClient = GetClient<PaymentGatewaysClient>();
            _profileClient = GetClient<ProfilesClient>();
            _categoryAgent = new CategoryAgent();
            _storeAdminAgent = new StoreAdminAgent();
            _accountAgent = new AccountAgent();
            if (!string.IsNullOrEmpty(paymentAppUrl) && !paymentAppUrl.Trim().EndsWith("/"))
            {
                paymentAppUrl = string.Format("{0}/", paymentAppUrl);
            }
        }

        #endregion

        #region Public Methods

        public PaymentOptionListViewModel GetPaymentOptions(FilterCollectionDataModel filterModel)
        {
            _paymentOptionClient.RefreshCache = true;
            filterModel.Filters.Add(new FilterTuple(FilterKeys.Username, FilterOperators.Contains, HttpContext.Current.User.Identity.Name));
            PaymentOptionListModel paymentOptions = _paymentOptionClient.GetPaymentOptions(new ExpandCollection { ExpandKeys.PaymentGateway, ExpandKeys.PaymentType }, filterModel.Filters, filterModel.SortCollection, filterModel.Page - 1, filterModel.RecordPerPage);
            return (!Equals(paymentOptions, null) && !Equals(paymentOptions.PaymentOptions, null)) ? PaymentOptionViewModelMap.ToListViewModel(paymentOptions) : new PaymentOptionListViewModel();
        }

        public PaymentOptionViewModel GetPaymentOption(int paymentOptionId)
        {
            _paymentOptionClient.RefreshCache = true;
            PaymentOptionModel paymentOption = _paymentOptionClient.GetPaymentOption(paymentOptionId, new ExpandCollection { ExpandKeys.PaymentGateway, ExpandKeys.PaymentType });
            PaymentOptionViewModel model = PaymentOptionViewModelMap.ToViewModel(paymentOption);
            model = GetPaymentSettingDetails(model);
            model.PaymentTypes = GetPaymentTypesListItems();
            model.PaymentGateways = GetpaymentGetwayListItems();
            model.Profiles = PaymentOptionViewModelMap.ToListItems(_profileClient.GetProfilesAssociatedWithUsers(HttpContext.Current.User.Identity.Name).Profiles);
            return model;
        }

        public PaymentOptionViewModel GetPaymentOptionViewData()
        {
            return new PaymentOptionViewModel()
            {
                PaymentTypes = GetPaymentTypesListItems(),
                PaymentGateways = GetpaymentGetwayListItems(),
                Profiles = PaymentOptionViewModelMap.ToListItems(_profileClient.GetProfilesAssociatedWithUsers(HttpContext.Current.User.Identity.Name).Profiles)
            };
        }

        public PaymentGatewayListModel GetPaymentGetways()
        {
            return _paymentGatewaysClient.GetPaymentGateways(null, null);
        }

        public PaymentTypeListModel GetPaymentTypes()
        {
            return _paymentTypesClient.GetPaymentTypes(null, null);
        }

        public PaymentOptionModel AddPaymentOption(PaymentOptionViewModel model, out string status)
        {
            try
            {
                PaymentOptionModel addModel = null;
                status = string.Empty;
                addModel = _paymentOptionClient.CreatePaymentOption(PaymentOptionViewModelMap.ToModel(model));

                if (!Equals(addModel, null))
                {
                    HttpPaymentPostRequest(addModel, model);
                    return addModel;
                }
                else
                {
                    return new PaymentOptionModel();
                }
            }
            catch (Exception ex)
            {
                status = ex.Message;
                return new PaymentOptionModel();
            }
        }

        public bool UpdatePaymentOption(PaymentOptionViewModel model, out string status)
        {
            try
            {
                status = string.Empty;
                PaymentOptionModel _model = _paymentOptionClient.UpdatePaymentOption(model.PaymentOptionId, PaymentOptionViewModelMap.ToModel(model));

                if (!Equals(_model, null))
                {
                    HttpPaymentPostRequest(_model, model);
                }

                return true;
            }
            catch (Exception ex)
            {
                status = ex.Message;
                return false;
            }
        }

        public bool DeletePaymentOption(int paymentOptionId, out string status)
        {
            try
            {
                status = string.Empty;
                string paymentAppDeleteUrl = string.Concat(paymentAppUrl, string.Format(deletePaymentUrl, paymentOptionId));
                using (WebClient client = new WebClient())
                {
                    status = client.DownloadString(paymentAppDeleteUrl);

                    if (status.ToLower().Contains(paymentAppSuccessMessage))
                    {
                        return _paymentOptionClient.DeletePaymentOption(paymentOptionId);
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                status = ex.Message;
                return false;
            }
        }

        public List<SelectListItem> GetActivePaymentOptionListItems(int? portalId = null)
        {
            int? profileId = null;
            FilterCollection filters = new FilterCollection();
            if (!Equals(portalId, null))
            {
                ProfileListModel profiles = _profileClient.GetProfileListByPortalId(portalId.Value);
                profileId = profiles.Profiles.FirstOrDefault().ProfileId;
                filters.Add(new FilterTuple(FilterKeys.ProfileId, FilterOperators.Equals, profileId.ToString()));
                filters.Add(new FilterTuple(FilterKeys.IsActive, FilterOperators.Equals, FilterKeys.ActiveIndTrue));
            }
            else
            {
                filters.Add(new FilterTuple(FilterKeys.ProfileId, FilterOperators.Equals, "null"));
                filters.Add(new FilterTuple(FilterKeys.IsActive, FilterOperators.Equals, FilterKeys.ActiveIndTrue));
            }

            _paymentOptionClient.RefreshCache = true;
            PaymentOptionListModel allPaymentOptionList = _paymentOptionClient.GetPaymentOptions(new ExpandCollection() { ExpandKeys.PaymentType, ExpandKeys.PaymentGateway }, filters, new SortCollection());
            AccountViewModel accountModel = _accountAgent.GetAccountViewModel();

            profileId = accountModel != null ? accountModel.ProfileId : PortalAgent.CurrentPortal.DefaultAnonymousProfileId;

            if (profileId.HasValue && !Equals(profileId, 0))
            {
                filters.Add(new FilterTuple(FilterKeys.ProfileId, FilterOperators.Equals, profileId.ToString()));
                filters.Add(new FilterTuple(FilterKeys.IsActive, FilterOperators.Equals, FilterKeys.ActiveIndTrue));

                SortCollection sort = new SortCollection();
                sort.Add(SortKeys.DisplayOrder, SortDirections.Ascending);

                _paymentOptionClient.RefreshCache = true;
                PaymentOptionListModel paymentOptions = _paymentOptionClient.GetPaymentOptions(new ExpandCollection { ExpandKeys.PaymentGateway, ExpandKeys.PaymentType }, filters, sort);
                if (paymentOptions.PaymentOptions != null && paymentOptions.PaymentOptions.Any())
                {
                    allPaymentOptionList.PaymentOptions =
                        new Collection<PaymentOptionModel>(
                            allPaymentOptionList.PaymentOptions.Union(paymentOptions.PaymentOptions).OrderBy(x => x.DisplayOrder).ToList());
                }
            }

            if (!Equals(allPaymentOptionList.PaymentOptions, null))
            {
                PaymentOptionListViewModel paymentTypes = PaymentOptionViewModelMap.ToListViewModel(allPaymentOptionList);
                List<SelectListItem> paymentTypeItems = new List<SelectListItem>();
                paymentTypes.PaymentOptionList.ToList().ForEach(x =>
                {
                    paymentTypeItems.Add(new SelectListItem()
                    {
                        Text = x.PaymentTypeName,
                        Value = x.PaymentOptionId.ToString()
                    });
                });
                return paymentTypeItems.GroupBy(x => x.Text).Select(x => x.First()).ToList();
            }
            else
            {
                return new List<SelectListItem>();
            }
        }

        public string GetPaymentGatwayNameByPaymetOptionId(int? paymetOptionId, out int? paymentProfileId, int? portalId = null)
        {
            paymentProfileId = null;
            string gatwayname = string.Empty;
            FilterCollection filters = new FilterCollection();
            if (!Equals(portalId, null))
            {
                ProfileListModel profiles = _profileClient.GetProfileListByPortalId(portalId.Value);
                int profileId = profiles.Profiles.FirstOrDefault().ProfileId;
                filters.Add(new FilterTuple(FilterKeys.ProfileId, FilterOperators.Equals, profileId.ToString()));
            }
            filters.Add(new FilterTuple(FilterKeys.IsActive, FilterOperators.Equals, FilterKeys.ActiveIndTrue));
            filters.Add(new FilterTuple(FilterKeys.PaymentSettingID, FilterOperators.Equals, paymetOptionId.Value.ToString()));

            var sort = new SortCollection();
            sort.Add(SortKeys.DisplayOrder, SortDirections.Ascending);

            _paymentOptionClient.RefreshCache = true;
            PaymentOptionListModel paymentOptions = _paymentOptionClient.GetPaymentOptions(new ExpandCollection { ExpandKeys.PaymentGateway, ExpandKeys.PaymentType }, filters, sort);

            PaymentOptionModel payModel = paymentOptions.PaymentOptions.FirstOrDefault(x => x.PaymentOptionId.Equals(paymetOptionId.Value));

            if (!Equals(payModel, null) && !Equals(payModel.PaymentGateway, null))
            {
                gatwayname = payModel.PaymentGateway.Name;
                paymentProfileId = payModel.ProfileId;
            }
            return gatwayname;
        }

        public string ProcessPayNow(PayPalPaymentModel model)
        {
            if (!Equals(model, null))
            {
                var data = JsonConvert.SerializeObject(model);

                using (WebClient client = new WebClient())
                {
                    client.Encoding = System.Text.Encoding.UTF8;
                    client.Headers[HttpRequestHeader.ContentType] = "application/json";
                    client.Headers[HttpRequestHeader.Accept] = "text/xml";

                    string jsonModel = client.UploadString(string.Format("{0}{1}", ConfigurationManager.AppSettings["PaymentApplicationUrl"], payNowUrl), data);

                    if (!string.IsNullOrEmpty(jsonModel))
                    {
                        try
                        {
                            JavaScriptSerializer serializer = new JavaScriptSerializer();
                            dynamic item = serializer.Deserialize<object>(jsonModel);
                            return item["Data"];
                        }
                        catch
                        {

                        }
                    }
                }
            }
            return string.Empty;
        }       

        #region Franchise Admin

        public PaymentOptionListViewModel GetFranchisePaymentOptions(FilterCollectionDataModel filterModel)
        {
            int portalId = Convert.ToInt32(filterModel.Filters.FirstOrDefault().FilterValue);
            filterModel.Filters.Remove(filterModel.Filters.FirstOrDefault());

            ProfileListModel profileList = _profileClient.GetProfileListByPortalId(portalId);

            int profileId = (!Equals(profileList, null) && !Equals(profileList.Profiles, null)) ? profileList.Profiles.FirstOrDefault().ProfileId : 0;

            filterModel.Filters.Add(new FilterTuple(FilterKeys.ProfileId, FilterOperators.Equals, profileId.ToString()));
            _paymentOptionClient.RefreshCache = true;
            var paymentOptions = _paymentOptionClient.GetPaymentOptions(new ExpandCollection { ExpandKeys.PaymentGateway, ExpandKeys.PaymentType }, filterModel.Filters, filterModel.SortCollection);
            PaymentOptionListViewModel model = (!Equals(paymentOptions, null) && !Equals(paymentOptions.PaymentOptions, null)) ? PaymentOptionViewModelMap.ToListViewModel(paymentOptions) : new PaymentOptionListViewModel();

            return model;
        }

        public PaymentOptionViewModel SetProfileIdByPortalId(int portalId, PaymentOptionViewModel model)
        {
            ProfileListModel profileList = _profileClient.GetProfileListByPortalId(portalId);
            int profileId = (!Equals(profileList, null) && !Equals(profileList.Profiles, null)) ? profileList.Profiles.FirstOrDefault().ProfileId : 0;
            model.ProfileId = profileId;
            return model;
        }
        #endregion

        #endregion

        #region Private Methods

        public List<SelectListItem> GetPaymentTypesListItems()
        {
            var paymentTypes = GetPaymentTypes();

            List<SelectListItem> items = new List<SelectListItem>();
            paymentTypes.PaymentTypes.ToList().ForEach(x =>
            {
                items.Add(new SelectListItem()
                {
                    Text = x.Name,
                    Value = x.PaymentTypeId.ToString()
                });
            });
            return items;
        }

        public List<SelectListItem> GetPaymentOptionListItems()
        {
            var paymentTypes = GetPaymentOptions(new FilterCollectionDataModel());

            List<SelectListItem> items = new List<SelectListItem>();
            paymentTypes.PaymentOptionList.ToList().ForEach(x =>
            {
                items.Add(new SelectListItem()
                {
                    Text = x.PaymentTypeName,
                    Value = x.PaymentOptionId.ToString()
                });
            });
            return items;
        }

        /// <summary>
        /// This method will call the payment api and to process PayPal payment option
        /// </summary>
        /// <param name="model">PayPalPaymentModel model</param>
        /// <returns>returns PayPal Response</returns>
        public string HttpPayPalPostRequest(PayPalPaymentModel model)
        {
            if (!Equals(model, null))
            {
                var data = JsonConvert.SerializeObject(model);
                string paymentAppDeleteUrl = string.Concat(paymentAppUrl, string.Format(payPalUrl));
                using (WebClient client = new WebClient())
                {
                    client.Encoding = System.Text.Encoding.UTF8;
                    client.Headers[HttpRequestHeader.ContentType] = jsonContentType;
                    client.Headers[HttpRequestHeader.Accept] = acceptType;
                    string jsonModel = client.UploadString(paymentAppDeleteUrl, data);
                    if (!string.IsNullOrEmpty(jsonModel))
                    {
                        try
                        {
                            JavaScriptSerializer serializer = new JavaScriptSerializer();
                            dynamic item = serializer.Deserialize<object>(jsonModel);
                            string payPalResponse = item["Data"];
                            return payPalResponse;
                        }
                        catch
                        {

                        }
                    }
                }
            }
            return string.Empty;
        }

        private List<SelectListItem> GetpaymentGetwayListItems()
        {
            var paymentGeways = GetPaymentGetways();

            List<SelectListItem> items = new List<SelectListItem>();
            paymentGeways.PaymentGateways.ToList().ForEach(x =>
            {
                items.Add(new SelectListItem()
                {
                    Text = x.Name,
                    Value = x.PaymentGatewayId.ToString()
                });
            });
            return items;
        }

        /// <summary>
        /// To Get PaymentSetting Details from Payment API to admin PaymentSetting model
        /// </summary>
        /// <param name="model">PaymentOptionViewModel admin PaymentSetting model</param>
        /// <returns>returns admin PaymentSetting model with the values from Payment API keys</returns>
        private PaymentOptionViewModel GetPaymentSettingDetails(PaymentOptionViewModel model)
        {
            if (!Equals(model, null))
            {
                string paymentSettingUrl = string.Concat(paymentAppUrl, getPaymentSettingDetails);
                paymentSettingUrl = string.Format(paymentSettingUrl, model.PaymentOptionId);
                using (WebClient client = new WebClient())
                {
                    string jsonModel = client.DownloadString(paymentSettingUrl);
                    if (!string.IsNullOrEmpty(jsonModel))
                    {
                        try
                        {
                            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
                            var paymentSettingObject = javaScriptSerializer.Deserialize(jsonModel, typeof(object));

                            var paymentObject = JObject.Parse(paymentSettingObject.ToString());
                            foreach (var item in paymentObject)
                            {
                                SetPaymentSettingKeyValue(model, item.Key, Convert.ToString(item.Value));
                            }
                        }
                        catch
                        {

                        }
                    }
                }
            }
            return model;
        }

        /// <summary>
        /// To Set values of PaymentSettingModel from API result by Key Value
        /// </summary>
        /// <param name="model">PaymentOptionViewModel model from admin site</param>
        /// <param name="keyname">payment api paymentsetting key</param>
        /// <param name="values">payment api paymentsetting value</param>
        private void SetPaymentSettingKeyValue(PaymentOptionViewModel model, string keyname, string values)
        {
            bool? nullvalue = null;
            switch (keyname.ToLower())
            {
                case "enablediscover":
                    model.EnableDiscover = string.IsNullOrEmpty(values) ? nullvalue : Convert.ToBoolean(values);
                    break;
                case "enablemastercard":
                    model.EnableMasterCard = string.IsNullOrEmpty(values) ? nullvalue : Convert.ToBoolean(values);
                    break;
                case "enablerecurringpayments":
                    model.EnableRecurringPayments = string.IsNullOrEmpty(values) ? nullvalue : Convert.ToBoolean(values);
                    break;
                case "enablevault":
                    model.EnableVault = string.IsNullOrEmpty(values) ? nullvalue : Convert.ToBoolean(values);
                    break;
                case "enablevisa":
                    model.EnableVisa = string.IsNullOrEmpty(values) ? nullvalue : Convert.ToBoolean(values);
                    break;
                case "enableamex":
                    model.EnableAmericanExpress = string.IsNullOrEmpty(values) ? nullvalue : Convert.ToBoolean(values);
                    break;
                case "preauthorize":
                    model.PreAuthorize = string.IsNullOrEmpty(values) ? false : Convert.ToBoolean(values);
                    break;
                case "testmode":
                    model.TestMode = string.IsNullOrEmpty(values) ? false : Convert.ToBoolean(values);
                    break;
                case "partner":
                    model.Partner = values;
                    break;
                case "gatewaypassword":
                    model.PaymentGatewayPassword = values;
                    break;
                case "gatewayusername":
                    model.PaymentGatewayUsername = values;
                    break;
                case "transactionkey":
                    model.TransactionKey = values;
                    break;
                case "vendor":
                    model.Vendor = values;
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// This method will call the payment api and will Add/Update the entries in payment DB.
        /// </summary>
        /// <param name="model">PaymentOptionModel model</param>
        private void HttpPaymentPostRequest(PaymentOptionModel model, PaymentOptionViewModel paymentModel)
        {
            paymentAppUrl = string.Concat(paymentAppUrl, configurePaymentUrl);
            string postData = GetPostedData(model, paymentModel);

            using (WebClient client = new WebClient())
            {
                client.Headers.Add(HttpRequestHeader.ContentType, jsonContentType);
                string result = client.UploadString(paymentAppUrl, postData);
            }
        }

        /// <summary>
        /// This method will convert the PaymentOptionModel to ZNodePaymentSettingModel which will be in used
        /// to add/modify the data in Payment API
        /// </summary>
        /// <param name="model">PaymentOptionModel model</param>
        /// <returns>Json converrted data of ZNodePaymentSettingModel</returns>
        private string GetPostedData(PaymentOptionModel model, PaymentOptionViewModel paymentModel)
        {
            ZNodePaymentSettingModel objEntity = new ZNodePaymentSettingModel()
            {
                ActiveInd = model.IsActive,
                DisplayOrder = model.DisplayOrder,
                EnableAmex = paymentModel.EnableAmericanExpress,
                EnableDiscover = paymentModel.EnableDiscover,
                EnableMasterCard = paymentModel.EnableMasterCard,
                EnableRecurringPayments = paymentModel.EnableRecurringPayments,
                EnableVault = paymentModel.EnableVault,
                EnableVisa = paymentModel.EnableVisa,
                GatewayPassword = paymentModel.PaymentGatewayPassword,
                GatewayTypeID = paymentModel.PaymentGatewayId,
                GatewayUsername = paymentModel.PaymentGatewayUsername,
                IsRMACompatible = model.IsRmaCompatible,
                Partner = paymentModel.Partner,
                PaymentSettingID = model.PaymentOptionId,
                PaymentTypeID = model.PaymentTypeId,
                PreAuthorize = paymentModel.PreAuthorize,
                ProfileID = model.ProfileId,
                TestMode = paymentModel.TestMode,
                TransactionKey = paymentModel.TransactionKey,
                Vendor = paymentModel.Vendor
            };

            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            return JsonConvert.SerializeObject(objEntity, settings);
        }

        #endregion

    }
}