﻿using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Web;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.MvcAdmin.Agents.IAgents;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.MvcAdmin.Agents.Agents
{
    public class DiagnosticsAgent : BaseAgent, IDiagnosticsAgent
    {
        #region Variables

        private IDiagnosticsClient _client;

        #endregion

        #region Constructor

        public DiagnosticsAgent()
        {
            _client = new DiagnosticsClient();
        }

        #endregion

        /// <summary>
        /// This method calls diagnostics client to check SMTP Account
        /// </summary>
        /// <returns>Returns the DiagnosticsResponse which cntains the status of smtp server</returns>
        public DiagnosticsResponse CheckEmailAccount()
        {
            return _client.CheckEmailAccount();
        }

        /// <summary>
        /// This method calls diagnostics client to get Version details of product from database
        /// </summary>
        /// <returns>Returns the DiagnosticsResponse which contains the version details</returns>
        public DiagnosticsResponse GetProductVersionDetails()
        {
            return _client.GetProductVersionDetails();
        }

        /// <summary>
        /// This method calls the diagnostics client to sends the diagnostics email
        /// </summary>
        /// <param name="caseNumber">Case number for diagnostics</param>
        /// <returns>Returns the DiagnosticsResponse which contains the email sent status</returns>
        public DiagnosticsResponse EmailDiagnostics(DiagnosticsEmailModel model)
        {
            return _client.EmailDiagnostics(model);
        }

        /// <summary>
        /// This method validates the license (whether the license is installed on the calling server or not)
        /// This method calls the ZNode.Libraries.Framework.Business.ZNodeLicenseManager class to validate the license
        /// </summary>
        /// <returns>Returns ZNodeLicenseType which will contain the status true if the license is valid otherwise false</returns>
        public ZNodeLicenseType ValidateLicense()
        {
            ActivateLicenseResponse response = new ActivateLicenseResponse();
            ZNodeLicenseManager licenseMgr = new ZNodeLicenseManager();
            return licenseMgr.Validate();
        }

        /// <summary>
        /// This method validates the license (whether the license is installed on the calling server or not)
        /// This method calls the ZNode.Libraries.Framework.Business.ZNodeLicenseManager class to get the license status description
        /// </summary>
        /// <returns>Returns License Status Description</returns>
        public string GetLicenseStatusDescription()
        {
            ActivateLicenseResponse response = new ActivateLicenseResponse();
            ZNodeLicenseManager licenseMgr = new ZNodeLicenseManager();
            licenseMgr.Validate();
            return licenseMgr.GetStatusDescription();
        }

        /// <summary>
        /// Check Public Folder for Read + Write Permissions
        /// </summary>
        /// <param name="path">Represents the path </param>
        /// <returns>true if the folder have read+write permission</returns>
        public bool CheckFolderPermissions(string path)
        {
            try
            {
                FileInfo file = new FileInfo(HttpContext.Current.Server.MapPath(path) + "test.txt");
                FileStream filestream = file.Create();
                filestream.Close();
                file.Delete();
                return true;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Check for Database connection String
        /// </summary>
        /// <returns>Returns the status of database connection</returns>
        public bool CheckDataBaseConnection()
        {
            try
            {
                SqlConnection myConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString);
                myConnection.Open();
                myConnection.Close();
                myConnection.Dispose();
                return true;
            }
            catch
            {
                throw;
            }
        }
    }
}