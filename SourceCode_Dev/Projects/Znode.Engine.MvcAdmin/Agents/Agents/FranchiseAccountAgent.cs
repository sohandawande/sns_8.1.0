﻿using Resources;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;
using Znode.Engine.MvcDemo.Maps;

namespace Znode.Engine.MvcAdmin.Agents
{
    public class FranchiseAccountAgent : BaseAgent, IFranchiseAccountAgent
    {
        #region Private Variables

        private readonly ISuppliersClient _supplierClient;
        private readonly IVendorAccountClient _vendorAccountClient;
        private readonly IPortalsClient _portalsClient;
        private readonly IProfilesClient _profileClient;
        private readonly ICatalogsClient _catalogClient;
        private readonly IAddressesClient _addressClient;
        private readonly IAccountsClient _accountClient;
        private readonly IThemeClient _themeClient;
        private readonly IFranchiseAccountClient _franchiseAccountClient;
        private readonly IPortalCatalogsClient _portalCatalogClient;
        private readonly ICSSClient _cssClient;
        private readonly IAccountsProfilesClient _accountProfileclient;

        private readonly IPortalAgent _portalAgent;
        private readonly ICatalogAgent _catalogAgent;
        private readonly IDomainAgent _domainAgent;
        private readonly ITaxClassAgent _taxClassAgent;
        private readonly IProfilesAgent _profileAgent;
        private readonly IPortalProfileAgent _portalProfileAgent;
        private readonly IPermissionAgent _PermissionAgent;


        private const string PhoneNo = "vendorcontact.phonenumber";
        private const string Email = "vendorcontact.email";
        private const string Default = "_Default";
        private const string TaxClassName = "Default";

        #endregion

        #region Constructor

        public FranchiseAccountAgent()
        {
            _supplierClient = GetClient<SuppliersClient>();
            _catalogClient = GetClient<CatalogsClient>();
            _vendorAccountClient = GetClient<VendorAccountClient>();
            _themeClient = GetClient<ThemeClient>();
            _portalsClient = GetClient<PortalsClient>();
            _profileClient = GetClient<ProfilesClient>();
            _franchiseAccountClient = GetClient<FranchiseAccountClient>();
            _addressClient = GetClient<AddressesClient>();
            _accountClient = GetClient<AccountsClient>();
            _portalCatalogClient = GetClient<PortalCatalogsClient>();
            _cssClient = GetClient<CSSClient>();
            _accountProfileclient = GetClient<AccountsProfilesClient>();

            _portalAgent = new PortalAgent();
            _PermissionAgent = new PermissionAgent();
            _catalogAgent = new CatalogAgent();
            _domainAgent = new DomainAgent();
            _taxClassAgent = new TaxClassAgent();
            _profileAgent = new ProfilesAgent();
            _portalProfileAgent = new PortalProfileAgent();
        }

        #endregion

        #region Public Methods

        public VendorAccountListViewModel GetFranchiseAccountList(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            VendorAccountListViewModel model = new VendorAccountListViewModel();
            filters.Add(new FilterTuple(FilterKeys.RoleName, FilterOperators.Like, FilterKeys.Franchise));
            filters.Add(new FilterTuple(FilterKeys.Username, FilterOperators.Equals, string.Format("'{0}'", HttpContext.Current.User.Identity.Name)));

            ReplaceFilterKeyName(ref filters, PhoneNo, MvcAdminConstants.PhoneNumber);
            ReplaceFilterKeyName(ref filters, Email, MvcAdminConstants.Email);
            ReplaceFilterKeyName(ref filters, MvcAdminConstants.StartDate, FilterKeys.CreateDate);
            ReplaceFilterKeyName(ref filters, MvcAdminConstants.EndDate, FilterKeys.CreateDate);

            ReplaceSortKeyName(sortCollection, PhoneNo, MvcAdminConstants.PhoneNumber);
            ReplaceSortKeyName(sortCollection, Email, MvcAdminConstants.Email);         
            _vendorAccountClient.RefreshCache = true;
            var accountList = _vendorAccountClient.GetVendorAccountList(null, filters, sortCollection, pageIndex, recordPerPage);
            return (Equals(accountList, null) && (Equals(accountList.VendorAccount, null))) ? new VendorAccountListViewModel() : VendorAccountViewModelMap.ToListViewModel(accountList);
        }

        public VendorAccountDownloadListViewModel GetFranchiseAccountToDownload(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            VendorAccountDownloadListViewModel model = new VendorAccountDownloadListViewModel();
            ReplaceFilterKeyName(ref filters, PhoneNo, MvcAdminConstants.PhoneNumber);
            ReplaceFilterKeyName(ref filters, Email, MvcAdminConstants.Email);
            ReplaceFilterKeyName(ref filters, MvcAdminConstants.StartDate, FilterKeys.CreateDate);
            ReplaceFilterKeyName(ref filters, MvcAdminConstants.EndDate, FilterKeys.CreateDate);

            ReplaceSortKeyName(sortCollection, PhoneNo, MvcAdminConstants.PhoneNumber);
            ReplaceSortKeyName(sortCollection, Email, MvcAdminConstants.Email);
            var accountList = _vendorAccountClient.GetVendorAccountList(null, filters, sortCollection, pageIndex, recordPerPage);
            return (Equals(accountList, null) && (Equals(accountList.VendorAccount, null))) ? new VendorAccountDownloadListViewModel() : VendorAccountViewModelMap.ToDownload(accountList);
        }

        public FranchiseAccountViewModel BindPortalInformation(FranchiseAccountViewModel model)
        {
            if (!Equals(model, null))
            {
                model.Themes = PortalViewModelMap.ToSelectListItems(_themeClient.GetThemes(Filters, new SortCollection()).Themes);
            }
            return model;
        }

        public bool CreateFranchiseAccount(FranchiseAccountViewModel model, out int accountId, out string errorMessage)
        {
            accountId = 0;
            int profileId = 0;
            int portalId = 0;
            int catalogId = 0;
            int portalCatalogId = 0;
            try
            {
                errorMessage = string.Empty;
                string notificationMessage = string.Empty;
                PortalViewModel portalViewModel = SetStoreParameters(model);
                bool isPoratalCreated = _portalAgent.CreatePortal(portalViewModel, out notificationMessage);
                portalId = portalViewModel.PortalId;
                if (isPoratalCreated)
                {
                    bool SiteUrl = !string.IsNullOrEmpty(model.SiteURL) ? this.SetSiteUrl(model, portalId) : false;
                    CatalogViewModel catalog = this.CreateCatalog(model, portalId);
                    catalogId = catalog.CatalogId;
                    PortalCatalogModel portalCatalog = CreatePortalCatalog(model, portalId, catalogId);
                    portalCatalogId = portalCatalog.PortalCatalogId;
                    this.CreateTaxClass(portalId);
                    this.CreateProfileAndPortalProfile(model, portalId, out profileId);
                    this.updateStore(portalId, profileId);
                    model.ProfileId = profileId;
                    model.PortalId = portalId;
                    AccountModel franchiseAccount = _franchiseAccountClient.CreateFranchiseAccount(FranchiseAdministratorsViewModelMap.ToAccountModel(model));
                    accountId = franchiseAccount.AccountId;
                    if (accountId > 0)
                    {
                        if (franchiseAccount.IsEmailSentFailed)
                        {
                            errorMessage = string.Format("{0} {1}", ZnodeResources.ErrorSmptSetting, PortalAgent.CurrentPortal.StoreName);
                            return false;
                        }
                        else
                        {
                            return true;
                        }
                    }
                    return false;
                }
                else
                {
                    errorMessage = notificationMessage;
                    return false;
                }
            }
            catch (ZnodeException ex)
            {
                if (portalId > 0)
                {
                    _portalAgent.DeletePortal(portalId);
                }
                if (profileId > 0)
                {
                    _profileAgent.DeleteProfile(profileId);
                }

                errorMessage = ex.ErrorMessage;
                return false;
            }
        }

        public FranchiseAccountViewModel GetFranchiseAccountById(int accountId)
        {
            FilterCollection filters = new FilterCollection();
            filters.Add(new FilterTuple(FilterKeys.AccountId, FilterOperators.Equals, accountId.ToString()));
            _vendorAccountClient.RefreshCache = true;
            VendorAccountModel accountModel = _vendorAccountClient.GetVendorAccountById(accountId);
            return (!Equals(accountModel, null)) ? FranchiseAdministratorsViewModelMap.ToFranchiseViewModel(accountModel) : new FranchiseAccountViewModel();
        }

        public bool UpdateFranchiseAccount(FranchiseAccountViewModel model)
        {
            var account = _vendorAccountClient.UpdateVendorAccount(model.AccountId, FranchiseAdministratorsViewModelMap.ToAccountModel(model));
            return account.AccountId > 0;
        }

        public AccountViewModel ResetPassword(int accountId, string areaName = "")
        {
            AccountViewModel accountViewModel = new AccountViewModel();
            AccountModel accountModel = _accountClient.GetAccount(accountId, new ExpandCollection { ExpandKeys.User });
            try
            {
                if (!Equals(accountModel, null))
                {
                    if (Equals(accountModel.User.Email, accountModel.Email))
                    {
                        accountModel.BaseUrl = GetDomainUrl(areaName);
                        AccountModel accountResetPassword = _accountClient.ResetPassword(accountModel);
                        if (accountResetPassword.IsEmailSentFailed)
                        {
                            accountViewModel.HasError = true;
                            accountViewModel.ErrorMessage = ZnodeResources.ErrorSendResetPasswordLink;
                            return accountViewModel;
                        }
                        else
                        {
                            accountViewModel.SuccessMessage = Resources.ZnodeResources.SuccessResetPassword;
                            return accountViewModel;
                        }


                    }
                    else
                    {
                        accountViewModel.HasError = true;
                        accountViewModel.ErrorMessage = ZnodeResources.ValidEmailAddress;
                        return accountViewModel;
                    }
                }
                else
                {
                    accountViewModel.HasError = true;
                    accountViewModel.ErrorMessage = ZnodeResources.ErrorAccessDenied;
                    return accountViewModel;
                }
            }
            catch (ZnodeException exception)
            {
                if (Equals(exception.ErrorCode, ErrorCodes.UserNameUnavailable))
                {
                    accountViewModel.HasError = true;
                    accountViewModel.ErrorMessage = exception.ErrorMessage;
                }
            }
            return accountViewModel;
        }

        public void DownloadFile(VendorAccountDownloadListViewModel model, HttpResponseBase response, string fileName)
        {
            DownloadHelper downloadHelper = new DownloadHelper();
            downloadHelper.Download(model.VendorAccountDownloadList, Convert.ToString(Convert.ToInt32(FileTypes.CSV)), response, null, fileName);
        }

        public ProfileListViewModel GetProfileList(int storeId)
        {
            ProfileListModel profiles = (!Equals(storeId, 0)) ? _profileClient.GetProfileListByPortalId(storeId) : _profileClient.GetProfilesAssociatedWithUsers(HttpContext.Current.User.Identity.Name);
            return (!Equals(profiles, null) && !Equals(profiles.Profiles, null)) ? ProfileViewModelMap.ToListViewModel(profiles, null, null) : new ProfileListViewModel();
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Set Store store parameters to create store
        /// </summary>
        /// <param name="model">FranchiseAccountViewModel model</param>
        /// <returns>Returns Portal view model</returns>
        private PortalViewModel SetStoreParameters(FranchiseAccountViewModel model)
        {
            PortalViewModel existingPortal = PortalAgent.CurrentPortal;
            PortalViewModel portalViewModel = new PortalViewModel();
            portalViewModel.AdminEmail = model.ContactInfo.Email;
            portalViewModel.CompanyName = model.StoreName;
            portalViewModel.CustomerServiceEmail = model.ContactInfo.Email;
            portalViewModel.CustomerServicePhoneNumber = model.ContactInfo.PhoneNumber;
            portalViewModel.LocaleId = 43;
            portalViewModel.SalesEmail = model.ContactInfo.Email;
            portalViewModel.SalesPhoneNumber = model.ContactInfo.PhoneNumber;
            portalViewModel.IsActive = true;
            portalViewModel.StoreName = model.StoreName;
            portalViewModel.LogoPath = Equals(model.Logo, null) ? existingPortal.ImageNotAvailablePath : model.Logo;
            portalViewModel.LogoPathImageName = Equals(model.Logo, null) ? existingPortal.ImageNotAvailablePathUrl : model.Logo.FileName;
            portalViewModel.DefaultReviewStatus = "N";
            portalViewModel.DefaultOrderStateId = 50;
            portalViewModel.DefaultProductReviewStateId = 10;
            portalViewModel.CSSId = 1;
            portalViewModel.ThemeId = model.ThemeId;
            portalViewModel.UserType = model.UserType;
            return portalViewModel;
        }

        /// <summary>
        /// Create catalog
        /// </summary>
        /// <param name="model">FranchiseAccountViewModel model</param>
        /// <returns>Returns catalog view model</returns>
        private CatalogViewModel CreateCatalog(FranchiseAccountViewModel model, int portalId)
        {
            CatalogViewModel catalogViewModel = new CatalogViewModel();
            catalogViewModel.Name = model.StoreName + Default;
            catalogViewModel.PortalId = portalId;
            catalogViewModel.IsActive = true;
            return _catalogAgent.CreateCatalog(catalogViewModel);
        }

        /// <summary>
        /// Update site url
        /// </summary>
        /// <param name="model">FranchiseAccountViewModel model</param>
        /// <param name="portalId">int portalId</param>
        /// <returns>Returns true or false</returns>
        private bool SetSiteUrl(FranchiseAccountViewModel model, int portalId)
        {
            DomainViewModel domainViewModel = new DomainViewModel();
            domainViewModel.PortalId = portalId;
            domainViewModel.DomainName = model.SiteURL;
            domainViewModel.IsActive = true;
            return _domainAgent.createDomainUrl(domainViewModel);
        }

        /// <summary>
        ///Create Tax Class 
        /// </summary>
        /// <param name="portalId">int portalId</param>
        /// <param name="taxId">out int taxId</param>
        /// <returns>Returns true or false</returns>
        private bool CreateTaxClass(int portalId)
        {
            string message = string.Empty;
            TaxClassViewModel taxClassViewModel = new TaxClassViewModel();
            int taxId = 0;
            taxClassViewModel.PortalId = portalId;
            taxClassViewModel.Name = TaxClassName;
            taxClassViewModel.DisplayOrder = 1;
            taxClassViewModel.IsActive = true;
            return _taxClassAgent.SaveTaxClass(taxClassViewModel, out taxId, out message);
        }

        /// <summary>
        /// Create profile and set portal profile
        /// </summary>
        /// <param name="model">FranchiseAccountViewModel model</param>
        /// <param name="portalId">int portalId</param>
        /// <param name="profileId">out int profileId</param>
        /// <returns>Returns true or false</returns>
        private bool CreateProfileAndPortalProfile(FranchiseAccountViewModel model, int portalId, out int profileId)
        {
            ProfileViewModel profileViewModel = new ProfileViewModel();
            PortalProfileViewModel portalProfileViewModel = new PortalProfileViewModel();
            profileViewModel.Name = string.Format("{0}_{1}", model.StoreName, portalId);
            this.SetProfileDefaultValues(profileViewModel);
            _profileAgent.CreateProfile(profileViewModel, out profileId);
            portalProfileViewModel.ProfileID = profileId;
            portalProfileViewModel.PortalID = portalId;
            portalProfileViewModel.IsCurrentAnonymousProfile = true;
            portalProfileViewModel.IsCurrentRegisteredProfile = true;
            _portalProfileAgent.CreatePortalProfile(portalProfileViewModel);
            return true;
        }

        /// <summary>
        /// Update Store
        /// </summary>
        /// <param name="portalId">int portalId</param>
        /// <param name="profileId">int profileId</param>
        /// <returns>Returns true or false</returns>
        private bool updateStore(int portalId, int profileId)
        {
            PortalModel portalViewModel = _portalsClient.GetPortal(portalId);
            portalViewModel.DefaultAnonymousProfileId = profileId;
            portalViewModel.DefaultRegisteredProfileId = profileId;
            PortalModel portalModel = _portalsClient.UpdatePortal(portalViewModel.PortalId, portalViewModel);
            return (!Equals(portalModel, null) && (portalModel.PortalId > 0)) ? true : false;
        }

        /// <summary>
        /// Create portal Catalog
        /// </summary>
        /// <param name="model">FranchiseAccountViewModel model</param>
        /// <param name="portalId">int portalId</param>
        /// <param name="catalogId">int catalogId</param>
        /// <returns>Retuen Portal Catalog Model</returns>
        private PortalCatalogModel CreatePortalCatalog(FranchiseAccountViewModel model, int portalId, int catalogId)
        {
            PortalCatalogModel portalCatalog = new PortalCatalogModel();
            portalCatalog.PortalId = portalId;
            portalCatalog.CatalogId = catalogId;
            portalCatalog.ThemeId = model.ThemeId;
            portalCatalog.LocaleId = 43;
            CSSListModel cssList = _cssClient.GetCSSs(null, null, null);
            if (!Equals(cssList, null) && !Equals(cssList.CSSs, null) && (cssList.CSSs.Count > 0))
            {
                portalCatalog.CssId = cssList.CSSs.FirstOrDefault().CSSID;
            }
            return _portalCatalogClient.CreatePortalCatalog(portalCatalog);
        }

        /// <summary>
        /// Set Profile default values from Znode Profile
        /// </summary>
        /// <param name="model">ProfileViewModel model</param>
        /// <returns>Retuen Profile view model</returns>
        private ProfileViewModel SetProfileDefaultValues(ProfileViewModel model)
        {
            _profileClient.RefreshCache = true;
            ProfileModel profileModel = _profileClient.GetZnodeProfile();
            model.ProductPrice = profileModel.ShowPricing;
            model.WholeSalePrice = profileModel.UseWholesalePricing;
            return model;
        }


        /// <summary>
        /// To Get the Domain Url
        /// </summary>
        /// <returns>Return the Domain Url.</returns>
        private string GetDomainUrl(string areaName)
        {
            string baseDomainUrl = (!string.IsNullOrEmpty(HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority))) ? HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) : string.Empty;
            return (string.IsNullOrEmpty(areaName)) ? baseDomainUrl : (Equals(areaName, MvcAdminConstants.CustomerKey)) ? ConfigurationManager.AppSettings["DemoWebsiteUrl"] : string.Format("{0}/{1}", baseDomainUrl, areaName);
        }
        #endregion

        #region Bind Dropdowns

        public List<PortalViewModel> GetPortalsList()
        {
            PortalListViewModel portalListModel = _portalAgent.GetPortals();
            return !Equals(portalListModel, null) && !Equals(portalListModel.Portals, null) ? (portalListModel.Portals.ToList()) : new List<PortalViewModel>();
        }

        public List<ProfileModel> ProfileList()
        {
            _profileClient.RefreshCache = true;
            ProfileListModel profiles = _profileClient.GetProfilesAssociatedWithUsers(HttpContext.Current.User.Identity.Name);
            return (!Equals(profiles, null) && !Equals(profiles.Profiles, null)) ? profiles.Profiles.ToList() : new List<ProfileModel>();
        }

        #endregion
    }
}