﻿using System.Collections.Generic;
using System.Linq;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public class ManageCSSAgent : BaseAgent, IManageCSSAgent
    {
        #region Private Client Variables
        private readonly ICSSClient _cssClient;
        private readonly IThemeClient _themeClient;
        #endregion

        #region Public contructor of ManageCSSAgent
        public ManageCSSAgent()
        {
            _cssClient = GetClient<CSSClient>();
            _themeClient = GetClient<ThemeClient>();
        } 
        #endregion

        #region Public Methods
        public CSSListViewModel GetCSSList(FilterCollection filters, SortCollection sorts, int? pageIndex = null, int? recordPerPage = null)
        {
            _cssClient.RefreshCache = true;
            ReplaceFilterKeyName(ref filters, FilterKeys.Name, FilterKeys.CSSName);
            ReplaceFilterKeyName(ref filters, FilterKeys.ThemeId, FilterKeys.CSSThemeId);
            CSSListViewModel cssList = CSSViewModelMap.ToListViewModel(_cssClient.GetCSSs(new ExpandCollection { ExpandKeys.Theme }, filters, sorts, pageIndex - 1, recordPerPage));
            return (!Equals(cssList, null) && !Equals(cssList.CSSList, null)) ? cssList : new CSSListViewModel();
        }

        public bool CreateCSS(CSSViewModel model, out string message)
        {
            try
            {
                message = string.Empty;
                CSSModel theme = _cssClient.CreateCSS(CSSViewModelMap.ToModel(model));
                return true;
            }
            catch (ZnodeException ex)
            {
                message = ex.ErrorMessage;
                return false;
            }
        }

        public CSSViewModel GetCSSById(int cssId)
        {
            _cssClient.RefreshCache = true;
            var cssModel = _cssClient.GetCSS(cssId);
            return !Equals(cssModel, null) ? CSSViewModelMap.ToViewModel(cssModel) : new CSSViewModel();
        }

        public bool UpdateCSS(CSSViewModel model, out string message)
        {
            try
            {
                message = string.Empty;
                CSSModel theme = _cssClient.UpdateCSS(model.CSSId, CSSViewModelMap.ToModel(model));
                return true;
            }
            catch (ZnodeException ex)
            {
                message = ex.ErrorMessage;
                return false;
            }
        }

        public bool DeleteCSS(int cssId, out string message)
        {
            try
            {
                message = string.Empty;
                return _cssClient.DeleteCSS(cssId);
            }
            catch (ZnodeException ex)
            {
                message = ex.ErrorMessage;
                return false;
            }
        }

        #endregion

        #region DropDowns For Search
        public List<ThemeModel> GetThemeModelList()
        {
            _themeClient.RefreshCache = true;
            ThemeListModel themes = _themeClient.GetThemes(null, null);
            return (!Equals(themes, null) && !Equals(themes.Themes, null)) ? themes.Themes.ToList() : new List<ThemeModel>();
        } 
        #endregion


    }
}