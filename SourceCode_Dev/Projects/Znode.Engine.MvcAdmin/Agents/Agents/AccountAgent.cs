﻿using Resources;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using System.Web.Security;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;
using Znode.Libraries.Helpers.Constants;

namespace Znode.Engine.MvcAdmin.Agents
{
    public class AccountAgent : BaseAgent, IAccountAgent
    {
        #region Private Variables
        private readonly IAccountsClient _accountClient;
        private readonly ICountriesClient _countriesClient;
        private readonly IEnvironmentConfigClient _environmentConfigClient;
        private readonly IProductsClient _productsClient;
        #endregion

        #region Constructor
        public AccountAgent()
        {
            _accountClient = GetClient<AccountsClient>();
            _countriesClient = GetClient<CountriesClient>();
            _environmentConfigClient = GetClient<EnvironmentConfigClient>();
            _countriesClient = GetClient<CountriesClient>();
            _productsClient = GetClient<ProductsClient>();
        }
        #endregion

        #region Public Methods
        #region Login method

        /// <summary>
        ///  Login based on the username and password
        /// </summary>
        /// <param name="model">Login details as Login View Model</param>
        /// <returns>Success or not</returns>
        public LoginViewModel Login(LoginViewModel model)
        {
            AccountModel accountModel;
            LoginViewModel loginViewModel;
            try
            {
                accountModel = _accountClient.Login(AccountViewModelMap.ToLoginModel(model), GetExpands());

                //In case user requested for Reset password,
                if (!Equals(accountModel, null) && !Equals(accountModel.User, null) && !string.IsNullOrEmpty(accountModel.User.PasswordToken))
                {
                    loginViewModel = AccountViewModelMap.ToLoginViewModel(accountModel);
                    loginViewModel.IsResetPassword = true;
                    loginViewModel.HasError = true;
                    loginViewModel.ErrorMessage = ZnodeResources.InvalidUserNamePassword;
                    return loginViewModel;
                }
                SaveInSession(MvcAdminConstants.UserAccountSessionKey, AccountViewModelMap.ToAccountViewModel(accountModel));

                //Saves the value from ZNode.Libraries.Framework.Business.ZNodeEnvironmentConfig in session.
                EnvironmentConfigModel environmentConfigModel = _environmentConfigClient.GetEnvironmentConfig();
                SaveInSession(MvcAdminConstants.EnvironmentConfigKey, environmentConfigModel);

                return AccountViewModelMap.ToLoginViewModel(accountModel);

            }
            catch (ZnodeException ex)
            {
                switch (ex.ErrorCode)
                {
                    case 1://Error Code For Reset Super Admin Details for the  first time login.
                        return ReturnErrorViewModel(model, ex, true, out accountModel, out loginViewModel);
                    case 2://Error Code to Reset the Password for the first time login.
                        return ReturnErrorViewModel(model, ex, false, out accountModel, out loginViewModel);
                    case ErrorCodes.AccountLocked:
                        return new LoginViewModel() { HasError = true, ErrorMessage = (!string.IsNullOrEmpty(ex.ErrorMessage)) ? ex.ErrorMessage : ZnodeResources.InvalidUserNamePassword };
                    default:
                        break;
                }
            }
            return new LoginViewModel() { HasError = true, ErrorMessage = ZnodeResources.InvalidUserNamePassword };
        }


        #endregion

        #region Logout
        /// <summary>
        /// Logout the user
        /// </summary>
        public void Logout()
        {
            FormsAuthentication.SignOut();
            HttpContext.Current.Session.Abandon();
        }
        #endregion

        public bool IsUserInRole(string userName, string roleName)
        {
            AccountModel accountModel;
            try
            {
                accountModel = _accountClient.CheckUserRole(new AccountModel { UserName = userName, RoleName = roleName });
                return accountModel.IsUserInRole;
            }
            catch (ZnodeException)
            {
                return false;
            }
        }

        public bool IsAddressValid(AddressViewModel shippingAddress)
        {
            try
            {
                var address = _accountClient.IsAddressValid(AddressViewModelMap.ToModel(shippingAddress));
                if (!Equals(address, null) && address.ValidAddress)
                {
                    return address.ValidAddress;
                }
                return false;
            }
            catch (ZnodeException)
            {
                return false;
            }
        }

        #endregion

        /// <summary>
        /// To Check whether the Account details are present in Session.
        /// </summary>
        /// <returns>Return true or false.</returns>
        public bool CheckAccountKey()
        {
            var accountViewModel = GetFromSession<AccountModel>(MvcAdminConstants.UserAccountSessionKey);
            return (!Equals(accountViewModel, null));
        }

        /// <summary>
        /// To Reset the Admin Details for the default admin user in case of first time login.
        /// </summary>
        /// <param name="model">Model of the type ResetPassordModel</param>
        /// <returns>Return the response in ResetPasswordModel type.</returns>
        public ResetPasswordModel ResetPassword(ResetPasswordModel model)
        {
            try
            {
                if (!Equals(model, null))
                {
                    var accountViewModel = GetFromSession<AccountModel>(MvcAdminConstants.UserAccountSessionKey);
                    if (Equals(accountViewModel, null))
                    {
                        return new ResetPasswordModel() { HasError = true, ErrorMessage = ZnodeResources.ErrorResetAdminDetails };
                    }
                    var errorCode = GetFromSession<int?>(MvcAdminConstants.ErrorCodeSessionKey);

                    var accountModel = AccountViewModelMap.ToAccountModel(model);
                    accountModel.AccountId = accountViewModel.AccountId;
                    accountModel.UserId = accountViewModel.UserId;
                    accountModel.ErrorCode = errorCode.ToString();
                    var registerModel = _accountClient.ResetAdminDetails(accountModel);
                    return new ResetPasswordModel { SuccessMessage = ZnodeResources.SuccessResetAdminDetails };
                }
            }
            catch (ZnodeException exception)
            {
                if (Equals(exception.ErrorCode, ErrorCodes.UserNameUnavailable))
                {
                    return new ResetPasswordModel() { HasError = true, ErrorMessage = ZnodeResources.ErrorDoNotUseDefaultUserNamePassword };
                }
                else if (Equals(exception.ErrorCode, ErrorCodes.ZnodeEncryptionError))
                {
                    return new ResetPasswordModel() { HasError = true, ErrorMessage = ZnodeResources.ErrorReadingZnodeCertificate };
                }
            }
            return new ResetPasswordModel() { HasError = true, ErrorMessage = ZnodeResources.ErrorResetAdminDetails };

        }

        /// <summary>
        /// Forgot Password method.
        /// </summary>
        /// <param name="model">AccountViewModel </param>
        /// <returns>Returns fogot password details.</returns>
        public AccountViewModel ForgotPassword(AccountViewModel model)
        {
            if (!Equals(model, null))
            {
                try
                {
                    var accountModel = _accountClient.GetAccountByUser(model.UserName.Trim(), new ExpandCollection { ExpandKeys.User });
                    if (!Equals(accountModel, null))
                    {
                        if (!model.HasError && !Equals(accountModel, null))
                        {
                            //Used to skip role check in case the use having the roles other than Vendor & franchise and using the Admin Site Url.
                            bool skipRoleCheck = false;
                            string roleName = GetUserRoleName(model.UserName.Trim(), out skipRoleCheck);
                            if ((IsUserInRole(model.UserName.Trim(), roleName)) || skipRoleCheck)
                            {
                                if (Equals(model.EmailAddress, accountModel.Email))
                                {
                                    model.BaseUrl = GetDomainUrl();
                                    model = ResetPassword(model);
                                    model.UserId = accountModel.User.UserId;
                                    model.AccountId = accountModel.AccountId;
                                    return model;
                                }
                                else
                                {
                                    return ErrorModel(model, ZnodeResources.ValidEmailAddress);
                                }
                            }
                            else
                            {
                                return ErrorModel(model, ZnodeResources.ErrorAccessDenied);
                            }
                        }
                    }
                    else
                    {
                        return ErrorModel(model, ZnodeResources.InvalidAccountInformation);
                    }
                }
                catch (ZnodeException)
                {
                    return ErrorModel(model, ZnodeResources.InvalidAccountInformation);
                }
            }

            return model;
        }

        /// <summary>
        /// This function will check the Reset Password Link current status.
        /// </summary>
        /// <param name="model">ChangePasswordViewModel</param>
        /// <returns>Returns Status of Reset Password Link.</returns>
        public ResetPasswordStatusTypes CheckResetPasswordLinkStatus(ChangePasswordViewModel model)
        {
            try
            {
                string errorCode = string.Empty;
                var data = _accountClient.CheckResetPasswordLinkStatus(AccountViewModelMap.ToChangePasswordModel(model));
                return ResetPasswordStatusTypes.NoRecord;
            }
            catch (ZnodeException ex)
            {
                switch (ex.ErrorCode)
                {
                    case ErrorCodes.ResetPasswordContinue:
                        return ResetPasswordStatusTypes.Continue;
                    case ErrorCodes.ResetPasswordLinkExpired:
                        return ResetPasswordStatusTypes.LinkExpired;
                    case ErrorCodes.ResetPasswordNoRecord:
                        return ResetPasswordStatusTypes.NoRecord;
                    case ErrorCodes.ResetPasswordTokenMismatch:
                        return ResetPasswordStatusTypes.TokenMismatch;
                    default:
                        return ResetPasswordStatusTypes.NoRecord;
                }
            }
        }

        /// <summary>
        /// Used to change the password
        /// </summary>
        /// <param name="model">Old password and new password as Change password view model</param>
        /// <returns>Return true or false if password is changed or not</returns>
        public ChangePasswordViewModel ChangePassword(ChangePasswordViewModel model)
        {
            try
            {
                _accountClient.ChangePassword(AccountViewModelMap.ToChangePasswordModel(model));

                return new ChangePasswordViewModel
                {
                    SuccessMessage = ZnodeResources.SuccessPasswordChanged,
                };
            }
            catch (ZnodeException ex)
            {
                return new ChangePasswordViewModel
                {
                    HasError = true,
                    ErrorMessage = (string.IsNullOrEmpty(ex.ErrorMessage)) ? ZnodeResources.ErrorChangePassword : ex.ErrorMessage,
                };
            }
        }

        #region Private Methods
        #region GetExpands
        /// <summary>
        /// Returns the Expands needed for the account agent.
        /// </summary>
        /// <returns></returns>
        private ExpandCollection GetExpands()
        {
            return new ExpandCollection()
                {
                    ExpandKeys.Addresses,
                    ExpandKeys.Profiles,
                    ExpandKeys.WishLists,
                    ExpandKeys.Orders,
                    ExpandKeys.OrderLineItems,
                    ExpandKeys.User,
                    ExpandKeys.GiftCardHistory
                };
        }
        #endregion

        #region GetDomainUrl
        /// <summary>
        /// To Get the Domain Url
        /// </summary>
        /// <returns>Return the Domain Url.</returns>
        private string GetDomainUrl()
        {
            string baseDomainUrl = (!string.IsNullOrEmpty(HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority))) ? HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) : string.Empty;
            string areaName = (Equals(HttpContext.Current.Request.RequestContext.RouteData.DataTokens[MvcAdminConstants.AreaKey], null)) ? string.Empty : Convert.ToString(HttpContext.Current.Request.RequestContext.RouteData.DataTokens[MvcAdminConstants.AreaKey]);
            return (string.IsNullOrEmpty(areaName)) ? baseDomainUrl : string.Format("{0}/{1}", baseDomainUrl, areaName);
        }
        #endregion

        /// <summary>
        /// Reset password method
        /// </summary>
        /// <param name="model">AccountViewModel</param>
        /// <returns>Returns password changed successfully or not</returns>
        private AccountViewModel ResetPassword(AccountViewModel model)
        {
            try
            {
                _accountClient.ResetPassword(AccountViewModelMap.ToAccountModel(model));
                model.SuccessMessage = Resources.ZnodeResources.SuccessResetPassword;
            }
            catch (ZnodeException ex)
            {
                model.HasError = true;
                model.ErrorMessage = ex.ErrorMessage;
            }
            return model;
        }

        /// <summary>
        /// To Update the View Model, based on the error code conditions for the logged in user.
        /// </summary>
        /// <param name="model">LoginViewModel model</param>
        /// <param name="ex">ZnodeException ex</param>
        /// <param name="isResetAdmin">bool isResetAdmin</param>
        /// <param name="accountModel">out AccountModel accountMode</param>
        /// <param name="loginViewModel">out LoginViewModel loginViewModel</param>
        /// <returns>Return the mapped view moded in LoginViewModel format</returns>
        private LoginViewModel ReturnErrorViewModel(LoginViewModel model, ZnodeException ex, bool isResetAdmin, out AccountModel accountModel, out LoginViewModel loginViewModel)
        {
            accountModel = _accountClient.GetAccountByUser(model.Username);
            SaveInSession(MvcAdminConstants.UserAccountSessionKey, accountModel);
            SaveInSession(MvcAdminConstants.ErrorCodeSessionKey, ex.ErrorCode);
            loginViewModel = new LoginViewModel();
            if (isResetAdmin)
            {
                loginViewModel.IsResetAdmin = true;
            }
            else
            {
                loginViewModel.IsResetAdminPassword = true;
            }
            loginViewModel.HasError = true;
            loginViewModel.ErrorMessage = ex.ErrorMessage;
            return loginViewModel;
        }
        #endregion

        /// <summary>
        /// Return collection of active countries for specified portal id.
        /// </summary>
        /// <param name="portalId">portal id</param>
        /// <returns>Collection of CountryModel</returns>
        public Collection<CountryModel> GetCountries(int portalId)
        {
            CountryListModel countryListModel = _countriesClient.GetActiveCountryByPortalId(portalId);
            return countryListModel.Countries;
        }

        /// <summary>
        /// Return collection of active countries for specified portal id.
        /// </summary>
        /// <param name="portalId">portal id</param>
        /// <returns>Collection of CountryModel</returns>
        public Collection<CountryModel> GetCountriesByPortalId(int portalId)
        {
            CountryListModel countryListModel = _countriesClient.GetCountryByPortalId(portalId);
            return countryListModel.Countries;
        }

        /// <summary>
        /// Gets the account view model from the session
        /// </summary>
        /// <returns>Returns the account view model with the orders, order history, wish list, profile for the user</returns>
        public AccountViewModel GetAccountViewModel()
        {
            var accountViewModel = GetFromSession<AccountViewModel>(MvcAdminConstants.AccountKey);

            if (Equals(accountViewModel, null))
                return null;

            // To hold the last action message for certain scenarios.
            var returnModel = accountViewModel.Clone() as AccountViewModel;

            // Reset the messages from session for certain scenarios.
            accountViewModel.HasError = false;
            accountViewModel.SuccessMessage = accountViewModel.ErrorMessage = string.Empty;

            SaveInSession(MvcAdminConstants.AccountKey, accountViewModel);

            return returnModel;
        }

        /// <summary>
        /// Update the account view model to the session
        /// </summary>
        /// <returns>Returns the account view model with the orders, order history, wish list, profile for the user</returns>
        public void UpdateAccountViewModel(object model)
        {
            var accountModel = GetFromSession<AccountViewModel>(MvcAdminConstants.AccountKey);

            if (!HttpContext.Current.User.Identity.IsAuthenticated || Equals(accountModel, null)) return;

            if (model is WishListItemViewModel)
            {
                var viewModel = model as WishListItemViewModel;
                if (accountModel.WishList.Items.Any(x => x.WishListId == viewModel.WishListId))
                    accountModel.WishList.Items.Remove(accountModel.WishList.Items.First(x => x.WishListId == viewModel.WishListId));

                viewModel.Product = ProductViewModelMap.ToViewModel(
                    _productsClient.GetProduct(viewModel.ProductId,
                                               new ExpandCollection
                                                   {
                                                       ExpandKeys.AddOns, ExpandKeys.BundleItems, ExpandKeys.Attributes, 
                                                       ExpandKeys.Categories, ExpandKeys.Promotions, ExpandKeys.Skus
                                                   }));

                accountModel.WishList.Items.Add(viewModel);
            }
            else if (model is AddressViewModel)
            {
                var viewModel = model as AddressViewModel;
                if (accountModel.Addresses.Any(x => x.AddressId == viewModel.AddressId))
                    accountModel.Addresses.Remove(accountModel.Addresses.First(x => x.AddressId == viewModel.AddressId));
                accountModel.Addresses.Add(viewModel);
                accountModel.Addresses = new Collection<AddressViewModel>(accountModel.Addresses.OrderBy(x => x.AddressId).ToList());

                // Set the address success message to account model
                SetMessage(accountModel, viewModel);
            }
            else if (model is OrdersViewModel)
            {
                var viewModel = model as OrdersViewModel;
                if (accountModel.Orders.Any(x => x.OrderId == viewModel.OrderId))
                    accountModel.Orders.Remove(accountModel.Orders.First(x => x.OrderId == viewModel.OrderId));
                accountModel.Orders.Add(viewModel);
                accountModel.Orders = new Collection<OrdersViewModel>(accountModel.Orders.OrderByDescending(x => x.OrderDate).ToList());
            }

            SaveInSession(MvcAdminConstants.AccountKey, accountModel);
        }

        /// <summary>
        /// Sets the messages
        /// </summary>
        /// <param name="accountModel">Account Model</param>
        /// <param name="viewModel">View Model</param>
        private void SetMessage(AccountViewModel accountModel, BaseViewModel viewModel)
        {
            accountModel.SuccessMessage = viewModel.SuccessMessage;
            accountModel.HasError = viewModel.HasError;
            accountModel.ErrorMessage = viewModel.ErrorMessage;
            viewModel.ErrorMessage = viewModel.SuccessMessage = string.Empty;
            viewModel.HasError = false;
        }

        /// <summary>
        /// Update the Model with error properties.
        /// </summary>
        /// <param name="model">Model for the AccountViewModel</param>
        /// <param name="errorMessage">Error Message to be display on UI</param>
        /// <returns>Return the updated model in AccountViewModelFormat</returns>
        private AccountViewModel ErrorModel(AccountViewModel model, string errorMessage)
        {
            model.HasError = true;
            model.ErrorMessage = errorMessage;
            return model;
        }

        /// <summary>
        /// To Get User Role Name based on Current Request Area.
        /// </summary>
        /// <returns>Return User Role Name.</returns>
        private string GetUserRoleName(string userName, out bool skipRoleCheck)
        {
            skipRoleCheck = false;
            string roleName = string.Empty;
            string areaName = (Equals(HttpContext.Current.Request.RequestContext.RouteData.DataTokens[MvcAdminConstants.AreaKey], null)) ? string.Empty : Convert.ToString(HttpContext.Current.Request.RequestContext.RouteData.DataTokens[MvcAdminConstants.AreaKey]);
            roleName = (string.IsNullOrEmpty(areaName)) ? Constant.RoleAdmin : (Equals(areaName, MvcAdminConstants.MallAdminArea)) ? Constant.RoleVendor : (Equals(areaName, MvcAdminConstants.FranchiseAdminArea)) ? Constant.RoleFranchise : Constant.RoleAdmin;
            //Check to set the role name only in case of Admin site.
            if (Equals(roleName, Constant.RoleAdmin))
            {
                roleName = SetUserRoleName(userName, ref skipRoleCheck);
            }
            return roleName;
        }
        /// <summary>
        /// To Set Role Name to Admin in Case the User has role of Vendor,Franchise & using site admin url to Reset Password.
        /// And also set the role name to empty in case the user is a customer which dont have any role.
        /// </summary>
        /// <param name="userName">userName for Logged In User</param>
        /// <param name="skipRoleCheck"> ref Param to set the skipRoleCheck flag</param>
        /// <returns>Return the Role Name based on Logged in User.</returns>
        private string SetUserRoleName(string userName, ref bool skipRoleCheck)
        {
            string roleName = string.Empty;
            if (IsUserInRole(userName, Constant.RoleVendor))
            {
                //Set Role Name to Admin to Restrict the user.
                roleName = Constant.RoleAdmin;
            }
            else if (IsUserInRole(userName, Constant.RoleFranchise))
            {
                //Set Role Name to Admin to Restrict the user.
                roleName = Constant.RoleAdmin;
            }
            else if (IsUserInRole(userName, Constant.RoleAdmin) || IsUserInRole(userName, Constant.RoleCatalogEditor) || IsUserInRole(userName, Constant.RoleContentEditor) || IsUserInRole(userName, Constant.RoleCustomerServiceRep) || IsUserInRole(userName, Constant.RoleExecutive) || IsUserInRole(userName, Constant.RoleOrderApprover) || IsUserInRole(userName, Constant.RoleOrderOnly) || IsUserInRole(userName, Constant.RoleReviewer) || IsUserInRole(userName, Constant.RoleSEO))
            {
                //Set skipRoleCheck to allow reset password.
                skipRoleCheck = true;
            }
            return roleName;
        }

        /// <summary>
        /// Get Account Data from Session.
        /// </summary>
        /// <returns>Return Account Data in the AccountViewModel format.</returns>
        public AccountViewModel GetAccounts()
        {
            return GetFromSession<AccountViewModel>(MvcAdminConstants.UserAccountSessionKey);
        }

        /// <summary>
        /// To Get the User Role Permission list based on the logged in user name.
        /// </summary>
        /// <returns>Return the Role Permission list in RolePermissionListViewModel format.</returns>
        public RolePermissionListViewModel GetRolePermission()
        {
            return AccountViewModelMap.ToListModel(_accountClient.GetRolePermission(HttpContext.Current.User.Identity.Name));
        }

        /// <summary>
        /// To Check for the existance of WishLists
        /// </summary>
        /// <param name="productId">Id for the product</param>
        /// <param name="accountId">Id for the Account</param>
        /// <returns>returns the Wishlist id.</returns>
        public int CheckWishListExists(int productId, int accountId)
        {
            AccountViewModel accountModel = GetAccountViewModel();

            if (accountModel != null && accountModel.WishList.Items.Any())
            {
                var productExist = accountModel.WishList.Items.FirstOrDefault(x => x.ProductId == productId && x.AccountId == accountId);

                if (productExist != null)
                {
                    return productExist.WishListId;
                }               
            }
            return 0;
        }

        /// <summary>
        /// To Get the Menu List based on login user Role.
        /// </summary>
        /// <param name="userName">UserName for the login User.</param>
        /// <returns>Return Role Menu List in RoleMenuListModel format.</returns>
        public RoleMenuListViewModel GetRoleMenuList(string userName)
        {
            return AccountViewModelMap.ToMenuListModel(_accountClient.GetRoleMenuList(userName));
        }

        /// <summary>
        /// To Get the account Details based on user account id.
        /// </summary>
        /// <param name="accountId">User accountId</param>
        /// <returns>Return the Account details in AccountViewModel format</returns>
        public AccountViewModel GetAccountByAccountId(int accountId)
        {
            var accountModel = _accountClient.GetAccount(accountId, new ExpandCollection { ExpandKeys.User });
            return Equals(accountModel, null) ? new AccountViewModel() : AccountViewModelMap.ToAccountViewModel(accountModel);
        }
    }

}