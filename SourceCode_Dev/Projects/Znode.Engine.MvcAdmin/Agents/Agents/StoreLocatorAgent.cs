﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public class StoreLocatorAgent : BaseAgent, IStoreLocatorAgent
    {
        #region Private Variabls
        private readonly IStoreLocatorClient _storeLocatorClient;
        private readonly IPortalsClient _portalsClient;
        private readonly IProductsClient _productsClient;
        private readonly IPortalAgent _portalAgent;
        #endregion

        #region Public Constructor

        /// <summary>
        /// Constructor for the Store locator.
        /// </summary>
        public StoreLocatorAgent()
        {
            _storeLocatorClient = GetClient<StoreLocatorClient>();
            _portalsClient = GetClient<PortalsClient>();
            _productsClient = GetClient<ProductsClient>();
            _portalAgent = new PortalAgent();
        }

        #endregion

        #region Public Methods

        public StoreLocatorListViewModel GetStoreLocators(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            if (Equals(filters, null))
            {
                filters = new FilterCollection();
            }
            filters.Add(new FilterTuple(FilterKeys.Username, FilterOperators.Contains, HttpContext.Current.User.Identity.Name));
            _storeLocatorClient.RefreshCache = true;
            var list = _storeLocatorClient.GetStoreLocators(null, filters, sortCollection, pageIndex - 1, recordPerPage);
            return Equals(list, null) ? null : StoreLocatorViewModelMap.ToListViewModel(list);
        }

        public bool SaveStore(StoreLocatorViewModel model)
        {

            GetImagePath(model);
            var storeLocator = _storeLocatorClient.CreateStoreLocator(StoreLocatorViewModelMap.ToModel(model));

            if (storeLocator.StoreID > 0)
            {
                model.ImageFile = storeLocator.ImageFile;
                model.StoreID = storeLocator.StoreID;
                SaveImage(model);
            }
            return (storeLocator.StoreID > 0);
        }



        public StoreLocatorViewModel GetStoreLocatorByID(int storeId)
        {
            StoreModel model = _storeLocatorClient.GetStoreLocator(storeId);
            if (Equals(model.ImageFile, null) || Equals(model.ImageFile, string.Empty))
            {
                PortalModel portalModel = _portalsClient.GetPortalInformationByPortalId(model.PortalID);
                model.ImageFile = portalModel.ImageNotAvailablePath;
            }
            return !Equals(model, null) ? StoreLocatorViewModelMap.ToViewModel(model) : null;
        }

        public List<PortalViewModel> GetPortalsList()
        {
            PortalListViewModel portalListModel = new PortalListViewModel();
            _portalsClient.RefreshCache = true;

            portalListModel = _portalAgent.GetPortals();

            return !Equals(portalListModel, null) && !Equals(portalListModel.Portals, null) ? (portalListModel.Portals.ToList()) : new List<PortalViewModel>();
        }

        public bool UpdateStoreLocator(StoreLocatorViewModel model)
        {
            GetImagePath(model);
            var storeLocator = _storeLocatorClient.UpdateStoreLocator(model.StoreID, StoreLocatorViewModelMap.ToModel(model));

            if (storeLocator.StoreID > 0)
            {
                model.ImageFile = storeLocator.ImageFile;
                SaveImage(model);
            }
            return !Equals(storeLocator, null);
        }

        public bool DeleteStoreLocator(int storeId)
        {
            return _storeLocatorClient.DeleteStoreLocator(storeId);
        }

        public void SetStoreLocatorProperty(StoreLocatorViewModel model)
        {
            if (!Equals(model.StoreImage, null))
            {
                model.ImageFile = (!Equals(model.StoreImage, null)) ? model.StoreImage.FileName : null;
            }
            else
            {
                model.ImageFile = model.ImageFile;
            }
            model.ImageFile = model.NoImage ? null : model.ImageFile;
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// To save image by Id in data folder and update image name in database table
        /// </summary>
        /// <param name="model">StoreLocatorViewModel model</param>
        private void SaveImage(StoreLocatorViewModel model)
        {
            if (!Equals(model.StoreImage, null))
            {
                //to save image in data folder
                ImageHelper imageHelper = new ImageHelper();
                string validExtension = Convert.ToString(ConfigurationManager.AppSettings[MvcAdminConstants.ImageExtension]);
                string imagePath = imageHelper.SaveImageToPhysicalPathByName(null, null, validExtension, model.ImageFile, model.StoreImage, null);
                if (!string.IsNullOrEmpty(imagePath) && !Equals(imagePath, MvcAdminConstants.FileUploadErrorCode))
                {
                    //to update image path in database by id
                    UpdateImageModel imageModel = new UpdateImageModel();
                    imageModel.Id = model.StoreID;
                    imageModel.EntityName = Convert.ToString(EntityName.Store);
                    imageModel.ImagePath = imagePath;
                    _productsClient.UpdateImage(imageModel);
                }
            }
        }

        /// <summary>
        /// To set image path to store in database.
        /// </summary>
        /// <param name="model">StoreLocatorViewModel</param>
        private static void GetImagePath(StoreLocatorViewModel model)
        {
            if (Equals(model.StoreID, 0))
            {
                model.ImageFile = !Equals(model.ImageFile, null) && !Equals(model.ImageFile, string.Empty) ?
                Path.GetFileNameWithoutExtension(model.ImageFile) + MvcAdminConstants.UnderScore + DateTime.Now.ToString(MvcAdminConstants.ImageWithDateFormat) + Path.GetExtension(model.ImageFile)
                : string.Empty;
            }
        }
        #endregion
    }
}