﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    /// <summary>
    /// URL Redirect Agent
    /// </summary>
    public class URLRedirectAgent : BaseAgent, IURLRedirectAgent
    {
        #region Private Variables
        private readonly IUrlRedirectClient _urlRedirectClient;

        private string trueValue = "true";
        private string falseValue = "false";
        private string isactive = "isactive";
        private string value = "Value";
        private const string selectZeroIndex = "0";
        private const string selectFirstIndex = "1";
        #endregion

        #region Constructor
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public URLRedirectAgent()
        {
            _urlRedirectClient = GetClient<UrlRedirectClient>();
        }
        #endregion

        #region Public Methods

        public URLRedirectListViewModel GetUrlRedirects(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            bool isOldUrlPresent = false;
            bool isNewUrlPresent = false;

            #region Update Filter value
            if (!Equals(filters, null) && filters.Count.Equals(0))
            {
                filters.Add(new FilterTuple(FilterKeys.OldUrl, FilterOperators.NotEquals, string.Empty));
                filters.Add(new FilterTuple(FilterKeys.NewUrl, FilterOperators.NotEquals, string.Empty));
            }
            else
            {
                foreach (var tuple in filters)
                {
                    if (Equals(tuple.Item1.ToLower(), FilterKeys.OldUrl))
                    {
                        isOldUrlPresent = true;
                    }
                    if (Equals(tuple.Item1.ToLower(), FilterKeys.NewUrl))
                    {
                        isNewUrlPresent = true;
                    }
                    if (Equals(tuple.Item1.ToLower(), FilterKeys.Value))
                    {
                        var activeValue = tuple.Item3;
                        filters.Add(new FilterTuple(FilterKeys.IsActive, FilterOperators.Equals, Equals(activeValue, selectFirstIndex) ? trueValue : falseValue));
                        filters.Remove(tuple);
                        break;
                    }
                }
                if(!isOldUrlPresent)
                {
                    filters.Add(new FilterTuple(FilterKeys.OldUrl, FilterOperators.NotEquals, string.Empty));
                }
                if(!isNewUrlPresent)
                {
                    filters.Add(new FilterTuple(FilterKeys.NewUrl, FilterOperators.NotEquals, string.Empty));
                }
            }
            #endregion
            _urlRedirectClient.RefreshCache = true;
            UrlRedirectListModel list = _urlRedirectClient.Get301Redirects(null, filters, sortCollection, pageIndex - 1, recordPerPage);

            #region Undo Filter value
            foreach (var tuple in filters)
            {
                if (Equals(tuple.Item1.ToLower(), isactive) && (Equals(tuple.Item3.ToLower(), trueValue)))
                {
                    filters.Add(new FilterTuple(value, FilterOperators.Equals, selectFirstIndex));
                    filters.Remove(tuple);
                    break;
                }
                else if (Equals(tuple.Item1.ToLower(), isactive) && (Equals(tuple.Item3.ToLower(), falseValue)))
                {
                    filters.Add(new FilterTuple(value, FilterOperators.Equals, selectZeroIndex));
                    filters.Remove(tuple);
                    break;
                }
            }
            #endregion

            return Equals(list, null) ? new URLRedirectListViewModel() : URLRedirectViewModelMap.ToListViewModel(list);
        }

        public URLRedirectViewModel GetUrlRedirect(int UrlRedirectId)
        {
            _urlRedirectClient.RefreshCache = true;
            UrlRedirectModel urlRedirect = _urlRedirectClient.GetUrlRedirect(UrlRedirectId);
            return (!Equals(urlRedirect, null)) ? URLRedirectViewModelMap.ToViewModel(urlRedirect) : null;
        }

        public bool CreateUrlRedirect(URLRedirectViewModel model, out string errorMessage)
        {
            errorMessage = string.Empty;
            try
            {
                UrlRedirectModel urlRedirectModel = _urlRedirectClient.CreateUrlRedirect(URLRedirectViewModelMap.ToModel(model));
                return (!Equals(urlRedirectModel, null) && (urlRedirectModel.UrlRedirectId > 0));
            }
            catch (ZnodeException ex)
            {
                errorMessage = ex.ErrorMessage;
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool UpdateUrlRedirect(URLRedirectViewModel model, out string errorMessage)
        {
            errorMessage = string.Empty;
            try
            {
                UrlRedirectModel updatedGiftCard = _urlRedirectClient.UpdateUrlRedirect(model.URLRedirectId, URLRedirectViewModelMap.ToModel(model));
                return (!Equals(updatedGiftCard, null) && updatedGiftCard.UrlRedirectId > 0);
            }
            catch (ZnodeException ex)
            {
                errorMessage = ex.ErrorMessage;
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool DeleteUrlRedirect(int urlredirectId, out string errorMessage)
        {
            errorMessage = string.Empty;
            try
            {
                return _urlRedirectClient.DeleteUrlRedirect(urlredirectId);
            }
            catch (ZnodeException ex)
            {
                errorMessage = ex.ErrorMessage;
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public List<SelectListItem> GetStatusList()
        {
            return URLRedirectViewModelMap.ToListItems();
        }

        #endregion
    }
}