﻿using System;
using System.Configuration;
using System.Web;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.MvcAdmin.ViewModels;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.MvcAdmin.Agents
{
    public abstract class BaseAgent
    {
        public ExpandCollection Expands { get; set; }
        public FilterCollection Filters { get; set; }
        public SortCollection Sorts { get; set; }
        public int? PageIndex { get; set; }
        public int? PageSize { get; set; }

        /// <summary>
        /// Get API client object with current domain name and key.
        /// </summary>
        /// <typeparam name="T">The type of API client object.</typeparam>
        /// <returns>An API client object of type T.</returns>
        protected T GetClient<T>() where T : class
        {
            var obj = Activator.CreateInstance<T>();

            if (obj is BaseClient)
            {
                // Need to use Url.Authority to set domin name and its API key for multi store
                var urlAuthority = HttpContext.Current.Request.Url.Authority;
                (obj as BaseClient).DomainName = urlAuthority; // ConfigurationManager.AppSettings["ZnodeApiDomainName"];
                (obj as BaseClient).DomainKey = ConfigurationManager.AppSettings[urlAuthority]; // ConfigurationManager.AppSettings["ZnodeApiDomainKey"]           
                //(obj as BaseClient).RefreshCache = true; //For Admin request everytime get the data from database
                // Set account ID to get the profile based API response
                if (HttpContext.Current.User != null && HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    var model = GetFromSession<AccountViewModel>("UserAccount");//Helpers.MvcDemoConstants.AccountKey);
                    if (model != null)
                    {
                        (obj as BaseClient).AccountId = model.AccountId;
                    }
                }
            }

            return obj;
        }


        /// <summary>
        /// Gets a cookie value.
        /// </summary>
        /// <param name="key">The key for the value being retrieved.</param>
        /// <returns>The value for the key.</returns>
        protected string GetFromCookie(string key)
        {
            var cookie = HttpContext.Current.Request.Cookies.Get(key);
            if (cookie != null)
            {
                return cookie.Value;
            }

            return String.Empty;
        }

        /// <summary>
        /// Gets an object from session.
        /// </summary>
        /// <typeparam name="T">The type of object to retrieve.</typeparam>
        /// <param name="key">The key for the session object being retrieved.</param>
        /// <returns>The object of type T from session.</returns>
        protected T GetFromSession<T>(string key)
        {
            if (HttpContext.Current.Session != null)
            {

                var o = HttpContext.Current.Session[key];
                if (o is T)
                {
                    return (T)o;
                }
            }

            return default(T);
        }

        /// <summary>
        /// Saves a cookie value.
        /// </summary>
        /// <param name="key">The key for the cookie value.</param>
        /// <param name="value">The cookie value.</param>
        protected void SaveInCookie(string key, string value)
        {
            HttpContext.Current.Response.Cookies.Add(new HttpCookie(key)
            {
                Expires = DateTime.Now.AddYears(1),
                Name = key,
                Value = value
            });
        }

        /// <summary>
        /// Removes a cookie value.
        /// </summary>
        /// <param name="key">The key for the cookie value being removed.</param>
        protected void RemoveCookie(string key)
        {
            var cookie = HttpContext.Current.Request.Cookies.Get(key);
            if (cookie == null) return;
            cookie.Expires = DateTime.Now.AddYears(-1);
            HttpContext.Current.Response.Cookies.Add(cookie);
        }

        /// <summary>
        /// Saves an object in session.
        /// </summary>
        /// <typeparam name="T">The type of object being saved.</typeparam>
        /// <param name="key">The key for the session object.</param>
        /// <param name="value">The value of the session object.</param>
        protected void SaveInSession<T>(string key, T value)
        {
            HttpContext.Current.Session[key] = value;
        }

        /// <summary>
        /// Removes an object from session.
        /// </summary>
        /// <param name="key">The key of the session object.</param>
        protected void RemoveInSession(string key)
        {
            var obj = HttpContext.Current.Session[key];
            if (obj == null) return;

            HttpContext.Current.Session[key] = null;

            HttpContext.Current.Session.Remove(key);
        }

        protected void SetStatusMessage(BaseViewModel model)
        {
            if (HttpContext.Current.Session["ErrorMessage"] != null)
            {
                model.ErrorMessage = HttpContext.Current.Session["ErrorMessage"].ToString();
                model.HasError = true;
                HttpContext.Current.Session["ErrorMessage"] = null;
                HttpContext.Current.Session.Remove("ErrorMessage");
            }
            else if (HttpContext.Current.Session["SuccessMessage"] != null)
            {
                model.SuccessMessage = HttpContext.Current.Session["SuccessMessage"].ToString();
                HttpContext.Current.Session["SuccessMessage"] = null;
                HttpContext.Current.Session.Remove("SuccessMessage");
            }
        }

        /// <summary>
        /// Replace filter key name 
        /// </summary>
        /// <param name="filters">Reference of filter collection</param>
        /// <param name="keyName">Old Key Name</param>
        /// <param name="newKeyName">New key name</param>
        protected void ReplaceFilterKeyName(ref FilterCollection filters, string keyName, string newKeyName)
        {
            FilterCollection tempCollection = new FilterCollection();
            tempCollection = filters;
            FilterCollection newCollection = new FilterCollection();

            foreach (var tuple in filters)
            {
                if (Equals(tuple.Item1.ToLower(), keyName.ToLower()))
                    newCollection.Add(new FilterTuple(newKeyName, tuple.Item2, tuple.Item3));
            }
            foreach (var temp in tempCollection)
            {
                if (!Equals(temp.Item1.ToLower(), keyName.ToLower()))
                    newCollection.Add(temp);
            }
            filters = newCollection;
        }

        /// <summary>
        /// Replace sort key name
        /// </summary>
        /// <param name="sort">Sort collection</param>
        /// <param name="keyName">Old key name</param>
        /// <param name="newKeyName">New key name</param>
        protected void ReplaceSortKeyName(SortCollection sort, string keyName, string newKeyName)
        {
            if (!Equals(sort, null) && !Equals(sort.Count, 0))
            {
                if (sort["sort"].ToString().ToLower().Equals(keyName.ToLower()))
                {
                    sort["sort"] = newKeyName;
                }
            }
        }

        /// <summary>
        /// This method will log the exception in the admin log files
        /// </summary>
        /// <param name="ex">Exception ex which needs to be tracked</param>
        protected void LogException(Exception ex) 
        {
            //ZNodeLoggingBase.LogAdminMessage(string.Concat(ex.Message.ToString(), "\n", ex.StackTrace.ToString()));
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }

        /// <summary>
        /// This method will log the general message in the admin log files
        /// </summary>
        /// <param name="message">string message to be logged</param>
        protected void LogMessage(string message)
        {
            //ZNodeLoggingBase.LogAdminMessage(message);
        }
    }
}