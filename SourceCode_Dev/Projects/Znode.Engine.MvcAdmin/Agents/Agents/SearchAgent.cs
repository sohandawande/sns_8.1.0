﻿using Znode.Engine.Api.Client;

namespace Znode.Engine.MvcAdmin.Agents
{
    public class SearchAgent : BaseAgent, ISearchAgent
    {
        #region Private Variables
        private readonly ISearchClient _searchClient;
        #endregion

        #region Constructor
        public SearchAgent()
        {
            _searchClient = GetClient<SearchClient>();
        }
        #endregion

        #region Public Methods

        public bool IsRestrictedSeoUrl(string seoUrl)
        {
            if (string.IsNullOrEmpty(seoUrl))
            {
                return false;
            }
            _searchClient.RefreshCache = true;
            bool seoUrlCount = _searchClient.IsRestrictedSeoUrl(seoUrl);
            return Equals(seoUrlCount, null) ? false : seoUrlCount;
        }

        #endregion
    }
}