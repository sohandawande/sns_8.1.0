﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public class AffiliateTrackingAgent : BaseAgent, IAffiliateTrackingAgent
    {
        #region Private Variables
        private readonly IAffiliateTrackingClient _affiliateTrackingClient;

        #endregion

        #region Constructor
        public AffiliateTrackingAgent()
        {
            _affiliateTrackingClient = GetClient<AffiliateTrackingClient>();
        }
        #endregion

        #region Public Methods

        public DataSet GetAffiliateTrackingData(string startDate, string endDate)
        {
            _affiliateTrackingClient.RefreshCache = true;
            FilterCollection filters = new FilterCollection();
            filters.Add(new FilterTuple(FilterKeys.StartDate, FilterOperators.Equals, startDate.ToString()));
            filters.Add(new FilterTuple(FilterKeys.EndDate, FilterOperators.Equals, endDate.ToString()));
            var trackingData = _affiliateTrackingClient.GetTrackingData(filters);
            return !Equals(trackingData, null) ? trackingData : null;
        }

        public List<SelectListItem> FileTypeList()
        {
            DownloadHelper downloadHelper = new DownloadHelper();
            List<SelectListItem> fileTypes = new List<SelectListItem>();
            fileTypes.Add(new SelectListItem { Value = Convert.ToInt32(FileTypes.Excel).ToString(), Text = downloadHelper.ToEnumString(FileTypes.Excel) });
            fileTypes.Add(new SelectListItem { Value = Convert.ToInt32(FileTypes.CSV).ToString(), Text = downloadHelper.ToEnumString(FileTypes.CSV) });

            return fileTypes;
        }

        public void DownloadFile(AffiliateTrackingViewModel model, DataSet trackingData, HttpResponseBase response)
        {
            DownloadHelper downloadHelper = new DownloadHelper();
            downloadHelper.Download(trackingData, model.FileTypeId.ToString(), response);
        }

        #endregion
    }
}