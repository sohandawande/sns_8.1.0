﻿using System;
using System.Web.Hosting;
using Znode.Engine.Api.Client;
using Znode.Engine.MvcAdmin.Agents.IAgents;

namespace Znode.Engine.MvcAdmin.Agents
{
    public class LoadPluginsAgent: BaseAgent, ILoadPluginsAgent
    {
        
        private readonly ILoadPluginsClient _loadPluginsClient;

       
        public LoadPluginsAgent()
        {
            _loadPluginsClient = GetClient<LoadPluginsClient>();
        }

       
        public bool IsPluginsLoaded()
        {
            try
            {
                UpdateGlobalAsax();
                return _loadPluginsClient.IsPluginsLoaded();
            }
            catch (Exception)
            {
              return false;
            }
        }

        /// <summary>
        /// Update Global.asax file time
        /// </summary>
        private void UpdateGlobalAsax()
        {
            var path = HostingEnvironment.MapPath("~/global.asax");
            System.IO.File.SetLastWriteTimeUtc(path, DateTime.UtcNow);
        }
    }
}
