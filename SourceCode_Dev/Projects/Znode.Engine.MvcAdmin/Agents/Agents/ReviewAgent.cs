﻿using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public class ReviewAgent : BaseAgent, IReviewAgent
    {
        #region Private Variables

        private readonly IReviewsClient _reviewsClient;

        #endregion

        #region Constructor

        public ReviewAgent()
        {
            _reviewsClient = GetClient<ReviewsClient>();
        }

        #endregion

        #region Public Methods

        public ReviewViewModel GetReviews(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            ReplaceFilterKeyName(ref filters, FilterKeys.Value, FilterKeys.ReviewStatus);
            _reviewsClient.RefreshCache = true;
            var reviewList = _reviewsClient.GetReviews(new ExpandCollection { ExpandKeys.Product }, filters, sortCollection, pageIndex, recordPerPage);
            return ReviewViewModelMap.ToViewModels(reviewList.Reviews, reviewList.TotalResults, reviewList.PageSize, reviewList.TotalPages);
        }

        public ReviewItemViewModel GetReview(int reviewId)
        {
            _reviewsClient.RefreshCache = true;
            var reviewModel = _reviewsClient.GetReview(reviewId, new ExpandCollection { ExpandKeys.Product });

            if (!Equals(reviewModel, null))
            {
                return ReviewViewModelMap.ToViewModel(reviewModel);
            }
            return null;
        }

        public bool UpdateReviewStatus(ReviewItemViewModel model)
        {
            //Get the Review Model to update the Review Status.
            var reviewModel = _reviewsClient.GetReview(model.ReviewId);
            if (Equals(reviewModel, null)) return false;
            reviewModel.Status = model.Status;

            //updates the Review Status.
            var updatedReviews = _reviewsClient.UpdateReview(model.ReviewId, reviewModel);
            return !Equals(updatedReviews, null);
        }

        public bool UpdateReview(ReviewItemViewModel model)
        {
            var updatedReviews = _reviewsClient.UpdateReview(model.ReviewId, ReviewViewModelMap.ToReviewModel(model));
            return !Equals(updatedReviews, null);
        }

        public bool DeleteReview(int reviewId)
        {
            return _reviewsClient.DeleteReview(reviewId);
        }

        public ReviewViewModel BindStatus()
        {
            ReviewViewModel model = new ReviewViewModel();
            model.GetReviewStatusList = ReviewViewModelMap.GetReviewStatus(string.Empty);
            return model;
        }

        #endregion
    }
}