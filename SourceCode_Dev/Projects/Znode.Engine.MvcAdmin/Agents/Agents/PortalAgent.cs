﻿using Microsoft.AspNet.Identity;
using Resources;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.ViewModels;
using Znode.Engine.MvcDemo.Maps;
using Znode.Libraries.Helpers;

namespace Znode.Engine.MvcAdmin.Agents
{
    public class PortalAgent : BaseAgent, IPortalAgent
    {
        #region Private Variables
        #region Readonly Variables
        private readonly IPortalsClient _portalsClient;
        private readonly IDomainsClient _domainClient;
        private readonly ICatalogsClient _catalogClient;
        private readonly IThemeClient _themeClient;
        private readonly ICSSClient _cssClient;
        private readonly IOrderStateClient _orderStateClient;
        private readonly IProductReviewStateClient _productReviewStateClient;
        private readonly ILocaleClient _localeClient;
        private readonly IPortalCountriesClient _portalCountriesClient;
        private readonly IPortalCatalogsClient _portalCatalogsClient;
        private readonly IContentPageClient _contentPageClient;
        private readonly IMessageConfigsClient _messageConfigClient;
        private readonly ICurrencyTypeClient _currencyTypeClient;
        private readonly ICountriesClient _countryClient;
        private readonly IProductsClient _productsClient;
        private readonly IEnvironmentConfigClient _environmentConfigClient;
        #endregion

        #region Constant Variables
        private const string FedExCSPKey = "FedExCSPKey";
        private const string FedExCSPPassword = "FedExCSPPassword";
        private const string FedExClientProductVersion = "FedExClientProductVersion";
        private const string FedExClientProductId = "FedExClientProductId";
        private const string HashUrl = "#";
        private const int PendingApprovalId = 50;
        private const string DateFormat = "ddMMyyhhmmss";
        private const string defaultName = "<Name>";
        #endregion
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor for portal agent.
        /// </summary>
        public PortalAgent()
        {
            _portalsClient = GetClient<PortalsClient>();
            _domainClient = GetClient<DomainsClient>();
            _catalogClient = GetClient<CatalogsClient>();
            _themeClient = GetClient<ThemeClient>();
            _cssClient = GetClient<CSSClient>();
            _orderStateClient = GetClient<OrderStateClient>();
            _productReviewStateClient = GetClient<ProductReviewStateClient>();
            _localeClient = GetClient<LocaleClient>();
            _portalCountriesClient = GetClient<PortalCountriesClient>();
            _portalCatalogsClient = GetClient<PortalCatalogsClient>();
            _contentPageClient = GetClient<ContentPageClient>();
            _messageConfigClient = GetClient<MessageConfigsClient>();
            _currencyTypeClient = GetClient<CurrencyTypeClient>();
            _countryClient = GetClient<CountriesClient>();
            _productsClient = GetClient<ProductsClient>();
            _environmentConfigClient = GetClient<EnvironmentConfigClient>();
        }
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets the current portal.
        /// </summary>
        public static PortalViewModel CurrentPortal
        {
            get
            {
                if (HttpContext.Current.Cache[HttpContext.Current.Request.Url.Authority] == null)
                {
                    var agent = new PortalAgent();
                    var model = agent.GetCurrentPortal();

                    if (!Equals(model, null))
                    {
                        HttpContext.Current.Cache.Insert(HttpContext.Current.Request.Url.Authority, model, null, DateTime.Now.AddMinutes(5),
                                                         Cache.NoSlidingExpiration);
                    }
                }
                return HttpContext.Current.Cache[HttpContext.Current.Request.Url.Authority] as PortalViewModel;
            }
        }

        public static EnvironmentConfigModel GetEnvironmentConfig
        {
            get
            {
                var environmentConfig = new EnvironmentConfigClient();
                return environmentConfig.GetEnvironmentConfig();
            }
        }
        #endregion

        #region Public Methods

        public bool DeletePortal(int portalId)
        {
            try
            {
                return _portalsClient.DeletePortalByPortalId(portalId);
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public PortalListViewModel GetPortals(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            PortalListViewModel portalListViewModel = new PortalListViewModel();

            if (Equals(filters, null))
            {
                filters = new FilterCollection();
            }
            filters.Add(new FilterTuple(FilterKeys.Username, FilterOperators.Equals, string.Format("'{0}'", HttpContext.Current.User.Identity.GetUserName())));
            _portalsClient.RefreshCache = true;
            var list = _portalsClient.GetPortalsByProfileAccess(Expands, filters, sortCollection, pageIndex, recordPerPage);

            portalListViewModel = PortalViewModelMap.ToListViewModel(list, pageIndex, recordPerPage);

            DomainAgent domainAgent = new DomainAgent();
            DomainListViewModel allDomainList = domainAgent.GetDomains();

            foreach (var portal in portalListViewModel.Portals)
            {
                portal.DomainName = this.GetPortalDomain(portal.PortalId, allDomainList);
                if (string.IsNullOrEmpty(portal.DomainName))
                {
                    portal.DomainName = HashUrl;
                }
            }
            return portalListViewModel;
        }

        public PortalListViewModel GetAllPortals()
        {
            PortalListViewModel portalListViewModel = new PortalListViewModel();
            _portalsClient.RefreshCache = true;
            var list = _portalsClient.GetPortals(null, null, null);

            DomainAgent domainAgent = new DomainAgent();
            DomainListViewModel allDomainList = domainAgent.GetDomains();

            if (!Equals(list.Portals, null) || list.Portals.Count > 0)
            {
                portalListViewModel = PortalViewModelMap.ToListViewModel(list, null, null);

                foreach (var portal in portalListViewModel.Portals)
                {
                    portal.DomainName = this.GetPortalDomain(portal.PortalId, allDomainList);
                }
            }
            return portalListViewModel;
        }

        public PortalViewModel GetPortalInformationByPortalId(int portalId)
        {
            PortalViewModel portalViewModel = new PortalViewModel();
            _portalsClient.RefreshCache = true;
            var model = PortalViewModelMap.ToEditViewModel(_portalsClient.GetPortalInformationByPortalId(portalId));
            if (!Equals(model, null))
            {
                portalViewModel = model;
            }

            this.BindPortalInformation(portalViewModel);
            portalViewModel.WeightUnits = this.GetWeightUnits();
            portalViewModel.DimensionUnits = this.GetDimensionsUnit();
            Expands = new ExpandCollection();
            Expands.Add(ExpandKeys.Country);
            _countryClient.RefreshCache = true;
            portalViewModel.Countries = PortalViewModelMap.ToSelectListItems(_countryClient.GetCountries(Expands, Filters, new SortCollection()).Countries);
            portalViewModel.FedexDropOffTypes = this.GetFedexDropOffType();
            portalViewModel.PackagingTypes = this.GetFedexPackagingType();

            this.SetCurrencyDetails(portalViewModel);

            PortalCatalogModel portalCatalog = new PortalCatalogModel();
            PortalCatalogListModel portalcatalogListModel = _portalCatalogsClient.GetPortalCatalogsByPortalId(portalId);
            if (!Equals(portalcatalogListModel.PortalCatalogs, null) && portalcatalogListModel.PortalCatalogs.Count > 0)
            {
                portalCatalog = portalcatalogListModel.PortalCatalogs[0];
            }

            portalViewModel.CatalogId = portalCatalog.CatalogId;
            portalViewModel.ThemeId = portalCatalog.ThemeId;

            portalViewModel.CSSId = portalCatalog.CssId;

            return portalViewModel;
        }

        public PortalViewModel GetPortalByPortalId(int portalId)
        {
            PortalViewModel portal = PortalViewModelMap.ToViewModel(_portalsClient.GetPortal(portalId));
            if (!Equals(portal, null))
            {
                return portal;
            }
            return new PortalViewModel();
        }

        public PortalViewModel UpdatePortal(PortalViewModel portalViewModel, int tabMode)
        {
            switch (tabMode)
            {
                case (int)SelectedTab.General:
                    return UpdatePortalGeneralInformation(portalViewModel);
                case (int)SelectedTab.Display:
                    return UpdatePortalDisplay(portalViewModel);
                case (int)SelectedTab.Smtp:
                    return UpdatePortalSmtp(portalViewModel);
                case (int)SelectedTab.Javascript:
                    return UpdatePortalJavascript(portalViewModel);
                case (int)SelectedTab.Shipping:
                    return UpdatePortalShipping(portalViewModel);
                case (int)SelectedTab.Units:
                    return UpdatePortalUnits(portalViewModel);
                case (int)SelectedTab.Profile:
                    return UpdatePortalProfileInformation(portalViewModel);
                default:
                    return new PortalViewModel();
            }
        }

        public PortalViewModel BindPortalInformation(PortalViewModel portalViewModel)
        {
            if (!Equals(portalViewModel, null))
            {
                portalViewModel.Catalogs = PortalViewModelMap.ToSelectListItems(_catalogClient.GetCatalogs(Filters, new SortCollection()).Catalogs);
                portalViewModel.Themes = PortalViewModelMap.ToSelectListItems(_themeClient.GetThemes(Filters, new SortCollection()).Themes);
                portalViewModel.CSS = this.BindCssList();
                portalViewModel.OrderState = PortalViewModelMap.ToSelectListItems(_orderStateClient.GetOrderStates(Expands, Filters, new SortCollection()).OrderStates);
                portalViewModel.ProductReviewState = PortalViewModelMap.ToSelectListItems(_productReviewStateClient.GetProductReviewStates(Expands, Filters, new SortCollection()).ProductReviewStates);
                portalViewModel.DefaultReviewStates = this.GetCustomerReviewState();
                portalViewModel.Locales = PortalViewModelMap.ToSelectListItems(_localeClient.GetLocales(Expands, Filters, new SortCollection()).Locales);
            }
            return portalViewModel;
        }

        public bool CreatePortal(PortalViewModel portalViewModel, out string notificationMessage)
        {
            notificationMessage = string.Empty;
            bool isPortalCreated = false;
            int localeId = 0;

            //Check if the Requires manual approval of Order state.
            if (portalViewModel.RequiresManualApproval)
            {
                portalViewModel.DefaultOrderStateId = 50;
            }

            //Checks if the store is multi-store.
            EnvironmentConfigModel environmentConfig = GetEnvironmentConfigVariables(portalViewModel);

            if (Equals(environmentConfig, null) || !environmentConfig.MultiStoreAdminEnabled)
            {
                notificationMessage = ZnodeResources.UpdateStores;
                return false;
            }

            //Sets the Fedex keys for the portal.
            SetFedexKeys(portalViewModel);
            //Gets the locale Id for the portal.
            localeId = GetLocale(portalViewModel, localeId);
            //Sets Default values of the store.
            SetDefaultValues(portalViewModel);
            //Sets the value from existing portal for the store.
            SetValuesFromExistingPortal(portalViewModel);
            //Sets the log path of the store.
            if (!Equals(portalViewModel.LogoPathImageName, CurrentPortal.ImageNotAvailablePathUrl))
            {
                portalViewModel.LogoPathImageName = GetLogoPath(portalViewModel.LogoPathImageName, environmentConfig);
            }
            if (Equals(portalViewModel.IsEnableCompare, false))
            {
                portalViewModel.CompareType = string.Empty;
            }

            PortalModel portal = _portalsClient.CreatePortal(PortalViewModelMap.ToModel(portalViewModel));
            isPortalCreated = portal.PortalId > 0 ? true : false;

            if (isPortalCreated)
            {
                portalViewModel.PortalId = portal.PortalId;
                //Sets data like PortalCountry, PortalCatalog, Default content pages and Defualt messages for the store.
                SaveImage(portalViewModel);
                isPortalCreated = SetPortalData(portalViewModel, isPortalCreated, localeId);
            }
            return isPortalCreated;
        }

        public bool CopyStore(int portalId)
        {
            if (portalId > 0)
            {
                return _portalsClient.CopyStore(portalId);
            }
            return false;
        }

        public List<SelectListItem> BindCssList(int? cssThemeId = null)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            Filters = new FilterCollection { new FilterTuple(FilterKeys.CSSThemeId, FilterOperators.Equals, cssThemeId.ToString()) };
            _cssClient.RefreshCache = true;
            CSSListModel cssModel = _cssClient.GetCSSs(null, Filters, new SortCollection());
            return PortalViewModelMap.ToSelectListItems(cssModel.CSSs);
        }

        public object GetCurrencyInformationByCurrencyId(int currencyId)
        {
            CurrencyTypeModel currencyType = this.GetCurrencyType(currencyId);
            string currencySymbol = currencyType.Name;
            CultureInfo info = new CultureInfo(currencySymbol);
            decimal price = 100.12M;
            string preview = price.ToString("c", info.NumberFormat);
            if (!string.IsNullOrEmpty(currencyType.CurrencySuffix))
            {
                preview = string.Concat(preview, "(", currencyType.CurrencySuffix, ")");
            }
            object currencyInfo = new { currencySuffix = currencyType.CurrencySuffix, currencyPreview = preview };
            return currencyInfo;
        }

        public CurrencyTypeModel GetCurrencyType(int currencyTypeId)
        {
            CurrencyTypeModel currencyType = _currencyTypeClient.GetCurrencyType(currencyTypeId);
            return currencyType;
        }

        public PortalSelectListViewModel GetPortalSelectList()
        {
            PortalListViewModel portals = GetAllPortals();
            return new PortalSelectListViewModel() { PortalSelectList = PortalViewModelMap.ToSelectListItems(portals.Portals) };
        }

        #endregion

        #region Private Method
        /// <summary>
        /// Gets the current portal.
        /// </summary>
        /// <returns>PortalViewModel</returns>
        private PortalViewModel GetCurrentPortal()
        {
            _domainClient.RefreshCache = true;
            var list =
                _domainClient.GetDomains(
                    new FilterCollection()
                        {
                            new FilterTuple(FilterKeys.DomainName, FilterOperators.Equals,
                                            HttpContext.Current.Request.Url.Authority)
                        }, new SortCollection());

            if (list.Domains.Any())
            {
                _portalsClient.RefreshCache = true;
                var portal = _portalsClient.GetPortal(list.Domains.First().PortalId, new ExpandCollection() { ExpandKeys.PortalCatalogs, ExpandKeys.Catalogs, ExpandKeys.CurrencyType });
                return PortalViewModelMap.ToViewModel(portal);
            }
            return null;
        }

        /// <summary>
        /// Gets domain according to portal id
        /// </summary>
        /// <param name="id">portal id</param>
        /// <returns>protal domain's name</returns>
        private string GetPortalDomain(int id, DomainListViewModel domainList)
        {
            string portalUrl = string.Empty;
            DomainAgent domainAgent = new DomainAgent();

            DomainListViewModel domainListByPortalId = new DomainListViewModel();

            domainListByPortalId.Domains = (from domain in domainList.Domains
                                            where domain.PortalId.Equals(id)
                                            select domain).ToList();

            if (!Equals(domainListByPortalId, null))
            {
                portalUrl = domainAgent.GetDomains(id).Domains.Where(x => x.IsActive).Select(x => x.DomainName).FirstOrDefault() as string;
            }
            return portalUrl;
        }

        /// <summary>
        /// Get List of select list items for customer review states.
        /// </summary>
        /// <returns>List of Select List items.</returns>
        private List<SelectListItem> GetCustomerReviewState()
        {
            List<SelectListItem> customerReviewState = new List<SelectListItem>(){
                new SelectListItem(){Text=MvcAdminConstants.PublishImmediatelyText, Value=MvcAdminConstants.PublishImmediately},
                new SelectListItem(){Text=MvcAdminConstants.DoNotPublishText,Value=MvcAdminConstants.DoNotPublish,Selected=true}
            };
            return customerReviewState;
        }

        /// <summary>
        /// Get List of select list items for weights.
        /// </summary>
        /// <returns>List of Select List items.</returns>
        private List<SelectListItem> GetWeightUnits()
        {
            List<SelectListItem> weightUnits = new List<SelectListItem>(){
                new SelectListItem(){Text="LBS", Value="LBS"},
                new SelectListItem(){Text="KGS",Value="KGS"}
            };
            return weightUnits;
        }

        /// <summary>
        /// Get List of select list items for units.
        /// </summary>
        /// <returns>List of Select List items.</returns>
        private List<SelectListItem> GetDimensionsUnit()
        {
            List<SelectListItem> dimensionsUnit = new List<SelectListItem>(){
                new SelectListItem(){Text="IN", Value="IN"},
                new SelectListItem(){Text="CM",Value="CM"}
            };
            return dimensionsUnit;
        }

        /// <summary>
        /// Get List of select list items for Fedex drop off types.
        /// </summary>
        /// <returns>List of Select List items.</returns>
        private List<SelectListItem> GetFedexDropOffType()
        {
            List<SelectListItem> fedexDropOffTypes = new List<SelectListItem>(){
                new SelectListItem(){Text=MvcAdminConstants.BUSINESS_SERVICE_CENTERText, Value=MvcAdminConstants.BUSINESS_SERVICE_CENTER},
                new SelectListItem(){Text=MvcAdminConstants.DROP_BOXText,Value=MvcAdminConstants.DROP_BOX},
                new SelectListItem(){Text=MvcAdminConstants.REGULAR_PICKUP, Value=MvcAdminConstants.REGULAR_PICKUPText},
                new SelectListItem(){Text=MvcAdminConstants.REQUEST_COURIERText, Value=MvcAdminConstants.REQUEST_COURIERText},
                new SelectListItem(){Text=MvcAdminConstants.STATIONText, Value=MvcAdminConstants.STATIONText}
            };
            return fedexDropOffTypes;
        }

        /// <summary>
        /// Get List of select list items for Packaging types.
        /// </summary>
        /// <returns>List of Select List items.</returns>
        private List<SelectListItem> GetFedexPackagingType()
        {
            List<SelectListItem> fedexPackagingType = new List<SelectListItem>(){
                new SelectListItem(){Text=MvcAdminConstants.YOUR_PACKAGINGText, Value=MvcAdminConstants.YOUR_PACKAGING},
                new SelectListItem(){Text=MvcAdminConstants.FEDEX_BOXText,Value=MvcAdminConstants.FEDEX_BOX},
                new SelectListItem(){Text=MvcAdminConstants.FEDEX_ENVELOPEText, Value=MvcAdminConstants.FEDEX_ENVELOPE},
                new SelectListItem(){Text=MvcAdminConstants.FEDEX_TUBEText, Value=MvcAdminConstants.FEDEX_TUBE},
                new SelectListItem(){Text=MvcAdminConstants.FEDEX_PAKText, Value=MvcAdminConstants.FEDEX_PAK}
            };
            return fedexPackagingType;
        }

        /// <summary>
        /// Sets default fedex keys.
        /// </summary>
        /// <param name="portalViewModel">portalViewModel</param>
        private void SetFedexKeys(PortalViewModel portalViewModel)
        {
            DataSet ds = _portalsClient.GetFedexKeys();
            if (!Equals(ds, null) && !Equals(ds.Tables, null) && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                portalViewModel.FedExCspKey = ds.Tables[0].Rows[0][FedExCSPKey].ToString();
                portalViewModel.FedExCspPassword = ds.Tables[0].Rows[0][FedExCSPPassword].ToString();
                portalViewModel.FedExClientProductVersion = ds.Tables[0].Rows[0][FedExClientProductVersion].ToString();
                portalViewModel.FedExClientProductId = ds.Tables[0].Rows[0][FedExClientProductId].ToString();
            }
        }

        /// <summary>
        /// Sets the default value from existing portal.
        /// </summary>
        /// <param name="portalViewModel">portalViewModel</param>
        private void SetValuesFromExistingPortal(PortalViewModel portalViewModel)
        {
            PortalViewModel existingPortal = CurrentPortal;


            if (!Equals(existingPortal, null))
            {
                portalViewModel.SmtpPort = existingPortal.SmtpPort;
                portalViewModel.WeightUnit = existingPortal.WeightUnit;
                portalViewModel.DimensionUnit = existingPortal.DimensionUnit;

                portalViewModel.CurrencyTypeId = existingPortal.CurrencyTypeId;
                portalViewModel.MaxCatalogCategoryDisplayThumbnails = existingPortal.MaxCatalogCategoryDisplayThumbnails;
                portalViewModel.MaxCatalogDisplayColumns = existingPortal.MaxCatalogDisplayColumns;

                portalViewModel.MaxCatalogDisplayItems = existingPortal.MaxCatalogDisplayItems;
                portalViewModel.MaxCatalogItemCrossSellWidth = existingPortal.MaxCatalogItemCrossSellWidth;
                portalViewModel.MaxCatalogItemLargeWidth = existingPortal.MaxCatalogItemLargeWidth;

                portalViewModel.MaxCatalogItemMediumWidth = existingPortal.MaxCatalogItemMediumWidth;
                portalViewModel.MaxCatalogItemSmallThumbnailWidth = existingPortal.MaxCatalogItemSmallThumbnailWidth;
                portalViewModel.MaxCatalogItemSmallWidth = existingPortal.MaxCatalogItemSmallWidth;

                portalViewModel.MaxCatalogItemThumbnailWidth = existingPortal.MaxCatalogItemThumbnailWidth;
                portalViewModel.ImageNotAvailablePathUrl = existingPortal.ImageNotAvailablePathUrl;
                portalViewModel.ShopByPriceMax = existingPortal.ShopByPriceMax;

                portalViewModel.ShopByPriceMin = existingPortal.ShopByPriceMin;
                portalViewModel.ShopByPriceIncrement = existingPortal.ShopByPriceIncrement;
            }
        }

        /// <summary>
        /// Sets default view model to the portal to be created.
        /// </summary>
        /// <param name="portalViewModel">portalViewModel</param>
        private void SetDefaultValues(PortalViewModel portalViewModel)
        {
            portalViewModel.IsActive = true;
            portalViewModel.FedExAccountNumber = string.Empty;
            portalViewModel.FedExAddInsurance = false;

            portalViewModel.FedExProductionKey = string.Empty;
            portalViewModel.FedExSecurityCode = string.Empty;
            //portalViewModel.InclusiveTax = false;

            portalViewModel.SmtpPassword = string.Empty;
            portalViewModel.SmtpServer = string.Empty;
            portalViewModel.SmtpUsername = string.Empty;

            portalViewModel.SiteWideAnalyticsJavascript = string.Empty;
            portalViewModel.SiteWideBottomJavascript = string.Empty;
            portalViewModel.SiteWideTopJavascript = string.Empty;

            portalViewModel.OrderReceiptAffiliateJavascript = string.Empty;
            portalViewModel.GoogleAnalyticsCode = string.Empty;
            portalViewModel.UpsKey = string.Empty;

            portalViewModel.UpsPassword = string.Empty;
            portalViewModel.UpsUsername = string.Empty;
            portalViewModel.MobileTheme = MvcAdminConstants.DefaultMobileTheme;
        }

        /// <summary>
        /// Sets logo path for the portal.
        /// </summary>
        /// <param name="fileName">File name provided by the user.</param>
        /// <returns>Logo path of the Portal.</returns>
        private string GetLogoPath(string fileName, EnvironmentConfigModel environmentConfigModel)
        {
            // Set logo path
            string filePath = string.Empty;
            // Check for Product Image            
            filePath = environmentConfigModel.OriginalImagePath + System.IO.Path.GetFileNameWithoutExtension(fileName) + "_" + DateTime.Now.ToString(DateFormat) + System.IO.Path.GetExtension(fileName);
            return filePath;
        }

        /// <summary>
        /// Sets Default Content Pages for the portal.
        /// </summary>
        /// <param name="portalViewModel">portalViewModel</param>
        /// <param name="portalCatalogModel">portalCatalogModel</param>
        private void SetDefaultContentPage(PortalViewModel portalViewModel, PortalCatalogModel portalCatalogModel)
        {
            // Get Exisitng Portal Settings By portalID
            PortalViewModel existingPortal = CurrentPortal;

            if (!Equals(existingPortal, null))
            {
                ContentPageListModel contentPageListModel = new ContentPageListModel();
                FilterCollection contentPageFilters = new FilterCollection();
                contentPageFilters.Add(FilterKeys.PortalId, FilterOperators.Equals, existingPortal.PortalId.ToString());
                contentPageFilters.Add(FilterKeys.LocaleId, FilterOperators.Equals, existingPortal.LocaleId.ToString());
                _contentPageClient.RefreshCache = true;
                contentPageListModel = _contentPageClient.GetContentPages(Expands, contentPageFilters, new SortCollection());

                ContentPageModel contentPageModel = new ContentPageModel();
                foreach (ContentPageModel contentPage in contentPageListModel.ContentPages)
                {
                    contentPageModel = _contentPageClient.CopyContentPage(contentPage) as ContentPageModel;
                    contentPageModel.ContentPageID = -1;
                    contentPageModel.PortalID = portalViewModel.PortalId;
                    contentPageModel.LocaleId = portalCatalogModel.LocaleId;
                    contentPageModel.SEOURL = null;
                    contentPageModel.ThemeID = portalCatalogModel.ThemeId;
                    contentPageModel.Html = string.Empty;
                    contentPageModel.UpdatedUser = HttpContext.Current.User.Identity.Name;
                    contentPageModel.IsUrlRedirectEnabled = false;
                    contentPageModel.MappedSeoUrl = null;
                    _contentPageClient.AddPage(contentPageModel);
                }
            }
        }

        /// <summary>
        /// Creates entry for portal catalog mapping.
        /// </summary>
        /// <param name="portalViewModel"></param>
        /// <param name="localeId"></param>
        /// <returns></returns>
        private PortalCatalogModel CreatePortalCatalog(PortalViewModel portalViewModel, int localeId)
        {
            PortalCatalogModel portalCatalogModel = new PortalCatalogModel();
            // Add the new Selection
            portalCatalogModel.PortalId = portalViewModel.PortalId;
            portalCatalogModel.CatalogId = portalViewModel.CatalogId ?? 0;
            portalCatalogModel.ThemeId = portalViewModel.ThemeId;
            portalCatalogModel.CssId = portalViewModel.CSSId;
            portalCatalogModel.LocaleId = localeId;
            PortalCatalogModel portalcatalog = _portalCatalogsClient.CreatePortalCatalog(portalCatalogModel);
            return portalCatalogModel;
        }

        /// <summary>
        /// Deletes portal catalog mapping.
        /// </summary>
        /// <param name="portalViewModel"></param>
        private void DeleteExistingPortalCatalogMapping(PortalViewModel portalViewModel)
        {
            FilterCollection portalCatalogFilterCollection = new FilterCollection();
            portalCatalogFilterCollection.Add(FilterKeys.PortalId, FilterOperators.Equals, portalViewModel.PortalId.ToString());

            _portalCatalogsClient.RefreshCache = true;
            PortalCatalogListModel portalCatalogListModel = _portalCatalogsClient.GetPortalCatalogs(Expands, portalCatalogFilterCollection, new SortCollection());

            if (!Equals(portalCatalogListModel.PortalCatalogs, null))
            {
                foreach (PortalCatalogModel portalCatalog in portalCatalogListModel.PortalCatalogs)
                {
                    _portalCatalogsClient.DeletePortalCatalog(portalCatalog.PortalCatalogId);
                }
            }
        }

        /// <summary>
        /// Created portal country mapping.
        /// </summary>
        /// <param name="portalViewModel"></param>
        /// <param name="isPortalCreated"></param>
        /// <returns></returns>
        private bool CreatePortalCountry(PortalViewModel portalViewModel, bool isPortalCreated)
        {
            PortalCountryModel portalCountry = new PortalCountryModel();
            // Add PortalCountry List
            portalCountry.PortalId = portalViewModel.PortalId;
            portalCountry.IsBillingActive = true;
            portalCountry.IsShippingActive = true;
            portalCountry.CountryCode = MvcAdminConstants.DefaultCountry;
            isPortalCreated = (_portalCountriesClient.CreatePortalCountry(portalCountry).PortalCountryId > 0) ? true : false;
            return isPortalCreated;
        }

        /// <summary>
        /// To save image by Id in data folder and update image name in database table
        /// </summary>
        /// <param name="model">PortalViewModel model</param>
        private void SaveImage(PortalViewModel model)
        {
            if (!Equals(model.LogoPath, null))
            {
                //to save image in data folder
                ImageHelper imageHelper = new ImageHelper();
                string validExtension = Convert.ToString(ConfigurationManager.AppSettings[MvcAdminConstants.ImageExtension]);
                imageHelper.SaveImageToPhysicalPathByName(model.PortalId, null, validExtension, System.IO.Path.GetFileName(model.LogoPathImageName), model.LogoPath, "sadmin");
            }
        }

        private void SaveNoImagePath(PortalViewModel model)
        {
            if (!Equals(model.ImageNotAvailablePath, null))
            {
                //to save image in data folder
                ImageHelper imageHelper = new ImageHelper();
                string validExtension = Convert.ToString(ConfigurationManager.AppSettings[MvcAdminConstants.ImageExtension]);
                imageHelper.SaveImageToPhysicalPathByName(model.PortalId, null, validExtension, System.IO.Path.GetFileName(model.ImageNotAvailablePathUrl), model.ImageNotAvailablePath, "sadmin");
            }
        }

        /// <summary>
        /// Set Display properties for currency.
        /// </summary>
        private void SetCurrencyDetails(PortalViewModel portalViewModel)
        {
            CurrencyTypeListModel currencyList = new CurrencyTypeListModel();
            _currencyTypeClient.RefreshCache = true;
            currencyList = _currencyTypeClient.GetCurrencyTypes(Expands, Filters, new SortCollection());


            portalViewModel.CurrencyTypes = PortalViewModelMap.ToSelectListItems(currencyList.Currency);

            if (!Equals(currencyList.Currency, null) && currencyList.Currency.Count > 0)
            {
                string currencySymbol = portalViewModel.CurrencyName;

                CultureInfo info = new CultureInfo(currencySymbol);

                decimal price = 100.12M;

                portalViewModel.CurrencyPreview = price.ToString("c", info.NumberFormat);

                if (portalViewModel.CurrencySuffix.Trim().Length > 0)
                {

                    portalViewModel.CurrencyPreview = string.Concat(portalViewModel.CurrencyPreview, " (", portalViewModel.CurrencySuffix, ")");
                }
            }
        }

        /// <summary>
        /// Gets the locale ID.
        /// </summary>
        /// <param name="portalViewModel">Portal view model.</param>
        /// <param name="localeId">Locale Id to be returned.</param>
        /// <returns>Locale Id.</returns>
        private int GetLocale(PortalViewModel portalViewModel, int localeId)
        {
            return localeId = portalViewModel.LocaleId.Equals(0) ? Helpers.MvcAdminConstants.LocaleId : portalViewModel.LocaleId;
        }


        /// <summary>
        /// Sets portal data like catalog, content pages and messages.
        /// </summary>
        /// <param name="portalViewModel">Porte model for which data will be set.</param>
        /// <param name="isPortalCreated">Bool value to check if the store is created or not.</param>
        /// <param name="localeId">Locale Id of the store.</param>
        /// <returns>Bool value showing the data is updated in the portal or not.</returns>
        private bool SetPortalData(PortalViewModel portalViewModel, bool isPortalCreated, int localeId)
        {
            isPortalCreated = this.CreatePortalCountry(portalViewModel, isPortalCreated);

            this.DeleteExistingPortalCatalogMapping(portalViewModel);

            PortalCatalogModel portalCatalogModel = this.CreatePortalCatalog(portalViewModel, portalViewModel.LocaleId);

            this.SetDefaultContentPage(portalViewModel, portalCatalogModel);

            _portalsClient.CreateMessage(portalViewModel.PortalId, localeId);
            return isPortalCreated;
        }

        #region Store Update
        /// <summary>
        /// Updates portal's general information.
        /// </summary>
        /// <param name="portalViewModel">Portal model to be updated.</param>
        /// <returns>Updated portal model.</returns>
        private PortalViewModel UpdatePortalGeneralInformation(PortalViewModel portalViewModel)
        {
            if (!Equals(portalViewModel.LogoPath, null))
            {
                portalViewModel.LogoPathImageName = portalViewModel.LogoPath.FileName;
            }
            PortalViewModel model = this.GetPortalInformationByPortalId(portalViewModel.PortalId);
            model.StoreName = portalViewModel.StoreName;
            model.CompanyName = portalViewModel.CompanyName;
            model.UseSsl = portalViewModel.UseSsl;
            model.AdminEmail = portalViewModel.AdminEmail;
            model.SalesEmail = portalViewModel.SalesEmail;
            model.CustomerServiceEmail = portalViewModel.CustomerServiceEmail;
            model.SalesPhoneNumber = portalViewModel.SalesPhoneNumber;
            model.CustomerServicePhoneNumber = portalViewModel.CustomerServicePhoneNumber;
            model.DefaultReviewStatus = portalViewModel.DefaultReviewStatus;

            model.DefaultOrderStateId = portalViewModel.RequiresManualApproval ? PendingApprovalId : portalViewModel.DefaultOrderStateId;

            model.InclusiveTax = portalViewModel.InclusiveTax;
            model.PersistentCartEnabled = portalViewModel.PersistentCartEnabled;
            model.EnableAddressValidation = portalViewModel.EnableAddressValidation;
            model.RequireValidatedAddress = portalViewModel.RequireValidatedAddress;
            model.EnableCustomerPricing = portalViewModel.EnableCustomerPricing;
            model.DefaultProductReviewStateId = portalViewModel.DefaultProductReviewStateId;
            model.IsEnableCompare = portalViewModel.IsEnableCompare;
            model.IsMultipleCouponCodeAllowed = portalViewModel.IsMultipleCouponCodeAllowed;
            model.CompareType = portalViewModel.IsEnableCompare ? portalViewModel.CompareType : string.Empty;
            model.IsEnableSinglePageCheckout = Equals(portalViewModel.IsEnableSinglePageCheckout, null) ? false : portalViewModel.IsEnableSinglePageCheckout.Value;

            EnvironmentConfigModel environmentConfig = GetEnvironmentConfigVariables(portalViewModel);

            if (!Equals(portalViewModel.LogoPath, null))
            {
                if (Equals(portalViewModel.LogoNewImage, MvcAdminConstants.NewImage))
                {
                    portalViewModel.LogoPathImageName = GetLogoPath(portalViewModel.LogoPathImageName, environmentConfig);
                    model.LogoPathImageName = environmentConfig.OriginalImagePath + "Turnkey/" + model.PortalId + "/" + System.IO.Path.GetFileName(portalViewModel.LogoPathImageName);
                }
            }

            PortalModel portalModel = _portalsClient.UpdatePortal(model.PortalId, PortalViewModelMap.ToModel(model));
            if (!Equals(portalModel, null) && portalModel.PortalId > 0)
            {
                SaveImage(portalViewModel);
                if (portalViewModel.PortalId.Equals(CurrentPortal.PortalId))
                {
                    HttpContext.Current.Cache.Remove(HttpContext.Current.Request.Url.Authority);
                }
                return PortalViewModelMap.ToViewModel(portalModel);
            }
            return null;
        }

        /// <summary>
        /// Updates portal display information.
        /// </summary>
        /// <param name="portalViewModel">Portal model to be updated.</param>
        /// <returns>Updated portal model.</returns>
        private PortalViewModel UpdatePortalDisplay(PortalViewModel portalViewModel)
        {
            if (!Equals(portalViewModel.ImageNotAvailablePath, null))
            {
                EnvironmentConfigModel enviromentConfig = GetEnvironmentConfigVariables(portalViewModel);
                portalViewModel.ImageNotAvailablePathUrl = enviromentConfig.OriginalImagePath + "Turnkey/" + portalViewModel.PortalId + "/" + portalViewModel.ImageNotAvailablePath.FileName;
            }
            PortalViewModel model = this.GetPortalInformationByPortalId(portalViewModel.PortalId);
            model.MaxCatalogDisplayColumns = portalViewModel.MaxCatalogDisplayColumns;
            model.MaxCatalogCategoryDisplayThumbnails = portalViewModel.MaxCatalogCategoryDisplayThumbnails;
            model.UseDynamicDisplayOrder = portalViewModel.UseDynamicDisplayOrder;
            model.MaxCatalogItemLargeWidth = portalViewModel.MaxCatalogItemLargeWidth;
            model.MaxCatalogItemMediumWidth = portalViewModel.MaxCatalogItemMediumWidth;
            model.MaxCatalogItemSmallWidth = portalViewModel.MaxCatalogItemSmallWidth;
            model.MaxCatalogItemCrossSellWidth = portalViewModel.MaxCatalogItemCrossSellWidth;
            model.MaxCatalogItemThumbnailWidth = portalViewModel.MaxCatalogItemThumbnailWidth;
            model.MaxCatalogItemSmallThumbnailWidth = portalViewModel.MaxCatalogItemSmallThumbnailWidth;
            if (Equals(portalViewModel.NoImageNewImage, MvcAdminConstants.NewImage))
            {
                model.ImageNotAvailablePathUrl = portalViewModel.ImageNotAvailablePathUrl;
            }
            var portalModel = _portalsClient.UpdatePortal(portalViewModel.PortalId, PortalViewModelMap.ToModel(model));
            if (!Equals(portalModel, null) && portalModel.PortalId > 0)
            {
                this.SaveNoImagePath(portalViewModel);
                if (portalViewModel.PortalId.Equals(CurrentPortal.PortalId))
                {
                    HttpContext.Current.Cache.Remove(HttpContext.Current.Request.Url.Authority);
                }
                return PortalViewModelMap.ToViewModel(portalModel);
            }
            return null;
        }

        /// <summary>
        /// Updates portal's SMTP data.
        /// </summary>
        /// <param name="portalViewModel">Portal model to be updated.</param>
        /// <returns>Updated portal model.</returns>
        private PortalViewModel UpdatePortalSmtp(PortalViewModel portalViewModel)
        {
            if (portalViewModel.PortalId.Equals(CurrentPortal.PortalId))
            {
                HttpContext.Current.Cache.Remove(HttpContext.Current.Request.Url.Authority);
            }
            PortalViewModel model = this.GetPortalInformationByPortalId(portalViewModel.PortalId);
            model.SmtpPort = portalViewModel.SmtpPort;
            model.SmtpServer = portalViewModel.SmtpServer;
            model.SmtpUsername = portalViewModel.SmtpUsername;
            model.SmtpPassword = portalViewModel.SmtpPassword;
            model.EnableSslForSmtp = portalViewModel.EnableSslForSmtp;

            var portalModel = _portalsClient.UpdatePortal(portalViewModel.PortalId, PortalViewModelMap.ToModel(model));
            if (!Equals(portalModel, null) && portalModel.PortalId > 0)
            {
                if (portalViewModel.PortalId.Equals(CurrentPortal.PortalId))
                {
                    HttpContext.Current.Cache.Remove(HttpContext.Current.Request.Url.Authority);
                }
                return PortalViewModelMap.ToViewModel(portalModel);
            }
            return null;
        }

        /// <summary>
        /// Updates Portal's javascript data.
        /// </summary>
        /// <param name="portalViewModel">Portal model to be updated.</param>
        /// <returns>Updated portal model.</returns>
        private PortalViewModel UpdatePortalJavascript(PortalViewModel portalViewModel)
        {
            if (portalViewModel.PortalId.Equals(CurrentPortal.PortalId))
            {
                HttpContext.Current.Cache.Remove(HttpContext.Current.Request.Url.Authority);
            }
            PortalViewModel model = this.GetPortalInformationByPortalId(portalViewModel.PortalId);
            model.SiteWideTopJavascript = portalViewModel.SiteWideTopJavascript;
            model.SiteWideBottomJavascript = portalViewModel.SiteWideBottomJavascript;
            model.SiteWideAnalyticsJavascript = portalViewModel.SiteWideAnalyticsJavascript;
            model.OrderReceiptAffiliateJavascript = portalViewModel.OrderReceiptAffiliateJavascript;

            var portalModel = _portalsClient.UpdatePortal(portalViewModel.PortalId, PortalViewModelMap.ToModel(model));
            if (!Equals(portalModel, null) && portalModel.PortalId > 0)
            {
                if (portalViewModel.PortalId.Equals(CurrentPortal.PortalId))
                {
                    HttpContext.Current.Cache.Remove(HttpContext.Current.Request.Url.Authority);
                }
                return PortalViewModelMap.ToViewModel(portalModel);
            }
            return null;
        }

        /// <summary>
        /// Updates portal shipping data.
        /// </summary>
        /// <param name="portalViewModel">Portal model to be updated.</param>
        /// <returns>Updated portal model.</returns>
        private PortalViewModel UpdatePortalShipping(PortalViewModel portalViewModel)
        {
            if (portalViewModel.PortalId.Equals(CurrentPortal.PortalId))
            {
                HttpContext.Current.Cache.Remove(HttpContext.Current.Request.Url.Authority);
            }
            PortalViewModel model = this.GetPortalInformationByPortalId(portalViewModel.PortalId);
            model.ShippingOriginAddress1 = portalViewModel.ShippingOriginAddress1;
            model.ShippingOriginAddress2 = portalViewModel.ShippingOriginAddress2;
            model.ShippingOriginCity = portalViewModel.ShippingOriginCity;
            model.ShippingOriginStateCode = portalViewModel.ShippingOriginStateCode;
            model.ShippingOriginZipCode = portalViewModel.ShippingOriginZipCode;
            model.ShippingOriginCountryCode = portalViewModel.ShippingOriginCountryCode;
            model.ShippingOriginPhoneNumber = portalViewModel.ShippingOriginPhoneNumber;
            model.FedExAccountNumber = portalViewModel.FedExAccountNumber;
            model.FedExMeterNumber = portalViewModel.FedExMeterNumber;
            model.FedExProductionKey = portalViewModel.FedExProductionKey;
            model.FedExSecurityCode = portalViewModel.FedExSecurityCode;
            model.FedExDropoffType = portalViewModel.FedExDropoffType;
            model.FedExPackagingType = portalViewModel.FedExPackagingType;
            model.FedExUseDiscountRate = portalViewModel.FedExUseDiscountRate;
            model.FedExAddInsurance = portalViewModel.FedExAddInsurance;
            model.UpsUsername = portalViewModel.UpsUsername;
            model.UpsPassword = portalViewModel.UpsPassword;
            model.UpsKey = portalViewModel.UpsKey;

            var portalModel = _portalsClient.UpdatePortal(portalViewModel.PortalId, PortalViewModelMap.ToModel(model));
            if (!Equals(portalModel, null) && portalModel.PortalId > 0)
            {
                if (portalViewModel.PortalId.Equals(CurrentPortal.PortalId))
                {
                    HttpContext.Current.Cache.Remove(HttpContext.Current.Request.Url.Authority);
                }
                return PortalViewModelMap.ToViewModel(portalModel);
            }
            return null;
        }

        /// <summary>
        /// Updates portal's Unit's information data.
        /// </summary>
        /// <param name="portalViewModel">Portal model to be updated.</param>
        /// <returns>Updated portal model.</returns>
        private PortalViewModel UpdatePortalUnits(PortalViewModel portalViewModel)
        {
            if (portalViewModel.PortalId.Equals(CurrentPortal.PortalId))
            {
                HttpContext.Current.Cache.Remove(HttpContext.Current.Request.Url.Authority);
                HttpContext.Current.Cache.Remove(HttpContext.Current.Request.Url.Authority + MvcAdminConstants.CurrencyUnits);
            }
            PortalViewModel model = this.GetPortalInformationByPortalId(portalViewModel.PortalId);
            model.WeightUnit = portalViewModel.WeightUnit;
            model.DimensionUnit = portalViewModel.DimensionUnit;
            model.CurrencyTypeId = portalViewModel.CurrencyTypeId;
            model.CurrencySuffix = portalViewModel.CurrencySuffix;

            CurrencyTypeModel currency = new CurrencyTypeModel();
            currency = _currencyTypeClient.GetCurrencyType(portalViewModel.CurrencyTypeId.GetValueOrDefault());


            if (!Equals(currency.CurrencySuffix, portalViewModel.CurrencySuffix))
            {
                currency.CurrencySuffix = portalViewModel.CurrencySuffix;
                currency = _currencyTypeClient.UpdateCurrencyType(currency.CurrencyTypeID, currency);
            }

            var portalModel = _portalsClient.UpdatePortal(portalViewModel.PortalId, PortalViewModelMap.ToModel(model));
            if (!Equals(portalModel, null) && portalModel.PortalId > 0)
            {
                if (portalViewModel.PortalId.Equals(CurrentPortal.PortalId))
                {
                    HttpContext.Current.Cache.Remove(HttpContext.Current.Request.Url.Authority);
                }
                return PortalViewModelMap.ToViewModel(portalModel);
            }
            return null;
        }

        /// <summary>
        /// Updates pprtal's profile associated with it.
        /// </summary>
        /// <param name="portalViewModel">Portal model to be updated.</param>
        /// <returns>Updated portal model.</returns>
        private PortalViewModel UpdatePortalProfileInformation(PortalViewModel portalViewModel)
        {
            if (portalViewModel.PortalId.Equals(CurrentPortal.PortalId))
            {
                HttpContext.Current.Cache.Remove(HttpContext.Current.Request.Url.Authority);
            }
            PortalViewModel model = this.GetPortalInformationByPortalId(portalViewModel.PortalId);
            if (portalViewModel.IsDefaultAnonymousProfileId)
            {
                model.DefaultAnonymousProfileId = portalViewModel.PortalProfileViewModel.ProfileID;
            }
            if (portalViewModel.IsDefaultRegisteredProfileId)
            {
                model.DefaultRegisteredProfileId = portalViewModel.PortalProfileViewModel.ProfileID;
            }
            var portalModel = _portalsClient.UpdatePortal(portalViewModel.PortalId, PortalViewModelMap.ToModel(model));
            if (!Equals(portalModel, null) && portalModel.PortalId > 0)
            {
                if (portalViewModel.PortalId.Equals(CurrentPortal.PortalId))
                {
                    HttpContext.Current.Cache.Remove(HttpContext.Current.Request.Url.Authority);
                }
                return PortalViewModelMap.ToViewModel(portalModel);
            }
            return null;
        }
        #endregion

        /// <summary>
        /// Gets enviroment config variables.
        /// </summary>
        /// <param name="portalViewModel">View model of the portal</param>
        /// <returns>Environment cofig model.</returns>
        private EnvironmentConfigModel GetEnvironmentConfigVariables(PortalViewModel portalViewModel)
        {
            EnvironmentConfigModel environmentConfig;
            if ((!Equals(portalViewModel.UserType, null)) && (Equals(portalViewModel.UserType, UserTypes.FranchiseAdmin.ToString())))
            {
                environmentConfig = _environmentConfigClient.GetEnvironmentConfig();
            }
            else
            {
                environmentConfig = GetFromSession<EnvironmentConfigModel>(MvcAdminConstants.EnvironmentConfigKey);
            }
            return environmentConfig;
        }
        #endregion

        #region Manage SEO

        public bool UpdatePortalDefaultSettings(ManageSEOViewModel viewModel)
        {
            PortalViewModel model = this.GetPortalInformationByPortalId(viewModel.PortalId);

            model.SeoDefaultContentKeyword = viewModel.SeoDefaultContentKeyword;
            model.SeoDefaultProductTitle = Equals(viewModel.SeoDefaultProductTitle, defaultName) ? model.StoreName : viewModel.SeoDefaultProductTitle;
            model.SeoDefaultProductDescription = Equals(viewModel.SeoDefaultProductDescription, defaultName) ? model.StoreName : viewModel.SeoDefaultProductDescription;
            model.SeoDefaultProductKeyword = viewModel.SeoDefaultProductKeyword;
            model.SeoDefaultCategoryTitle = Equals(viewModel.SeoDefaultCategoryTitle, defaultName) ? model.StoreName : viewModel.SeoDefaultCategoryTitle;
            model.SeoDefaultCategoryDescription = Equals(viewModel.SeoDefaultCategoryDescription, defaultName) ? model.StoreName : viewModel.SeoDefaultCategoryDescription;
            model.SeoDefaultCategoryKeyword = viewModel.SeoDefaultCategoryKeyword;
            model.SeoDefaultContentTitle = Equals(viewModel.SeoDefaultContentTitle, defaultName) ? model.StoreName : viewModel.SeoDefaultContentTitle;
            model.SeoDefaultContentDescription = Equals(viewModel.SeoDefaultContentDescription, defaultName) ? model.StoreName : viewModel.SeoDefaultContentDescription;

            var portal = _portalsClient.UpdatePortal(viewModel.PortalId, PortalViewModelMap.ToModel(model));
            return portal.PortalId > 0 ? true : false;
        }

        #endregion
    }
}



