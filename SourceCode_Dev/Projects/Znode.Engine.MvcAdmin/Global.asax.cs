﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.Controllers.Home;
using Znode.Engine.MvcAdmin.Helpers.Trim;

namespace Znode.Engine.MvcAdmin
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            ModelBinders.Binders.DefaultBinder = new TrimModelBinder();
        }

        protected void Application_BeginRequest()
        {
            //PRFT Custom Code for allowing TLS12 with the new FedEx updates
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.UtcNow.AddHours(-1));
            Response.Cache.SetNoStore();

            HttpContextBase currentContext = new HttpContextWrapper(HttpContext.Current);
            UrlHelper urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);

            if (!Equals(currentContext, null) && !Equals(urlHelper, null))
            {
                RouteData routeData = urlHelper.RouteCollection.GetRouteData(currentContext);
                string action = Equals(routeData, null) ? string.Empty : routeData.Values["action"] as string;

                if (!string.IsNullOrEmpty(action) && !action.Equals("login", StringComparison.CurrentCultureIgnoreCase) && PortalAgent.CurrentPortal.UseSsl && !HttpContext.Current.Request.IsSecureConnection)
                {
                    Response.Redirect("https://" + Request.ServerVariables["HTTP_HOST"] + HttpContext.Current.Request.RawUrl);
                }
            }
        }


        protected void Application_Error(Object sender, EventArgs e)
        {
            Exception exception = null;
            try
            {
                exception = Server.GetLastError().GetBaseException();
                LogMessage(exception);
                RedirectToErrorPage(sender, exception);
            }
            catch (Exception ex)
            {
                LogMessage(ex);
                RedirectToErrorPage(sender, ex);
            }
        }

        private void LogMessage(Exception exception)
        {
            if (exception != null)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(exception);
            }
        }

        private void RedirectToErrorPage(Object sender, Exception exception)
        {
            var controller = new HomeController();
            var httpContext = ((MvcApplication)sender).Context;

            var routeData = new RouteData();
            httpContext.ClearError();
            httpContext.Response.Clear();

            SetErrorPage(exception, routeData);

            ((IController)controller).Execute(new RequestContext(new HttpContextWrapper(httpContext), routeData));
        }

        private void SetErrorPage(Exception exception, RouteData routeData)
        {
            routeData.Values.Add("controller", "home");
            routeData.Values.Add("action", "ErrorHandler");
            routeData.Values.Add("exception", exception);
        }
    }
}
