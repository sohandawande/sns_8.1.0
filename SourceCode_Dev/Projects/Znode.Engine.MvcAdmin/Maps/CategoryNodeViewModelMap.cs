﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    public class CategoryNodeViewModelMap
    {
        #region Public Methods

        /// <summary>
        ///  Maps the CategoryNodeModel type of object to CategoryNodeViewModel.
        /// </summary>
        /// <param name="model">Model of type CategoryNodeModel</param>
        /// <returns>Returns the mapped CategoryNodeModel to CategoryNodeViewModel.</returns>
        public static CategoryNodeViewModel ToViewModel(CategoryNodeModel model)
        {
            if (Equals(model, null))
            {
                return null;
            }

            return new CategoryNodeViewModel()
            {
                Catalog = CatalogViewModelMap.ToViewModel(model.Catalog),
                CatalogId = model.CatalogId,
                Category = CategoryViewModelMap.ToViewModel(model.Category),
                CategoryId = model.CategoryId,
                CategoryNodeId = model.CategoryNodeId,
                BeginDate = model.BeginDate,
                CssId = model.CssId,
                DisplayOrder = model.DisplayOrder,
                EndDate = model.EndDate,
                IsActive = model.IsActive,
                MasterPageId = !Equals(model.MasterPageId, null) ? model.MasterPageId : null,
                ParentCategoryNodeId = !Equals(model.ParentCategoryNodeId, null) ? model.ParentCategoryNodeId : null,
                ThemeId = !Equals(model.ThemeId, null) ? model.ThemeId : null,
                CatalogName = (!Equals(model.Catalog, null)) ? model.Catalog.Name : string.Empty,
                ErrorMessage = (!Equals(model.ErrorMessage, null)) ? model.ErrorMessage : string.Empty

            };
        }

        /// <summary>
        /// Maps CategoryNodeViewModel type of object to CategoryNodeModel type.
        /// </summary>
        /// <param name="viewModel">The model of type CategoryNodeViewModel.</param>
        /// <returns>Returns the mapped model of type CategoryNodeModel.</returns>
        public static CategoryNodeModel ToModel(CategoryNodeViewModel viewModel)
        {
            if (!Equals(viewModel, null))
            {
                CategoryNodeModel model = new CategoryNodeModel();
                model.Catalog = CatalogViewModelMap.ToModel(viewModel.Catalog);
                model.CatalogId = viewModel.CatalogId;
                model.Category = CategoryViewModelMap.ToModel(viewModel.Category);
                model.CategoryId = viewModel.CategoryId;
                model.CategoryNodeId = viewModel.CategoryNodeId;
                model.BeginDate = viewModel.BeginDate;
                model.ParentCategoryNodeId = !Equals(viewModel.ParentCategoryNodeId, 0) ? viewModel.ParentCategoryNodeId : null;
                model.CssId = !Equals(viewModel.CssId, 0) ? viewModel.CssId : null;
                model.ThemeId = !Equals(viewModel.ThemeId, 0) ? viewModel.ThemeId : null;
                model.MasterPageId = !Equals(viewModel.MasterPageId, 0) ? viewModel.MasterPageId : null;
                model.DisplayOrder = viewModel.DisplayOrder;
                model.EndDate = viewModel.EndDate;
                model.IsActive = viewModel.IsActive;
                return model;
            }
            return null;
        }

        #endregion

    }
}