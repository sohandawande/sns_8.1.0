﻿using Resources;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Web.Mvc;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    /// <summary>
    /// Case Request View Model Map
    /// </summary>
    public class CaseRequestViewModelMap
    {
        /// <summary>
        /// View Model Mapper for CaseRequest
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static CaseRequestViewModel ToViewModel(CaseRequestModel model)
        {
            if (!Equals(model, null))
            {
                return new CaseRequestViewModel()
                {
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    CompanyName = model.CompanyName,
                    EmailId = model.Email,
                    PhoneNumber = model.PhoneNumber,
                    Message = Equals(model.Description, null) ? string.Empty : model.Description,
                    CaseOrigin = model.CaseOrigin,
                    Title = model.Title,
                    Description = Equals(model.Description, null) ? string.Empty : model.Description,
                    CreateUser = model.CreateUser,
                    StoreName = Equals(model.StoreName, null) ? string.Empty : model.StoreName,
                    CaseStatusName = Equals(model.CaseStatusName, null) ? string.Empty : model.CaseStatusName,
                    CasePriorityName = Equals(model.CasePriorityName, null) ? string.Empty : model.CasePriorityName,
                    PortalId = model.PortalId,
                    CaseRequestId = model.CaseRequestId,
                    AccountId = model.AccountId,
                    OwnerAccountId = model.OwnerAccountId,
                    CaseStatusId = model.CaseStatusId,
                    CasePriorityId = model.CasePriorityId,
                    CaseTypeId = model.CaseTypeId,
                    CreatedDate = model.CreateDate,
                };
                
            }
            return new CaseRequestViewModel();
        }

        /// <summary>
        /// Model Mapper for CaseRequest
        /// </summary>
        /// <param name="viewModel">CaseRequestViewModel viewModel</param>
        /// <returns>CaseRequestModel</returns>
        public static CaseRequestModel ToModel(CaseRequestViewModel viewModel)
        {
            if (!Equals(viewModel, null))
            {
                CaseRequestModel model = new CaseRequestModel()
                {
                    FirstName = viewModel.FirstName,
                    LastName = viewModel.LastName,
                    CompanyName = viewModel.CompanyName,
                    Email = viewModel.EmailId,
                    PhoneNumber = viewModel.PhoneNumber,
                    CaseOrigin = viewModel.CaseOrigin,
                    Title = viewModel.Title,
                    Description = Equals(viewModel.Description, null) ? string.Empty : viewModel.Description,
                    CreateUser = viewModel.CreateUser,
                    StoreName = Equals(viewModel.StoreName, null) ? string.Empty : viewModel.StoreName,
                    CaseStatusName = Equals(viewModel.CaseStatusName, null) ? string.Empty : viewModel.CaseStatusName,
                    CasePriorityName = Equals(viewModel.CasePriorityName, null) ? string.Empty : viewModel.CasePriorityName,
                    PortalId = viewModel.PortalId,
                    CaseRequestId = viewModel.CaseRequestId,
                    AccountId = viewModel.AccountId,
                    OwnerAccountId = viewModel.OwnerAccountId,
                    CaseStatusId = viewModel.CaseStatusId,
                    CasePriorityId = viewModel.CasePriorityId,
                    CaseTypeId = viewModel.CaseTypeId,
                    CaseOldStatusName = viewModel.CaseOldStatusName,
                    CaseNewStatusName = viewModel.CaseNewStatusName
                };
                model.CaseNote = new NoteModel();
                model.CaseNote.NoteTitle = ZnodeResources.ColumnStatusChanged;
                model.CaseNote.NoteBody = GenerateNoteBody(viewModel);

                return model;
            }
            return new CaseRequestModel();
        }

        /// <summary>
        /// Model Mapper for NoteModel
        /// </summary>
        /// <param name="viewModel">NotesViewModel viewModel</param>
        /// <returns>returns NoteModel</returns>
        public static NoteModel ToModel(NotesViewModel viewModel)
        {
            if (!Equals(viewModel, null))
            {
                return new NoteModel()
                {
                    NoteId = viewModel.NoteId,
                    CaseId = viewModel.CaseId,
                    AccountId = viewModel.AccountId,
                    NoteTitle = viewModel.NoteTitle,
                    NoteBody = viewModel.NoteBody,
                    CreateDte = DateTime.Now,
                    CreateUser = viewModel.CreateUser,

                };
            }
            return new NoteModel();
        }

        /// <summary>
        /// Model Mapper for SendMailModel
        /// </summary>
        /// <param name="viewModel">CaseRequestViewModel viewModel</param>
        /// <returns>returns SendMailModel</returns>
        public static SendMailModel ToEmailModel(CaseRequestViewModel viewModel)
        {
            if (!Equals(viewModel, null))
            {

                SendMailModel model = new SendMailModel()
                {
                    EmailId = viewModel.EmailId,
                    Subject = viewModel.EmailSubject,
                    Body = viewModel.EmailMessage,
                };

                if (!Equals(viewModel.Attachments, null))
                {
                    string result = new StreamReader(viewModel.Attachments.InputStream).ReadToEnd();
                    ContentType mimeType = new System.Net.Mime.ContentType("application/octet-stream");
                    model.Attachment = AlternateView.CreateAlternateViewFromString(result, mimeType);
                }
                return model;
            }
            return new SendMailModel();
        }
        /// <summary>
        /// List View Model Mapper for CaseRequest
        /// </summary>
        /// <param name="model">CaseRequestListModel model</param>
        /// <returns></returns>
        public static CaseRequestListViewModel ToListViewModel(CaseRequestListModel model)
        {
            if (!Equals(model, null) && !Equals(model.CaseRequests, null))
            {
                var viewModel = new CaseRequestListViewModel()
                {
                    CaseRequestsList = model.CaseRequests.ToList().Select(
                    x => new CaseRequestViewModel()
                    {
                        CaseRequestId = x.CaseRequestId,
                        Title = x.Title,
                        Description = x.Description,
                        StoreName = Equals(x.StoreName, null) ? string.Empty : x.StoreName,
                        CaseStatusName = Equals(x.CaseStatusName, null) ? string.Empty : x.CaseStatusName,
                        CasePriorityName = Equals(x.CasePriorityName, null) ? string.Empty : x.CasePriorityName,
                        CreatedDate = x.CreateDate,
                        FormatedCreatedDate = Equals(x.CreateDate, null) ? string.Empty : HelperMethods.ViewDateFormat(x.CreateDate),
                        PortalId = x.PortalId,
                        CaseStatusId = x.CaseStatusId,
                        CasePriorityId = x.CasePriorityId
                    }).ToList()
                };

                viewModel.TotalResults = Convert.ToInt32(model.TotalResults);
                viewModel.Page = Convert.ToInt32(model.PageIndex);
                viewModel.RecordPerPage = Convert.ToInt32(model.PageSize);
                viewModel.TotalPages = Convert.ToInt32(model.TotalPages);
                viewModel.TotalResults = Convert.ToInt32(model.TotalResults);

                return viewModel;
            }
            return new CaseRequestListViewModel();
        }

        /// <summary>
        /// List View Model Mapper for CaseNotes
        /// </summary>
        /// <param name="model">NoteListModel model</param>
        /// <returns>returns NotesListViewModel</returns>
        public static NotesListViewModel ToNoteListViewModel(NoteListModel model)
        {
            if (!Equals(model, null) && !Equals(model.Notes, null))
            {
                var viewModel = new NotesListViewModel()
                {
                    Notes = model.Notes.ToList().Select(
                    x => new NotesViewModel()
                    {
                        NoteId = x.NoteId,
                        NoteTitle = Equals(x.NoteTitle, null) ? string.Empty : Equals(x.CreateUser, null) ? string.Empty : Equals(x.CreateUser, null) ? string.Empty : FormatCustomerNote(x.NoteTitle, x.CreateUser, x.CreateDte),
                        NoteBody = Equals(x.NoteBody, null) ? string.Empty : x.NoteBody,
                        CreateDate = x.CreateDte,
                        CreateUser = x.CreateUser,
                    }).ToList()
                };
                return viewModel;
            }
            return new NotesListViewModel();
        }

        /// <summary>
        /// To get list of portals
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static List<SelectListItem> ToListItems(IEnumerable<PortalViewModel> model)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                list = (from item in model
                        select new SelectListItem
                        {
                            Text = item.StoreName,
                            Value = item.PortalId.ToString(),
                        }).ToList();
            }
            return list;
        }

        /// <summary>
        /// To get list of CaseStatus
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static List<SelectListItem> ToListItems(IEnumerable<CaseStatusModel> model)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                list = (from item in model
                        select new SelectListItem
                        {
                            Text = item.Name,
                            Value = item.CaseStatusId.ToString(),
                        }).ToList();
            }
            return list;
        }

        /// <summary>
        /// To get list of CasePriority
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static List<SelectListItem> ToListItems(IEnumerable<CasePriorityModel> model)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                list = (from item in model
                        select new SelectListItem
                        {
                            Text = item.Name,
                            Value = item.CasePriorityId.ToString(),
                        }).ToList();
            }
            return list;
        }

        /// <summary>
        /// List View Model Mapper for CaseNotes
        /// </summary>
        /// <param name="model">NoteListModel model</param>
        /// <returns>returns NotesListViewModel</returns>
        public static NotesListViewModel ToCustomerNoteListViewModel(NoteListModel model)
        {
            if (!Equals(model, null) && !Equals(model.Notes, null))
            {
                var viewModel = new NotesListViewModel()
                {
                    Notes = model.Notes.ToList().Select(
                    x => new NotesViewModel()
                    {
                        NoteId = x.NoteId,
                        NoteTitle = Equals(x.NoteTitle, null) ? string.Empty : x.NoteTitle,
                        NoteBody = Equals(x.NoteBody, null) ? string.Empty : x.NoteBody,
                        CreateDate = x.CreateDte,
                        CreateUser = x.CreateUser,
                    }).ToList()
                };
                return viewModel;
            }
            return new NotesListViewModel();
        }

        #region Private Methods
        /// <summary>
        /// To generate note body
        /// </summary>
        /// <param name="model">CaseRequestViewModel model</param>
        /// <returns>returns formated NoteBody</returns>
        private static string GenerateNoteBody(CaseRequestViewModel model)
        {
            string separator = " ";
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append(ZnodeResources.ColumnStatusChanged);          
            stringBuilder.Append(separator);
            stringBuilder.Append(ZnodeResources.ColumnTo);
            stringBuilder.Append(separator);
            stringBuilder.Append(model.CaseOldStatusName);
            stringBuilder.Append(separator);
            stringBuilder.Append(ZnodeResources.ColumnTo);
            stringBuilder.Append(separator);
            stringBuilder.Append(model.CaseNewStatusName);      
            stringBuilder.Append(separator);
            return stringBuilder.ToString();
        }

        /// <summary>
        /// To Format Customer Note
        /// </summary>
        /// <param name="noteTitle">string noteTitle</param>
        /// <param name="createUser">string createUser</param>
        /// <param name="createDate">DateTime createDate</param>
        /// <returns>returns FormatCustomerNote</returns>
        private static string FormatCustomerNote(string noteTitle, string createUser, DateTime createDate)
        {
            string separator = " ";
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append(noteTitle);
            stringBuilder.Append(separator + ZnodeResources.Hyphen + separator);
            stringBuilder.Append(createUser);
            stringBuilder.Append(ZnodeResources.ColumnOn);
            stringBuilder.Append(createDate);
            return stringBuilder.ToString();
        }
        #endregion
    }
}