﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    /// <summary>
    /// Static Class for ManageMessage Mapper 
    /// </summary>
    public static class ManageMessageViewModelMap
    {
        /// <summary>
        /// Mapper to convert MessageConfigModel to ManageMessageViewModel  
        /// </summary>
        /// <param name="paymentOptionModel">to convert to view model</param>
        /// <returns>ManageMessageViewModel</returns>
        public static ManageMessageViewModel ToViewModel(MessageConfigModel messageConfigModel)
        {
            if (!Equals(messageConfigModel, null))
            {
                return new ManageMessageViewModel()
                {
                    MessageKey = messageConfigModel.Key,
                    Description = messageConfigModel.Description,
                    Value = messageConfigModel.Value,
                    MessageConfigId = messageConfigModel.MessageConfigId,
                    PortalId = messageConfigModel.PortalId,
                    LocaleId = messageConfigModel.LocaleId,
                    MessageTypeId = messageConfigModel.MessageTypeId,
                    PortalName = Equals(messageConfigModel.PortalName, null) ? String.Empty : messageConfigModel.PortalName,
                    CustomErrorMessage = messageConfigModel.CustomErrorMessage
                };
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Mapper to convert ManageMessageViewModel to MessageConfigModel   
        /// </summary>
        /// <param name="paymentOptionModel">to convert to model</param>
        /// <returns>MessageConfigModel</returns>
        public static MessageConfigModel ToModel(ManageMessageViewModel manageMessageViewModel)
        {
            if (!Equals(manageMessageViewModel, null))
            {
                return new MessageConfigModel()
                {
                    Key = manageMessageViewModel.MessageKey,
                    Description = manageMessageViewModel.Description,
                    Value = manageMessageViewModel.Value,
                    MessageConfigId = manageMessageViewModel.MessageConfigId,
                    PortalId = manageMessageViewModel.PortalId,
                    PortalName = manageMessageViewModel.PortalName,
                    LocaleId = manageMessageViewModel.LocaleId,
                    MessageTypeId = manageMessageViewModel.MessageTypeId,
                    CustomErrorMessage = manageMessageViewModel.CustomErrorMessage
                };
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Mapper to convert MessageConfigModel to ManageMessageListViewModel
        /// </summary>
        /// <param name="models">to convert to list view model</param>
        /// <returns>ManageMessageListViewModel</returns>
        public static ManageMessageListViewModel ToListViewModel(MessageConfigListModel models, int? pageIndex = null, int? recordPerPage = null)
        {           
            if (!Equals(models, null) && !Equals(models.MessageConfigs, null))
            {
                var viewModel = new ManageMessageListViewModel()
                {
                    ManageMessage = models.MessageConfigs.ToList().Select(
                    x => new ManageMessageViewModel()
                    {
                        MessageKey = x.Key,
                        Description = x.Description,
                        Value = x.Value,
                        MessageConfigId = x.MessageConfigId,
                        PortalId = x.PortalId,                       
                        PortalName = Equals(x.PortalName, null) ? String.Empty : x.PortalName,
                    }).ToList()
                };

                viewModel.RecordPerPage = recordPerPage ?? 10;
                viewModel.Page = pageIndex ?? 1;
                viewModel.TotalResults = Convert.ToInt32(models.TotalResults);

                return viewModel;
            }
            else
            {
                return new ManageMessageListViewModel();
            }
        }

        /// <summary>
        /// Mapper to convert IEnumerable<PortalModel> to List<SelectListItem>
        /// </summary>
        /// <param name="model">IEnumerable<PortalModel></param>
        /// <returns>List<SelectListItem></returns>
        public static List<SelectListItem> ToListItems(IEnumerable<PortalModel> model)
        {
            List<SelectListItem> portal = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                portal = (from item in model
                          select new SelectListItem
                          {
                              Text = item.StoreName,
                              Value = item.PortalId.ToString(),
                          }).ToList();
            }
            portal.Add(new SelectListItem { Text = "All Stores", Value = "0" });
            return portal;
        }
    }
}