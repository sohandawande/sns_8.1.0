﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Extensions;
using Znode.Engine.MvcAdmin.ViewModels;
using System.IO;
using System.Data;
using System.Text;
using System.Dynamic;
using System.Data.OleDb;
using Znode.Engine.Api.Client;
using Newtonsoft.Json;
using System.Web.Helpers;

namespace Znode.Engine.MvcAdmin.Maps
{
    public class ImportExportViewModelMap
    {

        private const string ExtensionCSV = ".csv";
        private const string ExtensionXLS = ".xls";
        private const string ExtensionXLSX = ".xlsx";
        /// <summary>
        /// To map IEnumerable<CatalogModel> model to List<SelectListItem>
        /// </summary>
        /// <param name="model">IEnumerable<CatalogModel></param>
        /// <returns>returns List of SelectListItem</returns>
        public static List<SelectListItem> ToListItems(IEnumerable<CatalogModel> model, string profileStoreAccess = "")
        {
            List<SelectListItem> lstItems = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                lstItems = (from item in model
                            select new SelectListItem
                            {
                                Text = item.Name,
                                Value = item.CatalogId.ToString(),
                            }).ToList();


            }
            return lstItems;
        }

        /// <summary>
        /// To Map the List Models to the List<SelectListItem>
        /// </summary>
        /// <param name="model">IEnumerable<BaseModel></param>
        /// <param name="labelValue">Key to Get the value</param>
        /// <param name="labelText">Keys to get the value</param>
        /// <param name="isIncludeDefaultItem">Include the default selection</param>
        /// <returns></returns>
        public static List<SelectListItem> GetSelectListItem(IEnumerable<BaseModel> model, string labelValue, string labelText, bool isIncludeDefaultItem = false)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            try
            {
                if (!Equals(model, null))
                {
                    items = model.Select(x => new SelectListItem
                    {
                        Value = x.GetProperty(labelValue).ToString(),
                        Text = x.GetProperty(labelText).ToString(),
                        Selected = false
                    }).ToList();

                    if (isIncludeDefaultItem)
                    {
                        items.Insert(0, new SelectListItem { Text = "All", Value = "0" });
                    }
                }
            }
            catch (Exception ex)
            {
                items = new List<SelectListItem>();
            }
            return items;
        }

        /// <summary>
        /// To Map Export Model to Export View Model.
        /// </summary>
        /// <param name="model">IEnumerable<BaseModel></param>
        /// <returns>Return Model of type ExportViewModel</returns>
        public static ExportViewModel ToViewModel(ExportModel model)
        {
            if (!Equals(model.RecordSet, null))
            {
                return new ExportViewModel()
                {
                    ExportDataSet = model.RecordSet,
                };
            }
            else
            {
                return new ExportViewModel();
            }
        }

        /// <summary>
        /// To Map Export View Model to Export Model.
        /// </summary>
        /// <param name="model">Model of type ExportViewModel<param>
        /// <returns>Return Model of type ExportModel</returns>
        public static ExportModel ToModel(ExportViewModel model)
        {
            return new ExportModel()
            {
                FileType = model.FileType,
                Type = model.Type,
                UserName = HttpContext.Current.User.Identity.Name,
                CatalogId = model.SelectedCatalogId,
                AttributeTypeId = model.SelectedAttributeTypeId,
                CategoryId = model.SelectedCategoryId,
                ProductId = model.ProductId,
                ProductName = model.ProductName,
                AttributeIds = model.ProductAttributes,
                StartDate = model.StartDate,
                EndDate = model.EndDate,
                CountryCode=model.SelectedCountryCode,
            };
        }

        /// <summary>
        /// To Map ImportViewModel & EnvironmentConfigModel to ImportModel
        /// </summary>
        /// <param name="model">Model of Type ImportViewModel</param>
        /// <param name="configModel">Modle of type EnvironmentConfigModel</param>
        /// <returns>Return Model of Type ImportModel</returns>
        public static ImportModel ToImportModel(ImportViewModel model, EnvironmentConfigModel configModel)
        {
            return new ImportModel()
            {
                Type = model.Type,
                UserName = HttpContext.Current.User.Identity.Name,
                ImportData = DataTableToList(model.ImportFile, configModel, model.Type),
            };
        }

        /// <summary>
        /// To Map Import Model to Import View Model.
        /// </summary>
        /// <param name="model">Model of type ImportModel</param>
        /// <returns>Return Model of type ImportViewModel</returns>
        public static ImportViewModel ToImportViewModel(ImportModel model)
        {
            if (!Equals(model, null))
            {
                return new ImportViewModel()
                {
                    ImportStatus = model.Status,
                    DataTableList = model.ErrorData,
                    WebGridColumn = MapListToViewModel(model.ErrorData),
                    ImportConfigData = ToImportConfigViewModel(model.ImportConfigData),
                };
            }
            else
            {
                return new ImportViewModel();
            }
        }

        /// <summary>
        /// To Convert File Stream to String
        /// </summary>
        /// <param name="fileStream">Stream of the File</param>
        /// <returns>Return the File stream string.</returns>
        private static string GetImportFile(Stream fileStream)
        {
            string dataStream = string.Empty;
            if (!Equals(fileStream, null))
            {
                StreamReader fileReader = new StreamReader(fileStream);
                dataStream = fileReader.ReadToEnd();
            }
            return dataStream;
        }


        /// <summary>
        /// Convert File Stream to DataTable
        /// </summary>
        /// <param name="fileData">Stream of the File</param>
        /// <returns>Return DataTable.</returns>
        private static DataTable ConvertCSVStreamToDataTable(Stream fileData)
        {
            StreamReader sr = new StreamReader(fileData);
            string dataStream = string.Empty;
            dataStream = sr.ReadLine();
            string[] headers = dataStream.Split('|');
            DataTable dt = new DataTable();
            foreach (string header in headers)
            {
                dt.Columns.Add(header);
            }
            while (!sr.EndOfStream)
            {
                string[] rows = sr.ReadLine().Split('|');
                DataRow dr = dt.NewRow();
                for (int i = 0; i < headers.Length; i++)
                {
                    dr[i] = rows[i];
                }
                dt.Rows.Add(dr);
            }
            return dt;
        }


        /// <summary>
        /// To Convert Strem to Datatable & Datatable to Dynamic List
        /// </summary>
        /// <param name="importFile">Uploaded File Info</param>
        /// <param name="configModel">Modle of the EnvironmentConfigModel</param>
        /// <param name="type">Uploaded Import File Type</param>
        /// <returns>Return the Response in List<dynamic> type.</returns>
        public static List<dynamic> DataTableToList(HttpPostedFileBase importFile, EnvironmentConfigModel configModel, string type)
        {
            DataTable dataTable = new DataTable();
            var result = new List<dynamic>();
            string path = string.Empty;
            try
            {
                string fileExtension = Path.GetExtension(importFile.FileName).ToLower();
                if (!Equals(fileExtension.ToLower(), ExtensionCSV))
                {
                    string fileName = GetUpdatedFileName(importFile.FileName);
                    path = Path.Combine(HttpContext.Current.Server.MapPath(configModel.DataPath), fileName);
                    importFile.SaveAs(path);
                    type = GetAllSheetNameFromExcel(path).FirstOrDefault();
                }

                switch (fileExtension.ToLower())
                {
                    case ExtensionXLSX:
                        dataTable = GetDataFromExcel(path, type);
                        break;
                    case ExtensionXLS:
                        dataTable = GetDataFromExcel(path, type);
                        break;
                    case ExtensionCSV:
                        dataTable = ConvertCSVStreamToDataTable(importFile.InputStream);
                        break;
                    default:
                        break;
                }


                if (!Equals(dataTable, null) && dataTable.Rows.Count > 0)
                {
                    foreach (DataRow row in dataTable.Rows)
                    {
                        var obj = (IDictionary<string, object>)new ExpandoObject();
                        foreach (DataColumn col in dataTable.Columns)
                        {
                            obj.Add(col.ColumnName, row[col.ColumnName]);
                        }
                        result.Add(obj);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                dataTable.Dispose();
                if (File.Exists(path))
                {
                    File.Delete(path);
                }
            }
            return result;
        }

        /// <summary>
        /// To Convert the Excel File to Datatable
        /// </summary>
        /// <param name="filePath">Path of the Excel File</param>
        /// <param name="sheetName">Name of the Excel file Sheet</param>
        /// <returns>Return the Datatable</returns>
        public static DataTable GetDataFromExcel(string filePath, string sheetName)
        {
            DataTable dataTable = new DataTable();
            DataSet dataSet = new DataSet();
            using (OleDbConnection oleDbConnection = new OleDbConnection(GetConnectionString(filePath)))
            {
                oleDbConnection.Open();
                try
                {
                    dataTable = GetExcelDataSchema(oleDbConnection);
                    if (dataTable != null && dataTable.Rows.Count > 0)
                    {
                        try
                        {
                            FillDataAdapter(oleDbConnection, string.Empty, sheetName, dataSet);
                        }
                        catch (Exception ex)
                        {
                            oleDbConnection.Close();
                            throw;
                        }
                    }
                    oleDbConnection.Close();
                }
                catch (Exception ex)
                { }
                finally { oleDbConnection.Close(); }
            }
            return (!Equals(dataSet, null) && !Equals(dataSet.Tables[0], null)) ? dataSet.Tables[0] : null;
        }

        /// <summary>
        /// To Get the Excel Connection String.
        /// </summary>
        /// <param name="filePath">Path of the Excel File</param>
        /// <returns>Return the Connection String</returns>
        private static string GetConnectionString(string filePath)
        {
            if (!Path.GetExtension(filePath).ToLower().Equals(ExtensionXLSX))
            {
                return "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filePath + ";Extended Properties='Excel 8.0;HDR=YES;IMEX=1'";
            }
            else
            {
                return "Provider = Microsoft.ACE.OLEDB.12.0;Data Source= " + filePath + ";Extended Properties='Excel 12.0;IMEX=1;HDR=YES'";
            }

        }

        /// <summary>
        /// To Get the Excel data Schema.
        /// </summary>
        /// <param name="oleDbConnection">Connection Of the Excel</param>
        /// <returns>Return the Datatable</returns>
        private static DataTable GetExcelDataSchema(OleDbConnection oleDbConnection)
        {
            return oleDbConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
        }

        /// <summary>
        /// To Bind the Data from Excel Schema to DataTable
        /// </summary>
        /// <param name="oleDbConnection">Connection Of the Excel</param>
        /// <param name="condition">Optional Parameter</param>
        /// <param name="sheetname">Excel File Sheet Name</param>
        /// <param name="dataSet">Data set Object</param>
        private static void FillDataAdapter(OleDbConnection oleDbConnection, string condition, string sheetname, DataSet dataSet)
        {
            OleDbDataAdapter oleDbDataAdapter;
            if (!string.IsNullOrEmpty(condition))
            {
                oleDbDataAdapter = new OleDbDataAdapter("SELECT * FROM [" + sheetname + "] where " + condition, oleDbConnection);
            }
            else
            {
                oleDbDataAdapter = new OleDbDataAdapter("SELECT  * FROM [" + sheetname + "]", oleDbConnection);
            }
            oleDbDataAdapter.Fill(dataSet, sheetname);
        }

        /// <summary>
        /// To Get all Sheet Names from the Excel File.
        /// </summary>
        /// <param name="filePath">Path of the Excel File</param>
        /// <returns>Return the Excel Sheet Names.</returns>
        public static string[] GetAllSheetNameFromExcel(string filePath)
        {
            string TABLE_NAME = "table_name";
            string[] excelSheetNames = null;
            using (OleDbConnection oleDbConnection = new OleDbConnection(GetConnectionString(filePath)))
            {
                OpenConnection(oleDbConnection);
                DataTable dataTable = GetExcelDataSchema(oleDbConnection);
                if (dataTable != null && dataTable.Rows.Count > 0)
                {
                    excelSheetNames = new string[dataTable.Rows.Count];
                    for (int sheetCount = 0; sheetCount < dataTable.Rows.Count; sheetCount++)
                    {
                        excelSheetNames[sheetCount] = dataTable.Rows[sheetCount][TABLE_NAME].ToString().Replace("'", string.Empty);
                    }
                }
            }
            return excelSheetNames;
        }

        /// <summary>
        /// To Map List of Dynamic Type to List<WebGridColumn>.
        /// </summary>
        /// <param name="lstImportError">List of type List<dynamic></param>
        /// <returns>Return the web grid Columns in List<WebGridColumn> format.</returns>
        public static List<WebGridColumn> MapListToViewModel(List<dynamic> lstImportError)
        {
            List<WebGridColumn> columnNames = new List<WebGridColumn>();
            DataTable dt = new DataTable();
            try
            {
                if (!Equals(lstImportError, null) && lstImportError.Count > 0)
                {
                    string jsonString = JsonConvert.SerializeObject(lstImportError);
                    dt = JsonConvert.DeserializeObject<DataTable>(jsonString);
                }
            }
            catch (Exception) { }

            columnNames = (from item in dt.Columns.Cast<DataColumn>()
                           select new WebGridColumn
                           {
                               ColumnName = item.ColumnName,
                               Header = item.ColumnName,
                           }).ToList();

            return columnNames;
        }

        /// <summary>
        /// To Update the Uploaded File Name with LoginUserName & CurrentDateTime.
        /// </summary>
        /// <param name="fileName">Uploaded File Name</param>
        /// <returns>Return the File Name.</returns>
        private static string GetUpdatedFileName(string fileName)
        {
            return string.Format("{0}_{1}_{2}{3}", Path.GetFileNameWithoutExtension(fileName), (string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name)) ? string.Empty : HttpContext.Current.User.Identity.Name, DateTime.UtcNow.ToString("ddMMyyyyhhmmss"), Path.GetExtension(fileName).ToLower());
        }

        /// <summary>
        /// To Map ImportConfigUploadHead Model to ImportConfigUploadHead View Model.
        /// </summary>
        /// <param name="model">ImportConfigUploadHeadModel model</param>
        /// <returns>Return Model of type ImportConfigUploadHeadViewModel</returns>
        public static ImportConfigUploadHeadViewModel ToImportConfigViewModel(ImportConfigUploadHeadModel model)
        {
            if (!Equals(model, null))
            {
                return new ImportConfigUploadHeadViewModel()
                {
                    Id = model.Id,
                    Name = model.Name,
                    TemplateUniqueName = model.TemplateUniqueName,
                    DownloadLink = model.DownloadLink,
                    Status = model.Status,
                    Description = model.Description,
                };
            }
            else
            {
                return new ImportConfigUploadHeadViewModel();
            }
        }

        private static void OpenConnection(OleDbConnection oleDbConnection)
        {
            try
            {
                oleDbConnection.Open();
            }
            catch (Exception ex)
            {
                oleDbConnection.Close();
                // throw new CorruptedExcelFileException(ex.Message);
            }
        }
    }
}