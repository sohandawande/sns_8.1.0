﻿using System.Collections.Generic;
using System.Web.Mvc;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;
using System.Linq;
using System;
using Znode.Engine.MvcAdmin.Helpers;

namespace Znode.Engine.MvcAdmin.Maps
{
    /// <summary>
    /// Static Class for AddOnValue View Model Mapper 
    /// </summary>
    public static class AddOnValueViewModelMap
    {
        /// <summary>
        /// Convert AddOnValueViewModel to AddOnValueModel
        /// </summary>
        /// <param name="model">AddOnValueViewModel model</param>
        /// <returns>Returns the model of AddOn Value</returns>
        public static AddOnValueModel ToModel(AddOnValueViewModel model)
        {
            if (Equals(model, null))
            {
                return null;
            }
            return new AddOnValueModel()
            {
                Name = model.Name,
                SKU = model.SKU,
                RecurringBillingPeriod = model.BillingPeriod,
                SupplierId = Equals(model.SupplierId, 0) ? null : model.SupplierId,
                TaxClassId = model.TaxClassId,
                QuantityOnHand = model.QuantityOnHand,
                DisplayOrder = Equals(model.DisplayOrder, null) ? 500 : Convert.ToInt32(model.DisplayOrder),
                ShippingRuleTypeId = model.ShippingRuleTypeId,
                RetailPrice = HelperMethods.FormatPrice(model.RetailPrice),
                Weight = HelperMethods.FormatPrice(model.Weight),
                Width = Equals(model.Width, null) ? null : model.Width,
                Height = Equals(model.Height, null) ? null : model.Height,
                Length = Equals(model.Length, null) ? null : model.Length,
                RecurringBillingInitialAmount = Equals(model.BillingAmount, null) ? null : model.BillingAmount,
                SalePrice = Equals(model.SalePrice, null) ? null : model.SalePrice,
                WholesalePrice = Equals(model.WholesalePrice, null) ? null : model.WholesalePrice,
                FreeShippingInd = model.EnableFreeShipping,
                RecurringBillingInd = model.EnableRecurringBilling,
                IsDefault = model.IsDefault,
                AddOnId = model.AddOnId,
                AddOnValueId = model.AddOnValueId,
                Description = Equals(model.Description, null) ? String.Empty : model.Description,
                ReOrderLevel = Equals(model.ReOrderLevel, null) ? 0 : Convert.ToInt32(model.ReOrderLevel),
                RecurringBillingFrequency = model.RecurringBillingFrequency,
                RecurringBillingTotalCycles = model.RecurringBillingTotalCycles,
                RecurringBillingInstallmentInd = model.RecurringBillingInstallmentInd
            };
        }

        /// <summary>
        /// Convert AddOnValueModel to AddOnValueViewModel
        /// </summary>
        /// <param name="model">AddOnValueModel model</param>
        /// <returns>Returns the view model of AddOn Value</returns>
        public static AddOnValueViewModel ToViewModel(AddOnValueModel model)
        {
            if (Equals(model, null))
            {
                return null;
            }
            return new AddOnValueViewModel()
            {
                Name = model.Name,
                SKU = model.SKU,
                BillingPeriod = model.RecurringBillingPeriod,
                SupplierId = model.SupplierId,
                TaxClassId = model.TaxClassId,
                QuantityOnHand = model.QuantityOnHand,
                DisplayOrder = model.DisplayOrder,
                ShippingRuleTypeId = model.ShippingRuleTypeId,
                RetailPrice = HelperMethods.FormatPrice(model.RetailPrice),
                Weight = HelperMethods.FormatPrice(model.Weight),
                Width = (model.Width <= 0) ? null : HelperMethods.FormatNullablePrice(model.Width),
                Height = (model.Height <= 0) ? null : HelperMethods.FormatNullablePrice(model.Height),
                Length = (model.Length <= 0) ? null : HelperMethods.FormatNullablePrice(model.Length),
                BillingAmount = Equals(model.RecurringBillingInitialAmount, null) ? null : HelperMethods.FormatNullablePrice(model.RecurringBillingInitialAmount),
                SalePrice = Equals(model.SalePrice, null) ? null : HelperMethods.FormatNullablePrice(model.SalePrice),
                WholesalePrice = Equals(model.WholesalePrice, null) ? null : HelperMethods.FormatNullablePrice(model.WholesalePrice),
                EnableFreeShipping = model.FreeShippingInd,
                EnableRecurringBilling = model.RecurringBillingInd,
                IsDefault = model.IsDefault,
                AddOnId = model.AddOnId,
                AddOnValueId = model.AddOnValueId,
                Description = model.Description,
                ReOrderLevel = (model.ReOrderLevel <= 0) ? null : model.ReOrderLevel,
                RecurringBillingFrequency = model.RecurringBillingFrequency,
                RecurringBillingTotalCycles = model.RecurringBillingTotalCycles,
                RecurringBillingInstallmentInd = model.RecurringBillingInstallmentInd
            };
        }

        /// <summary>
        /// This method returns the List of Suppliers
        /// </summary>
        /// <param name="model">IEnumerable SupplieModel model</param>
        /// <returns>Returns list of Suppliers</returns>
        public static List<SelectListItem> ToListItems(IEnumerable<SupplierModel> model)
        {
            List<SelectListItem> supplierItems = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                supplierItems = (from item in model
                                 select new SelectListItem
                                 {
                                     Text = item.Name,
                                     Value = item.SupplierId.ToString()
                                 }).ToList();
            }
            return supplierItems;
        }

        /// <summary>
        /// This method returns the List of TaxClasses
        /// </summary>
        /// <param name="model">IEnumerable TaxClassModel model</param>
        /// <returns>Returns list of TaxClass</returns>
        public static List<SelectListItem> ToListItems(IEnumerable<TaxClassModel> model)
        {
            List<SelectListItem> taxClassItems = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                taxClassItems = (from item in model
                                 select new SelectListItem
                                     {
                                         Text = item.Name,
                                         Value = item.TaxClassId.ToString()
                                     }).ToList();
            }
            return taxClassItems;
        }


        /// <summary>
        /// This method returns the List of Shipping Rule Type
        /// </summary>
        /// <param name="model">IEnumerable ShippingRuleModel model</param>
        /// <returns>Returns list of ShippingRule Type</returns>
        public static List<SelectListItem> ToListItems(IEnumerable<ShippingRuleTypeModel> model)
        {
            List<SelectListItem> shippingRuleType = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                shippingRuleType = (from item in model
                                    select new SelectListItem
                                    {
                                        Text = item.Name,
                                        Value = item.ShippingRuleTypeId.ToString()
                                    }).ToList();
            }
            return shippingRuleType;
        }
        
        /// <summary>
        /// Convert AddOnValueListModel to AddOnValueListViewModel
        /// </summary>
        /// <param name="models">AddOnValueListModel models,</param>
        /// <param name="pageIndex">Nullable int pageIndex</param>
        /// <param name="recordPerPage">Nullable int recordPerPage</param>
        /// <param name="totalPages">int? totalPages</param>
        /// <param name="totalResults">int? totalResults</param>
        /// <returns></returns>
        public static AddOnValueListViewModel ToListViewModel(AddOnValueListModel models, int? pageIndex, int? recordPerPage, int? totalPages, int? totalResults)
        {
            if (!Equals(models, null) && !Equals(models.AddOnValues, null))
            {
                var viewModel = new AddOnValueListViewModel()
                {
                    AddOnValues = models.AddOnValues.ToList().Select(
                    x => new AddOnValueViewModel()
                    {
                        AddOnValueId = !Equals(x.AddOnValueId, null) ? x.AddOnValueId : 0,
                        Name = !Equals(x.Name, null) ? x.Name : string.Empty,
                        SKU = !Equals(x.SKU, null) ? x.SKU : string.Empty,
                        QuantityOnHand = !Equals(x.QuantityOnHand, null) ? x.QuantityOnHand : 0,
                        ReOrderLevel = !Equals(x.ReOrderLevel, null) ? x.ReOrderLevel : 0,
                        DisplayOrder = !Equals(x.DisplayOrder, null) ? x.DisplayOrder : 500,
                        DisplayPrice = Equals(x.RetailPrice, null) ?string.Empty: MvcAdminUnits.GetCurrencyValue(x.RetailPrice),
                        IsDefault = !Equals(x.IsDefault, null) ? x.IsDefault : false

                    }).ToList()
                };

                viewModel.RecordPerPage = recordPerPage ?? 10;
                viewModel.Page = pageIndex ?? 1;
                viewModel.TotalPages = Convert.ToInt32(totalPages);
                viewModel.TotalResults = Convert.ToInt32(totalResults);
                return viewModel;
            }
            else
            {
                return new AddOnValueListViewModel();
            }
        }
    }
}