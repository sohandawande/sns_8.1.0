﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    public class ProfileCommonViewModelMap
    {
        public static ProfileCommonViewModel ToViewModel(IEnumerable<ProfileCommonModel> model)
        {
            ProfileCommonViewModel viewModel = new ProfileCommonViewModel();
            if (!Equals(model, null) && model.Count() > 0)
            {
                viewModel.ProfileStoreAccess = string.Join(",", model.ToList().Select(x => x.PortalId.ToString()));
                viewModel.UserId = (Equals(model.ToList().Select(x => x.UserId).FirstOrDefault(), null)) ? Guid.Empty : model.ToList().Select(x => x.UserId).FirstOrDefault();
            }
            return viewModel;
        }
    }
}