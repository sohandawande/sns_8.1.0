﻿using System;
using System.Collections.Generic;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    public static class ManufacturersViewModelMap
    {
        /// <summary>
        /// View Model Mapper for Manufacturer
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static ManufacturersViewModel ToViewModel(ManufacturerModel model)
        {
            return new ManufacturersViewModel()
            {
                ManufacturerId = model.ManufacturerId,
                Description = model.Description,
                Email = model.Email,
                IsActive = model.IsActive,
                Name = model.Name,
                DisplayOrder=model.DisplayOrder
            };
        }

        /// <summary>
        /// Converts Manufacturer model to Manufacturer list view model
        /// </summary>
        /// <param name="models"></param>
        /// <returns></returns>
        public static ManufacturersListViewModel ToListViewModel(ManufacturerListModel model)
        {
            if (!Equals(model, null) && !Equals(model.Manufacturers, null))
            {
                var viewModel = new ManufacturersListViewModel()
                {
                    Manufacturers = model.Manufacturers.ToList().Select(
                    x => new ManufacturersViewModel()
                    {
                        ManufacturerId = x.ManufacturerId,
                        Name = x.Name,
                        Description = x.Description,
                        Email = x.Email,
                        IsActive = x.IsActive,
                        DisplayOrder=x.DisplayOrder
                    }).ToList()
                };

                viewModel.TotalResults = Convert.ToInt32(model.TotalResults);
                viewModel.Page = Convert.ToInt32(model.PageIndex);
                viewModel.RecordPerPage = Convert.ToInt32(model.PageSize);
                viewModel.TotalPages = Convert.ToInt32(model.TotalPages);
                viewModel.TotalResults = Convert.ToInt32(model.TotalResults);
                
                return viewModel;
            }
            else
            {
                return new ManufacturersListViewModel();
            }
        }

        /// <summary>
        /// Model Mapper for Manufacturer
        /// </summary>
        /// <param name="model">ManufacturersViewModel model</param>
        /// <returns>returns the ManufacturerModel</returns>
        public static ManufacturerModel ToModel(ManufacturersViewModel model)
        {
            return new ManufacturerModel()
            {
                Custom1 = string.Empty,
                Custom2 = string.Empty,
                Custom3 = string.Empty,
                Description = model.Description,
                DisplayOrder = (model.DisplayOrder!=null)?(int)model.DisplayOrder:0,
                Email = model.Email,
                EmailNotificationTemplate = string.Empty,
                IsActive = model.IsActive,
                IsDropShipper = true,
                ManufacturerId = model.ManufacturerId,
                Name = model.Name,
                PortalId = null,
                Website = string.Empty
            };
        }

    }
}