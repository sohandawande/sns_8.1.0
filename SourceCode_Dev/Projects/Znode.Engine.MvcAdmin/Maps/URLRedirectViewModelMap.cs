﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    /// <summary>
    /// Static Class for URLRedirect View Model Mapper 
    /// </summary>
    public static class URLRedirectViewModelMap
    {
        /// <summary>
        /// Convert URLRedirectViewModel to UrlRedirectModel 
        /// </summary>
        /// <param name="viewModel">to covert to model</param>
        /// <returns>UrlRedirectModel</returns>
        public static UrlRedirectModel ToModel(URLRedirectViewModel viewModel)
        {
            if (Equals(viewModel, null))
            {
                return null;
            }

            return new UrlRedirectModel()
            {
                NewUrl =MakeUrl(viewModel.NewUrl),
                OldUrl = MakeUrl(viewModel.OldUrl),
                IsActive = viewModel.IsActive,
                UrlRedirectId = viewModel.URLRedirectId
            };
        }

        /// <summary>
        /// Convert UrlRedirectModel to URL301RedirectViewModel  
        /// </summary>
        /// <param name="model">to covert to view model</param>
        /// <returns>URLRedirectViewModel</returns>
        public static URLRedirectViewModel ToViewModel(UrlRedirectModel model)
        {
            if (Equals(model, null))
            {
                return null;
            }

            return new URLRedirectViewModel()
            {
                NewUrl =FormattedUrl(model.NewUrl),
                OldUrl = FormattedUrl(model.OldUrl),
                IsActive = model.IsActive,
                URLRedirectId = model.UrlRedirectId
            };
        }

        /// <summary>
        /// Convert UrlRedirectListModel to URLRedirectListViewModel.
        /// </summary>
        /// <param name="models">To convert to list view model.</param>
        /// <returns>List of url redirect view model.</returns>
        public static URLRedirectListViewModel ToListViewModel(UrlRedirectListModel models)
        {
            if (!Equals(models, null) && !Equals(models.UrlRedirects, null))
            {
                var viewModel = new URLRedirectListViewModel()
                {
                    UrlRedirects = models.UrlRedirects.ToList().Select(
                    model => new URLRedirectViewModel()
                    {
                        NewUrl = FormattedUrl(model.NewUrl),
                        OldUrl = FormattedUrl(model.OldUrl),
                        IsActive = model.IsActive,
                        URLRedirectId = model.UrlRedirectId
                    }).ToList()
                };
                viewModel.TotalResults = Convert.ToInt32(models.TotalResults);
                viewModel.RecordPerPage = models.PageSize.Value;
                viewModel.Page = models.PageIndex.Value;
                viewModel.TotalPages = Convert.ToInt32(models.TotalPages);
                return viewModel;
            }
            else
            {
                return new URLRedirectListViewModel();
            }
        }

        /// <summary>
        /// To get list items from Status enum.
        /// </summary>
        /// <returns>returns list on Status items</returns>
        public static List<SelectListItem> ToListItems()
        {
            List<SelectListItem> statusItem = new List<SelectListItem>();
            statusItem.Add(new SelectListItem { Value = Convert.ToInt32(StatusType.Enabled).ToString(), Text = StatusType.Enabled.ToString() });
            statusItem.Add(new SelectListItem { Value = Convert.ToInt32(StatusType.Disabled).ToString(), Text = StatusType.Disabled.ToString() });
            return statusItem;
        }

        #region Private Methods
        /// <summary>
        /// Formatted Url Method
        /// </summary>
        /// <param name="url">The value of url</param>
        /// <returns>Returns the formatted url</returns>
        private static string FormattedUrl(string url)
        {
            if (url.Length > 0)
            {
                url = url.Replace("~/", string.Empty);
            }
            return url;
        }

        /// <summary>
        /// Make Url Method
        /// </summary>
        /// <param name="seoUrl">The value of seoUrl</param>
        /// <returns>Returns the url</returns>
        private static string MakeUrl(string seoUrl)
        {
            seoUrl = "~/" + seoUrl;
            return seoUrl;
        }
        #endregion

    }
}