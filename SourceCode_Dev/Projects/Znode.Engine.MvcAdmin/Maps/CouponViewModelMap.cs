﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    public static class CouponViewModelMap
    {
        public static CouponViewModel ToViewModel(CouponModel model)
        {
            if (!Equals(model, null))
            {
                var viewModel = new CouponViewModel
                {
                    Coupon = model.Coupon,
                    CouponMessage = model.CouponMessage,
                    CouponApplied = model.CouponApplied,
                    CouponValid = model.CouponValid
                };
                return viewModel;
            }
            return null;
        }

        public static CouponModel ToViewModel(CouponViewModel viewModel)
        {
            if (!Equals(viewModel, null))
            {
                var model = new CouponModel
                {
                    Coupon = viewModel.Coupon,
                    CouponMessage = viewModel.CouponMessage,
                    CouponApplied = viewModel.CouponApplied,
                    CouponValid = viewModel.CouponValid
                };
                return model;
            }
            return null;
        }
    }
}