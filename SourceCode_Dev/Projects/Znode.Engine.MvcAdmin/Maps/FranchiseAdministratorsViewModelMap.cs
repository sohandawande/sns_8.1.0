﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    /// <summary>
    /// Static Class for  Franchise Administrators Mapper
    /// </summary>
    public static class FranchiseAdministratorsViewModelMap
    {
        /// <summary>
        /// Convert FranchiseAccountViewModel to AccountModel
        /// </summary>
        /// <param name="model">FranchiseAccountViewModel model</param>
        /// <returns>AccountModel</returns>
        public static AccountModel ToAccountModel(FranchiseAccountViewModel model)
        {
            return new AccountModel()
            {
                AddressModel = new AddressModel()
                {
                    City = model.ContactInfo.City,
                    StreetAddress1 = model.ContactInfo.Street1,
                    StreetAddress2 = model.ContactInfo.Street2,
                    StateCode = model.ContactInfo.State,
                    PostalCode = model.ContactInfo.PostalCode,
                    CompanyName = model.StoreName,
                },
                User = new UserModel()
                {

                    Username = model.UserName,
                    Email = model.ContactInfo.Email,
                    Password=model.Password,
                },
                IsActive = true,
                AccountId = model.AccountId,
                ProfileId=model.ProfileId,
                Email = model.ContactInfo.Email,
                FirstName = model.ContactInfo.FirstName,
                FullName = model.UserName,
                LastName = model.ContactInfo.LastName,
                PhoneNumber = model.ContactInfo.PhoneNumber,
                RoleName = MvcAdminConstants.FranchiseRole,
                UserName = model.UserName,
                ExternalAccountNum = model.FranchiseNumber,
                CompanyName = model.StoreName,
                CreateUser = HttpContext.Current.User.Identity.Name,
                EmailOptIn = model.IsEmailOptional,
                UserType=model.UserType,
                PortalId = model.PortalId,
            };
        }

        /// <summary>
        /// Convert Vendor Account Model to Franchise Account View Model
        /// </summary>
        /// <param name="model">VendorAccountModel model</param>
        /// <returns>Franchise Account View Model</returns>
        public static FranchiseAccountViewModel ToFranchiseViewModel(VendorAccountModel model)
        {
            {
                var viewModel = new FranchiseAccountViewModel()
                {

                    UserName = model.UserName,
                    AccountId = model.AccountId,
                    FullName = model.FullName,
                    ContactInfo = new VendorContactViewModel()
                    {
                        FirstName = model.FirstName,
                        LastName = model.LastName,
                        Email = model.Email,
                        AccountId = model.AccountId,
                        PhoneNumber = model.PhoneNumber,
                        Street1 = model.Street,
                        Street2 = model.Street1,
                        State = model.StateCode,
                        FullName = model.FirstName + " " + model.LastName,
                        PostalCode=model.PostalCode,
                        CompanyName=model.CompanyName,
                        City=model.City,
                    },
                    accountViewModel = new AccountViewModel()
                    {
                        UserId = model.UserId,
                        CreateDte = model.CreateDte,
                        UpdateDte = model.UpdateDte,
                        Website = model.Website,
                        Description = model.Description,
                        CreateUser = model.CreateUser,
                        UpdateUser = model.UpdateUser,
                        Custom1 = model.Custom1,
                        Custom2 = model.Custom2,
                        Custom3 = model.Custom3,
                        Source = model.Source,
                        EmailOptIn= model.EmailOptIn,
                    },
                    FranchiseNumber = Equals(model.ExternalAccountNo, null) ? string.Empty : model.ExternalAccountNo,
                    CheckRole = model.CheckRole
                };
                return viewModel;
            }
        }
    }
}
