﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    public class AccountViewModelMap
    {
        /// <summary>
        /// Convert Login View Model to Account Model
        /// </summary>
        /// <param name="model">Model of Type LoginViewModel</param>
        /// <returns>Return model of type AccountModel</returns>
        public static AccountModel ToLoginModel(LoginViewModel model)
        {
            return new AccountModel()
                {
                    User = new UserModel()
                        {
                            Username = model.Username,
                            Password = model.Password,
                            PasswordQuestion = model.PasswordQuestion,                            
                        },
                        BaseUrl = (!string.IsNullOrEmpty(HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority))) ? HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) : string.Empty
                };
        }

        /// <summary>
        /// Convert Account Model to Login View Model.
        /// </summary>
        /// <param name="model">Model of Type AccountModel</param>
        /// <returns>Return model of type LoginViewModel</returns>
        public static LoginViewModel ToLoginViewModel(AccountModel model)
        {
            return new LoginViewModel()
            {
                Username = model.User.Username,
                Password = model.User.Password,
                PasswordQuestion = model.User.PasswordQuestion,
                IsResetPassword = model.User.IsLockedOut,
                PasswordResetToken = model.User.PasswordToken,
                ProfileId = model.ProfileId,
            };
        }

        /// <summary>
        /// Convert Account Model to Account View Model.
        /// </summary>
        /// <param name="model">Model of Type AccountModel</param>
        /// <returns>Return model of type AccountViewModel</returns>
        public static AccountViewModel ToAccountViewModel(AccountModel model)
        {
            var accountViewModel = new AccountViewModel()
                {
                    AccountId = model.AccountId,
                    EmailAddress = Equals(model.User, null) ? model.Email : model.User.Email,
                    UserName = Equals(model.User, null) ? string.Empty : model.User.Username,
                    PasswordQuestion = Equals(model.User, null) ? string.Empty : model.User.PasswordQuestion,
                    PasswordAnswer = Equals(model.User, null) ? string.Empty : model.User.PasswordAnswer,
                    UserId = Equals(model.User, null) ? Guid.Empty : model.User.UserId,
                    IsUserExists = !Equals(model.User, null),
                    CompanyName = model.CompanyName,
                    EmailOptIn = model.EmailOptIn,

                    EnableCustomerPricing = model.EnableCustomerPricing,
                    ExternalId = model.ExternalId,
                    IsActive = model.IsActive,
                    ProfileId = model.Profiles.Any() ? model.Profiles.First().ProfileId : 0,
                    CreateDte = model.CreateDate,
                    UpdateDte = model.UpdateDate,
                    Description = model.Description,
                    Website = model.Website,

                    CreateUser = model.CreateUser,
                    UpdateUser = model.UpdateUser,
                    Custom1 = model.Custom1,
                    Custom2 = model.Custom2,
                    Custom3 = model.Custom3,
                    Source = model.Source,
                    ParentAccountID=model.ParentAccountId,//PRFT Custom Code
                };

            return accountViewModel;
        }

        /// <summary>
        /// Convert ResetPassword Model to Account Model.
        /// </summary>
        /// <param name="model">Model of Type ResetPasswordModel</param>
        /// <returns>Return model of type AccountModel</returns>
        public static AccountModel ToAccountModel(ResetPasswordModel model)
        {
            var accountModel = new AccountModel()
            {
                User = new UserModel()
                {
                    Username = model.Username,
                    Password = model.Password,
                    Email = model.Email,
                    IsApproved = true,
                },
                IsActive = true,
            };

            return accountModel;
        }

        /// <summary>
        /// Convert Account View Model to Account Model.
        /// </summary>
        /// <param name="model">Model of Type AccountViewModel</param>
        /// <returns>Return model of type AccountModel</returns>
        public static AccountModel ToAccountModel(AccountViewModel model)
        {
            return new AccountModel()
            {
                AccountId = model.AccountId,
                Email = model.EmailAddress,
                User = new UserModel()
                {
                    Username = model.UserName,
                    Email = model.EmailAddress,
                    UserId = model.UserId.GetValueOrDefault(),
                    PasswordQuestion = model.PasswordQuestion,
                    PasswordAnswer = model.PasswordAnswer,

                },
                CompanyName = model.CompanyName,
                EmailOptIn = model.EmailOptIn,
                EnableCustomerPricing = model.EnableCustomerPricing,
                ExternalId = model.ExternalId,
                IsActive = model.IsActive,
                ProfileId = model.ProfileId,
                UserId = model.UserId,
                //Addresses = new Collection<AddressModel>(model.Addresses.Select(AddressViewModelMap.ToModel).ToList()),
                BaseUrl = model.BaseUrl,
            };
        }

        /// <summary>
        /// Convert ChangePassword View Model to Account Model.
        /// </summary>
        /// <param name="model">Model of Type ChangePasswordViewModel</param>
        /// <returns>Return model of type AccountModel</returns>
        public static AccountModel ToChangePasswordModel(ChangePasswordViewModel model)
        {
            var changePasswordModel = new AccountModel()
            {
                User = new UserModel()
                {
                    Username = model.UserName,
                    Password = model.OldPassword,
                    NewPassword = model.NewPassword,
                    PasswordToken = model.PasswordToken,
                }
            };

            return changePasswordModel;
        }

        /// <summary>
        /// Mapper to convert rderModel model to rdersViewModel
        /// </summary>
        /// <param name="model">OrderModel model</param>
        /// <returns>returns OrdersViewModel</returns>
        public static OrdersViewModel ToOrderViewModel(OrderModel model)
        {
            if (Equals(model.OrderId, 0))
            {
                return null;
            }
            return new OrdersViewModel()
            {
                OrderId = model.OrderId,
                OrderDate = model.OrderDate.GetValueOrDefault().ToShortDateString(),
                OrderTotal = model.Total.GetValueOrDefault(0),
                PaymentMethod = (model.PaymentTypeId != null) ? ((EnumPaymentType)model.PaymentTypeId).ToString() : string.Empty,
                AccountId = model.AccountId,
                SubTotal = model.SubTotal.GetValueOrDefault(0),
                Discount = -model.DiscountAmount.GetValueOrDefault(0),
                Tax = model.TaxCost.GetValueOrDefault(0),
                Shipping = model.ShippingCost.GetValueOrDefault(0),
                OrderLineItems = ToOrderLineItemViewModel(model.OrderLineItems),
                BillingAddress = AddressViewModelMap.ToModel(model),
                ShippingAddress = AddressViewModelMap.ToModel(model.OrderLineItems.First().OrderShipment),
                ReceiptHtml = Equals(model.ReceiptHtml, null) ? String.Empty : HttpUtility.HtmlDecode(model.ReceiptHtml),
            };
        }

        /// <summary>
        /// Map the Collection of OrderLine Item Model to the Collection of OrderLine Item View Model
        /// </summary>
        /// <param name="model">Collection of the OrderLineItemModel</param>
        /// <returns>Return Collection of OrderLineItemViewModel</returns>
        public static Collection<OrderLineItemViewModel> ToOrderLineItemViewModel(Collection<OrderLineItemModel> model)
        {
            var orderLineItemModel =
                new Collection<OrderLineItemViewModel>(model.Select(x => new OrderLineItemViewModel()
                {
                    OrderLineItemId = x.OrderLineItemId,
                    Quantity = x.Quantity.GetValueOrDefault(0),
                    ItemPrice = x.Price.GetValueOrDefault(0),
                    Total = (x.Quantity * x.Price).GetValueOrDefault(0),
                    Discount = x.DiscountAmount.GetValueOrDefault(0),
                    Tax = x.SalesTax.GetValueOrDefault(0),
                    Shipping = x.ShippingCost.GetValueOrDefault(0),
                    Name = x.Name,
                    Description = x.Description,
                    ParentOrderLineItemId = x.ParentOrderLineItemId,
                    OrderLineItemRelationshipTypeId = x.OrderLineItemRelationshipTypeId
                }).ToList());

            foreach (var orderLineItemViewModel in orderLineItemModel.Where(x => x.ParentOrderLineItemId == null))
            {
                var childLineItems = orderLineItemModel.Where(x => x.ParentOrderLineItemId == orderLineItemViewModel.OrderLineItemId);
                if (childLineItems.Any())
                {
                    orderLineItemViewModel.Price += childLineItems.Sum(x => x.ItemPrice);
                    orderLineItemViewModel.Total += childLineItems.Sum(x => x.Total);
                }
            }

            return new Collection<OrderLineItemViewModel>(orderLineItemModel.Where(y => y.ParentOrderLineItemId == null).ToList());
        }

        /// <summary>
        /// Converts Role Permission list model to Role Permission List View Model.
        /// </summary>
        /// <param name="model">RolePermissionListModel model.</param>
        /// <returns>Return Users Role Permission in RolePermissionListViewModel format</returns>
        public static RolePermissionListViewModel ToListModel(RolePermissionListModel model)
        {
            if (!Equals(model, null) && !Equals(model.UserPermissionList, null))
            {
                var viewModel = new RolePermissionListViewModel()
                {
                    UserRolePermissionList = model.UserPermissionList.ToList().Select(
                    x => new RolePermissionViewModel()
                    {
                        RoleId = x.RoleId,
                        PermissionId = x.PermissionId,
                        EntityAction = x.EntityAction,
                    }).ToList()
                };
                return viewModel;
            }
            else
            {
                return new RolePermissionListViewModel();
            }
        }

        /// <summary>
        /// Converts Role Menu list model to Role Menu List View Model.
        /// </summary>
        /// <param name="model">RoleMenuListModel model.</param>
        /// <returns>Return Users Role Menus in RoleMenuListViewModel format</returns>
        public static RoleMenuListViewModel ToMenuListModel(RoleMenuListModel model)
        {
            if (!Equals(model, null) && !Equals(model.RoleMenuList, null))
            {
                var viewModel = new RoleMenuListViewModel()
                {
                    RoleMenuList = model.RoleMenuList.ToList().Select(
                    x => new RoleMenuViewModel()
                    {
                        MenuId=x.MenuId,
                        MenuName = x.MenuName,
                        ParentMenuId = x.ParentMenuId,
                        AreaName = x.AreaName,
                        ControllerName = x.ControllerName,
                        ActionName = x.ActionName,
                        CSSClassName = x.CSSClassName,
                        StatusId = x.StatusId,
                        SequenceNumber = x.SequenceNumber,
                    }).ToList()
                };
                return viewModel;
            }
            else
            {
                return new RoleMenuListViewModel();
            }
        }
    }
}