﻿using System;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.ViewModels;


namespace Znode.Engine.MvcAdmin.Maps
{
    /// <summary>
    /// Static Class for ContentPage View Model Mapper 
    /// </summary>
    public static class ContentPageRevisionViewModelMap
    {
        /// <summary>
        /// Convert ContentPageRevisionModel to ContentPageRevisionViewModel
        /// </summary>
        /// <param name="model">Model to convert</param>
        /// <returns>ContentPageRevisionViewModel</returns>
        public static ContentPageRevisionViewModel ToViewModel(ContentPageRevisionModel model)
        {
            if (!Equals(model, null))
            {
                return new ContentPageRevisionViewModel()
                {
                    RevisionId = model.RevisionID,
                    Html = model.HtmlText,
                    Description = model.Description,
                    UpdatedUser = model.UpdatedUser,
                    OldHtml = model.OldHtml,
                    UpdateDate = HelperMethods.ViewDateFormatWithTime(model.UpdateDate),
                    ContentPageName = model.ContentPageName
                };
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Convert ContentPageRevisionViewModel to ContentPageRevisionModel
        /// </summary>
        /// <param name="viewModel">View Model to convert</param>
        /// <returns>ContentPageRevisionModel</returns>
        public static ContentPageRevisionModel ToModel(ContentPageRevisionViewModel viewModel)
        {
            if (!Equals(viewModel, null))
            {
                return new ContentPageRevisionModel()
                {
                    RevisionID = viewModel.RevisionId,
                    HtmlText = viewModel.Html,
                    Description = viewModel.Description,
                    UpdatedUser = viewModel.UpdatedUser,
                    OldHtml = viewModel.OldHtml,
                    UpdateDate =Convert.ToDateTime(viewModel.UpdateDate),
                    ContentPageName = viewModel.ContentPageName
                };
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Convert IEnumerable<HighlightModel> to HighlightsListViewModel
        /// </summary>
        /// <param name="models">to convert to list view model</param>
        /// <returns>HighlightsListViewModel</returns>
        public static ContentPageRevisionListViewModel ToListViewModel(ContentPageRevisionListModel models)
        {
            if (!Equals(models, null) && !Equals(models.ContentPageRevisions, null))
            {
                var viewModel = new ContentPageRevisionListViewModel()
                {
                    ContentPageRevisions = models.ContentPageRevisions.ToList().Select(
                    model => new ContentPageRevisionViewModel()
                    {
                        UpdatedUser = model.UpdatedUser,
                        RevisionId = model.RevisionID,
                        Description = model.Description,
                        UpdateDate =HelperMethods.ViewDateFormatWithTime(model.UpdateDate),
                        OldHtml = model.OldHtml,
                        Html = model.HtmlText,
                        ContentPageName = model.ContentPageName
                    }).ToList()
                };
                viewModel.TotalResults = Convert.ToInt32(models.TotalResults);
                return viewModel;
            }
            else
            {
                return new ContentPageRevisionListViewModel();
            }
        }

        /// <summary>
        /// Maps the list of ContentPageRevisionListModel model to ContentPageRevisionListViewModel model.
        /// </summary>
        /// <param name="models">The list of type ContentPageRevisionListModel.</param>
        /// <returns>Returns the mapped list of CatalogAssociatedCategoriesModel model to CategoryListViewModel.</returns>
        public static ContentPageRevisionListViewModel ToListViewModel(ContentPageRevisionListModel models, int? pageIndex, int? recordPerPage, int? totalPages, int? totalResults)
        {
            if (!Equals(models, null) && !Equals(models.ContentPageRevisions, null))
            {
                var viewModel = new ContentPageRevisionListViewModel()
                {
                    ContentPageRevisions = models.ContentPageRevisions.ToList().Select(
                    model => new ContentPageRevisionViewModel()
                    {
                        RevisionId = !Equals(model.RevisionID, null) ? model.RevisionID : 0,
                        Description = !Equals(model.Description, null) ? model.Description : string.Empty,
                        OldHtml = !Equals(model.OldHtml, null) ? model.OldHtml : string.Empty,
                        Html = !Equals(model.HtmlText, null) ? model.HtmlText : string.Empty,
                        UpdatedUser = !Equals(model.UpdatedUser, null) ? model.UpdatedUser : string.Empty,
                        UpdateDate = HelperMethods.ViewDateFormatWithTime(model.UpdateDate),
                        ContentPageName = model.ContentPageName
                    }).ToList()
                };
                viewModel.RecordPerPage = recordPerPage ?? 10;
                viewModel.Page = pageIndex ?? 1;
                viewModel.TotalPages = Convert.ToInt32(totalPages);
                viewModel.TotalResults = Convert.ToInt32(totalResults);
                return viewModel;
            }

            return new ContentPageRevisionListViewModel();
        }
    }
}