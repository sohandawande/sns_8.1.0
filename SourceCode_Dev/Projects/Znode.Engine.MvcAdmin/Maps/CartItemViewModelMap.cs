﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web.Mvc;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    public static class CartItemViewModelMap
    {
        /// <summary>
        /// Maps the ShoppingCartItemModel to CartItemViewModel.
        /// </summary>
        /// <param name="model">Model of type ShoppingCartItemModel</param>
        /// <returns>Returns mapped CartItemViewModel from ShoppingCartItemModel</returns>
        public static CartItemViewModel ToViewModel(ShoppingCartItemModel model)
        {
            if (!Equals(model, null))
            {
                CartItemViewModel viewModel = new CartItemViewModel()
                    {
                        Description = model.CartDescription,
                        ExternalId = model.ExternalId,
                        Packaging = model.AttributeIds,
                        ProductId = model.ProductId,
                        Quantity = model.Quantity,
                        ShippingCost = model.ShippingCost,
                        Sku = model.Sku,
                        UnitPrice = model.UnitPrice,
                        ExtendedPrice = model.ExtendedPrice,
                        InsufficientQuantity = model.InsufficientQuantity,
                        AddOnValueIds = model.AddOnValueIds != null ? string.Join(",", model.AddOnValueIds) : string.Empty,
                        //Map SKU Image Path                       
                        ImagePath = HelperMethods.GetImagePath(model.ImagePath),
                        ProductName = model.ProductName,
                        MaxQuantity = model.MaxQuantity, 

                        //PRFT Custom code:Start
                        ERPUnitPrice = model.ERPUnitPrice,
                        ERPExtendedPrice=model.ERPExtendedPrice
                        //PRFT Custom code:Start
                };
                ImageHelper imageHelper = new ImageHelper();
                viewModel.ImageMediumPath = imageHelper.GetImageHttpPathMedium(model.ImageMediumPath);

                return viewModel;
            }
            return null;
        }

        /// <summary>
        /// Maps CartItemViewModel to ShoppingCartItemModel.
        /// </summary>
        /// <param name="viewModel">ViewModel of type CartItemViewModel</param>
        /// <returns>Returns mapped ShoppingCartItemModel from CartItemViewModel.</returns>
        public static ShoppingCartItemModel ToModel(CartItemViewModel viewModel)
        {
            if (!Equals(viewModel, null))
            {
                ShoppingCartItemModel itemModel = new ShoppingCartItemModel();
                itemModel.ExternalId = viewModel.ExternalId;
                itemModel.AttributeIds = viewModel.Packaging;
                itemModel.ProductId = viewModel.ProductId;
                itemModel.Quantity = viewModel.Quantity;
                itemModel.ShippingCost = viewModel.ShippingCost;
                itemModel.Sku = viewModel.Sku;
                itemModel.UnitPrice = viewModel.UnitPrice;
                itemModel.ExtendedPrice = viewModel.ExtendedPrice;
                itemModel.AddOnValueIds = (!string.IsNullOrEmpty(viewModel.AddOnValueIds) ? viewModel.AddOnValueIds.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToArray() : null);
                itemModel.ImagePath = viewModel.ImagePath;
                itemModel.MaxQuantity = viewModel.MaxQuantity;
                itemModel.ProductName = viewModel.ProductName;
                itemModel.BundleItems = viewModel.BundleItems;
                //PRFT Custom code:Start
                itemModel.ERPUnitPrice = viewModel.ERPUnitPrice;
                itemModel.ERPExtendedPrice = viewModel.ERPExtendedPrice;
                //PRFT Custom code:Start

                if (!string.IsNullOrEmpty(viewModel.AddOnValuesCustomText))
                {
                    itemModel.AddOnValuesCustomText = GetAddOnValuesCustomTextDictionary(viewModel);
                }
                return itemModel;
            }
            return null;
        }

        /// <summary>
        /// Maps the productid,quantity and sku of product to ShoppingCartItemModel.
        /// </summary>
        /// <param name="productId">int productId of the product</param>
        /// <param name="quantity">int quantity of the item in the cart</param>
        /// <param name="sku">string sku of the item in the cart</param>
        /// <returns>Returns mapped ShoppingCartItemModel on the basis of productId,quantity and sku.</returns>
        public static ShoppingCartItemModel ToShoppingCartModel(int productId, int quantity, string sku)
        {
            ShoppingCartItemModel itemModel = new ShoppingCartItemModel()
                {
                    ProductId = productId,
                    Sku = sku,
                    Quantity = quantity,
                };
            return itemModel;
        }

        /// <summary>
        /// Map AddOnValueIds and AddOnValueCutom text to Dictionary object
        /// </summary>
        /// <param name="viewModel">View Model Of CartItems</param>
        /// <returns>Dictionary Object</returns>
        private static Dictionary<int, string> GetAddOnValuesCustomTextDictionary(CartItemViewModel viewModel)
        {
            Dictionary<int, string> addOnValuesCustomText = new Dictionary<int, string>();

            string[] addOnValueIds = !Equals(viewModel.AddOnValueIds, null) ? viewModel.AddOnValueIds.Split(',') : null;

            string[] addOnValuesText = !Equals(viewModel.AddOnValueIds, null) ? viewModel.AddOnValuesCustomText.Split(',') : null;

            if (!Equals(addOnValueIds, null) && !Equals(addOnValuesText, null))
            {
                for (int index = 0; index < addOnValueIds.Length; index++)
                {
                    if (!string.IsNullOrEmpty(addOnValueIds[index]))
                    {
                        addOnValuesCustomText.Add(Convert.ToInt32(addOnValueIds[index]), addOnValuesText[index]);
                    }
                }
            }
            return addOnValuesCustomText;
        }
    }
}