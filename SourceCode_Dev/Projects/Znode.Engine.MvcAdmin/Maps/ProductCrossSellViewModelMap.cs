﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Extensions;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    public class ProductCrossSellViewModelMap
    {
        /// <summary>
        /// Converts CrossSellListModel to ProductCrossSellListViewModel.
        /// </summary>
        /// <param name="models">CrossSellListModel</param>
        /// <returns>ProductCrossSellListViewModel</returns>
        public static ProductCrossSellListViewModel ToListViewModel(CrossSellListModel models)
        {
            if (!Equals(models.CrossSellProducts, null) && models.CrossSellProducts.Count > 0)
            {
                var viewModel = new ProductCrossSellListViewModel()
                {
                    CrossSellProducts = models.CrossSellProducts.ToList().Select(
                    x => new ProductCrossSellViewModel()
                    {
                        ProductCrossSellTypeId = !Equals(x.ProductCrossSellTypeId, null) ? x.ProductCrossSellTypeId : MvcAdminConstants.ZerothIndex,
                        ProductId = !Equals(x.ProductId, null) ? x.ProductId : MvcAdminConstants.ZerothIndex,
                        RelatedProductId = !Equals(x.RelatedProductId, null) ? x.RelatedProductId : MvcAdminConstants.ZerothIndex,
                        RelationTypeId = !Equals(x.RelationTypeId, null) ? x.RelationTypeId : MvcAdminConstants.ZerothIndex,
                        DisplayOrder = !Equals(x.DisplayOrder, null) ? x.DisplayOrder : MvcAdminConstants.ZerothIndex
                    }).ToList()
                };
                return viewModel;
            }
            else
            {
                return new ProductCrossSellListViewModel();
            }
        }

        /// <summary>
        /// Converts list of Cross Sell List model to Cross Sell List View model.
        /// </summary>
        /// <param name="model">List of cross sell model<PortalModel></param>
        /// <param name="pageIndex">Page index<PortalModel></param>
        /// <param name="recordPerPage">Record Per Page<PortalModel></param> 
        /// <param name="portalId">Portal id of the portal being used</param>
        /// <param name="categoryId">Category Id of the category being used.</param>
        /// <returns>Product cross sell list view model</returns>
        public static ProductCrossSellListViewModel ToCrossSellItemListViewModel(CrossSellListModel model, int? pageIndex, int? recordPerPage, int portalId, int categoryId)
        {
            var viewModel = new ProductCrossSellListViewModel();

            if (!Equals(model.CrossSellProducts, null))
            {
                viewModel.CrossSellProducts = model.CrossSellProducts.Select(
                x => new ProductCrossSellViewModel()
                {
                    ProductId = x.ProductId,
                    ProductName = x.ProductName,
                    FrequentlyBoughtProduct1 = x.FrequentlyBoughtProduct1,
                    FrequentlyBoughtProduct2 = x.FrequentlyBoughtProduct2,
                    CrossSellProducts = x.CrossSellProducts,
                    CategoryId = categoryId,
                    PortalId = portalId
                }).ToList();
            }
            viewModel.RecordPerPage = recordPerPage ?? MvcAdminConstants.DefaultPageSize;
            viewModel.Page = pageIndex ?? MvcAdminConstants.DefaultPageNumber;
            viewModel.TotalResults = Convert.ToInt32(model.TotalResults);
            return viewModel;
        }

        /// <summary>
        /// Gtes select list items.
        /// </summary>
        /// <param name="model">View Model.</param>
        /// <param name="labelValue">Label value of select list item.</param>
        /// <param name="labelText">Text value of select list item.</param>
        /// <param name="isIncludeDefaultItem">if the select list item includes default value.</param>
        /// <returns>List of SelectListItems</returns>
        public static List<SelectListItem> GetSelectListItem(IEnumerable<BaseViewModel> model, string labelValue, string labelText, bool isIncludeDefaultItem = false)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            try
            {
                if (!Equals(model, null))
                {
                    items = model.Select(x => new SelectListItem
                    {
                        Value = x.GetProperty(labelValue).ToString(),
                        Text = x.GetProperty(labelText).ToString(),
                        Selected = false
                    }).ToList();

                    if (isIncludeDefaultItem)
                    {
                        items.Insert(MvcAdminConstants.ZerothIndex, new SelectListItem { Text = MvcAdminConstants.AllKey, Value = MvcAdminConstants.ZerothIndex.ToString() });
                    }
                }
            }
            catch (Exception ex)
            {
                items = new List<SelectListItem>();
            }
            return items;
        }

        /// <summary>
        /// To map ViewModel to Model
        /// </summary>
        /// <param name="model">ProductCrossSellViewModel</param>
        /// <returns>returns  CrossSellModel</returns>
        public static CrossSellModel ToModel(ProductCrossSellViewModel viewModel)
        {
            if (Equals(viewModel, null))
            {
                return null;
            }

            return new CrossSellModel()
            {
                ProductCrossSellTypeId = viewModel.ProductCrossSellTypeId,
                ProductId = viewModel.ProductId,
                ProductName = viewModel.ProductName,
                RelationTypeId = viewModel.RelationTypeId,
                RelatedProductId = viewModel.RelatedProductId,
                DisplayOrder = viewModel.DisplayOrder,
            };
        }

        public static CrossSellListModel ToModel(ProductCrossSellListViewModel viewModel)
        {
            CrossSellListModel model = new CrossSellListModel();
            if (!Equals(viewModel.CrossSellProducts, null) && viewModel.CrossSellProducts.Count > 0)
            {
                model.CrossSellProducts = viewModel.CrossSellProducts.ToList().Select(
                 x => new CrossSellModel()
                 {
                     ProductCrossSellTypeId = !Equals(x.ProductCrossSellTypeId, null) ? x.ProductCrossSellTypeId : MvcAdminConstants.ZerothIndex,
                     ProductId = !Equals(x.ProductId, null) ? x.ProductId : MvcAdminConstants.ZerothIndex,
                     RelatedProductId = !Equals(x.RelatedProductId, null) ? x.RelatedProductId : MvcAdminConstants.ZerothIndex,
                     RelationTypeId = !Equals(x.RelationTypeId, null) ? x.RelationTypeId : MvcAdminConstants.ZerothIndex,
                     DisplayOrder = !Equals(x.DisplayOrder, null) ? x.DisplayOrder : MvcAdminConstants.ZerothIndex
                 }).ToList().ToCollection<CrossSellModel>();
            }
            return model;
        }

        /// <summary>
        /// To map Model to ViewModel
        /// </summary>
        /// <param name="model">CrossSellModel</param>
        /// <returns>returns ProductCrossSellViewModel</returns>
        public static ProductCrossSellViewModel ToViewModel(CrossSellModel model)
        {
            if (Equals(model, null))
            {
                return null;
            }

            return new ProductCrossSellViewModel()
            {
                ProductCrossSellTypeId = model.ProductCrossSellTypeId,
                ProductId = model.ProductId,
                ProductName = model.ProductName,
                RelationTypeId = model.RelationTypeId,
                RelatedProductId = model.RelatedProductId,
                DisplayOrder = model.DisplayOrder,
            };
        }

        public static ProductCrossSellListViewModel PersonalizationViewModelToProductCrossSellListViewModel(PersonalizationViewModel model, int productCrossSellType)
        {
            int displayOrder = 0;
            if (!Equals(model, null) && !Equals(model.CrossSellProductList, null) && !Equals(model.CrossSellProductList.AssignedId, null) && (model.CrossSellProductList.AssignedId.Count() > 0))
            {
                ProductCrossSellListViewModel productCrossSellListViewModel = new ProductCrossSellListViewModel();
                foreach (int crossSellItemId in model.CrossSellProductList.AssignedId)
                {
                    ProductCrossSellViewModel productCrossSellViewModel = new ProductCrossSellViewModel();
                    productCrossSellViewModel.ProductId = model.ProductId;
                    productCrossSellViewModel.RelatedProductId = crossSellItemId;
                    productCrossSellViewModel.DisplayOrder = ++displayOrder;
                    productCrossSellViewModel.RelationTypeId = productCrossSellType;
                    productCrossSellListViewModel.CrossSellProducts.Add(productCrossSellViewModel);
                }
                return productCrossSellListViewModel;
            }
            return new ProductCrossSellListViewModel();
        }
    }
}