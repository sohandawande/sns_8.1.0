﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    public class CategoryViewModelMap
    {
        #region Public Methods

        /// <summary>
        /// Converts category list view model to category model list.
        /// </summary>
        /// <param name="model">Category list model.</param>
        /// <returns>Category List view model.</returns>
        public static CategoryListViewModel ToListModel(CategoryListModel model, int productId = 0)
        {
            if (!Equals(model, null) && !Equals(model.Categories, null))
            {
                var viewModel = new CategoryListViewModel()
                {
                    Categories = model.Categories.ToList().Select(
                    x => new CategoryViewModel()
                    {
                        CategoryId = x.CategoryId,
                        Name = x.Name,
                        Title = x.Title,
                        ProductId = productId,
                    }).ToList()
                };
                viewModel.RecordPerPage = model.PageSize ?? 10;
                viewModel.Page = model.PageIndex ?? 1;
                viewModel.TotalPages = Convert.ToInt32(model.TotalPages);
                viewModel.TotalResults = Convert.ToInt32(model.TotalResults);
                return viewModel;
            }
            else
            {
                return new CategoryListViewModel();
            }
        }

        /// <summary>
        /// Maps the list of CatalogAssociatedCategories model to CategoryListView model.
        /// </summary>
        /// <param name="models">The list of type CatalogAssociatedCategoriesModels.</param>
        /// <returns>Returns the mapped list of CatalogAssociatedCategoriesModel model to CategoryListViewModel.</returns>
        public static CatalogAssociatedCategoriesListViewModel ToListViewModel(CatalogAssociatedCategoriesListModel model)
        {
            if (Equals(model, null))
            {
                return new CatalogAssociatedCategoriesListViewModel();
            }
            var viewModel = new CatalogAssociatedCategoriesListViewModel()
            {
                AssociatedCategories = model.CategoryList.ToList().Select(
                x => new CatalogAssociatedCategoriesViewModel()
                {
                    CategoryId = !Equals(x.CategoryId, null) ? x.CategoryId : 0,
                    CatalogId = !Equals(x.CatalogId, null) ? x.CatalogId : 0,
                    CategoryNodeId = !Equals(x.CategoryNodeId, null) ? x.CategoryNodeId : 0,
                    Name = !Equals(x.Name, null) ? x.Name : string.Empty,
                    ParentCategoryNodeId = !Equals(x.ParentCategoryNodeId, null) ? x.ParentCategoryNodeId : 0,
                    SeoUrl = !Equals(x.SeoUrl, null) ? x.SeoUrl : string.Empty,
                    ActiveInd = x.ActiveInd,
                    DisplayOrder = !Equals(x.DisplayOrder, null) ? x.DisplayOrder : 99,
                    CatalogName = !Equals(x.CatalogName, null) ? x.CatalogName : string.Empty,
                    VisibleInd = x.VisibleInd,
                    //PRFT Custom Code: Start
                    //Property for Manufacturer ID
                    Custom1 = x.Custom1
                    //PRFT Custom Code: End
                }).ToList()
            };
            viewModel.TotalResults = Convert.ToInt32(model.TotalResults);
            viewModel.Page = Convert.ToInt32(model.PageIndex);
            viewModel.RecordPerPage = Convert.ToInt32(model.PageSize);
            viewModel.TotalPages = Convert.ToInt32(model.TotalPages);
            viewModel.TotalResults = Convert.ToInt32(model.TotalResults);
            return viewModel;
        }

        /// <summary>
        /// Creates a SelectListItem List for Categories
        /// </summary>
        /// <param name="model">IEnumerable type of list of CatalogAssociatedCategoriesModel.</param>
        /// <returns>SelectListItem List for categories.</returns>
        public static List<SelectListItem> ToParentCategorySelectListItems(IEnumerable<CategoryNodeModel> model)
        {
            List<SelectListItem> categories = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                categories = (from item in model
                              select new SelectListItem
                              {
                                  Text = item.Name,
                                  Value = item.CategoryNodeId.ToString(),
                              }).ToList();
            }
            return categories;
        }

        /// <summary>
        /// Creates a SelectListItem List for Categories
        /// </summary>
        /// <param name="model">List of CatalogAssociatedCategoriesModel.</param>
        /// <returns>SelectListItem List for categories.</returns>
        public static List<SelectListItem> ToParentCategoryListItems(IEnumerable<CatalogAssociatedCategoriesModel> model)
        {
            List<SelectListItem> categories = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                categories = (from item in model
                              select new SelectListItem
                              {
                                  Text = item.Name,
                                  Value = item.CategoryNodeId.ToString(),
                              }).ToList();
            }
            return categories;
        }

        /// <summary>
        /// Creates list of selectListItem type for theme.
        /// </summary>
        /// <param name="model">List of ThemeModel.</param>
        /// <returns>SelectListItem list for theme. </returns>
        public static List<SelectListItem> ToThemeSelectListItems(IEnumerable<ThemeModel> model, int themeId = 0)
        {
            List<SelectListItem> themeList = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                themeList = (from item in model
                             select new SelectListItem
                             {
                                 Text = item.Name,
                                 Value = item.ThemeID.ToString(),
                                 Selected = Equals(item.ThemeID, themeId) ? true : false
                             }).ToList();
            }
            return themeList;
        }

        /// <summary>
        /// Creates list of selectListItem type for css.
        /// </summary>
        /// <param name="model">List of CSSModel.</param>
        /// <returns>SelectListItem list for css. </returns>
        public static List<SelectListItem> ToCssSelectListItems(IEnumerable<CSSModel> model, int cssId = 0)
        {
            List<SelectListItem> cssList = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                model = model.GroupBy(x => x.Name).Select(y => y.First());
                cssList = (from item in model
                           select new SelectListItem
                           {
                               Text = item.Name,
                               Value = item.CSSID.ToString(),
                               Selected = Equals(item.CSSID, cssId) ? true : false
                           }).ToList();
            }
            return cssList;
        }

        /// <summary>
        /// Creates list of selectListItem type for master page.
        /// </summary>
        /// <param name="model">List of MasterPageModel.</param>
        /// <returns>SelectListItem list for master pages.</returns>
        public static List<SelectListItem> ToMasterPageSelectListItems(IEnumerable<MasterPageModel> model, int masterPageId = 0)
        {
            List<SelectListItem> masterPageList = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                masterPageList = (from item in model
                                  select new SelectListItem
                                  {
                                      Text = item.Name,
                                      Value = item.MasterPageId.ToString(),
                                      Selected = Equals(item.MasterPageId, masterPageId) ? true : false
                                  }).ToList();
            }
            return masterPageList;
        }

        /// <summary>
        ///  Creates a SelectListItem list for profiles.
        /// </summary>
        /// <param name="model">IEnumerable type of list of ProfileModel</param>
        /// <returns>SelectListItem list for profiles.</returns>
        public static List<SelectListItem> ToProfileSelectListItems(IEnumerable<ProfileModel> model, int profileId = 0)
        {
            List<SelectListItem> profileList = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                profileList = (from item in model
                               select new SelectListItem
                               {
                                   Text = item.Name,
                                   Value = item.ProfileId.ToString(),
                                   Selected = Equals(item.ProfileId, profileId) ? true : false
                               }).ToList();
            }
            return profileList;
        }

        /// <summary>
        ///  Maps the list of CategoryModel Model to CategoryListViewModel Model.
        /// </summary>
        /// <param name="models">IEnumerable type of CategoryModel</param>
        /// <returns>Returns the mapped Category Model to CategoryListViewModel Model.</returns>
        public static CategoryListViewModel ToViewModel(IEnumerable<CategoryModel> models)
        {
            if (!Equals(models, null))
            {
                var viewModel = new CategoryListViewModel()
                {
                    Categories = models.ToList().Select(x =>
                            new CategoryViewModel()
                            {
                                CategoryId = x.CategoryId,
                                Name = x.Name,
                            }).ToList()
                };

                return viewModel;
            }
            return null;
        }

        /// <summary>
        /// This method returns the catalog List
        /// </summary>
        /// <param name="model">The parameter is Promotion Type Model</param>
        /// <returns>list of all promotion types</returns>
        public static List<SelectListItem> ToListItems(IEnumerable<CatalogModel> model)
        {
            List<SelectListItem> catalogItem = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                catalogItem = (from item in model
                               select new SelectListItem
                               {
                                   Text = item.Name,
                                   Value = item.CatalogId.ToString()
                               }).ToList();
            }
            return catalogItem;
        }

        /// <summary>
        /// Maps the Category model to CategoryView model.
        /// </summary>
        /// <param name="model">Model of type CategoryModel</param>
        /// <returns>Returns the mapped Category model to CategoryView model.</returns>
        public static CategoryViewModel ToViewModel(CategoryModel model)
        {
            ImageHelper imageHelper = new ImageHelper();
            if (!Equals(model, null))
            {
                var viewModel = new CategoryViewModel()
                {
                    CategoryId = model.CategoryId,
                    Name = model.Name,
                    Title = model.Title,
                    DisplayOrder = model.DisplayOrder,
                    ChildCategories = model.SubcategoryGridIsVisible,
                    ImageAltTag = model.ImageAltTag,
                    ImageFile = model.ImageFile,
                    ImageLargePath = HelperMethods.GetImageRelativePath(imageHelper.GetImageHttpPathLarge(model.ImageFile)),
                    ImageMediumPath = HelperMethods.GetImageRelativePath(imageHelper.GetImageHttpPathMedium(model.ImageFile)),
                    ImageSmallThumbnailPath = HelperMethods.GetImageRelativePath(imageHelper.GetImageHttpPathSmallThumbnail(model.ImageFile)),
                    ShortDescription = model.ShortDescription,
                    Description = model.Description,
                    AlternateDescription = model.AlternateDescription,
                    CategoryBanner = model.CategoryBanner,
                    SeoDescription = Equals(model.SeoDescription, "<Name>") ? string.Empty : model.SeoDescription,
                    SeoKeywords = model.SeoKeywords,
                    SeoUrl = model.SeoUrl,
                    SeoTitle = Equals(model.SeoTitle, "<Name>") ? string.Empty : model.SeoTitle,
                    PortalID = model.PortalID,
                    IsVisible = model.IsVisible,

                    //PRFT Custom Code: Start
                    //Property for Manufacturer ID
                    Custom1 = model.Custom1,
                    IsManufacturer = string.IsNullOrEmpty(model.Custom1)? false : true
                    //PRFT Custom Code: End

                };
                return viewModel;
            }
            return new CategoryViewModel();
        }

        /// <summary>
        /// This method is responsible to map the list of category model to CategoryListViewModel
        /// </summary>
        /// <param name="models">List of CategoryModel</param>
        /// <param name="catalogId">int Catalog Id</param>
        /// <returns>return the object of type CategoryListViewModel</returns>
        public static CategoryTreeListViewModel ToViewModel(IEnumerable<CategoryModel> models, int catalogId)
        {
            CategoryTreeListViewModel viewModel = new CategoryTreeListViewModel()
            {
                Categories = models.Where(categoryItem => categoryItem.CategoryNodes.Any(categoryNodeItem => Equals(categoryNodeItem.ParentCategoryNodeId, null) && categoryNodeItem.IsActive && Equals(categoryNodeItem.CatalogId, catalogId))).ToList().Select(filteredCategoryItem =>
                       new CategoryHeaderViewModel()
                       {
                           id = filteredCategoryItem.CategoryId,
                           text = string.Concat(filteredCategoryItem.Name, " (", filteredCategoryItem.ProductCount, ")"),
                           children = ToChildMenuItemViewModel(models, catalogId, filteredCategoryItem.CategoryNodes[0].CategoryNodeId),

                       }).OrderBy(nodeorder => nodeorder.id).ThenBy(name => name.text).ToList()

            };
            return viewModel;
        }

        /// <summary>
        /// This method is used to add the Child Menus to the sub menus.
        /// </summary>
        /// <param name="models">List of CategoryModel</param>
        /// <param name="catalogId">int Catalog Id</param>
        /// <param name="parentCategoryId">Int Parent Category Id</param>
        /// <returns>returns the list of CategorySubHeaderViewModel</returns>
        private static List<CategorySubHeaderViewModel> ToChildMenuItemViewModel(IEnumerable<CategoryModel> models, int catalogId, int? parentCategoryId)
        {
            List<CategorySubHeaderViewModel> list = null;
            if (!Equals(parentCategoryId, null))
            {
                list = models.Where(p => p.CategoryNodes.Any(y => y.IsActive && Equals(y.ParentCategoryNodeId, parentCategoryId) && Equals(y.CatalogId, catalogId))).ToList().Select(x =>
                    new CategorySubHeaderViewModel()
                    {
                        id = x.CategoryId,
                        text = string.Concat(x.Name, " (", x.ProductCount, ")"),
                        children = ToChildMenuItemViewModel(models, catalogId, x.CategoryNodes[0].CategoryNodeId)
                    }).ToList();

            }
            return list;
        }
        /// <summary>
        /// Maps the CategoryView model to Category model.
        /// </summary>
        /// <param name="viewModel">Model of type CategoryViewModel</param>
        /// <returns>Returns the mapped CategoryView model to Category model.</returns>
        public static CategoryModel ToModel(CategoryViewModel viewModel)
        {
            if (Equals(viewModel, null))
            {
                return new CategoryModel();
            }
            var model = new CategoryModel()
            {
                CategoryId = viewModel.CategoryId,
                Name = viewModel.Name,
                Title = viewModel.Title,
                DisplayOrder = viewModel.DisplayOrder,
                ImageAltTag = viewModel.ImageAltTag,
                SubcategoryGridIsVisible = viewModel.ChildCategories,
                ImageFile = viewModel.ImageFile,
                ImageLargePath = viewModel.ImageLargePath,
                ImageMediumPath = viewModel.ImageMediumPath,
                ImageSmallThumbnailPath = viewModel.ImageSmallThumbnailPath,
                ShortDescription = viewModel.ShortDescription,
                Description = viewModel.Description,
                AlternateDescription = viewModel.AlternateDescription,
                CategoryBanner = viewModel.CategoryBanner,
                SeoDescription = viewModel.SeoDescription,
                SeoKeywords = viewModel.SeoKeywords,
                SeoUrl = viewModel.SeoUrl,
                SeoTitle = viewModel.SeoTitle,
                PortalID = viewModel.PortalID,
                IsVisible = true,
                ProductId = Equals(viewModel.ProductId, null) ? 0 : viewModel.ProductId.Value,
                CategoryIds = viewModel.CategoryIds,
                UserName = HttpContext.Current.User.Identity.Name,
                
                //PRFT Custom Code: Start
                //Property for Manufacturer ID
                Custom1 = viewModel.Custom1
                //PRFT Custom Code: End
            };
            return model;
        }

        /// <summary>
        /// This method returns the Promotion List Model
        /// </summary>
        /// <param name="models">IEnumerable type list of Promotion Model</param>
        /// <returns>List of type Promotion</returns>
        public static CategoryListViewModel ToListViewModel(CategoryListModel listModels)
        {
            var viewModel = new CategoryListViewModel();

            if (!Equals(listModels.Categories, null))
            {
                viewModel.Categories = listModels.Categories.Select(
                x => new CategoryViewModel()
                {
                    CategoryId = x.CategoryId,
                    Name = x.Name,
                    Title = x.Title,
                    DisplayOrder = x.DisplayOrder,
                    ChildCategories = x.SubcategoryGridIsVisible,
                    ImageAltTag = x.ImageAltTag,
                    ImageFile = x.ImageFile,
                    ShortDescription = x.ShortDescription,
                    Description = x.Description,
                    AlternateDescription = x.AlternateDescription,
                    CategoryBanner = x.CategoryBanner,
                    SeoDescription = x.SeoDescription,
                    SeoKeywords = x.SeoKeywords,
                    SeoUrl = x.SeoUrl,
                    SeoTitle = x.SeoTitle,
                    PortalID = x.PortalID,
                    //PRFT Custom Code: Start
                    //Property for indicating Category as Manufacturer
                    IsManufacturer = string.IsNullOrEmpty(x.Custom1) ? false : true,
                    Custom1 = x.Custom1
                    //PRFT Custom Code: End
                }).ToList();
                viewModel.TotalResults = Convert.ToInt32(listModels.TotalResults);
                return viewModel;
            }
            else
            {
                return new CategoryListViewModel();
            }

        }

        /// <summary>
        /// Maps the Profile model to CategoryProfileView model.
        /// </summary>
        /// <param name="model">Model of type ProfileModel</param>
        /// <returns>Returns the mapped Profile model to CategoryProfileView model.</returns>
        public static CategoryProfileViewModel ToCategoryProfileViewModel(ProfileModel model)
        {
            CategoryProfileViewModel profileViewModel = new CategoryProfileViewModel();
            if (!Equals(model, null))
            {
                profileViewModel.ProfileID = model.ProfileId;
                profileViewModel.ProfileName = model.Name;
            }
            return profileViewModel;
        }

        public static CategoryViewModel ToCategoryViewModel(CategoryModel searchCategoryModel)
        {
            var category = new CategoryViewModel()
            {
                CategoryId = searchCategoryModel.CategoryId,
                Name = searchCategoryModel.Name,
                Title = searchCategoryModel.Title,
                ShortDescription = searchCategoryModel.ShortDescription,
                SeoTitle = searchCategoryModel.SeoTitle,
                SeoUrl = searchCategoryModel.SeoUrl,
                SeoKeywords = searchCategoryModel.SeoKeywords,
                SeoDescription = searchCategoryModel.SeoDescription,
                Subcategories = PopulateCategory(searchCategoryModel.Subcategories),
                CategoryBanner = searchCategoryModel.CategoryBanner,
                
                //PRFT Custom Code: Start
                //Property for indicating Category as Manufacturer
                IsManufacturer = string.IsNullOrEmpty(searchCategoryModel.Custom1) ? false : true,
                Custom1 = searchCategoryModel.Custom1
                //PRFT Custom Code: End
            };

            return category;
        }

        private static Collection<CategoryViewModel> PopulateCategory(Collection<CategoryModel> categoryModel)
        {
            if (categoryModel != null && categoryModel.Any())
            {
                var categoryViewList = categoryModel.Select(x => new CategoryViewModel()
                {
                    Name = x.Name,
                    Title = x.Title,
                    CategoryId = x.CategoryId,
                    SeoTitle = x.SeoTitle,
                    SeoUrl = x.SeoUrl,
                    Subcategories = PopulateCategory(x.Subcategories)
                }).ToList();

                if (categoryViewList.Any())
                    return new Collection<CategoryViewModel>(categoryViewList);
            }

            return new Collection<CategoryViewModel>();
        }
        #endregion
    }
}