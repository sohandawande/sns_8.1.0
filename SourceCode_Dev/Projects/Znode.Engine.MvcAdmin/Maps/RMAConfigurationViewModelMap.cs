﻿using System;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    public class RMAConfigurationViewModelMap
    {

        /// <summary>
        /// Converting RMAConfiguration View Model to RMAConfiguration Model  
        /// </summary>
        /// <param name="model"></param>
        /// <returns>RMAConfigurationModel</returns>
        public static RMAConfigurationModel ToModel(RMAConfigurationViewModel model)
        {
            return new RMAConfigurationModel()
            {
                RMAConfigId = model.RMAConfigId,
                DisplayName = model.DisplayName,
                EmailId = model.EmailId,
                EnableEmailNotification = model.EnableEmailNotification,
                Address = model.ReturnMailingAddress,
                MaxDays = model.MaxDays,
                GiftCardExpirationPeriod = model.GiftCardExpirationPeriod,
                GiftCardNotification = model.GiftCardNotification,
                ShippingDirections = model.ShippingInstruction
            };
        }

        /// <summary>
        /// Converting RMAConfiguration Model to RMAConfiguration View Model  
        /// </summary>
        /// <param name="model"></param>
        /// <returns>RMAConfigurationViewModel</returns>
        public static RMAConfigurationViewModel ToViewModel(RMAConfigurationModel model)
        {
            return new RMAConfigurationViewModel()
            {
                RMAConfigId = model.RMAConfigId,
                DisplayName = model.DisplayName,
                EmailId = model.EmailId,
                EnableEmailNotification = model.EnableEmailNotification,
                ReturnMailingAddress = model.Address,
                MaxDays = model.MaxDays,
                GiftCardExpirationPeriod = Equals(model.GiftCardExpirationPeriod, null) ? 0 : model.GiftCardExpirationPeriod.Value,
                GiftCardNotification = model.GiftCardNotification,
                ShippingInstruction = model.ShippingDirections
            };
        }

        /// <summary>
        /// Map RMAConfiguration list view model to RMAConfiguration model list.
        /// </summary>
        /// <param name="model">RMAConfiguration list model.</param>
        /// <returns>RMAConfiguration List view model.</returns>
        public static RMAConfigurationListViewModel ToListViewModel(RMAConfigurationListModel model)
        {
            var viewModel = new RMAConfigurationListViewModel();

            if (!Equals(model.RMAConfigurations, null))
            {
                viewModel.RMAConfigurations = model.RMAConfigurations.Select(
                x => new RMAConfigurationViewModel()
                {
                    RMAConfigId = x.RMAConfigId,
                    MaxDays = x.MaxDays,
                    DisplayName = x.DisplayName,
                    EmailId = x.EmailId,
                    ReturnMailingAddress = x.Address,
                    ShippingInstruction = x.ShippingDirections,
                    EnableEmailNotification = x.EnableEmailNotification,
                    GiftCardExpirationPeriod = Equals(x.GiftCardExpirationPeriod, null) ? 0 : x.GiftCardExpirationPeriod.Value,
                    GiftCardNotification = x.GiftCardNotification
                }).ToList();
                return viewModel;
            }
            else
            {
                return new RMAConfigurationListViewModel();
            }
        }

        public static ReasonForReturnListViewModel ToListViewModel(ReasonForReturnListModel listModel)
        {
            if (!Equals(listModel, null) && !Equals(listModel.ReasonsForReturn, null))
            {
                var viewModel = new ReasonForReturnListViewModel()
                {
                    ReasonsForReturns = listModel.ReasonsForReturn.Select(
                    model => new ReasonForReturnViewModel()
                    {
                        ReasonForReturnId = model.ReasonForReturnId,
                        Reason = model.Name,
                        IsEnabled = model.IsEnabled
                    }).ToList()
                };
                viewModel.Page = Convert.ToInt32(listModel.PageIndex);
                viewModel.RecordPerPage = Convert.ToInt32(listModel.PageSize);
                viewModel.TotalPages = Convert.ToInt32(listModel.TotalPages);
                viewModel.TotalResults = Convert.ToInt32(listModel.TotalResults);
                return viewModel;  
            }
            else
            {
                return new ReasonForReturnListViewModel();
            }    
        }

        public static RequestStatusListViewModel ToListViewModel(RequestStatusListModel listModel)
        {
            if (!Equals(listModel, null) && !Equals(listModel.RequestStatusList, null))
            {
                var viewModel = new RequestStatusListViewModel()
                {
                    RequestStatusList = listModel.RequestStatusList.Select(
                    model => new RequestStatusViewModel()
                    {
                        RequestStatusID = model.RequestStatusID,
                        Name = model.Name,
                        IsEnabled = model.IsEnabled
                    }).ToList()
                };
                viewModel.Page = Convert.ToInt32(listModel.PageIndex);
                viewModel.RecordPerPage = Convert.ToInt32(listModel.PageSize);
                viewModel.TotalPages = Convert.ToInt32(listModel.TotalPages);
                viewModel.TotalResults = Convert.ToInt32(listModel.TotalResults);
                return viewModel;
            }
            else
            {
                return new RequestStatusListViewModel();
            }
        }
    }
}