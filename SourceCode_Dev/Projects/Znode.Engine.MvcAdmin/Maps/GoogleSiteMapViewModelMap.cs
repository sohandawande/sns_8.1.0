﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.WebPages.Html;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Enum;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    /// <summary>
    /// Mapper for Google Site Map View Model Map
    /// </summary>
    public class GoogleSiteMapViewModelMap
    {
        /// <summary>
        /// Mapper to convert GoogleSiteMapModel to GoogleSiteMapViewModel
        /// </summary>
        /// <param name="model">GoogleSiteMapModel model</param>
        /// <returns>GoogleSiteMapViewModel</returns>
        public static GoogleSiteMapViewModel ToViewModel(GoogleSiteMapModel model)
        {
            if (!Equals(model, null))
            {
                var googleSiteMap = new GoogleSiteMapViewModel()
                {
                    GoogleSiteMapId = model.GoogleSiteMapId,
                    Frequency = model.ChangeFreq,
                    LastModification = model.LastModified,
                    XMLFileName = model.XmlFileName,
                    Priority = model.Priority,
                    XMLSiteMap = model.RootTag,
                    XMLSiteMapType = model.RootTagValue,
                    Date = model.Date,
                    SuccessXMLGenerationMessage = model.SuccessXMLGenerationMessage,
                    ErrorMessage = model.ErrorMessage,
                    GoogleSiteMapFeedTitle = model.GoogleSiteMapFeedTitle,
                    GoogleSiteMapFeedLink = model.GoogleSiteMapFeedLink,
                    GoogleSiteMapFeedDescription = model.GoogleSiteMapFeedDescription
                };
                return googleSiteMap;
            }
            return null;
        }

        /// <summary>
        /// Mapper to convert GoogleSiteMapViewModel to GoogleSiteMapModel
        /// </summary>
        /// <param name="model">GoogleSiteMapViewModel model</param>
        /// <returns>GoogleSiteMapModel</returns>
        public static GoogleSiteMapModel ToModel(GoogleSiteMapViewModel model)
        {
            if (!Equals(model, null))
            {
                var googleSiteMap = new GoogleSiteMapModel()
                {
                    GoogleSiteMapId = model.GoogleSiteMapId,
                    ChangeFreq = model.Frequency,
                    LastModified = model.LastModification,
                    XmlFileName = model.XMLFileName,
                    Priority = model.Priority,
                    RootTag = model.XMLSiteMap,
                    RootTagValue = model.XMLSiteMapType,
                    Date = model.Date,
                    SuccessXMLGenerationMessage = model.SuccessXMLGenerationMessage,
                    PortalId = model.StoreList,
                    GoogleSiteMapFeedTitle = model.GoogleSiteMapFeedTitle,
                    GoogleSiteMapFeedLink = model.GoogleSiteMapFeedLink,
                    GoogleSiteMapFeedDescription = model.GoogleSiteMapFeedDescription
                };
                return googleSiteMap;
            }
            return null;
        }

        /// <summary>
        /// This method returns the List of frequency
        /// </summary>
        /// <returns>Returns List<SelectListItem></returns>
        public static List<SelectListItem> ToFrequencyListItems()
        {
            List<SelectListItem> frequencyItem = new List<SelectListItem>();
            List<Frequency> frequency = Enum.GetValues(typeof(Frequency)).Cast<Frequency>().ToList();

            frequency.ForEach(x =>
            {
                frequencyItem.Add(new SelectListItem()
                {
                    Text = x.ToString(),
                    Value = x.ToString()
                });
            });
            return frequencyItem;
        }

        /// <summary>
        /// This method returns the List of priority
        /// </summary>
        /// <returns>Returns List<SelectListItem></returns>
        public static List<SelectListItem> ToPriorityListItems()
        {
            List<SelectListItem> priorityItem = new List<SelectListItem>();
            List<Priority> priority = Enum.GetValues(typeof(Priority)).Cast<Priority>().ToList();
            string value = string.Empty;
            priority.ForEach(x =>
            {
                if ((Convert.ToInt32(x)).ToString().Length > 2)
                {
                    string[] arr = (Convert.ToInt32(x)).ToString().Split('0');
                    if (arr.Length > 2)
                    {
                        value = arr[0] + ".0";
                    }
                    else
                    {
                        value = arr[0] + "." + arr[1];
                    }
                }
                priorityItem.Add(new SelectListItem()
                {
                    Text = value,
                    Value = value
                });
            });

            return priorityItem;
        }

        /// <summary>
        /// This method returns the List of format in which last modified date is required
        /// </summary>
        /// <returns>Returns List<SelectListItem></returns>
        public static List<SelectListItem> ToLastModificationListItems()
        {
            List<SelectListItem> lastModificationItem = new List<SelectListItem>();
            List<LastModification> lastModification = Enum.GetValues(typeof(LastModification)).Cast<LastModification>().ToList();

            DownloadHelper downloadHelper = new DownloadHelper();

            lastModificationItem.Add(new SelectListItem { Value = Convert.ToInt32(LastModification.None).ToString(), Text = downloadHelper.ToEnumString(LastModification.None) });
            lastModificationItem.Add(new SelectListItem { Value = Convert.ToInt32(LastModification.DatabaseDate).ToString(), Text = downloadHelper.ToEnumString(LastModification.DatabaseDate) });
            lastModificationItem.Add(new SelectListItem { Value = Convert.ToInt32(LastModification.CustomDate).ToString(), Text = downloadHelper.ToEnumString(LastModification.CustomDate) });

            return lastModificationItem;
        }

        /// <summary>
        /// This method returns the List of XML Site Map
        /// </summary>
        /// <returns>Returns List<SelectListItem></returns>
        public static List<SelectListItem> ToXMLSiteMapListItems()
        {
            DownloadHelper downloadHelper = new DownloadHelper();
            List<SelectListItem> xMLSiteMap = new List<SelectListItem>();
            xMLSiteMap.Add(new SelectListItem { Value = Convert.ToInt32(XMLSiteMap.XmlSiteMap).ToString(), Text = downloadHelper.ToEnumString(XMLSiteMap.XmlSiteMap) });
            xMLSiteMap.Add(new SelectListItem { Value = Convert.ToInt32(XMLSiteMap.GoogleProductFeed).ToString(), Text = downloadHelper.ToEnumString(XMLSiteMap.GoogleProductFeed) });
            xMLSiteMap.Add(new SelectListItem { Value = Convert.ToInt32(XMLSiteMap.BingProductFeed).ToString(), Text = downloadHelper.ToEnumString(XMLSiteMap.BingProductFeed) });

            return xMLSiteMap;
        }

        /// <summary>
        /// This method returns the List of XML Site Map Type
        /// </summary>
        /// <returns>Returns List<SelectListItem></returns>
        public static List<SelectListItem> ToXMLSiteMapTypeListItems()
        {
            DownloadHelper downloadHelper = new DownloadHelper();
            List<SelectListItem> xMLSiteMapType = new List<SelectListItem>();
            xMLSiteMapType.Add(new SelectListItem { Value = Convert.ToInt32(XMLSiteMapType.Category).ToString(), Text = downloadHelper.ToEnumString(XMLSiteMapType.Category) });
            xMLSiteMapType.Add(new SelectListItem { Value = Convert.ToInt32(XMLSiteMapType.ContentPages).ToString(), Text = downloadHelper.ToEnumString(XMLSiteMapType.ContentPages) });
            //PRFT Custom code Start
            xMLSiteMapType.Add(new SelectListItem { Value = Convert.ToInt32(XMLSiteMapType.Product).ToString(), Text = downloadHelper.ToEnumString(XMLSiteMapType.Product) });
            //PRFT Custom code : End

            return xMLSiteMapType;
        }
    }
}