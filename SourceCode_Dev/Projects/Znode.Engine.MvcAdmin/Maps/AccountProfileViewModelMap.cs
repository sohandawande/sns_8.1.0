﻿using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    public class AccountProfileViewModelMap 
    {
        /// <summary>
        /// Converts Account profile view model to Account profile model   
        /// </summary>
        /// <param name="viewModel">Model of type AccountProfileView viewModel</param>
        /// <returns>Returns mapped model of type AccountProfileModel</returns>
        public static AccountProfileModel ToModel(AccountProfileViewModel viewModel)
        {
            if (viewModel == null)
            {
                return null;
            }

            return new AccountProfileModel()
            {
                AccountProfileID = viewModel.AccountProfileID,
                AccountID = viewModel.AccountID,
                ProfileID = viewModel.ProfileID
            };
        }
    }
}