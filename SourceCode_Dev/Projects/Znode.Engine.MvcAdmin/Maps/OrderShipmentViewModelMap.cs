﻿using System;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    public class OrderShipmentViewModelMap
    {
        /// <summary>
        /// Converts Order model to ordershipmentview model.
        /// </summary>
        /// <param name="model">Model of order model</param>
        /// <returns>OrderShipmentViewModel</returns>
        public static OrderShipmentViewModel ToOrderViewModel(OrderModel model)
        {
            if (!Equals(model, null))
            {
                OrderShipmentViewModel viewModel = new OrderShipmentViewModel();
                viewModel.OrderShipmentId = model.OrderShipmentId;
                viewModel.ShipName = model.ShipName;
                viewModel.ShippingOptionId = model.ShippingOptionId;
                viewModel.ShipToCity = model.ShipToCity;
                viewModel.ShipToCompanyName = model.ShipToCompanyName;
                viewModel.ShipToCountryCode = model.ShipToCountryCode;
                viewModel.ShipToEmail = model.ShipToEmailId;
                viewModel.ShipToFirstName = model.ShipToFirstName;
                viewModel.ShipToLastName = model.ShipToLastName;
                viewModel.ShipToPhoneNumber = model.ShipToPhoneNumber;
                viewModel.ShipToPostalCode = model.ShipToPostalCode;
                viewModel.ShipToStateCode = model.ShipToStateCode;
                viewModel.ShipToStreetAddress1 = model.ShipToStreet;
                viewModel.ShipToStreetAddress2 = model.ShipToStreet1;
                viewModel.AddressId = model.AddressId;
                viewModel.Quantity = model.Quantity;
                viewModel.TaxCost = model.TaxCost;
                viewModel.ShippingCost = model.ShippingCost;
                viewModel.ShippingName = model.ShippingName;
                return viewModel;
            }
            return null;
        }

        /// <summary>
        /// Converts OrderShipment model to OrderShipmentView model.
        /// </summary>
        /// <param name="model">Mdoel of type OrderShipment</param>
        /// <returns>OrderShipmentViewModel</returns>
        public static OrderShipmentViewModel ToViewModel(OrderShipmentModel model)
        {
            if (!Equals(model, null))
            {
                OrderShipmentViewModel viewModel = new OrderShipmentViewModel();
                viewModel.OrderShipmentId = model.OrderShipmentId;
                viewModel.ShipName = string.IsNullOrEmpty(model.ShipName) ? string.Empty : model.ShipName;
                viewModel.ShippingOptionId = model.ShippingOptionId;
                viewModel.ShipToCity = model.ShipToCity;
                viewModel.ShipToCompanyName = model.ShipToCompanyName;
                viewModel.ShipToCountryCode = model.ShipToCountryCode;
                viewModel.ShipToEmail = model.ShipToEmail;
                viewModel.ShipToFirstName = model.ShipToFirstName;
                viewModel.ShipToLastName = model.ShipToLastName;
                viewModel.ShipToPhoneNumber = model.ShipToPhoneNumber;
                viewModel.ShipToPostalCode = model.ShipToPostalCode;
                viewModel.ShipToStateCode = model.ShipToStateCode;
                viewModel.ShipToStreetAddress1 = model.ShipToStreetAddress1;
                viewModel.ShipToStreetAddress2 = model.ShipToStreetAddress2;
                viewModel.AddressId = model.AddressId;
                viewModel.Quantity = model.Quantity;
                viewModel.TaxCost = model.TaxCost;
                viewModel.ShippingCost = model.ShippingCost;
                viewModel.ShippingName = model.ShippingName;
                return viewModel;
            }
            return null;
        }

        /// <summary>
        /// Converts OrderShipmentView model to OrderShipment model.
        /// </summary>
        /// <param name="viewModel">Model of type OrderShipmentViewModel</param>
        /// <returns>OrderShipmentModel</returns>
        public static OrderShipmentModel ToModel(OrderShipmentViewModel viewModel)
        {
            if (!Equals(viewModel, null))
            {
                OrderShipmentModel model = new OrderShipmentModel();
                model.OrderShipmentId = viewModel.OrderShipmentId;
                model.ShipName = viewModel.ShipName;
                model.ShippingOptionId = viewModel.ShippingOptionId;
                model.ShipToCity = viewModel.ShipToCity;
                model.ShipToCompanyName = viewModel.ShipToCompanyName;
                model.ShipToCountryCode = viewModel.ShipToCountryCode;
                model.ShipToEmail = viewModel.ShipToEmail;
                model.ShipToFirstName = viewModel.ShipToFirstName;
                model.ShipToLastName = viewModel.ShipToLastName;
                model.ShipToPhoneNumber = viewModel.ShipToPhoneNumber;
                model.ShipToPostalCode = viewModel.ShipToPostalCode;
                model.ShipToStateCode = viewModel.ShipToStateCode;
                model.ShipToStreetAddress1 = viewModel.ShipToStreetAddress1;
                model.ShipToStreetAddress2 = viewModel.ShipToStreetAddress2;
                model.AddressId = viewModel.AddressId;
                model.Quantity = viewModel.Quantity;
                model.TaxCost = Convert.ToDecimal(viewModel.TaxCost);
                model.ShippingCost = Convert.ToDecimal(viewModel.ShippingCost);
                model.ShippingName = viewModel.ShippingName;
                return model;
            }
            return null;
        }
    }
}