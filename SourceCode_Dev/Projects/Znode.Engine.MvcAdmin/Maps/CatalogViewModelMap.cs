﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    public static class CatalogViewModelMap
    {
        #region Public Methods

        /// <summary>
        /// Maps the Catalog Model to CatalogViewList Model.
        /// </summary>
        /// <param name="models">The model of type CatalogModel.</param>
        /// <returns>Returns the mapped Catalog Model to CatalogViewList Model.</returns>
        public static CatalogViewModel ToViewModel(CatalogModel model)
        {
            if (!Equals(model, null))
            {
                CatalogViewModel viewModel = new CatalogViewModel();
                viewModel.CatalogId = model.CatalogId;
                viewModel.Name = model.Name;
                viewModel.IsDefault = model.IsDefault;
                viewModel.PortalId = model.PortalId;
                return viewModel;
            }
            return null;
        }

        /// <summary>
        /// Maps the CatalogViewModel to CatalogModel.
        /// </summary>
        /// <param name="viewModel">The model of type CatalogViewModel.</param>
        /// <returns>Returns the mapped CatalogViewModel to CatalogModel.</returns>
        public static CatalogModel ToModel(CatalogViewModel viewModel)
        {
            if (!Equals(viewModel, null))
            {
                CatalogModel model = new CatalogModel();
                model.CatalogId = viewModel.CatalogId;
                model.Name = viewModel.Name;
                model.IsActive = viewModel.IsActive;
                model.PortalId = viewModel.PortalId;
                model.PreserveCategories = viewModel.PreserveCategories;
                return model;
            }
            return null;
        }

        /// <summary>
        /// Converts collection of catalog model to catalogListview model.
        /// </summary>
        /// <param name="models">Collection of catalog models.</param>
        /// <returns>CatalogListViewModel</returns>
        public static CatalogListViewModel ToListViewModel(CatalogListModel model, int? pageIndex, int? recordPerPage)
        {
            if (!Equals(model, null))
            {
                var viewModel = new CatalogListViewModel()
                {
                    Catalogs = model.Catalogs.ToList().Select(
                    x => new CatalogViewModel()
                    {
                        CatalogId = x.CatalogId,
                        Name = x.Name,
                        IsActive = x.IsActive
                    }).ToList()
                };

                viewModel.RecordPerPage = recordPerPage ?? 10;
                viewModel.Page = pageIndex ?? 1;
                viewModel.TotalResults = Convert.ToInt32(model.TotalResults);
                return viewModel;
            }
            return null;
        }

        /// <summary>
        /// Converts collection of catalog model to catalogListview model.
        /// </summary>
        /// <param name="model">IEnumerable<CatalogModel> model</param>
        /// <returns>returns CatalogListViewModel</returns>
        public static CatalogListViewModel ToViewModels(IEnumerable<CatalogModel> model)
        {
            if (!Equals(model, null))
            {
                var viewModel = new CatalogListViewModel()
                {
                    Catalogs = model.ToList().Select(

                    x => new CatalogViewModel()
                    {
                        CatalogId = x.CatalogId,
                        ExternalId = x.ExternalId,
                        Name = x.Name,
                        PortalId = x.PortalId,
                    }).ToList()
                };
                return viewModel;
            }
            return null;
        }

        public static List<SelectListItem> GetAvailableProfiles(List<ProfileModel> list, int profileId = 0)
        {
            List<SelectListItem> selectList = new List<SelectListItem>();
            if (!Equals(list, null))
            {
                foreach (var item in list)
                {
                    selectList.Add(new SelectListItem()
                    {

                        Text = item.Name,
                        Value = item.ProfileId.ToString(),
                        Selected = Equals(item.ProfileId, profileId) ? true : false
                    });
                }
                return selectList;
            }
            else
            {
                return new List<SelectListItem>();
            }
        }

        #endregion
    }
}