﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.MvcAdmin.ViewModels;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Helpers;

namespace Znode.Engine.MvcAdmin.Maps
{
    public class ProductCategoryViewModelMap
    {
        /// <summary>
        /// Maps the Product Category model to Product Category View model.
        /// </summary>
        /// <param name="model">Model of type ProductCategoryModel</param>
        /// <returns>Returns the mapped ProductCategory model to ProductCategoryView model.</returns>
        public static ProductCategoryViewModel ToViewModel(ProductCategoryModel model)
        {
            if (Equals(model, null))
            {
                return null;
            }

            var viewModel = new ProductCategoryViewModel()
            {
                ProductCategoryId = model.ProductCategoryId,
                ProductId = model.ProductId,
                CategoryId = model.CategoryId,
                DisplayOrder = Equals(model.DisplayOrder, null) ? MvcAdminConstants.DefaultDisplayOrder : model.DisplayOrder,
                ThemeId = model.ThemeId,
                CssId = model.CssId,
                MasterPageId = model.MasterPageId,
                IsActive = !model.IsActive? model.IsActive:true
            };
            return viewModel;
        }


        /// <summary>
        /// Maps the Product Category model to Product Category View model.
        /// </summary>
        /// <param name="model">Model of type ProductCategoryModel</param>
        /// <returns>Returns the mapped ProductCategory model to ProductCategoryView model.</returns>
        public static ProductCategoryModel ToModel(ProductCategoryViewModel viewModel)
        {
            if (Equals(viewModel, null))
            {
                return null;
            }

            var model = new ProductCategoryModel()
            {
                ProductCategoryId = viewModel.ProductCategoryId,
                ProductId = viewModel.ProductId,
                CategoryId = viewModel.CategoryId,
                DisplayOrder = viewModel.DisplayOrder,
                ThemeId = viewModel.ThemeId,
                CssId = viewModel.CssId,
                MasterPageId = viewModel.MasterPageId,
                IsActive = viewModel.IsActive
            };
            return model;
        }
    }
}