﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    public class PRFTInventoryViewModelMap
    {
        public static PRFTInventoryListViewModel ToListViewModel(PRFTInventoryListModel model)
        {
            if (!Equals(model, null) && !Equals(model.Inventories, null))
            {
                var viewModel = new PRFTInventoryListViewModel()
                {
                    InventoryList = model.Inventories.ToList().Select(
                    x => new PRFTInventoryViewModel()
                    {
                        ProductId = x.ProductId,
                        Name = x.Name,
                        ProductNum = x.ProductNum,
                        Milwaukee = x.Milwaukee,
                        Bloomington = x.Bloomington,
                        Nebraska = x.Nebraska,
                        DesMoines = x.DesMoines,
                        PortalID = (x.PortalID==null)?1:x.PortalID
                    }).ToList()
                };

                viewModel.TotalResults = Convert.ToInt32(model.TotalResults);
                viewModel.Page = Convert.ToInt32(model.PageIndex);
                viewModel.RecordPerPage = Convert.ToInt32(model.PageSize);
                viewModel.TotalPages = Convert.ToInt32(model.TotalPages);
                viewModel.TotalResults = Convert.ToInt32(model.TotalResults);

                return viewModel;
            }
            return new PRFTInventoryListViewModel();
        }
    }
}