﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    public class PRFTCreditApplicationViewModelMap
    {
        public static PRFTCreditApplicationModel ToModel(PRFTCreditApplicationViewModel viewmodel)
        {
            if (viewmodel == null)
            {
                return new PRFTCreditApplicationModel();
            }

            var model = new PRFTCreditApplicationModel
            {
                CreditApplicationID = viewmodel.CreditApplicationID,
                BusinessName = viewmodel.BusinessName,
                BusinessStreet = viewmodel.BusinessStreet,
                BusinessStreet1 = viewmodel.BusinessStreet1,
                BusinessCity = viewmodel.BusinessCity,
                BusinessStateCode = viewmodel.BusinessStateCode,
                BusinessPostalCode = viewmodel.BusinessPostalCode,
                BusinessCountryCode = viewmodel.BusinessCountryCode,
                BusinessPhoneNumber = viewmodel.BusinessPhoneNumber,
                BusinessFaxNumber = viewmodel.BusinessFaxNumber,
                BusinessEmail = viewmodel.BusinessEmail,
                BusinessYearEstablished = viewmodel.BusinessYearEstablished,
                BusinessType = viewmodel.BusinessType,
                IsTaxExempt = viewmodel.IsTaxExempt,
                TaxExemptCertificate = viewmodel.TaxExemptCertificate,
                BillingStreet = viewmodel.BillingStreet,
                BillingStreet1 = viewmodel.BillingStreet1,
                BillingCity = viewmodel.BillingCity,
                BillingStateCode = viewmodel.BillingStateCode,
                BillingPostalCode = viewmodel.BillingPostalCode,
                BillingCountryCode = viewmodel.BillingCountryCode,
                BankName = viewmodel.BankName,
                BankAddress = viewmodel.BankAddress,
                BankPhoneNumber = viewmodel.BankPhoneNumber,
                BankAccountNumber = viewmodel.BankAccountNumber,
                RequestDate = viewmodel.RequestDate,
                IsCreditApproved = viewmodel.IsCreditApproved,
                CreditDays = viewmodel.CreditDays,
                CreditApprovedDate = viewmodel.CreditApprovedDate,
                Custom1 = viewmodel.Custom1,
                Custom2 = viewmodel.Custom2,
                Custom3 = viewmodel.Custom3,
                Custom4 = viewmodel.Custom4,
                Custom5 = viewmodel.Custom5
            };

            foreach (var item in viewmodel.TradeReferences)
            {
                model.TradeReferences.Add(PRFTTradeReferencesViewModelMap.ToModel(item));
            }

            return model;
        }

        public static PRFTCreditApplicationViewModel ToViewModel(PRFTCreditApplicationModel model)
        {
            if (model == null)
            {
                return new PRFTCreditApplicationViewModel();
            }

            var viewmodel = new PRFTCreditApplicationViewModel
            {
                CreditApplicationID = model.CreditApplicationID,
                BusinessName = model.BusinessName,
                BusinessStreet = model.BusinessStreet,
                BusinessStreet1 = model.BusinessStreet1,
                BusinessCity = model.BusinessCity,
                BusinessStateCode = model.BusinessStateCode,
                BusinessPostalCode = model.BusinessPostalCode,
                BusinessCountryCode = model.BusinessCountryCode,
                BusinessPhoneNumber = model.BusinessPhoneNumber,
                BusinessFaxNumber = model.BusinessFaxNumber,
                BusinessEmail = model.BusinessEmail,
                BusinessYearEstablished = model.BusinessYearEstablished,
                BusinessType = model.BusinessType,
                IsTaxExempt = model.IsTaxExempt,
                TaxExemptCertificate = model.TaxExemptCertificate,
                BillingStreet = model.BillingStreet,
                BillingStreet1 = model.BillingStreet1,
                BillingCity = model.BillingCity,
                BillingStateCode = model.BillingStateCode,
                BillingPostalCode = model.BillingPostalCode,
                BillingCountryCode = model.BillingCountryCode,
                BankName = model.BankName,
                BankAddress = model.BankAddress,
                BankPhoneNumber = model.BankPhoneNumber,
                BankAccountNumber = model.BankAccountNumber,
                RequestDate = model.RequestDate,
                IsCreditApproved = model.IsCreditApproved,
                CreditDays = model.CreditDays,
                CreditApprovedDate = model.CreditApprovedDate,
                Custom1 = model.Custom1,
                Custom2 = model.Custom2,
                Custom3 = model.Custom3,
                Custom4 = model.Custom4,
                Custom5 = model.Custom5
            };

            foreach (var item in model.TradeReferences)
            {
                viewmodel.TradeReferences.Add(PRFTTradeReferencesViewModelMap.ToViewModel(item));
            }

            return viewmodel;
        }

        public static PRFTCreditApplicationListViewModel ToListViewModel(PRFTCreditApplicationListModel model)
        {
            if (!Equals(model, null) && !Equals(model.CreditApplications, null))
            {
                var viewModel = new PRFTCreditApplicationListViewModel()
                {
                    CreditApplicationsList = model.CreditApplications.ToList().Select(
                    x => new PRFTCreditApplicationForListViewModel()
                    {
                        CreditApplicationID = x.CreditApplicationID,
                        BusinessName = x.BusinessName,
                        IsTaxExempt = (x.IsTaxExempt!=null && x.IsTaxExempt==true)?"Yes":"No",
                        IsCreditApproved = (x.IsCreditApproved!=null && x.IsCreditApproved==true)?"Yes":"No",
                        RequestDate = x.RequestDate,
                        CreditDays=x.CreditDays
                    }).ToList()
                };

                viewModel.TotalResults = Convert.ToInt32(model.TotalResults);
                viewModel.Page = Convert.ToInt32(model.PageIndex);
                viewModel.RecordPerPage = Convert.ToInt32(model.PageSize);
                viewModel.TotalPages = Convert.ToInt32(model.TotalPages);
                viewModel.TotalResults = Convert.ToInt32(model.TotalResults);

                return viewModel;
            }
            return new PRFTCreditApplicationListViewModel();
        }
    
    }
}