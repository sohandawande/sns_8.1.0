﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    public class ManageSEOViewModelMap
    {
        #region Private Constants
        private const string defaultName = "<Name>"; 
        #endregion

        /// <summary>
        /// To Map model to ViewModel
        /// </summary>
        /// <param name="model">PortalModel model</param>
        /// <returns>returns </returns>
        public static ManageSEOViewModel ToViewModel(PortalModel model)
        {
            return new ManageSEOViewModel()
            {
                PortalId = model.PortalId,
                SeoDefaultProductTitle = (string.IsNullOrEmpty(model.SeoDefaultProductTitle)|| Equals(model.SeoDefaultProductTitle,model.StoreName)) ? defaultName : model.SeoDefaultProductTitle,
                SeoDefaultProductDescription = (string.IsNullOrEmpty(model.SeoDefaultProductDescription) || Equals(model.SeoDefaultProductDescription,model.StoreName)) ? defaultName : model.SeoDefaultProductDescription,
                SeoDefaultProductKeyword = model.SeoDefaultProductKeyword,
                SeoDefaultCategoryTitle = (string.IsNullOrEmpty(model.SeoDefaultCategoryTitle) || Equals(model.SeoDefaultCategoryTitle, model.StoreName)) ? defaultName : model.SeoDefaultCategoryTitle,
                SeoDefaultCategoryDescription = (string.IsNullOrEmpty(model.SeoDefaultCategoryDescription) || Equals(model.SeoDefaultCategoryDescription, model.StoreName)) ? defaultName : model.SeoDefaultCategoryDescription,
                SeoDefaultCategoryKeyword = model.SeoDefaultCategoryKeyword,
                SeoDefaultContentTitle = (string.IsNullOrEmpty(model.SeoDefaultContentTitle) || Equals(model.SeoDefaultContentTitle, model.StoreName)) ? defaultName : model.SeoDefaultContentTitle,
                SeoDefaultContentDescription = (string.IsNullOrEmpty(model.SeoDefaultContentDescription) || Equals(model.SeoDefaultContentDescription, model.StoreName)) ? defaultName : model.SeoDefaultContentDescription,
                SeoDefaultContentKeyword = model.SeoDefaultContentKeyword,
                PortalName = model.StoreName,
            };
        }

        /// <summary>
        /// To map model to view model
        /// </summary>
        /// <param name="model">ProductModel model</param>
        /// <returns>returns view model</returns>
        public static ManageSEOProductViewModel ToViewModel(ProductModel model)
        {
            var viewModel = new ManageSEOProductViewModel()
            {
                ProductId = model.ProductId,
                ProductSpecifications = Equals(model.Specifications, null) ? String.Empty : HttpUtility.HtmlDecode(model.Specifications),
                LongDescription = Equals(model.Description, null) ? String.Empty : HttpUtility.HtmlDecode(model.Description),
                FeaturesDescription = Equals(model.FeaturesDescription, null) ? String.Empty : HttpUtility.HtmlDecode(model.FeaturesDescription),
                ShippingInformation = Equals(model.AdditionalInfo, null) ? String.Empty : HttpUtility.HtmlDecode(model.AdditionalInfo),
                ShortDescription = Equals(model.ShortDescription, null) ? String.Empty : HttpUtility.HtmlDecode(model.ShortDescription),
                SeoDescription = Equals(model.SeoDescription, null) ? string.Empty : HttpUtility.HtmlDecode(model.SeoDescription),
                SeoKeywords = Equals(model.SeoKeywords, null) ? string.Empty : HttpUtility.HtmlDecode(model.SeoKeywords),
                SeoPageUrl = Equals(model.SeoUrl, null) ? string.Empty : model.SeoUrl,
                SeoTitle = Equals(model.SeoTitle, null) ? string.Empty : HttpUtility.HtmlDecode(model.SeoTitle),
                ProductName = Equals(model.Name, null) ? string.Empty : model.Name,
                RedirectUrlInd = model.RedirectUrlInd,
            };
            return viewModel;
        }

        /// <summary>
        ///  To map viewmodel to model
        /// </summary>
        /// <param name="viewModel">ManageSEOProductViewModel viewModel</param>
        /// <returns>returns model</returns>
        public static ProductModel ToModel(ManageSEOProductViewModel viewModel)
        {
            var model = new ProductModel()
            {
                ProductId = Equals(viewModel.ProductId, null) ? 0 : viewModel.ProductId,
                AdditionalInfo = Equals(viewModel.ShippingInformation, null) ? String.Empty : HttpUtility.HtmlEncode(viewModel.ShippingInformation),
                Specifications = Equals(viewModel.ProductSpecifications, null) ? String.Empty : HttpUtility.HtmlEncode(viewModel.ProductSpecifications),
                Description = Equals(viewModel.LongDescription, null) ? String.Empty : HttpUtility.HtmlEncode(viewModel.LongDescription),
                FeaturesDescription = Equals(viewModel.FeaturesDescription, null) ? String.Empty : HttpUtility.HtmlEncode(viewModel.FeaturesDescription),
                ShortDescription = Equals(viewModel.ShortDescription, null) ? String.Empty : HttpUtility.HtmlEncode(viewModel.ShortDescription),
                SeoDescription = Equals(viewModel.SeoDescription, null) ? String.Empty : HttpUtility.HtmlEncode(viewModel.SeoDescription),
                SeoKeywords = Equals(viewModel.SeoKeywords, null) ? String.Empty : HttpUtility.HtmlEncode(viewModel.SeoKeywords),
                SeoTitle = Equals(viewModel.SeoTitle, null) ? String.Empty : HttpUtility.HtmlEncode(viewModel.SeoTitle),
                Name = Equals(viewModel.ProductName, null) ? string.Empty : viewModel.ProductName,
                RedirectUrlInd = viewModel.RedirectUrlInd,
            };
            return model;
        }

        /// <summary>
        /// To map model to view model
        /// </summary>
        /// <param name="model">CategoryModel model</param>
        /// <returns></returns>
        public static ManageSEOCategoryViewModel ToViewModel(CategoryModel model)
        {
            if (Equals(model, null))
            {
                return null;
            }
            var viewModel = new ManageSEOCategoryViewModel()
            {
                CategoryId = model.CategoryId,
                CategoryName = model.Name,
                CategoryTitle = model.Title,
                ShortDescription = model.ShortDescription,
                Description = model.Description,
                SeoDescription = HttpUtility.HtmlDecode(model.SeoDescription),
                SeoKeywords = model.SeoKeywords,
                SeoUrl = model.SeoUrl,
                SeoTitle = model.SeoTitle,
                RedirectUrlInd = model.RedirectUrlInd
            };
            return viewModel;
        }

        /// <summary>
        /// To map viewModel to model
        /// </summary>
        /// <param name="viewModel">ManageSEOCategoryViewModel viewModel</param>
        /// <returns>returns CategoryModel</returns>
        public static CategoryModel ToModel(ManageSEOCategoryViewModel viewModel)
        {
            var model = new CategoryModel()
            {
                CategoryId = Equals(viewModel.CategoryId, null) ? 0 : viewModel.CategoryId,
                Name = Equals(viewModel.CategoryName, null) ? String.Empty : viewModel.CategoryName,
                Title = Equals(viewModel.CategoryTitle, null) ? String.Empty : viewModel.CategoryTitle,
                Description = Equals(viewModel.Description, null) ? String.Empty : viewModel.Description,
                ShortDescription = Equals(viewModel.ShortDescription, null) ? String.Empty : HttpUtility.HtmlEncode(viewModel.ShortDescription),
                SeoDescription = Equals(viewModel.SeoDescription, null) ? String.Empty : viewModel.SeoDescription,
                SeoKeywords = Equals(viewModel.SeoKeywords, null) ? String.Empty : viewModel.SeoKeywords,
                SeoTitle = Equals(viewModel.SeoTitle, null) ? String.Empty : viewModel.SeoTitle,
                SeoUrl = Equals(viewModel.SeoUrl, null) ? String.Empty : viewModel.SeoUrl,
                RedirectUrlInd = Equals(viewModel.RedirectUrlInd, null) ? false : viewModel.RedirectUrlInd,
            };
            return model;
        }

        /// <summary>
        /// Convert list of portal view model to select list item.
        /// </summary>
        /// <param name="model">model to convert.</param>
        /// <returns>select list item.</returns>
        public static List<SelectListItem> ToSelectListItems(IEnumerable<PortalViewModel> model)
        {
            List<SelectListItem> portalItems = new List<SelectListItem>();
            
            if (!Equals(model, null))
            {
                model = model.OrderBy(s => s.StoreName);
                portalItems = (from item in model
                               select new SelectListItem
                               {
                                   Text = item.StoreName,
                                   Value = item.PortalId.ToString()
                               }).ToList();
            }
            portalItems[0].Selected = true;
            return portalItems;
        }
    }
}