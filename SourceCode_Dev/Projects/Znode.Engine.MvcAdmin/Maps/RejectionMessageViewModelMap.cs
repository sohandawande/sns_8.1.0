﻿using System;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    /// <summary>
    /// Map for rejection message.
    /// </summary>
    public static class RejectionMessageViewModelMap
    {
        /// <summary>
        /// Convert RejectionMessageModel to RejectionMessageViewModel.
        /// </summary>
        /// <param name="model">model to convert.</param>
        /// <returns>RejectionMessageViewModel</returns>
        public static RejectionMessageViewModel ToViewModel(RejectionMessageModel model)
        {
            if (!Equals(model, null))
            {
                return new RejectionMessageViewModel()
                {
                    RejectionMessagesId = model.RejectionMessagesId,
                    PortalId = model.PortalId,
                    LocaleId = model.LocaleId,
                    MessageKey = model.MessageKey,
                    MessageValue = model.MessageValue,
                    StoreName=model.StoreName
                };
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Convert RejectionMessageViewModel to RejectionMessageModel.
        /// </summary>
        /// <param name="viewModel">view model to convert.</param>
        /// <returns>RejectionMessageModel</returns>
        public static RejectionMessageModel ToModel(RejectionMessageViewModel viewModel)
        {
            if (!Equals(viewModel, null))
            {
                return new RejectionMessageModel()
                {
                    RejectionMessagesId = viewModel.RejectionMessagesId,
                    PortalId = viewModel.PortalId,
                    LocaleId = viewModel.LocaleId,
                    MessageKey = viewModel.MessageKey,
                    MessageValue = viewModel.MessageValue,
                    StoreName = viewModel.StoreName
                };
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Convert RejectionMessageListModel to RejectionMessageListViewModel.
        /// </summary>
        /// <param name="models">models to convert.</param>
        /// <returns>RejectionMessageListViewModel</returns>
        public static RejectionMessageListViewModel ToListViewModel(RejectionMessageListModel models)
        {
            if (!Equals(models, null) && !Equals(models.RejectionMessages, null))
            {
                var viewModel = new RejectionMessageListViewModel()
                {
                    RejectionMessages = models.RejectionMessages.ToList().Select(
                    model => new RejectionMessageViewModel()
                    {
                        RejectionMessagesId = model.RejectionMessagesId,
                        PortalId = model.PortalId,
                        LocaleId = model.LocaleId,
                        MessageKey = model.MessageKey,
                        MessageValue = model.MessageValue,
                        StoreName = model.StoreName
                    }).ToList()
                };
                viewModel.TotalResults = Convert.ToInt32(models.TotalResults);
                return viewModel;
            }
            else
            {
                return new RejectionMessageListViewModel();
            }
        }
    }
}