﻿using System;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    /// <summary>
    /// Static class for ProductSearchSetting Mapper
    /// </summary>
    public static class ProductSearchSettingViewModelMap
    {
        /// <summary>
        /// Convert ProductLevelSettingListModel to ProductLevelSettingListViewModel
        /// </summary>
        /// <param name="model">ProductLevelSettingListModel model</param>
        /// <returns>ProductLevelSettingListModel</returns>
        public static ProductLevelSettingListViewModel ToListViewModel(ProductLevelSettingListModel model)
        {
            if (!Equals(model, null) && !Equals(model.Products, null))
            {
                var viewModel = new ProductLevelSettingListViewModel()
                {
                    Products = model.Products.ToList().Select(
                x => new ProductLevelSettingViewModel()
                {
                    ProductId = x.ProductId,
                    Name = x.Name,
                    Boost = Equals(x.Boost, null) ? HelperMethods.FormatPrice(1.00) : HelperMethods.FormatPrice(x.Boost),
                    ProductNumber = x.ProductNumber,
                    ProductTypeId = x.ProductTypeId,
                    CatalogId = x.CatalogId,
                    categoryId = x.categoryId,
                    SKU = x.SKU,
                    ManufacturerId = x.ManufacturerId
                }).ToList()
                };
                viewModel.TotalResults = Convert.ToInt32(model.TotalResults);
                viewModel.Page = Convert.ToInt32(model.PageIndex);
                viewModel.RecordPerPage = Convert.ToInt32(model.PageSize);
                viewModel.TotalPages = Convert.ToInt32(model.TotalPages);
                return viewModel;
            }
            else
            {
                return new ProductLevelSettingListViewModel();
            }
        }

        /// <summary>
        /// Convert CategoryLevelSettingListModel to CategoryLevelSettingListViewModel
        /// </summary>
        /// <param name="model">CategoryLevelSettingListModel model model</param>
        /// <returns>CategoryLevelSettingListViewModel</returns>
        public static CategoryLevelSettingListViewModel ToListViewModel(CategoryLevelSettingListModel model)
        {
            if (!Equals(model, null) && !Equals(model.Categories, null))
            {
                var viewModel = new CategoryLevelSettingListViewModel()
                {
                    Category = model.Categories.ToList().Select(
                x => new CategoryLevelSettingViewModel()
                {
                    ProductId = x.ProductId,
                    Name = x.Name,
                    CategoryName = x.CategoryName,
                    Boost = Equals(x.Boost, null) ? HelperMethods.FormatPrice(1.00) : HelperMethods.FormatPrice(x.Boost),
                    ProductNumber = x.ProductNumber,
                    ProductTypeId = x.ProductTypeId,
                    CatalogId = x.CatalogId,
                    categoryId = x.categoryId,
                    ProductCategoryId = x.ProductCategoryId,
                    SKU = x.SKU,
                    ManufacturerId = x.ManufacturerId
                }).ToList()
                };
                viewModel.TotalResults = Convert.ToInt32(model.TotalResults);
                viewModel.Page = Convert.ToInt32(model.PageIndex);
                viewModel.RecordPerPage = Convert.ToInt32(model.PageSize);
                viewModel.TotalPages = Convert.ToInt32(model.TotalPages);
                return viewModel;
            }
            else
            {
                return new CategoryLevelSettingListViewModel();
            }
        }

        /// <summary>
        /// Convert FieldLevelSettingListModel into FieldLevelSettingListViewModel
        /// </summary>
        /// <param name="model">FieldLevelSettingListModel</param>
        /// <returns>FieldLevelSettingListViewModel</returns>
        public static FieldLevelSettingListViewModel ToListViewModel(FieldLevelSettingListModel model)
        {
            if (!Equals(model, null) && !Equals(model.FieldLevelSettings, null))
            {
                var viewModel = new FieldLevelSettingListViewModel()
                {
                    Fields = model.FieldLevelSettings.ToList().Select(
                x => new FieldLevelSettingViewModel()
                {
                    FieldId = x.Id,
                    PropertyName = x.DocumentName,
                    Boost = Equals(x.Boost, null) ? HelperMethods.FormatPrice(1.00) : HelperMethods.FormatPrice(x.Boost),
                }).ToList()
                };
                viewModel.TotalResults = Convert.ToInt32(model.TotalResults);
                viewModel.Page = Convert.ToInt32(model.PageIndex);
                viewModel.RecordPerPage = Convert.ToInt32(model.PageSize);
                viewModel.TotalPages = Convert.ToInt32(model.TotalPages);
                return viewModel;
            }
            else
            {
                return new FieldLevelSettingListViewModel();
            }
        }

        /// <summary>
        /// Convert Boost values and ids into BoostSearchSettingModel type model.
        /// </summary>
        /// <param name="ids">String Array of ids for Product,Category and Fild</param>
        /// <param name="values">String Array of Values for Product,Category and Fild</param>
        /// <param name="name">Type Name</param>
        /// <returns>returns BoostSearchSettingModel</returns>
        public static BoostSearchSettingModel ToBoostModel(string[] ids, string[] values, string name)
        {
            BoostSearchSettingModel model = new BoostSearchSettingModel();
            ProductLevelSettingListViewModel productList = new ProductLevelSettingListViewModel();
            CategoryLevelSettingListViewModel catagoryList = new CategoryLevelSettingListViewModel();
            FieldLevelSettingListViewModel fieldslist = new FieldLevelSettingListViewModel();

            string properyName = string.Empty;
            switch (name.ToLower())
            {
                case MvcAdminConstants.product:
                    productList = (ProductLevelSettingListViewModel)HttpContext.Current.Session[MvcAdminConstants.BoostValuesListCache];
                    break;
                case MvcAdminConstants.category:
                    catagoryList = (CategoryLevelSettingListViewModel)HttpContext.Current.Session[MvcAdminConstants.BoostValuesListCache];
                    break;
                case MvcAdminConstants.fields:
                    fieldslist = (FieldLevelSettingListViewModel)HttpContext.Current.Session[MvcAdminConstants.BoostValuesListCache];
                    break;
            }

            int counter = 0;
            double checkBoost = 0;
            ids.ToList().ForEach(x =>
            {
                if (name.Equals(MvcAdminConstants.product))
                    checkBoost = Convert.ToDouble(productList.Products.FirstOrDefault(y => y.ProductId.ToString().Equals(x)).Boost);
                else if (name.Equals(MvcAdminConstants.category))
                    checkBoost = Convert.ToDouble(catagoryList.Category.FirstOrDefault(y => y.ProductCategoryId.ToString().Equals(x)).Boost);
                else if (name.Equals(MvcAdminConstants.fields))
                    checkBoost = Convert.ToDouble(fieldslist.Fields.FirstOrDefault(y => y.FieldId.ToString().Equals(x)).Boost);

                if (!Equals(Convert.ToDouble(values[counter]), checkBoost))
                {
                    model.BoostCollection.Add(int.Parse(x), Convert.ToDouble(values[counter]));
                }
                counter++;
            });
            model.Name = name;
            return model;
        }

    }
}