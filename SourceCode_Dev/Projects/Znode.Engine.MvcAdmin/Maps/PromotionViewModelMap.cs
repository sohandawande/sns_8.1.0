﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    public static class PromotionViewModelMap
    {
        /// <summary>
        /// Converting Promotion model to Promotion View Model
        /// </summary>
        /// <param name="models">The model of promotion </param>
        /// <returns>PromotionsViewModel</returns>
        public static PromotionsViewModel ToViewModel(PromotionModel model)
        {
            PortalsClient _portalsClient = new PortalsClient();
            if (!Equals(model, null))
            {
                var viewModel = new PromotionsViewModel()
                {
                    PromotionId = model.PromotionId,
                    Name = model.Name,
                    DiscountAmount = model.Discount,
                    DiscountPercent = model.Discount,
                    PromoCode = model.PromoCode,
                    Description = model.Description,
                    PortalId = model.PortalId,
                    CatalogId = model.CatalogId,
                    ManufacturerId = model.ManufacturerId,
                    PromotionTypeId = model.PromotionTypeId,
                    RequiredProductId = model.RequiredProductId,
                    DiscountedProductId = model.DiscountedProductId,
                    CategoryId = model.CategoryId,
                    OrderMinimum = HelperMethods.FormatPrice(model.MinimumOrderAmount),
                    ProfileId = model.ProfileId,
                    DiscountedProductQuantity = model.DiscountedProductQuantity,
                    QuantityMinimum = model.RequiredProductMinimumQuantity,
                    CallForPriceMessage = model.PromotionMessage,
                    DisplayOrder = model.DisplayOrder,
                    CouponInd = model.HasCoupon,
                    EnableCouponUrl = (!Equals(model.EnableCouponUrl, null) ? model.EnableCouponUrl.Value : false),
                    StartDate = HelperMethods.ViewDateFormat(model.StartDate),
                    EndDate = HelperMethods.ViewDateFormat(model.EndDate),
                    Coupons = new CouponCodeViewModel()
                    {
                        CouponCode = model.CouponCode,
                        PromotionMessage = model.PromotionMessage,
                        CouponQuantityAvailable = model.CouponQuantityAvailable,
                    },
                    IsCouponAllowedWithOtherCoupons = model.IsCouponAllowedWithOtherCoupons,
                };

                //if (!Equals(viewModel.PortalId, null))
                //{
                //    viewModel.StoreAllowsMultipleCoupon = _portalsClient.GetPortal(viewModel.PortalId.Value).IsMultipleCouponAllowed;
                //}

                return viewModel;
            }
            else
            {
                return new PromotionsViewModel();
            }
        }

        /// <summary>
        /// Converting Promotion View model to Promotion Model
        /// </summary>
        /// <param name="models">The view model of promotion</param>
        /// <returns>PromotionModel</returns>
        public static PromotionModel ToModel(PromotionsViewModel viewModel)
        {
            if (Equals(viewModel, null))
            {
                return null;
            }

            var model = new PromotionModel()
            {
                PromotionId = viewModel.PromotionId,
                Name = viewModel.Name,
                Description = viewModel.Description,
                DisplayOrder = viewModel.DisplayOrder.Value,
                StartDate = Convert.ToDateTime(viewModel.StartDate),
                EndDate = Convert.ToDateTime(viewModel.EndDate),
                PromoCode = viewModel.PromoCode,
                PromotionTypeId = viewModel.PromotionTypeId,
                PortalId = viewModel.PortalId,
                CatalogId = viewModel.CatalogId,
                CategoryId = viewModel.CategoryId,
                ProfileId = viewModel.ProfileId,
                SkuId = viewModel.SkuId,
                DiscountedProductQuantity = viewModel.DiscountedProductQuantity,
                RequiredProductMinimumQuantity = viewModel.QuantityMinimum,
                ManufacturerId = viewModel.ManufacturerId,
                DiscountedProductId = viewModel.DiscountedProductId,
                RequiredProductId = viewModel.RequiredProductId,
                MinimumOrderAmount = viewModel.OrderMinimum,
                PromotionMessage = viewModel.Coupons.PromotionMessage,
                CouponCode = viewModel.Coupons.CouponCode,
                HasCoupon = (!Equals(viewModel.CouponInd.Value, null) ? viewModel.CouponInd.Value : false),
                EnableCouponUrl = viewModel.EnableCouponUrl,
                CouponQuantityAvailable = viewModel.Coupons.CouponQuantityAvailable,
                IsCouponAllowedWithOtherCoupons = viewModel.IsCouponAllowedWithOtherCoupons
            };

            if (!Equals(viewModel.DiscountAmount, null))
            {
                model.Discount = viewModel.DiscountAmount.Value;
            }
            else if (!Equals(viewModel.DiscountPercent, null))
            {
                model.Discount = viewModel.DiscountPercent.Value;
            }

            if (!Equals(viewModel.Coupons.PromotionMessage, null))
            {
                model.PromotionMessage = viewModel.Coupons.PromotionMessage;
            }
            else if (!Equals(viewModel.CallForPriceMessage, null))
            {
                model.PromotionMessage = viewModel.CallForPriceMessage;
            }

            return model;
        }

        /// <summary>
        /// This method returns the portals List
        /// </summary>
        /// <param name="model">The parameter is Portal model</param>
        /// <returns>list of all protals</returns>
        public static List<SelectListItem> ToListItems(IEnumerable<PortalModel> model)
        {
            List<SelectListItem> portalItems = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                portalItems = (from item in model
                               select new SelectListItem
                               {
                                   Text = item.StoreName,
                                   Value = item.PortalId.ToString()
                               }).ToList();
            }
            //portalItems[0].Selected = true;
            return portalItems;
        }

        /// <summary>
        /// This method returns the promotion types List
        /// </summary>
        /// <param name="model">The parameter is Promotion Type Model</param>
        /// <returns>list of all promotion types</returns>
        public static List<SelectListItem> ToListItems(IEnumerable<PromotionTypeModel> model)
        {
            List<SelectListItem> promotionTypeItem = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                promotionTypeItem = (from item in model
                                     select new SelectListItem
                                     {
                                         Text = item.Name,
                                         Value = item.PromotionTypeId.ToString()
                                     }).ToList();
            }
            return promotionTypeItem;
        }

        /// <summary>
        /// This method returns the manufacturers List
        /// </summary>
        /// <param name="model">The parameter is Manufacturer Model</param>
        /// <returns>list of all manufacturers</returns>
        public static List<SelectListItem> ToListItems(IEnumerable<ManufacturerModel> model)
        {
            List<SelectListItem> manufacturerItems = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                manufacturerItems = (from item in model
                                     select new SelectListItem
                                     {
                                         Text = item.Name,
                                         Value = item.ManufacturerId.ToString()
                                     }).ToList();
            }
            return manufacturerItems;
        }

        /// <summary>
        /// This method returns the catalogs List
        /// </summary>
        /// <param name="model">The parameter is Catalog Model</param>
        /// <returns>list of all catalogs</returns>
        public static List<SelectListItem> ToListItems(IEnumerable<CatalogModel> model, bool isIncludeDefaultItem)
        {
            List<SelectListItem> catalogItems = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                catalogItems = (from item in model
                                select new SelectListItem
                                {
                                    Text = item.Name,
                                    Value = item.CatalogId.ToString(),
                                }).ToList();
                if (isIncludeDefaultItem)
                {
                    catalogItems.Insert(0, new SelectListItem { Text = MvcAdminConstants.AllKey, Value = MvcAdminConstants.IndexZero });
                }
            }
            return catalogItems;
        }

        /// <summary>
        /// This method returns the category List
        /// </summary>
        /// <param name="model">The parameter is Catalog Model</param>
        /// <returns>list of all categories</returns>
        public static List<SelectListItem> ToListItems(IEnumerable<CategoryModel> model)
        {
            List<SelectListItem> categoryItems = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                categoryItems = (from item in model
                                 select new SelectListItem
                                 {
                                     Text = item.Name,
                                     Value = item.CategoryId.ToString(),
                                 }).ToList();
            }
            return categoryItems;
        }

        /// <summary>
        /// This method returns the profile List
        /// </summary>
        /// <param name="model">The parameter is Profile Model</param>
        /// <returns>list of all profiles</returns>
        public static List<SelectListItem> ToListItems(IEnumerable<ProfileModel> model)
        {
            List<SelectListItem> profileItems = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                profileItems = (from item in model
                                select new SelectListItem
                                {
                                    Text = item.Name,
                                    Value = item.ProfileId.ToString(),
                                }).ToList();
            }
            return profileItems;
        }

        /// <summary>
        /// This method returns the promotion types List
        /// </summary>
        /// <param name="model">The parameter is Promotion Type Model</param>
        /// <returns>list of all promotion types</returns>
        public static List<SelectListItem> ToListItems(IEnumerable<SkuModel> model)
        {
            List<SelectListItem> SKUItem = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                SKUItem = (from item in model
                           select new SelectListItem
                           {
                               Text = item.Sku,
                               Value = item.SkuId.ToString()
                           }).ToList();
            }
            return SKUItem;
        }


        /// <summary>
        /// This method returns the Promotion List Model
        /// </summary>
        /// <param name="models">IEnumerable type list of Promotion Model</param>
        /// <returns>List of type Promotion</returns>
        public static PromotionsListViewModel ToListViewModel(PromotionListModel listModels)
        {
            if (!Equals(listModels, null) && !Equals(listModels.Promotions, null))
            {
                var viewModel = new PromotionsListViewModel()
                {
                    Promotions = listModels.Promotions.Select(
                    model => new PromotionsViewModel()
                    {
                        PromotionId = model.PromotionId,
                        Name = model.Name,
                        StoreName = model.StoreName,
                        Profile = model.Profile,
                        PromotionType = model.PromotionType.Name,
                        DiscountAmount = model.Discount,
                        DiscountPercent = model.Discount,
                        DisplayOrder = model.DisplayOrder,
                        StartDate = HelperMethods.ViewDateFormat(model.StartDate),
                        EndDate = HelperMethods.ViewDateFormat(model.EndDate),
                        Coupons = new CouponCodeViewModel
                        {
                            CouponCode = model.CouponCode
                        }
                    }).ToList()
                };
                viewModel.Page = Convert.ToInt32(listModels.PageIndex);
                viewModel.RecordPerPage = Convert.ToInt32(listModels.PageSize);
                viewModel.TotalPages = Convert.ToInt32(listModels.TotalPages);
                viewModel.TotalResults = Convert.ToInt32(listModels.TotalResults);
                return viewModel;
            }
            else
            {
                return new PromotionsListViewModel();
            }
        }

        /// <summary>
        /// This method returns the Product List Model
        /// </summary>
        /// <param name="models">IEnumerable type list of Product Model</param>
        /// <returns>List of Products</returns>
        public static ProductListViewModel ToListViewModel(IEnumerable<ProductModel> listModels)
        {

            if (Equals(listModels, null))
            {
                return null;
            }

            var viewModel = new ProductListViewModel()
            {
                Products = listModels.ToList().Select(
                x => new ProductViewModel()
                {
                    ProductId = x.ProductId,
                    Name = x.Name,
                    Sku = x.ProductNumber,
                    ImageSmallThumbnailPath = x.ImageSmallThumbnailPath,
                    ProductCode = x.ProductNumber,
                }).ToList()
            };
            return viewModel;
        }
    }
}