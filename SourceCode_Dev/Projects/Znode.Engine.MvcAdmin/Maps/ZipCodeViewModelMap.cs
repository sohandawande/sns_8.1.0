﻿using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    /// <summary>
    /// Mapper for Zip Code View Model
    /// </summary>
    public class ZipCodeViewModelMap
    {
        /// <summary>
        /// Converts ZipCodeListModel to ZipCodeListViewModel
        /// </summary>
        /// <param name="models">ZipCodeListModel</param>
        /// <returns>Returns ZipCodeListViewModel</returns>
        public static ZipCodeListViewModel ToListViewModel(ZipCodeListModel models)
        {
            if (models != null && models.ZipCode.Count() > 0)
            {
                var viewModel = new ZipCodeListViewModel()
                {
                    ZipCodes = models.ZipCode.ToList().Select(
                    x => new ZipCodeViewModel()
                    {
                        CountyName = x.CountyName,
                        StateAbbr = x.StateAbbr,
                        StateFips = x.StateFips,
                        ZipCodeId = x.ZipCodeId,
                        CountyFips = x.CountyFips
                    }).ToList()
                };             

                return viewModel;
            }
            return new ZipCodeListViewModel();
        }
    }
}