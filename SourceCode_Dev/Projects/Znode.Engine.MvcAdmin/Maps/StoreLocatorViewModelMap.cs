﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    public static class StoreLocatorViewModelMap
    {
        /// <summary>
        /// Converts Store model to Store view model
        /// </summary>
        /// <param name="model">StoreModel</param>
        /// <returns>StoreLocatorViewModel</returns>
        public static StoreLocatorViewModel ToViewModel(StoreModel model)
        {
            if (!Equals(model, null))
            {
                ImageHelper imageHelper = new ImageHelper();
                return new StoreLocatorViewModel()
                {
                    StoreID = model.StoreID,
                    PortalID = model.PortalID,
                    AccountID = model.AccountID,
                    Name = model.Name,
                    Address1 = model.Address1,
                    Address2 = model.Address2,
                    Address3 = model.Address3,
                    City = model.City,
                    ContactName = model.ContactName,
                    Custom1 = model.Custom1,
                    Custom2 = model.Custom2,
                    Custom3 = model.Custom3,
                    DisplayOrder = model.DisplayOrder,
                    Fax = model.Fax,
                    Phone = model.Phone,
                    State = model.State,
                    Zip = model.Zip,
                    ActiveInd = model.ActiveInd,
                    ImageFile = model.ImageFile,
                    ImageLargePath = HelperMethods.GetImageRelativePath(imageHelper.GetImageHttpPathLarge(model.ImageFile)),                  
                };
            }
            return new StoreLocatorViewModel();
        }

        /// <summary>
        /// Converts Store List model to Store list view model
        /// </summary>
        /// <param name="models">StoreListModel</param>
        /// <returns>StoreLocatorListViewModel</returns>
        public static StoreLocatorListViewModel ToListViewModel(StoreListModel models)
        {
            if (!Equals(models, null) && !Equals(models.Stores, null))
            {
                var viewModel = new StoreLocatorListViewModel()
                {
                    StoreLocators = models.Stores.ToList().Select(
                    x => new StoreLocatorViewModel()
                    {
                        StoreID = x.StoreID,
                        PortalID = x.PortalID,
                        AccountID = x.AccountID,
                        Name = x.Name,
                        Address1 = x.Address1,
                        Address2 = x.Address2,
                        Address3 = x.Address3,
                        City = x.City,
                        ContactName = x.ContactName,
                        Custom1 = x.Custom1,
                        Custom2 = x.Custom2,
                        Custom3 = x.Custom3,
                        DisplayOrder = x.DisplayOrder,
                        Fax = x.Fax,
                        Phone = x.Phone,
                        State = x.State,
                        Zip = x.Zip,
                        ActiveInd = x.ActiveInd,
                        ImageFile = x.ImageFile,
                        
                    }).ToList()
                };

                viewModel.TotalResults = Convert.ToInt32(models.TotalResults);

                return viewModel;
            }
            return new StoreLocatorListViewModel();
        }

        /// <summary>
        /// Model Mapper for Store
        /// </summary>
        /// <param name="model">StoreViewModel model</param>
        /// <returns>StoreModel</returns>
        public static StoreModel ToModel(StoreLocatorViewModel model)
        {
            if (!Equals(model, null))
            {
                return new StoreModel()
                {
                    StoreID = model.StoreID,
                    PortalID = model.PortalID,
                    AccountID = model.AccountID,
                    Name = model.Name,
                    Address1 = model.Address1,
                    Address2 = model.Address2,
                    Address3 = model.Address3,
                    City = model.City,
                    ContactName = model.ContactName,
                    Custom1 = model.Custom1,
                    Custom2 = model.Custom2,
                    Custom3 = model.Custom3,
                    DisplayOrder = model.DisplayOrder,
                    Fax = model.Fax,
                    Phone = model.Phone,
                    State = model.State,
                    Zip = model.Zip,
                    ActiveInd = model.ActiveInd,
                    ImageFile = model.ImageFile,

                };
            }

            return new StoreModel();
        }

        /// <summary>
        /// Model Mapper for Attributes list in dropdown.
        /// </summary>
        /// <param name="model">IEnumerable PortalModel</param>
        /// <returns>List of SelectListItem></returns>
        public static List<SelectListItem> ToListItems(IEnumerable<PortalModel> model)
        {
            List<SelectListItem> storeItems = new List<SelectListItem>();

            if (!Equals(model, null))
            {
                storeItems = (from item in model
                              select new SelectListItem
                              {
                                  Text = item.StoreName,
                                  Value = item.PortalId.ToString(),
                              }).ToList();
            }
            return storeItems;
        }
    }


}