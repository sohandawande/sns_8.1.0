﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    /// <summary>
    /// Static Class for Gift Card Mapper
    /// </summary>
    public static class GiftCardViewModelMap
    {
        /// <summary>
        /// Convert GiftCardViewModel to GiftCardModel
        /// </summary>
        /// <param name="giftCardViewModel">GiftCardViewModel giftCardViewModel</param>
        /// <returns>GiftCardModel</returns>
        public static GiftCardModel ToModel(GiftCardViewModel giftCardViewModel)
        {
            if (Equals(giftCardViewModel, null))
            {
                return null;
            }

            return new GiftCardModel()
            {
                Name = giftCardViewModel.Name,
                Amount = giftCardViewModel.Amount,
                CreateDate = Convert.ToDateTime(giftCardViewModel.CreateDate),
                ExpirationDate =Convert.ToDateTime(giftCardViewModel.ExpirationDate),
                PortalId = giftCardViewModel.PortalId,
                AccountId = giftCardViewModel.AccountId,
                GiftCardId = giftCardViewModel.GiftCardId,
                CreatedBy = giftCardViewModel.CreatedBy,
                CardNumber = giftCardViewModel.CardNumber,
                DisplayAmount=MvcAdminUnits.GetCurrencyValue(giftCardViewModel.Amount.GetValueOrDefault()),
                OrderId=giftCardViewModel.OrderId
            };
        }

        /// <summary>
        /// Convert GiftCardModel to GiftCardViewModel
        /// </summary>
        /// <param name="giftCardModel">GiftCardModel giftCardModel</param>
        /// <returns>GiftCardViewModel</returns>
        public static GiftCardViewModel ToViewModel(GiftCardModel giftCardModel)
        {
            if (Equals(giftCardModel, null))
            {
                return null;
            }
            return new GiftCardViewModel()
            {
                Name = giftCardModel.Name,
                Amount = HelperMethods.FormatPrice(giftCardModel.Amount),
                CreateDate = HelperMethods.ViewDateFormat(giftCardModel.CreateDate),
                ExpirationDate =Equals(giftCardModel.ExpirationDate,null)?null: HelperMethods.ViewDateFormat(giftCardModel.ExpirationDate.Value),
                PortalId = giftCardModel.PortalId,
                AccountId = giftCardModel.AccountId,
                GiftCardId = giftCardModel.GiftCardId,
                CreatedBy = giftCardModel.CreatedBy,
                CardNumber = giftCardModel.CardNumber
            };
        }

        /// <summary>
        /// Converts GiftCardModel to GiftCardListViewModel
        /// </summary>
        /// <param name="models">IEnumerable</param>
        /// <returns>GiftCardListViewModel</returns>
        public static GiftCardListViewModel ToListViewModel(GiftCardListModel model)
        {
            if (!Equals(model, null) && !Equals(model.GiftCards, null))
            {
                var viewModel = new GiftCardListViewModel()
              {
                  GiftCards = model.GiftCards.ToList().Select(
                  x => new GiftCardViewModel()
                  {
                      Name = x.Name,
                      Amount = HelperMethods.FormatPrice(x.Amount),
                      DisplayAmount =Equals(x.Amount,null)?string.Empty: MvcAdminUnits.GetCurrencyValue(x.Amount.Value),
                      GiftCardId = x.GiftCardId,
                      CardNumber = x.CardNumber,
                      CreateDate = HelperMethods.ViewDateFormat(x.CreateDate),
                      ExpirationDate = HelperMethods.ViewDateFormat(x.ExpirationDate.Value),
                      AccountId=x.AccountId
                  }).ToList()
              };
                viewModel.TotalResults = Convert.ToInt32(model.TotalResults);
                viewModel.Page = Convert.ToInt32(model.PageIndex);
                viewModel.RecordPerPage = Convert.ToInt32(model.PageSize);
                viewModel.TotalPages = Convert.ToInt32(model.TotalPages);
                viewModel.TotalResults = Convert.ToInt32(model.TotalResults);
                return viewModel;
            }
            else
            {
                return new GiftCardListViewModel();
            }
        }

        /// <summary>
        /// This method returns the List of Portals
        /// </summary>
        /// <param name="model">IEnumerable PortalModel model</param>
        /// <returns>List of Portals</returns>
        public static List<SelectListItem> ToListItems(IEnumerable<PortalModel> model)
        {
            List<SelectListItem> portalItems = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                portalItems = (from item in model
                               select new SelectListItem
                               {
                                   Text = item.StoreName,
                                   Value = item.PortalId.ToString()
                               }).ToList();
            }
            return portalItems;
        }
    }
}
