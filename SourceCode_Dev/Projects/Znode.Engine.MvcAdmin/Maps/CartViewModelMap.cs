﻿using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    public static class CartViewModelMap
    {
        /// <summary>
        /// Maps ShoppingCartModel to CartViewModel.
        /// </summary>
        /// <param name="model">Model of type ShoppingCartModel</param>
        /// <returns>Reurns mapped CartViewModel from ShoppingCartModel.</returns>
        public static CartViewModel ToViewModel(ShoppingCartModel model)
        {
            if (!Equals(model, null))
            {
                var viewModel = new CartViewModel
                {
                    GiftCardApplied = model.GiftCardApplied,
                    GiftCardAmount = model.GiftCardAmount,
                    GiftCardMessage = model.GiftCardMessage,
                    GiftCardNumber = model.GiftCardNumber,
                    GiftCardValid = model.GiftCardValid,
                    GiftCardBalance = model.GiftCardBalance,
                    CookieId = model.CookieId,
                    SubTotal = model.SubTotal,
                    TaxCost = model.TaxCost,
                    Discount = model.Discount,
                    TaxRate = model.TaxRate,
                    Total = model.Total,
                    Vat = model.Vat
                };

                foreach (var item in model.ShoppingCartItems)
                {
                    viewModel.Items.Add(CartItemViewModelMap.ToViewModel(item));
                }

                foreach(CouponModel coupon in model.Coupons)
                {
                    viewModel.Coupons.Add(CouponViewModelMap.ToViewModel(coupon));
                }

                viewModel.Count = model.ShoppingCartItems.Sum(x => x.Quantity);

                viewModel.ShippingCost = model.ShoppingCartItems.Sum(itemModel => itemModel.ShippingCost) +
                                         model.OrderLevelShipping - model.Shipping.ShippingDiscount;

                return viewModel;
            }
            return null;
        }

        /// <summary>
        /// Maps the CartViewModel to ShoppingCartModel.
        /// </summary>
        /// <param name="viewmodel">Model of type CartViewModel</param>
        /// <returns>Returns mapped ShoppingCartModel from CartViewModel.</returns>
        public static ShoppingCartModel ToModel(CartViewModel viewmodel)
        {
            if (!Equals(viewmodel, null))
            {
                var model = new ShoppingCartModel
                {
                    CookieId = viewmodel.CookieId,
                    SubTotal = viewmodel.SubTotal,
                    TaxCost = viewmodel.TaxCost,
                    Discount = viewmodel.Discount,
                    TaxRate = viewmodel.TaxRate,
                    Total = viewmodel.Total,
                    Vat = viewmodel.Vat
                };

                foreach (var item in viewmodel.Items)
                {
                    model.ShoppingCartItems.Add(CartItemViewModelMap.ToModel(item));
                }

                return model;
            }
            return null;
        }

        /// <summary>
        /// Maps AccountModel and PurchaseOrderNumber details to ShoppingCartModel.
        /// </summary>
        /// <param name="account">Model of type AccountModel having details of the customer placing order</param>
        /// <param name="shoppingCart">Model of type ShoppingCartModel having details of the shopping cart</param>
        /// <param name="purchaseOrderNumber">String purchaseOrderNumber of the order being placed</param>
        /// <returns>Returns mapped ShoppingCartModel with account and purchase order details.</returns>
        public static ShoppingCartModel AddAccountDetailsToShoppingCartModel(AccountModel account, ShoppingCartModel shoppingCart, string purchaseOrderNumber)
        {
            if (!Equals(shoppingCart, null))
            {
                shoppingCart.Account = account;
                shoppingCart.Gst = 0;
                shoppingCart.Hst = 0;
                shoppingCart.Pst = 0;
                shoppingCart.Vat = 0;
                shoppingCart.PurchaseOrderNumber = purchaseOrderNumber;
                return shoppingCart;
            }
            return null;
        }
    }
}