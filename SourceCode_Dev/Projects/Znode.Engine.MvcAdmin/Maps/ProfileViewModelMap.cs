﻿using System;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    public static class ProfileViewModelMap
    {
        /// <summary>
        /// Converts profile view model to profile model   
        /// </summary>
        /// <param name="model">Model of type profileview model</param>
        /// <returns>Returns mapped model of type profilemodel</returns>
        public static ProfileModel ToModel(ProfileViewModel viewModel)
        {
            if (viewModel == null)
            {
                return null;
            }

            return new ProfileModel()
            {
                ProfileId = viewModel.ProfileId,
                Name = viewModel.Name,
                Weighting = viewModel.Weighting,
                ShowPricing = viewModel.ProductPrice,
                UseWholesalePricing = viewModel.WholeSalePrice,
                TaxExempt = viewModel.TaxExempt,
                ShowOnPartnerSignup = viewModel.Affiliate,
                ExternalId = viewModel.ExternalId
            };
        }

        /// <summary>
        /// Converts profile model to profile view model  
        /// </summary>
        /// <param name="model">Model of type profile model</param>
        /// <returns>Returns mapped model of type profileviewmodel</returns>
        public static ProfileViewModel ToViewModel(ProfileModel model)
        {
            if (model == null)
            {
                return null;
            }

            return new ProfileViewModel()
            {
                ProfileId = model.ProfileId,
                Name = model.Name,
                Weighting = model.Weighting,
                ProductPrice = model.ShowPricing,
                WholeSalePrice = model.UseWholesalePricing,
                TaxExempt = model.TaxExempt,
                Affiliate = model.ShowOnPartnerSignup,
                ExternalId = model.ExternalId
            };
        }

        /// <summary>
        /// Converts collection of profile model to profilelistview model.
        /// </summary>
        /// <param name="models">Collection of profile models</param>
        /// <returns>Returns mapped model of type profilelistviewmodel</returns>
        public static ProfileListViewModel ToListViewModel(ProfileListModel model, int? pageIndex, int? recordPerPage)
        {
            if (!Equals(model, null) && !Equals(model.Profiles, null))
            {
                var viewModel = new ProfileListViewModel()
                {
                    Profiles = model.Profiles.ToList().Select(
                    x => new ProfileViewModel()
                    {
                        AccountProfileID = x.AccountProfileID,
                        ProfileId = x.ProfileId,
                        Name = x.Name,
                        Weighting = x.Weighting,
                        ProductPrice = x.ShowPricing,
                        WholeSalePrice = x.UseWholesalePricing,
                        TaxExempt = x.TaxExempt,
                        Affiliate = x.ShowOnPartnerSignup,
                        ShowPricing = x.ShowPricing
                    }).ToList()
                };

                viewModel.RecordPerPage = recordPerPage ?? 10;
                viewModel.Page = pageIndex ?? 1;
                viewModel.TotalResults = Convert.ToInt32(model.TotalResults);
                return viewModel;
            }
            return new ProfileListViewModel();
        }
    }
}