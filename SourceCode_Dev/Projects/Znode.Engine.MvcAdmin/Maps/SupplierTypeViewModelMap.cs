﻿using System;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    /// <summary>
    /// Static Class for SupplierType Mapper
    /// </summary>
    public static class SupplierTypeViewModelMap
    {
        /// <summary>
        /// Convert SupplierTypeListModel to SupplierTypeListViewModel
        /// </summary>
        /// <param name="model">SupplierTypeListModel model</param>
        /// <returns>Return SupplierTypeListViewModel</returns>
        public static SupplierTypeListViewModel ToListViewModel(SupplierTypeListModel model)
        {
            if (!Equals(model, null) && !Equals(model.SupplierTypes, null))
            {
                var viewModel = new SupplierTypeListViewModel()
                {
                    SupplierType = model.SupplierTypes.ToList().Select(
                    x => new SupplierTypeViewModel()
                    {
                        SupplierTypeId=x.SupplierTypeId,
                        Name = x.Name,
                        ClassName = x.ClassName,
                        IsActive = x.IsActive,
                        Description=x.Description,
                    }).ToList()
                };
                viewModel.TotalResults = Convert.ToInt32(model.TotalResults);
                viewModel.Page = Convert.ToInt32(model.PageIndex);
                viewModel.RecordPerPage = Convert.ToInt32(model.PageSize);
                viewModel.TotalPages = Convert.ToInt32(model.TotalPages);
                viewModel.TotalResults = Convert.ToInt32(model.TotalResults);
                return viewModel;
            }
            else
            {
                return new SupplierTypeListViewModel();
            }
        }
    }
}