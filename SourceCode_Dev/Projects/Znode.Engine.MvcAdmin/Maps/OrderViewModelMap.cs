﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    /// <summary>
    /// Mapper model for Order Section
    /// </summary>
    public static class OrderViewModelMap
    {
        #region Private Variables
        private const string constPendingApproval = "PENDING APPROVAL";
        #endregion

        /// <summary>
        /// Converting Order View Model to Order Model  
        /// </summary>
        /// <param name="viewModel">Model of type OrderView model</param>
        /// <returns>OrderModel</returns>
        public static OrderModel ToOrderModel(OrderViewModel viewModel)
        {
            if (Equals(viewModel, null))
            {
                return null;
            }
            OrderModel model = new OrderModel();
            model.OrderId = viewModel.OrderId;
            model.OrderDate = !string.IsNullOrEmpty(viewModel.OrderDate) ? DateTime.Parse(viewModel.OrderDate) : new DateTime();
            model.AccountId = viewModel.AccountId;
            model.AdditionalInstructions = viewModel.AdditionalInstructions;
            model.BillingCity = viewModel.BillingCity;
            model.BillingCompanyName = viewModel.BillingCompanyName;
            model.BillingCountryCode = viewModel.BillingCountryCode;
            model.BillingEmail = viewModel.BillingEmail;
            model.BillingFirstName = viewModel.BillingFirstName;
            model.BillingLastName = viewModel.BillingLastName;
            model.BillingPhoneNumber = viewModel.BillingPhoneNumber;
            model.BillingPostalCode = viewModel.BillingPostalCode;
            model.BillingStateCode = viewModel.BillingStateCode;
            model.BillingStreetAddress1 = viewModel.BillingStreetAddress1;
            model.BillingStreetAddress2 = viewModel.BillingStreetAddress2;
            model.CardAuthorizationCode = viewModel.CardAuthorizationCode;
            model.CardExpiration = viewModel.CardExpiration;
            model.CardTransactionId = viewModel.CardTransactionId;
            model.CardTypeId = viewModel.CardTypeId;
            model.CouponCode = viewModel.CouponCode;
            model.CreateDate = viewModel.CreateDate;
            model.CreatedBy = viewModel.CreatedBy;
            model.DiscountAmount = Equals(viewModel.DiscountAmount, null) ? 0 : MvcAdminUnits.ConvertCurrencyToDecimalValue(viewModel.DiscountAmount);
            model.ExternalId = viewModel.ExternalId;
            model.Gst = viewModel.Gst;
            model.Hst = viewModel.Hst;
            model.ModifiedBy = viewModel.ModifiedBy;
            model.ModifiedDate = viewModel.ModifiedDate;
            model.OrderStateId = viewModel.OrderStateId;
            model.PaymentOptionId = viewModel.PaymentOptionId;
            model.PaymentStatusId = viewModel.PaymentStatusId;
            model.PaymentTypeId = viewModel.PaymentTypeId;
            model.PortalId = viewModel.PortalId;
            model.PromotionDescription = viewModel.PromotionDescription;
            model.Pst = viewModel.Pst;
            model.PurchaseOrderNumber = viewModel.PurchaseOrderNumber;
            model.ReceiptHtml = viewModel.ReceiptHtml;
            model.IsOffsitePayment = viewModel.IsOffsitePayment;
            model.ReferralAccountId = viewModel.ReferralAccountId;
            model.ReturnDate = viewModel.ReturnDate;
            model.SalesTax = viewModel.SalesTax;
            model.ShipDate = !string.IsNullOrEmpty(viewModel.ShipDate) ? DateTime.Parse(viewModel.ShipDate) : new DateTime();
            model.ShippingCost = Equals(viewModel.ShippingCost, null) ? 0 : MvcAdminUnits.ConvertCurrencyToDecimalValue(viewModel.ShippingCost);
            model.ShippingOptionId = viewModel.ShippingOptionId;
            model.SubTotal = viewModel.SubTotal;
            model.TaxCost = !string.IsNullOrEmpty(viewModel.TaxCost) ? MvcAdminUnits.ConvertCurrencyToDecimalValue(viewModel.TaxCost) : 0;
            model.Total = !string.IsNullOrEmpty(viewModel.Total) ? MvcAdminUnits.ConvertCurrencyToDecimalValue(viewModel.Total) : 0;
            model.TrackingNumber = viewModel.TrackingNumber;
            model.Vat = viewModel.Vat;
            model.WebServiceDownloadDate = viewModel.WebServiceDownloadDate;
            model.OrderShipment = !Equals(viewModel.OrderShipment, null) ? OrderShipmentViewModelMap.ToModel(viewModel.OrderShipment) : null;
            model.ShippingID = viewModel.ShippingID;
            model.ShippingTypeName = HttpUtility.HtmlDecode(viewModel.ShippingTypeName);
            model.PaymentSettingId = viewModel.PaymentSettingId;
            model.GiftAmount = !string.IsNullOrEmpty(viewModel.GiftAmount) ? MvcAdminUnits.ConvertCurrencyToDecimalValue(viewModel.GiftAmount) : 0;
            model.PaymentMethod = viewModel.PaymentMethod;
            model.PromoCode = viewModel.PromoCode;
            model.OrderStatus = viewModel.OrderStatus;
            model.PaymentStatusName = viewModel.PaymentStatus;
            model.RefundCardNumber = viewModel.RefundCardNumber;
            model.SecurityCode = viewModel.SecurityCode;
            model.RefundAmount = !string.IsNullOrEmpty(viewModel.RefundAmount) ? MvcAdminUnits.ConvertCurrencyToDecimalValue(viewModel.RefundAmount) : 0.00M;
            model.Month = viewModel.Month;
            model.Year = viewModel.Year;
            model.OrderLineItemList = OrderLineItemViewModelMap.ToListModel(viewModel.OrderLineItemList);
            return model;
        }

        /// <summary>
        /// Converting Order Model to Order View Model  
        /// </summary>
        /// <param name="model">Model of type Order model</param>
        /// <returns>OrderViewModel</returns>
        public static OrderViewModel ToViewModel(OrderModel model)
        {
            if (Equals(model, null))
            {
                return null;
            }

            return new OrderViewModel()
            {
                OrderId = model.OrderId,
                OrderDate = HelperMethods.ViewDateFormatWithTime(Convert.ToDateTime(model.OrderDate)),
                AccountId = model.AccountId,
                AdditionalInstructions = model.AdditionalInstructions,
                BillingCity = model.BillingCity,
                BillingCompanyName = model.BillingCompanyName,
                BillingCountryCode = model.BillingCountryCode,
                BillingEmail = model.BillingEmailId,
                BillingFirstName = model.BillingFirstName,
                BillingLastName = model.BillingLastName,
                BillingPhoneNumber = model.BillingPhoneNumber,
                BillingPostalCode = model.BillingPostalCode,
                BillingStateCode = model.BillingStateCode,
                BillingStreetAddress1 = model.BillingStreet,
                BillingStreetAddress2 = model.BillingStreet1,
                CardAuthorizationCode = model.CardAuthorizationCode,
                CardExpiration = model.CardExpiration,
                CardTransactionId = model.CardTransactionId,
                CardTypeId = model.CardTypeId,
                CouponCode = model.CouponCode,
                CreateDate = model.CreateDate,
                CreatedBy = model.CreatedBy,
                DiscountAmount = MvcAdminUnits.GetCurrencyValue(Convert.ToDecimal(model.DiscountAmount)),
                ExternalId = model.ExternalId,
                Gst = model.Gst,
                Hst = model.Hst,
                ModifiedBy = model.ModifiedBy,
                ModifiedDate = model.ModifiedDate,
                OrderStateId = model.OrderStateId,
                PaymentOptionId = model.PaymentOptionId,
                PaymentStatusId = model.PaymentStatusId,
                PaymentTypeId = model.PaymentTypeId,
                PortalId = model.PortalId,
                PromotionDescription = model.PromotionDescription,
                Pst = model.Pst,
                PurchaseOrderNumber = model.PurchaseOrderNumber,
                ReceiptHtml = model.ReceiptHtml,
                IsOffsitePayment = model.IsOffsitePayment,
                ReferralAccountId = model.ReferralAccountId,
                ReturnDate = model.ReturnDate,
                SalesTax = model.SalesTax,
                ShipDate = HelperMethods.ViewDateFormatWithTime(Convert.ToDateTime(model.ShipDate)),
                ShippingCost = MvcAdminUnits.GetCurrencyValue(Convert.ToDecimal(model.ShippingCost)),
                ShippingOptionId = model.ShippingOptionId,
                SubTotal = model.SubTotal,
                TaxCost = MvcAdminUnits.GetCurrencyValue(Convert.ToDecimal(model.TaxCost)),
                Total = MvcAdminUnits.GetCurrencyValue(Convert.ToDecimal(model.Total)),
                TrackingNumber = model.TrackingNumber,
                Vat = model.Vat,
                WebServiceDownloadDate = model.WebServiceDownloadDate,
                OrderShipment = OrderShipmentViewModelMap.ToOrderViewModel(model),
                ShippingID = model.ShippingID,
                ShippingTypeName = HttpUtility.HtmlDecode(model.ShippingTypeName),
                PaymentSettingId = model.PaymentSettingId,
                GiftAmount = MvcAdminUnits.GetCurrencyValue(Convert.ToDecimal(model.GiftAmount)),
                PaymentMethod = model.PaymentMethod,
                PromoCode = model.PromoCode,
                OrderStatus = model.OrderStatus,
                PaymentStatus = model.PaymentStatusName,
                FullName = string.Format("{0} {1}", model.BillingFirstName, model.BillingLastName),
                RefundCardNumber = model.RefundCardNumber,
                SecurityCode = model.SecurityCode,
                RefundAmount = MvcAdminUnits.GetCurrencyValue(Convert.ToDecimal(model.RefundAmount)),
                Month = model.Month,
                Year = model.Year,
                PaymentTypeName = model.PaymentTypeName,
                OrderLineItemList = OrderLineItemViewModelMap.ToListViewModel(model.OrderLineItemList, 1, 10),
                rmaRequestList = RMARequestViewModelMap.ToListViewModel(model.rmaRequestList),
                ErrorMessage = (!Equals(model.ErrorMessage, null)) ? model.ErrorMessage : string.Empty,
                Custom1=model.Custom1
                                
            };
        }

        /// <summary>
        /// Maps AdminOrderList model to AdminOrderListView model.
        /// </summary>
        /// <param name="model">Mdoel of type AdminOrderList model</param>
        /// <param name="pageIndex">Index of page</param>
        /// <param name="recordPerPage">Records to be shown per page</param>
        /// <returns>Returns AdminOrderListViewModel</returns>
        public static AdminOrderListViewModel ToAdminOrderListViewModel(AdminOrderListModel model, int? pageIndex, int? recordPerPage)
        {
            if (!Equals(model, null))
            {
                var viewModel = new AdminOrderListViewModel()
                {
                    OrderList = model.OrderList.ToList().Select(
                    x => new AdminOrderViewModel()
                    {
                        OrderID = x.OrderID,
                        PortalId = x.PortalId,
                        AccountID = x.AccountID,
                        OrderStateID = x.OrderStateID,
                        ShippingID = x.ShippingID,
                        PaymentTypeId = x.PaymentTypeId,
                        PaymentStatusID = x.PaymentStatusID,
                        OrderStatus = x.OrderStatus,
                        ShippingTypeName = HttpUtility.HtmlDecode(x.ShippingTypeName),
                        PaymentTypeName = x.PaymentTypeName,
                        PaymentStatusName = x.PaymentStatusName,
                        BillingFirstName = x.BillingFirstName,
                        BillingLastName = x.BillingLastName,
                        Total = MvcAdminUnits.GetCurrencyValue(Convert.ToDecimal(x.Total)),
                        OrderDate = HelperMethods.ViewDateFormatWithTime(x.OrderDate),
                        ShipDate = HelperMethods.ViewDateFormatWithTime(x.ShipDate),
                        TrackingNumber = x.TrackingNumber,
                        PaymentSettingId = x.PaymentSettingId,
                        FullName = string.Format("{0} {1}", x.BillingFirstName, x.BillingLastName),
                        StoreName = x.StoreName,
                        BillingCompanyName = x.BillingCompanyName,
                        CardTransactionId = x.CardTransactionId
                    }).ToList()
                };

                viewModel.RecordPerPage = recordPerPage ?? 10;
                viewModel.Page = pageIndex ?? 1;
                viewModel.TotalResults = Convert.ToInt32(model.TotalResults);
                return viewModel;
            }
            return null;
        }

        /// <summary>
        /// Converts the OrderState model List to select list items type of list.
        /// </summary>
        /// <param name="model">IEnumerable<OrderStateModel> of list</param>
        /// <returns>List<SelectListItem></returns>
        public static List<SelectListItem> ToOrderStateSelectListItems(IEnumerable<OrderStateModel> model, int orderStateId = 0)
        {
            List<SelectListItem> orderStateList = new List<SelectListItem>();
            List<OrderStateModel> orderList = new List<OrderStateModel>();
            orderList = model.ToList<OrderStateModel>();
            foreach (OrderStateModel item in orderList)
            {
                if (item.OrderStateName.Contains(constPendingApproval))
                {
                    orderList.Remove(item);
                    break;
                }
            }

            if (!Equals(model, null))
            {
                orderStateList = (from item in orderList
                                  select new SelectListItem
                                  {
                                      Text = item.OrderStateName,
                                      Value = item.OrderStateID.ToString(),
                                      Selected = Equals(item.OrderStateID, orderStateId) ? true : false
                                  }).ToList();
            }
            return orderStateList;
        }
    }
}