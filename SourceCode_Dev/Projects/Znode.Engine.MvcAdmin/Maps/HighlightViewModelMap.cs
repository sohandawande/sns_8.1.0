﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.ViewModels;
using Znode.Libraries.Helpers;

namespace Znode.Engine.MvcAdmin.Maps
{
    /// <summary>
    /// Static Class for Highlight View Model Mapper 
    /// </summary>
    public static class HighlightViewModelMap
    {
        /// <summary>
        /// Convert HighlightModel to HighlightViewModel
        /// </summary>
        /// <param name="model">to covert to view model</param>
        /// <returns>HighlightViewModel</returns>
        public static HighlightViewModel ToViewModel(HighlightModel model)
        {
            ImageHelper imageHelper = new ImageHelper();
            if (!Equals(model, null))
            {
                var viewModel = new HighlightViewModel()
              {
                  ImageAltTag = Equals(model.ImageAltTag, null) ? string.Empty : model.ImageAltTag,
                  ImageFile = Equals(model.ImageFile, null) ? string.Empty : model.ImageFile,
                  ShowPopup = model.DisplayPopup,
                  HighlightId = model.HighlightId,
                  HighlightTypeId = Equals(model.HighlightTypeId, null) ? 0 : model.HighlightTypeId,
                  IsActive = (model.IsActive.HasValue) ? (bool)model.IsActive : false,
                  Hyperlink = Equals(model.Hyperlink, null) ? string.Empty : model.Hyperlink,
                  Name = Equals(model.Name, null) ? string.Empty : model.Name,
                  DisplayOrder = Equals(model.DisplayOrder, null) ? 0 : model.DisplayOrder,
                  Description = Equals(model.Description, null) ? string.Empty : model.Description,
                  HighlightType = Equals(model.HighlightType, null) ? null : HighlightTypeViewModelMap.ToViewModel(model.HighlightType),
                  LocaleId = model.LocaleId,
                  IsAssociatedProduct = model.IsAssociatedProduct,
                  HighlightTypeName = (!Equals(model.HighlightType, null) && !Equals(model.HighlightType.Name, null)) ? model.HighlightType.Name : string.Empty,
                  HyperlinkNewWindow=(!Equals(model.Hyperlink,null) && Equals(model.Description,null))?true:false,
              };

                viewModel.ImageSmallThumbnailPath = HelperMethods.GetImageRelativePath(imageHelper.GetImageHttpPathSmallThumbnail(model.ImageFile));
                viewModel.ImageMediumPath = HelperMethods.GetImageRelativePath(imageHelper.GetImageHttpPathMedium(model.ImageFile));
                viewModel.ImageLargePath = HelperMethods.GetImageRelativePath(imageHelper.GetImageHttpPathLarge(model.ImageFile));

                return viewModel;

            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Convert HighlightViewModel to HighlightModel
        /// </summary>
        /// <param name="viewModel">to covert to model</param>
        /// <returns>HighlightModel</returns>
        public static HighlightModel ToModel(HighlightViewModel viewModel)
        {
            ImageHelper imageHelper = new ImageHelper();
            if (!Equals(viewModel, null))
            {
                return new HighlightModel()
                {
                    HighlightTypeId = viewModel.TypeId,
                    ImageAltTag = viewModel.ImageAltTag,
                    ImageFile = viewModel.ImageFile,
                    DisplayPopup = viewModel.ShowPopup,
                    HyperlinkNewWindow = viewModel.HyperlinkNewWindow,
                    HighlightId = viewModel.HighlightId,
                    IsActive = viewModel.IsActive,
                    HighlightType = HighlightTypeViewModelMap.ToModel(viewModel.HighlightType),
                    LocaleId = viewModel.LocaleId,
                    Description = viewModel.Description,
                    Hyperlink = viewModel.Hyperlink,
                    Name = viewModel.Name,
                    DisplayOrder = viewModel.DisplayOrder,
                    IsAssociatedProduct = viewModel.IsAssociatedProduct
                };
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Convert IEnumerable<HighlightModel> to HighlightsListViewModel
        /// </summary>
        /// <param name="models">to convert to list view model</param>
        /// <returns>HighlightsListViewModel</returns>
        public static HighlightsListViewModel ToListViewModel(HighlightListModel models)
        {
            HighlightsListViewModel listViewModel = new HighlightsListViewModel();

            if (!Equals(models, null) && !Equals(models.Highlights, null))
            {
                foreach (HighlightModel model in models.Highlights)
                {
                    listViewModel.Highlights.Add(ToViewModel(model));
                }
            }

            listViewModel.Page = Convert.ToInt32(models.PageIndex);
            listViewModel.RecordPerPage = Convert.ToInt32(models.PageSize);
            listViewModel.TotalPages = Convert.ToInt32(models.TotalPages);
            listViewModel.TotalResults = Convert.ToInt32(models.TotalResults);

            return listViewModel;
        }

        /// <summary>
        /// Convert IEnumerable<HighlightModel> to HighlightsListViewModel
        /// </summary>
        /// <param name="models">to convert to list view model</param>
        /// <returns>HighlightsListViewModel</returns>
        public static HighlightsListViewModel ToListViewModels(HighlightListModel models)
        {
            if (!Equals(models, null)&&(!Equals(models.Highlights, null)))
            {
                var viewModel = new HighlightsListViewModel()
                {
                    Highlights = models.Highlights.ToList().Select(
                    model => new HighlightViewModel()
                    {
                        TypeId = model.HighlightTypeId,
                        HighlightType = (!Equals(model.HighlightType, null)) ? HighlightTypeViewModelMap.ToViewModel(model.HighlightType) : new HighlightTypeViewModel(),
                        ImageAltTag = model.ImageAltTag,
                        ImageFile = model.ImageFile,
                        ImageLargePath = HelperMethods.GetImagePath(model.ImageLargePath),
                        ImageMediumPath = HelperMethods.GetImagePath(model.ImageMediumPath),
                        ImageSmallThumbnailPath = HelperMethods.GetImagePath(model.ImageSmallThumbnailPath),
                        HighlightTypeId = model.HighlightTypeId,
                        ShowPopup = model.DisplayPopup,
                        HyperlinkNewWindow = model.HyperlinkNewWindow,
                        HighlightId = model.HighlightId,
                        IsActive = (model.IsActive.HasValue) ? (bool)model.IsActive : false,
                        Hyperlink = model.Hyperlink,
                        Name = model.Name,
                        DisplayOrder = model.DisplayOrder,
                        Description = model.Description,
                        LocaleId = model.LocaleId,
                        IsAssociatedProduct = model.IsAssociatedProduct,
                        ProductHighlightID = model.ProductHighlightID,
                        HighlightTypeName = string.IsNullOrEmpty(model.HighlightTypeName) ? string.Empty : model.HighlightTypeName,
                    }).ToList()
                };
                viewModel.TotalResults = Convert.ToInt32(models.TotalResults);
                viewModel.Page = Convert.ToInt32(models.PageIndex);
                viewModel.RecordPerPage = Convert.ToInt32(models.PageSize);
                viewModel.TotalPages = Convert.ToInt32(models.TotalPages);
                return viewModel;
            }
            else
            {
                return new HighlightsListViewModel();
            }
        }

        /// <summary>
        /// Convert HighlightListModel to HighlightListViewModel.
        /// </summary>
        /// <param name="model">model to convert into view model.</param>
        /// <returns>HighlightsListViewModel</returns>
        public static HighlightsListViewModel ToHighlightListViewModel(HighlightListModel model)
        {
            HighlightsListViewModel listViewModel = new HighlightsListViewModel();

            if (model.NotEquals(null))
            {
                foreach (var item in model.Highlights)
                {
                    HighlightViewModel viewModel = new HighlightViewModel();
                    viewModel.HighlightId = item.HighlightId;
                    viewModel.Name = item.Name;
                    viewModel.HighlightType = (!Equals(item.HighlightType, null)) ? HighlightTypeViewModelMap.ToViewModel(item.HighlightType) : new HighlightTypeViewModel();
                    viewModel.DisplayOrder = item.DisplayOrder;
                    listViewModel.Highlights.Add(viewModel);
                }

                listViewModel.Page = Convert.ToInt32(model.PageIndex);
                listViewModel.RecordPerPage = Convert.ToInt32(model.PageSize);
                listViewModel.TotalPages = Convert.ToInt32(model.TotalPages);
                listViewModel.TotalResults = Convert.ToInt32(model.TotalResults);
            }

            return listViewModel;
        }
        
        /// <summary>
        /// Convert Highlightlistviewmodel to List of HighlightModel
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public static List<HighlightModel> ToHighlightModels(HighlightsListViewModel viewModel)
        {
            List<HighlightModel> list = new List<HighlightModel>();
            if (!Equals(viewModel, null) && !Equals(viewModel.Highlights, null))
            {
                foreach (HighlightViewModel item in viewModel.Highlights)
                {
                    HighlightModel model = new HighlightModel();
                    model.HighlightId = item.HighlightId;
                    model.ProductId = item.ProductId;
                    model.UserName = HttpContext.Current.User.Identity.Name;
                    model.UserType = viewModel.UserType;
                    list.Add(model);
                }
            }
            return list;
        }
    }
}