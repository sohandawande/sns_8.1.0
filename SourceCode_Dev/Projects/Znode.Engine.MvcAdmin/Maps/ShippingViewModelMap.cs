﻿using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    /// <summary>
    /// Static Class for Shipping Mapper
    /// </summary>
    public static class ShippingViewModelMap
    {
        /// <summary>
        /// Mapper to convert ShippingOptionModel to ShippingViewModel  
        /// </summary>
        /// <param name="shippingOptionModel">Object of ShippingOptionModel</param>
        /// <returns>ShippingViewModel</returns>
        public static ShippingViewModel ToViewModel(ShippingOptionModel shippingOptionModel)
        {
            if (Equals(shippingOptionModel, null))
            {
                return null;
            }

            return new ShippingViewModel()
            {
                DisplayName = shippingOptionModel.Description,
                InternalCode = shippingOptionModel.ShippingCode,
                DisplayOrder = shippingOptionModel.DisplayOrder,
                HandlingCharge = shippingOptionModel.HandlingCharge,
                Enable = shippingOptionModel.ActiveInd,

                BaseCost = shippingOptionModel.ShippingRule.BaseCost,
                PerUnitCost = shippingOptionModel.ShippingRule.PerItemCost,
                LowerLimit = shippingOptionModel.ShippingRule.LowerLimit,
                UpperLimit = shippingOptionModel.ShippingRule.UpperLimit
            };
        }

        /// <summary>
        /// Mapper to convert ShippingViewModel to ShippingOptionModel  
        /// </summary>
        /// <param name="shippingViewModel">Object of ShippingViewModel</param>
        /// <returns>ShippingOptionModel</returns> 
        public static ShippingOptionModel ToModel(ShippingViewModel shippingViewModel)
        {
            if (Equals(shippingViewModel, null))
            {
                return null;
            }

            return new ShippingOptionModel()
            {
                Description = shippingViewModel.DisplayName,
                ShippingCode = shippingViewModel.InternalCode,
                DisplayOrder = shippingViewModel.DisplayOrder,
                HandlingCharge = shippingViewModel.HandlingCharge,
                ActiveInd = shippingViewModel.Enable,

                ShippingRule = new ShippingRuleModel()
                {
                    BaseCost = shippingViewModel.BaseCost,
                    PerItemCost = shippingViewModel.PerUnitCost,
                    LowerLimit = shippingViewModel.LowerLimit,
                    UpperLimit = shippingViewModel.UpperLimit,
                }
            };
        }
    }
}