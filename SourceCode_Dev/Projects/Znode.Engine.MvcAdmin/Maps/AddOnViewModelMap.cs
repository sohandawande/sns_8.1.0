﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Libraries.Helpers;
using Znode.Engine.MvcAdmin.ViewModels;
using System.Collections.ObjectModel;
namespace Znode.Engine.MvcAdmin.Maps
{
    /// <summary>
    /// Static Class For AddOn Mapper
    /// </summary>
    public static class AddOnViewModelMap
    {
        /// <summary>
        /// Convert AddOnViewModel to AddOnModel
        /// </summary>
        /// <param name="model">AddOnViewModel model</param>
        /// <returns>Returns the model of AddOn</returns>
        public static AddOnModel ToModel(AddOnViewModel model)
        {
            if (Equals(model, null))
            {
                return null;
            }

            return new AddOnModel()
            {
                Name = model.Name,
                Title = model.Title,
                Description = Equals(model.Description, null) ? String.Empty : HttpUtility.HtmlDecode(model.Description),
                InStockMessage = Equals(model.InStockMessage, null) ? String.Empty : model.InStockMessage,
                OutOfStockMessage = model.OutOfStockMessage,
                BackOrderMessage = Equals(model.BackOrderMessage, null) ? String.Empty : model.BackOrderMessage,
                DisplayOrder = Equals(model.DisplayOrder, null) ? 500 : Convert.ToInt32(model.DisplayOrder),
                IsOptional = model.IsOptional,
                AllowBackOrder = model.AllowBackOrder,
                TrackInventory = model.TrackInventory,
                PromptMessage = Equals(model.PromptMessage, null) ? String.Empty : model.PromptMessage,
                DisplayType = model.DisplayType,
                AddOnId = model.AddOnId,
                PortalId = model.PortalId,
                LocaleId = model.LocaleId,
                AccountId = model.AccountId
            };
        }
        /// <summary>
        /// Convert AddOnViewModel to AddOnModel
        /// </summary>
        /// <param name="model">AddOnModel model</param>
        /// <returns>Returns the view model of AddOn Value</returns>
        public static AddOnViewModel ToViewModel(AddOnModel model)
        {
            if (Equals(model, null))
            {
                return null;
            }
            return new AddOnViewModel()
            {
                Name = model.Name,
                Title = model.Title,
                Description = model.Description,
                InStockMessage = model.InStockMessage,
                OutOfStockMessage = model.OutOfStockMessage,
                BackOrderMessage = model.BackOrderMessage,
                DisplayOrder = model.DisplayOrder,
                IsOptional = model.IsOptional,
                OptionalIndicator = model.IsOptional,
                AllowBackOrder = model.AllowBackOrder,
                TrackInventory = model.TrackInventory,
                PromptMessage = model.PromptMessage,
                DisplayType = model.DisplayType,
                AddOnId = model.AddOnId,
                SelectedAddOnValue = model.IsOptional ? null : GetDefaultSelectedAddOnValues(model.AddOnValues),
                AddOnValues = model.AddOnValues.Select(ToAddonValuesViewModel).OrderBy(x => x.DisplayOrder).ToList(),
                PortalId = model.PortalId,
                LocaleId = model.LocaleId,
                AccountId = model.AccountId
            };
        }
        /// <summary>
        /// Convert Addon Model to AddOnListViewModel
        /// </summary>
        /// <param name="model">IEnumerable AddOnModel model</param>
        /// <returns>Returns list of addOn</returns>
        public static AddOnListViewModel ToListViewModel(AddOnListModel model)
        {
            if (!Equals(model, null) && !Equals(model.AddOns, null))
            {
                var viewModel = new AddOnListViewModel()
            {
                AddOns = model.AddOns.ToList().Select(
            x => new AddOnViewModel()
            {
                AddOnId = x.AddOnId,
                Name = x.Name,
                DisplayOrder = x.DisplayOrder,
                Title = x.Title,
                IsOptional = x.IsOptional,
                ProductAddOnId = x.ProductAddOnId,
                DisplayType = x.DisplayType,
                AddOnValueName = HttpUtility.HtmlDecode(x.AddOnValueName)
            }).ToList()
            };
                viewModel.TotalResults = Convert.ToInt32(model.TotalResults);
                viewModel.Page = Convert.ToInt32(model.PageIndex);
                viewModel.RecordPerPage = Convert.ToInt32(model.PageSize);
                viewModel.TotalPages = Convert.ToInt32(model.TotalPages);
                return viewModel;
            }
            else
            {
                return new AddOnListViewModel();
            }
        }

        /// <summary>
        /// Converts AddOn List view model to AddOn list model
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public static List<AddOnModel> ToAddOnModels(AddOnListViewModel viewModel)
        {
            List<AddOnModel> list = null;

            if (viewModel.NotEquals(null))
            {
                list = new List<AddOnModel>();
                foreach (var item in viewModel.AddOns)
                {
                    AddOnModel model = new AddOnModel();
                    model.ProductId = viewModel.ProductId;
                    model.AddOnId = item.AddOnId;
                    model.UserName = HttpContext.Current.User.Identity.Name;
                    list.Add(model);
                }
            }
            return list;
        }

        public static AddOnValueViewModel ToAddonValuesViewModel(AddOnValueModel model)
        {
            return new AddOnValueViewModel()
            {
                AddOnValueId = model.AddOnValueId,
                Name = model.Name,
                DefaultIndicator = model.IsDefault,
                CustomText = model.CustomText,
                RetailPrice = model.RetailPrice,
                SalePrice = model.SalePrice,
                WholesalePrice = model.WholesalePrice,
                SKU = model.SKU,
                QuantityOnHand = model.QuantityOnHand,
                DisplayOrder = model.DisplayOrder
            };
        }

        private static int[] GetDefaultSelectedAddOnValues(Collection<AddOnValueModel> addOnValues)
        {
            var defaultAddOnValue = 0;

            var addOnValuesViewModel = addOnValues.FirstOrDefault(x => x.IsDefault);

            if (addOnValuesViewModel != null)
            {
                defaultAddOnValue = addOnValuesViewModel.AddOnValueId;
            }
            else
            {
                var addOnValueModel = addOnValues.OrderBy(x => x.DisplayOrder).FirstOrDefault();

                if (addOnValueModel != null)
                {
                    defaultAddOnValue = addOnValueModel.AddOnValueId;
                }
            }

            return new int[] { defaultAddOnValue };
        }
    }
}