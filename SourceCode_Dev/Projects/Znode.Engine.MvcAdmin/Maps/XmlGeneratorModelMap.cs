﻿using System;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    public static class XmlGeneratorModelMap
    {
        public static ApplicationSettingListViewModel ToListModel(ApplicationSettingListModel model)
        {
            if (!Equals(model, null) && !Equals(model.ApplicationSettingList, null))
            {
                var viewModel = new ApplicationSettingListViewModel()
                {
                    List = model.ApplicationSettingList.ToList().Select(
                    x => new ApplicationSettingModel()
                    {
                        Id = x.Id,
                        GroupName = x.GroupName,
                        ItemName = x.ItemName,
                        Setting = x.Setting,
                        ViewOptions = x.ViewOptions,
                        FrontPageName = x.FrontPageName,
                        FrontObjectName = x.FrontObjectName,
                        IsCompressed = x.IsCompressed
                    }).ToList()
                };

                viewModel.RecordPerPage = Convert.ToInt32(model.PageSize);
                viewModel.Page = Convert.ToInt32(model.PageIndex);
                viewModel.TotalResults = Convert.ToInt32(model.TotalResults);
                return viewModel;
            }
            return new ApplicationSettingListViewModel();
        }

        public static ApplicationSettingDataModel ToDataModel(ApplicationSettingModel model)
        {
            return new ApplicationSettingDataModel()
            {
                SettingTableName = model.SettingTableName,
                Id = model.Id,
                GroupName = model.GroupName,
                ItemName = model.ItemName,
                Setting = model.Setting,
                CreatedDate = model.CreatedDate,
                ModifiedDate = model.ModifiedDate,
                CreatedBy = model.CreatedBy,
                ModifiedBy = model.ModifiedBy,
                CreatedByName = model.CreatedByName,
                ModifiedByName = model.ModifiedByName,
                ViewOptions = model.ViewOptions,
                FrontPageName = model.FrontPageName,
                FrontObjectName = model.FrontObjectName,
                IsCompressed = model.IsCompressed,
                ActionMode = model.ActionMode
            };
        }

        public static ApplicationSettingModel ToApplicationSettingModel(string xmlSTring, string viewOptions, string entityType, string entityName, string frontPageName, string frontObjectName, int id)
        {
            return new ApplicationSettingModel()
            {
                Id = id,
                ViewOptions = viewOptions,
                GroupName = entityType,
                ItemName = entityName,
                Setting = xmlSTring,
                FrontPageName = frontPageName,
                FrontObjectName = frontObjectName
            };

        }
    }
}