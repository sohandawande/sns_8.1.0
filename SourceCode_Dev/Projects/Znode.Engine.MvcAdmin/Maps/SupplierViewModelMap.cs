﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    /// <summary>
    /// Static Class for Supplier Mapper.
    /// </summary>
    public static class SupplierViewModelMap
    {
        /// <summary>
        /// Convert SupplierViewModel to SupplierModel.
        /// </summary>
        /// <param name="viewModel">view model to convert.</param>
        /// <returns>SupplierModel</returns>
        public static SupplierModel ToModel(SupplierViewModel viewModel)
        {
            if (Equals(viewModel, null))
            {
                return null;
            }

            return new SupplierModel()
            {
                SupplierId=viewModel.SupplierId,
                ContactEmail=Equals(viewModel.ContactEmail,null) ? string.Empty : viewModel.ContactEmail,
                ContactFirstName=Equals(viewModel.ContactFirstName,null) ? string.Empty : viewModel.ContactFirstName,
                ContactLastName=Equals(viewModel.ContactLastName,null) ? string.Empty : viewModel.ContactLastName,
                ContactPhone=Equals(viewModel.ContactPhoneNumber,null) ? string.Empty : viewModel.ContactPhoneNumber,
                Custom1=Equals(viewModel.Custom1,null) ?string.Empty : viewModel.Custom1,
                Custom2 = Equals(viewModel.Custom2, null) ? string.Empty : viewModel.Custom2,
                Custom3 = Equals(viewModel.Custom3, null) ? string.Empty : viewModel.Custom3,
                Custom4 = Equals(viewModel.Custom4, null) ? string.Empty : viewModel.Custom4,
                Custom5 = Equals(viewModel.Custom5, null) ? string.Empty : viewModel.Custom5,
                Description=Equals(viewModel.Description,null) ? string.Empty : viewModel.Description,
                DisplayOrder=Equals(viewModel.DisplayOrder,null) ? 0 : viewModel.DisplayOrder,
                EmailNotificationTemplate=Equals(viewModel.EmailNotificationTemplate,null) ? string.Empty : viewModel.EmailNotificationTemplate,
                EnableEmailNotification=Equals(viewModel.EnableEmailNotification,null) ? false : viewModel.EnableEmailNotification,
                ExternalSupplierNumber=Equals(viewModel.ExternalSupplierNumber,null) ? string.Empty : viewModel.ExternalSupplierNumber,
                IsActive=viewModel.IsActive,
                Name=Equals(viewModel.Name,null) ? string.Empty : viewModel.Name,
                NotificationEmail = Equals(viewModel.NotificationEmail, null) ? string.Empty : viewModel.NotificationEmail,
                SupplierTypeId=viewModel.SupplierTypeId,
                ClassName = string.IsNullOrEmpty(viewModel.SupplierTypeClassName) ? string.Empty : viewModel.SupplierTypeClassName
            };
        }

        /// <summary>
        /// Convert SupplierModel to SupplierViewModel.
        /// </summary>
        /// <param name="model">mode to convert.</param>
        /// <returns>SupplierViewModel</returns>
        public static SupplierViewModel ToViewModel(SupplierModel model)
        {
            if (Equals( model, null))
            {
                return null;
            }
            return new SupplierViewModel()
            {
                SupplierId =Equals(model.SupplierId,null)? 0 :model.SupplierId,
                ContactEmail = Equals(model.ContactEmail, null) ? string.Empty : model.ContactEmail,
                ContactFirstName = Equals(model.ContactFirstName, null) ? string.Empty : model.ContactFirstName,
                ContactLastName = Equals(model.ContactLastName, null) ? string.Empty : model.ContactLastName,
                ContactPhoneNumber = Equals(model.ContactPhone, null) ? string.Empty : model.ContactPhone,
                Custom1 = Equals(model.Custom1, null) ? string.Empty : model.Custom1,
                Custom2 = Equals(model.Custom2, null) ? string.Empty : model.Custom2,
                Custom3 = Equals(model.Custom3, null) ? string.Empty : model.Custom3,
                Custom4 = Equals(model.Custom4, null) ? string.Empty : model.Custom4,
                Custom5 = Equals(model.Custom5, null) ? string.Empty : model.Custom5,
                Description = Equals(model.Description, null) ? string.Empty : model.Description,
                DisplayOrder = (Equals(model.DisplayOrder, null) && model.DisplayOrder.HasValue) ? 0 : model.DisplayOrder.Value,
                EmailNotificationTemplate = Equals(model.EmailNotificationTemplate, null) ? string.Empty : model.EmailNotificationTemplate,
                EnableEmailNotification = Equals(model.EnableEmailNotification, null) ? false : model.EnableEmailNotification,
                ExternalSupplierNumber = Equals(model.ExternalSupplierNumber, null) ? string.Empty : model.ExternalSupplierNumber,
                IsActive = Equals(model.IsActive, null) ? false : model.IsActive,
                Name = Equals(model.Name, null) ? string.Empty : model.Name,
                NotificationEmail = Equals(model.NotificationEmail, null) ? string.Empty : model.NotificationEmail,
                SupplierTypeId =Equals(model.SupplierTypeId,null)? null : model.SupplierTypeId,
                SupplierTypeClassName=string.IsNullOrEmpty(model.ClassName) ? string.Empty : model.ClassName
            };
        }

        /// <summary>
        /// Convert SupplierListModel to SupplierListViewModel.
        /// </summary>
        /// <param name="listModel">list model to convert.</param>
        /// <returns>SupplierListViewModel</returns>
        public static SupplierListViewModel ToListViewModel(SupplierListModel listModel)
        {
            if (!Equals(listModel, null) && !Equals(listModel.Suppliers, null))
            {
                SupplierListViewModel listViewModel = new SupplierListViewModel()
                {
                    Suppliers = listModel.Suppliers.ToList().Select(
                    model => new SupplierViewModel()
                    {
                        SupplierId = model.SupplierId,
                        ContactEmail = model.ContactEmail,
                        ContactFirstName = model.ContactFirstName,
                        ContactLastName = model.ContactLastName,
                        ContactPhoneNumber = model.ContactPhone,
                        Custom1 = model.Custom1,
                        Custom2 = model.Custom2,
                        Custom3 = model.Custom3,
                        Custom4 = model.Custom4,
                        Custom5 = model.Custom5,
                        Description = model.Description,
                        DisplayOrder = model.DisplayOrder.Value,
                        EmailNotificationTemplate = model.EmailNotificationTemplate,
                        EnableEmailNotification = model.EnableEmailNotification,
                        ExternalSupplierNumber = model.ExternalSupplierNumber,
                        IsActive = model.IsActive,
                        Name = model.Name,
                        NotificationEmail = model.NotificationEmail,
                        SupplierTypeId = model.SupplierTypeId,
                        SupplierTypeName = (!Equals(model.SupplierType, null) && !Equals(model.SupplierType.Name, null)) ? model.SupplierType.Name : string.Empty
                    }).ToList()
                };

                listViewModel.TotalResults = Convert.ToInt32(listModel.TotalResults);
                listViewModel.Page = Convert.ToInt32(listModel.PageIndex);
                listViewModel.RecordPerPage = Convert.ToInt32(listModel.PageSize);
                listViewModel.TotalPages = Convert.ToInt32(listModel.TotalPages);
                listViewModel.TotalResults = Convert.ToInt32(listModel.TotalResults);
                return listViewModel;
            }
            else
            {
                return new SupplierListViewModel();
            }
        }

        /// <summary>
        /// To get list items from Active Status Type.
        /// </summary>
        /// <returns>returns list on Status items</returns>
        public static List<SelectListItem> ToListItems()
        {
            List<SelectListItem> statusItem = new List<SelectListItem>();
            statusItem.Add(new SelectListItem { Value = Convert.ToInt32(ActiveStatusType.Active).ToString(), Text = ActiveStatusType.Active.ToString() });
            statusItem.Add(new SelectListItem { Value = Convert.ToInt32(ActiveStatusType.Inactive).ToString(), Text = ActiveStatusType.Inactive.ToString() });
            return statusItem;
        }
    }
}