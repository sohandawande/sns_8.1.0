﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Extensions;

namespace Znode.Engine.MvcAdmin.Maps
{
    public class FacetGroupViewModelMap
    {
        /// <summary>
        /// Method maps the data from Review model to Review view model.
        /// </summary>
        /// <param name="models"></param>
        /// <returns>Retunrs the Review model</returns>
        public static FacetGroupListViewModel ToViewModels(IEnumerable<FacetGroupModel> model, int? totalResult=0)
        {
            var viewModel = new FacetGroupListViewModel()
            {

                FacetGroups = model.ToList().Select(
                x => new FacetGroupViewModel()
                {
                    CatalogId = x.CatalogID,
                    DisplayOrder = x.DisplayOrder,
                    FacetGroupLabel = x.FacetGroupLabel,
                    ControlTypeId = x.ControlTypeID,
                    FacetGroupId = x.FacetGroupID,
                }).ToList()
            };
            viewModel.TotalResults = Convert.ToInt32(totalResult);
            return viewModel;
        }

        public static CatalogListViewModel ToViewModels(IEnumerable<CatalogModel> model)
        {
            var viewModel = new CatalogListViewModel()
            {
                Catalogs = model.ToList().Select(

                x => new CatalogViewModel()
                {
                    CatalogId = x.CatalogId,
                    ExternalId = x.ExternalId,
                    Name = x.Name,
                    PortalId = x.PortalId,
                }).ToList()
            };
            return viewModel;
        }

        public static List<SelectListItem> GetSelectListItem(IEnumerable<BaseModel> model, string labelValue, string labelText, bool isIncludeDefaultItem = false)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            try
            {
                if (!Equals(model, null))
                {
                    items = model.Select(x => new SelectListItem
                    {
                        Value = x.GetProperty(labelValue).ToString(),
                        Text = x.GetProperty(labelText).ToString(),
                        Selected = false
                    }).ToList();
                    items = items.GroupBy(x => x.Value).Select(x => x.First()).ToList();
                    if (isIncludeDefaultItem)
                    {
                        items.Insert(0, new SelectListItem { Text = "All", Value = "0" });
                    }
                }
            }
            catch (Exception ex)
            {
                items = new List<SelectListItem>();
            }
            return items;
        }

        public static List<SelectListItem> ToListItem(CategoryModel model)
        {
            List<SelectListItem> categoryItems = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                categoryItems.Add(new SelectListItem { Text = model.Name, Value = model.CategoryId.ToString() });
            }
            return categoryItems;
        }


        public static FacetGroupModel ToModel(FacetGroupViewModel viewModel)
        {
            var model = new FacetGroupModel()
            {
                CatalogID = viewModel.CatalogId,
                ControlTypeID = viewModel.ControlTypeId,
                DisplayOrder = viewModel.DisplayOrder,
                FacetGroupID = viewModel.FacetGroupId,
                FacetGroupLabel = viewModel.FacetGroupLabel,
            };
            return model;
        }

        public static FacetGroupCategoryListModel ToListModel(FacetGroupViewModel viewModel, int facetGroupId)
        {
            if (viewModel.AssociateCatagoriesListViewModel != null && viewModel.AssociateCatagoriesListViewModel.AssignedId.Count() > 0)
            {
                FacetGroupCategoryListModel model = new FacetGroupCategoryListModel();
                model.FacetGroupCategoryList = new System.Collections.ObjectModel.Collection<FacetGroupCategoryModel>();
                for (int i = 0; i < viewModel.AssociateCatagoriesListViewModel.AssignedId.Count(); i++)
                {
                    model.FacetGroupCategoryList.Add(new FacetGroupCategoryModel { CategoryDisplayOrder = i + 1, CategoryID = viewModel.AssociateCatagoriesListViewModel.AssignedId[i], FacetGroupID = facetGroupId });
                }
                return model;
            }
            return null;
        }

        public static FacetGroupViewModel ToViewModel(FacetGroupModel model)
        {
            var viewModel = new FacetGroupViewModel()
            {
                CatalogId = model.CatalogID,
                DisplayOrder = model.DisplayOrder,
                FacetGroupLabel = model.FacetGroupLabel,
                ControlTypeId = model.ControlTypeID,
                FacetGroupId = model.FacetGroupID,
            };
            return viewModel;
        }

        public static List<FacetViewModel> ToViewModel(IEnumerable<FacetModel> model)
        {
            List<FacetViewModel> facetItems = new List<FacetViewModel>();
            if (!Equals(model, null))
            {
                facetItems = (from item in model
                              select new FacetViewModel
                              {
                                  FacetID = item.FacetID,
                                  FacetName = item.FacetName,
                                  FacetGroupID = item.FacetGroupID,
                                  FacetDisplayOrder = item.FacetDisplayOrder
                              }).ToList();
            }
            return facetItems;
        }

        public static FacetModel ToModel(FacetViewModel viewModel)
        {
            var model = new FacetModel()
            {
                FacetGroupID = viewModel.FacetGroupID,
                FacetDisplayOrder = viewModel.FacetDisplayOrder,
                FacetName = viewModel.FacetName,
                IconPath = viewModel.IconPath,
                FacetID = viewModel.FacetID,
            };
            return model;
        }

        public static FacetViewModel ToViewModel(FacetModel model)
        {
            var viewModel = new FacetViewModel()
            {
                FacetDisplayOrder = model.FacetDisplayOrder,
                FacetGroupID = model.FacetGroupID,
                FacetID = model.FacetID,
                FacetName = model.FacetName,
            };
            return viewModel;
        }
       
        public static List<SelectListItem> ToListItems(List<PortalCatalogViewModel> model)
        {
            List<SelectListItem> categoryItems = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                categoryItems = (from item in model
                               select new SelectListItem
                               {
                                   Text = item.CatalogName,
                                   Value = item.CatalogId.ToString()
                               }).ToList();
            }
            return categoryItems;
        }
    }
}