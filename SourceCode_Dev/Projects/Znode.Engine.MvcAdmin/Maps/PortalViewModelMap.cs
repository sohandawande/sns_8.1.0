﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcDemo.Maps
{
    /// <summary>
    /// Maps portal models to portal view model which is used to show on views.
    /// </summary>
    public static class PortalViewModelMap
    {
        /// <summary>
        /// Converts portal model to portal view model.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static PortalViewModel ToViewModel(PortalModel model)
        {
            if (Equals(model, null))
            {
                return null;
            }
            PortalViewModel portalViewModel = new PortalViewModel()
                {
                    CompanyName = model.CompanyName,
                    PortalId = model.PortalId,
                    StoreName = model.StoreName,
                    PersistentCartEnabled = model.PersistentCartEnabled.GetValueOrDefault(false),
                    DefaultAnonymousProfileId = model.DefaultAnonymousProfileId.GetValueOrDefault(0),
                    MaxCatalogCategoryDisplayThumbnails = model.MaxCatalogCategoryDisplayThumbnails,
                    LogoPathImageName = model.LogoPath.Substring(model.LogoPath.LastIndexOf('/') + 1),
                    UseSsl = model.UseSsl,
                    AdminEmail = model.AdminEmail,
                    SalesEmail = model.SalesEmail,
                    CustomerServiceEmail = model.CustomerServiceEmail,
                    DefaultRegisteredProfileId = model.DefaultRegisteredProfileId.GetValueOrDefault(0),
                    CustomerServicePhoneNumber = model.CustomerServicePhoneNumber,
                    SalesPhoneNumber = model.SalesPhoneNumber,
                    DefaultReviewStatus = model.DefaultReviewStatus,
                    DefaultOrderStateId = model.DefaultOrderStateId,
                    EnableAddressValidation = model.EnableAddressValidation,
                    RequireValidatedAddress = model.RequireValidatedAddress,
                    EnableCustomerPricing = model.EnableCustomerPricing,
                    DefaultProductReviewStateId = model.DefaultProductReviewStateId,
                    DimensionUnit = model.DimensionUnit,
                    FedExAccountNumber = model.FedExAccountNumber,
                    FedExAddInsurance = model.FedExAddInsurance,
                    FedExClientProductId = model.FedExClientProductId,
                    FedExClientProductVersion = model.FedExClientProductVersion,
                    FedExCspKey = model.FedExCspKey,
                    FedExCspPassword = model.FedExCspPassword,
                    FedExDropoffType = model.FedExDropoffType,
                    FedExMeterNumber = model.FedExMeterNumber,
                    FedExPackagingType = model.FedExPackagingType,
                    FedExProductionKey = model.FedExProductionKey,
                    FedExSecurityCode = model.FedExSecurityCode,
                    FedExUseDiscountRate = model.FedExUseDiscountRate,
                    GoogleAnalyticsCode = model.GoogleAnalyticsCode,
                    ImageNotAvailablePathUrl = model.ImageNotAvailablePath,
                    InclusiveTax = model.InclusiveTax,
                    IsActive = model.IsActive,
                    IsShippingTaxable = model.IsShippingTaxable,
                    LocaleId = model.LocaleId,
                    MasterPage = model.MasterPage,
                    MaxCatalogDisplayColumns = model.MaxCatalogDisplayColumns,
                    MaxCatalogDisplayItems = model.MaxCatalogDisplayItems,
                    MaxCatalogItemCrossSellWidth = model.MaxCatalogItemCrossSellWidth,
                    MaxCatalogItemLargeWidth = model.MaxCatalogItemLargeWidth,
                    MaxCatalogItemMediumWidth = model.MaxCatalogItemMediumWidth,
                    MaxCatalogItemSmallThumbnailWidth = model.MaxCatalogItemSmallThumbnailWidth,
                    MaxCatalogItemSmallWidth = model.MaxCatalogItemSmallWidth,
                    MaxCatalogItemThumbnailWidth = model.MaxCatalogItemThumbnailWidth,
                    MobileTheme = model.MobileTheme,
                    OrderReceiptAffiliateJavascript = model.OrderReceiptAffiliateJavascript,
                    SeoDefaultCategoryDescription = model.SeoDefaultCategoryDescription,
                    SeoDefaultCategoryKeyword = model.SeoDefaultCategoryKeyword,
                    SeoDefaultCategoryTitle = model.SeoDefaultCategoryTitle,
                    SeoDefaultContentDescription = model.SeoDefaultContentDescription,
                    SeoDefaultContentKeyword = model.SeoDefaultContentKeyword,
                    SeoDefaultContentTitle = model.SeoDefaultContentTitle,
                    SeoDefaultProductDescription = model.SeoDefaultProductDescription,
                    SeoDefaultProductKeyword = model.SeoDefaultProductKeyword,
                    SeoDefaultProductTitle = model.SeoDefaultProductTitle,
                    ShippingOriginAddress1 = model.ShippingOriginAddress1,
                    ShippingOriginAddress2 = model.ShippingOriginAddress2,
                    ShippingOriginCity = model.ShippingOriginCity,
                    ShippingOriginCountryCode = model.ShippingOriginCountryCode,
                    ShippingOriginPhoneNumber = model.ShippingOriginPhone,
                    ShippingOriginStateCode = model.ShippingOriginStateCode,
                    ShippingOriginZipCode = model.ShippingOriginZipCode,
                    ShopByPriceIncrement = model.ShopByPriceIncrement,
                    ShopByPriceMax = model.ShopByPriceMax,
                    ShopByPriceMin = model.ShopByPriceMin,
                    SiteWideTopJavascript = model.SiteWideTopJavascript,
                    SiteWideBottomJavascript = model.SiteWideBottomJavascript,
                    SiteWideAnalyticsJavascript = model.SiteWideAnalyticsJavascript,
                    SmtpPassword = model.SmtpPassword,
                    SmtpPort = model.SmtpPort,
                    SmtpServer = model.SmtpServer,
                    SmtpUsername = model.SmtpUsername,
                    SplashCategoryId = model.SplashCategoryId,
                    SplashImageFile = model.SplashImageFile,
                    TimeZoneOffset = model.TimeZoneOffset,
                    UpsKey = model.UpsKey,
                    UpsPassword = model.UpsPassword,
                    UpsUsername = model.UpsUsername,
                    UseDynamicDisplayOrder = model.UseDynamicDisplayOrder,
                    WeightUnit = model.WeightUnit,
                    CurrencyTypeId = model.CurrencyTypeId,
                    DefaultOrderStateName = model.OrderStatus,
                    DefaultProductReviewStateName = model.DefaultProductReviewStateName,
                    CurrencySuffix = !string.IsNullOrEmpty(model.CurrencySuffix) ? model.CurrencyName : !Equals(model.CurrencyTypeModel, null) ? model.CurrencyTypeModel.CurrencySuffix : string.Empty,
                    CurrencyName = !string.IsNullOrEmpty(model.CurrencyName) ? model.CurrencyName : !Equals(model.CurrencyTypeModel, null)? model.CurrencyTypeModel.Name : string.Empty,
                    CatalogName = model.CatalogName,
                    CSSName = model.CssName,
                    ThemeName = model.ThemeName,
                    LocaleDescription = model.LocaleDescription,
                    IsEnableCompare = Equals(model.IsEnableCompare, null) ? false : model.IsEnableCompare.Value,
                    IsEnableSinglePageCheckout = Equals(model.IsEnableSinglePageCheckout, null) ? false : model.IsEnableSinglePageCheckout.Value,
                    CompareType = model.CompareType,
                    TabMode=model.TabMode,
                    IsMultipleCouponCodeAllowed=model.IsMultipleCouponAllowed
                };
            return portalViewModel;
        }

        /// <summary>
        /// Converts portal model to portal view model in edit or view mode.
        /// </summary>
        /// <param name="model">Portal model</param>
        /// <returns>Portal View Model</returns>
        public static PortalViewModel ToEditViewModel(PortalModel model)
        {
            if (Equals(model, null))
            {
                return null;
            }
            ImageHelper imageHelper = new ImageHelper();
            PortalViewModel portalViewModel = new PortalViewModel()
            {
                CompanyName = model.CompanyName,
                PortalId = model.PortalId,
                StoreName = model.StoreName,
                PersistentCartEnabled = model.PersistentCartEnabled.GetValueOrDefault(false),
                DefaultAnonymousProfileId = model.DefaultAnonymousProfileId.GetValueOrDefault(0),
                MaxCatalogCategoryDisplayThumbnails = model.MaxCatalogCategoryDisplayThumbnails,
                LogoPathImageName = model.LogoPath,
                UseSsl = model.UseSsl,
                AdminEmail = model.AdminEmail,
                SalesEmail = model.SalesEmail,
                CustomerServiceEmail = model.CustomerServiceEmail,
                DefaultRegisteredProfileId = model.DefaultRegisteredProfileId.GetValueOrDefault(0),
                CustomerServicePhoneNumber = model.CustomerServicePhoneNumber,
                SalesPhoneNumber = model.SalesPhoneNumber,
                DefaultReviewStatus = model.DefaultReviewStatus,
                DefaultOrderStateId = model.DefaultOrderStateId,
                EnableAddressValidation = model.EnableAddressValidation,
                RequireValidatedAddress = model.RequireValidatedAddress,
                EnableCustomerPricing = model.EnableCustomerPricing,
                DefaultProductReviewStateId = model.DefaultProductReviewStateId,
                DimensionUnit = model.DimensionUnit,
                FedExAccountNumber = model.FedExAccountNumber,
                FedExAddInsurance = model.FedExAddInsurance,
                FedExClientProductId = model.FedExClientProductId,
                FedExClientProductVersion = model.FedExClientProductVersion,
                FedExCspKey = model.FedExCspKey,
                FedExCspPassword = model.FedExCspPassword,
                FedExDropoffType = model.FedExDropoffType,
                FedExMeterNumber = model.FedExMeterNumber,
                FedExPackagingType = model.FedExPackagingType,
                FedExProductionKey = model.FedExProductionKey,
                FedExSecurityCode = model.FedExSecurityCode,
                FedExUseDiscountRate = model.FedExUseDiscountRate,
                GoogleAnalyticsCode = model.GoogleAnalyticsCode,
                ImageNotAvailablePathUrl = model.ImageNotAvailablePath,
                InclusiveTax = model.InclusiveTax,
                IsActive = model.IsActive,
                IsShippingTaxable = model.IsShippingTaxable,
                LocaleId = model.LocaleId,
                MasterPage = model.MasterPage,
                MaxCatalogDisplayColumns = model.MaxCatalogDisplayColumns,
                MaxCatalogDisplayItems = model.MaxCatalogDisplayItems,
                MaxCatalogItemCrossSellWidth = model.MaxCatalogItemCrossSellWidth,
                MaxCatalogItemLargeWidth = model.MaxCatalogItemLargeWidth,
                MaxCatalogItemMediumWidth = model.MaxCatalogItemMediumWidth,
                MaxCatalogItemSmallThumbnailWidth = model.MaxCatalogItemSmallThumbnailWidth,
                MaxCatalogItemSmallWidth = model.MaxCatalogItemSmallWidth,
                MaxCatalogItemThumbnailWidth = model.MaxCatalogItemThumbnailWidth,
                MobileTheme = model.MobileTheme,
                OrderReceiptAffiliateJavascript = model.OrderReceiptAffiliateJavascript,
                SeoDefaultCategoryDescription = model.SeoDefaultCategoryDescription,
                SeoDefaultCategoryKeyword = model.SeoDefaultCategoryKeyword,
                SeoDefaultCategoryTitle = model.SeoDefaultCategoryTitle,
                SeoDefaultContentDescription = model.SeoDefaultContentDescription,
                SeoDefaultContentKeyword = model.SeoDefaultContentKeyword,
                SeoDefaultContentTitle = model.SeoDefaultContentTitle,
                SeoDefaultProductDescription = model.SeoDefaultProductDescription,
                SeoDefaultProductKeyword = model.SeoDefaultProductKeyword,
                SeoDefaultProductTitle = model.SeoDefaultProductTitle,
                ShippingOriginAddress1 = model.ShippingOriginAddress1,
                ShippingOriginAddress2 = model.ShippingOriginAddress2,
                ShippingOriginCity = model.ShippingOriginCity,
                ShippingOriginCountryCode = model.ShippingOriginCountryCode,
                ShippingOriginPhoneNumber = model.ShippingOriginPhone,
                ShippingOriginStateCode = model.ShippingOriginStateCode,
                ShippingOriginZipCode = model.ShippingOriginZipCode,
                ShopByPriceIncrement = model.ShopByPriceIncrement,
                ShopByPriceMax = model.ShopByPriceMax,
                ShopByPriceMin = model.ShopByPriceMin,
                SiteWideTopJavascript = model.SiteWideTopJavascript,
                SiteWideBottomJavascript = model.SiteWideBottomJavascript,
                SiteWideAnalyticsJavascript = model.SiteWideAnalyticsJavascript,
                SmtpPassword = model.SmtpPassword,
                SmtpPort = model.SmtpPort,
                SmtpServer = model.SmtpServer,
                SmtpUsername = model.SmtpUsername,
                SplashCategoryId = model.SplashCategoryId,
                SplashImageFile = model.SplashImageFile,
                TimeZoneOffset = model.TimeZoneOffset,
                UpsKey = model.UpsKey,
                UpsPassword = model.UpsPassword,
                UpsUsername = model.UpsUsername,
                UseDynamicDisplayOrder = model.UseDynamicDisplayOrder,
                WeightUnit = model.WeightUnit,
                CurrencyTypeId = model.CurrencyTypeId,
                DefaultOrderStateName = model.OrderStatus,
                DefaultProductReviewStateName = model.DefaultProductReviewStateName,
                CurrencySuffix = model.CurrencySuffix,
                CurrencyName = model.CurrencyName,
                CatalogName = model.CatalogName,
                CSSName = model.CssName,
                ThemeName = model.ThemeName,
                LocaleDescription = model.LocaleDescription,
                IsEnableCompare = Equals(model.IsEnableCompare, null) ? false : model.IsEnableCompare.Value,
                CompareType = model.CompareType,
                TabMode = model.TabMode,
                IsMultipleCouponCodeAllowed = model.IsMultipleCouponAllowed,
                IsEnableSinglePageCheckout = Equals(model.IsEnableSinglePageCheckout, null) ? false : model.IsEnableSinglePageCheckout.Value,
                EnableSslForSmtp=model.EnableSslForSmtp,
                 
            };
           

            #region Setting properties for ReviewStates
            switch (model.DefaultReviewStatus)
            {
                case MvcAdminConstants.PublishImmediately:
                    portalViewModel.DefaultReviewStatusName = MvcAdminConstants.PublishImmediatelyText;
                    break;
                case MvcAdminConstants.DoNotPublish:
                    portalViewModel.DefaultReviewStatusName = MvcAdminConstants.DoNotPublishText;
                    break;

            }
            #endregion

            #region Setting properties for Fedex.
            switch (model.FedExDropoffType)
            {
                case MvcAdminConstants.BUSINESS_SERVICE_CENTER:
                    portalViewModel.FedExDropoffTypeName = MvcAdminConstants.BUSINESS_SERVICE_CENTERText;
                    break;
                case MvcAdminConstants.DROP_BOX:
                    portalViewModel.FedExDropoffTypeName = MvcAdminConstants.DROP_BOXText;
                    break;
                case MvcAdminConstants.REGULAR_PICKUP:
                    portalViewModel.FedExDropoffTypeName = MvcAdminConstants.REGULAR_PICKUPText;
                    break;
                case MvcAdminConstants.REQUEST_COURIER:
                    portalViewModel.FedExDropoffTypeName = MvcAdminConstants.REQUEST_COURIERText;
                    break;
                case MvcAdminConstants.STATION:
                    portalViewModel.FedExDropoffTypeName = MvcAdminConstants.STATIONText;
                    break;
            }
            switch (model.FedExPackagingType)
            {
                case MvcAdminConstants.YOUR_PACKAGING:
                    portalViewModel.FedexPackagingTypeName = MvcAdminConstants.YOUR_PACKAGINGText;
                    break;
                case MvcAdminConstants.FEDEX_BOX:
                    portalViewModel.FedexPackagingTypeName = MvcAdminConstants.FEDEX_BOXText;
                    break;
                case MvcAdminConstants.FEDEX_ENVELOPE:
                    portalViewModel.FedexPackagingTypeName = MvcAdminConstants.FEDEX_ENVELOPEText;
                    break;
                case MvcAdminConstants.FEDEX_TUBE:
                    portalViewModel.FedexPackagingTypeName = MvcAdminConstants.FEDEX_TUBEText;
                    break;
                case MvcAdminConstants.FEDEX_PAK:
                    portalViewModel.FedexPackagingTypeName = MvcAdminConstants.FEDEX_PAKText;
                    break;

            }
            #endregion
            return portalViewModel;
        }

        /// <summary>
        /// Converts list of portal model to portallist view model.
        /// </summary>
        /// <param name="models">IEnumerable<PortalModel></param>
        /// <returns>PortalListViewModel</returns>
        public static PortalListViewModel ToListViewModel(PortalListModel model, int? pageIndex, int? recordPerPage)
        {
            var viewModel = new PortalListViewModel();

            if (!Equals(model.Portals, null))
            {
                viewModel.Portals = model.Portals.Select(
                x => new PortalViewModel()
                {
                    PortalId = x.PortalId,
                    CompanyName = x.CompanyName,
                    StoreName = x.StoreName
                }).ToList();
            }
            viewModel.Portals.Where(x => x.PortalId.Equals(PortalAgent.CurrentPortal.PortalId)).ToList().ForEach(x => { x.IsCurrentPortal = true; });

            viewModel.RecordPerPage = recordPerPage ?? 10;
            viewModel.Page = pageIndex ?? 1;
            viewModel.TotalResults = Convert.ToInt32(model.TotalResults);
            return viewModel;
        }

        /// <summary>
        /// Converts portal view model to portal model.
        /// </summary>
        /// <param name="portalViewModel">Portal view model to be converted to portal model.</param>
        /// <returns>Portal model.</returns>
        public static PortalModel ToModel(PortalViewModel portalViewModel)
        {
            if (Equals(portalViewModel, null))
            {
                return null;
            }
            var portalModel = new PortalModel()
            {
                CompanyName = portalViewModel.CompanyName,
                PortalId = portalViewModel.PortalId,
                StoreName = portalViewModel.StoreName,
                PersistentCartEnabled = portalViewModel.PersistentCartEnabled,
                DefaultAnonymousProfileId = portalViewModel.DefaultAnonymousProfileId,
                DefaultRegisteredProfileId = portalViewModel.DefaultRegisteredProfileId,
                MaxCatalogCategoryDisplayThumbnails = portalViewModel.MaxCatalogCategoryDisplayThumbnails,
                LogoPath = portalViewModel.LogoPathImageName,
                UseSsl = portalViewModel.UseSsl,
                AdminEmail = portalViewModel.AdminEmail,
                SalesEmail = portalViewModel.SalesEmail,
                CustomerServiceEmail = portalViewModel.CustomerServiceEmail,
                CustomerServicePhoneNumber = portalViewModel.CustomerServicePhoneNumber,
                SalesPhoneNumber = portalViewModel.SalesPhoneNumber,
                DefaultReviewStatus = portalViewModel.DefaultReviewStatus,
                DefaultOrderStateId = portalViewModel.DefaultOrderStateId,
                EnableAddressValidation = portalViewModel.EnableAddressValidation,
                RequireValidatedAddress = portalViewModel.RequireValidatedAddress,
                EnableCustomerPricing = portalViewModel.EnableCustomerPricing,
                DefaultProductReviewStateId = portalViewModel.DefaultProductReviewStateId,
                DimensionUnit = portalViewModel.DimensionUnit,
                FedExAccountNumber = portalViewModel.FedExAccountNumber,
                FedExAddInsurance = portalViewModel.FedExAddInsurance,
                FedExClientProductId = portalViewModel.FedExClientProductId,
                FedExClientProductVersion = portalViewModel.FedExClientProductVersion,
                FedExCspKey = portalViewModel.FedExCspKey,
                FedExCspPassword = portalViewModel.FedExCspPassword,
                FedExDropoffType = portalViewModel.FedExDropoffType,
                FedExMeterNumber = portalViewModel.FedExMeterNumber,
                FedExPackagingType = portalViewModel.FedExPackagingType,
                FedExProductionKey = portalViewModel.FedExProductionKey,
                FedExSecurityCode = portalViewModel.FedExSecurityCode,
                FedExUseDiscountRate = portalViewModel.FedExUseDiscountRate,
                GoogleAnalyticsCode = portalViewModel.GoogleAnalyticsCode,
                ImageNotAvailablePath = portalViewModel.ImageNotAvailablePathUrl,
                InclusiveTax = portalViewModel.InclusiveTax,
                IsActive = portalViewModel.IsActive,
                IsShippingTaxable = portalViewModel.IsShippingTaxable,
                LocaleId = portalViewModel.LocaleId,
                MasterPage = portalViewModel.MasterPage,
                MaxCatalogDisplayColumns = portalViewModel.MaxCatalogDisplayColumns,
                MaxCatalogDisplayItems = portalViewModel.MaxCatalogDisplayItems,
                MaxCatalogItemCrossSellWidth = portalViewModel.MaxCatalogItemCrossSellWidth,
                MaxCatalogItemLargeWidth = portalViewModel.MaxCatalogItemLargeWidth,
                MaxCatalogItemMediumWidth = portalViewModel.MaxCatalogItemMediumWidth,
                MaxCatalogItemSmallThumbnailWidth = portalViewModel.MaxCatalogItemSmallThumbnailWidth,
                MaxCatalogItemSmallWidth = portalViewModel.MaxCatalogItemSmallWidth,
                MaxCatalogItemThumbnailWidth = portalViewModel.MaxCatalogItemThumbnailWidth,
                MobileTheme = portalViewModel.MobileTheme,
                OrderReceiptAffiliateJavascript = portalViewModel.OrderReceiptAffiliateJavascript,
                SeoDefaultCategoryDescription = portalViewModel.SeoDefaultCategoryDescription,
                SeoDefaultCategoryKeyword = portalViewModel.SeoDefaultCategoryKeyword,
                SeoDefaultCategoryTitle = portalViewModel.SeoDefaultCategoryTitle,
                SeoDefaultContentDescription = portalViewModel.SeoDefaultContentDescription,
                SeoDefaultContentKeyword = portalViewModel.SeoDefaultContentKeyword,
                SeoDefaultContentTitle = portalViewModel.SeoDefaultContentTitle,
                SeoDefaultProductDescription = portalViewModel.SeoDefaultProductDescription,
                SeoDefaultProductKeyword = portalViewModel.SeoDefaultProductKeyword,
                SeoDefaultProductTitle = portalViewModel.SeoDefaultProductTitle,
                ShippingOriginAddress1 = portalViewModel.ShippingOriginAddress1,
                ShippingOriginAddress2 = portalViewModel.ShippingOriginAddress2,
                ShippingOriginCity = portalViewModel.ShippingOriginCity,
                ShippingOriginCountryCode = portalViewModel.ShippingOriginCountryCode,
                ShippingOriginPhone = portalViewModel.ShippingOriginPhoneNumber,
                ShippingOriginStateCode = portalViewModel.ShippingOriginStateCode,
                ShippingOriginZipCode = portalViewModel.ShippingOriginZipCode,
                ShopByPriceIncrement = portalViewModel.ShopByPriceIncrement,
                ShopByPriceMax = portalViewModel.ShopByPriceMax,
                ShopByPriceMin = portalViewModel.ShopByPriceMin,
                SiteWideTopJavascript = portalViewModel.SiteWideTopJavascript,
                SiteWideBottomJavascript = portalViewModel.SiteWideBottomJavascript,
                SiteWideAnalyticsJavascript = portalViewModel.SiteWideAnalyticsJavascript,
                SmtpPassword = portalViewModel.SmtpPassword,
                SmtpPort = portalViewModel.SmtpPort,
                SmtpServer = portalViewModel.SmtpServer,
                SmtpUsername = portalViewModel.SmtpUsername,
                SplashCategoryId = portalViewModel.SplashCategoryId,
                SplashImageFile = portalViewModel.SplashImageFile,
                TimeZoneOffset = portalViewModel.TimeZoneOffset,
                UpsKey = portalViewModel.UpsKey,
                UpsPassword = portalViewModel.UpsPassword,
                UpsUsername = portalViewModel.UpsUsername,
                UseDynamicDisplayOrder = portalViewModel.UseDynamicDisplayOrder,
                WeightUnit = portalViewModel.WeightUnit,
                CurrencyTypeId = portalViewModel.CurrencyTypeId,
                OrderStatus = portalViewModel.DefaultOrderStateName,
                DefaultProductReviewStateName = portalViewModel.DefaultProductReviewStateName,
                CurrencySuffix = portalViewModel.CurrencySuffix,
                CurrencyName = portalViewModel.CurrencyName,
                CatalogName = portalViewModel.CatalogName,
                CssName = portalViewModel.CSSName,
                ThemeName = portalViewModel.ThemeName,
                IsEnableCompare = portalViewModel.IsEnableCompare,
                CompareType = portalViewModel.CompareType,
                TabMode = portalViewModel.TabMode,
                IsMultipleCouponAllowed = portalViewModel.IsMultipleCouponCodeAllowed,
                IsEnableSinglePageCheckout = Equals(portalViewModel.IsEnableSinglePageCheckout, null) ? false : portalViewModel.IsEnableSinglePageCheckout.Value,
                EnableSslForSmtp = portalViewModel.EnableSslForSmtp,
            };
            return portalModel;
        }

        /// <summary>
        /// Creates a SelectListItem List for catalogs
        /// </summary>
        /// <param name="model">Catalog Models list</param>
        /// <returns>SelectListItem List for catalogs.</returns>
        public static List<SelectListItem> ToSelectListItems(IEnumerable<CatalogModel> model)
        {
            List<SelectListItem> catalogItems = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                catalogItems = (from item in model
                                select new SelectListItem
                                {
                                    Text = item.Name,
                                    Value = item.CatalogId.ToString(),
                                }).ToList();
            }
            return catalogItems;
        }

        /// <summary>
        /// Creates a SelectListItem List for catalogs
        /// </summary>
        /// <param name="model">Catalog Models list</param>
        /// <returns>SelectListItem List for catalogs.</returns>
        public static List<SelectListItem> ToSelectListItems(IEnumerable<PortalModel> model)
        {
            List<SelectListItem> portalItems = new List<SelectListItem>();
            model = model.OrderBy(s => s.StoreName);
            if (!Equals(model, null))
            {
                portalItems = (from item in model
                               select new SelectListItem
                               {
                                   Text = item.StoreName,
                                   Value = item.PortalId.ToString()
                               }).ToList();
            }
            portalItems[0].Selected = true;
            return portalItems;
        }

        /// <summary>
        /// Creates a SelectListItem List for Themes
        /// </summary>
        /// <param name="model">Themes Models list</param>
        /// <returns>SelectListItem List for themes.</returns>
        public static List<SelectListItem> ToSelectListItems(IEnumerable<ThemeModel> model)
        {
            List<SelectListItem> themeItems = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                themeItems = (from item in model
                              select new SelectListItem
                              {
                                  Text = item.Name,
                                  Value = item.ThemeID.ToString(),
                              }).ToList();
            }

            foreach (SelectListItem item in themeItems)
            {
                if (item.Value.Equals("1"))
                {
                    item.Selected = true;
                }
            }
            return themeItems;
        }

        /// <summary>
        /// Creates a SelectListItem List for CSS
        /// </summary>
        /// <param name="model">CSS Models list</param>
        /// <returns>SelectListItem List for CSS.</returns>
        public static List<SelectListItem> ToSelectListItems(IEnumerable<CSSModel> model)
        {
            List<SelectListItem> cssItems = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                cssItems = (from item in model
                            select new SelectListItem
                            {
                                Text = item.Name,
                                Value = item.CSSID.ToString(),
                            }).ToList();
            }
            return cssItems;
        }

        /// <summary>
        /// Creates a SelectListItem List for OrderStatus
        /// </summary>
        /// <param name="model">OrderStatus Models list</param>
        /// <returns>SelectListItem List for OrderStatus.</returns>
        public static List<SelectListItem> ToSelectListItems(IEnumerable<OrderStateModel> model)
        {
            List<SelectListItem> orderStateItems = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                orderStateItems = (from item in model
                                   select new SelectListItem
                                   {
                                       Text = item.OrderStateName,
                                       Value = item.OrderStateID.ToString(),
                                   }).ToList();
                foreach (SelectListItem orderStates in orderStateItems)
                {
                    if (orderStates.Text.Equals(MvcAdminConstants.PendingApproval))
                    {
                        orderStateItems.Remove(orderStates);
                        break;
                    }
                }
            }
            return orderStateItems;
        }
        /// <summary>
        /// Creates a SelectListItem List for ProductReviewState
        /// </summary>
        /// <param name="model">ProductReviewState Models list</param>
        /// <returns>SelectListItem List for ProductReviewState.</returns>
        public static List<SelectListItem> ToSelectListItems(IEnumerable<ProductReviewStateModel> model)
        {
            List<SelectListItem> productReviewStateItems = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                productReviewStateItems = (from item in model
                                           select new SelectListItem
                                           {
                                               Text = item.ReviewStateName,
                                               Value = item.ReviewStateID.ToString(),
                                           }).ToList();
            }
            return productReviewStateItems;
        }

        /// <summary>
        /// Creates a SelectListItem List for Locale
        /// </summary>
        /// <param name="model">Locale Models list</param>
        /// <returns>SelectListItem List for Locale.</returns>
        public static List<SelectListItem> ToSelectListItems(IEnumerable<LocaleModel> model)
        {
            List<SelectListItem> localeItems = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                localeItems = (from item in model
                               select new SelectListItem
                               {
                                   Text = item.LocaleCode,
                                   Value = item.LocaleID.ToString(),
                               }).ToList();
            }

            SetDefaultLocaleAtTop(localeItems);
            return localeItems;
        }

        /// <summary>
        /// Sets Default locale at top of the list.
        /// </summary>
        /// <param name="localeItems">List of select list items.</param>
        private static void SetDefaultLocaleAtTop(List<SelectListItem> localeItems)
        {
            SelectListItem firstElement = new SelectListItem();

            foreach (SelectListItem item in localeItems)
            {
                if (item.Text.Equals(MvcAdminConstants.DefaultLocaleCode))
                {
                    firstElement = item;
                    break;
                }
            }

            localeItems.Remove(firstElement);
            localeItems.Insert(0, firstElement);
            firstElement.Selected = true;
        }

        /// <summary>
        /// Creates a SelectListItem List for Currency Type
        /// </summary>
        /// <param name="model">Currency Type Models list</param>
        /// <returns>SelectListItem List for Currency Type.</returns>
        public static List<SelectListItem> ToSelectListItems(IEnumerable<CurrencyTypeModel> model)
        {
            List<SelectListItem> currencyTypeItems = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                currencyTypeItems = (from item in model
                                     select new SelectListItem
                                     {
                                         Text = item.Description,
                                         Value = item.CurrencyTypeID.ToString()
                                     }).ToList();
            }
            return currencyTypeItems;
        }

        /// <summary>
        /// Creates a SelectListItem List for Countires
        /// </summary>
        /// <param name="model">Countires Models list</param>
        /// <returns>SelectListItem List for Countires.</returns>
        public static List<SelectListItem> ToSelectListItems(IEnumerable<CountryModel> model)
        {
            List<SelectListItem> countryItems = new List<SelectListItem>();

            List<CountryModel> countryList = model.ToList();

            if (!Equals(model, null) && countryList.Contains(countryList.Where(countryItem => countryItem.Code.Equals(MvcAdminConstants.DefaultCountry)).SingleOrDefault()))
            {
                //Removes US Country Code from country list and inserts it to first position.
                CountryModel country = countryList.Where(countryItem => countryItem.Code.Equals(MvcAdminConstants.DefaultCountry)).SingleOrDefault();
                countryList.Remove(country);
                countryList.Insert(MvcAdminConstants.ZerothIndex, country);
                countryItems = (from item in countryList
                                select new SelectListItem
                                {
                                    Text = item.Code,
                                    Value = item.Code
                                }).ToList();
            }
            return countryItems;
        }

        /// <summary>
        /// Creates a SelectListItem List for Profiles
        /// </summary>
        /// <param name="model">Profiles Models list</param>
        /// <returns>SelectListItem List for Profiles.</returns>
        public static List<SelectListItem> ToSelectListItems(IEnumerable<ProfileModel> model)
        {
            List<SelectListItem> profileItems = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                profileItems = (from item in model
                                select new SelectListItem
                                {
                                    Text = item.Name,
                                    Value = item.ProfileId.ToString()
                                }).ToList();
            }
            return profileItems;

        }

        /// <summary>
        /// Creates a SelectListItem List for Portals
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static List<SelectListItem> ToSelectListItems(IEnumerable<PortalViewModel> model)
        {
            List<SelectListItem> profileItems = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                profileItems = (from item in model
                                select new SelectListItem
                                {
                                    Text = item.StoreName,
                                    Value = item.PortalId.ToString()
                                }).ToList();
            }
            return profileItems;

        }
    }
}