﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    public class PromotionTypeViewModelMap
    {
        /// <summary>
        /// Converting Promotion type model to Promotion type view model
        /// </summary>
        /// <param name="models">The model of promotion type</param>
        /// <returns>PromotionTypeViewModel</returns>
        public static PromotionTypeViewModel ToViewModel(PromotionTypeModel model)
        {
            if (Equals(model, null))
            {
                return null;
            }

            return new PromotionTypeViewModel()
            {
                ClassName = model.ClassName,
                ClassType = model.ClassType,
                Description = model.Description,
                IsActive = model.IsActive,
                Name = model.Name,
                PromotionTypeId = model.PromotionTypeId,
            };
        }

        /// <summary>
        /// Converting Promotion type view model to Promotion type model
        /// </summary>
        /// <param name="models">The view model of promotion type</param>
        /// <returns>PromotionTypeViewModel</returns>
        public static PromotionTypeModel ToModel(PromotionTypeViewModel viewModel)
        {
            if (Equals(viewModel, null))
            {
                return null;
            }

            return new PromotionTypeModel()
            {
                ClassName = viewModel.ClassName,
                ClassType = viewModel.ClassType,
                Description = viewModel.Description,
                IsActive = viewModel.IsActive,
                Name = viewModel.Name,
                UserType = viewModel.UserType,
                PromotionTypeId = viewModel.PromotionTypeId,
            };
        }

        /// <summary>
        /// Converting Promotion type view model to Promotion type list model
        /// </summary>
        /// <param name="models">The model of promotion type</param>
        /// <returns>PromotionTypeViewModel</returns>
        public static List<PromotionTypeViewModel> ToViewModel(PromotionTypeListModel listModel)
        {
            if (Equals(listModel, null))
            {
                return null;
            }

            var list = new List<PromotionTypeViewModel>();

            foreach (PromotionTypeModel model in listModel.PromotionTypes)
            {
                list.Add(ToViewModel(model));
            }
            return list;
        }

        /// <summary>
        /// Convert PromotionTypeListModel to PromotionTypeListViewModel
        /// </summary>
        /// <param name="model">PromotionTypeListModel model</param>
        /// <returns>Returns PromotionTypeListViewModel</returns>
        public static PromotionTypeListViewModel ToListViewModel(PromotionTypeListModel model)
        {
            if (!Equals(model, null) && !Equals(model.PromotionTypes, null))
            {
                var viewModel = new PromotionTypeListViewModel()
                {
                    PromotionTypes = model.PromotionTypes.ToList().Select(
                    x => new PromotionTypeViewModel()
                    {
                        PromotionTypeId=x.PromotionTypeId,
                        Name = x.Name,
                        ClassName=x.ClassName,
                        ClassType=x.ClassType,
                        IsActive=x.IsActive,
                        Description=x.Description,
                        
                    }).ToList()
                };
                viewModel.TotalResults = Convert.ToInt32(model.TotalResults);
                viewModel.Page = Convert.ToInt32(model.PageIndex);
                viewModel.RecordPerPage = Convert.ToInt32(model.PageSize);
                viewModel.TotalPages = Convert.ToInt32(model.TotalPages);
                viewModel.TotalResults = Convert.ToInt32(model.TotalResults);
                return viewModel;
            }
            else
            {
                return new PromotionTypeListViewModel();
            }
        }

        /// <summary>
        /// This method returns the List of Promotion Type
        /// </summary>
        /// <param name="model">IEnumerable PromotionTypeModel model</param>
        /// <returns>Returns list of Promotion Type</returns>
        public static List<SelectListItem> ToListItems(IEnumerable<PromotionTypeModel> model)
        {
            List<SelectListItem> promotionType = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                promotionType = (from item in model
                                 select new SelectListItem
                                 {
                                     Text = item.Name,
                                     Value = item.Name
                                 }).ToList();
            }
            return promotionType;
        }
        
        /// <summary>
        /// This method returns the List of Tax Rule Type
        /// </summary>
        /// <param name="model">IEnumerable TaxRuleTypeModel model</param>
        /// <returns>Returns list of Tax Rule Type</returns>
        public static List<SelectListItem> ToListItems(IEnumerable<TaxRuleTypeModel> model)
        {
            List<SelectListItem> taxRuleType = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                taxRuleType = (from item in model
                                 select new SelectListItem
                                 {
                                     Text = item.Name,
                                     Value = item.Name
                                 }).ToList();
            }
            return taxRuleType;
        }

        /// <summary>
        /// This method returns the List of Supplier Type
        /// </summary>
        /// <param name="model">IEnumerable SupplierTypeModel model</param>
        /// <returns>Returns list of Supplier Type</returns>
        public static List<SelectListItem> ToListItems(IEnumerable<SupplierTypeModel> model)
        {
            List<SelectListItem> supplierType = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                supplierType = (from item in model
                                 select new SelectListItem
                                 {
                                     Text = item.Name,
                                     Value = item.Name
                                 }).ToList();
            }
            return supplierType;
        }

        /// <summary>
        /// This method returns the List of Shipping Type
        /// </summary>
        /// <param name="model">IEnumerable ShippingTypeModel model</param>
        /// <returns>Returns list of Shipping Type</returns>
        public static List<SelectListItem> ToListItems(IEnumerable<ShippingTypeModel> model)
        {
            List<SelectListItem> shippingType = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                shippingType = (from item in model
                                select new SelectListItem
                                {
                                    Text = item.Name,
                                    Value = item.Name
                                }).ToList();
            }
            return shippingType;
        }
    }
}