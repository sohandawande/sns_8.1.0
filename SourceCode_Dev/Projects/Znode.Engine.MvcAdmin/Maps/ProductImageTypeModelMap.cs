﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    public static class ProductImageTypeModelMap
    {
        /// <summary>
        /// Convert Product image model to List items view.
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="productImageTypeId"></param>
        /// <returns></returns>
        public static List<SelectListItem> ToListItemsView(IEnumerable<ProductImageTypeModel> model, int? productImageTypeId)
        {
            List<SelectListItem> lstItems = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                lstItems = (from item in model
                            select new SelectListItem
                            {
                                Text = item.Name,
                                Value = item.ProductImageTypeID.ToString(),
                                Selected = Equals(item.ProductImageTypeID, productImageTypeId),
                            }).ToList();
            }
            return lstItems;
        }

        /// <summary>
        /// Convert Alternate image view model to Api model.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static ProductAlternateImageModel ToModel(ProductAlternateImageViewModel model)
        {
            if (!Equals(model, null))
            {
                return new ProductAlternateImageModel()
                {
                    ActiveInd = model.ActiveInd,
                    AlternateThumbnailImageFile = model.AlternateThumbnailImageFile,
                    DisplayOrder = model.DisplayOrder,
                    ImageAltTag = model.ImageAltText,
                    ImageFile = model.ImageFile,
                    Name = model.Name,
                    ProductID = model.ProductId,
                    ShowOnCategoryPage = model.ShowOnCategoryPage,
                    ProductImageTypeID = model.ProductImageTypeID,
                    ReviewStateID = model.ReviewStateID,
                    ProductImageID = model.ProductImageId,
                    UserName = HttpContext.Current.User.Identity.Name,
                };
            }
            return new ProductAlternateImageModel();
        }

        /// <summary>
        /// Convert Alternate image api list model to view list model.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static ProductAlternateImageListViewModel ToListViewModel(IEnumerable<ProductAlternateImageModel> model, int? totalResult)
        {
            ImageHelper imageHelper = new ImageHelper();            
            if (!Equals(model, null))
            {
                var viewModel = new ProductAlternateImageListViewModel()
                {
                    Images = model.ToList().Select(
                    x => new ProductAlternateImageViewModel()
                   {
                       ActiveInd = x.ActiveInd,
                       AlternateThumbnailImageFile = HelperMethods.GetImageRelativePath(imageHelper.GetImageHttpPathThumbnail(x.ImageFile)),
                       DisplayOrder = x.DisplayOrder,
                       ImageAltText = x.ImageAltTag,
                       ImageFile = x.ImageFile,
                       Name = x.Name,
                       ProductId = x.ProductID,
                       ProductImageTypeID = x.ProductImageTypeID,
                       ShowOnCategoryPage = x.ShowOnCategoryPage,
                       ProductImageId = x.ProductImageID,
                       ReviewStateID = x.ReviewStateID,
                       VendorId = x.VendorId,
                       VendorName = x.VendorName,
                       ProductSku = x.ProductSku,
                       ImageTypeName = ((ProductImageType)x.ProductImageTypeID).ToString(),
                       ReviewStateString = x.ReviewStateString,
                       ImageSmallThumbnailPath = BindImagePath(x.ImageFile),

                   }).ToList()
                };
                viewModel.TotalResults = Convert.ToInt32(totalResult);


                return viewModel;
            }
            return new ProductAlternateImageListViewModel();
        }

        /// <summary>
        /// Convert Api product image model to  view image model.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static ProductAlternateImageViewModel ToViewModel(ProductAlternateImageModel model)
        {
            if (!Equals(model, null))
            {
                return new ProductAlternateImageViewModel()
                {
                    ActiveInd = model.ActiveInd,
                    AlternateThumbnailImageFile = model.AlternateThumbnailImageFile,
                    DisplayOrder = model.DisplayOrder,
                    ImageAltText = model.ImageAltTag,
                    ImageFile = model.ImageFile,
                    Name = model.Name,
                    ProductId = model.ProductID,
                    ProductImageTypeID = model.ProductImageTypeID,
                    ReviewStateID = model.ReviewStateID,
                    ProductImageId = model.ProductImageID,
                    ShowOnCategoryPage = model.ShowOnCategoryPage,
                    ImagePath = model.ImagePath,
                };
            }
            return new ProductAlternateImageViewModel();
        }

        private static string BindImagePath(string imageName)
        {
            string imagePath = string.Empty;
            ImageHelper imageHelper = new ImageHelper();
            imagePath = HelperMethods.GetImageRelativePath(imageHelper.GetImageHttpPathThumbnail(imageName));
            return imagePath;
        }
    }
}