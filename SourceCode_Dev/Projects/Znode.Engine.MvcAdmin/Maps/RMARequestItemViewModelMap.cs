﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    public class RMARequestItemViewModelMap
    {
        #region Public Static Methods
        /// <summary>
        /// Maps RMA request item list view model to RMA request model list.
        /// </summary>
        /// <param name="models">RMARequestItem Model</param>
        /// <returns>RMA request item list ViewModel</returns>
        public static RMARequestItemListViewModel ToListViewModel(RMARequestItemListModel models)
        {
            if (!Equals(models, null) && !Equals(models.RMARequestItemList, null))
            {
                var viewModel = new RMARequestItemListViewModel()
                {
                    RMARequestItems = models.RMARequestItemList.ToList().Select(
                    x => new RMARequestItemViewModel()
                    {
                        OrderLineItemID = x.OrderLineItemID,
                        OrderID = x.OrderID.GetValueOrDefault(),
                        ProductNum = x.ProductNum,
                        Name = x.Name,
                        Description = x.Description,
                        MaxQuantity = x.MaxQuantity.GetValueOrDefault(),
                        Price = MvcAdminUnits.GetCurrencyValue(x.Price.GetValueOrDefault()-x.DiscountAmount.GetValueOrDefault()),
                        PriceWithoutCurrencySymbol = x.Price.GetValueOrDefault()-x.DiscountAmount.GetValueOrDefault(),
                        SKU = x.SKU,
                        DiscountAmount = MvcAdminUnits.GetCurrencyValue(x.DiscountAmount.GetValueOrDefault()),
                        ShippingCost = MvcAdminUnits.GetCurrencyValue(x.ShippingCost.GetValueOrDefault()),
                        PromoDescription = x.PromoDescription,
                        SalesTax = MvcAdminUnits.GetCurrencyValue(x.SalesTax.GetValueOrDefault()),
                        RMAMaxQuantity = x.RMAMaxQuantity.GetValueOrDefault(),
                        RMAQuantity = x.RMAQuantity.GetValueOrDefault(),
                        IsReturnable = x.IsReturnable,
                        IsReceived = x.IsReceived,
                        ReasonForReturnId = x.ReasonForReturnId,
                        TaxCost = x.TaxCost.GetValueOrDefault(),
                        SubTotal = x.SubTotal.GetValueOrDefault(),
                        Total = x.Total.GetValueOrDefault(),
                        TaxCostString =MvcAdminUnits.GetCurrencyValue(x.TaxCost.GetValueOrDefault()),
                        SubTotalString = MvcAdminUnits.GetCurrencyValue(x.SubTotal.GetValueOrDefault()),
                        TotalString = MvcAdminUnits.GetCurrencyValue(x.Total.GetValueOrDefault()),
                        ResonForReturn = x.ReasonforReturn,
                        RMARequestItemID = x.RMARequestItemID,
                        AccountID = x.AccountID,
                        GCExpirationPeriod = x.GCExpirationPeriod.GetValueOrDefault(),
                        Quantity = x.Quantity
                    }).ToList()
                };
                viewModel.Page = Convert.ToInt32(models.PageIndex);
                viewModel.RecordPerPage = Convert.ToInt32(models.PageSize);
                viewModel.TotalPages = Convert.ToInt32(models.TotalPages);
                viewModel.TotalResults = Convert.ToInt32(models.TotalResults);
                return viewModel;
            }
            else
            {
                return new RMARequestItemListViewModel();
            }
        }

        /// <summary>
        /// Converts RMARequestItemModel to RMARequestItemViewModel.
        /// </summary>
        /// <param name="model">RMARequestItemViewModel to be converted from agent level.</param>
        /// <returns>Equivalent RMARequestItemViewModel of RMARequestItemModel.</returns>
        public static RMARequestItemViewModel ToViewModel(RMARequestItemModel model)
        {
            if (Equals(model, null))
            {
                return null;
            }

            return new RMARequestItemViewModel()
            {
                RMARequestItemID = model.RMARequestItemID,
                RMArequestId = model.RMARequestId.GetValueOrDefault(),
                GiftCardId = model.GiftCardId.GetValueOrDefault(),
                Quantity = model.Quantity,
                OrderLineItemID = model.OrderLineItemID,
                TransactionId = model.TransactionId,
                OrderID = model.OrderID.GetValueOrDefault(),
                ProductNum = model.ProductNum,
                Name = model.Name,
                Description = model.Description,
                MaxQuantity = model.MaxQuantity.GetValueOrDefault(),
                Price = model.Price.GetValueOrDefault().ToString("c"),
                SKU = model.SKU,
                DiscountAmount = model.DiscountAmount.GetValueOrDefault().ToString("c"),
                PromoDescription = model.PromoDescription,
                SalesTax = model.SalesTax.GetValueOrDefault().ToString("c"),
                RMAMaxQuantity = model.RMAMaxQuantity.GetValueOrDefault(),
                RMAQuantity = model.RMAQuantity.GetValueOrDefault(),
                IsReturnable = model.IsReturnable,
                IsReceived = model.IsReceived,
                ReasonForReturnId = model.ReasonForReturnId,
                TaxCost = model.TaxCost.GetValueOrDefault(),
                SubTotal = model.SubTotal.GetValueOrDefault(),
                Total = model.Total.GetValueOrDefault(),
                ResonForReturn = model.ReasonforReturn
            };
        }

        /// <summary>
        /// Converts RMARequestItemViewModel to RMARequestItemModel.
        /// </summary>
        /// <param name="viewModel">RMARequestItemViewModel to be converted from agent level.</param>
        /// <returns>Equivalent RMARequestItemModel of RMARequestItemViewModel.</returns>
        public static RMARequestItemModel ToModel(RMARequestItemViewModel viewModel)
        {
            if (Equals(viewModel, null))
            {
                return null;
            }

            RMARequestItemModel rmaRequestItemModel = new RMARequestItemModel();

            rmaRequestItemModel.OrderLineItemID = viewModel.OrderLineItemID;
            rmaRequestItemModel.OrderID = viewModel.OrderID;
            rmaRequestItemModel.ProductNum = string.IsNullOrEmpty(viewModel.ProductNum) ? string.Empty : viewModel.ProductNum;
            rmaRequestItemModel.Name = string.IsNullOrEmpty(viewModel.Name) ? string.Empty : viewModel.Name;
            rmaRequestItemModel.Description = string.IsNullOrEmpty(viewModel.Description) ? string.Empty : viewModel.Description;
            rmaRequestItemModel.MaxQuantity = viewModel.MaxQuantity.GetValueOrDefault();
            rmaRequestItemModel.Price = Convert.ToDecimal(string.IsNullOrEmpty(viewModel.Price) ? null : viewModel.Price.Trim('$'));
            rmaRequestItemModel.SKU = string.IsNullOrEmpty(viewModel.SKU) ? string.Empty : viewModel.SKU;
            rmaRequestItemModel.DiscountAmount = Convert.ToDecimal(string.IsNullOrEmpty(viewModel.DiscountAmount) ? "0.00" : viewModel.DiscountAmount.Trim('$'));
            rmaRequestItemModel.PromoDescription = string.IsNullOrEmpty(viewModel.PromoDescription) ? string.Empty : viewModel.PromoDescription;
            rmaRequestItemModel.SalesTax = Convert.ToDecimal(string.IsNullOrEmpty(viewModel.SalesTax) ? "0.00" : viewModel.SalesTax.Trim('$'));
            rmaRequestItemModel.RMAMaxQuantity = viewModel.RMAMaxQuantity;
            rmaRequestItemModel.RMAQuantity = viewModel.RMAQuantity;
            rmaRequestItemModel.IsReturnable = viewModel.IsReturnable;
            rmaRequestItemModel.IsReceived = viewModel.IsReceived;
            rmaRequestItemModel.ReasonForReturnId = viewModel.ReasonForReturnId;
            rmaRequestItemModel.TaxCost = viewModel.TaxCost;
            rmaRequestItemModel.SubTotal = viewModel.SubTotal;
            rmaRequestItemModel.Total = viewModel.Total;
            rmaRequestItemModel.ReasonforReturn = string.IsNullOrEmpty(viewModel.ResonForReturn) ? string.Empty : viewModel.ResonForReturn;
            rmaRequestItemModel.GiftCardId = viewModel.GiftCardId;
            rmaRequestItemModel.RMARequestId = viewModel.RMArequestId;
            rmaRequestItemModel.Quantity = viewModel.Quantity;

            return rmaRequestItemModel;
        } 
        #endregion
    }
}