﻿using System;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    /// <summary>
    /// Tax Class View Model Map
    /// </summary>
    public class TaxClassViewModelMap
    {
        /// <summary>
        /// Convert TaxClassViewModel to TaxClassModel
        /// </summary>
        /// <param name="model">Model of type TaxClassViewModel</param>
        /// <returns>Returns the model of Tax Class</returns>
        public static TaxClassModel ToModel(TaxClassViewModel model)
        {
            if (Equals(model, null))
            {
                return null;
            }

            return new TaxClassModel()
            {
                Name = model.Name,
                ActiveInd = model.IsActive, 
                PortalId=model.PortalId,
                DisplayOrder = Equals(model.DisplayOrder, null) ? 0 : Convert.ToInt32(model.DisplayOrder),                
            };
        }
        /// <summary>
        /// Convert TaxClassModel to TaxClassViewModel
        /// </summary>
        /// <param name="model">Model of type TaxClassModel</param>
        /// <returns>Returns the view model of Tax Class</returns>
        public static TaxClassViewModel ToViewModel(TaxClassModel model)
        {
            if (Equals(model, null))
            {
                return null;
            }
            return new TaxClassViewModel()
            {
                Name = model.Name,
                IsActive = model.ActiveInd,
                DisplayOrder = Equals(model.DisplayOrder, null) ? 0 : Convert.ToInt32(model.DisplayOrder),
                taxRuleList = TaxRulesViewModelMap.ToListViewModel(model.TaxRuleList,1,10),
                PortalId = model.PortalId,
                TaxClassId = model.TaxClassId, 
                CustomErrorMessage = model.CustomErrorMessage,
                TotalResults= model.TaxRuleList.TotalResults.Value,
            };            
        }

        /// <summary>
        /// Converts collection of TaxClass model to Taxclasslistview model.
        /// </summary>
        /// <param name="models">Collection of taxclass model</param>
        /// <returns>Returns mapped collection of TaxClass model to Taxclasslistview model.</returns>
        public static TaxClassListViewModel ToListViewModel(TaxClassListModel model, int? pageIndex, int? recordPerPage)
        {
            if (!Equals(model, null))
            {
                var viewModel = new TaxClassListViewModel()
                {
                    TaxClasses = model.TaxClasses.ToList().Select(
                    x => new TaxClassViewModel()
                    {
                       DisplayOrder=x.DisplayOrder,
                       ExternalId = x.ExternalId,
                       IsActive = x.ActiveInd,
                       Name = x.Name,
                       PortalId = x.PortalId,
                       TaxClassId = x.TaxClassId, 
                       StoreName = x.StoreName,
                       taxRuleList = TaxRulesViewModelMap.ToListViewModel(x.TaxRuleList,pageIndex,recordPerPage),       
                    }).ToList()
                };
                viewModel.RecordPerPage = recordPerPage ?? 10;
                viewModel.Page = pageIndex ?? 1;
                viewModel.TotalResults = Convert.ToInt32(model.TotalResults);
                return viewModel;
            }
            return new TaxClassListViewModel();
        } 
    }
}