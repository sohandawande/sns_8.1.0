﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    public class ProductAttributesViewModelMap
    {

        public static ProductAttributesViewModel ToViewModel(Collection<AttributeModel> attributes, ProductModel product)
        {
            var viewModel = new ProductAttributesViewModel()
            {
                ProductId = product.ProductId,
            };
            var attributeTypes = attributes.Where(model => model.IsActive).Select(model => model.AttributeTypeId).Distinct();

            foreach (var attributTypeId in attributeTypes)
            {
                var type = new AttributeTypesViewModel();
                var attributesValues = attributes.Where(model => model.AttributeTypeId.Equals(attributTypeId)).ToList();

                foreach (var attributesValue in attributesValues)
                {
                    var skuIds =
                        product.Skus.Where(
                            s =>
                            s.Attributes.Any(
                                x =>
                                x.AttributeId == attributesValue.AttributeId &&
                                x.AttributeTypeId == attributesValue.AttributeTypeId)).Select(x => x.SkuId);

                    var attribute = AttributesViewModelMap.ToEntity(attributesValue);

                    if (skuIds.Any())
                        attribute.SkuIds = new Collection<int>(skuIds.ToList());

                    type.ProductAttributes.Add(attribute);
                }

                type.Name = attributesValues.FirstOrDefault().AttributeType.Name;
                type.AttributeTypeId = type.ProductAttributes.First().AttributeTypeId;

                viewModel.Attributes.Add(type);
            }
            return viewModel;

        }
    }
}