﻿using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    /// <summary>
    /// Static Class for Provider Engine mapper
    /// </summary>
    public static class ProviderEngineViewModelMap
    {
        /// <summary>
        /// Convert ProviderEngineViewModel to PromotionTypeModel
        /// </summary>
        /// <param name="providerEngineViewModel">ProviderEngineViewModel providerEngineViewModel</param>
        /// <returns>Return PromotionTypeModel</returns>
        public static PromotionTypeModel ToPromotionTypeModel(ProviderEngineViewModel providerEngineViewModel)
        {
            if (Equals(providerEngineViewModel, null))
            {
                return null;
            }

            return new PromotionTypeModel()
            {
                Name = providerEngineViewModel.Name,
                ClassName = providerEngineViewModel.ClassName,
                ClassType = providerEngineViewModel.ClassType,
                Description = providerEngineViewModel.Description,
                IsActive = providerEngineViewModel.IsActive
            };
        }

        /// <summary>
        /// Convert PromotionTypeModel to ProviderEngineViewModel
        /// </summary>
        /// <param name="promotionTypeModel">PromotionTypeModel promotionTypeModel</param>
        /// <returns>Return ProviderEngineViewModel</returns>
        public static ProviderEngineViewModel ToViewModel(PromotionTypeModel promotionTypeModel)
        {
            if (Equals(promotionTypeModel, null))
            {
                return null;
            }

            return new ProviderEngineViewModel()
            {
                Id = promotionTypeModel.PromotionTypeId,
                Name = promotionTypeModel.Name,
                ClassName = promotionTypeModel.ClassName,
                Description = promotionTypeModel.Description,
                IsActive = promotionTypeModel.IsActive,
                ClassType = promotionTypeModel.ClassType,
            };
        }

        /// <summary>
        /// Convert TaxRuleTypeModel to ProviderEngineViewModel
        /// </summary>
        /// <param name="taxRuleTypeModel">TaxRuleTypeModel taxRuleTypeModel</param>
        /// <returns>Returns ProviderEngineViewModel</returns>
        public static ProviderEngineViewModel ToViewModel(TaxRuleTypeModel taxRuleTypeModel)
        {
            if (Equals(taxRuleTypeModel, null))
            {
                return null;
            }

            return new ProviderEngineViewModel()
            {
                Id = taxRuleTypeModel.TaxRuleTypeId,
                Name = taxRuleTypeModel.Name,
                ClassName = taxRuleTypeModel.ClassName,
                Description = taxRuleTypeModel.Description,
                IsActive = taxRuleTypeModel.IsActive,
            };
        }

        public static TaxRuleTypeModel ToTaxRuleTypeModel(ProviderEngineViewModel providerEngineViewModel)
        {
            if (Equals(providerEngineViewModel, null))
            {
                return null;
            }

            return new TaxRuleTypeModel()
            {
                Name = providerEngineViewModel.Name,
                ClassName = providerEngineViewModel.ClassName,
                Description = providerEngineViewModel.Description,
                IsActive = providerEngineViewModel.IsActive
            };
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="taxRuleTypeModel"></param>
        /// <returns></returns>
        public static ProviderEngineViewModel ToViewModel(SupplierTypeModel supplierTypeModel)
        {
            if (Equals(supplierTypeModel, null))
            {
                return null;
            }

            return new ProviderEngineViewModel()
            {
                Id = supplierTypeModel.SupplierTypeId,
                Name = supplierTypeModel.Name,
                ClassName = supplierTypeModel.ClassName,
                Description = supplierTypeModel.Description,
                IsActive = supplierTypeModel.IsActive,
            };
        }

        /// <summary>
        /// Convert ProviderEngineViewModel to SupplierTypeModel
        /// </summary>
        /// <param name="providerEngineViewModel">ProviderEngineViewModel providerEngineViewModel</param>
        /// <returns>Return SupplierTypeModel</returns>
        public static SupplierTypeModel ToSupplierTypeModel(ProviderEngineViewModel providerEngineViewModel)
        {
            if (Equals(providerEngineViewModel, null))
            {
                return null;
            }

            return new SupplierTypeModel()
            {
                Name = providerEngineViewModel.Name,
                ClassName = providerEngineViewModel.ClassName,
                Description = providerEngineViewModel.Description,
                IsActive = providerEngineViewModel.IsActive
            };
        }

        /// <summary>
        /// Convert ShippingTypeModel to ProviderEngineViewModel
        /// </summary>
        /// <param name="shippingTypeModel">ShippingTypeModel shippingTypeModel</param>
        /// <returns>Return ProviderEngineViewModel</returns>
        public static ProviderEngineViewModel ToViewModel(ShippingTypeModel shippingTypeModel)
        {
            if (Equals(shippingTypeModel, null))
            {
                return null;
            }

            return new ProviderEngineViewModel()
            {
                Id = shippingTypeModel.ShippingTypeId,
                Name = shippingTypeModel.Name,
                ClassName = shippingTypeModel.ClassName,
                Description = shippingTypeModel.Description,
                IsActive = shippingTypeModel.IsActive,
            };
        }

        /// <summary>
        /// Convert ProviderEngineViewModel to ShippingTypeModel
        /// </summary>
        /// <param name="providerEngineViewModel">ProviderEngineViewModel providerEngineViewModel</param>
        /// <returns>Return ShippingTypeModel</returns>
        public static ShippingTypeModel ToShippingTypeModel(ProviderEngineViewModel providerEngineViewModel)
        {
            if (Equals(providerEngineViewModel, null))
            {
                return null;
            }

            return new ShippingTypeModel()
            {
                Name = providerEngineViewModel.Name,
                ClassName = providerEngineViewModel.ClassName,
                Description = providerEngineViewModel.Description,
                IsActive = providerEngineViewModel.IsActive
            };
        }
    }
}