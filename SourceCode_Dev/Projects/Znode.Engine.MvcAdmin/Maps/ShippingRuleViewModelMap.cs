﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    /// <summary>
    ///  Static Class for Shipping View Model Mapper 
    /// </summary>
    public static class ShippingRuleViewModelMap
    {
        /// <summary>
        /// Convert ShippingRuleModel to ShippingRuleViewModel
        /// </summary>
        /// <param name="shippingRuleModel">to convert to view model</param>
        /// <returns>ShippingRuleViewModel</returns>
        public static ShippingRuleViewModel ToViewModel(ShippingRuleModel shippingRuleModel)
        {
            if (!Equals(shippingRuleModel, null))
            {
                return new ShippingRuleViewModel()
                {
                    ShippingRuleTypeId = shippingRuleModel.ShippingRuleTypeId,
                    ShippingRuleId = shippingRuleModel.ShippingRuleId,
                    BaseCost = shippingRuleModel.BaseCost,
                    PerItemCost = shippingRuleModel.PerItemCost,
                    LowerLimit = shippingRuleModel.LowerLimit,
                    UpperLimit = shippingRuleModel.UpperLimit,
                    shippingId = shippingRuleModel.ShippingOptionId,
                    customErrorMessage = shippingRuleModel.errorMessage,
                };
            }
            return null;
        }

        /// <summary>
        /// Convert ShippingRuleViewModel to ShippingRuleModel
        /// </summary>
        /// <param name="shippingRuleViewModel">to convert to model</param>
        /// <returns>ShippingRuleModel</returns>
        public static ShippingRuleModel ToModel(ShippingRuleViewModel shippingRuleViewModel)
        {
            if (!Equals(shippingRuleViewModel, null))
            {
                return new ShippingRuleModel()
                {
                    ShippingRuleTypeId = shippingRuleViewModel.ShippingRuleTypeId,
                    ShippingRuleId = shippingRuleViewModel.ShippingRuleId,
                    BaseCost = shippingRuleViewModel.BaseCost,
                    PerItemCost = shippingRuleViewModel.PerItemCost,
                    LowerLimit = shippingRuleViewModel.LowerLimit,
                    UpperLimit = shippingRuleViewModel.UpperLimit,
                    ShippingOptionId = shippingRuleViewModel.shippingId,
                    errorMessage = shippingRuleViewModel.customErrorMessage,
                    ShippingOption = ShippingOptionViewModelMap.ToModel(shippingRuleViewModel.ShippingOption)
                };
            }
            return null;
        }

        /// <summary>
        /// Convert ShippingRuleListModel to ShippingRuleListViewModel
        /// </summary>
        /// <param name="models">to convert to list view model</param>
        /// <returns>ShippingRuleListViewModel</returns>
        public static ShippingRuleListViewModel ToListViewModel(ShippingRuleListModel models, int? pageIndex, int? recordPerPage)
        {
            if (!Equals(models, null))
            {
                var viewModel = new ShippingRuleListViewModel()
                {
                    ShippingRule = models.ShippingRules.ToList().Select(
                    x => new ShippingRuleViewModel()
                    {
                        ShippingRuleTypeId = x.ShippingRuleTypeId,
                        ShippingRuleId = x.ShippingRuleId,
                        BaseCost = x.BaseCost,
                        PerItemCost = x.PerItemCost,
                        LowerLimit = x.LowerLimit,
                        UpperLimit = x.UpperLimit
                    }).ToList()
                };
                viewModel.TotalResults = Convert.ToInt32(models.TotalResults);
                viewModel.Page = Convert.ToInt32(models.PageIndex);
                viewModel.RecordPerPage = Convert.ToInt32(models.PageSize);
                viewModel.TotalPages = Convert.ToInt32(models.TotalPages);

                return viewModel;
            }
            return new ShippingRuleListViewModel();
        }

        /// <summary>
        /// Convert IEnumerable<ShippingRuleTypeModel> to List<SelectListItem>
        /// </summary>
        /// <param name="model">to convert to list item type</param>
        /// <returns>List<SelectListItem></returns>
        public static List<SelectListItem> ToListItems(IEnumerable<ShippingRuleTypeModel> model)
        {
            List<SelectListItem> shippingRuleTypeItems = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                shippingRuleTypeItems = (from item in model
                                         select new SelectListItem
                                         {
                                             Text = item.Name,
                                             Value = item.ShippingRuleTypeId.ToString(),
                                         }).ToList();
            }
            return shippingRuleTypeItems;
        }
    }
}