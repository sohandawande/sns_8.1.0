﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    public class CustomerViewModelMap
    {
        /// <summary>
        /// Convert Address List Model to Address View Model.
        /// </summary>
        /// <param name="model">IEnumerable<AddressModel> model</param>
        /// <param name="totalResult">int? totalResult</param>
        /// <returns>Return Address List in AddressListViewModel format.</returns>
        public static AddressListViewModel ToCustomerAddressList(IEnumerable<AddressModel> model, string EmailId, int? totalResult = 0)
        {
            if (!Equals(model, null))
            {
                var viewModel = new AddressListViewModel()
                {
                    AccountAddress = model.ToList().Select(
                    x => new AddressViewModel()
                    {
                        AccountId = x.AccountId,
                        AddressId = x.AddressId,
                        Name = x.Name,
                        Email = EmailId,
                        IsDefaultBilling = x.IsDefaultBilling,
                        IsDefaultShipping = x.IsDefaultShipping,
                        FirstName = x.FirstName,
                        LastName = x.LastName,
                        CompanyName = x.CompanyName,
                        PhoneNumber = x.PhoneNumber,
                        StreetAddress1 = x.StreetAddress1,
                        StreetAddress2 = x.StreetAddress2,
                        PostalCode = x.PostalCode,
                        CountryCode = x.CountryCode,
                        StateCode = x.StateCode,
                        City = x.City,
                        FullAddress = FullAddress(x)

                    }).ToList()
                };
                viewModel.TotalResults = Convert.ToInt32(totalResult);
                return viewModel;
            }
            return new AddressListViewModel();
        }

        /// <summary>
        /// Convert Customer View Model to Account Model.
        /// </summary>
        /// <param name="model">model for the Customer View.</param>
        /// <returns>Return Account Details in AccountModel Format.</returns>
        public static AccountModel ToAccountModel(CustomerViewModel model)
        {

            return new AccountModel()
            {
                AccountId = model.CustomerAccount.AccountId,
                Email = model.CustomerAccount.EmailAddress,
                User = new UserModel()
                {
                    Username = model.CustomerAccount.UserName,
                    Email = model.CustomerAccount.EmailAddress,
                    UserId = Equals(model.CustomerAccount.UserId, null) ? Guid.Empty : (Guid)model.CustomerAccount.UserId,
                },
                CompanyName = HttpUtility.HtmlEncode(model.CustomerAccount.CompanyName),
                EmailOptIn = model.CustomerAccount.EmailOptIn,
                EnableCustomerPricing = model.CustomerAccount.EnableCustomerPricing,
                ExternalId = string.IsNullOrEmpty(model.CustomerAccount.ExternalId) ? null : HttpUtility.HtmlEncode(model.CustomerAccount.ExternalId.Trim()),
                IsActive = model.CustomerAccount.IsActive,
                ProfileId = model.CustomerAccount.ProfileId,
                PortalId = model.CustomerAccount.PortalId,
                UserId = model.CustomerAccount.UserId,
                UpdateUser = HttpContext.Current.User.Identity.Name,
                CreateUser = model.CustomerAccount.CreateUser ?? HttpContext.Current.User.Identity.Name,
                Description = HttpUtility.HtmlEncode(model.CustomerAccount.Description),
                Website = HttpUtility.HtmlEncode(model.CustomerAccount.Website),
                Source = HttpUtility.HtmlEncode(model.CustomerAccount.Source),
                //AccountProfileCode = model.CustomerAccount.AccountProfileCode ?? DBNull.Value.ToString(CultureInfo.InvariantCulture),
                CreateDate = model.CustomerAccount.CreateDte,
                UpdateDate = DateTime.Now,
                UserName = model.CustomerAccount.UserName,
                Custom1 = (!Equals(model.CustomerAccount.Custom1, null) ? HttpUtility.HtmlEncode(model.CustomerAccount.Custom1.Trim()) : string.Empty),
                Custom2 = (!Equals(model.CustomerAccount.Custom2, null) ? HttpUtility.HtmlEncode(model.CustomerAccount.Custom2.Trim()) : string.Empty),
                Custom3 = (!Equals(model.CustomerAccount.Custom3, null) ? HttpUtility.HtmlEncode(model.CustomerAccount.Custom3.Trim()) : string.Empty),
                ParentAccountId = model.CustomerAccount.ParentAccountID, //PRFT Custom Code               
            };

        }

        /// <summary>
        /// To download customer pricing .
        /// </summary>
        /// <param name="models">IEnumerable CustomerModel</param>
        /// <param name="totalResults">No of records</param>
        /// <param name="recordPerPage">count of record per page</param>
        /// <param name="pageIndex">page index </param>
        /// <param name="totalPages">total pages</param>
        /// <returns>CustomerDownloadListViewModel</returns>
        public static CustomerDownloadListViewModel ToDownload(IEnumerable<CustomerModel> models, int? totalResults = 0, int? recordPerPage = 0, int? pageIndex = 0, int? totalPages = 0)
        {
            if (!Equals(models, null) && models.Count() > 0)
            {
                var viewModel = new CustomerDownloadListViewModel()
                {
                    customerList = models.ToList().Select(
                    x => new CustomerDownloadViewModel()
                    {
                        AccountId = x.AccountId,
                        UserId = x.UserId,
                        Email = x.Email,
                        FirstName = x.FirstName,
                        LastName = x.LastName,
                        EnableCustomerPricing = Equals(x.EnableCustomerPricing, null) ? false : true,

                        PhoneNumber = x.PhoneNumber,
                    }).ToList()
                };
                viewModel.RecordPerPage = recordPerPage ?? 10;
                viewModel.Page = pageIndex ?? 1;
                viewModel.TotalPages = Convert.ToInt32(totalPages);
                viewModel.TotalResults = (Equals(totalResults, 0)) ? Convert.ToInt32(viewModel.TotalResults) : Convert.ToInt32(totalResults);
                return viewModel;
            }
            return new CustomerDownloadListViewModel();
        }

        /// <summary>
        /// To Convert Model To List View Model.
        /// </summary>
        /// <param name="models">IEnumerable CustomerModel  </param>
        /// <param name="totalResults">No of records</param>
        /// <param name="recordPerPage">count of record per page</param>
        /// <param name="pageIndex">page index </param>
        /// <param name="totalPages">total pages</param>
        /// <returns>CustomerListViewModel</returns>
        public static CustomerListViewModel ToListViewModel(IEnumerable<CustomerModel> models, int? totalResults = 0, int? recordPerPage = 0, int? pageIndex = 0, int? totalPages = 0)
        {
            if (!Equals(models, null) && models.Count() > 0)
            {
                var viewModel = new CustomerListViewModel()
                {
                    Customers = models.ToList().Select(
                    x => new CustomerViewModel()
                    {
                        Address = new AddressViewModel()
                        {
                            FirstName = x.FirstName,
                            LastName = x.LastName,
                            AccountId = x.AccountId,
                            Email = x.Email,
                            City = x.City,
                            PostalCode = x.PostalCode,
                            StateCode = x.StateCode
                        },
                        PhoneNumber = x.PhoneNumber,
                        FullName = x.FullName,
                        AccountId = x.AccountId,
                        CompanyName = x.CompanyName,
                        LoginName = x.UserName,
                        IsConfirmed = x.IsConfirmed,
                        CustomerAccount=new AccountViewModel()
                        {
                            ExternalId = (x.ExternalAccountNo),
                        },
                        EnableCustomerPricing = Equals(x.EnableCustomerPricing, null) ? false : x.EnableCustomerPricing,
                        Custom1 = x.Custom1,
                        Custom2 = x.Custom2,
                        //Custom3=x.Custom3 //PRFT Custom Code
                        CustomerUserMappingId=x.CustomerUserMappingId,
                    }).ToList()
                };

                viewModel.RecordPerPage = recordPerPage ?? 10;
                viewModel.Page = pageIndex ?? 1;
                viewModel.TotalPages = Convert.ToInt32(totalPages);
                viewModel.TotalResults = (Equals(totalResults, 0)) ? Convert.ToInt32(viewModel.TotalResults) : Convert.ToInt32(totalResults);
                return viewModel;
            }
            return new CustomerListViewModel();
        }

        /// <summary>
        /// Map CustomerAffiliateViewModel to CustomerAffiliateModel
        /// </summary>
        /// <param name="model">CustomerAffiliateModel</param>
        /// <returns>CustomerAffiliateViewModel</returns>
        public static CustomerAffiliateViewModel ToCustomerAffiliateViewModel(CustomerAffiliateModel model)
        {
            if (!Equals(model, null) && !Equals(model.AccountInformation, null))
            {
                CustomerAffiliateViewModel customerAffiliateViewModel = new CustomerAffiliateViewModel()
                {
                    AccountId = model.AccountInformation.AccountId,
                    ReferralCommisionTypeId = model.AccountInformation.ReferralCommissionTypeId,
                    ReferalStatus = Equals(model.AccountInformation.ReferralStatus, null) ? MvcAdminConstants.Inactive : GetApprovalStatusDictionary().ContainsKey(model.AccountInformation.ReferralStatus) ? GetApprovalStatusDictionary()[model.AccountInformation.ReferralStatus] : model.AccountInformation.ReferralStatus,
                    Status = Equals(model.AccountInformation.ReferralStatus, null) ? MvcAdminConstants.Inactive : model.AccountInformation.ReferralStatus,
                    TaxID = model.AccountInformation.TaxId,
                    TrackingLink = model.AffiliateLink,
                    AmountOwed = MvcAdminUnits.GetCurrencyValue(model.AmountOwed.GetValueOrDefault()),
                    ReferralCommissionType = model.AccountInformation.ReferralCommissionType,
                    ReferralCommission = HelperMethods.FormatPrice(model.AccountInformation.ReferralCommission)
                };
                return customerAffiliateViewModel;
            }
            else
            {
                return new CustomerAffiliateViewModel();
            }
        }



        /// <summary>
        /// This method returns the manufacturers List
        /// </summary>
        /// <param name="model">The parameter is ReferralCommissionTypeModel Model</param>
        /// <returns>list of all manufacturers</returns>
        public static List<SelectListItem> ToListItems(IEnumerable<ReferralCommissionTypeModel> model)
        {
            List<SelectListItem> referralCommissionTypItems = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                referralCommissionTypItems = (from item in model
                                              select new SelectListItem
                                              {
                                                  Text = item.ReferralCommissionTypeName,
                                                  Value = item.ReferralCommissionTypeId.ToString(),
                                              }).ToList();
            }
            return referralCommissionTypItems;
        }

        /// <summary>
        /// Bind Lst of Approval status to dropdown
        /// </summary>
        /// <param name="status">string status</param>
        /// <returns>List of Approval Status in SelectListItem type</returns>
        public static List<SelectListItem> GetApprovalStatus(string status)
        {
            List<SelectListItem> statusList = new List<SelectListItem>();

            statusList = (from item in GetApprovalStatusDictionary()
                          select new SelectListItem
                          {
                              Text = item.Value,
                              Value = item.Key,
                              Selected = Equals(item.Key, status),
                          }).ToList();

            return statusList;
        }

        /// <summary>
        /// Map AccountPaymentListModel TO AccountPaymentListViewModel
        /// </summary>
        /// <param name="listModel">AccountPaymentListModel</param>
        /// <returns>AccountPaymentListViewModel</returns>
        public static AccountPaymentListViewModel ToListViewModel(AccountPaymentListModel listModel)
        {
            if (!Equals(listModel, null) && !Equals(listModel.AccountPayments, null))
            {
                var viewModel = new AccountPaymentListViewModel()
                {
                    AccountPaymentList = listModel.AccountPayments.Select(
                    model => new AccountPaymentViewModel()
                    {
                        AccountId = model.AccountId,
                        AccountPaymentId = model.AccountPaymentId,
                        OrderId = Equals(model.OrderId, null) ? null : model.OrderId,
                        TransactionId = Equals(model.Description, null) ? string.Empty : model.Description,
                        Description = Equals(model.Description, null) ? string.Empty : model.Description,
                        DisplayPaymentDate = HelperMethods.ViewDateFormat(model.PaymentDate.Value),
                        DisplayAmount = Equals(model.Amount, null) ? string.Empty : MvcAdminUnits.GetCurrencyValue(model.Amount.Value),

                    }).ToList()
                };
                viewModel.Page = Convert.ToInt32(listModel.PageIndex);
                viewModel.RecordPerPage = Convert.ToInt32(listModel.PageSize);
                viewModel.TotalPages = Convert.ToInt32(listModel.TotalPages);
                viewModel.TotalResults = Convert.ToInt32(listModel.TotalResults);
                return viewModel;
            }
            else
            {
                return new AccountPaymentListViewModel();
            }
        }

        /// <summary>
        /// Map ReferralCommissionListModel TO ReferralCommissionListViewModel
        /// </summary>
        /// <param name="listModel">ReferralCommissionListModel</param>
        /// <returns>ReferralCommissionListViewModel</returns>
        public static ReferralCommissionListViewModel ToListViewModel(ReferralCommissionListModel listModel)
        {
            if (!Equals(listModel, null) && !Equals(listModel.ReferralCommissions, null))
            {
                var viewModel = new ReferralCommissionListViewModel()
                {
                    ReferralCommissions = listModel.ReferralCommissions.Select(
                    model => new ReferralCommissionViewModel()
                    {
                        ReferralCommissionId = model.ReferralCommissionId,
                        ReferralCommissionTypeId = model.ReferralCommissionTypeId,
                        ReferralAccountId = model.ReferralAccountId,
                        ReferralCommission = HelperMethods.FormatPrice(model.ReferralCommission),
                        OrderId = model.OrderId,
                        TransactionId = model.TransactionId,
                        Description = model.Description,
                        CommissionAmount = model.CommissionAmount,
                        ReferralCommissionType = model.ReferralCommissionType,
                        DisplayCommissionAmount = MvcAdminUnits.GetCurrencyValue(model.CommissionAmount.Value)
                    }).ToList()
                };
                viewModel.Page = Convert.ToInt32(listModel.PageIndex);
                viewModel.RecordPerPage = Convert.ToInt32(listModel.PageSize);
                viewModel.TotalPages = Convert.ToInt32(listModel.TotalPages);
                viewModel.TotalResults = Convert.ToInt32(listModel.TotalResults);
                return viewModel;
            }
            else
            {
                return new ReferralCommissionListViewModel();
            }
        }

        /// <summary>
        ///Map AccountPaymentModel to AccountPaymentViewModel
        /// </summary>
        /// <param name="model">AccountPaymentModel</param>
        /// <returns>AccountPaymentViewModel</returns>
        public static AccountPaymentViewModel ToAccountPaymentViewModel(AccountPaymentModel model)
        {
            if (Equals(model, null))
            {
                return null;
            }

            AccountPaymentViewModel viewModel = new AccountPaymentViewModel
            {
                AccountPaymentId = model.AccountPaymentId,
                AccountId = model.AccountId,
                OrderId = model.OrderId,
                TransactionId = model.TransactionId,
                Description = model.Description,
                PaymentDate = Equals(model.PaymentDate, null) ? null : HelperMethods.ViewDateFormat(model.PaymentDate.Value),
                Amount = model.Amount
            };
            return viewModel;
        }

        /// <summary>
        /// Map AccountPaymentViewModel to AccountPaymentModel
        /// </summary>
        /// <param name="viewModel">AccountPaymentViewModel</param>
        /// <returns>AccountPaymentModel</returns>
        public static AccountPaymentModel ToAccountPaymentModel(AccountPaymentViewModel viewModel)
        {
            if (Equals(viewModel, null))
            {
                return null;
            }

            AccountPaymentModel model = new AccountPaymentModel
            {
                AccountPaymentId = viewModel.AccountPaymentId,
                AccountId = viewModel.AccountId,
                OrderId = viewModel.OrderId,
                TransactionId = viewModel.TransactionId,
                Description = viewModel.Description,
                PaymentDate = Convert.ToDateTime(viewModel.PaymentDate),
                Amount = viewModel.Amount
            };
            return model;
        }

        #region Approval Status Dictionary

        /// <summary>
        ///Dropdown for Approval Status 
        /// </summary>
        /// <returns>List</returns>
        private static Dictionary<string, string> GetApprovalStatusDictionary()
        {
            Dictionary<string, string> approvalStatus = new Dictionary<string, string>();
            approvalStatus.Add("I", "Inactive");
            approvalStatus.Add("N", "Pending Approval");
            approvalStatus.Add("A", "Approved");
            approvalStatus.Add("D", "Declined");
            return approvalStatus;
        }

        /// <summary>
        /// To set full address of customer.
        /// </summary>
        /// <param name="Address">AddressModel contains full address information</param>
        /// <returns>Full address string</returns>
        private static string FullAddress(AddressModel Address)
        {

            StringBuilder FullName = new StringBuilder();

            string phoneNumber = " PH : ";

            if (!Equals(Address.FirstName, string.Empty))
            {
                FullName.Append(Address.FirstName);
                if (!Equals(Address.LastName, string.Empty))
                    FullName.Append(" ");
            }

            if (!Equals(Address.LastName, string.Empty))
            {
                FullName.Append(Address.LastName);
                if (!Equals(Address.CompanyName, string.Empty))
                    FullName.Append(" ");
            }

            if (!Equals(Address.CompanyName, string.Empty))
            {
                FullName.Append(", " + Address.CompanyName);
                if (!Equals(Address.StreetAddress1, string.Empty))
                    FullName.Append(" ");
            }

            if (!Equals(Address.StreetAddress1, string.Empty))
            {
                FullName.Append(", " + Address.StreetAddress1);
                if (!Equals(Address.StreetAddress2, string.Empty))
                    FullName.Append(" ");
            }

            if (!Equals(Address.StreetAddress2, string.Empty))
            {
                FullName.Append(", " + Address.StreetAddress2);
                if (!Equals(Address.City, string.Empty))
                    FullName.Append(" ");
            }

            if (!Equals(Address.City, string.Empty))
            {
                FullName.Append(", " + Address.City);
                if (!Equals(Address.StateCode, string.Empty))
                    FullName.Append(" ");
            }

            if (!Equals(Address.StateCode, string.Empty))
            {
                FullName.Append(", " + Address.StateCode);
                if (!Equals(Address.PostalCode, string.Empty))
                    FullName.Append(" ");
            }

            if (!Equals(Address.PostalCode, string.Empty))
            {
                FullName.Append(", " + Address.PostalCode);
                if (!Equals(Address.CountryCode, string.Empty))
                    FullName.Append(" ");
            }

            if (!(Equals(Address.CountryCode, string.Empty) || Equals(Address.CountryCode, null)))
            {
                FullName.Append(", " + Address.CountryCode);
                if (!Equals(Address.PhoneNumber, string.Empty))
                    FullName.Append(" ");
            }

            if (!Equals(Address.PhoneNumber, string.Empty))
            {
                FullName.Append(", " + phoneNumber);
                FullName.Append(Address.PhoneNumber);
                if (!Equals(Address.PhoneNumber, string.Empty))
                    FullName.Append(" ");
            }
            return FullName.ToString();
        }
        #endregion

        /// <summary>
        /// Get affiliated approval status.
        /// </summary>
        /// <param name="status">status value</param>
        /// <returns>List of SelectListItem</returns>
        public static List<SelectListItem> GetAffiliateApprovalStatus(string status)
        {
            List<SelectListItem> statusList = new List<SelectListItem>();

            statusList = (from item in GetAffiliateApprovalStatusDictionary()
                          where item.Key != "0"
                          select new SelectListItem
                          {
                              Text = item.Value,
                              Value = item.Key,
                              Selected = Equals(item.Key, status),
                          }).ToList();

            return statusList;
        }

        #region GetAffiliateApprovalStatusDictionary
        private static Dictionary<string, string> GetAffiliateApprovalStatusDictionary()
        {
            Dictionary<string, string> reviewStatus = new Dictionary<string, string>();
            reviewStatus.Add(MvcAdminConstants.KeyN, MvcAdminConstants.LabelPendingApproval);
            reviewStatus.Add(MvcAdminConstants.KeyA, MvcAdminConstants.ValueApproved);
            reviewStatus.Add(MvcAdminConstants.KeyI, MvcAdminConstants.ValueDeclined);
            return reviewStatus;
        }
        #endregion
    }
}