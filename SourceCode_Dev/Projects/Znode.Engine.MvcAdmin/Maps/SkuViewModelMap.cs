﻿using System.Linq;
using Resources;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.ViewModels;
using System.Web;
using System;

namespace Znode.Engine.MvcAdmin.Maps
{
    public class SkuViewModelMap
    {
        /// <summary>
        /// View Model Mapper for Sku
        /// </summary>
        /// <param name="model">SkuModel model</param>
        /// <returns>return SkuViewModel</returns>
        public static SkuViewModel ToViewModel(SkuModel model)
        {
            var viewModel = new SkuViewModel()
            {
                ProductId = model.ProductId,
                ImageAltTag = model.ImageAltTag,
                IsActive = model.IsActive,
                SKU = model.Sku,
                WeightAdditional = model.WeightAdditional,
                SKUPicturePath = model.ImageFile,
                SalePriceOverride = model.SalePriceOverride,
                RecurringBillingPeriod = model.RecurringBillingPeriod,
                RecurringBillingFrequency = model.RecurringBillingFrequency,
                RecurringBillingTotalCycles = model.RecurringBillingTotalCycles,
                RecurringBillingInitialAmount = model.RecurringBillingInitialAmount
            };
            return viewModel;
        }

        /// <summary>
        /// View Model Mapper for Sku
        /// </summary>
        /// <param name="model">ProductModel</param>
        /// <returns>returns SkuViewModel</returns>
        public static SkuViewModel ToViewModel(ProductModel model)
        {
            var viewModel = new SkuViewModel()
            {
                ProductId = model.ProductId,
                ImageAltTag = model.ImageAltTag,
                IsActive = model.IsActive,
                SKUPicturePath = model.ImageFile,
                RecurringBillingPeriod = model.RecurringBillingPeriod,
                RecurringBillingFrequency = model.RecurringBillingFrequency,
                RecurringBillingTotalCycles = model.RecurringBillingTotalCycles,
                RecurringBillingInitialAmount = model.RecurringBillingInitialAmount,
                ProductTypeId = model.ProductTypeId,
                ProductTypeName = model.ProductTypeName,
                AttributeCount = Equals(model.AttributeCount, null) ? 0 : model.AttributeCount,
                ParentChildProduct = Equals(model.IsChildProduct, null) ? false : model.IsChildProduct.Value,
                Name = Equals(model.Name, null) ? string.Empty : model.Name,
            };
            return viewModel;
        }

        /// <summary>
        /// Model Mapper for Sku
        /// </summary>
        /// <param name="viewModel">SkuViewModel</param>
        /// <returns>returns SkuModel</returns>
        public static SkuModel ToModel(SkuViewModel viewModel)
        {
            var model = new SkuModel()
            {
                SkuId = viewModel.Id,
                ProductId = viewModel.ProductId,
                ImageAltTag = viewModel.ImageAltTag,
                IsActive = (Equals(viewModel.ProductTypeName,"Default")) ? true : viewModel.IsActive,
                Sku = viewModel.SKU,
                WeightAdditional = viewModel.WeightAdditional,
                ImageFile = viewModel.SKUPicturePath,
                SalePriceOverride = viewModel.SalePriceOverride,
                WholesalePriceOverride = viewModel.WholesalePriceOverride,
                RetailPriceOverride = viewModel.RetailPriceOverride,
                RecurringBillingPeriod = viewModel.RecurringBillingPeriod,
                RecurringBillingFrequency = viewModel.RecurringBillingFrequency,
                RecurringBillingTotalCycles = viewModel.RecurringBillingTotalCycles,
                RecurringBillingInitialAmount = viewModel.RecurringBillingInitialAmount,
                AttributesIds = viewModel.ProductAttributes,
                SupplierId = viewModel.SupplierId,
                UserType=viewModel.UserType,
                UserName = HttpContext.Current.User.Identity.Name,
                Custom1=viewModel.Custom1,
            };

            model.Inventory = new InventoryModel();
            model.Inventory.Sku = viewModel.SKU;
            model.Inventory.QuantityOnHand = viewModel.QuantityOnHand;
            model.Inventory.ReorderLevel = viewModel.ReorderLevel;

            return model;
        }

        /// <summary>
        /// Model Mapper for Sku 
        /// </summary>
        /// <param name="viewModel">ProductViewModel</param>
        /// <returns>returns SkuModel</returns>
        public static SkuModel ToModel(ProductViewModel viewModel)
        {
            var model = new SkuModel()
            {
                ProductId = viewModel.ProductId,
                ImageAltTag = viewModel.ImageAltTag,
                IsActive = viewModel.IsActive,
                Sku = viewModel.Sku
            };

            model.Inventory = new InventoryModel();
            model.Inventory.Sku = viewModel.Sku;
            model.Inventory.QuantityOnHand = viewModel.QuantityOnHand;
            model.Inventory.ReorderLevel = viewModel.ReorderLevel;

            return model;
        }

        /// <summary>
        /// To map list view model
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static SkuListViewModel ToListModel(ProductSkuListModel model)
        {
            if (!Equals(model, null) && !(Equals(model.ProductSkus,null)))
            {
                var viewModel = new SkuListViewModel()
                {
                    Skus = model.ProductSkus.ToList().Select(
                    x => new SkuViewModel()
                    {
                        Id = x.SKUId,
                        SKU = x.SKU,
                        QuantityOnHand = x.QuantityOnHand,
                        ReorderLevel = x.ReorderLevel,
                        IsActive = x.IsActive,
                        ProductTypeName = x.ProductTypeName,
                        IsGiftCard = x.IsGiftCard,
                        ParentChildProduct = x.ParentChildProduct,
                        AttributeCount = x.AttributeCount,
                    }).ToList()
                };
                viewModel.RecordPerPage = model.PageSize ?? 10;
                viewModel.Page = model.PageIndex ?? 1;
                viewModel.TotalPages = Convert.ToInt32(model.TotalPages);
                viewModel.TotalResults = Convert.ToInt32(model.TotalResults);
                return viewModel;
            }
            else
            {
                return new SkuListViewModel();
            }
        }

        /// <summary>
        /// View Model Mapper for Product Sku
        /// </summary>
        /// <param name="model">ProductSkuListModel model</param>
        /// <returns>returns SkuViewModel</returns>
        public static SkuViewModel ToViewModel(ProductSkuListModel model)
        {
            if (Equals(model, null) || Equals(model.ProductSkus, null))
            {
                return null;
            }
            var viewModel = new SkuViewModel()
               {
                   ProductId = Equals(model.ProductSkus[0].ProductId, null) ? 0 : model.ProductSkus[0].ProductId,
                   Id = Equals(model.ProductSkus[0].ProductId, null) ? 0 : model.ProductSkus[0].SKUId,
                   SKU = Equals(model.ProductSkus[0].SKU, null) ? string.Empty : model.ProductSkus[0].SKU,
                   QuantityOnHand = Equals(model.ProductSkus[0].QuantityOnHand, null) ? 0 : model.ProductSkus[0].QuantityOnHand,
                   WeightAdditional = Equals(model.ProductSkus[0].WeightAdditional, null) ? 0 : model.ProductSkus[0].WeightAdditional,
                   SalePriceOverride = Equals(model.ProductSkus[0].SalePriceOverride, null) ? model.ProductSkus[0].SalePriceOverride : HelperMethods.FormatPrice(model.ProductSkus[0].SalePriceOverride),
                   RetailPriceOverride = Equals(model.ProductSkus[0].RetailPriceOverride, null) ? model.ProductSkus[0].RetailPriceOverride : HelperMethods.FormatPrice(model.ProductSkus[0].RetailPriceOverride), 
                   WholesalePriceOverride = Equals(model.ProductSkus[0].WholesalePriceOverride, null) ? model.ProductSkus[0].WholesalePriceOverride : HelperMethods.FormatPrice(model.ProductSkus[0].WholesalePriceOverride), 
                   ImageAltTag = Equals(model.ProductSkus[0].ImageAltTag, null) ? string.Empty : model.ProductSkus[0].ImageAltTag,
                   IsActive = Equals(model.ProductSkus[0].IsActive, null) ? false : model.ProductSkus[0].IsActive,
                   SKUPicturePath = Equals(model.ProductSkus[0].SKUPicturePath, null) ? string.Empty : model.ProductSkus[0].SKUPicturePath,
                   RecurringBillingPeriod = Equals(model.ProductSkus[0].RecurringBillingPeriod, null) ? string.Empty : model.ProductSkus[0].RecurringBillingPeriod,
                   RecurringBillingFrequency = Equals(model.ProductSkus[0].RecurringBillingFrequency, null) ? string.Empty : model.ProductSkus[0].RecurringBillingFrequency,
                   RecurringBillingTotalCycles = Equals(model.ProductSkus[0].RecurringBillingTotalCycles, null) ? 0 : model.ProductSkus[0].RecurringBillingTotalCycles,
                   RecurringBillingInitialAmount = Equals(model.ProductSkus[0].RecurringBillingInitialAmount, null) ? 0 : model.ProductSkus[0].RecurringBillingInitialAmount,
                   ProductTypeId = Equals(model.ProductSkus[0].ProductTypeId, null) ? 0 : model.ProductSkus[0].ProductTypeId,
                   SupplierId = Equals(model.ProductSkus[0].SupplierId, null) ? 0 : model.ProductSkus[0].SupplierId,
                   ProductAttributes = Equals(model.ProductSkus[0].AttributeIds, null) ? string.Empty : model.ProductSkus[0].AttributeIds,
                   ProductTypeName = Equals(model.ProductSkus[0].ProductTypeName, null) ? string.Empty : model.ProductSkus[0].ProductTypeName,
                   Name = Equals(model.ProductSkus[0].Name, null) ? string.Empty : model.ProductSkus[0].Name,
                   IsGiftCard = Equals(model.ProductSkus[0].IsGiftCard, null) ? false : model.ProductSkus[0].IsGiftCard,
                   AttributeCount = Equals(model.ProductSkus[0].AttributeCount, null) ? 0 : model.ProductSkus[0].AttributeCount,
                   ParentChildProduct = Equals(model.ProductSkus[0].ParentChildProduct, null) ? false : model.ProductSkus[0].ParentChildProduct,
                   ReorderLevel = model.ProductSkus[0].ReorderLevel,
                   Custom1=model.ProductSkus[0].Custom1,
               };
            if (!string.IsNullOrEmpty(viewModel.SKUPicturePath))
            {
                ImageHelper imageHelper = new ImageHelper();
                viewModel.ImageMediumPath = imageHelper.GetImageHttpPathLarge(viewModel.SKUPicturePath);
            }
            return viewModel;
        }

        public static SkuViewModel ToViewModel(ProductListModel model)
        {
            var viewModel = new SkuViewModel()
            {
                ProductId = Equals(model.Products[0].ProductId, null) ? 0 : model.Products[0].ProductId,
                Name = Equals(model.Products[0].Name, null) ? string.Empty : model.Products[0].Name,
                ProductTypeId = Equals(model.Products[0].ProductTypeId, null) ? 0 : model.Products[0].ProductTypeId,
                ProductTypeName = Equals(model.Products[0].ProductTypeName, null) ? string.Empty : model.Products[0].ProductTypeName
            };
            return viewModel;
        }
    }
}