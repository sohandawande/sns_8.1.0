﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    public class PortalProfileViewModelMap
    {
        public static PortalProfileViewModel ToViewModel(PortalProfileModel model)
        {
            if(Equals(model,null))
            {
                return null;
            }
            return new PortalProfileViewModel()
            {
               PortalProfileID=model.PortalProfileID,
               PortalID=model.PortalID,
               ProfileID=model.ProfileID
            };

        }

        public static PortalProfileModel ToModel(PortalProfileViewModel model)
        {
            if (Equals(model, null))
            {
                return null;
            }
            return new PortalProfileModel()
            {
                PortalProfileID = model.PortalProfileID,
                PortalID = model.PortalID,
                ProfileID = model.ProfileID
            };
        }

        public static PortalProfileListViewModel ToListViewModel(PortalProfileListModel model, int? pageIndex, int? recordPerPage)
        {
            var viewModel = new PortalProfileListViewModel();

            if (!Equals(model.PortalProfiles, null))
            {
                viewModel.PortalProfiles = model.PortalProfiles.Select(
                x => new PortalProfileViewModel()
                {
                    PortalProfileID = x.PortalProfileID,
                    PortalID = x.PortalID,
                    ProfileID = x.ProfileID,
                    Name=x.Name
                }).ToList();
            }
            viewModel.TotalResults = Convert.ToInt32(model.TotalResults);
            viewModel.Page = Convert.ToInt32(model.PageIndex);
            viewModel.RecordPerPage = Convert.ToInt32(model.PageSize);
            viewModel.TotalPages = Convert.ToInt32(model.TotalPages);
            return viewModel;
        }
    }
}