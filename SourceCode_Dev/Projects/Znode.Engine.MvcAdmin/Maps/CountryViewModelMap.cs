﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    public class CountryViewModelMap
    {
        /// <summary>
        /// Converts Domain List View Model to view model.
        /// </summary>
        /// <param name="models">List of domain models</param>
        /// <returns>List of domain view Models</returns>
        public static CountryListViewModel ToListViewModel(CountryListModel models)
        {

            if (!Equals(models, null) && !Equals(models.Countries, null) && models.Countries.Count() > 0)
            {
                var viewModel = new CountryListViewModel()
                {
                    Countries = models.Countries.ToList().Select(
                    x => new CountryViewModel()
                    {
                        Code = x.Code,
                        Name = x.Name,
                        DisplayOrder = x.DisplayOrder,
                        IsActive = x.IsActive
                    }).ToList()
                };

                viewModel.TotalResults = Convert.ToInt32(models.TotalResults);
                viewModel.Page = Convert.ToInt32(models.PageIndex);
                viewModel.RecordPerPage = Convert.ToInt32(models.PageSize);
                viewModel.TotalPages = Convert.ToInt32(models.TotalPages);

                return viewModel;
            }
            return new CountryListViewModel();
        }
    }
}