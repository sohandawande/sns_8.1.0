﻿using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    /// <summary>
    /// Static Class for Payment Mapper
    /// </summary>
    public static class PaymentViewModelMap
    {
        /// <summary>
        /// Mapper to convert PaymentOptionModel to PaymentViewModel
        /// </summary>
        /// <param name="paymentOptionModel">Object of PaymentOptionModel</param>
        /// <returns>PaymentViewModel</returns>
        public static PaymentViewModel ToViewModel(PaymentOptionModel paymentOptionModel)
        {
            if (Equals(paymentOptionModel, null))
            {
                return null;
            }

            return new PaymentViewModel()
            {
                TransactionKey = paymentOptionModel.TransactionKey,
                DisplayOrder = paymentOptionModel.DisplayOrder,
                EnableRMA = paymentOptionModel.IsRmaCompatible,
                EnableTestModel = paymentOptionModel.TestMode,
                Visa = paymentOptionModel.EnableVisa,
                MasterCard = paymentOptionModel.EnableMasterCard,
                AmericanExpress = paymentOptionModel.EnableAmericanExpress,
                Discover = paymentOptionModel.EnableDiscover,
                PreAuthorizeTransactionsWithoutCapturing = paymentOptionModel.PreAuthorize,
                Partner = paymentOptionModel.Partner,
                Vendor = paymentOptionModel.Vendor,
                Password = paymentOptionModel.PaymentGatewayPassword,
                MerchantLogin = paymentOptionModel.PaymentGatewayUsername
            };
        }

        /// <summary>
        /// Mapper to convert PaymentViewModel to PaymentOptionModel  
        /// </summary>
        /// <param name="paymentViewModel">Object of PaymentViewModel</param>
        /// <returns>PaymentOptionModel</returns>
        public static PaymentOptionModel ToModel(PaymentViewModel paymentViewModel)
        {
            if (Equals(paymentViewModel, null))
            {
                return null;
            }

            return new PaymentOptionModel()
            {
                TransactionKey = paymentViewModel.TransactionKey,
                DisplayOrder = paymentViewModel.DisplayOrder,
                IsRmaCompatible = paymentViewModel.EnableRMA,
                TestMode = paymentViewModel.EnableTestModel,
                EnableVisa = paymentViewModel.Visa,
                EnableMasterCard = paymentViewModel.MasterCard,
                EnableAmericanExpress = paymentViewModel.AmericanExpress,
                EnableDiscover = paymentViewModel.Discover,
                PreAuthorize = paymentViewModel.PreAuthorizeTransactionsWithoutCapturing,
                Partner = paymentViewModel.Partner,
                Vendor = paymentViewModel.Vendor,
                PaymentGatewayPassword = paymentViewModel.Password,
                PaymentGatewayUsername = paymentViewModel.MerchantLogin
            };
        }

        /// <summary>
        /// Mapper to convert PaymentOptionViewModel to PaymentModel 
        /// </summary>
        /// <param name="paymentOptionViewModel">PaymentOptionViewModel paymentOptionViewModel</param>
        /// <returns>returns PaymentModel</returns>
        public static PaymentModel ToViewModel(PaymentOptionViewModel paymentOptionViewModel)
        {
            if (Equals(paymentOptionViewModel, null))
            {
                return new PaymentModel();
            }
            else
            {
                PaymentModel model = new PaymentModel()
                {
                    PaymentOption = ToPaymentOptionsModel(paymentOptionViewModel),
                    CreditCard = ToCreditCartModel(paymentOptionViewModel),
                    PaymentName = paymentOptionViewModel.PaymentName
                };

                return model;
            }
        }

        /// <summary>
        /// Mapper to convert PaymentOptionViewModel to PaymentOptionModel 
        /// </summary>
        /// <param name="paymentOptionModel">PaymentOptionViewModel paymentOptionModel</param>
        /// <returns>returns PaymentOptionModel</returns>
        public static PaymentOptionModel ToPaymentOptionsModel(PaymentOptionViewModel paymentOptionModel)
        {
            PaymentOptionModel model = new PaymentOptionModel()
            {
                PaymentType = new PaymentTypeModel() { PaymentTypeId = paymentOptionModel.PaymentTypeId },
                PaymentOptionId = paymentOptionModel.PaymentOptionId,
                PaymentTypeId = paymentOptionModel.PaymentTypeId
            };

            return model;
        }

        /// <summary>
        /// Mapper to convert PaymentOptionViewModel to CreditCardModel 
        /// </summary>
        /// <param name="paymentOptionViewModel">PaymentOptionViewModel paymentOptionViewModel</param>
        /// <returns>returns CreditCardModel</returns>
        public static CreditCardModel ToCreditCartModel(PaymentOptionViewModel paymentOptionViewModel)
        {
            CreditCardModel model = new CreditCardModel()
            {
            };

            return model;
        }

        public static PayPalPaymentModel ToModel(ShoppingCartModel shoppingCart, AddressViewModel addressViewModel)
        {
            PayPalPaymentModel model = new PayPalPaymentModel();
            string currencyCode = Znode.Engine.MvcAdmin.Agents.PortalAgent.CurrentPortal.CurrencySuffix;

            if (!Equals(shoppingCart, null) && !Equals(shoppingCart.BillingAddress, null))
            {
                model.BillingCity = shoppingCart.BillingAddress.City;
                model.BillingFirstName = shoppingCart.BillingAddress.FirstName;
                model.BillingLastName = shoppingCart.BillingAddress.LastName;
                model.BillingCountryCode = shoppingCart.BillingAddress.CountryCode;
                model.BillingEmailId = shoppingCart.BillingAddress.Email;
                model.BillingName = shoppingCart.BillingAddress.Name;
                model.BillingPhoneNumber = shoppingCart.BillingAddress.PhoneNumber;
                model.BillingPostalCode = shoppingCart.BillingAddress.PostalCode;
                model.BillingStateCode = shoppingCart.BillingAddress.StateCode;
                model.BillingStreetAddress1 = shoppingCart.BillingAddress.StreetAddress1;
                model.BillingStreetAddress2 = shoppingCart.BillingAddress.StreetAddress2;
            }
            else
            {
                model.BillingCity = addressViewModel.City;
                model.BillingFirstName = addressViewModel.FirstName;
                model.BillingLastName = addressViewModel.LastName;
                model.BillingCountryCode = addressViewModel.CountryCode;
                model.BillingEmailId = addressViewModel.Email;
                model.BillingName = addressViewModel.Name;
                model.BillingPhoneNumber = addressViewModel.PhoneNumber;
                model.BillingPostalCode = addressViewModel.PostalCode;
                model.BillingStateCode = addressViewModel.StateCode;
                model.BillingStreetAddress1 = addressViewModel.StreetAddress1;
                model.BillingStreetAddress2 = addressViewModel.StreetAddress2;
            }

            model.SubTotal = shoppingCart.SubTotal.ToString();
            model.Total = shoppingCart.Total.ToString();
            model.OrderId = "1";
            model.TaxCost = shoppingCart.TaxCost.ToString();
            model.GiftCardAmount = shoppingCart.GiftCardAmount.ToString();
            model.GatewayCurrencyCode = string.IsNullOrEmpty(currencyCode) ? "USD" : currencyCode;
            model.ShippingCity = shoppingCart.ShippingAddress.City;
            model.ShippingFirstName = shoppingCart.ShippingAddress.FirstName;
            model.ShippingLastName = shoppingCart.ShippingAddress.LastName;
            model.ShippingCountryCode = shoppingCart.ShippingAddress.CountryCode;
            model.ShippingPhoneNumber = shoppingCart.ShippingAddress.PhoneNumber;
            model.ShippingPostalCode = shoppingCart.ShippingAddress.PostalCode;
            model.ShippingStateCode = shoppingCart.ShippingAddress.StateCode;
            model.ShippingStreetAddress1 = shoppingCart.ShippingAddress.StreetAddress1;
            model.ShippingStreetAddress2 = shoppingCart.ShippingAddress.StreetAddress2;
            model.OrderId = "1";
            model.ShippingCost = shoppingCart.ShippingCost.ToString();
            model.Discount = shoppingCart.Discount.ToString();

            foreach (var item in shoppingCart.ShoppingCartItems)
            {
                var cartCount = shoppingCart.ShoppingCartItems.Count;
                CartItemModel objCartItem = new CartItemModel();
                objCartItem.ProductName = item.ProductName;
                objCartItem.ProductAmount = item.UnitPrice;
                objCartItem.ProductDescription = item.Description;
                objCartItem.ProductNumber = item.Sku;
                objCartItem.ProductQuantity = item.Quantity;
                model.CartItems.Add(objCartItem);
            }

            return model;

        }
    }
}