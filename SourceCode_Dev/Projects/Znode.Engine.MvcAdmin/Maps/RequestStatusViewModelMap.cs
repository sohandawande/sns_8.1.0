﻿using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    public class RequestStatusViewModelMap
    {
        /// <summary>
        /// Converts RequestStatusListModel to RequestStatusListViewModel
        /// </summary>
        /// <param name="model">RequestStatusListModel</param>
        /// <returns>RequestStatusListViewModel</returns>
        public static RequestStatusListViewModel ToListViewModel(RequestStatusListModel model)
        {
            RequestStatusListViewModel viewModel = new RequestStatusListViewModel();

            if (!Equals(model.RequestStatusList, null))
            {
                viewModel.RequestStatusList = model.RequestStatusList.Select(
                x => new RequestStatusViewModel()
                {
                    AdminNotification = x.AdminNotification,
                    RequestStatusID = x.RequestStatusID,
                    Name = x.Name,
                    CustomerNotification = x.CustomerNotification,
                    IsEnabled = x.IsEnabled
                }).ToList();
            }
            return viewModel;
        }

        /// <summary>
        /// Converting RequestStatus Model to RequestStatus View Model  
        /// </summary>
        /// <param name="model"></param>
        /// <returns>RMAConfigurationViewModel</returns>
        public static RequestStatusViewModel ToViewModel(RequestStatusModel model)
        {
            return new RequestStatusViewModel()
            {
                AdminNotification = model.AdminNotification,
                RequestStatusID = model.RequestStatusID,
                Name = model.Name,
                CustomerNotification = model.CustomerNotification,
                IsEnabled = model.IsEnabled
            };
        }

        /// <summary>
        /// Converting RequestStatus Model to RequestStatus View Model  
        /// </summary>
        /// <param name="model"></param>
        /// <returns>RMAConfigurationViewModel</returns>
        public static RequestStatusModel ToModel(RequestStatusViewModel viewModel)
        {
            return new RequestStatusModel()
            {
                AdminNotification = viewModel.AdminNotification,
                RequestStatusID = viewModel.RequestStatusID,
                Name = viewModel.Name,
                CustomerNotification = viewModel.CustomerNotification,
                IsEnabled = viewModel.IsEnabled
            };
        }
    }
}