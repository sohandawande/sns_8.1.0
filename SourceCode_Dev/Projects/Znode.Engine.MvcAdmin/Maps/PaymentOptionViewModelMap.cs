﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    public class PaymentOptionViewModelMap
    {
        public static PaymentOptionViewModel ToViewModel(PaymentOptionModel model)
        {
            if (!Equals(model, null))
            {
                return new PaymentOptionViewModel()
                 {
                     DisplayOrder = model.DisplayOrder,
                     //EnableAmericanExpress = model.EnableAmericanExpress,
                     //EnableDiscover = model.EnableDiscover,
                     //EnableMasterCard = model.EnableMasterCard,
                     //EnableRecurringPayments = model.EnableRecurringPayments,
                     //EnableVault = model.EnableVault,
                     //EnableVisa = model.EnableVisa,
                     IsActive = model.IsActive,
                     IsRmaCompatible = model.IsRmaCompatible,
                     //Partner = model.Partner,
                     PaymentGateway = !Equals(model.PaymentGateway, null) ? new PaymentGatewayViewModel { Name = model.PaymentGateway.Name, PaymentGatewayId = model.PaymentGateway.PaymentGatewayId, Url = model.PaymentGateway.Url } : null,
                     PaymentGatewayId = model.PaymentGatewayId,
                     //PaymentGatewayPassword = model.PaymentGatewayPassword,
                     //PaymentGatewayUsername = model.PaymentGatewayUsername,
                     PaymentOptionId = model.PaymentOptionId,
                     PaymentType = !Equals(model.PaymentType, null) ? new PaymentTypeViewModel { Name = model.PaymentType.Name, Description = model.PaymentType.Description, IsActive = model.PaymentType.IsActive, PaymentTypeId = model.PaymentType.PaymentTypeId } : null,
                     PaymentTypeId = model.PaymentTypeId,
                     //PreAuthorize = model.PreAuthorize,
                     ProfileId = model.ProfileId,
                     //TestMode = model.TestMode,
                     //TransactionKey = model.TransactionKey,
                     //Vendor = model.Vendor,
                     AdditionalFee = model.Vendor,
                     PaymentName = !Equals(model.PaymentType, null) ? model.PaymentType.Name : string.Empty,
                 };
            }
            return null;
        }

        public static PaymentOptionListViewModel ToListViewModel(PaymentOptionListModel model)
        {
            if (!Equals(model, null))
            {
                var paymetOptions = new PaymentOptionListViewModel()
                {
                    PaymentOptionList = model.PaymentOptions.ToList().Select(x =>
                        new PaymentOptionViewModel()
                        {
                            DisplayOrder = x.DisplayOrder,
                            //EnableAmericanExpress = x.EnableAmericanExpress,
                            //EnableDiscover = x.EnableDiscover,
                            //EnableMasterCard = x.EnableMasterCard,
                            //EnableRecurringPayments = x.EnableRecurringPayments,
                            //EnableVault = x.EnableVault,
                            //EnableVisa = x.EnableVisa,
                            IsActive = x.IsActive,
                            IsRmaCompatible = x.IsRmaCompatible,
                            Partner = x.Partner,
                            PaymentGateway = ToPaymentGatewayViewModel(x.PaymentGateway),
                            PaymentGatewayId = x.PaymentGatewayId,
                            //PaymentGatewayPassword = x.PaymentGatewayPassword,
                            //PaymentGatewayUsername = x.PaymentGatewayUsername,
                            PaymentOptionId = x.PaymentOptionId,
                            PaymentType = ToPaymentTypeViewModel(x.PaymentType),
                            PaymentTypeId = x.PaymentTypeId,
                            //PreAuthorize = x.PreAuthorize,
                            ProfileId = x.ProfileId,
                            ProfileName = x.ProfileName,
                            //TestMode = x.TestMode,
                            //TransactionKey = x.TransactionKey,
                            //Vendor = x.Vendor,
                            AdditionalFee = x.Vendor,
                            PaymentTypeName = !Equals(x.PaymentType, null) ? x.PaymentType.Name : string.Empty,

                        }).ToList(),
                    TotalPages = Convert.ToInt32(model.TotalPages),
                    TotalResults = Convert.ToInt32(model.TotalResults),
                    Page = Convert.ToInt32(model.PageIndex),
                    RecordPerPage = Convert.ToInt32(model.PageSize)
                };

                return paymetOptions;
            }
            return new PaymentOptionListViewModel();
        }


        public static PaymentGatewayViewModel ToPaymentGatewayViewModel(PaymentGatewayModel model)
        {
            if (Equals(model, null))
            {
                return new PaymentGatewayViewModel();
            }
            return new PaymentGatewayViewModel()
            {
                Name = model.Name,
                PaymentGatewayId = model.PaymentGatewayId,
                Url = model.Url
            };
        }

        public static PaymentTypeViewModel ToPaymentTypeViewModel(PaymentTypeModel model)
        {
            if (Equals(model, null))
            {
                return new PaymentTypeViewModel();
            }
            return new PaymentTypeViewModel()
            {
                Description = model.Description,
                IsActive = model.IsActive,
                Name = model.Name,
                PaymentTypeId = model.PaymentTypeId
            };
        }


        public static PaymentOptionModel ToModel(PaymentOptionViewModel model)
        {
            if (!Equals(model, null))
            {
                return new PaymentOptionModel()
                {
                    DisplayOrder = model.DisplayOrder,
                    IsActive = model.IsActive,
                    IsRmaCompatible = model.IsRmaCompatible,
                    PaymentGatewayId = model.PaymentGatewayId,
                    PaymentOptionId = model.PaymentOptionId,
                    PaymentTypeId = model.PaymentTypeId,
                    PreAuthorize = model.PreAuthorize,
                    ProfileId = model.ProfileId,
                    TestMode = model.TestMode,                   
                };
            }
            return new PaymentOptionModel();
        }

        /// <summary>
        /// Convert IEnumerable<ProfileModel> to List<SelectListItem>
        /// </summary>
        /// <param name="model">to convert to list item type</param>
        /// <returns>Returns List<SelectListItem></returns>
        public static List<SelectListItem> ToListItems(IEnumerable<ProfileModel> model)
        {
            List<SelectListItem> profileItems = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                profileItems = (from item in model
                                select new SelectListItem
                                {
                                    Text = item.Name,
                                    Value = item.ProfileId.ToString(),
                                }).ToList();
            }
            return profileItems;
        }
    }
}