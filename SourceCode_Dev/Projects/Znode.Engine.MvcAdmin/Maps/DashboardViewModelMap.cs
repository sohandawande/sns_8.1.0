﻿using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    /// <summary>
    /// Static Class for Dashboard View Model Mapper. 
    /// </summary>
    public static class DashboardViewModelMap
    {
        public static DashboardViewModel ToViewModel(DashboardModel model)
        {
            if (Equals(model, null))
            {
                return null;
            }

            return new DashboardViewModel()
            {
                PaymentGateway = model.PaymentGateway,
                StatusMessage = model.StatusMessage,
                TotalProducts = model.TotalProducts,
                TotalCategories = model.TotalCategories,
                TotalInventory = model.TotalInventory,
                TotalOutOfStock = model.TotalOutOfStock,
                ShippedToday = model.ShippedToday,
                ReturnedToday = model.ReturnedToday,
                TotalOrders = model.TotalOrders,
                TotalOrdersMTD = model.TotalOrdersMTD,
                TotalNewOrders = model.TotalNewOrders,
                TotalPaymentPendingOrders = model.TotalPaymentPendingOrders,
                TotalSubmittedOrders = model.TotalSubmittedOrders,
                TotalShippedOrders = model.TotalShippedOrders,
                TotalAccounts = model.TotalAccounts,
                TotalAccountsMTD = model.TotalAccountsMTD,
                TotalPages = model.TotalPages,
                TotalShippingOptions = model.TotalShippingOptions,
                TotalPendingServiceRequests = model.TotalPendingServiceRequests,
                TotalReviewsToApprove = model.TotalReviewsToApprove,
                TotalAffiliatesToApprove = model.TotalAffiliatesToApprove,
                TotalLowInventoryItems = model.TotalLowInventoryItems,
                TotalFailedLoginsToday = model.TotalLoginFailedToday,
                EmailOptInCustomers = model.EmailOptInCustomers,
                TotalDeclinedTransactions = model.TotalDeclinedTransactions,
                YTDRevenue = model.YTDRevenue,
                MTDRevenue = model.MTDRevenue,
                TodayRevenue = model.TodayRevenue,
                TotalLowInventoryThanReorder = model.TotalLowInventoryThanReorder
            };
        }
    }
}