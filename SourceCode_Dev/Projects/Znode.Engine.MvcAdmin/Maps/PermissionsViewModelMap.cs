﻿using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    /// <summary>
    /// This is the mapper class which will map the entries fromm the model to view model
    /// </summary>
    public class PermissionsViewModelMap
    {
        /// <summary>
        /// This method will map the model to view model
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public static PermissionViewModel ToViewModel(PermissionsModel entity)
        {
            PermissionViewModel model = new PermissionViewModel();
            model.Roles = new RolesViewModel();
            model.Stores = new ProfilePortalViewModel();

            model.Roles.RolesList = entity.roleModel.Permissions;
            model.Stores.PortalList = entity.storeModel.Stores;
            model.UserId = entity.UserId;
            model.AccountId = entity.AccountId;
            model.UserName = entity.UserName;
            model.IsRolesVisible = true;
            model.IsStoresVisible = true;

            return model;
        } 
        
        /// <summary>
        /// This method will convert the List of Store and Role Ids to Model
        /// </summary>
        /// <param name="AccountId">int Account Id</param>
        /// <param name="StoreIds">int array of Store ids</param>
        /// <param name="RoleIds">int array of Role Ids</param>
        /// <returns></returns>
        public static PermissionsModel ToModel(int AccountId, int[] StoreIds, int[] RoleIds)
        {
            PermissionsModel model = new PermissionsModel();
            model.AccountId = AccountId;
            model.SelectedRoleIds = !Equals(RoleIds, null) ? string.Join(",", RoleIds) : string.Empty;
            model.SelectedStoreIds = !Equals(StoreIds, null) ? string.Join(",", StoreIds) : string.Empty;
            return model;
        }
    }
}