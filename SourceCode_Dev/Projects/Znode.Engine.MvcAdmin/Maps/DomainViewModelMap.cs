﻿using System;
using System.Collections.Generic;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    /// <summary>
    /// Converts domain model to domain view model.
    /// </summary>
    public class DomainViewModelMap
    {
        private const string httpName = "http://";

        /// <summary>
        /// Returns view model for domain model.
        /// </summary>
        /// <param name="model">Domain Model</param>
        /// <returns>Domain View Model</returns>
        public static DomainViewModel ToViewModel(DomainModel model)
        {
            if (Equals(model, null))
            {
                return null;
            }
            return new DomainViewModel()
            {
                ApiKey = model.ApiKey,
                DomainId = model.DomainId,
                DomainName = model.DomainName.ToLower().Contains(httpName) ? model.DomainName : httpName + model.DomainName,
                IsActive = model.IsActive,
                PortalId = model.PortalId
            };
        }

        /// <summary>
        /// Converts Domain List View Model to view model.
        /// </summary>
        /// <param name="models">List of domain models</param>
        /// <returns>List of domain view Models</returns>
        public static DomainListViewModel ToListViewModel(IEnumerable<DomainModel> models, int? totalResults = 0, int? recordPerPage = 0, int? pageIndex = 0, int? totalPages = 0)
        {
            if (models != null && models.Count() > 0)
            {
                var viewModel = new DomainListViewModel()
                {
                    Domains = models.ToList().Select(
                    x => new DomainViewModel()
                    {
                        ApiKey = x.ApiKey,
                        DomainId = x.DomainId,
                        DomainName = x.DomainName.ToLower().Contains(httpName) ? x.DomainName : httpName + x.DomainName,
                        IsActive = x.IsActive,
                        PortalId = x.PortalId
                    }).ToList()
                };
                viewModel.RecordPerPage = recordPerPage ?? 10;
                viewModel.Page = pageIndex ?? 1;
                viewModel.TotalPages = Convert.ToInt32(totalPages);
                viewModel.TotalResults = (Equals(totalResults, 0)) ? Convert.ToInt32(viewModel.TotalResults) : Convert.ToInt32(totalResults);
                return viewModel;
            }
            return new DomainListViewModel();
        }

        /// <summary>
        /// Convert DomainViewModel To DomainModel.
        /// </summary>
        /// <param name="viewModel">DomainViewModel</param>
        /// <returns>Returns DomainModel</returns>
        public static DomainModel ToModel(DomainViewModel viewModel)
        {
            if (Equals(viewModel, null))
            {
                return null;
            }
            return new DomainModel()
            {
                DomainId = viewModel.DomainId,
                DomainName = viewModel.DomainName,
                ApiKey = Guid.NewGuid().ToString(),
                IsActive = viewModel.IsActive,
                PortalId = viewModel.PortalId
            };
        }
    }
}