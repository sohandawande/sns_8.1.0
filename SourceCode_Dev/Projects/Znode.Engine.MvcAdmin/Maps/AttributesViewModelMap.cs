﻿using System;
using System.Collections.Generic;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    /// <summary>
    /// View Model Mapper for Attributes
    /// </summary>
    public class AttributesViewModelMap
    {
        /// <summary>
        /// View Model Mapper for Attribute 
        /// </summary>
        /// <param name="model"> AttributeModel </param>
        /// <returns>returns AttributesViewModel</returns>
        public static AttributesViewModel ToViewModel(AttributeModel model)
        {
            return new AttributesViewModel()
            {
                AttributeId = model.AttributeId,
                AttributeTypeId = model.AttributeTypeId,
                Name = model.Name,
                DisplayOrder = model.DisplayOrder,
                IsActive = model.IsActive
            };
        }
       
        /// <summary>
        /// Converts Attributes model to Attributes list view model
        /// </summary>
        /// <param name="models">ProductAttributeListModel models</param>
        /// <returns>returns AttributesListViewModel</returns>
        public static AttributesListViewModel ToListViewModel(ProductAttributeListModel models)
        {
            if (!Equals(models, null) && !Equals(models.Attributes, null))
            {
                var viewModel = new AttributesListViewModel()
                {
                    Attributes = models.Attributes.ToList().Select(
                    x => new AttributesViewModel()
                    {
                        AttributeId = x.AttributeId,
                        AttributeTypeId = x.AttributeTypeId,
                        Name = x.Name,
                        DisplayOrder = x.DisplayOrder,
                        IsActive = x.IsActive,
                        AttributeTypeName = x.AttributeTypeName
                    }).ToList()
                };
                viewModel.TotalResults = Convert.ToInt32(models.TotalResults);
                viewModel.Page = Convert.ToInt32(models.PageIndex);
                viewModel.RecordPerPage = Convert.ToInt32(models.PageSize);
                viewModel.TotalPages = Convert.ToInt32(models.TotalPages);
                return viewModel;
            }
            else
            {
                return new AttributesListViewModel();
            }
        }


        /// <summary>
        ///  Model Mapper for Attributes
        /// </summary>
        /// <param name="viewModel">AttributesViewModel</param>
        /// <returns>returns AttributeModel</returns>
        public static AttributeModel ToModel(AttributesViewModel viewModel)
        {
            return new AttributeModel()
            {
                AttributeId = viewModel.AttributeId,
                AttributeTypeId = viewModel.AttributeTypeId,
                Name = viewModel.Name,
                DisplayOrder = viewModel.DisplayOrder.Value,
                IsActive = viewModel.IsActive
            };
        }

        public static AttributesViewModel ToEntity(AttributeModel model)
        {
            if (model != null)
            {

                return new AttributesViewModel()
                {
                    AttributeId = model.AttributeId,
                    DisplayOrder = model.DisplayOrder,
                    AttributeTypeId = model.AttributeTypeId,
                    Name = model.Name

                };

            }

            return null;
        }
    }
}