﻿using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    /// <summary>
    /// Mapper for State View Model
    /// </summary>
    public class StateViewModelMap
    {
        /// <summary>
        /// Converts StateListModel to StateListViewModel
        /// </summary>
        /// <param name="models">StateListModel</param>
        /// <returns>Returns StateListModel</returns>
        public static StateListViewModel ToListViewModel(StateListModel models)
        {
            if (models != null && models.States.Count() > 0)
            {
                var viewModel = new StateListViewModel()
                {
                    State = models.States.ToList().Select(
                    x => new StateViewModel()
                    {
                        Code = x.Code,
                        CountryCode = x.CountryCode,
                        Name = x.Name,                        
                    }).ToList()
                };

                return viewModel;
            }
            return new StateListViewModel();
        }
    }
}