﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    public class RMARequestViewModelMap
    {
        #region Private Variables

        private const string AmountColumn = "Amount";
        private const string ExpirationDateColumn = "ExpirationDate";
        private const string CardNumberColumn = "CardNumber"; 

        #endregion

        #region Public Static Methods

        /// <summary>
        /// Maps RMA request list view model to RMA request model list.
        /// </summary>
        /// <param name="models">RMARequest Model</param>
        /// <returns>RMA request List ViewModel</returns>
        public static RMARequestListViewModel ToListViewModel(RMARequestListModel models)
        {
            if (!Equals(models, null) && !Equals(models.RMARequests, null))
            {
                var viewModel = new RMARequestListViewModel()
                {
                    RMARequestList = models.RMARequests.ToList().Select(
                    x => new RMARequestViewModel()
                    {
                        RMARequestID = x.RMARequestID,
                        StoreName = x.StoreName,
                        PortalId = x.PortalId,
                        OrderId = x.OrderId,
                        FirstName = x.BillingFirstName,
                        LastName = x.BillingLastName,
                        RequestDate = HelperMethods.ViewDateFormat(x.RequestDate),
                        RequestStatus = x.RequestStatus,
                        RequestStatusId = x.RequestStatusId,
                        CustomerName = x.BillingFirstName + " " + x.BillingLastName,
                        RequestNumber = Equals(x.RequestNumber, null) ? string.Empty : x.RequestNumber
                    }).ToList()
                };
                viewModel.Page = 0;
                viewModel.RecordPerPage = 10;
                viewModel.TotalPages = Convert.ToInt32(models.TotalPages);
                viewModel.TotalResults = Convert.ToInt32(models.TotalResults);
                return viewModel;
            }
            else
            {
                return new RMARequestListViewModel();
            }
        }

        /// <summary>
        /// Converts RMARequestModel to RMARequestViewModel.
        /// </summary>
        /// <param name="model">RMARequestModel</param>
        /// <returns>RMARequestViewModel</returns>
        public static RMARequestViewModel ToViewModel(RMARequestModel model)
        {
            if (Equals(model, null))
            {
                return null;
            }

            return new RMARequestViewModel()
            {
                RMARequestID = model.RMARequestID,
                StoreName = model.StoreName,
                PortalId = model.PortalId,
                OrderId = model.OrderId,
                FirstName = model.BillingFirstName,
                LastName = model.BillingLastName,
                Comments = model.Comments,
                RequestDate = HelperMethods.ViewDateFormat(model.RequestDate),
                RequestStatus = model.RequestStatus,
                RequestStatusId = model.RequestStatusId,
                CustomerName = model.CustomerName,
                RequestNumber = model.RequestNumber,
                TaxCost = Equals(model.TaxCost, null) ? null : MvcAdminUnits.GetCurrencyValue(model.TaxCost.Value),
                SubTotal = Equals(model.SubTotal, null) ? null : MvcAdminUnits.GetCurrencyValue(model.SubTotal.Value),
                Total = Equals(model.Total, null) ? null : MvcAdminUnits.GetCurrencyValue(model.Total.Value),
                Discount = Equals(model.Discount, null) ? null : MvcAdminUnits.GetCurrencyValue(model.Discount.Value),
                TaxCostAmount = model.TaxCost,
                DiscountAmount=model.Discount,
                SubTotalAmount=model.SubTotal,
                TotalAmount=model.Total
            };
        }

        /// <summary>
        /// Converts RMARequestViewModel to RMARequestModel.
        /// </summary>
        /// <param name="viewModel">RMARequestViewModel</param>
        /// <returns>RMARequestModel</returns>
        public static RMARequestModel ToModel(RMARequestViewModel viewModel)
        {
            if (Equals(viewModel, null))
            {
                return null;
            }

            return new RMARequestModel()
            {
                RMARequestID = viewModel.RMARequestID,
                StoreName = viewModel.StoreName,
                PortalId = viewModel.PortalId,
                OrderId = viewModel.OrderId,
                BillingFirstName = viewModel.FirstName,
                BillingLastName = viewModel.LastName,
                RequestDate = Convert.ToDateTime(viewModel.RequestDate),
                RequestStatus = viewModel.RequestStatus,
                RequestStatusId = viewModel.RequestStatusId,
                RequestNumber = viewModel.RequestNumber,
                Comments = viewModel.Comments,
                TaxCost=viewModel.TaxCostAmount,
                Discount=viewModel.DiscountAmount,
                Total=viewModel.TotalAmount,
                SubTotal=viewModel.SubTotalAmount
            };
        }

        public static RMARequestModel ToModel(CreateRequestViewModel viewModel)
        {
            if (Equals(viewModel, null))
            {
                return null;
            }

            RMARequestModel model = new RMARequestModel()
            {
                RMARequestID = viewModel.RMARequestId,
                RequestDate = DateTime.Now,
                Comments = Equals(viewModel.Comments, null) ? string.Empty : HttpUtility.HtmlEncode(viewModel.Comments),
                RequestStatusId = Convert.ToInt32(ZNodeRequestState.Authorized),
                UpdatedDate = DateTime.Now,
                RequestNumber = viewModel.RequestNumber,
                TaxCost = Equals(viewModel.TaxCost, null) ? 0 : Convert.ToDecimal(viewModel.TaxCost),
                SubTotal = Equals(viewModel.SubTotal, null) ? 0 : Convert.ToDecimal(viewModel.SubTotal),
                Total = Equals(viewModel.Total, null) ? 0 : Convert.ToDecimal(viewModel.Total),
            };

            if (!Equals(viewModel.RMARequestItems, null))
            {
                foreach (CreateRequestItemsViewModel items in viewModel.RMARequestItems)
                {
                    RMARequestItemModel item = new RMARequestItemModel();
                    item.RMARequestId = items.OrderId;
                    item.OrderLineItemID = items.OrderLineItemId;
                    item.Quantity = Equals(items.Quantity, null) ? 0 : items.Quantity;
                    item.Price = string.IsNullOrEmpty(items.Price) ? 0 : Convert.ToDecimal(items.Price);
                    item.ReasonForReturnId = Equals(items.ReasonForReturnId, null) ? 0 : items.ReasonForReturnId;
                    model.RMARequestItems.Add(item);
                }
            }

            return model;
        }

        /// <summary>
        /// Creates a SelectListItem List for ReasonForReturn
        /// </summary>
        /// <param name="model">IEnumerable<ReasonForReturnModel> model</param>
        /// <returns>returns List<SelectListItem></returns>
        public static List<SelectListItem> ToSelectListItems(IEnumerable<ReasonForReturnModel> model)
        {
            List<SelectListItem> catalogItems = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                catalogItems = (from item in model
                                select new SelectListItem
                                {
                                    Text = item.Name,
                                    Value = item.ReasonForReturnId.ToString(),
                                }).ToList();
            }
            return catalogItems;
        }
        #endregion
    }
}