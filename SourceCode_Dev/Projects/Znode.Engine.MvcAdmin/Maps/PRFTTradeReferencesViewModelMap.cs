﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    public class PRFTTradeReferencesViewModelMap
    {
        public static PRFTTradeReferencesModel ToModel(PRFTTradeReferencesViewModel viewmodel)
        {
            if (viewmodel == null)
            {
                return new PRFTTradeReferencesModel();
            }

            var model = new PRFTTradeReferencesModel
            {
                TradeRefID = viewmodel.TradeRefID,
                CreditApplicationID = viewmodel.CreditApplicationID,
                ReferenceName = viewmodel.ReferenceName,
                BusinessName = viewmodel.BusinessName,
                Street = viewmodel.Street,
                Street1 = viewmodel.Street1,
                City = viewmodel.City,
                StateCode = viewmodel.StateCode,
                CountryCode = viewmodel.CountryCode,
                PostalCode = viewmodel.PostalCode,
                PhoneNumber = viewmodel.PhoneNumber,
                FaxNumber = viewmodel.FaxNumber,
                Custom1 = viewmodel.Custom1,
                Custom2 = viewmodel.Custom2,
                Custom3 = viewmodel.Custom3,
                Custom4 = viewmodel.Custom4,
                Custom5 = viewmodel.Custom5
            };
            return model;
        }

        public static PRFTTradeReferencesViewModel ToViewModel(PRFTTradeReferencesModel model)
        {
            if (model == null)
            {
                return new PRFTTradeReferencesViewModel();
            }

            var viewmodel = new PRFTTradeReferencesViewModel
            {
                TradeRefID = model.TradeRefID,
                CreditApplicationID = model.CreditApplicationID,
                ReferenceName = model.ReferenceName,
                BusinessName = model.BusinessName,
                Street = model.Street,
                Street1 = model.Street1,
                City = model.City,
                StateCode = model.StateCode,
                CountryCode = model.CountryCode,
                PostalCode = model.PostalCode,
                PhoneNumber = model.PhoneNumber,
                FaxNumber = model.FaxNumber,
                Custom1 = model.Custom1,
                Custom2 = model.Custom2,
                Custom3 = model.Custom3,
                Custom4 = model.Custom4,
                Custom5 = model.Custom5
            };
            return viewmodel;
        }
    }
}