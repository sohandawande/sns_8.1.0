﻿using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    public class IssuedGiftCardViewModelMap
    {
        /// <summary>
        /// Convert issued gift card model to view model.
        /// </summary>
        /// <param name="models">Converts to list view model</param>
        /// <returns>HighlightsListViewModel</returns>
        public static IssuedGiftCardListViewModel ToListViewModel(IssuedGiftCardListModel models)
        {
            IssuedGiftCardListViewModel viewModel = new IssuedGiftCardListViewModel();
            if (!Equals(models.IssuedGiftCardModels, null))
            {
                viewModel.IssuedGiftCards = models.IssuedGiftCardModels.Select(
                x => new IssuedGiftCardViewModel()
                {
                    CardNumber = x.CardNumber,
                    Amount = x.Amount,
                    ExpirationDate = x.ExpirationDate.ToString(MvcAdmin.Helpers.HelperMethods.GetStringDateFormat())
                }).ToList();
            }
            return viewModel;
        }
    }
}