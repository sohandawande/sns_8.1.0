﻿using Resources;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    /// <summary>
    /// Mapper for Product
    /// </summary>
    public static class ProductViewModelMap
    {

        #region Private Variables
        private const string originalPriceTextinLowercase = "originalprice";
        private const string allProfilesText = "All Profiles";
        private const string price = "price";
        #endregion

        /// <summary>
        /// Maps the list of CategoryAssociatedProducts model to CategoryAssociatedProducts view model.
        /// </summary>
        /// <param name="models">The list of type CategoryAssociatedProductsModel.</param>
        /// <returns>Returns the mapped list of CategoryAssociatedProductsModel model to CategoryAssociatedProductsListModel.</returns>
        public static CategoryAssociatedProductsListViewModel ToListViewModel(Collection<CategoryAssociatedProductsModel> models, int? totalResults = 0)
        {
            CategoryAssociatedProductsListViewModel list = new CategoryAssociatedProductsListViewModel();
            if (!Equals(models, null))
            {
                foreach (CategoryAssociatedProductsModel model in models)
                {
                    list.AssociatedProducts.Add(ToCategoryAssociatedProductViewModelList(model));
                }
            }

            list.TotalResults = (Equals(totalResults, 0)) ? Convert.ToInt32(list.TotalResults) : Convert.ToInt32(totalResults);
            return list;
        }

        /// <summary>
        /// To map model to ViewModel list items
        /// </summary>
        /// <param name="model">CategoryAssociatedProductsModel model</param>
        /// <returns> returns CategoryAssociatedProductsViewModel</returns>
        public static CategoryAssociatedProductsViewModel ToCategoryAssociatedProductViewModelList(CategoryAssociatedProductsModel model)
        {
            if (!Equals(model, null))
            {
                CategoryAssociatedProductsViewModel viewModel = new CategoryAssociatedProductsViewModel()
                {
                    ProductId = !Equals(model.ProductId, null) ? model.ProductId : 0,
                    CategoryId = !Equals(model.CategoryId, null) ? model.CategoryId : 0,
                    ManufacturerId = !Equals(model.ManufacturerId, null) ? model.ManufacturerId : 0,
                    ProductTypeId = !Equals(model.ProductTypeId, null) ? model.ProductTypeId : 0,
                    SKUId = !Equals(model.SKUId, null) ? model.SKUId : 0,
                    SKU = !Equals(model.SKU, null) ? model.SKU : string.Empty,
                    Name = !Equals(model.Name, null) ? model.Name : string.Empty,
                    IsActive = !Equals(model.IsActive, null) ? model.IsActive : model.IsActive,
                    ImageFile = !Equals(model.ImageFile, null) ? model.ImageFile : string.Empty,
                    ProductNumber = !Equals(model.ProductNumber, null) ? model.ProductNumber : string.Empty,
                    ManufacturerName = !Equals(model.ManufacturerName, null) ? model.ManufacturerName : string.Empty,
                    CategoryName = !Equals(model.CategoryName, null) ? model.CategoryName : string.Empty,
                    ProductType = !Equals(model.ProductType, null) ? model.ProductType : string.Empty
                };
                ImageHelper imageHelper = new ImageHelper();
                viewModel.ImageSmallThumbnailPath = HelperMethods.GetImageRelativePath(imageHelper.GetImageHttpPathThumbnail(model.ImageFile));

                return viewModel;
            }
            else
            {
                return new CategoryAssociatedProductsViewModel();
            }
        }

        /// <summary>
        /// To map model to ViewModel
        /// </summary>
        /// <param name="model">ProductModel</param>
        /// <returns>returns  ProductViewModel</returns>
        public static ProductViewModel ToViewModel(ProductModel model)
        {
            if (!Equals(model, null))
            {
                ProductViewModel viewModel = new ProductViewModel()
                {
                    ProductId = model.ProductId,
                    BundleItemsIds = model.BundledProductIds,
                    ImageAltTag = model.ImageAltTag,
                    ImageFile = model.ImageFile,
                    IsActive = model.IsActive,
                    IsCallForPricing = model.CallForPricing,
                    MaxQuantity = Equals(null, model.MaxSelectableQuantity) ? 10 : model.MaxSelectableQuantity.Equals(0) ? 10 : model.MaxSelectableQuantity.GetValueOrDefault(10),
                    MinQuantity = Equals(null, model.MinSelectableQuantity) ? 1 : model.MinSelectableQuantity.Equals(0) ? 1 : model.MinSelectableQuantity.GetValueOrDefault(1),
                    Name = model.Name,
                    ProductCode = model.ProductNumber,
                    RetailPrice = Equals(model.RetailPrice, null) ? model.RetailPrice : HelperMethods.FormatPrice(model.RetailPrice),
                    SalePrice = Equals(model.SalePrice, null) ? model.SalePrice : HelperMethods.FormatPrice(model.SalePrice),
                    WholeSalePrice = Equals(model.WholesalePrice, null) ? null : HelperMethods.FormatNullablePrice(model.WholesalePrice),
                    ProductSpecifications = Equals(model.Specifications, null) ? String.Empty : HttpUtility.HtmlDecode(model.Specifications),
                    LongDescription = Equals(model.Description, null) ? String.Empty : HttpUtility.HtmlDecode(model.Description),
                    FeaturesDescription = Equals(model.FeaturesDescription, null) ? String.Empty : HttpUtility.HtmlDecode(model.FeaturesDescription),
                    ShippingInformation = Equals(model.AdditionalInfo, null) ? String.Empty : HttpUtility.HtmlDecode(model.AdditionalInfo),
                    ShortDescription = Equals(model.ShortDescription, null) ? String.Empty : HttpUtility.HtmlDecode(model.ShortDescription),
                    Weight = model.Weight,
                    Height = model.Height,
                    Width = model.Width,
                    Length = model.Length,
                    ProductTypeId = model.ProductTypeId,
                    TaxClassId = model.TaxClassId,
                    ReviewStateId = model.ReviewStateId,
                    DisplayOrder = model.DisplayOrder,
                    ShippingRuleTypeId = model.ShippingRuleTypeId,
                    DownloadLink = model.DownloadLink,
                    FreeShipping = Equals(model.AllowFreeShipping, null) ? false : model.AllowFreeShipping,
                    ExpirationPeriod = model.ExpirationPeriod,
                    ExpirationFrequency = model.ExpirationFrequency,
                    ShippingRate = Equals(model.ShippingRate, null) ? null : HelperMethods.FormatNullablePrice(model.ShippingRate),
                    BrandName = model.ManufacturerName,
                    SupplierName = Equals(model.SupplierName, null) ? ZnodeResources.LabelNotApplicable : string.IsNullOrEmpty(model.SupplierName) ? ZnodeResources.LabelNotApplicable : model.SupplierName,
                    TaxClassName = Equals(model.TaxClassName, null) ? string.Empty : model.TaxClassName,
                    ShippingRule = Equals(model.ShippingRule, null) ? string.Empty : model.ShippingRule,
                    ProductTypeName = Equals(model.ProductTypeName, null) ? string.Empty : model.ProductTypeName,
                    Sku = Equals(model.SkuName, null) ? string.Empty : model.SkuName,
                    SkuId = model.SkuId,
                    ProductAttributes = Equals(model.AttributeIds, null) ? string.Empty : model.AttributeIds,
                    QuantityOnHand = model.QuantityOnHand,
                    ReorderLevel = Equals(model.ReorderLevel, null) ? 0 : model.ReorderLevel,
                    NewProductInd = model.IsNewProduct,
                    FeaturedInd = model.IsFeatured,
                    Franchisable = model.IsFranchisable,
                    RecurringBillingInd = model.AllowRecurringBilling,
                    RecurringBillingInitialAmount = HelperMethods.FormatPrice(model.RecurringBillingInitialAmount),
                    RecurringBillingFrequency = model.RecurringBillingFrequency,
                    InStockMsg = Equals(model.InStockMessage, null) ? ZnodeResources.MessageInStock : HttpUtility.HtmlDecode(model.InStockMessage),
                    OutOfStockMsg = Equals(model.OutOfStockMessage, null) ? ZnodeResources.MessageOutOfStock : HttpUtility.HtmlDecode(model.OutOfStockMessage),
                    BackOrderMsg = Equals(model.BackOrderMessage, null) ? string.Empty : model.BackOrderMessage,
                    SeoDescription = Equals(model.SeoDescription, null) ? string.Empty : HttpUtility.HtmlDecode(model.SeoDescription),
                    SeoKeywords = Equals(model.SeoKeywords, null) ? string.Empty : HttpUtility.HtmlDecode(model.SeoKeywords),
                    SeoPageUrl = Equals(model.SeoUrl, null) ? string.Empty : model.SeoUrl,
                    SeoTitle = Equals(model.SeoTitle, null) ? string.Empty : HttpUtility.HtmlDecode(model.SeoTitle),
                    HomepageSpecial = model.IsHomepageSpecial,
                    RedirectUrlInd = model.RedirectUrlInd,
                    InventoryDisplay = model.InventoryDisplay,
                    AffiliateUrl = model.AffiliateUrl,
                    AccountID = model.AccountId,
                    SupplierId = model.SupplierId,
                    BrandId = model.ManufacturerId,
                    BundleProductDict = Equals(model.BundleProductDict, null) ? null : model.BundleProductDict,
                    ProductAddOns = Equals(model.ProductAddOns, null) ? null : model.ProductAddOns,
                    ProductAttributesCollection = Equals(model.ProductAttributes, null) ? null : model.ProductAttributes,
                    IsGiftCard = Equals(model.IsGiftCard, null) ? false : model.IsGiftCard,
                    ParentProduct = Equals(model.IsChildProduct, null) ? false : model.IsChildProduct,
                    AttributeCount = Equals(model.AttributeCount, null) ? 0 : model.AttributeCount,
                    TrackInventory = model.TrackInventory,
                    AllowBackOrder = model.AllowBackOrder,
                    PortalID = model.PortalId,
                    IsParentProduct = model.IsParentProductId,
                    Custom3 = model.Custom3 //PRFT Custom  Code
                };

                ImageHelper imageHelper = new ImageHelper();
                viewModel.ImageSmallThumbnailPath = imageHelper.GetImageHttpPathSmallThumbnail(model.ImageFile);
                viewModel.ImageMediumPath = imageHelper.GetImageHttpPathMedium(model.ImageFile);
                viewModel.ImageLargePath = imageHelper.GetImageHttpPathLarge(model.ImageFile);

                if (!Equals(viewModel.ExpirationFrequency, null))
                {
                    DurationType expPeriodFrequency = (DurationType)viewModel.ExpirationFrequency;
                    string statusString = expPeriodFrequency.ToString();
                    viewModel.ExpirationPeriodFrequency = string.Concat(viewModel.ExpirationPeriod, " ", statusString);
                }

                return viewModel;
            }
            else
            {
                return new ProductViewModel();
            }
        }

        /// <summary>
        /// To map model to ViewModel list items
        /// </summary>
        /// <param name="model">ProductModel model</param>
        /// <returns> returns ProductViewModel</returns>
        public static ProductViewModel ToViewModelList(ProductModel model)
        {
            if (!Equals(model, null))
            {
                ProductViewModel viewModel = new ProductViewModel()
                {
                    ProductId = model.ProductId,
                    ImageAltTag = model.ImageAltTag,
                    ImageFile = model.ImageFile,
                    InStockMsg = model.InStockMessage,
                    IsActive = model.IsActive,
                    Name = model.Name,
                    ProductCode = model.ProductNum,
                    DisplayRetailPrice = Equals(model.RetailPrice, null) ? string.Empty : MvcAdminUnits.GetCurrencyValue(model.RetailPrice.Value),
                    DisplaySalePrice = Equals(model.SalePrice, null) ? string.Empty : MvcAdminUnits.GetCurrencyValue(model.SalePrice.Value),
                    DisplayWholeSalePrice = Equals(model.WholesalePrice, null) ? string.Empty : MvcAdminUnits.GetCurrencyValue(model.WholesalePrice.Value),
                    QuantityOnHand = model.QuantityOnHand,
                    ReorderLevel = model.ReorderLevel,
                    DisplayOrder = model.DisplayOrder,
                    ParentProduct = model.IsChildProduct,
                    PortalID = model.PortalId,
                    VendorName = model.Vendor,
                    ReviewStateId = model.ReviewStateId,
                    ReviewStateString = HttpUtility.HtmlDecode(model.ReviewStateString),
                    ProductTypeId = model.ProductTypeId,
                    ReviewHistoryDescription = Equals(model.ReviewHistoryDescription, null) ? string.Empty : model.ReviewHistoryDescription

                };
                ImageHelper imageHelper = new ImageHelper();
                viewModel.ImageSmallThumbnailPath = HelperMethods.GetImageRelativePath(imageHelper.GetImageHttpPathSmallThumbnail(model.ImageFile));
                return viewModel;
            }
            else
            {
                return new ProductViewModel();
            }
        }

        /// <summary>
        /// To map ListViewModel
        /// </summary>
        /// <param name="models">Collection<ProductModel></param>
        /// <returns>returns ProductListViewModel</returns>
        public static ProductListViewModel ToListViewModel(Collection<ProductModel> models, int? totalResults = 0)
        {
            ProductListViewModel list = new ProductListViewModel();
            if (!Equals(models, null))
            {
                foreach (ProductModel model in models)
                {
                    list.Products.Add(ToViewModelList(model));
                }
            }

            list.TotalResults = (Equals(totalResults, 0)) ? Convert.ToInt32(list.TotalResults) : Convert.ToInt32(totalResults);
            return list;
        }

        /// <summary>
        /// Model Mapper for product
        /// </summary>
        /// <param name="viewModel">ProductViewModel</param>
        /// <returns>returns ProductModel</returns>
        public static ProductModel ToModel(ProductViewModel viewModel)
        {
            if (!Equals(viewModel, null))
            {
                ProductModel model = new ProductModel()
                {
                    ProductId = Equals(viewModel.ProductId, null) ? 0 : viewModel.ProductId,
                    AllowBackOrder = Equals(viewModel.AllowBackOrder, null) ? null : viewModel.AllowBackOrder,
                    BundledProductIds = Equals(viewModel.BundleItemsIds, null) ? string.Empty : viewModel.BundleItemsIds,
                    CallForPricing = Equals(viewModel.IsCallForPricing, null) ? false : viewModel.IsCallForPricing,
                    ImageAltTag = Equals(viewModel.ImageAltTag, null) ? string.Empty : viewModel.ImageAltTag,
                    ImageFile = Equals(viewModel.ImageFile, null) ? string.Empty : viewModel.ImageFile,
                    ImageLargePath = Equals(viewModel.ImageLargePath, null) ? string.Empty : viewModel.ImageLargePath,
                    ImageMediumPath = Equals(viewModel.ImageMediumPath, null) ? string.Empty : viewModel.ImageMediumPath,
                    ImageSmallThumbnailPath = Equals(viewModel.ImageSmallThumbnailPath, null) ? string.Empty : viewModel.ImageSmallThumbnailPath,
                    IsActive = Equals(viewModel.IsActive, null) ? false : viewModel.IsActive,
                    MaxSelectableQuantity = viewModel.MaxQuantity,
                    MinSelectableQuantity = viewModel.MinQuantity,
                    Name = Equals(viewModel.Name, null) ? String.Empty : viewModel.Name,
                    ProductNumber = Equals(viewModel.ProductCode, null) ? String.Empty : viewModel.ProductCode,
                    RetailPrice = Equals(viewModel.RetailPrice, null) ? 0 : viewModel.RetailPrice,
                    SalePrice = Equals(viewModel.SalePrice, null) ? null : viewModel.SalePrice,
                    WholesalePrice = Equals(viewModel.WholeSalePrice, null) ? null : viewModel.WholeSalePrice,
                    TrackInventory = Equals(viewModel.TrackInventory, null) ? null : viewModel.TrackInventory,
                    ProductTypeId = Equals(viewModel.ProductTypeId, null) ? 0 : viewModel.ProductTypeId,
                    ShippingRate = Equals(viewModel.ShippingRate, null) ? null : viewModel.ShippingRate,
                    TaxClassId = Equals(viewModel.TaxClassId, null) ? null : viewModel.TaxClassId,
                    ReviewStateId = Equals(viewModel.ReviewStateId, null) ? null : viewModel.ReviewStateId,
                    DisplayOrder = Equals(viewModel.DisplayOrder, null) ? null : viewModel.DisplayOrder,
                    IsFeatured = Equals(viewModel.FeaturedInd, null) ? false : viewModel.FeaturedInd,
                    IsNewProduct = Equals(viewModel.NewProductInd, null) ? false : viewModel.NewProductInd,
                    ShippingRuleTypeId = Equals(viewModel.ShippingRuleTypeId, null) ? null : viewModel.ShippingRuleTypeId,
                    DownloadLink = Equals(viewModel.DownloadLink, null) ? string.Empty : viewModel.DownloadLink,
                    AllowFreeShipping = Equals(viewModel.FreeShipping, null) ? null : viewModel.FreeShipping,
                    ExpirationPeriod = Equals(viewModel.ExpirationPeriod, null) ? null : viewModel.ExpirationPeriod,
                    ExpirationFrequency = Equals(viewModel.ExpirationFrequency, null) ? null : viewModel.ExpirationFrequency,
                    Weight = Equals(viewModel.Weight, null) ? null : viewModel.Weight,
                    Height = Equals(viewModel.Height, null) ? null : viewModel.Height,
                    Width = Equals(viewModel.Width, null) ? null : viewModel.Width,
                    Length = Equals(viewModel.Length, null) ? null : viewModel.Length,
                    BackOrderMessage = Equals(viewModel.BackOrderMsg, null) ? String.Empty : HttpUtility.HtmlEncode(viewModel.BackOrderMsg),
                    InStockMessage = Equals(viewModel.InStockMsg, null) ? String.Empty : HttpUtility.HtmlEncode(viewModel.InStockMsg),
                    OutOfStockMessage = Equals(viewModel.OutOfStockMsg, null) ? String.Empty : HttpUtility.HtmlEncode(viewModel.OutOfStockMsg),
                    AdditionalInfo = Equals(viewModel.ShippingInformation, null) ? String.Empty : HttpUtility.HtmlEncode(viewModel.ShippingInformation),
                    Specifications = Equals(viewModel.ProductSpecifications, null) ? String.Empty : HttpUtility.HtmlEncode(viewModel.ProductSpecifications),
                    Description = Equals(viewModel.LongDescription, null) ? String.Empty : HttpUtility.HtmlEncode(viewModel.LongDescription),
                    FeaturesDescription = Equals(viewModel.FeaturesDescription, null) ? String.Empty : HttpUtility.HtmlEncode(viewModel.FeaturesDescription),
                    ShortDescription = Equals(viewModel.ShortDescription, null) ? String.Empty : HttpUtility.HtmlEncode(viewModel.ShortDescription),
                    SeoDescription = Equals(viewModel.SeoDescription, null) ? String.Empty : HttpUtility.HtmlEncode(viewModel.SeoDescription),
                    SeoKeywords = Equals(viewModel.SeoKeywords, null) ? String.Empty : HttpUtility.HtmlEncode(viewModel.SeoKeywords),
                    SeoTitle = Equals(viewModel.SeoTitle, null) ? String.Empty : HttpUtility.HtmlEncode(viewModel.SeoTitle),
                    IsFranchisable = Equals(viewModel.Franchisable, null) ? false : viewModel.Franchisable,
                    AllowRecurringBilling = Equals(viewModel.RecurringBillingInd, null) ? false : viewModel.RecurringBillingInd,
                    RecurringBillingInitialAmount = Equals(viewModel.RecurringBillingInitialAmount, null) ? null : viewModel.RecurringBillingInitialAmount,
                    RecurringBillingFrequency = Equals(viewModel.RecurringBillingFrequency, null) ? string.Empty : viewModel.RecurringBillingFrequency,
                    SeoUrl = Equals(viewModel.SeoPageUrl, null) ? string.Empty : HttpUtility.HtmlDecode(viewModel.SeoPageUrl),
                    IsHomepageSpecial = Equals(viewModel.HomepageSpecial, null) ? false : viewModel.HomepageSpecial,
                    RedirectUrlInd = viewModel.RedirectUrlInd,
                    SkuName = viewModel.Sku,
                    QuantityOnHand = viewModel.QuantityOnHand,
                    ReorderLevel = Equals(viewModel.ReorderLevel, 0) ? null : viewModel.ReorderLevel,
                    AttributeIds = viewModel.ProductAttributes,
                    SkuId = viewModel.SkuId,
                    AffiliateUrl = viewModel.AffiliateUrl,
                    SupplierId = viewModel.SupplierId,
                    ManufacturerId = viewModel.BrandId,
                    CategoryIds = viewModel.CategoryIds,
                    AccountId = viewModel.AccountID,
                    PortalId = viewModel.PortalID,
                    UserType = viewModel.UserType,
                    UserName = HttpContext.Current.User.Identity.Name,
                    ProductIds = viewModel.ProductIds
                };
                return model;
            }
            else
            {
                return new ProductModel();
            }
        }

        /// <summary>
        /// To map IEnumerable<ProductTypeModel> model to List<SelectListItem>
        /// </summary>
        /// <param name="model">IEnumerable<ProductTypeModel></param>
        /// <returns>List<SelectListItem></returns>
        public static List<SelectListItem> ToListItems(IEnumerable<ProductTypeModel> model)
        {
            List<SelectListItem> productTypeItems = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                productTypeItems = (from item in model
                                    select new SelectListItem
                                    {
                                        Text = item.Name,
                                        Value = item.ProductTypeId.ToString(),
                                    }).ToList();
            }
            return productTypeItems;
        }

        /// <summary>
        /// To map IEnumerable<ManufacturerModel> model to List<SelectListItem>
        /// </summary>
        /// <param name="model">IEnumerable<ManufacturerModel></param>
        /// <returns>returns List of SelectListItem</returns>
        public static List<SelectListItem> ToListItems(IEnumerable<ManufacturerModel> model)
        {
            List<SelectListItem> manufacturerItems = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                manufacturerItems = (from item in model
                                     select new SelectListItem
                                     {
                                         Text = item.Name,
                                         Value = item.ManufacturerId.ToString(),
                                     }).ToList();
            }
            return manufacturerItems;
        }

        /// <summary>
        ///  To map IEnumerable<SupplierModel> model to List<SelectListItem>
        /// </summary>
        /// <param name="model">IEnumerable<SupplierModel></param>
        /// <returns>returns List of SelectListItem</returns>
        public static List<SelectListItem> ToListItems(IEnumerable<SupplierModel> model)
        {
            List<SelectListItem> supplierItems = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                supplierItems = (from item in model
                                 select new SelectListItem
                                 {
                                     Text = item.Name,
                                     Value = item.SupplierId.ToString(),
                                 }).ToList();
            }
            return supplierItems;
        }

        /// <summary>
        /// To map IEnumerable<TaxClassModel> model to List<SelectListItem>
        /// </summary>
        /// <param name="model">IEnumerable<TaxClassModel> </param>
        /// <returns>returns List of SelectListItem</returns>
        public static List<SelectListItem> ToListItems(IEnumerable<TaxClassModel> model)
        {
            List<SelectListItem> taxClassItems = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                taxClassItems = (from item in model
                                 select new SelectListItem
                                 {
                                     Text = item.Name,
                                     Value = item.TaxClassId.ToString(),
                                 }).ToList();
            }
            return taxClassItems;
        }

        /// <summary>
        /// To map IEnumerable<ShippingRuleTypeModel> model to List<SelectListItem>
        /// </summary>
        /// <param name="model">IEnumerable<ShippingRuleTypeModel></param>
        /// <returns>IEnumerable<ShippingRuleTypeModel></returns>
        public static List<SelectListItem> ToListItems(IEnumerable<ShippingRuleTypeModel> model)
        {
            List<SelectListItem> shippingRuleTypeItems = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                shippingRuleTypeItems = (from item in model
                                         select new SelectListItem
                                         {
                                             Text = item.Name,
                                             Value = item.ShippingRuleTypeId.ToString(),
                                         }).ToList();
            }
            return shippingRuleTypeItems;
        }

        /// <summary>
        /// To get list items from DurationType enum
        /// </summary>
        /// <returns>returns list on DurationType items</returns>
        public static List<SelectListItem> ToListItems()
        {
            List<SelectListItem> expirationFrequencyItems = new List<SelectListItem>();
            expirationFrequencyItems.Add(new SelectListItem { Value = Convert.ToInt32(DurationType.Days).ToString(), Text = DurationType.Days.ToString() });
            expirationFrequencyItems.Add(new SelectListItem { Value = Convert.ToInt32(DurationType.Weeks).ToString(), Text = DurationType.Weeks.ToString() });
            expirationFrequencyItems.Add(new SelectListItem { Value = Convert.ToInt32(DurationType.Months).ToString(), Text = DurationType.Months.ToString() });
            expirationFrequencyItems.Add(new SelectListItem { Value = Convert.ToInt32(DurationType.Years).ToString(), Text = DurationType.Years.ToString() });
            return expirationFrequencyItems;
        }

        /// <summary>
        ///  To map AttributeTypeValueListModel to List of AttributeTypeValueViewModel
        /// </summary>
        /// <param name="models">List of model</param>
        /// <returns>returns list of AttributeTypeValueViewModel view model.</returns>
        public static List<AttributeTypeValueViewModel> ToListViewModel(AttributeTypeValueListModel models)
        {
            List<AttributeTypeValueViewModel> list = new List<AttributeTypeValueViewModel>();

            if (!Equals(models, null) && !Equals(models.AttributeTypeValueList, null))
            {
                var dataModel = models.AttributeTypeValueList.ToList().GroupBy(x => x.AttributeTypeId)
                                          .Select(grp => new { AttributeTypeId = grp.Key, dataModel = grp.ToList() })
                                          .ToList();

                for (int counter = 0; counter < dataModel.Count; counter++)
                {
                    var model = dataModel[counter];
                    AttributeTypeValueViewModel items = new AttributeTypeValueViewModel();
                    List<SelectListItem> listItems = new List<SelectListItem>();
                    items.AttributeName = model.dataModel[0].AttributeType;
                    items.AttributeTypeId = model.dataModel[0].AttributeTypeId;
                    items.IsGiftCard = model.dataModel[0].IsGiftCard;
                    foreach (AttributeTypeValueModel lst in model.dataModel)
                    {
                        listItems.Add(new SelectListItem { Value = lst.AttributeId.ToString(), Text = lst.AttributeValue });
                    }
                    items.AttributeValueList = listItems;
                    list.Add(items);
                }
            }
            return list;
        }

        /// <summary>
        /// To map ProductListModel to List of ProductListViewModel
        /// </summary>
        /// <param name="models"></param>
        /// <returns></returns>
        public static ProductListViewModel ToListViewModel(ProductListModel models, int? pageIndex = null, int? recordPerPage = null)
        {
            if (!Equals(models, null) && !Equals(models.Products, null))
            {
                var viewModel = new ProductListViewModel()
                {
                    Products = models.Products.ToList().Select(
                    x => new ProductViewModel()
                    {
                        ProductId = !Equals(x.ProductId, null) ? x.ProductId : 0,
                        Name = !Equals(x.Name, null) ? x.Name : string.Empty,
                        DisplayOrder = !Equals(x.DisplayOrder, null) ? x.DisplayOrder : null,
                        IsActive = x.IsActive,
                        ProductCode = !Equals(x.ProductNumber, null) ? x.ProductNumber : x.ProductNum,
                        ShortDescription = !Equals(x.ShortDescription, null) ? x.ShortDescription : string.Empty,
                        Price = Equals(x.SalePrice, null) ? MvcAdminUnits.GetCurrencyValue(Convert.ToDecimal(x.RetailPrice)) : MvcAdminUnits.GetCurrencyValue(Convert.ToDecimal(x.SalePrice)),
                        ImageFile = !string.IsNullOrEmpty(x.ImageFile) ? x.ImageFile : PortalAgent.CurrentPortal.ImageNotAvailablePathUrl,
                    }).ToList()
                };
                viewModel.RecordPerPage = recordPerPage ?? 10;
                viewModel.Page = pageIndex ?? 1;
                viewModel.TotalResults = Convert.ToInt32(models.TotalResults);
                return viewModel;
            }
            else
            {
                return new ProductListViewModel();
            }
        }

        /// <summary>
        /// Maps the  ProductTierPricingViewModel view model to ProductTierPricingModel model.
        /// </summary>
        /// <param name="viewModel">The model of type ProductTierPricingViewModel.</param>
        /// <returns>Returns the mapped ProductTierPricingViewModel model to ProductTierPricingModel.</returns>
        public static ProductTierPricingModel ToProductTierModel(ProductTierPricingViewModel viewModel)
        {
            ProductTierPricingModel model = new ProductTierPricingModel();

            if (!Equals(viewModel, null))
            {
                model = new ProductTierPricingModel();
                model.ProductId = viewModel.ProductId;
                model.ProductTierID = viewModel.ProductTierId;
                model.ProfileID = viewModel.ProfileId;
                model.TierStart = Convert.ToInt32(viewModel.TierStart);
                model.TierEnd = Convert.ToInt32(viewModel.TierEnd);
                model.Price = Convert.ToDecimal(viewModel.Price);
                model.UserType = viewModel.UserType;
                model.UserName = HttpContext.Current.User.Identity.Name;
            }
            return model;
        }

        /// <summary>
        /// Maps the  ProductTierPricingModel model to ProductTierPricingViewModel view model.
        /// </summary>
        /// <param name="model">The model of type ProductTierPricingModel.</param>
        /// <returns>Returns the mapped ProductTierPricingModel model to ProductTierPricingViewModel.</returns>
        public static ProductTierPricingViewModel ToProductTierViewModel(ProductTierPricingModel model)
        {
            ProductTierPricingViewModel viewModel = new ProductTierPricingViewModel();

            if (!Equals(model, null))
            {
                viewModel = new ProductTierPricingViewModel();
                viewModel.ProductId = model.ProductId;
                viewModel.ProductTierId = model.ProductTierID;
                viewModel.ProfileId = model.ProfileID;
                viewModel.TierStart = model.TierStart;
                viewModel.TierEnd = model.TierEnd;
                viewModel.Price = model.Price;
            }
            return viewModel;
        }

        /// <summary>
        /// Maps the List of TierListModel model to ProductTierListViewModel view model.
        /// </summary>
        /// <param name="listModel">The list model of type TierListModel.</param>
        /// <param name="pageIndex">Start index for the page</param>
        /// <param name="recordPerPage">No of record to be display per page</param>
        /// <returns>Returns the mapped TierListModel model to ProductTierListViewModel.</returns>
        public static ProductTierListViewModel ToProductTierListViewModel(TierListModel listModel, int? pageIndex, int? recordPerPage)
        {
            ProductTierListViewModel viewModel = new ProductTierListViewModel();

            if (!Equals(listModel, null) && !Equals(listModel.Tiers, null))
            {
                foreach (var item in listModel.Tiers)
                {
                    ProductTierPricingViewModel model = new ProductTierPricingViewModel();
                    model.ProductId = item.ProductId;
                    model.ProductTierId = item.ProductTierId;
                    model.ProfileId = item.ProfileId;
                    model.TierStart = item.TierStart;
                    model.TierEnd = item.TierEnd;
                    model.ProfileName = Equals(item.ProfileName, null) ? allProfilesText : item.ProfileName;
                    model.PriceDisplay = MvcAdminUnits.GetCurrencyValue(item.Price);
                    viewModel.Tiers.Add(model);
                }
            }

            viewModel.RecordPerPage = recordPerPage ?? 10;
            viewModel.Page = pageIndex ?? 1;
            viewModel.TotalResults = Convert.ToInt32(listModel.TotalResults);

            return viewModel;
        }

        /// <summary>
        /// Maps the TierModel model to ProductTierPricingViewModel view model.
        /// </summary>
        /// <param name="model">The model of type TierModel.</param>
        /// <returns>Returns the mapped TierModel model to ProductTierPricingViewModel.</returns>
        public static ProductTierPricingViewModel ToProductTierViewModel(TierModel model)
        {
            ProductTierPricingViewModel viewModel = new ProductTierPricingViewModel();

            if (!Equals(model, null))
            {
                viewModel = new ProductTierPricingViewModel();
                viewModel.ProductId = model.ProductId;
                viewModel.ProductTierId = model.ProductTierId;
                viewModel.ProfileId = model.ProfileId;
                viewModel.TierStart = model.TierStart;
                viewModel.TierEnd = model.TierEnd;
                viewModel.Price = !Equals(model.Price, null) ? HelperMethods.FormatPrice(model.Price) : 0;

            }
            return viewModel;
        }

        /// <summary>
        /// Maps the DigitalAssetViewModel view model to DigitalAssetModel model.
        /// </summary>
        /// <param name="viewModel">The model of type DigitalAssetViewModel.</param>
        /// <returns>Returns the mapped DigitalAssetViewModel model to DigitalAssetModel.</returns>
        public static DigitalAssetModel ToDigitalAssetModel(DigitalAssetViewModel viewModel)
        {
            DigitalAssetModel model = new DigitalAssetModel();

            if (!Equals(viewModel, null))
            {
                model = new DigitalAssetModel();
                model.DigitalAssetID = viewModel.DigitalAssetID;
                model.ProductID = viewModel.ProductID;
                model.DigitalAsset = viewModel.DigitalAsset;
                model.OrderLineItemID = viewModel.OrderLineItemID;
                model.UserName = HttpContext.Current.User.Identity.Name;
                model.UserType = viewModel.UserType;
            }
            return model;
        }

        /// <summary>
        /// Maps the DigitalAssetModel model to DigitalAssetViewModel View model.
        /// </summary>
        /// <param name="model">The model of type DigitalAssetModel.</param>
        /// <returns>Returns the mapped DigitalAssetModel model to DigitalAssetViewModel.</returns>
        public static DigitalAssetViewModel ToDigitalAssetViewModel(DigitalAssetModel model)
        {
            DigitalAssetViewModel viewModel = new DigitalAssetViewModel();

            if (!Equals(model, null))
            {
                viewModel.DigitalAssetID = model.DigitalAssetID;
                viewModel.ProductID = model.ProductID;
                viewModel.DigitalAsset = model.DigitalAsset;
                viewModel.OrderLineItemID = model.OrderLineItemID;
            }
            return viewModel;
        }

        /// <summary>
        /// Maps the DigitalAssetLisModel model to DigitalAssetListViewModel View model.
        /// </summary>
        /// <param name="listModel">The list model of type DigitalAssetLisModel.</param>
        /// <param name="pageIndex">Start index for the page</param>
        /// <param name="recordPerPage">No of record to be display per page</param>
        /// <returns>Returns the mapped DigitalAssetLisModel model to DigitalAssetListViewModel.</returns>
        public static DigitalAssetListViewModel ToDigitalAssetListViewModel(DigitalAssetLisModel listModel, int? pageIndex, int? recordPerPage)
        {
            DigitalAssetListViewModel viewModel = new DigitalAssetListViewModel();

            if (!Equals(listModel, null) && !Equals(listModel.DigitalAssets, null))
            {
                foreach (var item in listModel.DigitalAssets)
                {
                    DigitalAssetViewModel model = new DigitalAssetViewModel();
                    model.ProductID = item.ProductID;
                    model.DigitalAssetID = item.DigitalAssetID;
                    model.DigitalAsset = item.DigitalAsset;
                    model.Assigned = Equals(item.OrderLineItemID, 0) ? false : true;
                    viewModel.DigitalAssets.Add(model);
                }
            }
            viewModel.RecordPerPage = recordPerPage ?? 10;
            viewModel.Page = pageIndex ?? 1;
            viewModel.TotalResults = Convert.ToInt32(listModel.TotalResults);

            return viewModel;
        }

        /// <summary>
        /// Maps the Collection of ProductModel model to VendorProductListViewModel View model.
        /// </summary>
        /// <param name="models">Collection of type ProductModel</param>
        /// <param name="totalResults">Total Records count</param>
        /// <returns>Returns the mapped Collection of ProductModel model to VendorProductListViewModel.</returns>
        public static VendorProductListViewModel ToVendorProductListViewModel(Collection<ProductModel> models, int? totalResults = 0)
        {
            VendorProductListViewModel list = new VendorProductListViewModel();
            if (!Equals(models, null))
            {
                foreach (ProductModel model in models)
                {
                    list.Products.Add(ToViewModelList(model));
                }
            }

            list.TotalResults = (Equals(totalResults, 0)) ? Convert.ToInt32(list.TotalResults) : Convert.ToInt32(totalResults);
            return list;
        }

        /// <summary>
        /// Maps the ProductReviewStateListModel model to ProductReviewStateListViewModel View model.
        /// </summary>
        /// <param name="model">The model of type ProductReviewStateListModel.</param>
        /// <returns>Returns the mapped ProductReviewStateListModel model to ProductReviewStateListViewModel.</returns>
        public static ProductReviewStateListViewModel ToProductReviewListModel(ProductReviewStateListModel model)
        {
            ProductReviewStateListViewModel viewModel = new ProductReviewStateListViewModel();
            if (!Equals(model, null))
            {
                viewModel = new ProductReviewStateListViewModel()
                {
                    ProductReviewStates = model.ProductReviewStates.ToList().Select(
                    x => new ProductReviewStateViewModel()
                    {
                        ReviewStateID = x.ReviewStateID,
                        ReviewStateName = x.ReviewStateName,
                        Description = x.Description,
                    }).ToList()
                };
            }
            return viewModel;
        }

        /// <summary>
        /// Convert multiple Product models to list of product view models
        /// </summary>
        /// <param name="models"></param>
        /// <returns></returns>
        public static List<ProductViewModel> ToViewModels(Collection<ProductModel> models)
        {
            if (Equals(models, null))
            {
                return null;
            }
            //Znode 7.2.2
            //Showing Revies and Ratings on Home page-Starts
            var list = new List<ProductViewModel>();

            foreach (ProductModel model in models)
            {
                list.Add(ToViewModel(model));
            }
            //Showing Revies and Ratings on Home page-Ends
            return list;
        }

        /// <summary>
        /// Convert ProductModel to ProductViewModel
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static ProductViewModel ToDetailsViewModel(ProductModel model)
        {
            if (!Equals(model, null))
            {
                ProductViewModel viewModel = new ProductViewModel()
            {
                BundleItemsIds = model.BundledProductIds,
                Description = model.Description,
                IsCallForPricing = model.CallForPricing,
                ImageAltTag = model.ImageAltTag,
                ImageFile = model.ImageFile,
                MaxQuantity = model.MaxSelectableQuantity.GetValueOrDefault(10),
                MinQuantity = model.MinSelectableQuantity.GetValueOrDefault(1),
                Name = model.Name,
                OutOfStockMessage = model.OutOfStockMessage,
                UnitPrice = GetProductPrice(model, originalPriceTextinLowercase),
                ProductPrice = GetProductPrice(model, price),
                OriginalPrice = GetProductPrice(model, originalPriceTextinLowercase),
                ProductId = model.ProductId,
                ProductCode = model.ProductNumber,
                SeoDescription = model.SeoDescription,
                SeoKeywords = model.SeoKeywords,
                SeoTitle = model.SeoTitle,
                SeoPageName = model.SeoUrl,
                ShortDescription = model.ShortDescription,
                IsActive = model.IsActive,
                AllowBackOrder = model.AllowBackOrder,
                TrackInventory = model.TrackInventory,
                BackOrderMessage = model.BackOrderMessage,
                InStockMessage = model.InStockMessage,
                Images = ToImageViewModel(model.Images),
                ImageSmallThumbnailPath = HelperMethods.GetImagePath((model.Skus != null && model.Skus.Any() && (Path.GetFileName(model.Skus.First().ImageSmallThumbnailPath) != "No Image" && Path.GetFileName(model.Skus.First().ImageSmallThumbnailPath) != "No Swatch Image")) ? HelperMethods.GetImagePath(model.Skus.First().ImageSmallThumbnailPath) : HelperMethods.GetImagePath(model.ImageSmallThumbnailPath)),
                ProductSpecification = model.Specifications,
                LongDescription = Equals(model.Description, null) ? String.Empty : HttpUtility.HtmlDecode(model.Description),
                RecurringBillingFrequency = model.RecurringBillingFrequency,
                RecurringBillingInd = model.AllowRecurringBilling,
                RecurringBillingInitialAmount = model.RecurringBillingInitialAmount
            };

                ImageHelper imageHelper = new ImageHelper();
                viewModel.ImageLargePath = HelperMethods.GetImageRelativePath(imageHelper.GetImageHttpPathLarge(model.ImageFile));
                viewModel.ImageMediumPath = HelperMethods.GetImageRelativePath(imageHelper.GetImageHttpPathMedium(model.ImageFile));

                if (model.Skus.Count > 0)
                {
                    viewModel.Sku = model.Skus.FirstOrDefault().Sku;
                }

                viewModel.ProductsAttributes = ProductAttributesViewModelMap.ToViewModel(model.Attributes, model);

                if (model.AddOns != null && model.AddOns.Count > 0)
                {
                    viewModel.AddOns = model.AddOns.Select(AddOnViewModelMap.ToViewModel).Where(x => x != null).ToList();
                }

                if (model.Reviews.Any())
                {
                    var reviewModel = model.Reviews.Where(x => x.Status == "A").OrderByDescending(x => x.CreateDate);
                    viewModel.Rating = reviewModel.Any() ? Convert.ToInt16(reviewModel.Average(x => x.Rating)) : 0;
                }
                else
                {
                    viewModel.Rating = 0;
                }

                if (model.FrequentlyBoughtTogether.Any())
                {
                    viewModel.FBTProductsIds = string.Join(",", model.FrequentlyBoughtTogether.Select(fbt => fbt.RelatedProductId.ToString()));

                }
                if (model.YouMayAlsoLike.Any())
                {
                    viewModel.YMALProductsIds = string.Join(",", model.YouMayAlsoLike.Select(ymal => ymal.RelatedProductId.ToString()));
                }
                return viewModel;
            }
            else
            {
                return new ProductViewModel();
            }
        }

        /// <summary>
        /// Get the available profile list.
        /// </summary>
        /// <param name="list"></param>
        /// <param name="profileId"></param>
        /// <returns></returns>
        public static List<SelectListItem> ToSelectList(List<ProfileModel> list, int profileId = 0)
        {
            List<SelectListItem> selectList = new List<SelectListItem>();
            if (!Equals(list, null))
            {
                foreach (var item in list)
                {
                    selectList.Add(new SelectListItem()
                    {

                        Text = item.Name,
                        Value = item.ProfileId.ToString(),
                        Selected = Equals(item.ProfileId, profileId) ? true : false
                    });
                }
                return selectList;
            }
            else
            {
                return new List<SelectListItem>();
            }
        }

        /// <summary>
        /// Get the product price based on product model and price type
        /// </summary>
        /// <param name="model">ProductModel model</param>
        /// <param name="priceType">string price type</param>
        /// <returns>decimal product price</returns>
        private static decimal GetProductPrice(ProductModel model, string priceType)
        {
            decimal finalPrice = 0.00m;

            if (priceType.ToLower().Equals(originalPriceTextinLowercase))
            {
                finalPrice = model.RetailPrice.HasValue ? model.RetailPrice.Value : 0;
                return finalPrice;
            }

            if (model.PromotionPrice.HasValue)
            {
                finalPrice = model.PromotionPrice.Value;
                return finalPrice;
            }

            if (model.SalePrice.HasValue)
            {
                finalPrice = model.SalePrice.Value;
                return finalPrice;
            }

            finalPrice = model.RetailPrice.HasValue ? model.RetailPrice.Value : 0;
            return finalPrice;
        }

        /// <summary>
        /// Convert list of image model to list of image view model
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private static List<ImageViewModel> ToImageViewModel(Collection<ImageModel> model)
        {
            if (!Equals(model, null))
            {
                List<ImageViewModel> imageViewModel = model.Select(x => new ImageViewModel()
            {
                AlternateThumbnailImageFile = x.AlternateThumbnailImageFile,
                DisplayOrder = x.DisplayOrder,
                ImageAltTag = x.ImageAltTag,
                ImageFile = x.ImageFile,
                ImageLargePath = HelperMethods.GetImagePath(x.ImageLargePath),
                ImageMediumPath = HelperMethods.GetImagePath(x.ImageMediumPath),
                ImageSmallPath = HelperMethods.GetImagePath(x.ImageSmallPath),
                ImageSmallThumbnailPath = HelperMethods.GetImagePath(x.ImageSmallThumbnailPath),
                ImageThumbnailPath = HelperMethods.GetImagePath(x.ImageThumbnailPath),
                IsActive = x.IsActive,
                Name = x.Name,
                ProductId = x.ProductId,
                ProductImageId = x.ProductImageId,
                ProductImageTypeId = x.ProductImageTypeId,
                ReviewStateId = x.ReviewStateId,
                ShowOnCategoryPage = x.ShowOnCategoryPage
            }).ToList();
                return imageViewModel;
            }
            else
            {
                return new List<ImageViewModel>();
            }
        }

        /// <summary>
        /// Maps the Collection of AttributeModel model & Product Model to ProductAttributesViewModel View model.
        /// </summary>
        /// <param name="attributes">Collection of Type AttributeModel</param>
        /// <param name="product">model of type ProductModel</param>
        /// <returns>Returns the mapped Collection of AttributeModel model & Product Model to ProductAttributesViewModel.</returns>
        public static ProductAttributesViewModel ToAttributesViewModel(Collection<AttributeModel> attributes, ProductModel product)
        {
            var viewModel = new ProductAttributesViewModel()
            {
                ProductId = product.ProductId,
            };
            var attributeTypes = attributes.Where(model => model.IsActive).Select(model => model.AttributeTypeId).Distinct();

            foreach (var attributTypeId in attributeTypes)
            {
                var type = new AttributeTypesViewModel();
                var attributesValues = attributes.Where(model => model.AttributeTypeId.Equals(attributTypeId)).ToList();

                foreach (var attributesValue in attributesValues)
                {
                    var skuIds =
                        product.Skus.Where(
                            s =>
                            s.Attributes.Any(
                                x =>
                                x.AttributeId == attributesValue.AttributeId &&
                                x.AttributeTypeId == attributesValue.AttributeTypeId)).Select(x => x.SkuId);

                    var attribute = ProductViewModelMap.ToEntity(attributesValue);

                    if (skuIds.Any())
                        attribute.SkuIds = new Collection<int>(skuIds.ToList());

                    type.ProductAttributes.Add(attribute);
                }

                type.Name = attributesValues.FirstOrDefault().AttributeType.Name;
                type.AttributeTypeId = type.ProductAttributes.First().AttributeTypeId;

                viewModel.Attributes.Add(type);
            }
            return viewModel;

        }

        /// <summary>
        /// Maps the AttributeModel model  to AttributesViewModel View model.
        /// </summary>
        /// <param name="model">Model of type AttributeModel</param>
        /// <returns>Returns the mapped AttributeModel model to AttributesViewModel. </returns>
        public static AttributesViewModel ToEntity(AttributeModel model)
        {
            if (model != null)
            {
                return new AttributesViewModel()
                {
                    AttributeId = model.AttributeId,
                    DisplayOrder = model.DisplayOrder,
                    AttributeTypeId = model.AttributeTypeId,
                    Name = model.Name

                };
            }
            return null;
        }

        public static List<ProductViewModel> ToDetailListViewModel(Collection<ProductModel> models)
        {
            List<ProductViewModel> products = new List<ProductViewModel>();
            if (!Equals(models, null))
            {
                foreach (ProductModel model in models)
                {
                    products.Add(ToDetailsViewModel(model));
                }
            }
            return products;
        }

        /// <summary>
        /// Convert list of product model to list of product bundle view model
        /// </summary>
        /// <param name="models"></param>
        /// <returns></returns>
        public static List<BundleDisplayViewModel> GetBundleDisplayViewModel(IEnumerable<ProductModel> models)
        {
            if (!Equals(models, null))
            {
                return models.Select(product => new BundleDisplayViewModel()
                {
                    Name = product.Name,
                    ImageAltTag = product.ImageAltTag,
                    ImageFile = product.ImageFile,
                    ImageLargePath = HelperMethods.GetImagePath(product.ImageLargePath),
                    ImageMediumPath = HelperMethods.GetImagePath(product.ImageMediumPath),
                    ImageSmallPath = HelperMethods.GetImagePath(product.ImageSmallPath),

                }).ToList();

            }

            return null;
        }
    }
}