﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    /// <summary>
    /// Static Class for ContentPage View Model Mapper 
    /// </summary>
    public static class ContentPageViewModelMap
    {
        /// <summary>
        /// Convert ContentPageModel to ContentPageViewModel
        /// </summary>
        /// <param name="viewModel">to covert to view model</param>
        /// <returns>ContentPageViewModel</returns>
        public static ContentPageViewModel ToViewModel(ContentPageModel model)
        {
            if (!Equals(model, null))
            {
                return new ContentPageViewModel()
                {
                    ActiveInd = model.ActiveInd,
                    AdditionalMetaInformation = model.MetaTagAdditional,
                    AllowDelete = model.AllowDelete,
                    ContentPageId = model.ContentPageID,
                    CSSID = (Equals(model.CSSID, 0)) ? null : model.CSSID,
                    IsUrlRedirectEnabled = model.IsUrlRedirectEnabled,
                    LocaleId = model.LocaleId,
                    PageName = model.Name,
                    PageTitle = model.Title,
                    PortalID = (Equals(model.PortalID, null)) ? 0 : model.PortalID,
                    SEODescription = model.SEOMetaDescription,
                    SEOFriendlyPageName = model.SEOURL,
                    SEOKeywords = model.SEOMetaKeywords,
                    ThemeID = (Equals(model.ThemeID, null)) ? null : model.ThemeID,
                    UpdatedUser = model.UpdatedUser,
                    Html = Equals(model.Html, null) ? String.Empty : model.Html,
                    MappedSeoUrl = model.MappedSeoUrl,
                    MasterPageID = (Equals(model.MasterPageID, 0)) ? null : model.MasterPageID,
                    SEOTitle = model.SEOTitle,
                    CustomErrorMessage = model.CustomErrorMessage,
                    OldHtml = Equals(model.OldHtml, null) ? String.Empty : model.OldHtml,
                    StoreName = Equals(model.StoreName, null) ? String.Empty : model.StoreName,
                    MasterPageName = Equals(model.MasterPageName, null) ? String.Empty : model.MasterPageName
                };
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Convert ContentPageViewModel to ContentPageModel.
        /// </summary>
        /// <param name="viewModel">to covert to model.</param>
        /// <returns>ContentPageModel</returns>
        public static ContentPageModel ToModel(ContentPageViewModel viewModel)
        {
            if (!Equals(viewModel, null))
            {
                return new ContentPageModel()
                {
                    ActiveInd = viewModel.ActiveInd,
                    MetaTagAdditional = Equals(viewModel.AdditionalMetaInformation, null) ? string.Empty : viewModel.AdditionalMetaInformation,
                    AllowDelete = Equals(viewModel.PageName.ToLower(), MvcAdminConstants.ContentPageHomeKey) ? false : true,
                    ContentPageID = viewModel.ContentPageId,
                    CSSID = (viewModel.CSSID > 0) ? viewModel.CSSID : null,
                    IsUrlRedirectEnabled = viewModel.IsUrlRedirectEnabled,
                    LocaleId = viewModel.LocaleId,
                    Name = viewModel.PageName,
                    Title = viewModel.PageTitle,
                    PortalID = (Equals(viewModel.PortalID, null)) ? 0 : viewModel.PortalID,
                    SEOMetaDescription = Equals(viewModel.SEODescription, null) ? string.Empty : viewModel.SEODescription,
                    SEOURL = (!Equals(viewModel.SEOFriendlyPageName,null) && viewModel.SEOFriendlyPageName.Trim().Length > 0 && !viewModel.PageName.ToLower().Equals(MvcAdminConstants.ContentPageHomeKey)) ? viewModel.SEOFriendlyPageName.Trim().Replace(" ", "-") : string.Empty,
                    SEOMetaKeywords = Equals(viewModel.SEOKeywords, null) ? string.Empty : viewModel.SEOKeywords,
                    SEOTitle = Equals(viewModel.SEOTitle, null) ? string.Empty : viewModel.SEOTitle,
                    ThemeID = (viewModel.ThemeID > 0) ? viewModel.ThemeID : null,
                    CustomErrorMessage = Equals(viewModel.CustomErrorMessage, null) ? string.Empty : viewModel.CustomErrorMessage,
                    Html = Equals(viewModel.Html, null) ? String.Empty : viewModel.Html,
                    MappedSeoUrl = Equals(viewModel.MappedSeoUrl, null) ? string.Empty : viewModel.MappedSeoUrl,
                    TemplateName = Equals(viewModel.TemplateName, null) ? string.Empty : viewModel.TemplateName,
                    UpdatedUser = Equals(viewModel.UpdatedUser, null) ? string.Empty : viewModel.UpdatedUser,
                    MasterPageID = (viewModel.MasterPageID > 0) ? viewModel.MasterPageID : null,
                    OldHtml = Equals(viewModel.OldHtml, null) ? String.Empty : viewModel.OldHtml
                };
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Convert ContentPageListModel to ContentPageListViewModel.
        /// </summary>
        /// <param name="models">To convert to list view model.</param>
        /// <returns>ContentPageListViewModel</returns>
        public static ContentPageListViewModel ToListViewModel(ContentPageListModel models)
        {
            if (!Equals(models, null) && !Equals(models.ContentPages, null))
            {
                var viewModel = new ContentPageListViewModel()
                {
                    ContentPages = models.ContentPages.ToList().Select(
                    model => new ContentPageViewModel()
                    {
                        ActiveInd = model.ActiveInd,
                        AdditionalMetaInformation = model.MetaTagAdditional,
                        AllowDelete = model.AllowDelete,
                        ContentPageId = model.ContentPageID,
                        CSSID = (Equals(model.CSSID, null)) ? 0 : model.CSSID,
                        IsUrlRedirectEnabled = model.IsUrlRedirectEnabled,
                        LocaleId = model.LocaleId,
                        PageName = model.Name,
                        PageTitle = model.Title,
                        PortalID = model.PortalID,
                        SEODescription = model.SEOMetaDescription,
                        SEOFriendlyPageName = model.SEOURL,
                        SEOKeywords = model.SEOMetaKeywords,
                        ThemeID = model.ThemeID,
                        StoreName = Equals(model.StoreName, null) ? String.Empty : model.StoreName,
                        MasterPageName = Equals(model.MasterPageName, null) ? String.Empty : model.MasterPageName
                    }).ToList()
                };
                viewModel.TotalResults = Convert.ToInt32(models.TotalResults);
                return viewModel;
            }
            else
            {
                return new ContentPageListViewModel();
            }
        }

        public static List<SelectListItem> ToListItems(IEnumerable<CSSModel> model)
        {
            List<SelectListItem> cssItems = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                cssItems = (from item in model
                            select new SelectListItem
                            {
                                Text = item.Name,
                                Value = item.CSSID.ToString(),
                            }).ToList();
            }
            return cssItems;
        }

        public static List<SelectListItem> ToListItems(IEnumerable<ThemeModel> model)
        {
            List<SelectListItem> themeItems = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                themeItems = (from item in model
                              select new SelectListItem
                              {
                                  Text = item.Name,
                                  Value = item.ThemeID.ToString(),
                              }).ToList();
            }
            return themeItems;
        }

        public static List<SelectListItem> ToListItems(IEnumerable<PortalModel> model)
        {
            List<SelectListItem> portalItems = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                portalItems = (from item in model
                               select new SelectListItem
                               {
                                   Text = item.StoreName,
                                   Value = item.PortalId.ToString(),
                               }).ToList();
            }
            return portalItems;
        }

        public static List<SelectListItem> ToListItems(IEnumerable<MasterPageModel> model)
        {
            List<SelectListItem> masterPageItems = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                masterPageItems = (from item in model
                                   select new SelectListItem
                                   {
                                       Text = item.Name,
                                       Value = item.MasterPageId.ToString(),
                                   }).ToList();
            }
            return masterPageItems;
        }
    }
}