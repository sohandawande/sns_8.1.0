﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    public class VendorAccountViewModelMap
    {
        public static VendorAccountDownloadListViewModel ToDownload(VendorAccountListModel models)
        {

            if (!Equals(models, null) && !Equals(models.VendorAccount, null))
            {
                var viewModel = new VendorAccountDownloadListViewModel()
                {
                    VendorAccountDownloadList = models.VendorAccount.ToList().Select(
                    x => new VendorAccountDownloadViewModel()
                    {
                        AccountId = x.AccountId,
                        UserId = x.UserId,
                        Email = x.Email,
                        FirstName = x.FirstName,
                        LastName = x.LastName,
                        EnableCustomerPricing = Equals(x.EnableCustomerPricing, null) ? false : true,

                        PhoneNumber = x.PhoneNumber,
                        RoleName = x.RoleName
                    }).ToList()
                };

                viewModel.TotalResults = Convert.ToInt32(models.TotalResults);
                viewModel.Page = Convert.ToInt32(models.PageIndex);
                viewModel.RecordPerPage = Convert.ToInt32(models.PageSize);
                viewModel.TotalPages = Convert.ToInt32(models.TotalPages);
                return viewModel;
            }
            return new VendorAccountDownloadListViewModel();
        }
        /// <summary>
        /// Converts Account List model to Vendor Account view model
        /// </summary>
        /// <param name="models">IEnumerable VendorAccountModel</param>
        /// <param name="totalResults">total record result</param>
        /// <param name="recordPerPage">no of page</param>
        /// <param name="pageIndex">page index</param>
        /// <param name="totalPages">total pages</param>
        /// <returns>VendorAccountListViewModel</returns>
        public static VendorAccountListViewModel ToListViewModel(VendorAccountListModel models)
        {
            if (!Equals(models, null) && !Equals(models.VendorAccount, null))
            {
                var viewModel = new VendorAccountListViewModel()
                {
                    VendorAccount = models.VendorAccount.ToList().Select(
                    x => new VendorAccountViewModel()
                    {
                        VendorContact = new VendorContactViewModel()
                        {
                            FirstName = x.FirstName,
                            LastName = x.LastName,
                            PhoneNumber = x.PhoneNumber,
                            Email = x.Email,
                        },
                        accountViewModel = new AccountViewModel()
                        {
                            CreateDte = x.CreateDte,
                            UpdateDte = x.UpdateDte
                        },
                        AccountId = x.AccountId,
                        UserId = x.UserId,
                        EnableCustomerPricing = x.EnableCustomerPricing,
                        FullName = x.FullName,
                        RoleName = x.RoleName,
                        IsConfirmed = Equals(x.IsConfirmed, DBNull.Value) ? false : Convert.ToBoolean(x.IsConfirmed),
                    }).ToList()
                };
                viewModel.TotalResults = Convert.ToInt32(models.TotalResults);
                viewModel.Page = Convert.ToInt32(models.PageIndex);
                viewModel.RecordPerPage = Convert.ToInt32(models.PageSize);
                viewModel.TotalPages = Convert.ToInt32(models.TotalPages);
                return viewModel;
            }
            return new VendorAccountListViewModel();
        }

        /// <summary>
        /// View Model Mapper for vendor account
        /// </summary>
        /// <param name="model">VendorAccountModel</param>
        /// <returns>VendorAccountViewModel</returns>
        public static VendorAccountViewModel ToViewModel(VendorAccountModel model)
        {
            var viewModel = new VendorAccountViewModel()
            {
                AccountId = model.AccountId,
                FullName = model.FullName,
                CompanyName = model.CompanyName,
                Website = model.Website,
                IsEmailOptional = model.EmailOptIn,

                VendorContact = new VendorContactViewModel()
                {
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    Email = model.Email,
                    AccountId = model.AccountId,
                    PhoneNumber = model.PhoneNumber,
                    Street1 = model.Street,
                    Street2 = model.Street1,
                    State = model.StateCode,
                    City = model.City,
                    PostalCode = model.PostalCode,
                    CompanyName = model.AddressCompanyName,
                    ExternalAccountNum = Equals(model.ExternalAccountNo, null) ? string.Empty : model.ExternalAccountNo
                },

                accountViewModel = new AccountViewModel()
                {
                    CreateDte = model.CreateDte,
                    UpdateDte = model.UpdateDte,
                    Website = model.Website,
                    Description = model.Description,
                    CreateUser = model.CreateUser,
                    UpdateUser = model.UpdateUser,
                    Custom1 = model.Custom1,
                    Custom2 = model.Custom2,
                    Custom3 = model.Custom3,
                    Source = model.Source
                },
                ProfileId = model.ProfileId,
                UserName = model.UserName,
                CheckRole = model.CheckRole,
                ExternalAccountNum = Equals(model.ExternalAccountNo, null) ? string.Empty : model.ExternalAccountNo
            };
            return viewModel;
        }

        /// <summary>
        /// Model for Account Model
        /// </summary>
        /// <param name="model">VendorAccountViewModel</param>
        /// <returns>AccountModel</returns>
        public static AccountModel ToAccountModel(VendorAccountViewModel model)
        {
            return new AccountModel()
            {
                AddressModel = new AddressModel()
                {
                    City = model.VendorContact.City,
                    StreetAddress1 = model.VendorContact.Street1,
                    StreetAddress2 = model.VendorContact.Street2,
                    StateCode = model.VendorContact.State,
                    PostalCode = model.VendorContact.PostalCode,
                    CompanyName = model.VendorContact.CompanyName,
                    CountryCode = model.VendorContact.CountryCode,
                },

                User = new UserModel()
                {
                    Email = model.VendorContact.Email,
                    Username = model.UserName,
                    Password = model.Password,
                },
                AccountId = model.AccountId,
                ProfileId = model.ProfileId,
                Email = model.VendorContact.Email,
                EnableCustomerPricing = model.EnableCustomerPricing,
                FirstName = model.VendorContact.FirstName,
                FullName = model.FullName,
                LastName = model.VendorContact.LastName,
                PhoneNumber = model.VendorContact.PhoneNumber,
                RoleName = "Vendor",
                UserName = model.UserName,
                CompanyName = model.CompanyName,
                ExternalAccountNum = model.VendorContact.ExternalAccountNum,
                CreateUser = HttpContext.Current.User.Identity.Name,
                EmailOptIn = model.IsEmailOptional,
                UserType = model.UserType,
                Website = model.Website,
            };
        }


        /// <summary>
        /// Model Mapper for Store list in dropdown.
        /// </summary>
        /// <param name="model">IEnumerable PortalModel</param>
        /// <returns></returns>
        public static List<SelectListItem> ToListItems(IEnumerable<PortalModel> model)
        {
            List<SelectListItem> storeItems = new List<SelectListItem>();

            if (!Equals(model, null))
            {
                storeItems = (from item in model
                              select new SelectListItem
                              {
                                  Text = item.StoreName,
                                  Value = item.PortalId.ToString(),
                              }).ToList();
            }
            return storeItems;
        }

        /// <summary>
        /// Model Mapper for Countries list in dropdown.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static List<SelectListItem> ToListItems(IEnumerable<CountryModel> model)
        {
            List<SelectListItem> countryItems = new List<SelectListItem>();

            if (!Equals(model, null))
            {
                countryItems = (from item in model
                                select new SelectListItem
                                {
                                    Text = item.Name,
                                    Value = item.Code.ToString(),
                                }).ToList();
            }
            return countryItems;
        }

        /// <summary>
        /// Convert Address List Model to Address View Model.
        /// </summary>
        /// <param name="model">IEnumerable<AddressModel> model</param>
        /// <param name="totalResult">int? totalResult</param>
        /// <returns>Return Address List in AddressListViewModel format.</returns>
        public static AddressListViewModel ToAccountAddressList(IEnumerable<AddressModel> model, int? totalResult = 0)
        {
            if (!Equals(model, null))
            {
                var viewModel = new AddressListViewModel()
                {
                    AccountAddress = model.ToList().Select(
                    x => new AddressViewModel()
                    {
                        AccountId = x.AccountId,
                        AddressId = x.AddressId,
                        Name = x.Name,
                        IsDefaultBilling = x.IsDefaultBilling,
                        IsDefaultShipping = x.IsDefaultShipping,
                        FullAddress = FullAddress(x) 
                    }).ToList()
                };
                viewModel.TotalResults = Convert.ToInt32(totalResult);
                return viewModel;
            }
            return new AddressListViewModel();
        }

        /// <summary>
        /// To set full address of vendor.
        /// </summary>
        /// <param name="Address">AddressModel contains full address information</param>
        /// <returns>Full address string</returns>
        private static string FullAddress(AddressModel Address)
        {

            StringBuilder FullName = new StringBuilder();

            string phoneNumber = " PH : ";

            if (!Equals(Address.FirstName, string.Empty))
            {
                FullName.Append(Address.FirstName);
                if (!Equals(Address.LastName, string.Empty))
                    FullName.Append(" ");
            }

            if (!Equals(Address.LastName, string.Empty))
            {
                FullName.Append(Address.LastName);
                if (!Equals(Address.CompanyName, string.Empty))
                    FullName.Append(" ");
            }

            if (!Equals(Address.CompanyName, string.Empty))
            {
                FullName.Append(", "+ Address.CompanyName);
                if (!Equals(Address.StreetAddress1, string.Empty))
                    FullName.Append(" ");
            }

            if (!Equals(Address.StreetAddress1, string.Empty))
            {
                FullName.Append(", " + Address.StreetAddress1);
                if (!Equals(Address.StreetAddress2, string.Empty))
                    FullName.Append(" ");
            }

            if (!Equals(Address.StreetAddress2, string.Empty))
            {
                FullName.Append(", " + Address.StreetAddress2);
                if (!Equals(Address.City, string.Empty))
                    FullName.Append(" ");
            }

            if (!Equals(Address.City, string.Empty))
            {
                FullName.Append(", " + Address.City);
                if (!Equals(Address.StateCode, string.Empty))
                    FullName.Append(" ");
            }

            if (!Equals(Address.StateCode, string.Empty))
            {
                FullName.Append(", " + Address.StateCode);
                if (!Equals(Address.PostalCode, string.Empty))
                    FullName.Append(" ");
            }

            if (!Equals(Address.PostalCode, string.Empty))
            {
                FullName.Append(", " + Address.PostalCode);
                if (!Equals(Address.CountryCode, string.Empty))
                    FullName.Append(" ");
            }

            if (!(Equals(Address.CountryCode, string.Empty) || Equals(Address.CountryCode, null)))
            {
                FullName.Append(", " + Address.CountryCode);
                if (!Equals(Address.PhoneNumber, string.Empty))
                    FullName.Append(" ");
            }

            if (!Equals(Address.PhoneNumber, string.Empty))
            {
                FullName.Append(", " + phoneNumber);
                FullName.Append(Address.PhoneNumber);
                if (!Equals(Address.PhoneNumber, string.Empty))
                    FullName.Append(" ");
            }
            return FullName.ToString();
        }
    }
}