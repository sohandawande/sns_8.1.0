﻿using System;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    /// <summary>
    /// This is the Mapper class for the Store Admin
    /// </summary>
    public static class StoreAdminViewModelMap
    {
        /// <summary>
        /// Convert the Account List Model to the Store Admin List view model
        /// </summary>
        /// <param name="model">AccountListModel model</param>
        /// <returns>StoreAdminListViewModel</returns>
        public static StoreAdminListViewModel ToListViewModel(AccountListModel model)
        {
            if (!Equals(model, null) && !Equals(model.Accounts, null))
            {
                var viewModel = new StoreAdminListViewModel()
                {
                    StoreAdmins = model.Accounts.ToList().Select(
                    x => new StoreAdminViewModel()
                    {
                        AccountId = x.AccountId,
                        UserID = x.UserName,
                        Email = x.Email,
                        Name = x.Addresses.Count > 0 ? x.Addresses[0].Name : string.Empty,
                        PhoneNumber = x.Addresses.Count > 0 ? x.Addresses[0].PhoneNumber : string.Empty,
                        LoginName = x.LoginName,
                        AccountNumber = x.ExternalAccountNum,
                        CompanyName = x.CompanyName,
                        UserGuid = x.UserId,
                        IsConfirmed = Equals(x.User, null) ? false : x.User.IsConfirmed
                    }).ToList()
                };

                viewModel.TotalResults = Convert.ToInt32(model.TotalResults);
                viewModel.Page = Convert.ToInt32(model.PageIndex);
                viewModel.RecordPerPage = Convert.ToInt32(model.PageSize);
                viewModel.TotalPages = Convert.ToInt32(model.TotalPages);
                viewModel.TotalResults = Convert.ToInt32(model.TotalResults);

                return viewModel;
            }
            else
            {
                return new StoreAdminListViewModel();
            }
        }

        /// <summary>
        /// This method will convert Account Model to Store Admin View model
        /// </summary>
        /// <param name="model">AccountModel model</param>
        /// <returns>StoreAdminViewModel</returns>
        public static StoreAdminViewModel ToModel(AccountModel model)
        {
            if (!Equals(model, null))
            {
                return new StoreAdminViewModel()
                {
                    AccountId = model.AccountId,
                    UserID = Equals(model.User, null) ? string.Empty : model.User.Username,
                    Email = model.Email,
                    LoginName = Equals(model.User, null) ? string.Empty : model.User.Username,
                    AccountNumber = Equals(model.ExternalAccountNum, null) ? null : model.ExternalAccountNum,
                    EmailOptIn = model.EmailOptIn,
                    CreateUser = Equals(model.CreateUser, null) ? null : model.CreateUser,
                    StartDate = HelperMethods.ViewDateFormat(model.CreateDate.Value),
                    UpdateUser = Equals(model.UpdateUser, null) ? null : model.UpdateUser,
                    EndDate = Equals(model.UpdateDate, null) ? null : HelperMethods.ViewDateFormat(model.UpdateDate.Value),
                    IsConfirmed = Equals(model.User, null) ? false : model.User.IsApproved,
                    FirstName = Equals(model.Addresses, null) ? string.Empty : model.AddressModel.FirstName,
                    LastName = Equals(model.Addresses, null) ? string.Empty : model.AddressModel.LastName,
                    MiddleName = Equals(model.Addresses, null) ? string.Empty : model.AddressModel.MiddleName                    
                };
            }
            else
            {
                return new StoreAdminViewModel();
            }
        }

        /// <summary>
        /// This method is used to create the Account fot Admins
        /// </summary>
        /// <param name="model">StoreAdminViewModel model</param>
        /// <returns>AccountModel</returns>
        public static AccountModel ToAdminCreateAccountModel(StoreAdminViewModel model)
        {
            if (!Equals(model, null))
            {
                return new AccountModel()
                {
                    UserName = model.UserID,
                    AccountId = Equals(model.AccountId, null) ? 0 : int.Parse(model.AccountId.ToString()),
                    Email = model.Email,
                    RoleName = "ADMIN",
                    Description = model.Description,
                    CreateUser = HttpContext.Current.User.Identity.Name,
                    CreateDate = DateTime.Now,
                    FirstName=model.FirstName,
                    LastName=model.LastName,
                    MiddleName=model.MiddleName
                };
            }
            else
            {
                return new AccountModel();
            }
        }

        /// <summary>
        /// This method is used to create the Account fot Admins
        /// </summary>
        /// <param name="model">StoreAdminViewModel model</param>
        /// <returns>AccountModel</returns>
        public static AccountModel ToAdminUpdateAccountModel(StoreAdminViewModel model)
        {
            if (!Equals(model, null))
            {
                return new AccountModel()
                {
                    UserName = model.UserID,
                    AccountId = Equals(model.AccountId, null) ? 0 : int.Parse(model.AccountId.ToString()),
                    Email = model.Email,
                    RoleName = "ADMIN",
                    Description = model.Description,
                    UpdateUser = HttpContext.Current.User.Identity.Name,
                    UpdateDate = DateTime.Now,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    MiddleName = model.MiddleName,

                    User = new UserModel()
                    {
                        Email = model.Email,
                        Username = model.UserID
                    }
                };
            }
            else
            {
                return new AccountModel();
            }
        }
    }
}