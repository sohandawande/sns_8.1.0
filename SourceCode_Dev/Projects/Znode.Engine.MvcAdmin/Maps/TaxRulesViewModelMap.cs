﻿using Resources;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web.Mvc;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    /// <summary>
    /// Static Class for TaxRules Mapper
    /// </summary>
    public static class TaxRulesViewModelMap
    {
        /// <summary>
        /// Mapper to convert TaxRuleModel to TaxRulesViewModel    
        /// </summary>
        /// <param name="taxRuleModel">Object of TaxRuleModel</param>
        /// <returns>Returns TaxRulesViewModel</returns>
        public static TaxRulesViewModel ToViewModel(TaxRuleModel taxRuleModel)
        {
            if (Equals(taxRuleModel, null))
            {
                return null;
            }

            // PRFT Custom Code:Start
            TaxRulesViewModel taxRulesViewModel = new TaxRulesViewModel();
            taxRulesViewModel.PortalId = taxRuleModel.PortalId;
            taxRulesViewModel.TaxRuleId = taxRuleModel.TaxRuleId;
            taxRulesViewModel.TaxClassId = taxRuleModel.TaxClassId;
            taxRulesViewModel.Precedence = taxRuleModel.Precedence;
            taxRulesViewModel.SalesTax = taxRuleModel.SalesTax.ToString();
            taxRulesViewModel.VATTax = taxRuleModel.Vat;
            taxRulesViewModel.GSTTax = taxRuleModel.Gst;
            taxRulesViewModel.PSTTax = taxRuleModel.Pst;
            taxRulesViewModel.HSTTax = taxRuleModel.Hst;
            taxRulesViewModel.InclusiveTax = taxRuleModel.IsInclusive;
            taxRulesViewModel.CountryCode = taxRuleModel.CountryCode;
            taxRulesViewModel.StateCode = taxRuleModel.StateCode;
            taxRulesViewModel.CountyFips = taxRuleModel.CountyFips;
            taxRulesViewModel.TaxRuleTypeId = taxRuleModel.TaxRuleTypeId; //PRFT Custom Code

            
            // Retrieve encrypted credientials and show in the edit screen.
            if (!string.IsNullOrEmpty(taxRuleModel.Custom1))
            {
                string[] credentials = taxRuleModel.Custom1.Split('|');
                if (credentials.Length > 0 && credentials.Length == 6)
                {
                    taxRulesViewModel.AccountID = credentials[0].Split('=')[1];
                    taxRulesViewModel.License = credentials[1].Split('=')[1];
                    taxRulesViewModel.Company = credentials[2].Split('=')[1];
                    taxRulesViewModel.WebserviceURL = credentials[3].Split('=')[1];
                    taxRulesViewModel.ClientCode = credentials[4].Split('=')[1];
                    taxRulesViewModel.ClientName = credentials[5].Split('=')[1];
                }
            }
            // PRFT Custom Code:End

            //PRFT Commented Code: Start
            //return new TaxRulesViewModel()
            //{
            //    PortalId = taxRuleModel.PortalId,
            //    TaxRuleId = taxRuleModel.TaxRuleId,
            //    TaxClassId = taxRuleModel.TaxClassId,
            //    Precedence = taxRuleModel.Precedence,
            //    SalesTax = taxRuleModel.SalesTax.ToString(),
            //    VATTax = taxRuleModel.Vat,
            //    GSTTax = taxRuleModel.Gst,
            //    PSTTax = taxRuleModel.Pst,
            //    HSTTax = taxRuleModel.Hst,
            //    InclusiveTax = taxRuleModel.IsInclusive,
            //    CountryCode = taxRuleModel.CountryCode,
            //    StateCode = taxRuleModel.StateCode,
            //    CountyFips = taxRuleModel.CountyFips,                
            //};
            //PRFT Commented Code: End

            return taxRulesViewModel;

        }

        /// <summary>
        /// Mapper to convert TaxRulesViewModel to TaxRuleModel  
        /// </summary>
        /// <param name="taxRulesViewModel">Object of TaxRuleViewModel</param>
        /// <returns>Returns TaxRuleModel</returns>
        public static TaxRuleModel ToModel(TaxRulesViewModel taxRulesViewModel)
        {
            if (Equals(taxRulesViewModel, null))
            {
                return null;
            }

            return new TaxRuleModel()
            {
                TaxRuleId = taxRulesViewModel.TaxRuleId,
                Precedence = taxRulesViewModel.Precedence,
                SalesTax = Convert.ToDecimal(taxRulesViewModel.SalesTax),
                Vat = taxRulesViewModel.VATTax,
                Gst = taxRulesViewModel.GSTTax,
                Pst = taxRulesViewModel.PSTTax,
                Hst = taxRulesViewModel.HSTTax,
                IsInclusive = taxRulesViewModel.InclusiveTax,
                CountryCode = taxRulesViewModel.CountryCode.Equals("0") ? null : taxRulesViewModel.CountryCode,
                StateCode = taxRulesViewModel.StateCode.Equals("0") ? null : taxRulesViewModel.StateCode,
                CountyFips = taxRulesViewModel.CountyFips.Equals("0") ? null : taxRulesViewModel.CountyFips,
                TaxRuleTypeId = taxRulesViewModel.TaxTypeId,
                TaxClassId = taxRulesViewModel.TaxClassId,
                PortalId = taxRulesViewModel.PortalId,
                Custom1=taxRulesViewModel.Custom1,//PRFT Custom Code

            };
        }

        /// <summary>
        /// Mapper to convert TaxRuleListModel to TaxRuleListViewModel  
        /// </summary>
        /// <param name="model">TaxRuleListModel</param>
        /// <param name="pageIndex">pageIndex</param>
        /// <param name="recordPerPage">recordPerPage</param>
        /// <returns>Returns TaxRuleListViewModel</returns>
        public static TaxRuleListViewModel ToListViewModel(TaxRuleListModel model, int? pageIndex, int? recordPerPage)
        {
            if (!Equals(model, null))
            {
                var viewModel = new TaxRuleListViewModel()
                {
                    TaxRules = model.TaxRules.ToList().Select(
                    x => new TaxRulesViewModel()
                    {
                        Precedence = x.Precedence,
                        SalesTax = String.Concat(Convert.ToString(x.SalesTax), "%"),
                        InclusiveTax = x.IsInclusive,
                        CountryCode = x.CountryCode,
                        StateCode = x.StateCode,
                        CountyFips = x.CountyFips,
                        VATTax = x.Vat,
                        GSTTax = x.Gst,
                        HSTTax = x.Hst,
                        PSTTax = x.Pst,
                        TaxRuleId = x.TaxRuleId,
                        CheckIsInclusive = x.IsInclusive ? @ZnodeResources.TextYes : @ZnodeResources.TextNo,
                    }).ToList()
                };
                viewModel.RecordPerPage = recordPerPage ?? 10;
                viewModel.Page = pageIndex ?? 1;
                viewModel.TotalResults = Convert.ToInt32(model.TotalResults);
                return viewModel;
            }
            return null;
        }

        /// <summary>
        /// This method returns the List of Tax Rule Type
        /// </summary>
        /// <param name="model">IEnumerable TaxRuleTypeModel model</param>
        /// <returns>Returns list of Tax Rule Type</returns>
        public static List<SelectListItem> ToListItems(IEnumerable<TaxRuleTypeModel> model)
        {
            List<SelectListItem> taxRuleType = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                taxRuleType = (from item in model
                               where item.IsActive = true
                               select new SelectListItem
                               {
                                   Text = item.Name,
                                   Value = item.TaxRuleTypeId.ToString(),                                   
                               }).ToList();
            }
            return taxRuleType;
        }

        

        /// <summary>
        /// This method returns the List of Country
        /// </summary>
        /// <param name="model">IEnumerable CountryModel model</param>
        /// <returns>Returns list of country</returns>
        public static List<SelectListItem> ToListItems(IEnumerable<CountryModel> model)
        {
            List<SelectListItem> country = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                country = (from item in model
                           select new SelectListItem
                           {
                               Text = item.Name,
                               Value = item.Code.ToString()
                           }).ToList();
            }
            return country;
        }

        /// <summary>
        /// This method returns the List of State
        /// </summary>
        /// <param name="model">IEnumerable CountryModel model</param>
        /// <returns>Returns list of state</returns>
        public static List<SelectListItem> ToListItems(IEnumerable<StateModel> model, string stateCode = "")
        {
            List<SelectListItem> state = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                state = (from item in model
                         select new SelectListItem
                         {
                             Text = item.Name,
                             Value = item.Code.ToString(),
                             Selected = item.Code == stateCode ? true : false
                         }).ToList();
            }
            return state;
        }

        /// <summary>
        /// This method returns the List of County
        /// </summary>
        /// <param name="model">IEnumerable ZipCodeModel model</param>
        /// <returns>Returns list of county</returns>
        public static List<SelectListItem> ToListItems(IEnumerable<ZipCodeModel> model, string countyCode = "")
        {
            if (!Equals(model, null))
            {
                model = model.GroupBy(x => new { x.CountyName, x.CountyFips }).Select(y => y.OrderBy(item => item.CountyName).First());
                List<SelectListItem> county = new List<SelectListItem>();
                if (!Equals(model, null))
                {
                    county = (from item in model
                              select new SelectListItem
                              {
                                  Text = item.CountyName,
                                  Value = item.CountyFips.ToString(),
                                  Selected = item.CountyFips == countyCode ? true : false
                              }).ToList();
                }
                return county;
            }
            return null;
        }

        /// <summary>
        /// Convert TaxRuleTypeListModel to TaxRuleTypeListViewModel
        /// </summary>
        /// <param name="model">TaxRuleTypeListModel model</param>
        /// <returns>Return TaxRuleTypeListViewModel</returns>
        public static TaxRuleTypeListViewModel ToListViewModel(TaxRuleTypeListModel model)
        {
            if (!Equals(model, null) && !Equals(model.TaxRuleTypes, null))
            {
                var viewModel = new TaxRuleTypeListViewModel()
                {
                    TaxRuleType = model.TaxRuleTypes.ToList().Select(
                    x => new TaxRuleTypeViewModels()
                    {
                        TaxRuleTypeId = x.TaxRuleTypeId,
                        Name = x.Name,
                        ClassName = x.ClassName,
                        IsActive = x.IsActive,
                        Description=x.Description,
                    }).ToList()
                };
                viewModel.TotalResults = Convert.ToInt32(model.TotalResults);
                viewModel.Page = Convert.ToInt32(model.PageIndex);
                viewModel.RecordPerPage = Convert.ToInt32(model.PageSize);
                viewModel.TotalPages = Convert.ToInt32(model.TotalPages);
                viewModel.TotalResults = Convert.ToInt32(model.TotalResults);
                return viewModel;
            }
            return new TaxRuleTypeListViewModel();
        }

        #region Private Region
        private static void setCredentials()
        {

        }
        #endregion
    }
}