﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    public class CustomerBasedPricingViewModelMap
    {

        /// <summary>
        /// Converts CustomerBasedPricing List model to CustomerBasedPricing list view model
        /// </summary>
        /// <param name="models">CustomerBasedPricingListModel</param>
        /// <returns>CustomerBasedPricingListViewModel</returns>
        public static CustomerBasedPricingListViewModel ToListViewModel(CustomerBasedPricingListModel models, int? pageIndex, int? recordPerPage, int? totalPages, int? totalResults)
        {
            if (!Equals(models, null) && !Equals(models.CustomerBasedPricing, null))
            {
                var viewModel = new CustomerBasedPricingListViewModel()
                {
                    CustomerBasedPricing = models.CustomerBasedPricing.ToList().Select(
                    x => new CustomerBasedPricingViewModel()
                    {
                        AccountId = x.AccountId,
                        ProductId = models.ProductId,
                        ExternalAccountNo = x.ExternalAccountNo,
                        NegotiatedPrice = Equals(x.NegotiatedPrice, null) ? string.Empty : MvcAdminUnits.GetCurrencyValue(x.NegotiatedPrice.Value),
                        CompanyName = x.CompanyName,
                        BasePrice = Equals(x.BasePrice, null) ? string.Empty : MvcAdminUnits.GetCurrencyValue(x.BasePrice),
                        FullName = x.FullName,
                        Discount = Equals(x.Discount, null) ? string.Empty : MvcAdminUnits.GetCurrencyValue(x.Discount.Value)
                    }).ToList()
                };
                viewModel.RecordPerPage = recordPerPage ?? 10;
                viewModel.Page = pageIndex ?? 1;
                viewModel.TotalPages = Convert.ToInt32(totalPages);
                viewModel.TotalResults = Convert.ToInt32(models.TotalResults);

                return viewModel;
            }
            else
            {
                return new CustomerBasedPricingListViewModel();
            }
        }

        /// <summary>
        /// To download customer pricing product list.
        /// </summary>
        /// <param name="models">CustomerBasedPricingListModel</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="recordPerPage">Total record per page</param>
        /// <param name="totalPages">Total pages</param>
        /// <param name="totalResults">Total result</param>
        /// <returns>CustomerBasedPricingProductListViewModel</returns>
        public static CustomerBasedPricingProductListViewModel ToCustomerPricingListViewModel(CustomerBasedPricingListModel models, int? pageIndex, int? recordPerPage, int? totalPages, int? totalResults)
        {
            if (!Equals(models, null) && !Equals(models.CustomerBasedPricing, null))
            {
                var viewModel = new CustomerBasedPricingProductListViewModel()
                {
                    CustomerBasedPricingProduct = models.CustomerBasedPricing.ToList().Select(
                    x => new CustomerBasedPricingProductViewModel()
                    {
                        AccountId = x.AccountId,
                        ExternalId = x.ExternalID,
                        ProductId = models.ProductId,
                        NegotiatedPrice = Equals(x.NegotiatedPrice, null) ? string.Empty : MvcAdminUnits.GetCurrencyValue(x.NegotiatedPrice.Value),
                        BasePrice = Equals(x.BasePrice, null) ? string.Empty : MvcAdminUnits.GetCurrencyValue(x.BasePrice),
                        Discount = Equals(x.Discount, null) ? string.Empty : MvcAdminUnits.GetCurrencyValue(x.Discount.Value),
                        SKU = x.SKU,
                        Name = x.Name
                    }).ToList()
                };
                viewModel.RecordPerPage = recordPerPage ?? 10;
                viewModel.Page = pageIndex ?? 1;
                viewModel.TotalPages = Convert.ToInt32(totalPages);
                viewModel.TotalResults = Convert.ToInt32(models.TotalResults);

                return viewModel;
            }
            else
            {
                return new CustomerBasedPricingProductListViewModel();
            }
        }

       
    }
}