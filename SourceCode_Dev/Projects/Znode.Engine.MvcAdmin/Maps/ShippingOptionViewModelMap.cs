﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    /// <summary>
    /// Static class for ShippingOptionViewModelMap
    /// </summary>
    public static class ShippingOptionViewModelMap
    {
        /// <summary>
        /// Convert ShippingOptionModel to ShippingOptionViewModel
        /// </summary>
        /// <param name="model">Model of type ShippingOptionModel</param>
        /// <returns>Returns ShippingOptionViewModel</returns>
        public static ShippingOptionViewModel ToViewModel(ShippingOptionModel model)
        {
            if (!Equals(model, null))
            {
                return new ShippingOptionViewModel()
                {
                    ShippingOptionId = model.ShippingId,
                    PortalId = model.PortalId,
                    ShippingCode = model.ShippingCode,
                    Description = model.Description,
                    ProfileId = model.ProfileId,
                    CountryCode = model.CountryCode,
                    HandlingCharge = HelperMethods.FormatPrice(model.HandlingCharge),
                    DisplayOrder = model.DisplayOrder,
                    IsActive = model.ActiveInd,
                    ShippingTypeId = model.ShippingTypeId,
                    ShippingType = model.ShippingType,
                    ShippingServiceCodeId = model.ShippingServiceCodeId,
                    ShippingTypeName = Equals(model.ShippingtypeName, null) ? String.Empty : HttpUtility.HtmlDecode(model.ShippingtypeName),
                    ProfileName = Equals(model.ProfileName, null) ? string.Empty : model.ProfileName,
                    CustomErrorMessage = model.CustomErrorMessage,
                    ClassName = model.ClassName,
                    UserType = model.UserType
                };
            }
            return null;
        }

        /// <summary>
        /// Convert ShippingOptionViewModel to ShippingOptionModel
        /// </summary>
        /// <param name="viewModel">Model of type ShippingOptionViewModel</param>
        /// <returns>Returns ShippingOptionModel</returns>
        public static ShippingOptionModel ToModel(ShippingOptionViewModel viewModel)
        {
            if (!Equals(viewModel, null))
            {
                string shippingCode = string.Empty;
                string description = string.Empty;
                int shippingServiceCodeId = 0;

                if (Equals(viewModel.ClassName, null))
                {
                    if (!Equals(viewModel.ShippingType, null))
                    {
                        if (Equals(viewModel.ShippingType.ClassName, MvcAdminConstants.ZnodeShippingFedEx) || Equals(viewModel.ShippingType.ClassName, MvcAdminConstants.ZnodeShippingUps))
                        {
                            shippingCode = viewModel.ShippingServiceCode.Code;
                            description = viewModel.ShippingServiceCode.Description;
                            shippingServiceCodeId = viewModel.ShippingServiceCodeId;
                        }
                        else
                        {
                            shippingCode = viewModel.ShippingCode;
                            description = viewModel.Description;
                            shippingServiceCodeId = 0;
                        }
                    }
                }
                else
                {
                    if (Equals(viewModel.ClassName, MvcAdminConstants.ZnodeShippingFedEx) || Equals(viewModel.ClassName, MvcAdminConstants.ZnodeShippingUps))
                    {
                        shippingCode = viewModel.ShippingServiceCode.Code;
                        description = viewModel.ShippingServiceCode.Description;
                        shippingServiceCodeId = viewModel.ShippingServiceCodeId;
                    }
                    else
                    {
                        shippingCode = viewModel.ShippingCode;
                        description = viewModel.Description;
                        shippingServiceCodeId = 0;
                    }
                }

                return new ShippingOptionModel()
                {
                    ShippingId = viewModel.ShippingOptionId,
                    PortalId = viewModel.PortalId,
                    ShippingCode = shippingCode,
                    Description = description,
                    ShippingServiceCodeId = shippingServiceCodeId,
                    ProfileId = viewModel.ProfileId,
                    CountryCode = Equals(viewModel.CountryCode, MvcAdminConstants.IndexZero) ? null : viewModel.CountryCode,
                    HandlingCharge = viewModel.HandlingCharge,
                    DisplayOrder = Equals(viewModel.DisplayOrder, null) ? MvcAdminConstants.DisplayOrder500 : Convert.ToInt32(viewModel.DisplayOrder),
                    ActiveInd = viewModel.IsActive,
                    ShippingTypeId = viewModel.ShippingTypeId,
                    ShippingType = viewModel.ShippingType,                    
                    CustomErrorMessage = viewModel.CustomErrorMessage,
                    ClassName = viewModel.ClassName,
                    UserType = viewModel.UserType
                };
            }
            return null;
        }

        /// <summary>
        /// Converts Shipping Option model to Shipping Option list view model
        /// </summary>
        /// <param name="models">IEnumerable ShippingOptionModel</param>
        /// <returns>Returns ShippingOptionListViewModel</returns>
        public static ShippingOptionListViewModel ToListViewModel(ShippingOptionListModel models)
        {
            if (!Equals(models, null) && !Equals(models.ShippingOptions, null))
            {
                var viewModel = new ShippingOptionListViewModel()
                {
                    ShippingOptions = models.ShippingOptions.ToList().Select(
                    x => new ShippingOptionViewModel()
                    {
                        ShippingOptionId = x.ShippingId,
                        ShippingCode = x.ShippingCode,
                        ShippingType = x.ShippingType,
                        Description = HttpUtility.HtmlDecode(x.Description),
                        ProfileId = x.ProfileId,
                        CountryCode = x.CountryCode,
                        HandlingChargeWithDollar = Equals(x.HandlingCharge, null) ? string.Empty : MvcAdminUnits.GetCurrencyValue(x.HandlingCharge),
                        DisplayOrder = x.DisplayOrder,
                        IsActive = x.ActiveInd,
                        ShippingTypeName = Equals(x.ShippingtypeName, null) ? String.Empty : HttpUtility.HtmlDecode(x.ShippingtypeName),
                        ProfileName = Equals(x.ProfileName, null) ? String.Empty : x.ProfileName,
                    }).ToList()
                };
                viewModel.TotalResults = Convert.ToInt32(models.TotalResults);
                return viewModel;
            }
            return new ShippingOptionListViewModel();
        }

        /// <summary>
        /// Convert IEnumerable<ShippingTypeModel> to List<SelectListItem>
        /// </summary>
        /// <param name="model">to convert to list item type</param>
        /// <returns>Returns List<SelectListItem></returns>
        public static List<SelectListItem> ToListItems(IEnumerable<ShippingTypeModel> model)
        {
            List<SelectListItem> shippingTypeItems = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                shippingTypeItems = (from item in model
                                     select new SelectListItem
                                     {
                                         Text = item.Name,
                                         Value = item.ClassName,
                                     }).ToList();
            }
            return shippingTypeItems;
        }

        /// <summary>
        /// Convert IEnumerable<ProfileModel> to List<SelectListItem>
        /// </summary>
        /// <param name="model">to convert to list item type</param>
        /// <returns>Returns List<SelectListItem></returns>
        public static List<SelectListItem> ToListItems(IEnumerable<ProfileModel> model)
        {
            List<SelectListItem> profileItems = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                profileItems = (from item in model
                                select new SelectListItem
                                {
                                    Text = item.Name,
                                    Value = item.ProfileId.ToString(),
                                }).ToList();
                profileItems.Insert(0, new SelectListItem { Text = "All Profiles", Value = "-1" });
            }
            return profileItems;
        }

        /// <summary>
        /// Convert IEnumerable<ProfileModel> to List<SelectListItem>
        /// </summary>
        /// <param name="model">to convert to list item type</param>
        /// <returns>Returns List<SelectListItem></returns>
        public static List<SelectListItem> ToListItems(IEnumerable<CountryModel> model)
        {
            List<SelectListItem> countryItems = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                countryItems = (from item in model
                                select new SelectListItem
                                {
                                    Text = item.Name,
                                    Value = item.Code.ToString(),
                                }).ToList();
            }
            return countryItems;
        }

        /// <summary>
        /// Convert IEnumerable<ShippingServiceCodeModel> to List<SelectListItem>
        /// </summary>
        /// <param name="model">to convert to list item type</param>
        /// <returns>Returns List<SelectListItem></returns>
        public static List<SelectListItem> ToListItems(IEnumerable<ShippingServiceCodeModel> model)
        {
            List<SelectListItem> shippingServiceCode = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                shippingServiceCode = (from item in model
                                       select new SelectListItem
                                       {
                                           Text = HttpUtility.HtmlDecode(item.Description),
                                           Value = item.ShippingServiceCodeId.ToString(),
                                       }).ToList();
            }
            return shippingServiceCode;
        }

        /// <summary>
        /// Convert IEnumerable<ShippingOptionModel> to List<SelectListItem>
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Returns List<SelectListItem></returns>
        public static List<SelectListItem> ToListItems(IEnumerable<ShippingOptionModel> model)
        {
            List<SelectListItem> shippingOption = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                shippingOption = (from item in model
                                  select new SelectListItem
                                  {
                                      Text = HttpUtility.HtmlDecode(item.Description),
                                      Value = item.ShippingId.ToString(),
                                  }).ToList();
            }
            return shippingOption;
        }

        /// <summary>
        /// Convert ShippingServiceCodeModel to ShippingServiceCodeViewModel
        /// </summary>
        /// <param name="model">ShippingServiceCodeModel</param>
        /// <returns>Returns ShippingServiceCodeModel</returns>
        public static ShippingServiceCodeViewModel ToViewModel(ShippingServiceCodeModel model)
        {
            if (!Equals(model, null))
            {
                return new ShippingServiceCodeViewModel()
                {
                    Code = model.Code,
                    Description = model.Description,
                };
            }
            else
            {
                return null;
            }
        }
    }
}