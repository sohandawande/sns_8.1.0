﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    public class PortalCountriesViewModelMap
    {
        public static PortalCountriesViewModel ToViewModel(PortalCountryModel model)
        {
            if (Equals(model, null))
            {
                return null;
            }
            return new PortalCountriesViewModel()
            {
                CountryCode = model.CountryCode,
                IsBillingActive = model.IsBillingActive,
                IsShippingActive = model.IsShippingActive,
                PortalCountryId = model.PortalCountryId,
                PortalId = model.PortalId,
                CountryName = model.CountryName
            };

        }

        public static PortalCountryModel ToModel(PortalCountriesViewModel model)
        {
            if (Equals(model, null))
            {
                return null;
            }
            return new PortalCountryModel()
            {
                CountryCode = model.CountryCode,
                IsBillingActive = model.IsBillingActive,
                IsShippingActive = model.IsShippingActive,
                PortalCountryId = model.PortalCountryId,
                PortalId = model.PortalId,
                CountryName = model.CountryName
            };
        }

        public static PortalCountriesListViewModel ToListViewModel(PortalCountryListModel model)
        {
            var viewModel = new PortalCountriesListViewModel();

            if (!Equals(model.PortalCountries, null))
            {
                viewModel.PortalCountryList = model.PortalCountries.Select(
                x => new PortalCountriesViewModel()
                {
                    CountryCode = x.CountryCode,
                    IsBillingActive = x.IsBillingActive,
                    IsShippingActive = x.IsShippingActive,
                    PortalCountryId = x.PortalCountryId,
                    PortalId = x.PortalId,
                    CountryName = x.CountryName
                }).ToList();
            }
            viewModel.TotalResults = Convert.ToInt32(model.TotalResults);
            viewModel.Page = Convert.ToInt32(model.PageIndex);
            viewModel.RecordPerPage = Convert.ToInt32(model.PageSize);
            viewModel.TotalPages = Convert.ToInt32(model.TotalPages);
            viewModel.TotalResults = Convert.ToInt32(model.TotalResults);
            return viewModel;
        }

    }
}