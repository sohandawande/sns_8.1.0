﻿using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    public class ReasonForReturnViewModelMap
    {
        /// <summary>
        /// Converting ReasonForReturn ViewModel to ReasonForReturn Model   
        /// </summary>
        /// <param name="model"></param>
        /// <returns>ReasonReturnModel</returns>
        public static ReasonForReturnModel ToModel(ReasonForReturnViewModel model)
        {
            if (Equals(model,null))
            {
                return null;
            }

            return new ReasonForReturnModel()
            {
                ReasonForReturnId = model.ReasonForReturnId,
                Name = model.Reason,
                IsEnabled = model.IsEnabled
            };
        }

        /// <summary>
        /// Converting ReasonForReturn Model to ReasonForReturn View Model   
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns>ReasonReturnViewModel</returns>
        public static ReasonForReturnViewModel ToViewModel(ReasonForReturnModel viewModel)
        {
            if (Equals(viewModel, null))
            {
                return null;
            }

            return new ReasonForReturnViewModel()
            {
                ReasonForReturnId = viewModel.ReasonForReturnId,
                Reason = viewModel.Name,
                IsEnabled = viewModel.IsEnabled
            };
        }
    }
}