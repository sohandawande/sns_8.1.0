﻿using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    public class ZnodeDeleteViewModelMap
    {
        #region Public Methods

        /// <summary>
        /// Maps the ZnodeDelete model to ZnodeDeleteView model.
        /// </summary>
        /// <param name="model">Model of type ZnodeDeleteModel</param>
        /// <returns>Returns the mapped ZnodeDelete model to ZnodeDeleteView model.</returns>
        public static ZnodeDeleteViewModel ToViewModel(ZnodeDeleteModel model)
        {
            if (Equals(model, null))
            {
                return null;
            }
            return new ZnodeDeleteViewModel()
            {
                IsDeleted = model.IsDeleted,
                ErrorMessage = model.ErrorMessage
            };
        }

        #endregion
    }
}