﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;
using System.Web.Mvc;
using Znode.Engine.MvcAdmin.Helpers;
namespace Znode.Engine.MvcAdmin.Maps
{
    public class VendorProductViewModelMap
    {
        /// <summary>
        /// Convert the Reject Product Model to Vendor Product List Model.
        /// </summary>
        /// <param name="model">RejectProductModel model</param>
        /// <returns>Return Rejected Product Detail in VendorProductListViewModel Model </returns>
        public static VendorProductListViewModel ToViewModel(RejectProductModel model)
        {
            if (!Equals(model, null))
            {
                var viewModel = new VendorProductListViewModel()
                {
                    ProductName = model.ProductName,
                    VendorName = model.VendorName,
                    RejectionResons = ToListItem(model.RejectionResons),
                };
                return viewModel;
            }
            return new VendorProductListViewModel();
        }

        /// <summary>
        /// Convert Rejected reasons from Dictonary to List Item.
        /// </summary>
        /// <param name="reasons">Dictionary<string, string> reasons</param>
        /// <returns>Return Rejected Reason List in List<SelectListItem> format</returns>
        private static List<SelectListItem> ToListItem(Dictionary<string, string> reasons)
        {
            List<SelectListItem> lstItems = new List<SelectListItem>();
            if (!Equals(reasons, null))
            {
                lstItems = (from item in reasons
                            select new SelectListItem()
                            {
                                Text = item.Key,
                                Value = item.Value,
                            }).ToList();
            }
            return lstItems;
        }

        /// <summary>
        /// Convert Vendor Product List View to Reject Product Model.
        /// </summary>
        /// <param name="model">VendorProductListViewModel model</param>
        /// <returns>Return Rejected Product reason in RejectProductModel format. </returns>
        public static RejectProductModel ToModel(VendorProductListViewModel model)
        {
            RejectProductModel rejectModel = new RejectProductModel();
            rejectModel.SelectedProductIds = model.SelectedProductIds;
            rejectModel.DetailReason = model.DetailReason;
            rejectModel.SelectedRejectionReason = model.SelectedRejectionReason;
            rejectModel.UserName = model.UserName;
            return rejectModel;
        }

        /// <summary>
        /// Convert Product Review History List Model to Product Review View Model.
        /// </summary>
        /// <param name="model">IEnumerable<ProductReviewHistoryModel> model</param>
        /// <param name="totalResult">int? totalResult</param>
        /// <returns>Return Product Review History List in ProductReviewHistoryListViewModel format.</returns>
        public static ProductReviewHistoryListViewModel ToProductReviewList(IEnumerable<ProductReviewHistoryModel> model, int? totalResult = 0)
        {
            if (!Equals(model, null))
            {
                var viewModel = new ProductReviewHistoryListViewModel()
                {
                    ProductReviews = model.ToList().Select(
                    x => new ProductReviewHistoryViewModel()
                    {
                        ProductReviewHistoryID = x.ProductReviewHistoryID,
                        ProductID = x.ProductID,
                        VendorID = x.VendorID,
                        Status = x.Status,
                        Reason = x.Reason,
                        Description = x.Description,
                        Username = x.Username,
                        LogDate = (!Equals(x.LogDate,null)) ? HelperMethods.ViewDateFormat(x.LogDate) : string.Empty,
                        NotificationDate = (x.NotificationDate.HasValue) ? HelperMethods.ViewDateFormat(x.NotificationDate.Value) : string.Empty,
                    }).ToList()
                };
                viewModel.TotalResults = Convert.ToInt32(totalResult);
                return viewModel;
            }
            return new ProductReviewHistoryListViewModel();
        }

        /// <summary>
        /// Convert Product Review History Model to Product Review View Model.
        /// </summary>
        /// <param name="model">ProductReviewHistoryModel model</param>
        /// <returns>Return Product Review History Details in ProductReviewHistoryListViewModel format.</returns>
        public static ProductReviewHistoryViewModel ToProductReviewView(ProductReviewHistoryModel model)
        {
            ProductReviewHistoryViewModel viewModel = new ProductReviewHistoryViewModel();
            if (!Equals(model, null))
            {
                viewModel.ProductReviewHistoryID = model.ProductReviewHistoryID;
                viewModel.ProductID = model.ProductID;
                viewModel.VendorID = model.VendorID;
                viewModel.Status = model.Status;
                viewModel.Reason = (!string.IsNullOrEmpty(model.Reason)) ? HttpUtility.HtmlDecode(model.Reason) : "N/A";
                viewModel.Description = (!string.IsNullOrEmpty(model.Description)) ? HttpUtility.HtmlDecode(model.Description) : "N/A";
                viewModel.Username = (!string.IsNullOrEmpty(model.Username)) ?model.Username : "N/A";
                viewModel.LogDate = model.LogDate.ToShortDateString();
                viewModel.NotificationDate = model.NotificationDate.ToString();
                viewModel.VendorName = (!string.IsNullOrEmpty(model.VendorName))?model.VendorName:string.Empty;
                viewModel.ProductName = (!string.IsNullOrEmpty(model.ProductName)) ? model.ProductName : string.Empty;
            }
            return viewModel;
        }

        /// <summary>
        /// Convert Product Model to Vendor Product Marketing View Model.
        /// </summary>
        /// <param name="model">ProductModel model</param>
        /// <returns>Return Product Marketing Details in VendorProductMarketingViewModel format.</returns>
        public static VendorProductMarketingViewModel ToMarketingViewModel(ProductModel model)
        {
            VendorProductMarketingViewModel viewModel = new VendorProductMarketingViewModel();
            if (!Equals(model, null))
            {
                viewModel.DisplayOrder=model.DisplayOrder;
                viewModel.HomepageSpecial = model.IsHomepageSpecial;
                viewModel.FeaturedInd = model.IsFeatured;
                viewModel.NewProductInd = model.IsNewProduct;
                viewModel.SeoTitle = model.SeoTitle;
                viewModel.SeoPageUrl = model.SeoUrl;
                viewModel.SeoDescription = model.SeoDescription;
                viewModel.SeoKeywords = model.SeoKeywords;
                viewModel.ProductName = model.Name;
                viewModel.ProductId = model.ProductId;
            }
            return viewModel;
        }
        /// <summary>
        /// Convert Product Marketing View Model to Product Model.
        /// </summary>
        /// <param name="viewModel">VendorProductMarketingViewModel viewModel</param>
        /// <returns>Return Product Details in ProductModel format.</returns>
        public static ProductModel ToProductModel(VendorProductMarketingViewModel viewModel)
        {
            ProductModel model = new ProductModel();
            model.DisplayOrder = viewModel.DisplayOrder;
            model.IsHomepageSpecial = viewModel.HomepageSpecial;
            model.IsFeatured = viewModel.FeaturedInd;
            model.IsNewProduct = viewModel.NewProductInd;
            model.SeoTitle = viewModel.SeoTitle;
            model.SeoUrl = viewModel.SeoPageUrl;
            model.SeoDescription = viewModel.SeoDescription;
            model.SeoKeywords = viewModel.SeoKeywords;
            model.ProductId = viewModel.ProductId;
            model.UserName = HttpContext.Current.User.Identity.Name;
            return model;
        }

        /// <summary>
        /// Convert the Product Model to Product Preview View Model.
        /// </summary>
        /// <param name="model">ProductModel model</param>
        /// <returns>Return Product Preview in  VendorProductPreviewViewModel format.</returns>
        public static VendorProductPreviewViewModel ToViewModel(ProductModel model)
        {
            var viewModel = new VendorProductPreviewViewModel()
            {
                ProductModel=ProductViewModelMap.ToViewModel(model),
                Images = ToImageViewModel(model.Images),

            };
            if (model.AddOns != null && model.AddOns.Count > 0)
            {
                viewModel.AddOns = model.AddOns.Select(ToViewModel).Where(x => x != null).ToList();
            }
            if (model.YouMayAlsoLike.Any())
            {
                viewModel.YMALProductsIds = string.Join(",", model.YouMayAlsoLike.Select(ymal => ymal.RelatedProductId.ToString()));
            }
            if (model.FrequentlyBoughtTogether.Any())
            {
                viewModel.FBTProductsIds = string.Join(",", model.FrequentlyBoughtTogether.Select(ymal => ymal.RelatedProductId.ToString()));
            }
            return viewModel;
        }
        private static List<ProductAlternateImageViewModel> ToImageViewModel(Collection<ImageModel> model)
        {
            ImageHelper imageHelper = new ImageHelper();
            var imageViewModel = model.Select(x => new ProductAlternateImageViewModel()
            {

                AlternateThumbnailImageFile = x.AlternateThumbnailImageFile,
                DisplayOrder = x.DisplayOrder,
                ImageAltText = x.ImageAltTag,
                ImageFile = x.ImageFile,
                ImageLargePath = imageHelper.GetImageHttpPathLarge(x.ImageFile),  
                ImageMediumPath = imageHelper.GetImageHttpPathMedium(x.ImageFile), 
                ImageSmallPath = HelperMethods.GetImageRelativePath(imageHelper.GetImageHttpPathSmall(x.ImageFile)),
                ImageSmallThumbnailPath =HelperMethods.GetImageRelativePath(imageHelper.GetImageHttpPathSmallThumbnail(x.ImageFile)), 
                ImageThumbnailPath = HelperMethods.GetImagePath(x.ImageThumbnailPath),
                ActiveInd = x.IsActive,
                Name = x.Name,
                ProductId = x.ProductId,
                ProductImageId = x.ProductImageId,
                ProductImageTypeID = x.ProductImageTypeId,
                ReviewStateID = x.ReviewStateId,
                ShowOnCategoryPage = x.ShowOnCategoryPage
            }).ToList();

            return imageViewModel;
        }

        /// <summary>
        /// Convert AddOnViewModel to AddOnModel
        /// </summary>
        /// <param name="model">AddOnModel model</param>
        /// <returns>Returns the view model of AddOn Value</returns>
        private static VendorAddOnViewModel ToViewModel(AddOnModel model)
        {
            if (Equals(model, null))
            {
                return null;
            }
            return new VendorAddOnViewModel()
            {
                Name = model.Name,
                AddOnTitle = model.Title,
                Description = model.Description,
                InStockMessage = model.InStockMessage,
                OutOfStockMessage = model.OutOfStockMessage,
                BackOrderMessage = model.BackOrderMessage,
                DisplayOrder = model.DisplayOrder,
                IsOptional = model.IsOptional,
                AllowBackOrder = model.AllowBackOrder,
                TrackInventory = model.TrackInventory,
                PromptMessage = model.PromptMessage,
                DisplayType = model.DisplayType,
                AddOnId = model.AddOnId,
                IsAssociatedAddOnValue = model.IsAssociatedAddOnValue,
                AddOnValues = model.AddOnValues.Select(ToAddonValuesViewModel).OrderBy(x => x.DisplayOrder).ToList(),
            };
        }

        /// <summary>
        /// Convert Add On Value model to Add On Value View Model.
        /// </summary>
        /// <param name="model">AddOnValueModel model</param>
        /// <returns>Return Add on Value details in AddOnValueViewModel format</returns>
        public static AddOnValueViewModel ToAddonValuesViewModel(AddOnValueModel model)
        {
            return new AddOnValueViewModel()
            {
                AddOnValueId = model.AddOnValueId,
                Name = model.Name,                
                RetailPrice = model.RetailPrice,
                SalePrice = model.SalePrice,              
                SKU = model.SKU,
                QuantityOnHand = model.QuantityOnHand,
                DisplayOrder = model.DisplayOrder
            };
        }

        /// <summary>
        /// Convert  List of Product associated Category in to the list of Product Category View Model.
        /// </summary>
        /// <param name="model">IEnumerable<ProductCategoryModel> model</param>
        /// <returns>Return List of Product associated Category</returns>
        public static List<ProductCategoryViewModel> ToListViewModel(IEnumerable<ProductCategoryModel> model)
        {
            List<ProductCategoryViewModel> associateCategory = new List<ProductCategoryViewModel>();
            if (!Equals(model, null) && model.Count() > 0)
            {
                foreach (ProductCategoryModel item in model)
                {
                    associateCategory.Add(ProductCategoryViewModelMap.ToViewModel(item));
                }
            }
            return associateCategory;
        }

        /// <summary>
        /// Convert Category Node list to Category Node List View model.
        /// </summary>
        /// <param name="model">IEnumerable<CategoryNodeModel> model</param>
        /// <returns>Return Category Node List.</returns>
        public static CategoryNodeListViewModel ToListViewModel(IEnumerable<CategoryNodeModel> model)
        {

            if (!Equals(model, null))
            {
                var viewModel = new CategoryNodeListViewModel()
                {
                    CategoryNodeList = model.ToList().Select(
                    x => new CategoryNodeViewModel()
                    { 
                        CatalogId = x.CatalogId,
                        CategoryId = x.CategoryId,
                        CategoryNodeId = x.CategoryNodeId,
                        BeginDate = x.BeginDate,
                        CssId = x.CssId,
                        DisplayOrder = x.DisplayOrder,
                        EndDate = x.EndDate,
                        IsActive = x.IsActive,
                        MasterPageId = !Equals(x.MasterPageId, null) ? x.MasterPageId : 0,
                        ParentCategoryNodeId = !Equals(x.ParentCategoryNodeId, null) ? x.ParentCategoryNodeId : 0,
                        ThemeId = !Equals(x.ThemeId, null) ? x.ThemeId : 0,
                        CatalogName = (!Equals(x.Catalog, null)) ? x.Catalog.Name : string.Empty,
                        ErrorMessage = (!Equals(x.ErrorMessage, null)) ? x.ErrorMessage : string.Empty,
                        Name=x.Name,
                        Title=x.Title,
                        VisibleInd=x.VisibleInd,
                    }).ToList()
                };
                
                return viewModel;
            }
            return new CategoryNodeListViewModel();


        }
    }
}