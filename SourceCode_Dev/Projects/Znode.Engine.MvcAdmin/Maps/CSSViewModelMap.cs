﻿using System;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    /// <summary>
    /// Maps css models to css view model which is used to show on views.
    /// </summary>
    public  static class CSSViewModelMap
    {
        /// <summary>
        /// Converts list of css model to list of css view model.
        /// </summary>
        /// <param name="listModel">CSSListModel listModel</param>
        /// <returns>CSSListViewModel</returns>
        public static CSSListViewModel ToListViewModel(CSSListModel listModel)
        {
            CSSListViewModel viewModel = new CSSListViewModel();

            if (!Equals(listModel, null) && !Equals(listModel.CSSs, null))
            {
                viewModel.CSSList = listModel.CSSs.Select(
                x => new CSSViewModel()
                {    
                    CSSId = x.CSSID,
                    ThemeId = x.ThemeID,
                    Name = x.Name,
                    ThemeName = x.Theme.Name,

                }).ToList();
            }
            viewModel.Page = Convert.ToInt32(listModel.PageIndex);
            viewModel.RecordPerPage = Convert.ToInt32(listModel.PageSize);
            viewModel.TotalPages = Convert.ToInt32(listModel.TotalPages);
            viewModel.TotalResults = Convert.ToInt32(listModel.TotalResults);
            return viewModel;
        }

        /// <summary>
        /// Map CSSViewModel To CSSModel
        /// </summary>
        /// <param name="viewModel">View Model Of CSS</param>
        /// <returns>Model Of CSS</returns>
        public static CSSModel ToModel(CSSViewModel viewModel)
        {
            if (!Equals(viewModel, null))
            {
                return new CSSModel()
                {
                    CSSID = viewModel.CSSId,
                    ThemeID = viewModel.ThemeId,
                    Name = viewModel.Name
                };
            }
            else
            {
                return new CSSModel();
            }
        }

        /// <summary>
        /// Map CSSModel To CSSViewModel
        /// </summary>
        /// <param name="model">Model Of CSS</param>
        /// <returns>View Model Of CSS</returns>
        public static CSSViewModel ToViewModel(CSSModel model)
        {
            if (!Equals(model, null))
            {
                return new CSSViewModel()
                {
                    CSSId = model.CSSID,
                    ThemeId = model.ThemeID,
                    Name = model.Name
                };
            }
            else
            {
                return new CSSViewModel();
            }
        }
    }
}