﻿using System;
using System.Collections.Generic;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    public static class ProductTypeViewModelMap
    {
        /// <summary>
        /// Converts ProductTypeModel to ProductTypeViewModel.
        /// </summary>
        /// <param name="model">ProductTypeModel</param>
        /// <returns>Returns ProductTypeViewModel</returns>
        public static ProductTypeViewModel ToViewModel(ProductTypeModel model)
        {
            return new ProductTypeViewModel()
            {
                ProductTypeId = model.ProductTypeId,
                Name = model.Name,
                Description = model.Description,
                DisplayOrder = model.DisplayOrder,
                IsFranchisable = model.IsFranchiseable,
                IsGiftCard = Equals(model.IsGiftCard, string.Empty) ? false : model.IsGiftCard
            };
        }

        /// <summary>
        /// Converts ProductTypeViewModel to ProductTypeModel.
        /// </summary>
        /// <param name="viewModel">ProductTypeViewModel</param>
        /// <returns>Returns ProductTypeModel</returns>
        public static ProductTypeModel ToModel(ProductTypeViewModel viewModel)
        {
            if (viewModel == null)
            {
                return null;
            }
            return new ProductTypeModel()
            {
                ProductTypeId = viewModel.ProductTypeId,
                Name = viewModel.Name,
                IsFranchiseable = viewModel.IsFranchisable,
                Description = Equals(viewModel.Description,null) ? string.Empty : viewModel.Description,
                DisplayOrder = viewModel.DisplayOrder
            };
        }

        /// <summary>
        /// Converts IEnumerable ProductTypeModel to ProductTypeListViewModel.
        /// </summary>
        /// <param name="models">IEnumerable ProductTypeModel</param>
        /// <returns>Returns ProductTypeListViewModel</returns>
        public static ProductTypeListViewModel ToListViewModel(ProductTypeListModel models)
        {
            var viewModel = new ProductTypeListViewModel()
            {
                ProductTypes = models.ProductTypes.ToList().Select(
                x => new ProductTypeViewModel()
                {
                    ProductTypeId = x.ProductTypeId,
                    Name = x.Name,
                    Description = x.Description,
                    DisplayOrder = x.DisplayOrder,
                    IsFranchisable = x.IsFranchiseable
                }
                ).ToList()
            };
            viewModel.TotalResults = Convert.ToInt32(models.TotalResults);

            return viewModel;
        }
    }
}