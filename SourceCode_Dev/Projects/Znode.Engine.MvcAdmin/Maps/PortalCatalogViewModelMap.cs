﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.ViewModels;
using Znode.Engine.MvcDemo.Maps;

namespace Znode.Engine.MvcAdmin.Maps
{
    public class PortalCatalogViewModelMap
    {
        /// <summary>
        /// Converts portalcatalog model to portal view model.
        /// </summary>
        /// <param name="model">Portal catalog model to be converted.</param>
        /// <returns>Converted portal catalog view model.</returns>
        public static PortalCatalogViewModel ToViewModel(PortalCatalogModel model)
        {
            if (Equals(model, null))
            {
                return null;
            }

            return new PortalCatalogViewModel()
            {
                Catalog = CatalogViewModelMap.ToViewModel(model.Catalog),
                CatalogId = model.CatalogId,
                CssId = model.CssId,
                LocaleId = model.LocaleId,
                Portal = PortalViewModelMap.ToViewModel(model.Portal),
                ThemeId = model.ThemeId,
                PortalId = model.PortalId,
                PortalCatalogId = model.PortalCatalogId,
                CSSName = model.CSSName,
                ThemeName = model.ThemeName,
                CatalogName = model.CatalogName
            };
        }

        /// <summary>
        /// Converts list of portalcatalog model to portalcatalog list view model.
        /// </summary>
        ///<param name="model">Portal Catalog List Model that will be converted to list view model.</param>
        ///<param name="pageIndex">Page Index of the list view model.</param>
        ///<param name="recordPerPage">Record per page of the list view model.</param>
        /// <returns>PortalListViewModel</returns>
        public static PortalCatalogListViewModel ToListViewModel(PortalCatalogListModel model, int? pageIndex, int? recordPerPage)
        {
            if (!Equals(model.PortalCatalogs, null))
            {
                PortalCatalogListViewModel viewModel = new PortalCatalogListViewModel();
                viewModel.PortalCatalogs = model.PortalCatalogs.Select(
                x => new PortalCatalogViewModel()
                {
                    Catalog = CatalogViewModelMap.ToViewModel(x.Catalog),
                    CatalogId = x.CatalogId,
                    CssId = x.CssId,
                    LocaleId = x.LocaleId,
                    Portal = PortalViewModelMap.ToViewModel(x.Portal),
                    ThemeId = x.ThemeId,
                    PortalId = x.PortalId,
                    PortalCatalogId = x.PortalCatalogId,
                    CSSName = x.CSSName,
                    ThemeName = x.ThemeName,
                    CatalogName = x.CatalogName
                }).ToList();
                viewModel.RecordPerPage = recordPerPage ?? MvcAdminConstants.DefaultPageSize;
                viewModel.Page = pageIndex ?? MvcAdminConstants.DefaultPageNumber;
                viewModel.TotalResults = Convert.ToInt32(model.TotalResults);
                return viewModel;
            }
            else
            {
                return new PortalCatalogListViewModel();
            }
        }

        /// <summary>
        /// Converts PotalCatalog view model to PortalCatalog model.
        /// </summary>
        /// <param name="portalViewModel">PortalCatalog View model.</param>
        /// <returns>PortalCatalog Model.</returns>
        public static PortalCatalogModel ToModel(PortalCatalogViewModel portalCatalogViewModel)
        {
            var portalCatalogModel = new PortalCatalogModel()
            {
                Catalog = CatalogViewModelMap.ToModel(portalCatalogViewModel.Catalog),
                CatalogId = portalCatalogViewModel.CatalogId,
                CssId = portalCatalogViewModel.CssId,
                LocaleId = portalCatalogViewModel.LocaleId,
                Portal = PortalViewModelMap.ToModel(portalCatalogViewModel.Portal),
                ThemeId = portalCatalogViewModel.ThemeId,
                PortalId = portalCatalogViewModel.PortalId,
                PortalCatalogId = portalCatalogViewModel.PortalCatalogId,
                CSSName = portalCatalogViewModel.CSSName,
                ThemeName = portalCatalogViewModel.ThemeName
            };
            return portalCatalogModel;
        }

        /// <summary>
        /// Creates a SelectListItem List for Portals
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static List<SelectListItem> ToSelectListItems(IEnumerable<CatalogViewModel> model)
        {
            List<SelectListItem> selectItems = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                selectItems = (from item in model
                               select new SelectListItem
                               {
                                   Text = item.Name,
                                   Value = item.CatalogId.ToString(),
                               }).ToList();
            }
            return selectItems;
        }
    }
}