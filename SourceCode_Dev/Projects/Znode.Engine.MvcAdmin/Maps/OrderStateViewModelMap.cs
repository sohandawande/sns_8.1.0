﻿using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    public class OrderStateViewModelMap
    {
        /// <summary>
        /// Converts list of order state model to orderstatelist view model.
        /// </summary>
        /// <param name="models">Model of type orderstatelistmodel</param>
        /// <returns>OrderStateListViewModel</returns>
        public static OrderStateListViewModel ToListViewModel(OrderStateListModel model)
        {
            var viewModel = new OrderStateListViewModel();

            if (!Equals(model.OrderStates, null))
            {
                viewModel.OrderStateList = model.OrderStates.Select(
                x => new OrderStateViewModel()
                {
                    OrderStateName = x.OrderStateName,
                    Description = x.Description,
                    OrderStateId = x.OrderStateID
                }).ToList();
            }            
            return viewModel;
        }
    }
}