﻿using System;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    /// <summary>
    /// Static Class for ShippingType Mapper
    /// </summary>
    public static class ShippingTypeViewModelMap
    {
        /// <summary>
        /// Convert ShippingTypeListModel to ShippingTypeListViewModel
        /// </summary>
        /// <param name="model">ShippingTypeListModel model</param>
        /// <returns>Return ShippingTypeListViewModel</returns>
        public static ShippingTypeListViewModel ToListViewModel(ShippingTypeListModel model)
        {
            if (!Equals(model, null) && !Equals(model.ShippingTypes, null))
            {
                var viewModel = new ShippingTypeListViewModel()
                {
                    ShippingType = model.ShippingTypes.ToList().Select(
                    x => new ShippingTypeViewModel()
                    {
                        ShippingTypeId=x.ShippingTypeId,
                        Name = x.Name,
                        ClassName = x.ClassName,
                        IsActive = x.IsActive,
                        Description=x.Description,
                    }).ToList()
                };
                viewModel.TotalResults = Convert.ToInt32(model.TotalResults);
                viewModel.Page = Convert.ToInt32(model.PageIndex);
                viewModel.RecordPerPage = Convert.ToInt32(model.PageSize);
                viewModel.TotalPages = Convert.ToInt32(model.TotalPages);
                viewModel.TotalResults = Convert.ToInt32(model.TotalResults);
                return viewModel;
            }
            else
            {
                return new ShippingTypeListViewModel();
            }
        }
    }
}