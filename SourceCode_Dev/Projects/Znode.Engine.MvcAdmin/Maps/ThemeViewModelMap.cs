﻿using System;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    /// <summary>
    /// Maps theme models to theme view model which is used to show on views.
    /// </summary>
    public static class ThemeViewModelMap
    {
        /// <summary>
        /// Converts list of portal model to portallist view model.
        /// </summary>
        /// <param name="listModel">ThemeListModel listModel</param>
        /// <returns>ThemeListViewModel</returns>
        public static ThemeListViewModel ToListViewModel(ThemeListModel listModel)
        {
            ThemeListViewModel viewModel = new ThemeListViewModel();

            if (!Equals(listModel, null) && !Equals(listModel.Themes, null))
            {
                viewModel.ThemeList = listModel.Themes.Select(
                x => new ThemeViewModel()
                {
                    ThemeId = x.ThemeID,
                    Name = x.Name
                }).ToList();
            }
            viewModel.Page = Convert.ToInt32(listModel.PageIndex);
            viewModel.RecordPerPage = Convert.ToInt32(listModel.PageSize);
            viewModel.TotalPages = Convert.ToInt32(listModel.TotalPages);
            viewModel.TotalResults = Convert.ToInt32(listModel.TotalResults);
            return viewModel;
        }

        /// <summary>
        /// Map ThemeViewModel To ThemeModel
        /// </summary>
        /// <param name="viewModel">View Model Of Theme</param>
        /// <returns>Model Of Theme</returns>
        public static ThemeModel ToModel(ThemeViewModel viewModel)
        {
            if (!Equals(viewModel,null))
            {
                return new ThemeModel()
                {
                    ThemeID = viewModel.ThemeId,
                    Name = viewModel.Name
                };
            }
            else
            {
                return new ThemeModel();
            }
        }

        /// <summary>
        /// Map ThemeModel To ThemeViewModel
        /// </summary>
        /// <param name="model">Model Of Theme</param>
        /// <returns>View Model Of Theme</returns>
        public static ThemeViewModel ToViewModel(ThemeModel model)
        {
            if (!Equals(model, null))
            {
                return new ThemeViewModel()
                {
                    ThemeId = model.ThemeID,
                    Name = model.Name
                };
            }
            else
            {
                return new ThemeViewModel();
            }
        }
    }
}