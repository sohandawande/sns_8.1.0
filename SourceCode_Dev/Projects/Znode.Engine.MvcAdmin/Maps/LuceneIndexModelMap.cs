﻿using System;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Enum;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    public class LuceneIndexModelMap
    {
        public static LuceneIndexListViewModel ToListViewModel(LuceneIndexListModel model)
        {
            LuceneIndexListViewModel listmodel = new LuceneIndexListViewModel();
            model.LuceneIndexList.ForEach(x =>
            {
                listmodel.LuceneIndexList.Add(ToViewModel(x));
            });
            listmodel.TotalResults = (int)model.LuceneIndexList[0].TotalResults;
            listmodel.Page = (int)model.LuceneIndexList[0].PageIndex;
            listmodel.RecordPerPage = (int)model.LuceneIndexList[0].PageSize;

            listmodel.TriggerFlag = model.LuceneIndexList[0].TriggerFlag;
            listmodel.ServiceFlag = model.LuceneIndexList[0].ServiceFlag;

            return listmodel;
        }

        public static LuceneIndexViewModel ToViewModel(LuceneIndexModel model)
        {
            return new LuceneIndexViewModel()
            {
                LuceneIndexMonitorId = model.LuceneIndexMonitorId,
                SourceId = model.SourceId,
                SourceType = model.SourceType,
                SourceTransationType = model.SourceTransationType,
                TransationDateTime =Convert.ToString(model.TransationDateTime),
                IsDuplicate = !Equals(model.IsDuplicate, null) ? (bool)model.IsDuplicate : false,
                AffectedType = model.AffectedType,
                IndexerStatusChangedBy = !Equals(model.IndexerStatusChangedBy, null) ? model.IndexerStatusChangedBy : null,
            };
        }

        public static LuceneIndexServerListViewModel ToServerListViewModel(LuceneIndexServerListModel model)
        {
            LuceneIndexServerListViewModel listmodel = new LuceneIndexServerListViewModel();
            model.IndexServerStatusList.ForEach(x =>
            {
                listmodel.IndexServerStatusList.Add(ToServerViewModel(x));
            });
            return listmodel;
        }


        public static LuceneIndexServerViewModel ToServerViewModel(LuceneIndexServerModel model)
        {
            return new LuceneIndexServerViewModel()
            {
                ServerName = model.ServerName,
                Status = Convert.ToString((ZNodeStatusNames)model.Status),
                StartTime = model.StartTime,
                EndTime = Equals(model.Status, 1) ? null : model.EndTime
            };
        }
    }
}