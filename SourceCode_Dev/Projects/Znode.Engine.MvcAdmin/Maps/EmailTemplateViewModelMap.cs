﻿using System;
using System.IO;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    public static class EmailTemplateViewModelMap
    {
        /// <summary>
        /// Converts Email template model to email template list view model.
        /// </summary>
        /// <param name="model">EmailTemplateListModel model</param>
        /// <returns>Return List of email templates details in the form of EmailTemplateListViewModel</returns>
        public static EmailTemplateListViewModel ToListViewModel(EmailTemplateListModel model)
        {
            if (!Equals(model, null) && !Equals(model.EmailTemplates, null))
            {
                var viewModel = new EmailTemplateListViewModel()
                {
                    EmailTemplateList = model.EmailTemplates.ToList().Select(
                    x => new EmailTemplateViewModel()
                    {
                        TemplateName = x.TemplateName,
                        Name = (x.TemplateName).Replace(Path.GetExtension(x.TemplateName), string.Empty),
                        Extension = Path.GetExtension(x.TemplateName).Replace(".", string.Empty)
                    }).ToList()
                };

                viewModel.TotalResults = Convert.ToInt32(model.TotalResults);
                viewModel.Page = Convert.ToInt32(model.PageIndex);
                viewModel.RecordPerPage = Convert.ToInt32(model.PageSize);
                viewModel.TotalPages = Convert.ToInt32(model.TotalPages);
                viewModel.TotalResults = Convert.ToInt32(model.TotalResults);

                return viewModel;
            }
            else
            {
                return new EmailTemplateListViewModel();
            }
        }

        /// <summary>
        /// Convert EmailTemplateViewModel to EmailTemplateModel.
        /// </summary>
        /// <param name="viewModel">EmailTemplateViewModelmodel.</param>
        /// <returns>Return email template details in EmailTemplateModel format.</returns>
        public static EmailTemplateModel ToModel(EmailTemplateViewModel viewModel)
        {
            if (!Equals(viewModel, null))
            {
                return new EmailTemplateModel()
                {
                    TemplateName = viewModel.TemplateName,
                    Html = viewModel.Html.ToString(),
                    IsNewAdd = viewModel.IsNewAdd,
                    Extension = viewModel.Extension
                };
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Convert EmailTemplateModel to EmailTemplateViewModel.
        /// </summary>
        /// <param name="model">EmailTemplateModel model</param>
        /// <returns>Return Email Template Detail in EmailTemplateViewModel Model </returns>
        public static EmailTemplateViewModel ToViewModel(EmailTemplateModel model)
        {
            if (!Equals(model, null))
            {
                return new EmailTemplateViewModel()
                {
                    Html = Equals(model.Html, null) ? string.Empty : model.Html.Contains("\r\n") ? model.Html.Replace("\r\n", "<!-- -->") : model.Html,
                    TemplateName = model.TemplateName,
                    Extension = model.Extension
                };
            }
            return null;
        }

    }
}