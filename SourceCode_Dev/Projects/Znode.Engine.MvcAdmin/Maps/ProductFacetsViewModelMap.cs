﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.ViewModels;
namespace Znode.Engine.MvcAdmin.Maps
{
    public static class ProductFacetsViewModelMap
    {
        /// <summary>
        /// Convert API Product Facet model to product facet View model.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static ProductFacetsViewModel ToViewModel(ProductFacetModel model, int facetGroupId = 0)
        {
            if (!Equals(model, null))
            {
                var viewModel = new ProductFacetsViewModel()
                {
                    FacetGroups = ToFacetGroup(model.FacetGroups, facetGroupId),
                    AvailableFacets = ToFacetList(model.AssociatedFacets),
                    SelectedFacets = model.AssociatedFacetsSkus.Any() ? ToSelectedFacets(model.AssociatedFacetsSkus, model.AssociatedFacets) : null,

                };
                return viewModel;
            }
            return new ProductFacetsViewModel();
        }

        /// <summary>
        /// Convert Product Facet View model to API product facet model.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static ProductFacetModel ToModel(ProductFacetsViewModel model)
        {
            ProductFacetModel facetModel = new ProductFacetModel();
            if (!Equals(model, null))
            {
                facetModel.ProductId = model.ProductId;
                facetModel.FacetIds = (Equals(model.PostedFacets, null)) ? null : model.PostedFacets.FacetIds;
                facetModel.FacetGroupId = model.FacetGroupId;
            }
            return facetModel;
        }

        /// <summary>
        /// Convert Collection of Facet group model to Drop down list.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private static List<SelectListItem> ToFacetGroup(IEnumerable<FacetGroupModel> model, int facetGroupId = 0)
        {
            List<SelectListItem> lstItems = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                lstItems = (from item in model
                            select new SelectListItem
                            {
                                Text = item.FacetGroupLabel,
                                Value = item.FacetGroupID.ToString(),
                                Selected = Equals(item.FacetGroupID.ToString(), facetGroupId.ToString()),
                            }).ToList();
            }
            return lstItems;

        }

        /// <summary>
        /// Convert Facet model to Facet View model.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private static List<Facets> ToFacetList(IEnumerable<FacetModel> model)
        {
            List<Facets> lstItems = new List<Facets>();
            if (!Equals(model, null))
            {
                lstItems = (from item in model
                            select new Facets
                            {
                                Id = item.FacetID,
                                Name = item.FacetName,

                            }).ToList();
            }
            return lstItems;

        }

        /// <summary>
        /// Get Selected Facets.
        /// </summary>
        /// <param name="model"></param>
        /// <param name="facetmodel"></param>
        /// <returns></returns>
        private static List<Facets> ToSelectedFacets(IEnumerable<FacetProductSKUModel> model, IEnumerable<FacetModel> facetmodel)
        {
            List<Facets> lstItems = new List<Facets>();
            var data = facetmodel.Where(x => model.Any(y => x.FacetID.Equals(y.FacetID))).ToList();
            foreach (var item in data)
            {
                lstItems.Add(new Facets { Id = item.FacetID, Name = item.FacetName });
            }
            return lstItems;

        }

        /// <summary>
        /// Convert Collection of Facet groupt model to facet list view model.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static ProductFacetListViewModel ToListViewModel(IEnumerable<FacetGroupModel> model, int? totalResult, int productId=0)
        {
            if (!Equals(model, null))
            {
                var viewModel = new ProductFacetListViewModel()
                {
                    Facets = model.ToList().Select(
                    x => new ProductFacetGroupsViewModel()
                    {
                        CatalogID = x.CatalogID,
                        FacetGroupID = x.FacetGroupID,
                        FacetGroupLabel = x.FacetGroupLabel,
                        FacetName = HttpUtility.HtmlDecode(x.FacetName),
                        ProductId = productId,
                    }
                    ).ToList()
                };
                viewModel.TotalResults = Convert.ToInt32(totalResult);

                return viewModel;
            }
            return new ProductFacetListViewModel();
        }

        /// <summary>
        /// Convert Collection of Facet groupt model to facet list view model with skuId.
        /// </summary>
        /// <param name="model">IEnumerable<FacetGroupModel> model</param>
        /// <param name="skuId">int skuId</param>
        /// <param name="totalResult">int? totalResult</param>
        /// <returns></returns>
        public static ProductFacetListViewModel ToListViewModel(IEnumerable<FacetGroupModel> model, int skuId, int? totalResult)
        {
            if (!Equals(model, null))
            {
                var viewModel = new ProductFacetListViewModel()
                {
                    Facets = model.ToList().Select(
                    x => new ProductFacetGroupsViewModel()
                    {
                        CatalogID = x.CatalogID,
                        FacetGroupID = x.FacetGroupID,
                        FacetGroupLabel = x.FacetGroupLabel,
                        FacetName = HttpUtility.HtmlDecode(x.FacetName.Replace(",",", ")),
                        SKUId = skuId,
                    }
                    ).ToList()
                };
                viewModel.TotalResults = Convert.ToInt32(totalResult);

                return viewModel;
            }
            return new ProductFacetListViewModel();
        }

    }
}