﻿using System;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    public class CategoryProfileViewModelMap
    {
        #region Public Methods

        /// <summary>
        /// Maps the CategoryProfile Model to CategoryProfileView Model.
        /// </summary>
        /// <param name="model">Model of type CategoryProfileModel</param>
        /// <returns>Returns the mapped CategoryProfile Model to CategoryProfileView Model.</returns>
        public static CategoryProfileViewModel ToViewModel(CategoryProfileModel model)
        {
            if (!Equals(model, null))
            {
                CategoryProfileViewModel viewModel = new CategoryProfileViewModel();
                viewModel.CategoryID = model.CategoryID;
                viewModel.CategoryProfileID = model.CategoryProfileID;
                viewModel.EffectiveDate = HelperMethods.ViewDateFormat(model.EffectiveDate);
                viewModel.ProfileID = model.ProfileID;
                return viewModel;
            }
            return null;
        }

        /// <summary>
        /// Maps the CategoryProfileList Model to CategoryProfileListView Model.
        /// </summary>
        /// <param name="models">Model of type CategoryProfileListModel</param>
        /// <returns>Returns the mapped CategoryProfileList Model to CategoryProfileListView Model.</returns>
        public static CategoryProfileListViewModel ToListViewModel(CategoryProfileListModel models)
        {
            if (!Equals(models, null))
            {
                var viewModel = new CategoryProfileListViewModel()
                {
                    CategoryProfiles = models.CategoryProfiles.ToList().Select(
                    x => new CategoryProfileViewModel()
                    {
                        CategoryProfileID = x.CategoryProfileID,
                        CategoryID = x.CategoryID,
                        ProfileID = x.ProfileID,
                        EffectiveDate = HelperMethods.ViewDateFormat(x.EffectiveDate),
                        ProfileName = x.ProfileName

                    }).ToList()
                };

                viewModel.RecordPerPage = 10;
                viewModel.Page = 1;
                viewModel.TotalResults = Convert.ToInt32(models.TotalResults);
                return viewModel;
            }
            return null;
        }

        /// <summary>
        /// Maps the CategoryProfileView Model to CategoryProfile Model.
        /// </summary>
        /// <param name="viewModel">The model of type CategoryProfileViewModel</param>
        /// <returns>Returns the mapped CategoryProfileView Model to CategoryProfile Model.</returns>
        public static CategoryProfileModel ToModel(CategoryProfileViewModel viewModel)
        {
            if (!Equals(viewModel, null))
            {
                CategoryProfileModel model = new CategoryProfileModel();
                model.CategoryID = viewModel.CategoryID;
                model.CategoryProfileID = viewModel.CategoryProfileID;
                model.EffectiveDate = Convert.ToDateTime(viewModel.EffectiveDate);
                model.ProfileID = viewModel.ProfileID;
                return model;
            }
            return null;
        }

        #endregion
    }
}