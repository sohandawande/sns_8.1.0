﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    /// <summary>
    /// Mapper for Sku Profile Effective View Model.
    /// </summary>
    public static class SkuProfileEffectiveViewModelMap
    {
        /// <summary>
        /// Maps the model to view model.
        /// </summary>
        /// <param name="model">model to convert to view model</param>
        /// <returns>SkuProfileEffectiveViewModel</returns>
        public static SkuProfileEffectiveViewModel ToViewModel(SkuProfileEffectiveModel model)
        {
            if (!Equals(model, null))
            {
                return new SkuProfileEffectiveViewModel()
                {
                    SkuProfileEffectiveID = model.SkuProfileEffectiveID,
                    ProfileId = model.ProfileId,
                    SkuId = model.SkuId,
                    EffectiveDate =HelperMethods.ViewDateFormat(model.EffectiveDate),
                    ProfileName=model.Name
                };
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Maps view model to model.
        /// </summary>
        /// <param name="viewModel">view model t convert to model.</param>
        /// <returns>SkuProfileEffectiveModel</returns>
        public static SkuProfileEffectiveModel ToModel(SkuProfileEffectiveViewModel viewModel)
        {
            if (!Equals(viewModel, null))
            {
                return new SkuProfileEffectiveModel()
                {
                    SkuProfileEffectiveID = viewModel.SkuProfileEffectiveID,
                    ProfileId = viewModel.ProfileId,
                    SkuId = viewModel.SkuId,
                    EffectiveDate = Convert.ToDateTime(viewModel.EffectiveDate),
                    Name = viewModel.ProfileName
                };
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Maps for converting list model to list view model.
        /// </summary>
        /// <param name="models">models to convert to view model.</param>
        /// <returns>SkuProfileEffectiveListViewModel</returns>
        public static SkuProfileEffectiveListViewModel ToListViewModel(SkuProfileEffectiveListModel models)
        {
            if (!Equals(models, null) && !Equals(models.SkuProfileEffectives, null))
            {
                var viewModel = new SkuProfileEffectiveListViewModel()
                {
                    SkuProfileEffectives = models.SkuProfileEffectives.ToList().Select(
                    model => new SkuProfileEffectiveViewModel()
                    {
                        SkuId = model.SkuId,
                        SkuProfileEffectiveID = model.SkuProfileEffectiveID,
                        ProfileId = model.ProfileId,
                        EffectiveDate =HelperMethods.ViewDateFormat(model.EffectiveDate),
                        ProfileName = model.Name
                    }).ToList()
                };
                viewModel.TotalResults = Convert.ToInt32(models.TotalResults);
                return viewModel;
            }
            else
            {
                return new SkuProfileEffectiveListViewModel();
            }
        }
    }
}