﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    public static class ProductBundlesModelMap
    {
        /// <summary>
        /// Convert Product Bundles API list model to view list model.
        /// </summary>
        /// <param name="model">IEnumerable<ProductBundlesModel> model</param>
        /// <param name="totalResult">int? totalResult</param>
        /// <returns>ProductBundlesListViewModel</returns>
        public static ProductBundlesListViewModel ToListViewModel(IEnumerable<ProductBundlesModel> model, int? totalResult)
        {
            if (!Equals(model,null))
            {
                var viewModel = new ProductBundlesListViewModel()
                {
                    ProductBundles = model.ToList().Select(
                   x => new ProductBundlesViewModel()
                   {
                       ChildProductID = x.ChildProductID,
                       ParentChildProductID = x.ParentChildProductID,
                       ParentProductID = x.ParentProductID,
                       ProductName = x.ProductName,
                       IsActive = x.IsActive,
                   }).ToList()
                };
                viewModel.TotalResults = Convert.ToInt32(totalResult);
                return viewModel;
            }
            return new ProductBundlesListViewModel();
        }

        /// <summary>
        /// To map IEnumerable<CatalogModel> model to List<SelectListItem>
        /// </summary>
        /// <param name="model">IEnumerable<CatalogModel></param>
        /// <returns>List<SelectListItem></returns>
        public static List<SelectListItem> ToListItems(IEnumerable<CatalogModel> model)
        {
            List<SelectListItem> catalogItems = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                catalogItems = (from item in model
                                    select new SelectListItem
                                    {
                                        Text = item.Name,
                                        Value = item.CatalogId.ToString(),
                                    }).ToList();
            }
            return catalogItems;
        }
    }
}