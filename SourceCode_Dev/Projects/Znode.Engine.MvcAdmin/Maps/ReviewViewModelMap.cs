﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;
using Znode.Engine.MvcAdmin.Helpers;

namespace Znode.Engine.MvcAdmin.Maps
{
    public class ReviewViewModelMap
    {
        /// <summary>
        /// Method maps the data from Review model to Review view model.
        /// </summary>
        /// <param name="models"></param>
        /// <returns>Retunrs the Review model</returns>
        public static ReviewViewModel ToViewModels(IEnumerable<ReviewModel> model, int? totalResults = 0, int? pageSize = 0, int? totalPages = 0)
        {
            if (Equals(model, null))
            {
                return new ReviewViewModel();
            }
            var viewModel = new ReviewViewModel()
            {

                ReviewItems = model.ToList().Select(
                x => new ReviewItemViewModel()
                {
                    AccountId = x.AccountId,
                    Comments = x.Comments,
                    CreateDate = (x.CreateDate.HasValue) ? HelperMethods.ViewDateFormat(x.CreateDate.Value) : string.Empty,
                    CreateUser = x.CreateUser,
                    ProductName = x.ProductName,
                    Rating = x.Rating,
                    ReviewId = x.ReviewId,
                    Status = (GetReviewStatusDictionary().ContainsKey(x.Status)) ? GetReviewStatusDictionary()[x.Status] : x.Status,
                    Subject = x.Subject,
                    UserLocation = x.UserLocation,
                }).ToList()

            };
            viewModel.RecordPerPage = (Equals(pageSize, 0)) ? Convert.ToInt32(viewModel.TotalResults) : Convert.ToInt32(totalResults);
            viewModel.TotalPages = (Equals(totalPages, 0)) ? Convert.ToInt32(viewModel.TotalPages) : Convert.ToInt32(totalPages);
            viewModel.TotalResults = (Equals(totalResults, 0)) ? Convert.ToInt32(viewModel.TotalResults) : Convert.ToInt32(totalResults);
            return viewModel;
        }

        public static ReviewModel ToReviewModel(ReviewItemViewModel model)
        {
            var reviewModel = new ReviewModel()
            {
                AccountId = model.AccountId,
                Comments = model.Comments,
                CreateDate = Convert.ToDateTime(model.CreateDate),
                CreateUser = model.CreateUser,
                UserLocation = model.UserLocation,
                Subject = model.Subject,
                Rating = model.Rating,
                Status = model.Status,
                ProductId = model.ProductId,
            };
            return reviewModel;
        }

        public static ReviewItemViewModel ToViewModel(ReviewModel model)
        {
            var viewModel = new ReviewItemViewModel()
            {
                AccountId = model.AccountId,
                Comments = model.Comments,
                CreateDate = HelperMethods.ViewDateFormat(Convert.ToDateTime(model.CreateDate)) ,// model.CreateDate.ToString(),
                CreateUser = model.CreateUser,
                ProductName = (Equals(model.Product, null)) ? string.Empty : ProductViewModelMap.ToViewModel(model.Product).Name,
                ProductId = model.ProductId,
                Rating = model.Rating,
                Status = model.Status,
                Subject = model.Subject,
                UserLocation = model.UserLocation,

            };
            viewModel.GetReviewStatus = GetReviewStatus(model.Status);
            viewModel.GetReviewRatings = GetReviewRatings(model.Rating);
            return viewModel;
        }

        public static List<SelectListItem> GetReviewStatus(string status)
        {
            List<SelectListItem> statusList = new List<SelectListItem>();

            statusList = (from item in GetReviewStatusDictionary()
                          where item.Key != "0"
                          select new SelectListItem
                          {
                              Text = item.Value,
                              Value = item.Key,
                              Selected = Equals(item.Key, status),
                          }).ToList();

            return statusList;
        }


        private static List<SelectListItem> GetReviewRatings(int rating)
        {
            List<SelectListItem> ratingList = new List<SelectListItem>();

            ratingList = (from item in GetReviewRatingsDictionary()
                          select new SelectListItem
                          {
                              Text = item.Value,
                              Value = item.Key.ToString(),
                              Selected = Equals(item.Key, rating),
                          }).ToList();

            return ratingList;
        }


        #region ReviewStatusDictionary
        private static Dictionary<string, string> GetReviewStatusDictionary()
        {
            Dictionary<string, string> reviewStatus = new Dictionary<string, string>();
            reviewStatus.Add("0", "All");
            reviewStatus.Add("A", "Active");
            reviewStatus.Add("I", "Inactive");
            reviewStatus.Add("N", "New");
            return reviewStatus;
        }
        #endregion
        #region ReviewRatingsDictionary
        private static Dictionary<int, string> GetReviewRatingsDictionary()
        {
            Dictionary<int, string> reviewRatings = new Dictionary<int, string>();
            reviewRatings.Add(5, "5 - Excellent, Perfect");
            reviewRatings.Add(4, "4 - That's Good Stuff");
            reviewRatings.Add(3, "3 - Average, Ordinary");
            reviewRatings.Add(2, "2 - Needs that Special Something");
            reviewRatings.Add(1, "1 - Not Good");
            return reviewRatings;
        }
        #endregion
    }
}