﻿using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    /// <summary>
    /// Static Class for Tax Mapper
    /// </summary>
    public static class TaxViewModelMap
    {
        /// <summary>
        /// Mapper to convert TaxClassModel to TaxViewModel   
        /// </summary>
        /// <param name="taxClassModel">Object of TaxClassModel</param>
        /// <returns>TaxViewModel</returns>
        public static TaxViewModel ToViewModel(TaxClassModel taxClassModel)
        {
            if (Equals(taxClassModel, null))
            {
                return null;
            }

            return new TaxViewModel()
            {
                TaxClassName = taxClassModel.Name,
                DisplayOrder = taxClassModel.DisplayOrder,
                Enable = taxClassModel.ActiveInd
            };
        }

        /// <summary>
        /// Mapper to convert TaxViewModel to TaxClassModel    
        /// </summary>
        /// <param name="taxViewModel">Object of TaxViewModel</param>
        /// <returns>TaxClassModel</returns>
        public static TaxClassModel ToModel(TaxViewModel taxViewModel)
        {
            if (Equals(taxViewModel, null))
            {
                return null;
            }

            return new TaxClassModel()
            {
                Name = taxViewModel.TaxClassName,
                DisplayOrder = taxViewModel.DisplayOrder,
                ActiveInd = taxViewModel.Enable
            };
        }
    }
}