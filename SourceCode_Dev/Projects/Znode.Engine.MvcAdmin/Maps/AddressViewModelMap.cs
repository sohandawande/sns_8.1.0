﻿using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    /// <summary>
    /// Mapper model for Address
    /// </summary>
    public static class AddressViewModelMap
    {
        /// <summary>
        /// Mapping of Address Model to Address View Model
        /// </summary>
        /// <param name="viewModel">Object of Address View Model</param>
        /// <returns>Address Model</returns>
        public static AddressModel ToModel(AddressViewModel viewModel)
        {
            if (viewModel == null )
            {
                
            }
            return new AddressModel()
            {
                AccountId = viewModel.AccountId,
                AddressId = viewModel.AddressId,
                City = viewModel.City,
                CompanyName = viewModel.CompanyName,
                FirstName = viewModel.FirstName,
                IsDefaultBilling = viewModel.IsDefaultBilling,
                IsDefaultShipping = viewModel.IsDefaultShipping,
                LastName = viewModel.LastName,
                Name = viewModel.Name,
                MiddleName = viewModel.MiddleName,
                PhoneNumber = viewModel.PhoneNumber,
                PostalCode = Equals(viewModel.PostalCode, null) ? string.Empty : viewModel.PostalCode,
                StateCode = Equals(viewModel.StateCode, null) ? string.Empty : viewModel.StateCode,
                StreetAddress1 = viewModel.StreetAddress1,
                StreetAddress2 = viewModel.StreetAddress2,
                CountryCode = viewModel.CountryCode,
            };
        }

        /// <summary>
        /// Mapping of Address View Model to Address Model 
        /// </summary>
        /// <param name="model">Object of Address Model</param>
        /// <returns>Address View Model</returns>
        public static AddressViewModel ToViewModel(AddressModel model)
        {
            return new AddressViewModel()
            {
                AccountId = model.AccountId,
                AddressId = model.AddressId,
                City = model.City,
                CompanyName = model.CompanyName,
                FirstName = model.FirstName,
                IsDefaultBilling = model.IsDefaultBilling,
                IsDefaultShipping = model.IsDefaultShipping,
                LastName = model.LastName,
                Name = model.Name,
                MiddleName = model.MiddleName,
                PhoneNumber = model.PhoneNumber,
                PostalCode = model.PostalCode,
                StateCode = model.StateCode,
                StreetAddress1 = model.StreetAddress1,
                StreetAddress2 = model.StreetAddress2,
                CountryCode = model.CountryCode,
            };
        }
       
        /// <summary>
        /// Mapping of OrderModel  to AddressViewModel
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public static AddressViewModel ToModel(OrderModel viewModel)
        {
            var model = new AddressViewModel()
            {
                City = viewModel.BillingCity,
                CompanyName = viewModel.BillingCompanyName,
                FirstName = viewModel.BillingFirstName,
                LastName = viewModel.BillingLastName,
                PhoneNumber = viewModel.BillingPhoneNumber,
                PostalCode = viewModel.BillingPostalCode,
                StateCode = viewModel.BillingStateCode,
                StreetAddress1 = viewModel.BillingStreetAddress1,
                StreetAddress2 = viewModel.BillingStreetAddress2,
                CountryCode = viewModel.BillingCountryCode
            };
            return model;
        }

        /// <summary>
        /// Mapping of OrderShipmentModel to AddressViewModel
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public static AddressViewModel ToModel(OrderShipmentModel viewModel)
        {
            var model = new AddressViewModel()
            {
                City = viewModel.ShipToCity,
                CompanyName = viewModel.ShipToCompanyName,
                FirstName = viewModel.ShipToFirstName,
                LastName = viewModel.ShipToLastName,
                PhoneNumber = viewModel.ShipToPhoneNumber,
                PostalCode = viewModel.ShipToPostalCode,
                StateCode = viewModel.ShipToStateCode,
                StreetAddress1 = viewModel.ShipToStreetAddress1,
                StreetAddress2 = viewModel.ShipToStreetAddress2,
                CountryCode = viewModel.ShipToCountryCode
            };
            return model;
        }

        /// <summary>
        /// Mapping of AddressModel Model to AddressViewModel
        /// </summary>
        /// <param name="model">AddressModel model</param>
        /// <returns>Address View Model</returns>
        public static AddressViewModel ToModel(AddressModel model)
        {
            var viewModel = new AddressViewModel()
            {
                AccountId = model.AccountId,
                AddressId = model.AddressId,
                City = model.City,
                CompanyName = model.CompanyName,
                FirstName = model.FirstName,
                IsDefaultBilling = model.IsDefaultBilling,
                IsDefaultShipping = model.IsDefaultShipping,
                LastName = model.LastName,
                Name = model.Name,
                MiddleName = model.MiddleName,
                PhoneNumber = model.PhoneNumber,
                PostalCode = model.PostalCode,
                StateCode = model.StateCode,
                StreetAddress1 = model.StreetAddress1,
                StreetAddress2 = model.StreetAddress2,
                CountryCode = model.CountryCode,
            };
            return viewModel;
        }
    }
}