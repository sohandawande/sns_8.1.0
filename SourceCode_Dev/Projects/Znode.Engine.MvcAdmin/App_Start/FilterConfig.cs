﻿using System.Web.Mvc;
using Znode.Engine.MvcAdmin.Helpers;

namespace Znode.Engine.MvcAdmin
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
        //    filters.Add(new ElmahHandledErrorLoggerFilter());
            filters.Add(new AuthenticationHelper());
            filters.Add(new HandleErrorAttribute());
        }
    }
}
