﻿using System.Web;
using System.Web.Optimization;

namespace Znode.Engine.MvcAdmin
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery")
                .Include("~/Scripts/jquery-{version}.js")
                .Include("~/Scripts/jquery-dialog.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                              "~/Scripts/jquery.unobtrusive*",
                              "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                       "~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Content/bootstrap-3.3.1/js/bootstrap.js",
                      "~/Content/bootstrap-3.3.1/js/datepicker.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/CustomJs")
                .Include("~/Scripts/CustomScripts/gluten.js")
                .Include("~/Scripts/CustomScripts/commonhelper.js")
                .Include("~/Scripts/CustomScripts/Config.js")
                .Include("~/Scripts/CustomScripts/global.js")
                .Include("~/Scripts/CustomScripts/Facet.js")
                .Include("~/Scripts/CustomScripts/api.js")
                .Include("~/Scripts/CustomScripts/CustomValidations.js"));


            bundles.Add(new ScriptBundle("~/bundles/dynamicgridJs")
            .Include("~/Scripts/jurl.js")
            .Include("~/Scripts/CustomScripts/GridPager.js")
            .Include("~/Scripts/WebGrid/dynamicgrid.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap-3.3.1/css/bootstrap.css",
                      "~/Content/site.css"));

            bundles.Add(new ScriptBundle("~/bundles/TwoListView").Include(
                     "~/Scripts/TwoListView.js"));

            bundles.Add(new ScriptBundle("~/bundles/CommonHelper").Include(
                    "~/Scripts/CustomScripts/commonhelper.js"));

            bundles.Add(new ScriptBundle("~/bundles/Highlight").Include(
                "~/Scripts/CustomScripts/Highlight.js"));

            bundles.Add(new ScriptBundle("~/bundles/StoreLocator").Include(
                "~/Scripts/CustomScripts/StoreLocator.js"));

            bundles.Add(new ScriptBundle("~/bundles/ContentPage").Include(
               "~/Scripts/CustomScripts/ContentPage.js"));

            bundles.Add(new ScriptBundle("~/bundles/product").Include(
                     "~/Scripts/CustomScripts/Product.js"));

            bundles.Add(new ScriptBundle("~/bundles/AttributeType").Include(
                     "~/Scripts/CustomScripts/AttributeType.js"));

            bundles.Add(new ScriptBundle("~/bundles/Promotion").Include(
                "~/Scripts/CustomScripts/Promotion.js"));

            bundles.Add(new ScriptBundle("~/bundles/Portal").Include(
                    "~/Scripts/CustomScripts/Portals.js"));

            bundles.Add(new ScriptBundle("~/bundles/AssociateCategory").Include(
                "~/Scripts/CustomScripts/AssociateCategory.js"));

            bundles.Add(new ScriptBundle("~/bundles/CategoryAssociateProducts").Include(
                "~/Scripts/CustomScripts/CategoryAssociateProducts.js"));

            bundles.Add(new ScriptBundle("~/bundles/AccountAssociatedProfiles").Include(
                "~/Scripts/CustomScripts/AccountAssociatedProfiles.js"));

            bundles.Add(new ScriptBundle("~/bundles/PRFTUserAssociatedCustomer").Include(
                "~/Scripts/CustomScripts/PRFTUserAssociatedCustomer.js"));

            bundles.Add(new ScriptBundle("~/bundles/Shipping").Include(
                "~/Scripts/CustomScripts/Shipping.js"));


            bundles.Add(new ScriptBundle("~/bundles/WebGrid").Include(
                     "~/Scripts/WebGrid/webgridpagin.Config.js"));


            bundles.Add(new ScriptBundle("~/bundles/Report").Include(
                                   "~/Scripts/CustomScripts/Reports.js"));

            bundles.Add(new StyleBundle("~/Content/WebGridCss").Include(
             "~/Content/WebGrid/WebGrid.css"
             ));

            bundles.Add(new ScriptBundle("~/bundles/ManageSEO").Include(
                    "~/Scripts/CustomScripts/ManageSEO.js"));

            bundles.Add(new ScriptBundle("~/bundles/VendorProduct").Include(
               "~/Scripts/CustomScripts/VendorProduct.js"));

            bundles.Add(new ScriptBundle("~/bundles/EmailTemplate").Include(
               "~/Scripts/CustomScripts/EmailTemplate.js"));

            bundles.Add(new ScriptBundle("~/bundles/Personalization").Include(
                "~/Scripts/CustomScripts/Personalization.js"));

            bundles.Add(new ScriptBundle("~/bundles/Tax").Include(
               "~/Scripts/CustomScripts/Tax.js"));

            bundles.Add(new ScriptBundle("~/bundles/GoogleSiteMap").Include(
              "~/Scripts/CustomScripts/GoogleSiteMap.js"));

            bundles.Add(new ScriptBundle("~/bundles/Supplier").Include(
                "~/Scripts/CustomScripts/Supplier.js"));

            bundles.Add(new ScriptBundle("~/bundles/RMAConfiguration").Include(
              "~/Scripts/CustomScripts/RMAConfiguration.js"));

            bundles.Add(new ScriptBundle("~/bundles/ProviderEngine").Include(
                "~/Scripts/CustomScripts/ProviderEngine.js"));

            bundles.Add(new ScriptBundle("~/bundles/orders").Include(
                "~/Scripts/jurl.min.js")
            .Include("~/Scripts/CustomScripts/orders.js"));

            bundles.Add(new ScriptBundle("~/bundles/Customers").Include(
               "~/Scripts/CustomScripts/Customers.js"));

            bundles.Add(new ScriptBundle("~/bundles/RMAManager").Include(
              "~/Scripts/CustomScripts/RMAManager.js"));


            bundles.Add(new ScriptBundle("~/bundles/ImportExport").Include(
               "~/Scripts/CustomScripts/ImportExport.js"));

            bundles.Add(new ScriptBundle("~/bundles/Catalog").Include(
              "~/Scripts/CustomScripts/Catalog.js"));

            bundles.Add(new ScriptBundle("~/bundles/GiftCard").Include(
              "~/Scripts/CustomScripts/GiftCard.js"));

            bundles.Add(new ScriptBundle("~/bundles/AddOn").Include(
              "~/Scripts/CustomScripts/AddOn.js"));

            bundles.Add(new ScriptBundle("~/bundles/FranchiseShipping").Include(
              "~/Scripts/CustomScripts/FranchiseShipping.js"));

            bundles.Add(new ScriptBundle("~/bundles/tinymceJs")
                .Include("~/Scripts/tinymce/js/tinymce/tinymce.min.js")
                .Include("~/Scripts/tinymce/js/tinymce/inittinymce.js"));

            bundles.Add(new ScriptBundle("~/bundles/FranchisePayment").Include(
               "~/Scripts/CustomScripts/FranchisePayment.js"));

            bundles.Add(new ScriptBundle("~/bundles/Payment").Include(
              "~/Scripts/CustomScripts/Payment.js"));

            bundles.Add(new ScriptBundle("~/bundles/FranchiseTax").Include(
               "~/Scripts/CustomScripts/FranchiseTax.js"));

            bundles.Add(new ScriptBundle("~/bundles/Nicescroll").Include(
                      "~/Content/bootstrap-3.3.1/js/nicescroll.js"));

            bundles.Add(new ScriptBundle("~/bundles/ContentPageRevision").Include(
                      "~/Scripts/CustomScripts/ContentPageRevision.js"));

            bundles.Add(new ScriptBundle("~/bundles/FranchiseAdmin").Include(
                      "~/Scripts/CustomScripts/FranchiseAdmin.js"));

            bundles.Add(new ScriptBundle("~/bundles/ChartJs")
                .Include("~/Scripts/Charts/highcharts.js",
                "~/Scripts/Charts/highcharts-3d.js",
                "~/Scripts/Charts/exporting.js",
                "~/Scripts/Charts/Charts.js"));
            BundleTable.EnableOptimizations = false;
        }
    }
}

