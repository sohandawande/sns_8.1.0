﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.MvcAdmin.Helpers;

namespace Znode.Engine.MvcAdmin.Controllers.Home
{
    public class HomeController : BaseController
    {
        /// <summary>
        /// To show Home page
        /// </summary>
        /// <returns></returns>
        public ActionResult Home()
        {
            return View();
        }

        /// <summary>
        /// Handles all application level errors.
        /// </summary>
        /// <param name="exception"></param>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult ErrorHandler(Exception exception)
        {
            HttpException httpexception = exception as HttpException;

            if (httpexception != null)
            {
                int httpCode = httpexception.GetHttpCode();

                switch (httpCode)
                {
                    case 404:
                        {
                            ViewBag.ErrorMessage = ZnodeResources.HttpCode_404_PageNotFoundMsg;
                            break;
                        }
                    case 401:
                        {
                            ViewBag.ErrorMessage = ZnodeResources.HttpCode_401_AccessDeniedMsg;
                            break;
                        }
                    default:
                        {
                            if (exception is HttpRequestValidationException)
                            {
                                ViewBag.ErrorMessage = ZnodeResources.HttpCode_500_RequestValidationErrorMsg;
                            }
                            else
                            {
                                if (exception is HttpAntiForgeryException)
                                {
                                    ViewBag.ErrorMessage = ZnodeResources.HttpAntiforgeryTokenExceptionMessage;
                                }
                                else
                                {
                                    ViewBag.ErrorMessage = ZnodeResources.HttpCode_500_InternalServerErrorMsg;
                                }
                            }
                            break;
                        }
                }
            }
            else
            {
                ViewBag.ErrorMessage = ZnodeResources.GenericErrorMessage;
            }
            return View(MvcAdminConstants.ErrorView);
        }
    }
}