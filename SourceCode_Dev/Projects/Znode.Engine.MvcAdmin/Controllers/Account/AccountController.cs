﻿using Resources;
using System;
using System.Configuration;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.Agents.Agents;
using Znode.Engine.MvcAdmin.Agents.IAgents;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.ViewModels;
using Znode.Libraries.Helpers.Constants;

namespace Znode.Engine.MvcAdmin.Controllers
{
    public class AccountController : BaseController
    {
        #region Private Variables
        private readonly IAccountAgent _accountAgent;
        private readonly IActivateAgent _activateAgent;
        private AuthenticationHelper _authenticationHelper;
        private LoginViewModel model = null;
        private string DashBoardControllerName = "Dashboard";
        private string DashBoardAction = "Dashboard";
        private string LoginAction = "Login";
        private string ForgotPasswordAction = "ForgotPassword";
        private string ResetAdminDetailsAction = "ResetAdminDetails";
        private string ResetAdminPasswordAction = "ResetAdminPassword";
        private string LoginView = "Login";
        private string ResetPasswordView = "ResetPassword";
        private string ForgotPasswordView = "ForgotPassword";
        private string ChangePasswordView = "ChangePassword";
        private string ResetAdminPasswordView = "ResetAdminPassword";
        private string HelpView = "Help";
        #endregion

        #region Constructor
        public AccountController()
        {
            _accountAgent = new AccountAgent();
            _activateAgent = new ActivateAgent();
            _authenticationHelper = new AuthenticationHelper();
        }
        #endregion

        #region Public Methods

        #region Get/Post action for Login
        /// <summary>
        /// Login index page
        /// This method first validates the ZNode License. If valid then redirects to login page else redirects to License Activation page
        /// </summary>
        /// <param name="returnUrl">String returnUrl for returning to original page</param>
        /// <returns>Returns login view.</returns>
        [AllowAnonymous]
        [HttpGet]
        public ActionResult Login(string returnUrl)
        {
            if (_activateAgent.ValidateLicense().HasError)
            {
                return RedirectToAction(MvcAdminConstants.IndexAction, MvcAdminConstants.ActivateController, new { @area = MvcAdminConstants.ActivateArea });
            }
            else
            {
                if (User.Identity.IsAuthenticated)
                {
                    _accountAgent.Logout();
                }
                //Get user name from cookies
                GetLoginRememberMeCookie();
                return View(LoginView, model);
            }
        }

        /// <summary>
        /// Posts the LoginViewModel to authenticate the user.
        /// </summary>
        /// <param name="model">Model of type LoginViewModel</param>
        /// <param name="returnUrl">String returnUrl for returning to original page</param>
        /// <returns>Logs in if the user is authenticated or it shows error messages accordingly.</returns>
        [AllowAnonymous]
        [HttpPost]
        public ActionResult Login(LoginViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                //Login the user.

                LoginViewModel loginviewModel = _accountAgent.Login(model);

                if (!loginviewModel.HasError)
                {
                    //Set the Authentication cookie.
                    _authenticationHelper.SetAuthCookie(model.Username, model.RememberMe);

                    //TODO//Remember me
                    if (checked(model.RememberMe))
                    {
                        SaveLoginRememberMeCookie(model.Username);
                    }
                    return ValidateUserRole(model, returnUrl);
                }
                if (!Equals(loginviewModel, null) && loginviewModel.IsResetAdmin)
                {
                    return RedirectToAction(ResetAdminDetailsAction);
                }
                if (!Equals(loginviewModel, null) && loginviewModel.IsResetAdminPassword)
                {
                    TempData[MvcAdminConstants.UserName] = model.Username;
                    return RedirectToAction(ResetAdminPasswordAction);
                }
                TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(loginviewModel.ErrorMessage, Znode.Engine.MvcAdmin.Models.NotificationType.error);
                return View(LoginView, loginviewModel);
            }
            return View(LoginView, model);
        }

        #endregion

        #region Post action for Logout
        /// <summary>
        /// Logs off the user from the site.
        /// </summary>
        /// <returns>Redirects to Login action</returns>
        [AllowAnonymous]
        [HttpPost]
        public ActionResult Logout()
        {
            if (User.Identity.IsAuthenticated)
            {
                _accountAgent.Logout();
            }
            return RedirectToAction(LoginAction);
        }

        #endregion

        #region Get/Post Action for ResetAdminDetails
        /// <summary>
        /// Get method to resets the admin details
        /// Checks the account key in session. If found, redirects to ResetAdminDetails view else to Login view
        /// </summary>
        /// <returns>if account key present in session, it redirects to ResetAdminDetails view else to Login view</returns>
        [AllowAnonymous]
        [HttpGet]
        public ActionResult ResetAdminDetails()
        {
            if (!_accountAgent.CheckAccountKey())
            {
                return RedirectToAction(LoginAction);
            }
            return View();
        }

        /// <summary>
        /// This method resets the admin details and login with new details
        /// </summary>
        /// <param name="model">ResetPasswordModel which contains details of admin</param>
        /// <returns>Returns ResetAdminDetails view</returns>
        [AllowAnonymous]
        [HttpPost]
        public ActionResult ResetAdminDetails(ResetPasswordModel model)
        {
            if (ModelState.IsValid)
            {
                ResetPasswordModel resetPasswordModel = _accountAgent.ResetPassword(model);
                if (!resetPasswordModel.HasError)
                {
                    LoginViewModel loginModel = new LoginViewModel()
                    {
                        Username = model.Username,
                        Password = model.Password,
                    };
                    LoginViewModel accountViewModel = _accountAgent.Login(loginModel);

                    if (!accountViewModel.HasError)
                    {
                        Session.Add(MvcAdminConstants.SuccessMessage, resetPasswordModel.SuccessMessage);
                        _authenticationHelper.SetAuthCookie(loginModel.Username, true);
                        TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(resetPasswordModel.SuccessMessage, Znode.Engine.MvcAdmin.Models.NotificationType.success);
                        return RedirectToAction(DashBoardAction, DashBoardControllerName);
                    }
                }
                else
                {
                    TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(resetPasswordModel.ErrorMessage, Znode.Engine.MvcAdmin.Models.NotificationType.error);
                }
            }
            return View();
        }

        #endregion

        #region Get/Post Action for ResetPassword
        /// <summary>
        /// Reset Password Page
        /// </summary>
        /// <param name="passwordToken">Reset Password Token for the User</param>
        /// <param name="userName">User name for the user</param>
        /// <returns>Return the Reset Password View</returns>
        [AllowAnonymous]
        [HttpGet]
        public ActionResult ResetPassword(string passwordToken, string userName)
        {
            passwordToken = WebUtility.UrlDecode(passwordToken);
            userName = WebUtility.UrlDecode(userName);
            ChangePasswordViewModel resetPassword = new ChangePasswordViewModel();
            resetPassword.UserName = userName;
            resetPassword.PasswordToken = passwordToken;

            //Set ResetPasword flag, use to hide Old Password field in View.
            resetPassword.IsResetPassword = true;
            ResetPasswordStatusTypes enumStatus;

            enumStatus = _accountAgent.CheckResetPasswordLinkStatus(resetPassword);
            switch (enumStatus)
            {
                case ResetPasswordStatusTypes.Continue:
                    {
                        return View(ResetPasswordView, resetPassword);
                    }
                case ResetPasswordStatusTypes.LinkExpired:
                case ResetPasswordStatusTypes.TokenMismatch:
                case ResetPasswordStatusTypes.NoRecord:
                default:
                    {
                        TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.ResetPasswordLinkError, Znode.Engine.MvcAdmin.Models.NotificationType.error);
                        return RedirectToAction(ForgotPasswordAction);
                    }
            }
        }

        /// <summary>
        /// Posts the ChangePasswordViewModel to Reset the user password.
        /// </summary>
        /// <param name="model">Model of type ChangePasswordViewModel</param>
        /// <returns>Reset the new password for authenticate user & send it over email or it shows error messages accordingly.</returns>
        [AllowAnonymous]
        [HttpPost]
        public ActionResult ResetPassword(ChangePasswordViewModel model)
        {
            ModelState.Remove(MvcAdminConstants.OldPassword);
            model.IsResetPassword = true;
            if (ModelState.IsValid)
            {
                ChangePasswordViewModel changepasswordmodel = _accountAgent.ChangePassword(model);

                if (!model.HasError)
                {
                    LoginViewModel loginModel = new LoginViewModel()
                    {
                        Username = model.UserName,
                        Password = model.NewPassword,
                    };
                    LoginViewModel accountViewModel = _accountAgent.Login(loginModel);

                    if (!accountViewModel.HasError)
                    {
                        Session.Add(MvcAdminConstants.SuccessMessage, changepasswordmodel.SuccessMessage);
                        _authenticationHelper.SetAuthCookie(loginModel.Username, true);
                        TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(changepasswordmodel.SuccessMessage, Znode.Engine.MvcAdmin.Models.NotificationType.success);
                        return RedirectToAction(DashBoardAction, DashBoardControllerName);
                    }
                }
                else
                {
                    TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(changepasswordmodel.ErrorMessage, Znode.Engine.MvcAdmin.Models.NotificationType.error);
                }
            }
            return View(ResetPasswordView, model);
        }

        #endregion

        #region Get/Post Action for Forgot Password
        /// <summary>
        /// Forgot Password Page
        /// </summary>
        /// <returns>Return Forgot Password View</returns>
        [AllowAnonymous]
        [HttpGet]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        /// <summary>
        /// Posts the AccountViewModel to Send the new Password to the user
        /// </summary>
        /// <param name="model">Model of type AccountViewModel</param>
        /// <returns>Create the new password for authenticate user & send it over email or it shows error messages accordingly.</returns>
        [AllowAnonymous]
        [HttpPost]
        public ActionResult ForgotPassword(AccountViewModel model)
        {
            ModelState.Remove(MvcAdminConstants.CreateDteModelKey);
            ModelState.Remove(MvcAdminConstants.UpdateDteModelKey);
            if (ModelState.IsValid)
            {
                model = _accountAgent.ForgotPassword(model);
                TempData[MvcAdminConstants.Notifications] = (model.HasError)
                    ? GenerateNotificationMessages(model.ErrorMessage, Znode.Engine.MvcAdmin.Models.NotificationType.error)
                    : GenerateNotificationMessages(model.SuccessMessage, Znode.Engine.MvcAdmin.Models.NotificationType.info);
            }
            return View(ForgotPasswordView, model);
        }
        #endregion

        #region Get/Post for Change Password
        /// <summary>
        /// Change Password Page
        /// </summary>
        /// <returns>Return Change Password View</returns>
        [Authorize]
        [HttpGet]
        public ActionResult Changepassword()
        {
            return View(ChangePasswordView, new ChangePasswordViewModel());
        }

        /// <summary>
        /// Posts the ChangePasswordViewModel to Change the Password for the user.
        /// </summary>
        /// <param name="model">Model of type ChangePasswordViewModel</param>
        /// <returns>Change the password for authenticate user or it shows error messages accordingly.</returns>
        [Authorize]
        [HttpPost]
        public ActionResult Changepassword(ChangePasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                model.UserName = User.Identity.Name;
                model = _accountAgent.ChangePassword(model);

                TempData[MvcAdminConstants.Notifications] = (model.HasError)
                    ? GenerateNotificationMessages(model.ErrorMessage, Znode.Engine.MvcAdmin.Models.NotificationType.error)
                    : GenerateNotificationMessages(model.SuccessMessage, Znode.Engine.MvcAdmin.Models.NotificationType.success);
                if (!model.HasError)
                {
                    return RedirectToAction(DashBoardAction, DashBoardControllerName);
                }
            }
            return View(ChangePasswordView, model);
        }

        #endregion

        #region Get/Post Action for Reset Admin Password
        /// <summary>
        /// Reset Admin Password view.
        /// </summary>
        /// <returns>Return th Reset Admin password view</returns>
        [AllowAnonymous]
        [HttpGet]
        public ActionResult ResetAdminPassword()
        {
            if (!_accountAgent.CheckAccountKey() || Equals(TempData[MvcAdminConstants.UserName], null))
            {
                return RedirectToAction(LoginAction);
            }
            ChangePasswordViewModel model = new ChangePasswordViewModel();
            model.UserName = Convert.ToString(TempData[MvcAdminConstants.UserName]);
            return View(ResetAdminPasswordView, model);
        }

        /// <summary>
        /// Post Action to Reset the Admin password Details
        /// </summary>
        /// <param name="model">ChangePasswordViewModel model</param>
        /// <returns>Return to the Dashboard view in case of sucess or it shows error messages accordingly.</returns>
        [AllowAnonymous]
        [HttpPost]
        public ActionResult ResetAdminPassword(ChangePasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                ChangePasswordViewModel resetPasswordModel = _accountAgent.ChangePassword(model);
                if (!resetPasswordModel.HasError)
                {
                    LoginViewModel loginModel = new LoginViewModel()
                    {
                        Username = model.UserName,
                        Password = model.NewPassword,
                    };
                    LoginViewModel accountViewModel = _accountAgent.Login(loginModel);

                    if (!accountViewModel.HasError)
                    {
                        Session.Add(MvcAdminConstants.SuccessMessage, resetPasswordModel.SuccessMessage);
                        _authenticationHelper.SetAuthCookie(loginModel.Username, true);
                        TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(resetPasswordModel.SuccessMessage, Znode.Engine.MvcAdmin.Models.NotificationType.success);
                        return RedirectToAction(DashBoardAction, DashBoardControllerName);
                    }
                }
                else
                {
                    TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(resetPasswordModel.ErrorMessage, Znode.Engine.MvcAdmin.Models.NotificationType.error);
                }
            }
            return View(model);
        }
        #endregion

        #region Help
        /// <summary>
        /// Help View
        /// </summary>
        /// <returns></returns>
        public ActionResult HelpPage()
        {
            return View(HelpView);
        }
        #endregion
        #endregion

        #region Private Methods
        /// <summary>
        /// Remember me function for logged in user on remember me selection
        /// This function is used to save user name in cookies.
        /// </summary>
        /// <param name="userId">string userId</param>
        private void SaveLoginRememberMeCookie(string userId)
        {
            //Check if the browser support cookies 
            if ((HttpContext.Request.Browser.Cookies))
            {
                HttpCookie cookieLoginRememberMe = new HttpCookie(MvcAdminConstants.LoginCookieNameValue);
                cookieLoginRememberMe.Values[MvcAdminConstants.LoginCookieNameValue] = userId;
                cookieLoginRememberMe.Expires = DateTime.Now.AddDays(Convert.ToDouble(ConfigurationManager.AppSettings["CookieExpiresValue"]));
                HttpContext.Response.Cookies.Add(cookieLoginRememberMe);
                cookieLoginRememberMe.HttpOnly = true;
            }
        }

        /// <summary>
        /// Remember me function for logged in user on remember me selection
        /// This method is used to get user name value from cookies.
        /// </summary>
        private void GetLoginRememberMeCookie()
        {
            if ((HttpContext.Request.Browser.Cookies))
            {
                if (!Equals(HttpContext.Request.Cookies[MvcAdminConstants.LoginCookieNameValue], null))
                {
                    HttpCookie cookieRememberMe = HttpContext.Request.Cookies[MvcAdminConstants.LoginCookieNameValue];
                    if (!Equals(cookieRememberMe, null))
                    {
                        string loginName = HttpUtility.HtmlEncode(cookieRememberMe.Values[MvcAdminConstants.LoginCookieNameValue]);
                        model = new LoginViewModel();
                        model.Username = loginName;
                        model.RememberMe = true;
                    }
                }
            }
        }

        private ActionResult ValidateUserRole(LoginViewModel model, string returnUrl)
        {
            if (_accountAgent.IsUserInRole(model.Username, Constant.RoleOrderApprover) && !_accountAgent.IsUserInRole(model.Username, Constant.RoleAdmin))
            {
                return RedirectToLocal(returnUrl);
            }
            if ((_accountAgent.IsUserInRole(model.Username.Trim(), Constant.RoleReviewer) || _accountAgent.IsUserInRole(model.Username.Trim(), Constant.RoleReviewerManager)) && !_accountAgent.IsUserInRole(model.Username, Constant.RoleAdmin))
            {
                return RedirectToLocal(returnUrl);
            }
            // if user is an admin, then redirect to the admin dashboard
            if (_accountAgent.IsUserInRole(model.Username.Trim(), Constant.RoleAdmin))// && string.IsNullOrEmpty(Request.QueryString["returnurl"]))
            {
                return RedirectToLocal(returnUrl);
            }
            else if (_accountAgent.IsUserInRole(model.Username.Trim(), Constant.RoleFranchise) && string.IsNullOrEmpty(Request.QueryString["returnurl"]))
            {
                return RedirectToAction(DashBoardAction, MvcAdminConstants.FranchiseDashboardController, new { area = MvcAdminConstants.FranchiseAdminArea });
            }
            else if (_accountAgent.IsUserInRole(model.Username.Trim(), Constant.RoleCustomerServiceRep) || _accountAgent.IsUserInRole(model.Username.Trim(), Constant.RoleCatalogEditor) || _accountAgent.IsUserInRole(model.Username.Trim(), Constant.RoleExecutive) || _accountAgent.IsUserInRole(model.Username.Trim(), Constant.RoleOrderOnly) || _accountAgent.IsUserInRole(model.Username.Trim(), Constant.RoleSEO) || _accountAgent.IsUserInRole(model.Username.Trim(), Constant.RoleContentEditor) && string.IsNullOrEmpty(Request.QueryString["returnurl"]))
            {
                return RedirectToLocal(returnUrl);
            }
            TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.ErrorAccessDenied, Znode.Engine.MvcAdmin.Models.NotificationType.error);
            _authenticationHelper.RedirectFromLoginPage(model.Username, false);
            return RedirectToAction(LoginAction);
        }

        /// <summary>
        /// To Redirect the user based on returnUrl
        /// </summary>
        /// <param name="returnUrl">string returnUrl</param>
        /// <returns>Return the Return url action</returns>
        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (!string.IsNullOrEmpty(returnUrl))
            {
                return Redirect(HttpUtility.UrlDecode(returnUrl));
            }
            else
            {
                return RedirectToAction(DashBoardAction, DashBoardControllerName);
            }
        }
        #endregion
    }

}
