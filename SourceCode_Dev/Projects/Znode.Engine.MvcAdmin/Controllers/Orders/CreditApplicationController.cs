﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.Api.Models.Enum;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Controllers.Orders
{
    public class CreditApplicationController : BaseController
    {
         #region Private Variables
        private readonly IPRFTCreditApplicationAgent _creditApplicationAgent;
        #endregion

        #region Public Constructors

        /// <summary>
        /// Constructor for the ServiceRequest
        /// </summary>
        public CreditApplicationController()
        {
            _creditApplicationAgent = new PRFTCreditApplicationAgent();
        }

        #endregion

        #region Action Methods

        /// <summary>
        /// This function will return the list of all the CaseRequest.
        /// </summary>
        /// <returns>ActionResult view of CaseRequest list</returns>
        public ActionResult List([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model)
        {
            ManageCreditApprovedFilter(model);

            PRFTCreditApplicationListViewModel list = _creditApplicationAgent.GetCreditApplications(model.Filters, model.SortCollection, model.Page, model.RecordPerPage);

            ReportModel gridModel = FilterHelpers.GetDynamicGridModel(model, list.CreditApplicationsList, Convert.ToInt32(ListType.PRFTCreditApplication));
            gridModel.TotalRecordCount = list.TotalResults;
            return ActionView(gridModel);
        }

        /// <summary>
        /// Handles Manage Credit flag
        /// </summary>
        /// <param name="model"></param>
        private void ManageCreditApprovedFilter(FilterCollectionDataModel model)
        {
            int iscreditFlagIndex = -1;
            bool isCreditApprovedFlagSetToDefault = false;
            TempData["SelectedCreditApprovedOption"] = "-1";
            if (model.Filters != null && model.Filters.Count > 0)
            {
                foreach (var item in model.Filters)
                {
                    iscreditFlagIndex++;
                    if (Equals(item.Item1.ToLower(), "iscreditapproved"))
                    {
                        TempData["SelectedCreditApprovedOption"] = item.Item3;
                        if (item.Item3.Contains("-1"))
                        {
                            isCreditApprovedFlagSetToDefault = true;
                            break;
                        }
                    }
                }
            }
            if (isCreditApprovedFlagSetToDefault)
            {
                model.Filters.RemoveAt(iscreditFlagIndex);
            }
        }

        /// <summary>
        /// To update Credit Application
        /// </summary>
        /// <param name="model">CreditApplicationViewModel model</param>
        /// <returns>returns to list page if true</returns>
        [HttpPost]
        public ActionResult Edit(PRFTCreditApplicationViewModel model)
        {
            if (ModelState.IsValid)
            {
                PRFTCreditApplicationViewModel currentModel = _creditApplicationAgent.GetCreditApplication(model.CreditApplicationID);
                currentModel.CreditDays = model.CreditDays;
                currentModel.IsCreditApproved = model.IsCreditApproved;
                currentModel.CreditApprovedDate = DateTime.Now;

                bool status = _creditApplicationAgent.UpdateCreditApplication(currentModel);
                TempData[MvcAdminConstants.Notifications] = status ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.SaveErrorMessage, NotificationType.error);
                if (!status)
                {
                    return View(MvcAdminConstants.ManageView, currentModel);
                }
            }
            return RedirectToAction(MvcAdminConstants.ListView);
        }

        /// <summary>
        /// To manage case Request
        /// </summary>
        /// <param name="id">int? id case Request id</param>
        /// <returns>returns case Request manage page</returns>
        [HttpGet]
        public ActionResult Manage(int? id)
        {
            if (!Equals(id, null))
            {
                PRFTCreditApplicationViewModel model = _creditApplicationAgent.GetCreditApplication(id.Value);
                return View(MvcAdminConstants.ManageView, model);
            }
            return View(MvcAdminConstants.ManageView, new PRFTCreditApplicationViewModel());
        }

        #endregion
	}
}