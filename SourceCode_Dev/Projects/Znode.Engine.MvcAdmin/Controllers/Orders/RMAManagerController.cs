﻿using Newtonsoft.Json;
using Resources;
using System;
using System.Web.Mvc;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Enum;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.Agents.Agents;
using Znode.Engine.MvcAdmin.Agents.IAgents;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Controllers.Orders
{
    public class RMAManagerController : BaseController
    {
        #region Private Variables
        #region ReadOnly variables
        private readonly IRMARequestAgent _rmaRequestAgent;
        private readonly IPortalAgent _portalAgent;
        private readonly int startIndex = 1;
        #endregion

        #region Constant Variables
        private const string AppendFlag = "append";
        private const string ViewFlag = "view";
        private const string VoidStatus = "void";
        private const string ReturndedRefunded = "returned/refunded";
        private const string RmaListAction = "RMAList";
        private const string EditViewRmaItemsView = "EditViewRMAItemsView";
        private const string EditMode = "Edit";
        private const string OrderAction = "Orders";
        private const string EditViewRMAItemsView = "EditViewRMAItemsView";
        private const int voidStatus = 4;
        private const int returndedRefunded = 3;
        private const string rMAList = "RMAList";
        #endregion

        private string GiftCardAppendUrl = "/GiftCard/Create?mode=RMA&item={0}&items={1}&qty={2}&orderid={3}&flag=append&amount={4}";
        private string GiftCardUrl = "/GiftCard/Create?mode=RMA&item={0}&items={1}&qty={2}&orderid={3}&amount={4}";
        private string rmaMode = "rma";
        #endregion

        #region Constructor
        /// <summary>
        /// Public Constructor for RMAManagerController 
        /// </summary>
        public RMAManagerController()
        {
            _rmaRequestAgent = new RMARequestAgent();
            _portalAgent = new PortalAgent();
        }
        #endregion

        #region Public Action Methods
        /// <summary>
        /// Gets list of RMA Requests with paging values.
        /// </summary>
        /// <param name="model">Filter collection data model containging sortings and filters.</param>
        /// <returns>List of RMA requests.</returns>
        public ActionResult RMAList([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model)
        {
            RMARequestListViewModel rmaRequestListViewModel = _rmaRequestAgent.GetRMARequests(model.Filters, model.SortCollection, model.Page, model.RecordPerPage);
            ReportModel gridModel = FilterHelpers.GetDynamicGridModel(model, rmaRequestListViewModel.RMARequestList, Convert.ToInt32(ListType.vw_ZNodeSearchRMA));
            //Get Request status list according to catalog id.
            rmaRequestListViewModel.RequestStatusList = _rmaRequestAgent.GetRequstStatusList();
            rmaRequestListViewModel.PortalList = _portalAgent.GetPortals().Portals;
            FilterHelpers.CreateDropdown(ref gridModel, rmaRequestListViewModel.RequestStatusList, model.Filters, MvcAdminConstants.RequestStatusID, MvcAdminConstants.LabelName, MvcAdminConstants.LabelStatus);
            FilterHelpers.CreateDropdown(ref gridModel, rmaRequestListViewModel.PortalList, model.Filters, MvcAdminConstants.PortalId, MvcAdminConstants.StoreName, MvcAdminConstants.StoreNameDropDown);
            gridModel.TotalRecordCount = rmaRequestListViewModel.TotalResults;
            return ActionView(gridModel);
        }

        /// <summary>
        /// Gets the view to edit an RMA Request and shows the RMA Request items for that RMA Request.
        /// </summary>
        /// <param name="OrderId">Order Id for which RMA Request items are shown.</param>
        /// <param name="requestDate">Date when RMA Request was made.</param>
        /// <param name="RMARequestID">RMA Request ID which is being edited.</param>
        /// <param name="pageIndex">Current page index of the RMA request item list.</param>
        /// <param name="requestNumber">Request number created while creating RMA request.</param>
        /// <param name="flag">Flag value for edit, view or append.</param>
        /// <returns>View to edit RMA Request.</returns>
        [HttpGet]
        public ActionResult Edit(int OrderId, string requestDate, int RMARequestID, int pageIndex = 1, string requestNumber = "", string flag = "")
        {
            RMARequestViewModel rmarequest = _rmaRequestAgent.GetRMARequest(RMARequestID);
            if (!flag.Equals(AppendFlag, StringComparison.InvariantCultureIgnoreCase))
            {
                if (rmarequest.RequestStatusId.Equals(Convert.ToInt32(RequestStatusModel.RequestStatus.Void)) || rmarequest.RequestStatusId.Equals(Convert.ToInt32(RequestStatusModel.RequestStatus.Returned_Refundable)))
                {
                    string status = string.Empty;
                    switch (rmarequest.RequestStatusId)
                    {
                        case voidStatus:
                            status = VoidStatus;
                            break;
                        case returndedRefunded:
                            status = ReturndedRefunded;
                            break;
                    }
                    TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(string.Format(ZnodeResources.ErrorCannotEditRmaRequest, status), Znode.Engine.MvcAdmin.Models.NotificationType.error);
                    return RedirectToAction(RmaListAction);
                }
            }
            flag = string.IsNullOrEmpty(flag) ? MvcAdminConstants.EmptyFlag : flag;
            RMARequestItemListViewModel rmaItemList = _rmaRequestAgent.GetRMARequestListItem(OrderId, RMARequestID, flag, startIndex, int.MaxValue, rmaMode);
            rmaItemList.flag = flag;
            if (!rmaItemList.Equals(null) && !Equals(rmaItemList.RMARequestItems, null) && rmaItemList.RMARequestItems.Count > 0)
            {
                rmaItemList.OrderId = OrderId;
                rmaItemList.RequestDate = requestDate;
                rmaItemList.RequestNumber = requestNumber;
                rmaItemList.RMARequestId = RMARequestID;
                rmaItemList.Comments = rmarequest.Comments;
                rmaItemList.StatusId = rmarequest.RequestStatusId.GetValueOrDefault();
                _rmaRequestAgent.RMARequestGiftCardDetails(rmaItemList, RMARequestID);
                return View(EditViewRmaItemsView, rmaItemList);
            }
            else
            {
                TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.ErrorAllRMAItemsProcessed, Znode.Engine.MvcAdmin.Models.NotificationType.error);
                return RedirectToAction(rMAList);
            }
        }

        /// <summary>
        /// Shows the RMA request items for the RMA request.
        /// </summary>
        /// <param name="OrderId">Order Id for which RMA Request items are shown.</param>
        /// <param name="requestDate">Date when RMA Request was made.</param>
        /// <param name="RMARequestID">RMA Request ID which is being edited.</param>
        /// <param name="pageIndex">Current page index of the RMA request item list.</param>
        /// <param name="requestNumber">Request number created while creating RMA request.</param>
        /// <param name="flag">Flag value for edit, view or append.</param>
        /// <param name="customerName">Customer's name.</param>
        /// <returns>View to edit RMA Request.</returns>
        [HttpGet]
        public ActionResult View(int OrderId, string requestDate, int RMARequestID, int pageIndex = 1, string requestNumber = "", int portalId = 0, string customerName="")
        {
            RMARequestItemListViewModel rmaItemList = new RMARequestItemListViewModel();
            RMARequestViewModel rmarequest = _rmaRequestAgent.GetRMARequest(RMARequestID);
            
            rmaItemList = _rmaRequestAgent.GetRMARequestListItem(OrderId, RMARequestID, ViewFlag, startIndex, int.MaxValue, rmaMode);
            rmaItemList.OrderId = OrderId;
            rmaItemList.RequestDate = requestDate;
            rmaItemList.RequestNumber = rmarequest.RequestNumber;
            rmaItemList.RMARequestId = RMARequestID;
            rmaItemList.Comments = rmarequest.Comments;
            _rmaRequestAgent.RMARequestGiftCardDetails(rmaItemList, RMARequestID);
            PortalViewModel portal = _portalAgent.GetPortalByPortalId(portalId);
            rmaItemList.StoreName = portal.StoreName;
            rmaItemList.SalesPhoneNumber = portal.SalesPhoneNumber;
            rmaItemList.CustomerName = customerName;
            rmaItemList.StatusId = rmarequest.RequestStatusId.GetValueOrDefault();
            rmaItemList.flag = ViewFlag;
            return View(EditViewRMAItemsView, rmaItemList);
        }

        /// <summary>
        /// Voids an RMA Request.
        /// </summary>
        /// <param name="rmaReqeustListItemModel">List of RMA request items.</param>
        /// <param name="command">Status value if the request is being made void.</param>
        /// <returns>RMA list view with void status of RMA request.</returns>
        [HttpPost]
        public ActionResult Edit(RMARequestItemListViewModel rmaReqeustListItemModel, string command = "")
        {
            if (command.ToLower().Equals(VoidStatus))
            {
                RMARequestViewModel rmarequest = _rmaRequestAgent.GetRMARequest(rmaReqeustListItemModel.RMARequestId);
                rmarequest.Comments = rmaReqeustListItemModel.Comments;
                bool isRequestUpdated = _rmaRequestAgent.UpdateRMARequest(rmaReqeustListItemModel.RMARequestId, rmarequest, command);

                string message = string.Empty;
                bool isFadeOut = CheckIsFadeOut();

                if (isRequestUpdated)
                {
                    TempData[MvcAdminConstants.Notifications] = ZnodeResources.SuccessRMARequestMadeVoid;
                    return RedirectToAction(RmaListAction);
                }
                TempData[MvcAdminConstants.Notifications] = ZnodeResources.ErrorRMARequestMadeVoid;
            }
            return View(EditViewRmaItemsView, rmaReqeustListItemModel);
        }

        /// <summary>
        /// Deletes unused RMA Requests.
        /// </summary>
        /// <param name="id">ID of the RMA request to be deleted.</param>
        /// <returns>JSON result containing status of deletion, status message and isfadeout value.</returns>
        public ActionResult Delete(int id)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();

            RMARequestViewModel rmarequest = _rmaRequestAgent.GetRMARequest(id);

            if (!id.Equals(0))
            {
                if (Equals(rmarequest, null) && rmarequest.RequestStatusId.Equals(1))
                {
                    //Deletes an rma request. 
                    status = _rmaRequestAgent.DeleteRMARequest(id);
                    message = status ? ZnodeResources.DeleteMessage : ZnodeResources.ErrorDeletingRMARequest;
                }
            }
            message = ZnodeResources.ErrorCannotDeleteRMARequest;
            return Json(new { sucess = status, message = message, id = id, isFadeOut = isFadeOut });
        }

        /// <summary>
        /// Redirects to issue gift card page.
        /// </summary>
        /// <param name="requestModel">Request model containing the data required to issue gift card for the RMA request.</param>
        /// <returns>Returns JSON result containing success status, status message, url to be redirected and isFadeOut value.</returns>
        public JsonResult IssueGiftCard(CreateRequestViewModel requestModel = null)
        {
            string message = string.Empty;
            string url = string.Empty;
            bool isRequestUpdated = false;
            bool isFadeOut = CheckIsFadeOut();
            if (requestModel.Total.Equals(0.0m))
            {
                message = ZnodeResources.ErrorSelectedPriceGreatedThanZero;
                return Json(new { sucess = isRequestUpdated, message = message, url = url, isFadeOut = isFadeOut });
            }
            RMARequestViewModel rmarequest = _rmaRequestAgent.GetRMARequest(requestModel.RMARequestId);
            rmarequest.Comments = requestModel.Comments;
            isRequestUpdated = _rmaRequestAgent.UpdateRMARequest(requestModel.RMARequestId, rmarequest, EditMode);
            if (requestModel.Flag.ToLower().Equals(AppendFlag))
            {
                url = string.Format(GiftCardAppendUrl, requestModel.RMARequestId.ToString(), requestModel.OrderLineItems, requestModel.Quantities, requestModel.OrderId, requestModel.Total);
            }
            else
            {
                url = string.Format(GiftCardUrl, requestModel.RMARequestId.ToString(), requestModel.OrderLineItems, requestModel.Quantities, requestModel.OrderId, requestModel.Total);
            }
            return Json(new { sucess = isRequestUpdated, message = message, url = url, isFadeOut = isFadeOut });
        }

        /// <summary>
        /// Gets converted currency values for the provided decimal values.
        /// </summary>
        /// <param name="subTotal">Subtotal of the RMA Items.</param>
        /// <param name="tax">Tax on RMA Items.</param>
        /// <param name="total">Total on RMA Items.</param>
        /// <returns>JSON result containing converted currency values.</returns>
        public JsonResult GetConvertedCurrencyValues(decimal decimalValue)
        {
            string currencyValue = MvcAdminUnits.GetCurrencyValue(decimalValue);

            return Json(new { _currencyValue = currencyValue });
        }


        #region Create RMA
        /// <summary>
        /// To show Create RMA page
        /// </summary>
        /// <param name="OrderId">int OrderId</param>
        /// <param name="OrderStatus">string OrderStatus</param>
        /// <param name="OrderDate">string OrderDate</param>
        /// <returns>returns view Create RMA page</returns>
        [HttpGet]
        public ActionResult CreateRMA(int OrderId, string OrderStatus, string OrderDate)
        {
            bool isEnable = _rmaRequestAgent.IsRMAEnable(OrderId, OrderStatus, OrderDate);
            if (isEnable)
            {
                RMARequestItemListViewModel rmaItemList = new RMARequestItemListViewModel();
                rmaItemList = _rmaRequestAgent.GetRMARequestListItem(OrderId, 0, MvcAdminConstants.EmptyFlag, startIndex, int.MaxValue);
                rmaItemList.OrderId = OrderId;
                rmaItemList.RequestDate = HelperMethods.ViewDateFormat(DateTime.Now);
                rmaItemList.RequestNumber = _rmaRequestAgent.GenerateRequestNumber();
                rmaItemList.ReasonForReturnItems = _rmaRequestAgent.BindReasonForReturn();
                rmaItemList.flag = MvcAdminConstants.EmptyFlag;
                return View(EditViewRmaItemsView, rmaItemList);
            }
            else
            {
                TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.DisabledRMAMessage, NotificationType.error);
                return RedirectToAction(MvcAdminConstants.ListView, OrderAction);
            }
        }

        /// <summary>
        ///  To save RMA to database
        /// </summary>
        /// <param name="requestModel">CreateRequestViewModel requestModel</param>
        /// <returns>returns true/false</returns>
        [HttpPost]
        public JsonResult CreateRMA(CreateRequestViewModel requestModel = null)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();
            if (!Equals(requestModel, null))
            {
                status = _rmaRequestAgent.CreateRMARequest(requestModel, out message);
                message = status ? ZnodeResources.SaveMessage : ZnodeResources.SaveErrorMessage;
            }
            else
            {
                status = false;
                message = ZnodeResources.ErrorSelectRMAItem;
            }
            return Json(new { sucess = status, message = message, id = requestModel.RMARequestId, isFadeOut = isFadeOut, });

        }
        #endregion
        #endregion
    }
}