﻿using Resources;
using System;
using System.Web.Mvc;
using Znode.Engine.Api.Models.Enum;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Controllers.Orders
{
    public class SuppliersController : BaseController
    {
        #region Private Variables
        private readonly ISupplierAgent _supplierAgent;
        #endregion

        #region Constructor
        public SuppliersController()
        {
            _supplierAgent = new SuppliersAgent();
        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Get list of Supplier.
        /// </summary>
        /// <param name="model">Filter Collection Data Model</param>
        /// <returns>View of Supplier list page.</returns>
        public ActionResult List([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model)
        {
            SupplierListViewModel supplierList = _supplierAgent.GetSuppliers(model.Filters, model.SortCollection, model.Page, model.RecordPerPage);
            var gridModel = FilterHelpers.GetDynamicGridModel(model, supplierList.Suppliers, Convert.ToInt32(ListType.SuppliersList));
            FilterHelpers.CreateDropdown(ref gridModel, _supplierAgent.GetStatusList(), model.Filters, MvcAdminConstants.Value, MvcAdminConstants.Text, ZnodeResources.ColumnStatus);
            gridModel.TotalRecordCount = supplierList.TotalResults;
            return ActionView(gridModel);
        }

        /// <summary>
        /// Create a new supplier.
        /// </summary>
        /// <returns>View of create supplier.</returns>
        [HttpGet]
        public ActionResult Create()
        {
            SupplierViewModel model = new SupplierViewModel();
            model.IsActive = true;
            model.SupplierTypeList = _supplierAgent.GetSupplierTypeList();
            if(model.SupplierTypeList.Count.Equals(0))
            {
                TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.AddSupplierErrorMessage, NotificationType.error);
                return RedirectToAction(MvcAdminConstants.ListView);
            }
            return View(MvcAdminConstants.CreateEditView, model);
        }

        /// <summary>
        /// Create a new supplier.
        /// </summary>
        /// <param name="model">model to create.</param>
        /// <returns>Returns view of List page</returns>
        [HttpPost]
        public ActionResult Create(SupplierViewModel model)
        {
            if (ModelState.IsValid)
            {
                bool status = _supplierAgent.CreateSupplier(model);
                TempData[MvcAdminConstants.Notifications] = status ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.SaveErrorMessage, NotificationType.error);
                return RedirectToAction(MvcAdminConstants.ListView);
            }
            model.SupplierTypeList = _supplierAgent.GetSupplierTypeList();
            return View(MvcAdminConstants.CreateEditView, model);
        }

        /// <summary>
        /// Edit an existing supplier.
        /// </summary>
        /// <param name="id">Id of the supplier.</param>
        /// <returns>Returns view of Edit Supplier.</returns>
        public ActionResult Edit(int id)
        {
            if (id > 0)
            {
                SupplierViewModel model = new SupplierViewModel();
                model = _supplierAgent.GetSupplier(id);
                model.SupplierTypeList = _supplierAgent.GetSupplierTypeList(true);
                return View(MvcAdminConstants.CreateEditView, model);
            }
            return RedirectToAction(MvcAdminConstants.ListView);
        }

        /// <summary>
        /// Edit an existing supplier.
        /// </summary>
        /// <param name="model">model to update.</param>
        /// <returns>Returns view of List page</returns>
        [HttpPost]
        public ActionResult Edit(SupplierViewModel model)
        {
            if (ModelState.IsValid)
            {
                if ((!Equals(model, null) && (model.SupplierId > 0)))
                {
                    TempData[MvcAdminConstants.Notifications] = _supplierAgent.UpdateSupplier(model) ? GenerateNotificationMessages(ZnodeResources.UpdateMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.UpdateErrorMessage, NotificationType.error);
                    return RedirectToAction(MvcAdminConstants.ListView);
                }
                else
                {
                    return RedirectToAction(MvcAdminConstants.ListView);
                }
            }
            model.SupplierTypeList = _supplierAgent.GetSupplierTypeList(true);
            return View(MvcAdminConstants.CreateEditView, model);
        }

        /// <summary>
        /// Delete an existing Supplier.
        /// </summary>
        /// <param name="id">Id of the supplier.</param>
        /// <returns>Returns the list page.</returns>
        [HttpPost]
        public JsonResult Delete(int? id)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();

            if (id.HasValue && id.Value > 0)
            {
                status = _supplierAgent.DeleteSupplier(id.Value, out message);
                message = status ? ZnodeResources.DeleteMessage : message;
            }

            return Json(new { sucess = status, message = message, id = id, isFadeOut = isFadeOut });
        }
        #endregion
    }
}