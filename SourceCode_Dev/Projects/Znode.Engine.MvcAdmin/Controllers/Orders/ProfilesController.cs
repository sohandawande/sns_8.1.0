﻿using Resources;
using System;
using System.Web.Mvc;
using Znode.Engine.Api.Models.Enum;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Controllers.Orders
{
    public class ProfilesController : BaseController
    {
        #region Private Variables

        private readonly IProfilesAgent _profileAgent;

        #endregion

        #region Constructor
        public ProfilesController()
        {
            _profileAgent = new ProfilesAgent();
        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Gets list of all profiles
        /// </summary>
        /// <param name="model">Model of type filtercollectiondata model</param>
        /// <returns>Returns view.</returns>
        public ActionResult List([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model)
        {
            ProfileListViewModel profileList = _profileAgent.GetListOfProfiles(model.Filters, model.SortCollection, model.Page, model.RecordPerPage);
            var gridModel = FilterHelpers.GetDynamicGridModel(model, profileList.Profiles, Convert.ToInt32(ListType.ProfilesList));
            gridModel.TotalRecordCount = profileList.TotalResults;
            return ActionView(MvcAdminConstants.ListView, gridModel);
        }

        /// <summary>
        /// Creates a new profile.
        /// </summary>
        /// <returns>Returns View.</returns>
        public ActionResult Create()
        {
            ProfileViewModel model = new ProfileViewModel();
            model.ProductPrice = true;
            return View(MvcAdminConstants.CreateEditView, model);
        }

        /// <summary>
        /// Creates a new profile.
        /// </summary>
        /// <param name="model">Model of type ProfileView model</param>
        /// <returns>Returns View.</returns>
        [HttpPost]
        public ActionResult Create(ProfileViewModel model)
        {
            if (!Equals(model, null) && ModelState.IsValid)
            {
                int profileId = 0;
                TempData[MvcAdminConstants.Notifications] = _profileAgent.CreateProfile(model, out profileId) ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.SaveErrorMessage, NotificationType.error);
                return RedirectToAction(MvcAdminConstants.ListView);
            }
            return RedirectToAction(MvcAdminConstants.CreateView);

        }

        /// <summary>
        /// Edits an existing profile.
        /// </summary>
        /// <param name="id">Id of profile</param>
        /// <returns>Returns View.</returns>
        public ActionResult Edit(int id)
        {
            ProfileViewModel model = id > 0 ? _profileAgent.GetProfileByProfileId(id) : new ProfileViewModel();
            return View(MvcAdminConstants.CreateEditView, model);
        }

        /// <summary>
        /// Edits an existing profile.
        /// </summary>
        /// <param name="model">Modle of type ProfileView model</param>
        /// <returns>Returns View.</returns>
        [HttpPost]
        public ActionResult Edit(ProfileViewModel model)
        {
            if (!Equals(model, null) && ModelState.IsValid)
            {
                TempData[MvcAdminConstants.Notifications] = _profileAgent.UpdateProfile(model) ? GenerateNotificationMessages(ZnodeResources.UpdateMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.UpdateErrorMessage, NotificationType.error);
                return RedirectToAction(MvcAdminConstants.ListView);
            }
            return RedirectToAction(MvcAdminConstants.EditAction, new { id = model.ProfileId });
        }

        /// <summary>
        /// Deletes profile.
        /// </summary>
        /// <param name="id">The id of profile</param>
        /// <returns>Returns json result.</returns>
        public JsonResult Delete(int id)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();
            if (id > 0)
            {
                status = _profileAgent.DeleteProfile(id);
                message = status ? ZnodeResources.DeleteMessage : ZnodeResources.ErrorDeleteProfile;
            }
            return Json(new { sucess = status, message = message, id = id, isFadeOut = isFadeOut });
        }

        #endregion

    }
}