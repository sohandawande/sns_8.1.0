﻿using Resources;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Enum;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Models;
using Znode.Engine.MvcAdmin.ViewModels;
using Znode.Libraries.Helpers.Extensions;

namespace Znode.Engine.MvcAdmin.Controllers.Orders
{
    public class OrdersController : BaseController
    {
        #region Private Variables

        #region Private Agent Variables
        private readonly IOrderAgent _orderAgent;
        private readonly ICustomerAgent _customerAgent;
        private readonly IPortalAgent _portalAgent;
        private readonly ICatalogAgent _catalogAgent;
        private readonly IAccountAgent _accountAgent;
        private readonly IStoreAdminAgent _storeAdminAgent;
        private readonly ICartAgent _cartAgent;
        private readonly IProductAgent _productAgent;
        private readonly IShippingAgent _shippingAgent;
        private readonly IPaymentAgent _paymentAgent;
        private readonly ICheckoutAgent _checkoutAgent;
        private readonly IVendorAccountAgent _vendorAccountAgent;
        #endregion

        #region Private Constant Variables
        private const string constRefund = "Refund";
        private const string constVoid = "Void";
        private const string refundAction = "RefundPayment";
        public const string manageAction = "Manage";
        public const string downloadOrderFileName = "Order.csv";
        public const string downloadOrderLineItemFileName = "OrderLine.csv";
        public const int recordsPerPage = 100;
        private const string DisplayShippingBillingAddressView = "DisplayShippingBillingAddress";
        private const string CustomerSearchListView = "CustomerSearchList";
        private const string CreateOrderView = "CreateOrder";
        private const string _CatalogSelectListView = "_CatalogSelectList";
        private const string BillingAddressModel_Name = "BillingAddressModel.Name";
        private const string ShippingAddressModel_Name = "ShippingAddressModel.Name";
        private const string refundCardNumberError = "RefundCardNumber";
        private const string securityCodeError = "SecurityCode";
        private const string delimiter = ",";
        private const string constOrdersPortalId = "OrdersPortalId";
        private const string constOrdersCatalogId = "OrdersCatalogId";
        private const string productListView = "_ProductList";
        private const string bundleView = "_Bundles";
        private const string productDetails = "_ProductDetails";
        private const string cartView = "Cart";
        private const string indexView = "Index";
        private const string changeAddressView = "AddNewCustomer";
        private const string checkout = "Checkout";
        private const string TotalView = "_TotalTable";
        private const string checkoutReceipt = "CheckoutReceipt";
        private const string startingOrderIdError = "StartingOrderId";
        private const string CreateAction = "Create";
        private const string CheckoutReceiptView = "CheckoutReceipt";

        #endregion

        #endregion

        #region Constructor
        public OrdersController()
        {
            _orderAgent = new OrderAgent();
            _portalAgent = new PortalAgent();
            _catalogAgent = new CatalogAgent();
            _accountAgent = new AccountAgent();
            _customerAgent = new CustomerAgent();
            _storeAdminAgent = new StoreAdminAgent();
            _cartAgent = new CartAgent();
            _productAgent = new ProductAgent();
            _shippingAgent = new ShippingAgent();
            _paymentAgent = new PaymentAgent();
            _checkoutAgent = new CheckoutAgent();
            _vendorAccountAgent = new VendorAccountAgent();
        }
        #endregion

        #region Public Methods

        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Gets order list
        /// </summary>
        /// <param name="model">Model of type FilterCollectionDataModel</param>
        /// <returns>Returns View</returns>
        public ActionResult List([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model, int? startingOrderId)
        {
            OrderViewModel orderModel = new OrderViewModel();
            model.Filters.Add(new FilterTuple(FilterKeys.Username, FilterOperators.Equals, string.Format("'{0}'", HttpContext.User.Identity.Name)));

            //PRFT Custom Code : Start
            if (CheckIsERPOrderFailFilters(model.Filters))
            {
                model.Filters.Add(new FilterTuple(FilterKeys.ExternalId, FilterOperators.Is, FilterKeys.Null));
            }
            this.RemoveFilters(model.Filters, "iserporderfailed");
            //PRFT Custom Code : End

            AdminOrderListViewModel list = _orderAgent.GetOrderList(model.Filters, model.SortCollection, model.Page, model.RecordPerPage);

            //PRFT Custom Code : Start
            TempData["isERPOrderFailed"] = false;
            if (CheckIsERPOrderFailOnModelFilters(model.Filters))
            {
                TempData["isERPOrderFailed"] = true;
            }
            //PRFT Custom Code : End

            if (Equals(list, null))
            {
                list = new AdminOrderListViewModel();
            }
            List<PortalViewModel> storeList = _storeAdminAgent.GetAllStores();
            List<OrderStateViewModel> orderStateList = _orderAgent.GetAllOrderStatusList();

            var gridModel = FilterHelpers.GetDynamicGridModel(model, list.OrderList, Convert.ToInt32(ListType.vw_ZNodeGetOrderDetails));
            FilterHelpers.CreateDropdown(ref gridModel, storeList, model.Filters, MvcAdminConstants.PortalId, MvcAdminConstants.StoreName, MvcAdminConstants.StoreNameDropDown);
            FilterHelpers.CreateDropdown(ref gridModel, orderStateList, model.Filters, MvcAdminConstants.OrderStateId, MvcAdminConstants.OrderStateName, MvcAdminConstants.OrderStateDropdown);
            gridModel.TotalRecordCount = list.TotalResults;
            orderModel.DynamicGrid = gridModel;
            if (!Equals(startingOrderId, null))
            {
                orderModel.StartingOrderId = startingOrderId;
            }
            return ActionView(MvcAdminConstants.ListView, orderModel);
        }

        /// <summary>
        /// Manages order and gets order details
        /// </summary>
        /// <param name="id">Order Id</param>
        /// <returns>Retund View</returns>
        public ActionResult Manage(int id)
        {
            OrderViewModel orderModel = _orderAgent.GetOrderDetails(id, null, null, null, null);

            if (Equals(orderModel, null))
            {
                return new EmptyResult();
            }
            _orderAgent.SetBillingShippingAddresses(orderModel);
            FilterCollectionDataModel model = new FilterCollectionDataModel();

            //Grid for orderLineItems
            var orderLineItemGridModel = FilterHelpers.GetDynamicGridModel(model, orderModel.OrderLineItemList.OrderLineItems, Convert.ToInt32(ListType.OrderLineItemList));
            orderLineItemGridModel.TotalRecordCount = orderModel.OrderLineItemList.OrderLineItems.Count;
            orderLineItemGridModel.RowPerPageCount = recordsPerPage;
            orderModel.OrderLineItemsGridModel = orderLineItemGridModel;

            //Grid for rmaRequests
            var rmaGridModel = FilterHelpers.GetDynamicGridModel(model, orderModel.rmaRequestList.RMARequestList, Convert.ToInt32(ListType.RmaRequestList_ManageOrder));
            rmaGridModel.TotalRecordCount = orderModel.OrderLineItemList.OrderLineItems.Count;
            rmaGridModel.RowPerPageCount = recordsPerPage;
            orderModel.RmaRequetsGridModel = rmaGridModel;
            return ActionView(manageAction, orderModel);
        }

        /// <summary>
        /// Downloads order or orderlineitems data on the basis of starting order Id.
        /// </summary>
        /// <param name="model">Model of type OrderView model</param>
        /// <param name="command">Command to identify which submit button was clicked</param>
        /// <returns>Returns downloaded file.</returns>
        [HttpPost]
        public ActionResult DownloadOrder(OrderViewModel model, string command)
        {
            DownloadHelper downloadHelper = new DownloadHelper();
            DataSet orderDownloadData = new DataSet();
            ModelState.Remove(refundCardNumberError);
            ModelState.Remove(securityCodeError);
            if (!Equals(model, null) && ModelState.IsValid)
            {
                if (Equals(command, ZnodeResources.ButtonDownloadOrdersToCSV))
                {
                    orderDownloadData = _orderAgent.DownloadOrderData(model.StartingOrderId.ToString());
                    if (orderDownloadData.Tables[0].Rows.Count > 0)
                    {
                        //below code downloads the file.                      
                        downloadHelper.Download(orderDownloadData, (Convert.ToInt32(FileTypes.CSV)).ToString(), Response, delimiter, downloadOrderFileName);
                    }
                    else
                    {
                        TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.OrderDownloadError, NotificationType.error);
                    }
                }
                else if (Equals(command, ZnodeResources.ButtonDownloadOrderlineItems))
                {
                    orderDownloadData = _orderAgent.DownloadOrderLineItemData(model.StartingOrderId.ToString());
                    if (orderDownloadData.Tables[0].Rows.Count > 0)
                    {
                        //below code downloads the file.                      
                        downloadHelper.Download(orderDownloadData, (Convert.ToInt32(FileTypes.CSV)).ToString(), Response, delimiter, downloadOrderLineItemFileName);
                    }
                    else
                    {
                        TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.OrderDownloadError, NotificationType.error);
                    }
                }
            }
            return RedirectToAction(MvcAdminConstants.ListView, new { startingOrderId = model.StartingOrderId });
        }

        /// <summary>
        /// To process payment on Payment API using customerProfileId & customerPaymentId
        /// </summary>
        /// <param name="paymentSettingId">paymentSettingId</param>
        /// <param name="customerProfileId">customerProfileId</param>
        /// <param name="customerPaymentId">customerPaymentId</param>
        /// <returns>returns payment response</returns>
        public JsonResult SubmitPayment(string paymentSettingId, string customerProfileId, string customerPaymentId)
        {
            int _paymentSettingId = Convert.ToInt32(paymentSettingId);
            string creditCardResponse = _checkoutAgent.ProcessCreditCardPayment(_paymentSettingId, customerProfileId, customerPaymentId);
            return Json(new { status = true, Data = creditCardResponse }, JsonRequestBehavior.AllowGet);
        }

        #region Order Status

        /// <summary>
        /// Gets Order status details.
        /// </summary>
        /// <param name="orderId">Order Id</param>
        /// <returns>Returns View</returns>
        public ActionResult OrderStatus(int orderId)
        {
            OrderViewModel orderModel = _orderAgent.GetOrderDetails(orderId);
            orderModel.OrderStatusList = _orderAgent.BindOrderStatus();
            return View(orderModel);
        }

        /// <summary>
        /// Posts the order status deatils for updating the order status.
        /// </summary>
        /// <param name="model">Mdoel of type OrderView model</param>
        /// <returns>Returns View</returns>
        [HttpPost]
        public ActionResult OrderStatus(OrderViewModel model)
        {
            if (!Equals(model, null))
            {
                string errorMessage = string.Empty;
                TempData[MvcAdminConstants.Notifications] = _orderAgent.UpdateOrderStatus(model, out errorMessage) ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success) : GenerateNotificationMessages(errorMessage, NotificationType.error);
            }
            return RedirectToAction(MvcAdminConstants.ListView);

        }

        #endregion

        #region Refund Payment

        /// <summary>
        /// Gets the refund payment details.
        /// </summary>
        /// <param name="orderId">Order Id</param>
        /// <returns>Returns View</returns>
        [HttpGet]
        public ActionResult RefundPayment(int orderId)
        {
            OrderViewModel orderModel = _orderAgent.GetOrderDetails(orderId);
            orderModel.MonthList = _orderAgent.ToMonthSelectListItems();
            orderModel.YearList = _orderAgent.ToYearSelectListItems();
            orderModel.RefundAmount = orderModel.Total;
            return View(orderModel);
        }

        /// <summary>
        /// Posts the refund details on the basis of command.
        /// </summary>
        /// <param name="model">Model of type OrderView model</param>
        /// <param name="command">Command to identify which submit button was clicked</param>
        /// <returns>Returns View</returns>
        [HttpPost]
        public ActionResult RefundPayment(OrderViewModel model, string command)
        {
            ModelState.Remove(startingOrderIdError);
            ModelState.Remove(securityCodeError);
            ModelState.Remove(refundCardNumberError);
            string statusMessage = string.Empty;
            if (Equals(command, ZnodeResources.ButtonRefund))
            {
                decimal refundAmount = 0.00M;
                if (model.RefundAmount.StartsWith("$"))
                {
                    decimal.TryParse(model.RefundAmount.Substring(1), out refundAmount);
                }
                else
                {
                    decimal.TryParse(model.RefundAmount, out refundAmount);
                }

                if (!Equals(model, null) && ModelState.IsValid)
                {
                    statusMessage = _checkoutAgent.CaptureVoidRefundPayment(model.CardTransactionId, model.OrderId, Paymentoptions.PaymentRefund, refundAmount);
                }
            }
            else if (Equals(command, ZnodeResources.ButtonVoid))
            {
                if (!Equals(model, null))
                {
                    statusMessage = _checkoutAgent.CaptureVoidRefundPayment(model.CardTransactionId, model.OrderId, Paymentoptions.PaymentVoid, null);
                }
            }

            TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(statusMessage, NotificationType.info);
            return RedirectToAction(refundAction, new { orderId = model.OrderId });
        }

        #endregion

        #region Create Order

        /// <summary>
        /// The index page for create order.
        /// </summary>
        /// <returns>Returns View.</returns>
        [HttpGet]
        public ActionResult Create()
        {
            OrderCreateViewModel viewModel = new OrderCreateViewModel();
            viewModel.Portals = _orderAgent.GetPortalSelectList();
            return ActionView(CreateOrderView, viewModel);
        }

        /// <summary>
        /// Gets the catalog list on the basis of selected portal id.
        /// </summary>
        /// <param name="portalId">int portal id</param>
        /// <returns>Returns View.</returns>       
        public ActionResult CatalogSelectList(int? portalId)
        {
            if (!Equals(portalId, null))
            {
                CatalogSelectListViewModel viewModel = _catalogAgent.GetCatalogSelectList(portalId.Value);
                _cartAgent.RemoveAllCartItems();
                return ActionView(_CatalogSelectListView, viewModel);
            }
            else
            {
                return ActionView(_CatalogSelectListView, new CatalogSelectListViewModel());
            }
        }

        /// <summary>
        /// Adds new customer.
        /// </summary>
        /// <param name="portalId">int portal Id</param>
        /// <returns>Returns View.</returns>      
        public ActionResult AddNewCustomer(int? portalId)
        {
            ShippingBillingAddressViewModel viewModel = new ShippingBillingAddressViewModel();
            Collection<CountryModel> countries = _accountAgent.GetCountries(portalId ?? (int)PortalId);
            viewModel.ShippingAddressModel.Countries = countries;
            viewModel.BillingAddressModel.Countries = countries;
            viewModel.PortalId = portalId;
            return View(viewModel);
        }

        /// <summary>
        /// To update address by account Id
        /// </summary>
        /// <param name="accountId">int? accountId user account Id</param>
        /// <returns>Returns View for edit mode</returns>
        public ActionResult ChangeAddress(int? accountId)
        {
            if (!Equals(accountId, null))
            {
                AccountViewModel account = _customerAgent.GetCustomerAccountById(accountId.Value);
                AddressListViewModel accountAddress = _customerAgent.GetCustomerAddressDetails(accountId.Value, new FilterCollection());
                ShippingBillingAddressViewModel viewModel = new ShippingBillingAddressViewModel();
                if (!Equals(accountAddress, null) && !Equals(accountAddress.AccountAddress, null))
                {
                    viewModel.BillingAddressModel = accountAddress.AccountAddress.Find(x => Equals(x.IsDefaultBilling, true));
                    viewModel.ShippingAddressModel = accountAddress.AccountAddress.Find(x => Equals(x.IsDefaultShipping, true));
                    viewModel.EmailAddress = account.EmailAddress;
                }
                Collection<CountryModel> countries = _accountAgent.GetCountries(viewModel.PortalId ?? (int)PortalId);
                viewModel.ShippingAddressModel.Countries = countries;
                viewModel.BillingAddressModel.Countries = countries;
                viewModel.BillingAddressModel.AccountId = accountId.Value;
                viewModel.ShippingAddressModel.AccountId = accountId.Value;

                return View(changeAddressView, viewModel);
            }
            else
            {
                return View(changeAddressView, new AddressListViewModel());
            }
        }

        /// <summary>
        /// Submits the details of new customer.
        /// </summary>
        /// <param name="viewModel">Model of type ShippingBillingAddressViewModel</param>
        /// <returns>Returns View.</returns>
        [HttpPost]
        public ActionResult AddNewCustomer(ShippingBillingAddressViewModel viewModel)
        {
            //PRFT Custom Code: Start
            if (string.IsNullOrEmpty(viewModel.BillingAddressModel.Name))
            {
                viewModel.BillingAddressModel.Name = ConfigurationManager.AppSettings["BillingDisplayName"];
            }
            if (string.IsNullOrEmpty(viewModel.ShippingAddressModel.Name))
            {
                viewModel.ShippingAddressModel.Name = ConfigurationManager.AppSettings["ShippingDisplayName"];
            }
            if (string.IsNullOrEmpty(viewModel.EmailAddress))
            {
                viewModel.EmailAddress = PortalAgent.CurrentPortal.CustomerServiceEmail;
            }
            //PRFT Custom Code: End
            if (viewModel.ShippingAddressModel.UseSameAsBillingAddress)
            {
                //PRFT Custom Code: Start                
                viewModel.BillingAddressModel.Name = viewModel.ShippingAddressModel.Name;
                //viewModel Custom Code: End
                ModelState.RemoveFor<ShippingBillingAddressViewModel>(x => x.ShippingAddressModel);
            }
            viewModel.PortalId = PortalId;
            ModelState.Remove(BillingAddressModel_Name);
            ModelState.Remove(ShippingAddressModel_Name);

            if (ModelState.IsValid)
            {
                if (viewModel.AccountId > 0)
                {

                    AddressListModel addressModel = _orderAgent.UpdateCustomerAddress(viewModel.AccountId, viewModel);
                    return Content("" + viewModel.AccountId);
                }
                else
                {
                    AddressListModel model = _orderAgent.AddNewCustomer(viewModel);
                    return Content("" + model.Addresses[0].AccountId);
                }
            }
            else
            {
                Collection<CountryModel> countries = _accountAgent.GetCountries(viewModel.PortalId ?? (int)PortalId);
                viewModel.ShippingAddressModel.Countries = countries;
                viewModel.BillingAddressModel.Countries = countries;
                return View(viewModel);
            }
        }

        /// <summary>
        /// Gets the customer information.
        /// </summary>
        /// <param name="accountId">int account id</param>
        /// <returns>Returns View.</returns>
        [HttpGet]
        public ActionResult GetCustomerInformation(int accountId)
        {
            //PRFT Custom Code: Start
            var accountModel = _customerAgent.GetCustomerAccountById(accountId);

            if (accountModel != null && accountModel.UserId != Guid.Empty)
            {
                return Content("<script>window.location = '" + System.Configuration.ConfigurationManager.AppSettings["DemoWebsiteUrl"] + "/account/adminuserlogin?aid=" + accountId + "';</script>");
            }
            //PRFT Custom Code: End

            AddressListViewModel accountAddress = _customerAgent.GetCustomerAddressDetails(accountId, new FilterCollection());

            ShippingBillingAddressViewModel viewModel = new ShippingBillingAddressViewModel();

            if (!Equals(accountAddress, null) && !Equals(accountAddress.AccountAddress, null))
            {
                viewModel.AccountId = accountId;
                viewModel.BillingAddressModel = accountAddress.AccountAddress.Find(x => Equals(x.IsDefaultBilling, true));
                viewModel.ShippingAddressModel = accountAddress.AccountAddress.Find(x => Equals(x.IsDefaultShipping, true));
                _checkoutAgent.SaveAccounts(viewModel);
                viewModel.BillingAddressModel.Email = viewModel.EmailAddress;
                _checkoutAgent.SaveShippingAddess(viewModel.ShippingAddressModel);
                _checkoutAgent.SaveBillingAddess(viewModel.BillingAddressModel, true);
            }

            return View(DisplayShippingBillingAddressView, viewModel);
        }

        /// <summary>
        /// Gets the list of customers.
        /// </summary>
        /// <param name="model">Model of type FilterCollectionDataModel</param>
        /// <returns>Returns View.</returns>     
        public ActionResult CustomerList([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model)
        {
            CustomerListViewModel storeAdmins = _customerAgent.GetCustomerList(model.Filters, model.SortCollection, model.Page, model.RecordPerPage);

            ReportModel gridModel = FilterHelpers.GetDynamicGridModel(model, storeAdmins.Customers, Convert.ToInt32(ListType.OrderCustomerSearchList));

            var stores = _storeAdminAgent.GetAllStores();
            var profiles = _storeAdminAgent.GetAllProfiles();

            gridModel.TotalRecordCount = storeAdmins.TotalResults;

            return View(CustomerSearchListView, gridModel);
        }

        /// <summary>
        /// Gets the list of products
        /// </summary>
        /// <param name="model">Model of type FilterCollectionDataModel</param>
        /// <returns>Returns View</returns>   
        public ActionResult ProductList([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model)
        {
            int portalId = GetIdValueFromCollection(model, FilterKeys.PortalId);
            int catalogId = GetIdValueFromCollection(model, FilterKeys.CatalogId);
            ProductListViewModel productList = new ProductListViewModel();
            if (!Equals(portalId, 0) && !Equals(catalogId, 0))
            {
                Session[constOrdersPortalId] = portalId;
                Session[constOrdersCatalogId] = catalogId;
            }
            model.Filters.Add(new FilterTuple(FilterKeys.PortalId, FilterOperators.Equals, !Equals(portalId, 0) ? portalId.ToString() : Session[constOrdersPortalId].ToString()));
            model.Filters.Add(new FilterTuple(FilterKeys.CatalogId, FilterOperators.Equals, !Equals(catalogId, 0) ? catalogId.ToString() : Session[constOrdersCatalogId].ToString()));

            productList = _orderAgent.SearchProducts(model.Filters, model.SortCollection, model.Page, model.RecordPerPage);

            var gridModel = FilterHelpers.GetDynamicGridModel(model, productList.Products, Convert.ToInt32(ListType.SearchProducts_CreateOrders));
            gridModel.TotalRecordCount = productList.TotalResults;
            productList.GridModel = gridModel;
            return ActionView(productListView, productList);
        }

        /// <summary>
        /// Gets the product details on the basis of id.
        /// </summary>
        /// <param name="id">int ProductId</param>
        /// <returns>Returns View</returns>      
        public ActionResult ProductDetails(int id)
        {
            if (id > 0)
            {
                var product = _productAgent.GetProductDetails(id);
                if (Equals(product, null))
                    throw new HttpException(404, ZnodeResources.CouldNotFindProductError);
                _productAgent.SetMessages(product);

                if (!string.IsNullOrEmpty(product.BundleItemsIds))
                {
                    BundleDetailsViewModel bundleProduct = _productAgent.GetCartModel(product);
                    product.ShowAddToCart = bundleProduct.Bundles.FirstOrDefault().ShowAddToCart;
                }

                var accountViewModel = _accountAgent.GetAccountViewModel();

                ViewBag.AccountId = !Equals(accountViewModel, null) ? accountViewModel.AccountId : 0;
                return PartialView(productDetails, product);
            }
            return RedirectToAction(productListView);
        }

        /// <summary>
        /// Adds the products to shopping cart.
        /// </summary>
        /// <param name="cartItem">Model of type CartItemViewModel</param>
        /// <returns>Returns cart View.</returns>
        [HttpPost]
        public ActionResult AddToCart(CartItemViewModel cartItem)
        {
            ProductViewModel product = _cartAgent.WishlistCheckInventory(cartItem);
            int? portalId = (int)Session[constOrdersPortalId];
            cartItem.PortalId = !Equals(portalId, null) ? portalId : null;
            if (!string.IsNullOrEmpty(product.InventoryMessage) && !product.ShowAddToCart)
            {
                TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(product.InventoryMessage, NotificationType.error);
                return null;
            }
            _cartAgent.Create(cartItem);
            CartViewModel cartViewModel = _cartAgent.GetCart();
            _cartAgent.SetMessages(cartViewModel);
            List<SelectListItem> shippingOptions = _shippingAgent.BindShippingList();
            if (!Equals(shippingOptions, null) && shippingOptions.Count > 0)
            {
                int defaultShippingId = Convert.ToInt32(shippingOptions[0].Value);
                cartViewModel = _checkoutAgent.GetShippingChargesById(defaultShippingId);
            }
            cartViewModel.ShippingServices = shippingOptions;
            cartViewModel.PaymentTypeList = _paymentAgent.GetActivePaymentOptionListItems();
            return PartialView(cartView, cartViewModel);
        }

        [HttpPost]
        public ActionResult AddAnotherProduct(CartItemViewModel cartItem)
        {
            var product = _cartAgent.WishlistCheckInventory(cartItem);
            if (product.ShowAddToCart)
            {
                int? portalId = (int)Session[constOrdersPortalId];
                cartItem.PortalId = !Equals(portalId, null) ? portalId : null;
                _cartAgent.Create(cartItem);
            }
            return null;
        }

        /// <summary>
        /// Removes the product from shopping cart.
        /// </summary>
        /// <param name="guid">string unique cart product value</param>
        /// <returns>Returns Shopping Cart View.</returns>
        [HttpPost]
        public ActionResult RemoveItem(string guid)
        {
            _cartAgent.RemoveItem(guid);
            CartViewModel cartViewModel = _cartAgent.GetCart();
            cartViewModel.ShippingServices = _shippingAgent.BindShippingList();
            cartViewModel.PaymentTypeList = _paymentAgent.GetActivePaymentOptionListItems();
            _cartAgent.SetMessages(cartViewModel);
            return PartialView(cartView, cartViewModel);
        }

        /// <summary>
        /// Updates the product quantity in the shopping cart.
        /// </summary>
        /// <param name="guid">string unique cart product value</param>
        /// <param name="quantity">int quantity</param>
        /// <returns>Returns View.</returns>
        [HttpPost]
        public ActionResult UpdateQuantity(string guid, int quantity)
        {
            _cartAgent.UpdateItem(guid, quantity);
            CartViewModel cartViewModel = _cartAgent.GetCart();
            cartViewModel.ShippingServices = _shippingAgent.BindShippingList();
            cartViewModel.PaymentTypeList = _paymentAgent.GetActivePaymentOptionListItems();
            _cartAgent.SetMessages(cartViewModel);
            return PartialView(cartView, cartViewModel);
        }

        /// <summary>
        /// Submits the order
        /// </summary>
        /// <param name="paymentOptionId">int selected payment option</param>
        /// <param name="shippingTypeId">int selected shipping option</param>
        /// <param name="additionalInstructions">string additional instructions if any</param>
        /// <returns>Returns View.</returns>
        [HttpPost]
        public ActionResult SubmitOrder(int paymentOptionId, int? shippingTypeId, string additionalInstructions, string purchaseOrderNumber)
        {
            PaymentOptionViewModel model = _paymentAgent.GetPaymentOption(paymentOptionId);
            int shippingId = 0;
            if (!Equals(shippingTypeId, null))
            {
                shippingId = shippingTypeId.Value;
            }
            OrderViewModel orderViewModel = _checkoutAgent.SubmitOrder(model, shippingId, purchaseOrderNumber, additionalInstructions);
            if (Equals(orderViewModel, null))
            {
                TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.ErrorUnablePlaceOrder, NotificationType.error);
                return RedirectToAction(CreateAction, orderViewModel);
            }
            if (!orderViewModel.HasError)
            {
                return View(checkoutReceipt, orderViewModel);
            }
            if (!Equals(orderViewModel, null) && orderViewModel.HasError)
            {
                if (string.IsNullOrEmpty(orderViewModel.ErrorMessage))
                {
                    orderViewModel.ErrorMessage = ZnodeResources.OrderSubmitError;
                }
                TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(orderViewModel.ErrorMessage, NotificationType.error);
                return RedirectToAction(CreateAction, orderViewModel);
            }
            ModelState.AddModelError("", ZnodeResources.FailedToPlaceOrder);
            return null;
        }

        /// <summary>
        /// Redirect to create order page after submit order
        /// </summary>
        /// <returns>returns create order page </returns>
        [HttpGet]
        public ActionResult SubmitOrder()
        {
            return RedirectToAction(CreateAction);
        }

        /// <summary>
        /// To CalculateShippingCharges
        /// </summary>
        /// <param name="shippingId">int shipping id</param>
        /// <returns>Returns View.</returns>
        [HttpGet]
        public ActionResult CalculateShippingCharges(int shippingId)
        {
            CartViewModel viewModel = _checkoutAgent.GetShippingChargesById(shippingId);
            return ActionView(TotalView, viewModel);
        }

        /// <summary>
        /// To apply & validate GiftCard
        /// </summary>
        /// <param name="number">string number</param>
        /// <returns>Returns Calculated Total view</returns>
        [HttpGet]
        public ActionResult ApplyGiftCard(string giftCardNumber)
        {
            CartViewModel cartViewModel = _checkoutAgent.ApplyGiftCard(giftCardNumber);
            return ActionView(TotalView, cartViewModel);
        }

        /// <summary>
        /// To apply & validate Coupon
        /// </summary>
        /// <param name="coupon">string coupon</param>
        /// <returns>Returns Calculated Total view</returns>
        [HttpGet]
        public ActionResult ApplyCoupon(string coupon)
        {
            CartViewModel cartViewModel = _cartAgent.ApplyCoupon(coupon);
            cartViewModel.ErrorMessage = string.IsNullOrEmpty(coupon) ? ZnodeResources.RequiredCouponCode : ZnodeResources.ErrorCouponCode;
            string totalView = RenderRazorViewToString(TotalView, cartViewModel);
            return Json(new
            {
                html = totalView,
                data = cartViewModel.Coupons
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetPaymentGatwayNameByPaymetOptionId(int? paymentOptionId)
        {
            string gatwayName = string.Empty;
            int? paymentProfileId = null;
            if (!Equals(paymentOptionId, null))
            {
                gatwayName = _paymentAgent.GetPaymentGatwayNameByPaymetOptionId(paymentOptionId, out paymentProfileId);
            }
            return Json(new
            {
                success = string.IsNullOrEmpty(gatwayName) ? false : true,
                name = gatwayName,
                paymentProfileId = paymentProfileId,
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Custom(string token, int paymentOptionId, int shippingId, string additionalNotes)
        {
            if (string.IsNullOrEmpty(token))
            {
                return RedirectToAction(CreateAction);
            }

            var orderViewModel = _checkoutAgent.SubmitCustomOrder(token, paymentOptionId, shippingId, additionalNotes);

            if (!orderViewModel.HasError)
            {
                return View(CheckoutReceiptView, orderViewModel);
            }

            ModelState.AddModelError(string.Empty, Resources.ZnodeResources.ErrorUnablePlaceOrder);

            return RedirectToAction(CreateAction);
        }

        [HttpGet]
        public ActionResult Capture(string CardTransactionId, string PaymentStatusName, int OrderID)
        {
            if (string.IsNullOrEmpty(CardTransactionId) && !PaymentStatusName.ToLower().Equals("cc_authorized"))
            {
                return RedirectToAction(MvcAdminConstants.ListView);
            }

            string statusMessage = _checkoutAgent.CaptureVoidRefundPayment(CardTransactionId, OrderID, Paymentoptions.PaymentCapture, null);
            TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(statusMessage, NotificationType.info); ;

            return RedirectToAction(MvcAdminConstants.ListView);
        }

        /// <summary>
        /// This method is used to place an order with the TwoCheckout method
        /// </summary>
        /// <param name="paymentOptionId">payment option id</param>
        /// <param name="token">string twocheckout token</param>
        /// <returns>response of the two checkout method</returns>
        [HttpGet]
        public JsonResult twoco(int? paymentOptionId, string token)
        {
            string actionUrl = _checkoutAgent.CallTwoCoPaymentMethod(paymentOptionId, token);
            return !string.IsNullOrEmpty(actionUrl) ? Json(new { status = true, Data = actionUrl }, JsonRequestBehavior.AllowGet) :
                Json(new { status = false, Data = string.Empty }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Product Attributes
        [HttpGet]
        public JsonResult GetAttributes(int? id, int? attributeId, string selectedIds, int? quantity, string selectedAddOnIds)
        {
            string selectedSku = string.Empty;
            string imagePath = string.Empty;
            string imageMediumPath = string.Empty;

            decimal productPrice;
            decimal finalProductPrice;
            var product = _productAgent.GetAttributes(id.Value, attributeId.GetValueOrDefault(0), selectedIds, quantity.GetValueOrDefault(1), out productPrice, out selectedSku, out imagePath, out imageMediumPath);

            if (!Equals(product.AddOns, null) && !string.IsNullOrEmpty(selectedAddOnIds))
            {
                product.AddOns = _productAgent.GetAddOnDetails(product, selectedAddOnIds, quantity.GetValueOrDefault(1),
                                                               productPrice, out finalProductPrice, selectedSku);
            }
            else
            {
                finalProductPrice = productPrice;
            }

            return Json(new
            {
                success = product.ShowAddToCart,
                message = product.InventoryMessage,
                data = new
                {
                    style = product.ShowAddToCart ? "success" : "error",
                    price = MvcAdminUnits.GetCurrencyValue(Convert.ToDecimal(finalProductPrice)),
                    unitPrice = MvcAdminUnits.GetCurrencyValue(Convert.ToDecimal(finalProductPrice / quantity)),
                    sku = selectedSku,
                    imagePath = imagePath,
                    imageMediumPath = imageMediumPath,
                    productName = product.Name,
                    addOnId = !Equals(product.AddOns, null) ? product.AddOns.Select(x => x.AddOnId) : null,
                    addOnMessage = !Equals(product.AddOns, null) ? product.AddOns.Select(x => x.OutOfStockMessage) : null,
                    isOutOfStock = !Equals(product.AddOns, null) ? product.AddOns.Select(x => x.IsOutOfStock) : null,
                    html = RenderHelper.PartialView(this, "_attributes", product),
                }
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetBundleDisplay(string ids)
        {
            if (string.IsNullOrEmpty(ids))
            {
                return new EmptyResult();
            }
            var viewModel = _productAgent.GetBundles(ids);
            if (Equals(viewModel, null) || viewModel.Count < 1)
            {
                return new EmptyResult();
            }
            return PartialView(bundleView, viewModel);
        }

        #endregion

        /// <summary>
        /// To Process PayPal Express Checkout Payment
        /// </summary>
        /// <param name="paymentOptionId">int paymentOptionId</param>
        /// <param name="returnUrl">string returnUrl</param>
        /// <param name="cancelUrl">string cancelUrl</param>
        /// <returns>returns PayPal response</returns>
        public JsonResult ProcessPayPalPayment(int paymentOptionId, string returnUrl, string cancelUrl)
        {
            string response = _checkoutAgent.ProcessPayPalCheckout(paymentOptionId, returnUrl, cancelUrl);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Private Method

        /// <summary>
        /// Gets the value of the string id passed.
        /// </summary>
        /// <param name="formModel">Model of type FilterCollectionDataModel</param>
        /// <param name="id">string id</param>
        /// <returns>Returns int id value.</returns>
        private static int GetIdValueFromCollection(FilterCollectionDataModel formModel, string id)
        {
            int idValue = 0;
            foreach (var x in formModel.Params)
            {
                if (x.Key.ToLower().Equals(id.ToLower()))
                {
                    return int.Parse(x.Value);
                }
            }
            return idValue;
        }
        #endregion

        #region PRFT Custom Methods
        [HttpGet]
        public ActionResult ReSubmitOrderToERP(int orderId)
        {
            string errorMessage = string.Empty;
            bool isSubmitted = _orderAgent.ResubmitOrderToERP(orderId, out errorMessage);
            TempData[MvcAdminConstants.Notifications] = isSubmitted ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success) : GenerateNotificationMessages(errorMessage, NotificationType.error);
            if (isSubmitted)
            {
                return RedirectToAction(MvcAdminConstants.ListView);
            }
            else
            {
                return RedirectToAction(MvcAdminConstants.ManageView, new { id = orderId });
            }
        }

        private bool CheckIsERPOrderFailOnModelFilters(FilterCollection filters)
        {
            if (!Equals(filters, null) && ((filters.Count > 0)))
            {
                foreach (var item in filters)
                {
                    if (Equals(item.Item1.ToLower(), "externalid"))
                    {
                        if (item.Item3 == "null")
                        {
                            return true;
                        }
                        else
                        { return false; }
                    }
                }
            }
            return false;
        }
        private bool CheckIsERPOrderFailFilters(FilterCollection filters)
        {
            if (!Equals(filters, null) && ((filters.Count > 0)))
            {
                foreach (var item in filters)
                {
                    if (Equals(item.Item1.ToLower(), "iserporderfailed"))
                    {
                        if (item.Item3.Contains("true"))
                        {
                            return true;
                        }
                        else
                        { return false; }
                    }
                }
            }
            return false;
        }
        private FilterCollection RemoveFilters(FilterCollection filters, string compair)
        {
            if (!Equals(filters, null) && ((filters.Count > 0)))
            {
                foreach (var item in filters)
                {
                    if (Equals(item.Item1.ToLower(), compair))
                    {
                        filters.Remove(item);
                        break;
                    }
                }
            }
            return filters;
        }
        #endregion
    }
}