﻿using Resources;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Enum;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Controllers
{
    /// <summary>
    /// Service Request Controller
    /// </summary>
    public class ServiceRequestController : BaseController
    {
        #region Private Variables
        private readonly ICaseRequestAgent _caseRequestAgent;
        private readonly IPortalAgent _portalAgent;
        private readonly IFranchiseAccountAgent _franchiseAccountAgent;

        private string addNoteView = "_AddNote";
        private string replyCustomerView = "ReplyCustomer";
        private readonly string portalId = "PortalId";
        private readonly string storeName = "StoreName";
        private readonly string pendingStatus = "1";
        #endregion

        #region Public Constructors

        /// <summary>
        /// Constructor for the ServiceRequest
        /// </summary>
        public ServiceRequestController()
        {
            _caseRequestAgent = new CaseRequestAgent();
            _portalAgent = new PortalAgent();
            _franchiseAccountAgent = new FranchiseAccountAgent();
        }

        #endregion

        #region Action Methods

        /// <summary>
        /// This function will return the list of all the CaseRequest.
        /// </summary>
        /// <returns>ActionResult view of CaseRequest list</returns>
        public ActionResult List([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model)
        {
            if (model.Params.Count.Equals(0))
            {
                model.Filters.Add(new FilterTuple(MvcAdminConstants.LabelCaseStatusId, FilterOperators.Equals, pendingStatus));
            }

            CaseRequestListViewModel list = _caseRequestAgent.GetCaseRequests(model.Filters, model.SortCollection, model.Page, model.RecordPerPage);

            ReportModel gridModel = FilterHelpers.GetDynamicGridModel(model, list.CaseRequestsList, Convert.ToInt32(ListType.vw_ZNodeSearchCase));
           
            #region Dynamic Dropdown Methods
            FilterHelpers.CreateDropdown(ref gridModel, _franchiseAccountAgent.GetPortalsList(), model.Filters, portalId, storeName, ZnodeResources.LabelStoreName);
            FilterHelpers.CreateDropdown(ref gridModel, _caseRequestAgent.GetCaseStatusList(), model.Filters, MvcAdminConstants.LabelCaseStatusId, MvcAdminConstants.LabelName, MvcAdminConstants.LabelCaseStatus);
            #endregion
            
            gridModel.TotalRecordCount = list.TotalResults;
            return ActionView(gridModel);
        }

        /// <summary>
        /// To create new service request
        /// </summary>
        /// <returns>returns add new page</returns>
        [HttpGet]
        public ActionResult Create()
        {
            CaseRequestViewModel model = new CaseRequestViewModel();
            _caseRequestAgent.BindPageDropdown(model);
            model.CaseOrigin = ZnodeResources.ColumnAdmin;
            return View(MvcAdminConstants.CreateEditView, model);
        }

        /// <summary>
        /// To save new service request
        /// </summary>
        /// <param name="model">CaseRequestViewModel model</param>
        /// <returns>returns true/false</returns>
        [HttpPost]
        public ActionResult Create(CaseRequestViewModel model)
        {
            if (ModelState.IsValid)
            {
                bool status = _caseRequestAgent.SaveCaseRequest(model);
                TempData[MvcAdminConstants.Notifications] = status ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.SaveErrorMessage, NotificationType.error);
                if (!status)
                {
                    return View(MvcAdminConstants.CreateEditView, model);
                }
            }
            return RedirectToAction(MvcAdminConstants.ListView);
        }

        /// <summary>
        /// To edit existing service request
        /// </summary>
        /// <param name="id">int? id</param>
        /// <returns>returns to list page if true</returns>
        [HttpGet]
        public ActionResult Edit(int? id)
        {
            if (!Equals(id, null) && id.Value > 0)
            {
                CaseRequestViewModel model = _caseRequestAgent.GetCaseRequest(id.Value);
                model.CaseOldStatusName = model.CaseStatusName;
                _caseRequestAgent.BindPageDropdown(model);
                model.CaseRequestId = id.Value;
                return View(MvcAdminConstants.CreateEditView, model);
            }
            return View(MvcAdminConstants.CreateEditView);
        }

        /// <summary>
        /// To save CaseRequest
        /// </summary>
        /// <param name="model">CaseRequestViewModel model</param>
        /// <returns>returns to list page if true</returns>
        [HttpPost]
        public ActionResult Edit(CaseRequestViewModel model)
        {
            if (ModelState.IsValid)
            {  
                bool status = _caseRequestAgent.UpdateCaseRequest(model);
                TempData[MvcAdminConstants.Notifications] = status ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.SaveErrorMessage, NotificationType.error);
                if (!status)
                {
                    return View(MvcAdminConstants.CreateEditView, model);
                }
            }
            return RedirectToAction(MvcAdminConstants.ListView);
        }

        /// <summary>
        /// To manage case Request
        /// </summary>
        /// <param name="id">int? id case Request id</param>
        /// <returns>returns case Request manage page</returns>
        [HttpGet]
        public ActionResult Manage(int? id)
        {
            if (!Equals(id, null))
            {
                CaseRequestViewModel model = _caseRequestAgent.GetCaseRequest(id.Value);
                return View(MvcAdminConstants.ManageView, model);
            }
            return View(MvcAdminConstants.ManageView, new CaseRequestViewModel());
        }

        /// <summary>
        /// To show ReplyCustomer view
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ReplyCustomer(int? id)
        {
            if (!Equals(id, null))
            {
                CaseRequestViewModel model = _caseRequestAgent.GetCaseRequest(id.Value);
                return View(replyCustomerView, model);
            }
            return View(replyCustomerView, new CaseRequestViewModel());
        }

        /// <summary>
        /// To ReplyCustomer
        /// </summary>
        /// <param name="model">CaseRequestViewModel model</param>
        /// <returns>returns to manage page</returns>
        [HttpPost]
        public ActionResult ReplyCustomer(CaseRequestViewModel model)
        {
            if (!Equals(model, null))
            {
                string notificationMessage = string.Empty;
               
                bool status = _caseRequestAgent.ReplyCustomer(model, out notificationMessage);
                TempData[MvcAdminConstants.Notifications] = status ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success) : GenerateNotificationMessages(notificationMessage, NotificationType.error);
                if (status)
                {
                    return RedirectToAction(MvcAdminConstants.ManageView, new { id = model.CaseRequestId });
                }
            }
            CaseRequestViewModel caseRequest = _caseRequestAgent.GetCaseRequest(model.CaseRequestId);
            return View(replyCustomerView, caseRequest);
        }

        /// <summary>
        /// To AddNote
        /// </summary>
        /// <param name="id">int? id</param>
        /// <returns>returns AddNote page</returns>
        [HttpGet]
        public ActionResult AddNote(int? id)
        {
            if (!Equals(id, null) && id.Value > 0)
            {
                NotesViewModel model = new NotesViewModel();
                model.CaseId = id.Value;
                return View(addNoteView, model);
            }
            return View(addNoteView);
        }

        /// <summary>
        /// To add note to case
        /// </summary>
        /// <param name="model">NotesViewModel model</param>
        /// <returns>returns to manage page</returns>
        [HttpPost]
        public ActionResult AddNote(NotesViewModel model)
        {
            if (ModelState.IsValid)
            {
                bool status = _caseRequestAgent.SaveCaseRequestsNotes(model);
                TempData[MvcAdminConstants.Notifications] = status ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.SaveErrorMessage, NotificationType.error);
                if (status)
                {
                    return RedirectToAction(MvcAdminConstants.ManageView, new { id = model.CaseId });
                }
            }
            return View(addNoteView, model);
        }

        #endregion

    }
}