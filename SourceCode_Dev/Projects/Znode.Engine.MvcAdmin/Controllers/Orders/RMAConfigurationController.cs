﻿using Resources;
using System;
using System.Web.Mvc;
using Znode.Engine.Api.Models.Enum;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Controllers.Orders
{
    public class RMAConfigurationController : BaseController
    {
        #region Private Variables

        #region Readonly Variables
        private readonly IRMAConfigurationAgent _rmaConfigurationAgent;
        private readonly IReasonForReturnAgent _reasonForReturnAgent; 
        #endregion

        #region Private Variables
        private const string IssueGiftCardPartialView = "_IssueGiftCard";
        private const string ReasonForReturnListPartialView = "_ReasonForReturn";
        private const string CreateReasonForReturnPartialView = "_CreateReasonForReturn";
        private const string EditRequestStatusPartialView = "_EditRequestStatus";
        private const string RequestStatusListPartialView = "_RequestStatuses";
        private const string EmailId = "EmailId"; 
        #endregion

        #endregion

        #region Constructor
        public RMAConfigurationController()
        {
            _rmaConfigurationAgent = new RMAConfigurationAgent();
            _reasonForReturnAgent = new ReasonForReturnAgent();
        }
        #endregion

        #region General Tab
        /// <summary>
        /// Open landing page of RMAConfiguration
        /// </summary>
        /// <returns>View of CreateEdit of RMAConfiguration</returns>
        [HttpGet]
        public ActionResult CreateEdit()
        {
            RMAConfigurationListViewModel rmaConfigurationList = _rmaConfigurationAgent.GetRMAConfigurations();
            if (!Equals(rmaConfigurationList, null) && rmaConfigurationList.RMAConfigurations.Count > 0)
            {
                RMAConfigurationViewModel rmaConfiguration = rmaConfigurationList.RMAConfigurations[0];
                return View(rmaConfiguration);
            }
            return View(new RMAConfigurationViewModel());
        }

        /// <summary>
        /// Post the data of RMAConfiguration
        /// </summary>
        /// <param name="rmaConfiguration">View Model of RMAConfiguration</param>
        /// <returns>View of CreateEdit of RMAConfiguration</returns>
        [HttpPost]
        public ActionResult CreateEdit(RMAConfigurationViewModel rmaConfiguration)
        {
            ModelState.Remove(EmailId);
            if (ModelState.IsValid)
            {
                if (rmaConfiguration.RMAConfigId > 0)
                {
                    var status = _rmaConfigurationAgent.UpdateRMAConfiguration(rmaConfiguration);
                    TempData[MvcAdminConstants.Notifications] = status ? GenerateNotificationMessages(ZnodeResources.UpdateMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.UpdateErrorMessage, NotificationType.error);
                    return View(rmaConfiguration);
                }
                else
                {
                    bool status = _rmaConfigurationAgent.CreateRMAConfiguration(rmaConfiguration);
                    TempData[MvcAdminConstants.Notifications] = status ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.SaveErrorMessage, NotificationType.error);
                    return View(rmaConfiguration);
                }
            }
            return View(rmaConfiguration);
        }
        #endregion

        #region Issue Gift Card Tab
        /// <summary>
        /// Open the IssueGiftCard PartialView
        /// </summary>
        /// <param name="rmaConfigId">rmaConfigId to get RMACOnfiguration Detail</param>
        /// <returns>View  of IssueGiftCard</returns>
        [HttpGet]
        public ActionResult IssueGiftCard(int rmaConfigId)
        {
            RMAConfigurationViewModel rmaConfiguration = null;
            if (rmaConfigId > 0)
            {
                rmaConfiguration = _rmaConfigurationAgent.GetRMAConfigurationsByRMAConfigId(rmaConfigId);
            }
            return ActionView(IssueGiftCardPartialView, rmaConfiguration);
        }

        /// <summary>
        /// Post the data about IssueGiftCard
        /// </summary>
        /// <param name="rmaConfiguration">View Model of RMAConfiguration</param>
        /// <returns>View of IssueGiftCard</returns>
        [HttpPost]
        public ActionResult IssueGiftCard(RMAConfigurationViewModel rmaConfiguration)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();
            ModelState.Remove(EmailId);
            if (ModelState.IsValid)
            {
                status  = _rmaConfigurationAgent.UpdateRMAConfiguration(rmaConfiguration);                
                message = status ? ZnodeResources.UpdateMessage : ZnodeResources.UpdateErrorMessage;
            }
            var ResponseData = RenderRazorViewToString(IssueGiftCardPartialView, rmaConfiguration);
            return Json(new { sucess = status, message = message, Response = ResponseData, isFadeOut = isFadeOut });
        }
        #endregion

        #region ReasonForReturn Tab
        /// <summary>
        /// Get the List of all Reasons For Return
        /// </summary>
        /// <param name="model">Filter collection model</param>
        /// <returns>View of list of Reasons For Return</returns>
        public ActionResult GetReasonForReturnList([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model)
        {
            ReasonForReturnListViewModel viewModel = new ReasonForReturnListViewModel();

            viewModel = _reasonForReturnAgent.GetReasonForReturnList(model.Filters, model.SortCollection, model.Page, model.RecordPerPage);
            if (!Equals(viewModel, null) && !Equals(viewModel.ReasonsForReturns, null))
            {
                var gridModel = FilterHelpers.GetDynamicGridModel(model, viewModel.ReasonsForReturns, Convert.ToInt32(ListType.ReasonForReturnList));
                gridModel.TotalRecordCount = viewModel.TotalResults;
                return ActionView(ReasonForReturnListPartialView, gridModel);
            }
            return ActionView(ReasonForReturnListPartialView, new ReportModel());
        }

        /// <summary>
        /// Create New Reason For Return
        /// </summary>
        /// <returns>Action View</returns>
        [HttpGet]
        public ActionResult CreateReasonForReturn()
        {
            ReasonForReturnViewModel model = new ReasonForReturnViewModel();
            model.IsEnabled = true;
            return ActionView(CreateReasonForReturnPartialView, model);
        }

        /// <summary>
        /// Create New Reason For Retrun
        /// </summary>
        /// <param name="reasonForReturn">View Model Of ReasonForReturn</param>
        /// <returns>Action View</returns>
        [HttpPost]
        public ActionResult CreateReasonForReturn(ReasonForReturnViewModel reasonForReturn)
        {
            string message = string.Empty;
            var status = _reasonForReturnAgent.CreateReaosnForReturn(reasonForReturn);
            TempData[MvcAdminConstants.Notifications] = status ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success) : GenerateNotificationMessages(message, NotificationType.error);
            if (status)
            {
                return RedirectToAction(MvcAdminConstants.CreateEditView);
            }
            return ActionView(CreateReasonForReturnPartialView);
        }


        /// <summary>
        /// Edit an existing Reason For Return
        /// </summary>
        /// <param name="reasonForReturnId">reasonForReturnId to get single ReasonForReturn Detail</param>
        /// <returns>Action View</returns>
        [HttpGet]
        public ActionResult EditReasonForReturn(int? reasonForReturnId)
        {
            ReasonForReturnViewModel model = new ReasonForReturnViewModel();
            if (!Equals(reasonForReturnId, null) && reasonForReturnId > 0)
            {
                model = _reasonForReturnAgent.GetReasonForReturn(reasonForReturnId.Value);
            }
            return ActionView(CreateReasonForReturnPartialView, model);
        }

        /// <summary>
        /// Edit an existing Reason For Return
        /// </summary>
        /// <param name="reasonForReturn">ReasonForReturn View Model</param>
        /// <returns>Action View</returns>
        [HttpPost]
        public ActionResult EditReasonForReturn(ReasonForReturnViewModel reasonForReturn)
        {
            string message = string.Empty;
            var status = _reasonForReturnAgent.UpdateReaosnForReturn(reasonForReturn);
            TempData[MvcAdminConstants.Notifications] = status ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success) : GenerateNotificationMessages(message, NotificationType.error);
            if (status)
            {
                return RedirectToAction(MvcAdminConstants.CreateEditView);
            }
            return ActionView(CreateReasonForReturnPartialView);
        }

        /// <summary>
        /// Delete an existing Reason For Return
        /// </summary>
        /// <param name="id">id to delete single ReasonForReturn</param>
        /// <returns>Return Json Result</returns>
        public JsonResult DeleteReasonForReturn(int? id)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();
            if (!Equals(null, id))
            {
                ReasonForReturnViewModel model = _reasonForReturnAgent.GetReasonForReturn(id.Value);
                if (model.ReasonForReturnId > MvcAdminConstants.ReasonforreturnId)
                {
                    status = _reasonForReturnAgent.DeleteReaosnForReturn(id.Value, out message);
                    message = status ? ZnodeResources.DeleteMessage : message;                    
                }
                else
                {
                    message = ZnodeResources.DeleteReturnForResultErrorMessage;
                }               
            }
            return Json(new { sucess = status, message = message, id = id, isFadeOut = isFadeOut });
        }

        #endregion

        #region Request Status Tab
        /// <summary>
        /// Get the List of all Request Statuses
        /// </summary>
        /// <param name="model">Filter collection model</param>
        /// <returns>View of list of Request Statuses</returns>
        public ActionResult GetRequestStatusList([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model)
        {
            RequestStatusListViewModel viewModel = new RequestStatusListViewModel();
            viewModel = _rmaConfigurationAgent.GetRequestStatusList(model.Filters, model.SortCollection, model.Page, model.RecordPerPage);
            if (!Equals(viewModel, null) && !Equals(viewModel.RequestStatusList, null))
            {
                var gridModel = FilterHelpers.GetDynamicGridModel(model, viewModel.RequestStatusList, Convert.ToInt32(ListType.RequestStatusList));
                gridModel.TotalRecordCount = viewModel.TotalResults;

                return ActionView(RequestStatusListPartialView, gridModel);
            }
            return ActionView(RequestStatusListPartialView, new ReportModel());
        }

        /// <summary>
        /// Edit an existing Request Status
        /// </summary>
        /// <param name="requestStatusId">requestStatusId to get single RequestStatus Detail</param>
        /// <returns>Action View</returns>
        [HttpGet]
        public ActionResult EditRequestStatus(int? requestStatusId)
        {
            RequestStatusViewModel model = new RequestStatusViewModel();
            if (!Equals(requestStatusId, null) && requestStatusId > 0)
            {
                model = _rmaConfigurationAgent.GetRequestStatus(requestStatusId.Value);
            }
            return ActionView(EditRequestStatusPartialView, model);
        }

        /// <summary>
        /// Edit New Request Status
        /// </summary>
        /// <param name="reasonForReturn">View Model Of RequestStatus</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult EditRequestStatus(RequestStatusViewModel requestStatus)
        {
            string message = string.Empty;
            var status = _rmaConfigurationAgent.UpdateRequestStatus(requestStatus);
            TempData[MvcAdminConstants.Notifications] = status ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success) : GenerateNotificationMessages(message, NotificationType.error);
            if (status)
            {
                return RedirectToAction(MvcAdminConstants.CreateEditView);
            }
            return ActionView(EditRequestStatusPartialView, requestStatus);
        }

        /// <summary>
        /// Delete an existing Reason For Return
        /// </summary>
        /// <param name="id">id to delete single RequestStatus</param>
        /// <returns>Return Json Result</returns>
        public JsonResult DeleteRequestStatus(int? id)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();
            if (!Equals(null, id))
            {
                RequestStatusViewModel model = _rmaConfigurationAgent.GetRequestStatus(id.Value);
                if (Equals(model.IsEnabled, false))
                {
                    status = _rmaConfigurationAgent.DeleteRequestStatus(id.Value);
                    message = status ? ZnodeResources.DeleteMessage : ZnodeResources.DeleteCategoryErrorMessage;
                }
                else
                {
                    message = ZnodeResources.DeleteRequestStatusErrorMessage;
                }
                return Json(new { sucess = status, message = message, id = id, isFadeOut = isFadeOut });
            }
            return Json(new { sucess = status, message = message, id = id, isFadeOut = isFadeOut });
        }
        #endregion
    }
}