﻿using Resources;
using System;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Web.Mvc;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Enum;
using Znode.Engine.Exceptions;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Controllers.Orders
{
    public class CustomersController : BaseController
    {
        #region Private Variables
        private ICustomerAgent _customerAgent;
        private readonly IProductAgent _productAgent;
        private IVendorAccountAgent _vendorAccountAgent;
        private readonly IStoreAdminAgent _storeAdminAgent;
        private readonly IAccountAgent _accountAgent;
        private readonly IFranchiseAccountAgent _franchiseAccountAgent;
        private readonly IOrderAgent _orderAgent;
        private readonly ICaseRequestAgent _caseRequestAgent;
        private string manageView = MvcAdminConstants.ManageView;
        private string createEditView = MvcAdminConstants.CreateEditView;
        private string createAddressView = "~/Views/Shared/CreateEditAddress.cshtml";
        private string editAction = "Edit";        
        private string listView = MvcAdminConstants.ListView;
        private string manageAction = "Manage";
        private string manageNewView = "ManageNew";
        private string customerFileName = "Customers.csv";
        private string customerAccountUpdateDate = "CustomerAccount.UpdateDte";
        private string customerAccountCreateDate = "CustomerAccount.CreateDte";
        private string customerAffiliateView = "_CustomerAffiliate";
        private string EditCustomerAffiliateView = "EditCustomerAffiliate";
        private string ordersListView = "_OrderList";
        private string noteListView = "_NoteList";
        private string createNoteView = "_CreateNote";
        private string addCustomerAccountPayment = "AddCustomerAccountPayment";
        private string accountProfileList = "_AccountProfileList";
        private string AssociatedProfileList = "AssociateProfileList";
        private string CustomerPricingProductView = "_CustomerPricingProductList";
        private string customerPricingFileName = "CustomerPricing.csv";
        private string referralCommissionListView = "_ReferralCommissionList";
        private string accountCommissionListView = "_AccountPaymentList";
        private string customerBasedPricingProductListAction = "CustomerBasedPricingProductList";
        private string customerAffiliate = "CustomerAffiliate";
        private const string DisableActionErrorMessageName = "disabled";
        private string associatedSuperUserList = "_AssociatedCustomerList";
        #endregion

        #region Constructor
        public CustomersController()
        {
            _productAgent = new ProductAgent();
            _customerAgent = new CustomerAgent();
            _vendorAccountAgent = new VendorAccountAgent();
            _storeAdminAgent = new StoreAdminAgent();
            _accountAgent = new AccountAgent();
            _franchiseAccountAgent = new FranchiseAccountAgent();
            _orderAgent = new OrderAgent();
            _caseRequestAgent = new CaseRequestAgent();
        }
        #endregion

        #region Public Methods

        #region Customer Account Profile
        /// <summary>
        /// Get Customer Profile List
        /// </summary>
        /// <param name="formModel">FilterCollectionDataModel</param>
        /// <returns>Customer Profile List</returns>
        public ActionResult AccountProfileList([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel formModel)
        {
            int accountId = 0;
            accountId = GetIdFromRouteData(formModel);
            ProfileListViewModel model = _customerAgent.GetCustomerProfileList(accountId, formModel.Filters, formModel.SortCollection, formModel.Page, formModel.RecordPerPage);

            if (!Equals(model, null))
            {
                model.AccountId = accountId;

                var gridModel = FilterHelpers.GetDynamicGridModel(formModel, model.Profiles, (int)ListType.vw_ZNodeGetAssociatedProfile);
                model.GridModel = gridModel;
                model.GridModel.TotalRecordCount = (model.TotalResults > 0) ? model.TotalResults : model.Profiles.Count;

                return ActionView(accountProfileList, model);
            }
            return ActionView(accountProfileList, new ProfileListViewModel());
        }

        /// <summary>
        /// Get Profiles list that not associated with Account
        /// </summary>
        /// <param name="formModel">FilterCollectionDataModel</param>
        /// <returns>Profiles list that not associated with Account</returns>
        public ActionResult CustomerNotAssociatedProfileList([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel formModel)
        {
            ViewBag.TabMode = SelectedTab.Profiles;
            int accountId = 0;
            accountId = GetIdFromRouteData(formModel);

            ProfileListViewModel model = _customerAgent.GetCustomerNotAssociatedProfileList(accountId, formModel.Filters, formModel.SortCollection, formModel.Page, formModel.RecordPerPage);

            if (!Equals(model, null))
            {
                var accountAddress = _customerAgent.GetCustomerAddressDetails(accountId, formModel.Filters, formModel.SortCollection, formModel.Page, formModel.RecordPerPage);
                if (!Equals(accountAddress, null) && !Equals(accountAddress.AccountAddress, null))
                {
                    var defaultBillingAddress = accountAddress.AccountAddress.Find(x => x.IsDefaultBilling == true);
                    if (!Equals(defaultBillingAddress, null))
                    {
                        model.FullName = string.Format("{0} {1}", defaultBillingAddress.FirstName, defaultBillingAddress.LastName);
                    }

                    model.AccountId = accountId;
                    var gridModel = FilterHelpers.GetDynamicGridModel(formModel, model.Profiles, (int)ListType.NotAssociatedProfileWithCustomer);
                    model.GridModel = gridModel;
                    model.GridModel.TotalRecordCount = (model.TotalResults > 0) ? model.TotalResults : model.Profiles.Count;
                }
                return ActionView(AssociatedProfileList, model);
            }
            return ActionView(AssociatedProfileList, new ProfileListViewModel());
        }

        [HttpPost]
        public ActionResult SaveAssociatedProfile(int id, string profileIds)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();
            if (!Equals(id, null) && !Equals(profileIds, null))
            {
                status = _customerAgent.SaveAssociateProfile(id, profileIds);
                message = status ? ZnodeResources.SaveMessage : ZnodeResources.SaveErrorMessage;
                TempData[MvcAdminConstants.Notifications] = status ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.SaveErrorMessage, NotificationType.error);
            }
            return Json(new { sucess = status, message = message, id = id, isFadeOut = isFadeOut });
        }

        /// <summary>
        /// Delete Account Profile.
        /// </summary>
        /// <param name="id">AccountProfile Id</param>
        /// <returns>Result success message while deleted record</returns>
        public JsonResult DeleteAccountAssociatedProfile(int id = 0)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();
            if (!Equals(null, id))
            {
                status = _customerAgent.DeleteAccountAssociatedProfile(id);
                message = status ? ZnodeResources.DeleteMessage : ZnodeResources.DeleteErrorMessage;
            }
            return Json(new { sucess = status, message = message, id = id, isFadeOut = isFadeOut });
        }
        #endregion

        #region Customer Details
        /// <summary>
        /// Get Customer Details list
        /// </summary>
        /// <param name="formModel">FilterCollectionDataModel</param>
        /// <returns>Get All Customer List</returns>
        public ActionResult List([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel formModel)
        {
            
            CustomerListViewModel model = _customerAgent.GetCustomerList(formModel.Filters, formModel.SortCollection, formModel.Page, formModel.RecordPerPage);

            //PRFT Custom Code : Start
            TempData["IsCustomer"] = false;
            TempData["isSuperUser"] = false;
            if (CheckIsCustomerFilters(formModel.Filters))
            {
                TempData["IsCustomer"] = true;
            }
            if (CheckIsSuperUserFilters(formModel.Filters))
            {
                TempData["isSuperUser"] = true;
            }
            //PRFT Custom Code : End
            //Filters for download
            Session[MvcAdminConstants.Filters] = formModel.Filters;

            if (!Equals(model, null))
            {

                ReportModel gridModel = FilterHelpers.GetDynamicGridModel(formModel, model.Customers, (int)ListType.vw_ZNodeFullAccountDetails);

                FilterHelpers.CreateDropdown(ref gridModel, _vendorAccountAgent.GetPortalsList(), formModel.Filters, MvcAdminConstants.PortalId, MvcAdminConstants.StoreName, MvcAdminConstants.StoreNameDropDown);
                FilterHelpers.CreateDropdown(ref gridModel, _vendorAccountAgent.BindProfileList(), formModel.Filters, MvcAdminConstants.ProfileId, MvcAdminConstants.LabelName, MvcAdminConstants.ProfileDropdown);
                FilterHelpers.CreateDropdown(ref gridModel, _customerAgent.BindStatus().GetAffilateApprovalStatusList, formModel.Filters, MvcAdminConstants.Value, MvcAdminConstants.Text, MvcAdminConstants.AffiliatedApprovalStatusName);

                model.GridModel = gridModel;
                model.GridModel.TotalRecordCount = (model.TotalResults > 0) ? model.TotalResults : model.Customers.Count;

                model.IsEnableCustomerPricing = Equals(EnableCustomerPricing, null) ? false : EnableCustomerPricing.Value;
                return ActionView(listView, model);
            }
            return ActionView(listView, new CustomerListViewModel());
        }

        /// <summary>
        /// To manage customer details
        /// </summary>
        /// <param name="formModel">FilterCollectionDataModel</param>
        /// <param name="tabMode">Current tabMode</param>
        /// <returns>Manage View</returns>
        public ActionResult Manage([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel formModel, string tabMode = "")
        {

            int customerId = 0;
            string mode = GetTabModeFromRouteData(formModel);
            customerId = GetIdFromRouteData(formModel);

            CustomerViewModel model = new CustomerViewModel();
            model.EnableCustomerPricing = (EnableCustomerPricing.HasValue) ? Convert.ToBoolean(EnableCustomerPricing) : false;
            if (!string.IsNullOrEmpty(tabMode))
            {
                TempData[MvcAdminConstants.SetTabMode] = tabMode;
            }
            if (!Equals(customerId, null) && customerId > 0)
            {
                model.CustomerAccount = _customerAgent.GetCustomerAccountById(customerId);

                if (!Equals(model, null) && !Equals(model.CustomerAccount, null))
                {
                    var accountAddress = _customerAgent.GetCustomerAddressDetails(customerId, formModel.Filters, formModel.SortCollection, formModel.Page, formModel.RecordPerPage);
                    if (!Equals(accountAddress, null) && !Equals(accountAddress.AccountAddress, null))
                    {
                        formModel.Filters.Add(new FilterTuple(FilterKeys.IsBillingActive, FilterOperators.Equals, FilterKeys.ActiveIndTrue));

                        var defaultBillingAddress = _customerAgent.GetCustomerAddressDetails(customerId, formModel.Filters, null, null, null);
                        if (!Equals(defaultBillingAddress, null) && !Equals(defaultBillingAddress.AccountAddress, null))
                        {

                            model.FullName = string.Format("{0} {1}", defaultBillingAddress.AccountAddress[0].FirstName, defaultBillingAddress.AccountAddress[0].LastName);
                            model.CompanyName = defaultBillingAddress.AccountAddress[0].CompanyName;
                            model.PhoneNumber = defaultBillingAddress.AccountAddress[0].PhoneNumber;
                        }
                        var gridModel = FilterHelpers.GetDynamicGridModel(formModel, accountAddress.AccountAddress, (int)ListType.CustomerAddressList);
                        model.GridModel = gridModel;
                        model.GridModel.TotalRecordCount = (accountAddress.TotalResults > 0) ? accountAddress.TotalResults : accountAddress.AccountAddress.Count;
                    }

                    return ActionView(((Request.IsAjaxRequest())) ? MvcAdminConstants.VendorProductPartialManageView : manageNewView, model);
                }
            }
            return ActionView(manageNewView, model);
        }

        /// <summary>
        /// Create customer account.
        /// </summary>
        /// <returns>Create view</returns>
        public ActionResult Create()
        {
            CustomerViewModel model = new CustomerViewModel();
                              
            //PRFT Custom Code : Start
            model.customerList = _customerAgent.GetCustomerList(new FilterCollection(), new SortCollection(), null, null);     
            model.SalesReps = _customerAgent.GetSalesRep();
            model.SalesReps.Insert(0, new Api.Models.PRFTSalesRepresentative() { SalesRepID = 0, SaleRepNameWithLocation = "Please Select", Email = string.Empty });

            model.IsSalesRepFound = false;
            model.SalesRepId = 0;
            if (User.Identity.IsAuthenticated)
            {
                AccountViewModel accountModel = _accountAgent.GetAccountViewModel();
                if (!Equals(accountModel, null))
                {
                    if (!string.IsNullOrEmpty(accountModel.Custom1))
                    {
                        int salesRepID = 0;
                        int.TryParse(accountModel.Custom1, out salesRepID);
                        if (salesRepID > 0)
                        {
                            model.SalesRepId = salesRepID;
                            model.IsSalesRepFound = true;
                        }
                    }
                }
            }
            //PRFT Custom Code : End
            return ActionView(createEditView, model);
        }

        /// <summary>
        /// This method will use to add Customer account
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(CustomerViewModel model, FormCollection collection)
        {
            //PRFT Custom Code: Start
            if (!collection["SalesRepId"].ToString().Equals("0"))
            {
                model.CustomerAccount.Custom1 = collection["SalesRepId"].ToString();
            }
            //if (!Convert.ToInt32( model.CustomerAccount.Custom2).Equals(1))
            //{
            //    model.CustomerAccount.ParentAccountID = Convert.ToInt32(collection["CustomerId"].ToString());
            //}
            
            //PRFT Custom Code: End

            ModelState.Remove(customerAccountUpdateDate);
            ModelState.Remove(customerAccountCreateDate);
            if (ModelState.IsValid)
            {
                //string customerAccountId = collection["CustomerId"].ToString();
                int accountId = 0;
                string message = string.Empty;
                bool status = _customerAgent.CreateCustomerAccount(model, out accountId, out message);
                TempData[MvcAdminConstants.Notifications] = status ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success) : GenerateNotificationMessages(message, NotificationType.error);

                if (status)
                {
                    //PRFT Custom Code : Start
                    if (!Convert.ToInt32(model.CustomerAccount.Custom2).Equals(1))
                    {
                        string customerAccountId = collection["CustomerId"].ToString();
                        bool associationStatus =  _customerAgent.SaveAssociateCustomer(accountId, customerAccountId);
                    }
                    //PRFT Custom Code : End
                    return RedirectToAction(manageView, new { id = accountId });
                }
                else
                {
                    return ActionView(createEditView, model);
                }
            }
            return ActionView(createEditView, model);
        }

        /// <summary>
        /// To Edit Customer details
        /// </summary>
        /// <param name="id">int account id</param>
        /// <returns>returns edit customer view</returns>
        [HttpGet]
        public ActionResult Edit(int? id)
        {
            
            if (!Equals(id, null) && id.Value > 0)
            {
                CustomerViewModel model = new CustomerViewModel();

                model.CustomerAccount = _customerAgent.GetCustomerAccountById(id.Value);


                //PRFT CUstom Code : Start
                //Custom Code for : To show selected ParentAccountId in dropdown : Start
                model.CustParentAccountId = 0;
                model.IsCustParentAccountFound = false;
                if (!Equals(model.CustomerAccount, null))
                {
                    if ((model.CustomerAccount.ParentAccountID) > 0)
                    {
                        int? custParentAccountId = model.CustomerAccount.ParentAccountID;                        
                        if (custParentAccountId > 0)
                        {
                            model.CustParentAccountId = custParentAccountId;
                            model.IsCustParentAccountFound = true;
                        }
                    }
                }
                //Custom Code for : To show selected ParentAccountId in dropdown : End

                model.customerList = _customerAgent.GetCustomerList(new FilterCollection(), new SortCollection(), null, null);
                model.SalesReps = _customerAgent.GetSalesRep();
                model.SalesReps.Insert(0, new Api.Models.PRFTSalesRepresentative() { SalesRepID = 0, SaleRepNameWithLocation = "Please Select", Email = string.Empty });

                model.IsSalesRepFound = false;
                model.SalesRepId = 0;
                if (User.Identity.IsAuthenticated)
                {
                    //AccountViewModel accountModel = _accountAgent.GetAccountViewModel();
                    if (!Equals(model.CustomerAccount, null))
                    {
                        if (!string.IsNullOrEmpty(model.CustomerAccount.Custom1))
                        {
                            int salesRepID = 0;
                            int.TryParse(model.CustomerAccount.Custom1, out salesRepID);
                            if (salesRepID > 0)
                            {
                                model.SalesRepId = salesRepID;
                                model.IsSalesRepFound = true;
                            }
                        }
                    }
                }
                //PRFT Custom Code : End

                return View(MvcAdminConstants.CreateEditView, model);
            }
            return View(MvcAdminConstants.CreateEditView);
        }

        /// <summary>
        /// To update Customer Account data.
        /// </summary>
        /// <param name="ViewModel">CustomerViewModel model</param>
        /// <returns>show sucess/fail message and redirect to list page</returns>
        [HttpPost]
        public ActionResult Edit(CustomerViewModel ViewModel, FormCollection collection)
        {
            //PRFT Custom Code: Start
            if (!collection["SalesRepId"].ToString().Equals("0"))
            {
                ViewModel.CustomerAccount.Custom1 = collection["SalesRepId"].ToString();
            }
            //if (ViewModel.CustomerAccount.Custom2 != null && !Convert.ToInt32(ViewModel.CustomerAccount.Custom2).Equals(1))
            //{
            //    ViewModel.CustomerAccount.ParentAccountID = Convert.ToInt32(collection["CustomerId"].ToString());
            //}
            //if (ViewModel.CustomerAccount.Custom3 != null && (ViewModel.CustomerAccount.Custom3).Equals("1"))--- Commented Code for SalesRep 
            //{
            //    ViewModel.CustomerAccount.Custom3 = (ConfigurationManager.AppSettings["salesRepAccountTypeId"].ToString());
            //}
            //PRFT Custom Code: End

            ModelState.Remove("CustomerAccount.UpdateDte");
            ModelState.Remove("CustomerAccount.CreateDte");
            if (ModelState.IsValid)
            {
                var accountDetails = _customerAgent.UpdateCustomerAccount(ViewModel);
                
                if (!Equals(accountDetails, null) && accountDetails.HasError)
                {
                    TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(accountDetails.ErrorMessage, NotificationType.error);
                }
                else
                {
                    TempData[MvcAdminConstants.Notifications] = (!Equals(accountDetails, null) && !Equals(accountDetails.CustomerAccount, null) && accountDetails.CustomerAccount.AccountId > 0)
                        ? GenerateNotificationMessages(ZnodeResources.UpdateMessage, NotificationType.success)
                        : GenerateNotificationMessages(ZnodeResources.UpdateErrorMessage, NotificationType.error);

                    //PRFT Custom Code : Start     
                    
                    if (!Convert.ToInt32(ViewModel.CustomerAccount.Custom2).Equals(1))
                    {
                        string customerAccountId = collection["CustomerId"].ToString();
                        bool associationStatus = _customerAgent.SaveAssociateCustomer(ViewModel.CustomerAccount.AccountId, customerAccountId);
                    }
                   
                    if (accountDetails.CustomerAccount.AccountId > 0 && Convert.ToInt32(ViewModel.CustomerAccount.Custom2).Equals(1))
                    {                        
                        bool deletestatus = _customerAgent.DeleteAllUserMapping(accountDetails.CustomerAccount.AccountId);
                    }
                    //PRFT Custom Code : End

                    return RedirectToAction(manageAction, new { @id = ViewModel.CustomerAccount.AccountId });
                }
            }
            return View(MvcAdminConstants.CreateEditView, ViewModel);
        }

        /// <summary>
        /// Reset Customer Account Password
        /// </summary>
        /// <param name="accountId">int accountId</param>
        /// <returns>Return to manage view</returns>
        [HttpGet]
        public ActionResult ResetPassword(int accountId)
        {
            if (accountId > 0)
            {
                AccountViewModel model = new AccountViewModel();
                model = _franchiseAccountAgent.ResetPassword(accountId, MvcAdminConstants.CustomerKey);
                if (model.HasError)
                {
                    TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(model.ErrorMessage, Znode.Engine.MvcAdmin.Models.NotificationType.error);
                    return RedirectToAction(editAction, new { @id = accountId });
                }
                TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(model.SuccessMessage, Znode.Engine.MvcAdmin.Models.NotificationType.info);
                return RedirectToAction(manageAction, new { @id = accountId });
            }
            return RedirectToAction(editAction, new { @id = accountId });
        }

        /// <summary>
        /// Download Customer account List 
        /// </summary>
        /// <param name="model">CustomerDownloadListViewModel</param>
        /// <param name="viewModel">FilterCollection</param>
        /// <returns>Download csv file </returns>
        [HttpPost]
        public ActionResult DownloadCustomerDetails(CustomerDownloadListViewModel model, [ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel viewModel)
        {
            if (!Equals(model, null))
            {
                model.customerList = _customerAgent.GetCustomerToDownload((Znode.Engine.Api.Client.Filters.FilterCollection)Session[MvcAdminConstants.Filters], viewModel.SortCollection, viewModel.Page, viewModel.TotalResults).customerList;

                if (!Equals(model, null))
                {
                    if (model.customerList.Count <= 0)
                    {
                        TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.NoRecordFoundMessageText, NotificationType.error);
                        return View(listView, model);
                    }
                }

                _customerAgent.DownloadFile(model, Response, customerFileName);

            }
            return Content(string.Empty);
        }
        #endregion

        #region Customer Address
        /// <summary>
        /// Create new Customer address
        /// </summary>
        /// <param name="accountId">account id</param>
        /// <returns></returns>
        public ActionResult CreateAddress(int accountId = 0)
        {
            AddressViewModel addressModel = new AddressViewModel();
            Collection<CountryModel> countries = _accountAgent.GetCountriesByPortalId(PortalId.Value);
            if (!Equals(countries, null))
            {
                addressModel.Countries = countries;
            }
            addressModel.AccountId = accountId;
            return ActionView(createAddressView, addressModel);
        }

        /// <summary>
        /// Create New Customer Address.
        /// </summary>
        /// <param name="model">AddressViewModel</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult CreateAddress(AddressViewModel model)
        {

            ModelState.Remove(MvcAdminConstants.StreetAddress1);
            ModelState.Remove(MvcAdminConstants.City);
            ModelState.Remove(MvcAdminConstants.PostalCode);
            ModelState.Remove(MvcAdminConstants.PhoneNumber);
            ModelState.Remove(MvcAdminConstants.StateCode);
            if (ModelState.IsValid)
            {
                var address = _vendorAccountAgent.UpdateAddressDetails(model);

                if (!Equals(address, null) && address.HasError)
                {
                    TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(address.ErrorMessage, Models.NotificationType.error);
                    Collection<CountryModel> countries = _accountAgent.GetCountriesByPortalId(PortalId.Value);
                    model.Countries = countries;
                    return ActionView(createAddressView, model);
                }
                else if (!Equals(address, null) && address.AddressId > 0)
                {
                    TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.SaveMessage, Models.NotificationType.success);
                    return RedirectToAction(manageView, new { @id = model.AccountId });
                }
                else
                {
                    TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.SaveErrorMessage, Models.NotificationType.error);
                }
                return ActionView(createAddressView, model);
            }
            return ActionView(createAddressView, model); ;


        }

        /// <summary>
        /// Edit Existing Customer Address.
        /// </summary>
        /// <param name="AddressId">int AddressId</param>
        /// <param name="AccountId">int AccountId</param>
        /// <returns>Return Create Edit Address view</returns>
        public ActionResult EditAddress(int AddressId = 0, int AccountId = 0)
        {
            if (ModelState.IsValid)
            {
                if (!Equals(AddressId, null))
                {

                    AddressViewModel viewModel = _vendorAccountAgent.GetAddressById(AddressId);
                    Collection<CountryModel> countries = _accountAgent.GetCountriesByPortalId(PortalId.Value);
                    viewModel.Countries = countries;
                    if (Equals(viewModel, null))
                    {
                        return RedirectToAction(MvcAdminConstants.ManageView, new { @id = AccountId });
                    }
                    return ActionView(createAddressView, viewModel);
                }
            }
            return RedirectToAction(manageView, new { @id = AccountId });
        }

        /// <summary>
        /// To Edit Customer Address.
        /// </summary>
        /// <param name="ViewModel">VendorAccountViewModel model</param>
        /// <returns>show susscess/fail message and redirect to list page</returns>
        [HttpPost]
        public ActionResult EditAddress(AddressViewModel model)
        {
            ModelState.Remove(MvcAdminConstants.StreetAddress1);
            ModelState.Remove(MvcAdminConstants.City);
            ModelState.Remove(MvcAdminConstants.PostalCode);
            ModelState.Remove(MvcAdminConstants.PhoneNumber);
            ModelState.Remove(MvcAdminConstants.StateCode);
            if (ModelState.IsValid)
            {
                var address = _vendorAccountAgent.UpdateAddressDetails(model);
                if (!Equals(address, null) && address.HasError)
                {
                    TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(address.ErrorMessage, Models.NotificationType.error);
                }
                else if (!Equals(address, null) && address.AddressId > 0)
                {
                    TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.UpdateMessage, Models.NotificationType.success);

                    return RedirectToAction(manageView, new { @id = model.AccountId });
                }
                else
                {
                    TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.SaveErrorMessage, Models.NotificationType.error);
                }
                Collection<CountryModel> countries = _accountAgent.GetCountriesByPortalId(PortalId.Value);
                model.Countries = countries;
                return View(createAddressView, model);
            }
            return View(createAddressView, model);
        }

        /// <summary>
        /// Delete Address associated with Customer Account
        /// </summary>
        /// <param name="id">Address Id</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult DeleteAddress(int id = 0)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();

            if (!Equals(id, null))
            {
                var isDefaultAddress = _vendorAccountAgent.GetAddressById(id);
                if (Equals(isDefaultAddress.IsDefaultBilling, true))
                {
                    message = status ? ZnodeResources.DeleteMessage : ZnodeResources.ErrorDefaultBillingAddressNotDeleted;
                }
                else if (Equals(isDefaultAddress.IsDefaultShipping, true))
                {
                    message = status ? ZnodeResources.DeleteMessage : ZnodeResources.ErrorDefaultShippingAddressNotDeleted;
                }
                else
                {
                    status = _vendorAccountAgent.DeleteAddress(id);
                }
            }
            return Json(new { sucess = status, message = message, id = id, isFadeOut = isFadeOut });
        }
        #endregion

        #region Customers Order
        /// <summary>
        /// Gets the order list by customer id.
        /// </summary>
        /// <param name="id">Id of the customer.</param>
        /// <param name="model">Filter Collection Data Model</param>
        /// <returns>Returns manage view.</returns>
        public ActionResult OrderList(int? id, [ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model)
        {
            model.Filters.Add(new FilterTuple(FilterKeys.AccountId, FilterOperators.Equals, id.ToString()));

            OrderViewModel orderModel = new OrderViewModel();

            AdminOrderListViewModel list = _orderAgent.GetOrderList(model.Filters, model.SortCollection, model.Page, model.RecordPerPage);

            var gridModel = FilterHelpers.GetDynamicGridModel(model, list.OrderList, Convert.ToInt32(ListType.OrdersList));
            gridModel.TotalRecordCount = list.TotalResults;
            orderModel.DynamicGrid = gridModel;

            return ActionView(ordersListView, orderModel);
        }
        #endregion

        #region Customer Note
        /// <summary>
        /// Create note.
        /// </summary>
        /// <returns>View of create note.</returns>
        public ActionResult CreateNote(int? id)
        {
            ViewBag.TabMode = SelectedTab.Notes;

            NotesViewModel model = new NotesViewModel();

            model.AccountId = id.HasValue ? id.Value : 0;
            return View(createNoteView, model);
        }

        /// <summary>
        /// Create note.
        /// </summary>
        /// <param name="model">model to create.</param>
        /// <returns>View of create note.</returns>
        [HttpPost]
        public ActionResult CreateNote(NotesViewModel model)
        {
            if (ModelState.IsValid)
            {
                bool status = _caseRequestAgent.SaveCaseRequestsNotes(model);
                TempData[MvcAdminConstants.Notifications] = status ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.SaveErrorMessage, NotificationType.error);
                return RedirectToAction(MvcAdminConstants.ManageView, new { id = model.AccountId, tabMode = SelectedTab.Notes });
            }
            return View(createNoteView, model);
        }

        /// <summary>
        /// Gets the note list.
        /// </summary>
        /// <param name="id">Id of the account.</param>
        /// <returns>View of note.</returns>
        public ActionResult NoteList(int? id)
        {
            NotesListViewModel list = new NotesListViewModel();

            if (id.HasValue && id.Value > 0)
            {
                list = _caseRequestAgent.GetNotesByAccountId(id.Value);

                list.AccountId = id.Value;

                if (!Equals(list, null) && !Equals(list.Notes, null) && (list.Notes.Count > 0))
                {
                    return ActionView(noteListView, list);
                }
            }
            return ActionView(noteListView, list);
        }
        #endregion

        #region Customer Affiliate

        /// <summary>
        /// Gets the Customer Affiliate details.
        /// </summary>
        /// <param name="id">Id of the account.</param>
        /// /// <param name="model">Filter Collection Data Model</param>
        /// <returns>View of Customer Affiliate.</returns>
        [HttpGet]
        public ActionResult GetCustomerAffiliate(int? id, [ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model)
        {
            CustomerAffiliateViewModel viewModel = new CustomerAffiliateViewModel();
            viewModel = _customerAgent.GetCustomerAffiliateByAccountId(id.Value);
            viewModel.AccountId = id.Value;
            AccountPaymentListViewModel accountPaymentList = _customerAgent.GetAccountPaymentList(id.Value, model.Filters, model.SortCollection, model.Page, model.RecordPerPage);
            var accountPaymentgridModel = FilterHelpers.GetDynamicGridModel(model, accountPaymentList.AccountPaymentList, Convert.ToInt32(ListType.AccountPaymentList));
            accountPaymentgridModel.TotalRecordCount = accountPaymentList.TotalResults;
            viewModel.AccountPaymentGridModel = accountPaymentgridModel;
            return ActionView(customerAffiliateView, viewModel);
        }

        /// <summary>
        /// Gets the Referral Commission list.
        /// </summary>
        /// <param name="id">Id of the account.</param>
        /// <returns>View of Referral Commission list.</returns>
        public ActionResult GetReferralCommissionList(int accountId, [ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model)
        {
            ReferralCommissionListViewModel referralCommissionList = _customerAgent.GetReferralCommissionList(accountId, model.Filters, model.SortCollection, model.Page, model.RecordPerPage);
            var gridModel = FilterHelpers.GetDynamicGridModel(model, referralCommissionList.ReferralCommissions, Convert.ToInt32(ListType.ReferralCommissionList));
            gridModel.TotalRecordCount = referralCommissionList.TotalResults;
            return ActionView(referralCommissionListView, gridModel);
        }

        /// <summary>
        /// Gets the Account Payment list.
        /// </summary>             
        /// <param name="refrralCommissionType">ReferralCommission Type</param>
        /// <param name="id">Id of the account.</param>   
        /// <param name="model">Filter Collection Data Model</param>
        /// <returns>View of Account Payment list.</returns>
        public ActionResult GetAccountPaymentList(string refrralCommissionType, int id, [ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model)
        {
            CustomerAffiliateViewModel viewModel = new CustomerAffiliateViewModel();
            viewModel.AccountId = id;
            viewModel.ReferralCommissionType = refrralCommissionType;
            AccountPaymentListViewModel accountPaymentList = _customerAgent.GetAccountPaymentList(id, model.Filters, model.SortCollection, model.Page, model.RecordPerPage);
            var accountPaymentgridModel = FilterHelpers.GetDynamicGridModel(model, accountPaymentList.AccountPaymentList, Convert.ToInt32(ListType.AccountPaymentList));
            accountPaymentgridModel.TotalRecordCount = accountPaymentList.TotalResults;
            viewModel.AccountPaymentGridModel = accountPaymentgridModel;
            return ActionView(accountCommissionListView, viewModel);
        }

        /// <summary>
        /// Edit Existing Customer Affiliate.
        /// </summary>
        /// <param name="id">Id of the account.</param>
        /// <returns>Return Edit Customer Affiliate view</returns>
        [HttpGet]
        public ActionResult EditCustomerAffiliate(int? id)
        {
            ViewBag.TabMode = SelectedTab.Affiliate;
            CustomerAffiliateViewModel viewModel = new CustomerAffiliateViewModel();
            viewModel = _customerAgent.GetCustomerAffiliateByAccountId(id.Value);
            viewModel.ReferralCommissionTypeList = _customerAgent.GetReferralCommissionTypeList();
            viewModel.PartnerApprovalStatusList = _customerAgent.GetApprovStatusList();
            return ActionView(EditCustomerAffiliateView, viewModel);
        }

        /// <summary>
        /// To Edit Customer Affiliate.
        /// </summary>
        /// <param name="viewModel">CustomerAffiliateViewModel</param>
        /// <returns>Action View</returns>
        [HttpPost]
        public ActionResult EditCustomerAffiliate(CustomerAffiliateViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var status = _customerAgent.UpdateCustomerAffiliate(viewModel);
                TempData[MvcAdminConstants.Notifications] = status ? GenerateNotificationMessages(ZnodeResources.UpdateMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.UpdateErrorMessage, NotificationType.error);
                return RedirectToAction(MvcAdminConstants.ManageView, new { id = viewModel.AccountId, tabMode = SelectedTab.Affiliate });
            }
            return ActionView(EditCustomerAffiliateView, viewModel);
        }

        /// <summary>
        /// Add Payment details
        /// </summary>
        /// <returns>Add Payment View</returns>
        [HttpGet]
        public ActionResult AddAccountPayment(int? id)
        {
            ViewBag.TabMode = SelectedTab.Affiliate;
            CustomerAffiliateViewModel viewModel = _customerAgent.GetCustomerAffiliateByAccountId(id.Value);
            AccountPaymentViewModel model = new AccountPaymentViewModel();
            model.AccountId = id.Value;
            model.DisplayAmountOwned = viewModel.AmountOwed;
            return View(addCustomerAccountPayment, model);
        }

        /// <summary>
        /// Post method to add Account Payment
        /// </summary>
        /// <param name="id">Account id to Add Account Payment</param>
        /// <param name="viewModel">Account Payment View Model</param>
        /// <returns>Action View</returns>
        [HttpPost]
        public ActionResult AddAccountPayment(int? id, AccountPaymentViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                viewModel.AccountId = id.Value;
                bool status = _customerAgent.CreateAccountPayment(viewModel);
                TempData[MvcAdminConstants.Notifications] = status ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.SaveErrorMessage, NotificationType.error);
                return RedirectToAction(MvcAdminConstants.ManageView, new { id = viewModel.AccountId, tabMode = SelectedTab.Affiliate });
            }
            return View(addCustomerAccountPayment, viewModel);
        }

        #endregion

        #region Customer Based Pricing


        /// <summary>
        /// To Delete the Customer based on account id.
        /// </summary>
        /// <param name="id">Account id</param>
        /// <returns>Success message on deleted</returns>
        public JsonResult Delete(int id)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();
            if (!Equals(null, id))
            {
                status = _customerAgent.DeleteCustomerAccount(id, out message);
                message = status ? ZnodeResources.DeleteMessage : message;
            }
            return Json(new { sucess = status, message = message, id = id, isFadeOut = isFadeOut });
        }

        /// <summary>
        ///  Gets the product Customer based pricing on account id.
        /// </summary>
        /// <param name="model">FilterCollectionDataModel</param>
        /// <returns>Customer Pricing Product List</returns>
        public ActionResult CustomerBasedPricingProductList([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model)
        {
            int accountId = 0;
            accountId = GetIdFromRouteData(model);
            if (accountId > 0)
            {
                model.Filters.Add(new FilterTuple(MvcAdminConstants.AccountId, FilterOperators.Equals, accountId.ToString()));

                CustomerBasedPricingProductListViewModel customerPricingModel = _customerAgent.GetCustomerPricingProductList(accountId, model.Filters, model.SortCollection, model.Page, model.RecordPerPage);
                Session[MvcAdminConstants.Filters] = model.Filters;
                if (!Equals(customerPricingModel, null))
                {
                    customerPricingModel.AccountId = accountId;
                    var manufacturerType = _productAgent.GetManufacturer();

                    customerPricingModel.GridModel = FilterHelpers.GetDynamicGridModel(model, customerPricingModel.CustomerBasedPricingProduct, (int)ListType.CustomerPricingProduct);
                    var grid = customerPricingModel.GridModel;
                    FilterHelpers.CreateDropdown(ref grid, _vendorAccountAgent.GetPortalsList(), model.Filters, MvcAdminConstants.PortalId, MvcAdminConstants.StoreName, MvcAdminConstants.StoreDropdownName);
                    customerPricingModel.GridModel.TotalRecordCount = (customerPricingModel.TotalResults > 0) ? customerPricingModel.TotalResults : customerPricingModel.CustomerBasedPricingProduct.Count;
                    manufacturerType.Insert(0, new ManufacturerModel { Name = MvcAdminConstants.NotApplicable });
                    FilterHelpers.CreateDropdown(ref grid, manufacturerType, model.Filters, MvcAdminConstants.ManufacturerId, MvcAdminConstants.LabelName, MvcAdminConstants.Brand);
                    FilterHelpers.CreateDropdown(ref grid, _productAgent.GetCategories(), model.Filters, MvcAdminConstants.CategoryId, MvcAdminConstants.Name, MvcAdminConstants.ProductCategoryDropdownName);

                    return ActionView(CustomerPricingProductView, customerPricingModel);
                }
            }
            return ActionView(CustomerPricingProductView, new CustomerBasedPricingProductListViewModel());
        }

        /// <summary>
        /// This method for download customer pricing details.
        /// </summary>
        /// <param name="model">CustomerBasedPricingListViewModel</param>
        /// <param name="viewModel">FilterCollectionDataModel</param>
        /// <returns>Download CSV file</returns>
        [HttpPost]
        public ActionResult DownloadCustomerBasedPricingList(CustomerBasedPricingProductListViewModel model, [ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel viewModel)
        {
            if (!Equals(model, null))
            {
                model.CustomerBasedPricingProduct = _customerAgent.GetCustomerPricingProductList(model.AccountId, (Znode.Engine.Api.Client.Filters.FilterCollection)Session[MvcAdminConstants.Filters], model.SortCollection, model.Page, model.RecordPerPage).CustomerBasedPricingProduct;

                //below code downloads the file.
                _customerAgent.DownloadCustomerPricingProductFile(model, Response, customerPricingFileName);

            }
            return Content(string.Empty);
        }

        #endregion

        /// <summary>
        /// This method will enable or disable the customer account.
        /// </summary>
        /// <param name="id">Nullable int customer Account Id</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult EnableDisable(int? id)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();
            if (!Equals(null, id))
            {
                AccountViewModel account = _accountAgent.GetAccountByAccountId(id.Value);
                if (!Equals(account, null) && !string.IsNullOrEmpty(account.UserName))
                {
                    string currentUserName = HttpContext.User.Identity.Name;
                    if (_storeAdminAgent.IsCurrentUser(id.Value, currentUserName))
                    {
                        message = string.Format(ZnodeResources.DeleteDisableCurrentUserErrorMessage, DisableActionErrorMessageName);
                    }
                    else
                    {
                        status = _storeAdminAgent.EnableDisableStoreAdmin(id.Value);
                        message = status ? ZnodeResources.DisableMessage : ZnodeResources.DisableErrorMessage;
                    }
                }
                else
                {
                    message = ZnodeResources.WarningEnableDisableGuestUser;
                }
            }
            return Json(new { sucess = status, message = message, id = id, action = ZnodeResources.DisableText, isFadeOut = isFadeOut });
        }

        #endregion

        #region private Methods

        /// <summary>
        /// To Get Id from Route Data.
        /// </summary>
        /// <param name="formModel">FilterCollectionDataModel formModel</param>
        /// <returns>Return Product Id</returns>
        private int GetIdFromRouteData(FilterCollectionDataModel formModel)
        {
            int productId = 0;
            if (!Equals(RouteData.Values["id"], null))
            {
                productId = int.Parse(RouteData.Values["id"].ToString());
            }
            if (Equals(productId, 0))
            {
                foreach (var x in formModel.Params)
                {
                    if (x.Key.ToLower().Equals("id"))
                    {
                        productId = int.Parse(x.Value);
                    }
                }
            }
            return productId;
        }

        /// <summary>
        /// To Get selected Tab mode from Route Data.
        /// </summary>
        /// <param name="formModel">FilterCollectionDataModel formModel</param>
        /// <returns>Return selected Tab Mode</returns>
        private string GetTabModeFromRouteData(FilterCollectionDataModel formModel)
        {
            string mode = string.Empty;
            if (!Equals(RouteData.Values["tabMode"], null))
            {
                mode = RouteData.Values["tabMode"].ToString();
            }
            if (string.IsNullOrEmpty(mode))
            {
                foreach (var x in formModel.Params)
                {
                    if (x.Key.ToLower().Equals("tabmode"))
                    {
                        mode = x.Value.ToString();
                    }
                }
            }
            return mode;
        }


        #endregion

        #region Private Enum
        /// <summary>
        /// Enum for selected tab.
        /// </summary>
        private enum SelectedTab
        {
            Orders,
            Notes,
            Profiles,
            Affiliate,
            CustomerBasePricing,
            CustomerMapping
        }
        #endregion

        #region PRFT Custom Method

        private bool CheckIsCustomerFilters(FilterCollection filters)
        {
            if (!Equals(filters, null) && ((filters.Count > 0)))
            {
                foreach (var item in filters)
                {
                    if (Equals(item.Item1.ToLower(), "iscustomer"))
                    {
                        if (item.Item3.Contains("true"))
                        {
                            return true;
                        }
                        else
                        { return false; }
                    }
                }
            }
            return false;
        }

        private bool CheckIsSuperUserFilters(FilterCollection filters)
        {
            if (!Equals(filters, null) && ((filters.Count > 0)))
            {
                foreach (var item in filters)
                {
                    if (Equals(item.Item1.ToLower(), "issuperuser"))
                    {
                        if (item.Item3.Contains("true"))
                        {
                            return true;
                        }
                        else
                        { return false; }
                    }
                }
            }
            return false;
        }

        #region Customer Mapping
        /// <summary>
        /// Get Customer Profile List
        /// </summary>
        /// <param name="formModel">FilterCollectionDataModel</param>
        /// <returns>Customer Profile List</returns>
        public ActionResult CustomerMappingList([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel formModel)
        {
            int accountId = 0;
            accountId = GetIdFromRouteData(formModel);
            
            CustomerListViewModel model = _customerAgent.GetAssociatedCustomerList(accountId, formModel.Filters, formModel.SortCollection, formModel.Page, formModel.RecordPerPage);           
            Session[MvcAdminConstants.Filters] = formModel.Filters;

            if (!Equals(model, null))
            {
                model.AccountId = accountId;
                ReportModel gridModel = FilterHelpers.GetDynamicGridModel(formModel, model.Customers, (int)ListType.PRFTCustomerUserMappingList);
                FilterHelpers.CreateDropdown(ref gridModel, _vendorAccountAgent.GetPortalsList(), formModel.Filters, MvcAdminConstants.PortalId, MvcAdminConstants.StoreName, MvcAdminConstants.StoreNameDropDown);
                FilterHelpers.CreateDropdown(ref gridModel, _vendorAccountAgent.BindProfileList(), formModel.Filters, MvcAdminConstants.ProfileId, MvcAdminConstants.LabelName, MvcAdminConstants.ProfileDropdown);
                FilterHelpers.CreateDropdown(ref gridModel, _customerAgent.BindStatus().GetAffilateApprovalStatusList, formModel.Filters, MvcAdminConstants.Value, MvcAdminConstants.Text, MvcAdminConstants.AffiliatedApprovalStatusName);

                model.GridModel = gridModel;
                model.GridModel.TotalRecordCount = (model.TotalResults > 0) ? model.TotalResults : model.Customers.Count;

                model.IsEnableCustomerPricing = Equals(EnableCustomerPricing, null) ? false : EnableCustomerPricing.Value;
                return ActionView(associatedSuperUserList, model);
            }
            return ActionView(associatedSuperUserList, new CustomerListViewModel());
        }

        /// <summary>
        /// Delete associated customer mapping from user.
        /// </summary>
        /// <param name="id">CustomerUserMapping Id</param>
        /// <returns>Result success message while deleted record</returns>
        public JsonResult DeleteUserAssociatedCustomer(int id = 0)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();
            if (!Equals(null, id))
            {
                status = _customerAgent.DeleteUserAssociatedCustomer(id);
                message = status ? ZnodeResources.DeleteMessage : ZnodeResources.DeleteErrorMessage;
            }
            return Json(new { sucess = status, message = message, id = id, isFadeOut = isFadeOut });
        }

        public ActionResult UserNotAssociatedToCustomerList([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel formModel)
        {
            ViewBag.TabMode = SelectedTab.CustomerMapping;
            int accountId = 0;
            accountId = GetIdFromRouteData(formModel);

            CustomerListViewModel model = _customerAgent.GetNotAssociatedCustomerList(accountId, formModel.Filters, formModel.SortCollection, formModel.Page, formModel.RecordPerPage);
            Session[MvcAdminConstants.Filters] = formModel.Filters;

            if (!Equals(model, null))
            {
                model.AccountId = accountId;
                ReportModel gridModel = FilterHelpers.GetDynamicGridModel(formModel, model.Customers, (int)ListType.PRFTUserNotAssociatedToCustomer);
                FilterHelpers.CreateDropdown(ref gridModel, _vendorAccountAgent.GetPortalsList(), formModel.Filters, MvcAdminConstants.PortalId, MvcAdminConstants.StoreName, MvcAdminConstants.StoreNameDropDown);
                FilterHelpers.CreateDropdown(ref gridModel, _vendorAccountAgent.BindProfileList(), formModel.Filters, MvcAdminConstants.ProfileId, MvcAdminConstants.LabelName, MvcAdminConstants.ProfileDropdown);
                FilterHelpers.CreateDropdown(ref gridModel, _customerAgent.BindStatus().GetAffilateApprovalStatusList, formModel.Filters, MvcAdminConstants.Value, MvcAdminConstants.Text, MvcAdminConstants.AffiliatedApprovalStatusName);

                model.GridModel = gridModel;
                model.GridModel.TotalRecordCount = (model.TotalResults > 0) ? model.TotalResults : model.Customers.Count;

                model.IsEnableCustomerPricing = Equals(EnableCustomerPricing, null) ? false : EnableCustomerPricing.Value;
                return ActionView("_PRFTNotAssociatedCustomerList", model);
            }
            return ActionView("_PRFTNotAssociatedCustomerList", new CustomerListViewModel());
        }

        [HttpPost]
        public ActionResult SaveAssociatedCustomer(int id, string customerAccountIds)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();
            if (!Equals(id, null) && !Equals(customerAccountIds, null))
            {
                status = _customerAgent.SaveAssociateCustomer(id, customerAccountIds);
                message = status ? ZnodeResources.SaveMessage : ZnodeResources.SaveErrorMessage;
                TempData[MvcAdminConstants.Notifications] = status ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.SaveErrorMessage, NotificationType.error);
            }
            return Json(new { sucess = status, message = message, id = id, isFadeOut = isFadeOut });
        }
        #endregion

        #endregion

    }
}