﻿using Elmah;
using Newtonsoft.Json;
using Resources;
using System;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Web.Mvc;
using System.Web.Routing;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.Models;

namespace Znode.Engine.MvcAdmin.Controllers
{
    public class BaseController : Controller
    {
        #region Public variable
        public const string Notifications = "Notifications";
        #endregion

        #region Property
        public int? PortalId { get; set; }
        public bool? EnableCustomerPricing { get; set; }
        #endregion

        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            this.ControllerContext = new ControllerContext(requestContext, this);
            PortalId = PortalAgent.CurrentPortal.PortalId;
            EnableCustomerPricing = PortalAgent.CurrentPortal.EnableCustomerPricing;
        }


        #region ActionView
        public ActionResult ActionView()
        {
            if (Request.IsAjaxRequest())
            {
                return PartialView();
            }
            return View();
        }

        public ActionResult ActionView(IView view)
        {
            if (Request.IsAjaxRequest())
            {
                return PartialView();
            }
            return View(view);
        }

        public ActionResult ActionView(object model)
        {
            if (Request.IsAjaxRequest())
            {
                return PartialView(model);
            }
            return View(model);
        }

        public ActionResult ActionView(string viewName)
        {
            if (Request.IsAjaxRequest())
            {
                return PartialView(viewName);
            }
            return View(viewName);
        }

        public ActionResult ActionView(IView view, object model)
        {
            if (Request.IsAjaxRequest())
            {
                return PartialView();
            }
            return View(view, model);
        }

        public ActionResult ActionView(string viewName, object model)
        {
            if (Request.IsAjaxRequest())
            {
                return PartialView(viewName, model);
            }
            return View(viewName, model);
        }

        public ActionResult ActionView(string viewName, string masterName)
        {
            if (Request.IsAjaxRequest())
            {
                return PartialView();
            }
            return View(viewName, masterName);
        }

        public ActionResult ActionView(string viewName, string masterName, object model)
        {
            if (Request.IsAjaxRequest())
            {
                return PartialView();
            }
            return View(viewName, masterName, model);
        }
        #endregion

        #region Protected Method

        /// <summary>
        /// To show Notification message 
        /// </summary>
        /// <param name="message">string message to show on page</param>
        /// <param name="type">enum type of message</param>
        /// <param name="isFadeOut">bool isFadeOut true/false</param>
        /// <param name="fadeOutMilliSeconds">int fadeOutMilliSeconds</param>
        /// <returns>string Json format of message box</returns>
        protected string GenerateNotificationMessages(string message, NotificationType type, int fadeOutMilliSeconds = 5000)
        {
            MessageBoxModel msgObj = new MessageBoxModel();
            msgObj.Message = message;
            msgObj.Type = type.ToString();
            msgObj.IsFadeOut = CheckIsFadeOut();
            msgObj.FadeOutMilliSeconds = fadeOutMilliSeconds;
            return JsonConvert.SerializeObject(msgObj);
        }
        #endregion

        #region public Method

        /// <summary>
        /// To get IsFadeOut status from web config file, 
        /// if NotificationMessagesIsFadeOut key not found in config then it will returns false 
        /// </summary>
        /// <returns>return true/false</returns>
        public bool CheckIsFadeOut()
        {
            bool isFadeOut = false;
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["NotificationMessagesIsFadeOut"]))
            {
                isFadeOut = Convert.ToBoolean(ConfigurationManager.AppSettings["NotificationMessagesIsFadeOut"]);
            }
            else
            {
                //To do : need to log this in log file after common log functionality is ready.
                //ZNodeLogging.LogMessage(ZnodeResources.ConfigKeyNotificationMessagesIsFadeOutMissing);
            }
            return isFadeOut;
        }

        /// <summary>
        /// Log error to Elmah
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="contextualMessage"></param>
        public void ExceptionHandler(Exception ex, string contextualMessage = null)
        {
            if (contextualMessage != null)
            {
                var annotatedException = new Exception(contextualMessage, ex);
                Elmah.ErrorSignal.FromCurrentContext().Raise(annotatedException);
            }
            else
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }

            if (ex is ZnodeException)
            {
                ZnodeException exception = ex as ZnodeException;
                ModelState.AddModelError(string.Empty, string.IsNullOrEmpty(exception.ErrorMessage) == true ? ZnodeResources.GenericErrorMessage : exception.ErrorMessage);
            }
            else
            {
                ModelState.AddModelError(string.Empty, ZnodeResources.GenericErrorMessage);
            }
        }

        //Method converts the Partial view result to string.
        public string RenderRazorViewToString(string viewName, object model)
        {
            ViewData.Model = model;
            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext,
                viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View,
                ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }

        /// <summary>
        /// To add portal Ids in FilterCollection for franchisee admin
        /// </summary>
        /// <param name="filters">FilterCollection filters</param>
        /// <returns>returns FilterCollection with portal ids</returns>
        public FilterCollection GetAuthorizedPortalIdFilter(FilterCollection filters)
        {
            ProfileCommonAgent profileAgent = new ProfileCommonAgent();
            string portalIds = profileAgent.GetProfileStoreAccess(HttpContext.User.Identity.Name).ProfileStoreAccess;
            if (!string.IsNullOrEmpty(portalIds))
            {
                if (!Equals(portalIds, "0"))
                {
                    if (Equals(filters, null))
                    {
                        filters = new FilterCollection();
                    }
                    filters.Add(new FilterTuple(FilterKeys.PortalId, FilterOperators.Equals, portalIds));                  
                }
            }
            return filters;
        }

        /// <summary>
        /// To add account Id in FilterCollection for mall admin
        /// </summary>
        /// <param name="filters">FilterCollection filters</param>
        /// <returns>returns FilterCollection with account id</returns>
        public FilterCollection GetAuthorizedAccountIdFilter(FilterCollection filters)
        {
            AccountAgent accountAgent = new AccountAgent();
            var account = accountAgent.GetAccounts();
            if (!Equals(account, null))
            {
                if (account.AccountId > 0)
                {
                    if (Equals(filters, null))
                    {
                        filters = new FilterCollection();
                    }
                    filters.Add(new FilterTuple(FilterKeys.AccountId, FilterOperators.Equals, account.AccountId.ToString()));                   
                }
            }
            return filters;
        }

        /// <summary>
        /// To Get the Authorized portal Id for the login user.
        /// </summary>
        /// <returns>Return the portal Id.</returns>
        public int? GetAuthorizedPortalId()
        {
            int portalId = 0;
            ProfileCommonAgent profileAgent = new ProfileCommonAgent();
            string portalIds = profileAgent.GetProfileStoreAccess(HttpContext.User.Identity.Name).ProfileStoreAccess;
            if (!string.IsNullOrEmpty(portalIds) && int.TryParse(portalIds.Split(',')[0], out portalId))
            {
                return portalId;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        ///  To Get the Authorized account Id for the login user.
        /// </summary>
        /// <returns>returns AccountId</returns>
        public int? GetAuthorizedAccountId()
        {
            AccountAgent accountAgent = new AccountAgent();
            var account = accountAgent.GetAccounts();
            if (!Equals(account, null) && account.AccountId > 0)
            {
                return account.AccountId;
            }
            else 
            {
                return null;
            }
        }

        #endregion
    }
}
