﻿using Resources;
using System;
using System.Web.Mvc;
using Znode.Engine.Api.Models.Enum;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Controllers.Advanced
{
    public class ProviderEngineController : BaseController
    {
        #region Private Variables
        private readonly IProviderEngineAgent _providerEngineAgent;
        private string ProviderEnginePartialView = "_PromotionTypeList";
        private string createEditView = MvcAdminConstants.CreateEditView;
        private string ManageProviderEngineAction = "ManageProviderEngine";
        #endregion

        #region Constructor
        public ProviderEngineController()
        {
            _providerEngineAgent = new ProviderEngineAgent();
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Manage Provider Engine
        /// </summary>
        /// <returns>Return to Manage Provider Engine View</returns>
        public ActionResult ManageProviderEngine(string tabMode = "")
        {
            if (!string.IsNullOrEmpty(tabMode))
            {
                TempData[MvcAdminConstants.SetTabMode] = tabMode;
            }
            return ActionView();
        }

        #region Promotion Type
        /// <summary>
        /// Get all Promotion Types
        /// </summary>
        /// <param name="model">FilterCollectionDataModel model</param>
        /// <returns>Return Promitions Type list view</returns>
        public ActionResult GetPromotionTypes([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model)
        {
            PromotionTypeListViewModel promotionTypes = _providerEngineAgent.GetPromotionTypes(model.Filters, model.SortCollection, model.Page, model.RecordPerPage);
            var gridModel = FilterHelpers.GetDynamicGridModel(model, promotionTypes.PromotionTypes, Convert.ToInt32(ListType.PromotionTypeList));
            gridModel.TotalRecordCount = promotionTypes.TotalResults;
            return ActionView(ProviderEnginePartialView, gridModel);
        }

        /// <summary>
        ///Delete Promotion Type 
        /// </summary>
        /// <param name="id">int PromotionTypeId</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult DeletePromotionType(int id)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();
            if (id > 0)
            {
                status = _providerEngineAgent.DeletePromotionType(id, out message);
                message = status ? ZnodeResources.DeleteMessage : message;
            }
            return Json(new { sucess = status, message = message, id = id, isFadeOut = isFadeOut });
        }

        /// <summary>
        /// Edit Promotion Type
        /// </summary>
        /// <param name="id">int PromotionTypeId</param>
        /// <returns>Return Promotion Type CreateEdit View</returns>
        public ActionResult EditPromotionType(int id)
        {
            if (id > 0)
            {
                ViewBag.TabMode = SelectedTab.PromotionType;
                ProviderEngineViewModel providerEngine = new ProviderEngineViewModel();
                providerEngine = _providerEngineAgent.GetPromotionType(id);
                return View(createEditView, providerEngine);
            }
            return RedirectToAction(ManageProviderEngineAction, new { tabMode = SelectedTab.PromotionType });
        }

        /// <summary>
        /// Edit Promotion Type
        /// </summary>
        /// <param name="model">ProviderEngineViewModel model</param>
        /// <returns>Return Promotion Type list</returns>
        [HttpPost]
        public ActionResult EditPromotionType(ProviderEngineViewModel model)
        {
            if (ModelState.IsValid)
            {
                TempData[MvcAdminConstants.Notifications] = _providerEngineAgent.UpdatePromotionType(model) ? GenerateNotificationMessages(ZnodeResources.UpdateMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.UpdateErrorMessage, NotificationType.error);
                return RedirectToAction(ManageProviderEngineAction, new { tabMode = SelectedTab.PromotionType });
            }
            return View(createEditView, model);
        }

        /// <summary>
        /// Get Promotion Type by class name
        /// </summary>
        /// <param name="ClassName">string ClassName</param>
        /// <returns>Json Result</returns>
        [HttpGet]
        public JsonResult GetPromotionTypeByClassName(string Name)
        {
            ProviderEngineViewModel providerEngineViewModel = _providerEngineAgent.GetPromotionTypeByClassName(Name);
            return Json(providerEngineViewModel, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Create Promotion Type
        /// </summary>
        /// <returns>Return Promotion Type CreateEdit View </returns>
        [HttpGet]
        public ActionResult CreatePromotionType()
        {
            ProviderEngineViewModel promotionTypeViewModel = new ProviderEngineViewModel();
            _providerEngineAgent.BindAvailablePromotionTypes(promotionTypeViewModel);
            promotionTypeViewModel.IsActive = true;
            return View(createEditView, promotionTypeViewModel);
        }

        /// <summary>
        /// Create Promotion Type
        /// </summary>
        /// <param name="model">ProviderEngineViewModel model</param>
        /// <returns>Return Promotion Type list</returns>
        [HttpPost]
        public ActionResult CreatePromotionType(ProviderEngineViewModel model)
        {
            if (ModelState.IsValid)
            {
                TempData[MvcAdminConstants.Notifications] = _providerEngineAgent.CreatePromotionType(model) ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.SaveErrorMessage, NotificationType.error);
                return RedirectToAction(ManageProviderEngineAction, new { tabMode = SelectedTab.PromotionType });
            }
            _providerEngineAgent.BindAvailablePromotionTypes(model);
            return View(createEditView, model);
        }

        #endregion

        #region Tax Rule Type

        /// <summary>
        /// Get Tax Rule Types
        /// </summary>
        /// <param name="model">FilterCollectionDataModel model</param>
        /// <returns>Return Tax Rule Type list view</returns>
        public ActionResult GetTaxRuleTypes([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model)
        {
            TaxRuleTypeListViewModel taxRuleType = _providerEngineAgent.GetTaxRuleTypes(model.Filters, model.SortCollection, model.Page, model.RecordPerPage);
            var gridModel = FilterHelpers.GetDynamicGridModel(model, taxRuleType.TaxRuleType, Convert.ToInt32(ListType.TaxRuleTypeList));
            gridModel.TotalRecordCount = taxRuleType.TotalResults;
            return ActionView(ProviderEnginePartialView, gridModel);
        }

        /// <summary>
        ///Delete Tax Rule Type 
        /// </summary>
        /// <param name="id">int TaxRuleTypeId</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult DeleteTaxRuleType(int id)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();
            if (id > 0)
            {
                status = _providerEngineAgent.DeleteTaxRuleType(id, out message);
                message = status ? ZnodeResources.DeleteMessage : message;
            }
            return Json(new { sucess = status, message = message, id = id, isFadeOut = isFadeOut });
        }

        /// <summary>
        /// Edit Tax Rule Type
        /// </summary>
        /// <param name="id">int taxRuleTypeId</param>
        /// <returns>Return Tax Rule Type CreateEdit View</returns>
        public ActionResult EditTaxRuleType(int id)
        {
            if (id > 0)
            {
                ViewBag.TabMode = SelectedTab.TaxRuleType;
                ProviderEngineViewModel providerEngine = new ProviderEngineViewModel();
                providerEngine = _providerEngineAgent.GetTaxRuleType(id);
                return View(createEditView, providerEngine);
            }
            return RedirectToAction(ManageProviderEngineAction, new { tabMode = SelectedTab.TaxRuleType });
        }

        /// <summary>
        /// Edit Tax Rule Type
        /// </summary>
        /// <param name="model">ProviderEngineViewModel model</param>
        /// <returns>Return Tax Rule Type list</returns>
        [HttpPost]
        public ActionResult EditTaxRuleType(ProviderEngineViewModel model)
        {
            if (ModelState.IsValid)
            {
                TempData[MvcAdminConstants.Notifications] = _providerEngineAgent.UpdateTaxRuleType(model) ? GenerateNotificationMessages(ZnodeResources.UpdateMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.UpdateErrorMessage, NotificationType.error);
                return RedirectToAction(ManageProviderEngineAction, new { tabMode = SelectedTab.TaxRuleType });
            }
            return View(createEditView, model);
        }

        /// <summary>
        /// Get Tax Rule Type by class name
        /// </summary>
        /// <param name="ClassName">string ClassName</param>
        /// <returns>Json Result</returns>
        public JsonResult GetTaxRuleTypeByClassName(string Name)
        {
            ProviderEngineViewModel providerEngineViewModel = _providerEngineAgent.GetTaxRuleTypeByClassName(Name);
            return Json(providerEngineViewModel, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Create Tax Rule Type
        /// </summary>
        /// <returns>Return Tax Rule Type CreateEdit View</returns>
        [HttpGet]
        public ActionResult CreateTaxRuleType()
        {
            ProviderEngineViewModel promotionTypeViewModel = new ProviderEngineViewModel();
            _providerEngineAgent.BindAvailableTaxRuleType(promotionTypeViewModel);
            promotionTypeViewModel.IsActive = true;
            return View(createEditView, promotionTypeViewModel);
        }

        /// <summary>
        /// Create Tax Rule Type
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Return Tax Rule Type CreateEdit View</returns>
        [HttpPost]
        public ActionResult CreateTaxRuleType(ProviderEngineViewModel model)
        {
            if (ModelState.IsValid)
            {
                TempData[MvcAdminConstants.Notifications] = _providerEngineAgent.CreateTaxRuleType(model) ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.SaveErrorMessage, NotificationType.error);
                return RedirectToAction(ManageProviderEngineAction, new { tabMode = SelectedTab.TaxRuleType });
            }
            _providerEngineAgent.BindAvailableTaxRuleType(model);
            return View(createEditView, model);
        }

        #endregion

        #region Supplier Type
        /// <summary>
        /// Get Supplier Types
        /// </summary>
        /// <param name="model">FilterCollectionDataModel model</param>
        /// <returns>Return Supplier Type list view</returns>
        public ActionResult GetSupplierTypes([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model)
        {
            SupplierTypeListViewModel supplierType = _providerEngineAgent.GetSupplierTypes(model.Filters, model.SortCollection, model.Page, model.RecordPerPage);
            var gridModel = FilterHelpers.GetDynamicGridModel(model, supplierType.SupplierType, Convert.ToInt32(ListType.SupplierTypeList));
            gridModel.TotalRecordCount = supplierType.TotalResults;
            return ActionView(ProviderEnginePartialView, gridModel);
        }

        /// <summary>
        /// Delete Supplier Types
        /// </summary>
        /// <param name="id">int SupplierTypeId</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult DeleteSupplierType(int id)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();
            if (id > 0)
            {
                status = _providerEngineAgent.DeleteSupplierType(id, out message);
                message = status ? ZnodeResources.DeleteMessage : message;
            }
            return Json(new { sucess = status, message = message, id = id, isFadeOut = isFadeOut });
        }

        /// <summary>
        /// Edit Supplier Type
        /// </summary>
        /// <param name="id">int supplierTypeId</param>
        /// <returns>Return Supplier Type CreateEdit View</returns>
        public ActionResult EditSupplierType(int id)
        {
            if (id > 0)
            {
                ViewBag.TabMode = SelectedTab.SupplierType;
                ProviderEngineViewModel providerEngine = new ProviderEngineViewModel();
                providerEngine = _providerEngineAgent.GetSupplierType(id);
                return View(createEditView, providerEngine);
            }
            return RedirectToAction(ManageProviderEngineAction, new { tabMode = SelectedTab.SupplierType });
        }

        /// <summary>
        /// Edit Supplier Type
        /// </summary>
        /// <param name="model">ProviderEngineViewModel model</param>
        /// <returns>Return Supplier Type list</returns>
        [HttpPost]
        public ActionResult EditSupplierType(ProviderEngineViewModel model)
        {
            if (ModelState.IsValid)
            {
                TempData[MvcAdminConstants.Notifications] = _providerEngineAgent.UpdateSupplierType(model) ? GenerateNotificationMessages(ZnodeResources.UpdateMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.UpdateErrorMessage, NotificationType.error);
                return RedirectToAction(ManageProviderEngineAction, new { tabMode = SelectedTab.SupplierType });
            }
            return View(createEditView, model);
        }

        /// <summary>
        /// Get Supplier Type by class name
        /// </summary>
        /// <param name="ClassName">string ClassName</param>
        /// <returns>Json Result</returns>
        public JsonResult GetSupplierTypeByClassName(string Name)
        {
            ProviderEngineViewModel providerEngineViewModel = _providerEngineAgent.GetSupplierTypeByClassName(Name);
            return Json(providerEngineViewModel, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Create Supplier Type
        /// </summary>
        /// <returns>Return Supplier Type CreateEdit View</returns>
        [HttpGet]
        public ActionResult CreateSupplierType()
        {
            ProviderEngineViewModel promotionTypeViewModel = new ProviderEngineViewModel();
            _providerEngineAgent.BindAvailableSupplierType(promotionTypeViewModel);
            promotionTypeViewModel.IsActive = true;
            return View(createEditView, promotionTypeViewModel);
        }

        /// <summary>
        /// Create Supplier Type
        /// </summary>
        /// <param name="model">ProviderEngineViewModel model</param>
        /// <returns>Return Supplier Type list</returns>
        [HttpPost]
        public ActionResult CreateSupplierType(ProviderEngineViewModel model)
        {
            if (ModelState.IsValid)
            {
                TempData[MvcAdminConstants.Notifications] = _providerEngineAgent.CreateSupplierType(model) ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.SaveErrorMessage, NotificationType.error);
                return RedirectToAction(ManageProviderEngineAction, new { tabMode = SelectedTab.SupplierType });
            }
            _providerEngineAgent.BindAvailableSupplierType(model);
            return View(createEditView, model);
        }

        #endregion

        #region Shipping Type
        /// <summary>
        /// Get Shipping Types
        /// </summary>
        /// <param name="model">FilterCollectionDataModel model</param>
        /// <returns>Return Shipping Type list view</returns>
        public ActionResult GetShippingTypes([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model)
        {
            ShippingTypeListViewModel shippingType = _providerEngineAgent.GetShippingTypes(model.Filters, model.SortCollection, model.Page, model.RecordPerPage);
            var gridModel = FilterHelpers.GetDynamicGridModel(model, shippingType.ShippingType, Convert.ToInt32(ListType.ShippingTypeList));
            gridModel.TotalRecordCount = shippingType.TotalResults;
            return ActionView(ProviderEnginePartialView, gridModel);
        }

        /// <summary>
        /// Delete Shipping Types
        /// </summary>
        /// <param name="id">int ShippingTypeId</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult DeleteShippingRuleType(int id)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();
            if (id > 0)
            {
                status = _providerEngineAgent.DeleteShippingType(id, out message);
                message = status ? ZnodeResources.DeleteMessage : message;
            }
            return Json(new { sucess = status, message = message, id = id, isFadeOut = isFadeOut });
        }

        /// <summary>
        /// Edit Shipping Type
        /// </summary>
        /// <param name="id">int shippingTypeId</param>
        /// <returns>Return Shipping Type CreateEdit View</returns>
        public ActionResult EditShippingRuleType(int id)
        {
            if (id > 0)
            {
                ViewBag.TabMode = SelectedTab.ShippingType;
                ProviderEngineViewModel providerEngine = new ProviderEngineViewModel();
                providerEngine = _providerEngineAgent.GetShippingType(id);
                return View(createEditView, providerEngine);
            }
            return RedirectToAction(ManageProviderEngineAction, new { tabMode = SelectedTab.ShippingType });
        }

        /// <summary>
        /// Edit Shipping Type
        /// </summary>
        /// <param name="model">ProviderEngineViewModel model</param>
        /// <returns>Return Shipping Type list</returns>
        [HttpPost]
        public ActionResult EditShippingRuleType(ProviderEngineViewModel model)
        {
            if (ModelState.IsValid)
            {
                TempData[MvcAdminConstants.Notifications] = _providerEngineAgent.UpdateShippingType(model) ? GenerateNotificationMessages(ZnodeResources.UpdateMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.UpdateErrorMessage, NotificationType.error);
                return RedirectToAction(ManageProviderEngineAction, new { tabMode = SelectedTab.ShippingType });
            }
            return View(createEditView, model);
        }

        /// <summary>
        /// Get Shipping Type by class name
        /// </summary>
        /// <param name="ClassName">string ClassName</param>
        /// <returns>Json Result</returns>
        public JsonResult GetShippingTypeByClassName(string Name)
        {
            ProviderEngineViewModel providerEngineViewModel = _providerEngineAgent.GetShippingTypeByClassName(Name);
            return Json(providerEngineViewModel, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Create Shipping Type
        /// </summary>
        /// <returns>Return Shipping Type CreateEdit View</returns>
        [HttpGet]
        public ActionResult CreateShippingType()
        {
            ProviderEngineViewModel promotionTypeViewModel = new ProviderEngineViewModel();
            _providerEngineAgent.BindAvailableShippingType(promotionTypeViewModel);
            promotionTypeViewModel.IsActive = true;
            return View(createEditView, promotionTypeViewModel);
        }

        /// <summary>
        /// Create Shipping Type
        /// </summary>
        /// <param name="model">ProviderEngineViewModel model</param>
        /// <returns>Return Shipping Type list</returns>
        [HttpPost]
        public ActionResult CreateShippingType(ProviderEngineViewModel model)
        {
            if (ModelState.IsValid)
            {
                TempData[MvcAdminConstants.Notifications] = _providerEngineAgent.CreateShippingType(model) ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.SaveErrorMessage, NotificationType.error);
                return RedirectToAction(ManageProviderEngineAction, new { tabMode = SelectedTab.ShippingType });
            }
            _providerEngineAgent.BindAvailableShippingType(model);
            return View(createEditView, model);
        }
        #endregion

        #endregion

        #region Enum
        /// <summary>
        /// Enum for Seleted tab
        /// </summary>
        private enum SelectedTab
        {
            PromotionType,
            TaxRuleType,
            SupplierType,
            ShippingType
        }
        #endregion
    }
}