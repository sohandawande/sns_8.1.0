﻿using System.Web.Mvc;
using Znode.Engine.Api.Models.Enum;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Controllers.Advanced
{
    public class ManageSearchIndexController : BaseController
    {
        #region Private Variables

        private readonly IManageSearchIndexAgent _searchIndex;
        private const string SearchIndexList = "ManageSearchIndex";
        private const string logView = "~/Views/ManageSearchIndex/_SearchIndexStatus.cshtml";
        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        public ManageSearchIndexController()
        {
            _searchIndex = new ManageSearchIndexAgent();

        }
        #endregion

        /// <summary>
        /// Get Search index status list
        /// </summary>
        /// <param name="model">FilterCollectionDataModel</param>
        /// <returns>View</returns>
        public ActionResult ManageSearchIndex([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model)
        {
            LuceneIndexListViewModel searchIndexList = _searchIndex.GetLuceneIndexStatusList(model);
            searchIndexList.ReportModel = FilterHelpers.GetDynamicGridModel(model, searchIndexList.LuceneIndexList, (int)ListType.LuceneIndexStatus);
            searchIndexList.ReportModel.TotalRecordCount = searchIndexList.TotalResults;
            return ActionView(searchIndexList);
        }

        /// <summary>
        /// Create Lucene search Index
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult CreateIndex()
        {
            _searchIndex.CreateIndex();
            return RedirectToAction(SearchIndexList);
        }

        /// <summary>
        /// Disable/Enable lucene trigger
        /// </summary>
        /// <param name="TriggerFlag"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DisableTriggers(int TriggerFlag)
        {
            _searchIndex.DisableTriggers(TriggerFlag);
            return RedirectToAction(SearchIndexList);
        }

        /// <summary>
        /// Disable/Enable lucene windows service
        /// </summary>
        /// <param name="ServiceFlag">ServiceFlag</param>
        /// <returns>redirect to action </returns>
        [HttpPost]
        public ActionResult DisableWinservice(int ServiceFlag)
        {
            _searchIndex.DisableWinservice(ServiceFlag);
            return RedirectToAction(SearchIndexList);
        }

        /// <summary>
        /// Get search index server status log list
        /// </summary>
        /// <param name="indexId">index Id</param>
        /// <returns>partial view</returns>
        public ActionResult GetSearchIndexStatus(int indexId)
        {
            var model = _searchIndex.GetSearchIndexStatus(indexId);
            return PartialView(logView, model);
        }

    }
}