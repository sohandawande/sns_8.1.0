﻿using Resources;
using System;
using System.Web.Mvc;
using Znode.Engine.Api.Models.Enum;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.Agents.Agents;
using Znode.Engine.MvcAdmin.Agents.IAgents;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Controllers.Advanced
{
    /// <summary>
    /// This controller will have all the actions for the Store Admin
    /// </summary>
    public class StoreAdminController : BaseController
    {

        #region Private Variables
        private readonly IStoreAdminAgent _storeAdminAgent;
        private readonly IPermissionAgent _permissionAgent;
        private readonly IFranchiseAccountAgent _franchiseAccountAgent;
        #endregion

        #region Constant Variables
        private const string AdminRoleName = "ADMIN";
        private const string DownloadFileName = "StoreAdmins.csv";
        private const string DeleteActionErrorMessageName = "deleted";
        private const string DisableActionErrorMessageName = "disabled";
        private const string PermissionNonEditModeViewName = "_StoreAdminPermissions";
        private const string PermissionEditModeViewName = "_EditStoreAdminPermissions";
        private const string PermissionsViewName = "Permissions";
        private const string ManageAction = "Manage";
        private const string EditAction = "EditAdminDetails";
        #endregion

        #region Constructor
        public StoreAdminController()
        {
            _storeAdminAgent = new StoreAdminAgent();
            _permissionAgent = new PermissionAgent();
            _franchiseAccountAgent = new FranchiseAccountAgent();
        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Add Update the  Store Admin Account
        /// </summary>
        /// <returns>Returns the view of the Store Admin</returns>
        public ActionResult Create()
        {
            return View(MvcAdminConstants.CreateEditView);
        }

        /// <summary>
        /// Create the Store Admin account
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Returns the view of the Store Admin</returns>
        [HttpPost]
        public ActionResult Create(StoreAdminViewModel model)
        {
            if (ModelState.IsValid)
            {
                int accountId = 0;
                string message = string.Empty;
                bool status = _storeAdminAgent.SaveStoreAdmin(model, out accountId, out message);
                TempData[MvcAdminConstants.Notifications] = status ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success) : GenerateNotificationMessages(message, NotificationType.error);
                if (status)
                {
                    return RedirectToAction(ManageAction, new { id = accountId });
                }
            }
            return View(MvcAdminConstants.CreateEditView, model);
        }

        /// <summary>
        /// This method will fetch the list of all the store admins
        /// </summary>
        /// <param name="model">Filtercollection model</param>
        /// <returns>returns the list of store admins</returns>
        public ActionResult List([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model)
        {
            StoreAdminListViewModel storeAdmins = _storeAdminAgent.GetStoreAdmins(AdminRoleName, model.Filters, model.SortCollection, model.Page, model.RecordPerPage);
            ReportModel gridModel = FilterHelpers.GetDynamicGridModel(model, storeAdmins.StoreAdmins, (int)ListType.vw_ZNodeAdminAccounts);

            //Filters for download
            Session[MvcAdminConstants.Filters] = model.Filters;

            FilterHelpers.CreateDropdown(ref gridModel, _storeAdminAgent.GetAllStores(), model.Filters, MvcAdminConstants.PortalId, MvcAdminConstants.StoreName, MvcAdminConstants.StoreNameDropDown);
            FilterHelpers.CreateDropdown(ref gridModel, _storeAdminAgent.GetAllProfiles(), model.Filters, MvcAdminConstants.ProfileId, MvcAdminConstants.ProfileName, MvcAdminConstants.SelectProfileDropdown);
           
            gridModel.TotalRecordCount = storeAdmins.TotalResults;
            return ActionView(MvcAdminConstants.ListView, gridModel);
        }

        /// <summary>
        /// This method will download the Store admin details
        /// </summary>
        /// <param name="viewModel">Filter Collection view method</param>
        /// <returns></returns>
        public ActionResult Download([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel viewModel)
        {
            StoreAdminListViewModel model = new StoreAdminListViewModel();
            model.StoreAdmins = _storeAdminAgent.GetStoreAdmins(AdminRoleName, (Znode.Engine.Api.Client.Filters.FilterCollection)Session[MvcAdminConstants.Filters], model.SortCollection, model.Page, model.RecordPerPage).StoreAdmins;
            _storeAdminAgent.DownloadFile(model, Response, DownloadFileName);
            return Content(string.Empty);
        }

        /// <summary>
        /// This method will delete the existing store admin
        /// </summary>
        /// <param name="id">int Account Id</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Delete(int? id)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();
            if (!Equals(null, id))
            {
                string currentUserName = HttpContext.User.Identity.Name;
                if (_storeAdminAgent.IsCurrentUser(id.Value, currentUserName))
                {
                    message = string.Format(ZnodeResources.DeleteDisableCurrentUserErrorMessage, DeleteActionErrorMessageName);
                }
                else
                {
                    status = _storeAdminAgent.DeleteStoreAdmin(id.Value, out message);
                    message = status ? ZnodeResources.DeleteMessage : message;
                }
            }
            return Json(new { sucess = status, message = message, id = id, isFadeOut = isFadeOut });
        }

        /// <summary>
        /// This method will redirect to the Manage page for Store Administrators.
        /// Where we can set the persmissions and can make different changes for the store admins
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult Manage(int id, [ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model, string tabMode = "")
        {
            if (!string.IsNullOrEmpty(tabMode))
            {
                TempData[MvcAdminConstants.SetTabMode] = tabMode;
            }

            if (id > 0)
            {
                StoreAdminViewModel storeAdmin = new StoreAdminViewModel();
                storeAdmin = _storeAdminAgent.GetStoreAdminByID(id);

                return ActionView(MvcAdminConstants.ManageView, storeAdmin);
            }
            return ActionView(MvcAdminConstants.ManageView);
        }

        /// <summary>
        /// This method will enable or disable the admin account.
        /// </summary>
        /// <param name="id">int Account Id</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult EnableDisable(int? id)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();
            if (!Equals(null, id))
            {
                string currentUserName = HttpContext.User.Identity.Name;
                if (_storeAdminAgent.IsCurrentUser(id.Value, currentUserName))
                {
                    message = string.Format(ZnodeResources.DeleteDisableCurrentUserErrorMessage, DisableActionErrorMessageName);
                }
                else
                {
                    status = _storeAdminAgent.EnableDisableStoreAdmin(id.Value);
                    message = status ? ZnodeResources.DisableMessage : ZnodeResources.DisableErrorMessage;
                }
            }
            return Json(new { sucess = status, message = message, id = id, action = ZnodeResources.DisableText, isFadeOut = isFadeOut });
        }

        /// <summary>
        /// This method will show the Permission and Roles for the relevant account id
        /// </summary>
        /// <param name="id">int Account Id</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Permissions(int id)
        {
            if (id > 0)
            {
                PermissionViewModel permissionModel = new PermissionViewModel();
                permissionModel = _permissionAgent.GetAllRolesAndPermissionsByAccountId(id);

                return ActionView(PermissionNonEditModeViewName, permissionModel);
            }
            return ActionView(PermissionNonEditModeViewName);
        }

        /// <summary>
        /// This method will return the edited view for Roles and Permissions based on Account Id.
        /// </summary>
        /// <param name="id">int account id</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult EditRolesAndPermissions(int id)
        {
            if (id > 0)
            {
                PermissionViewModel permissionModel = new PermissionViewModel();
                permissionModel = _permissionAgent.GetAllRolesAndPermissionsByAccountId(id);

                return ActionView(PermissionEditModeViewName, permissionModel);
            }
            return ActionView(PermissionEditModeViewName);
        }

        /// <summary>
        /// This method will save the changes made for the Roles and Permission for the specific Account Id.
        /// </summary>
        /// <param name="StoreList">List of Portal Ids</param>
        /// <param name="Roles">List of Role Ids</param>
        /// <param name="AccountId">int Account Id</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult EditRolesAndPermissions(int[] StoreList, int[] Roles, int AccountId)
        {
            bool status = _permissionAgent.UpdateRolesAndPermissions(StoreList, Roles, AccountId);
            TempData[MvcAdminConstants.Notifications] = status ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.SaveErrorMessage, NotificationType.error);
            return RedirectToAction(ManageAction, new { id = AccountId, tabMode = SelectedTab.Permissions });
        }

        /// <summary>
        /// This method will redirect to the edit admin details page
        /// </summary>
        /// <param name="id">int Account Id</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult EditAdminDetails(int id)
        {
            if (id > 0)
            {
                StoreAdminViewModel storeAdmin = new StoreAdminViewModel();
                storeAdmin = _storeAdminAgent.GetStoreAdminByID(id);

                return ActionView(MvcAdminConstants.CreateEditView, storeAdmin);
            }
            return ActionView(MvcAdminConstants.CreateEditView);
        }

        /// <summary>
        /// This method will redirect to the edit admin details page
        /// </summary>
        /// <param name="id">int Account Id</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult EditAdminDetails(int AccountId, StoreAdminViewModel model)
        {
            if (ModelState.IsValid)
            {
                bool status = _storeAdminAgent.UpdateStoreAdmin(AccountId, model);
                TempData[MvcAdminConstants.Notifications] = status ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.SaveErrorMessage, NotificationType.error);
                return RedirectToAction(ManageAction, new { @id = model.AccountId });
            }
            return RedirectToAction(MvcAdminConstants.ListView);
        }

        /// <summary>
        /// This method will reset the password for the admin
        /// </summary>
        /// <param name="id">int account id</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ResetPassword(int id)
        {
            if (id > 0)
            {
                var model = _franchiseAccountAgent.ResetPassword(id);
                if (!Equals(model, null) && model.HasError)
                {
                    TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(model.ErrorMessage, Znode.Engine.MvcAdmin.Models.NotificationType.error);
                    return RedirectToAction(EditAction, new { @id = id });
                }
                TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.SuccessResetPassword, Znode.Engine.MvcAdmin.Models.NotificationType.success);
                return RedirectToAction(ManageAction, new { @id = id });
            }
            return RedirectToAction(EditAction, new { @id = id });
        }

        #endregion

        #region Private Enum

        /// <summary>
        /// Enum for Selected tab.
        /// </summary>
        private enum SelectedTab
        {
            General,
            Permissions
        }


        #endregion
    }
}