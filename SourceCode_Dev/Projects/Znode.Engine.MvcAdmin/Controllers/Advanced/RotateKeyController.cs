﻿using Resources;
using System.Web.Mvc;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Models;

namespace Znode.Engine.MvcAdmin.Controllers.Advanced
{
    public class RotateKeyController : BaseController
    {
        #region Private Variables
        private readonly IRotateKeyAgent _rotateKeyAgent;
        private readonly string RotateKeyView = "RotateKey"; 
        #endregion

        #region Public Constructor
        /// <summary>
        /// Constructor For RotateKeyController
        /// </summary>
        public RotateKeyController()
        {
            _rotateKeyAgent = new RotateKeyAgent();
        } 
        #endregion

        #region Action Method
        /// <summary>
        /// Get Method For Ratate Key Landing Page
        /// </summary>
        /// <returns>Rotate Key View</returns>
        public ActionResult Key()
        {
            return View(RotateKeyView);
        }

        /// <summary>
        /// Generate new Rotate Key
        /// </summary>
        /// <param name="id">temporary variables</param>
        /// <returns>boolean status with success or error message</returns>
        [HttpPost]
        public ActionResult Key(int? id)
        {
            string message = string.Empty;
            bool status = _rotateKeyAgent.GenerateRotateKey(out message);
            TempData[MvcAdminConstants.Notifications] = status ? GenerateNotificationMessages(ZnodeResources.RotateKeySuccessMessage, NotificationType.success) : GenerateNotificationMessages(message, NotificationType.error);
            return View(RotateKeyView);
        } 
        #endregion
    }
}