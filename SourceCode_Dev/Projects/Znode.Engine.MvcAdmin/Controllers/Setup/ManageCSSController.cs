﻿using Resources;
using System;
using System.Web.Mvc;
using Znode.Engine.Api.Models.Enum;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Controllers.SetUp
{
    public class ManageCSSController : BaseController
    {

        #region Private Agent variables
        private readonly IManageCSSAgent _manageCSSAgent;
        private readonly ICategoryAgent _categoryAgent;
        #endregion

        /// <summary>
        /// Public constructor of ManageCSSController
        /// </summary>
        public ManageCSSController()
        {
            _manageCSSAgent = new ManageCSSAgent();
            _categoryAgent = new CategoryAgent();
        }


        /// <summary>
        /// This function will return the list of all the Themes.
        /// </summary>
        /// <param name="model">FilterCollectionDataModel</param>
        /// <returns>ActionResult view of theme list</returns>
        public ActionResult List([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model)
        {
            CSSListViewModel cssList = _manageCSSAgent.GetCSSList(model.Filters, model.SortCollection, model.Page, model.RecordPerPage);
            var gridModel = FilterHelpers.GetDynamicGridModel(model, cssList.CSSList, Convert.ToInt32(ListType.CSSList));
            gridModel.TotalRecordCount = cssList.TotalResults;
            FilterHelpers.CreateDropdown(ref gridModel, _manageCSSAgent.GetThemeModelList(), model.Filters, MvcAdminConstants.LabelThemeId, MvcAdminConstants.LabelName, MvcAdminConstants.LabelTheme);
            return ActionView(gridModel);
        }

        /// <summary>
        /// To show add new CSS view
        /// </summary>
        /// <returns>ActionResult view of create css</returns>   
        [HttpGet]
        public ActionResult Create()
        {
            CSSViewModel model = new CSSViewModel();

            model.ThemeList = _categoryAgent.BindThemeList();
            return ActionView(MvcAdminConstants.CreateEditView, model);
        }

        /// <summary>
        /// To save css details
        /// </summary>
        /// <param name="model">CSSViewModel</param>
        /// <returns>returns css view model to the view</returns>
        [HttpPost]
        public ActionResult Create(CSSViewModel model)
        {
            if (!Equals(model, null))
            {
                string message = string.Empty;
                bool status = _manageCSSAgent.CreateCSS(model, out message);
                if (!status)
                {
                    TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(message, NotificationType.error);
                    return RedirectToAction(MvcAdminConstants.CreateView);
                }
                else
                {
                    TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success);
                    return RedirectToAction(MvcAdminConstants.ListView);
                }
            }
            return View(MvcAdminConstants.CreateEditView, model);
        }

        /// <summary>
        /// To Show edit page by css id
        /// </summary>
        /// <param name="id">css ID</param>
        ///<returns>ActionResult view of edit css</returns>
        [HttpGet]
        public ActionResult Edit(int? id)
        {
            CSSViewModel model = new CSSViewModel();
            if (id > 0)
            {
                model = _manageCSSAgent.GetCSSById(id.Value);
                model.ThemeList = _categoryAgent.BindThemeList();
            }
            return View(MvcAdminConstants.CreateEditView, model);
        }

        /// <summary>
        /// To update CSS data
        /// </summary>
        /// <param name="model">CSS view model</param>
        /// <returns>returns css view model to the view</returns>
        [HttpPost]
        public ActionResult Edit(CSSViewModel model)
        {
            if (!Equals(model, null))
            {
                string message = string.Empty;
                bool status = _manageCSSAgent.UpdateCSS(model, out message);
                if (!status)
                {
                    TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(message, NotificationType.error);
                    return RedirectToAction(MvcAdminConstants.EditAction, new { @id = model.ThemeId });
                }
                else
                {
                    TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success);
                    return RedirectToAction(MvcAdminConstants.ListView);
                }
            }
            return View(MvcAdminConstants.CreateEditView, model);
        }

        /// <summary>
        /// This action method delete the css
        /// </summary>
        /// <param name="id">css ID</param>
        /// <returns>returns Json result</returns>
        [HttpPost]
        public JsonResult Delete(int? id)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();
            if (!Equals(null, id))
            {
                status = _manageCSSAgent.DeleteCSS(id.Value, out message);
                message = status ? ZnodeResources.DeleteMessage : message;
            }
            return Json(new { sucess = status, message = message, id = id, isFadeOut = isFadeOut });
        }
    }
}