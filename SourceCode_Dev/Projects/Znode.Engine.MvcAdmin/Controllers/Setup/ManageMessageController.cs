﻿using Resources;
using System;
using System.Net;
using System.Web.Mvc;
using Znode.Engine.Api.Models.Enum;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Models;
using Znode.Engine.MvcAdmin.ViewModels;
namespace Znode.Engine.MvcAdmin.Controllers.SetUp
{
    /// <summary>
    /// Manage Message Controller
    /// </summary>
    public class ManageMessageController : BaseController
    {
        #region Private Variables

        private readonly IMessageConfigAgent _messageConfigAgent;
        private readonly IPortalAgent _portalAgent;
        private int localeId = MvcAdminConstants.LocaleId;
        private string createEditView = MvcAdminConstants.CreateEditView;
        private string messageList = MvcAdminConstants.ListView;
        private string portalId = "PortalId";
        private string storeName = "StoreName";

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default Constructor
        /// </summary>
        public ManageMessageController()
        {
            _messageConfigAgent = new MessageConfigAgent();
            _portalAgent = new PortalAgent();
        }

        #endregion

        #region Action Methods

        /// <summary>
        /// Gets Message list
        /// </summary>
        /// <returns>Returns ActionResult</returns>
        public ActionResult List([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model)
        {
            ManageMessageListViewModel list = _messageConfigAgent.GetMessageConfigs(model.Filters, model.SortCollection, model.Page, model.RecordPerPage);
            var message = _messageConfigAgent.GetPortalsList();
            var gridModel = FilterHelpers.GetDynamicGridModel(model, list.ManageMessage, Convert.ToInt32(ListType.ManageMessageList));
            FilterHelpers.CreateDropdown(ref gridModel, message, model.Filters, portalId, storeName, ZnodeResources.LabelStoreName);
            gridModel.TotalRecordCount = list.TotalResults;
            return ActionView(gridModel);
        }

        /// <summary>
        /// Add New Message
        /// </summary>
        /// <param name="model">ManageMessageViewModel</param>
        /// <param name="PortalId">PortalId</param>
        /// <returns>Returns ActionResult</returns>
        public ActionResult Create()
        {
            var portalList = _messageConfigAgent.GetPortalsList();
            var model = new ManageMessageViewModel() { Portal = portalList };
            return View(createEditView, model);
        }

        /// <summary>
        /// Adds new message to list
        /// </summary>
        /// <param name="model">ManageMessageViewModel</param>
        /// <param name="portalId">portalId</param>
        /// <returns>Returns ActionResult</returns>
        [HttpPost]
        public ActionResult Create(ManageMessageViewModel model, int portalId)
        {
            if (ModelState.IsValid)
            {
                model.Value = model.Value.Replace("<p>", string.Empty).Replace("</p>", string.Empty);
                model.Value = WebUtility.HtmlDecode(model.Value);
                string message = string.Empty;
                model.LocaleId = localeId;
                model.PortalId = portalId;
                model.MessageTypeId = Convert.ToInt32(MessageType.MessageBlock);
                bool status = _messageConfigAgent.SaveMessageConfig(model, out message);
                TempData[MvcAdminConstants.Notifications] = status ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success) : GenerateNotificationMessages(message, NotificationType.error);
                return RedirectToAction(messageList);
            }
            var portalList = _messageConfigAgent.GetPortalsList();
            portalList.Insert(0, new PortalViewModel() { PortalId = 0, StoreName = ZnodeResources.LabelAllStores });
            model.Portal = portalList;
            return View(createEditView, model);
        }

        /// <summary>
        /// Get Message to be edited on the basis of messageConfigId
        /// </summary>
        /// <param name="id">messageConfigId</param>
        /// <param name="model">ManageMessageViewModel</param>
        /// <returns>Returns ActionResult</returns>
        [HttpGet]
        public ActionResult Edit(int id)
        {
            ManageMessageViewModel model = new ManageMessageViewModel();
            if (!id.Equals(null))
            {
                model = _messageConfigAgent.GetMessageConfig(id);
                model.Value = WebUtility.HtmlEncode(model.Value);
                model.Portal = _messageConfigAgent.GetPortalsList();
                model.Portal.Insert(0, new PortalViewModel() { PortalId = 0, StoreName = ZnodeResources.LabelAllStores });
                return View(createEditView, model);
            }
            return View(createEditView);
        }

        /// <summary>
        /// Edit message and add edited message to the list
        /// </summary>
        /// <param name="model">ManageMessageViewModel</param>
        /// <param name="configid">configid</param>
        /// <param name="localeid">localeid</param>
        /// <param name="editportalId">messageTypeId</param>
        /// <returns>Returns List with edited message</returns>
        [HttpPost]
        public ActionResult Edit(ManageMessageViewModel model, int configid, int editportalId)
        {
            if (ModelState.IsValid)
            {
                model.Value = model.Value.Replace("<p>", string.Empty).Replace("</p>", string.Empty);
                model.Value = WebUtility.HtmlDecode(model.Value);
                model.MessageConfigId = configid;
                model.PortalId = editportalId;
                var status = _messageConfigAgent.UpdateMessageConfig(model.MessageConfigId, model);
                TempData[MvcAdminConstants.Notifications] = status ? GenerateNotificationMessages(ZnodeResources.UpdateMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.UpdateErrorMessage, NotificationType.error);
            }
            return RedirectToAction(messageList);
        }

        /// <summary>
        /// Delete message by message Config Id
        /// </summary>
        /// <param name="id">integer message Config Id</param>
        /// <returns>Returns JsonResult</returns>
        [HttpPost]

        public JsonResult Delete(int id)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();
            if (!Equals(null, id))
            {
                status = _messageConfigAgent.DeleteMessageConfig(id);
                message = status ? ZnodeResources.DeleteMessage : ZnodeResources.DeleteErrorMessage;
            }
            return Json(new { sucess = status, message = message, id = id, isFadeOut = isFadeOut });
        }
        #endregion
    }
}