﻿using Resources;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Enum;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Controllers
{
    public class CategoryController : BaseController
    {
        #region Private Variables
        private readonly ICategoryAgent _categoryAgent;
        private readonly ICatalogAgent _catalogAgent;
        private readonly IPortalCatalogAgent _portalCatalogAgent;
        private readonly IProductAgent _productAgent;
        private readonly IProductCategoryAgent _productCategoryAgent;
        private string createEditView = MvcAdminConstants.CreateEditView;
        private const string ProductPageType = "Product";

        //PRFT Custom Code: Start
        private readonly IManufacturersAgent _manufacturersAgent;
        //PRFT Custom Code: End

        #endregion

        #region Constructor
        /// <summary>
        /// Constructor for Category controller
        /// </summary>
        public CategoryController()
        {
            _categoryAgent = new CategoryAgent();
            _productAgent = new ProductAgent();
            _portalCatalogAgent = new PortalCatalogAgent();
            _productCategoryAgent = new ProductCategoryAgent();
            _catalogAgent = new CatalogAgent();
            
            //PRFT Custom Code: Start
            _manufacturersAgent = new ManufacturersAgent();
            //PRFT Custom Code: End
        }
        #endregion

        #region Public Action Methods

        /// <summary>
        /// This function will return the list of all the Catalogs.
        /// </summary>
        /// <returns>ActionResult view of catalog list</returns>
        public ActionResult List([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model)
        {

            //PRFT Custom Code: Start
            TempData["IsBrand"] = false;
            foreach (var filter in model.Filters)
            {
                if (Equals(filter.FilterName, "isManufacturer"))
                {
                    if (filter.FilterValue.Contains("true"))
                    {
                        TempData["IsBrand"] = true;
                        break;
                    }
                }
            }
            //PRFT Custom Code: End
            
            CatalogAssociatedCategoriesListViewModel categoryList = _categoryAgent.GetCategoryList(model.Filters, model.SortCollection, model.Page, model.RecordPerPage);
            ReportModel gridModel = FilterHelpers.GetDynamicGridModel(model, categoryList.AssociatedCategories, Convert.ToInt32(ListType.vw_ZNodeSearchCategories));
            int portalId = GetAuthorizedPortalId().GetValueOrDefault();
            if (portalId > 0)
            {
                List<PortalCatalogViewModel> catalogs = _portalCatalogAgent.GetCatalogListByUserName();
                FilterHelpers.CreateDropdown(ref gridModel, catalogs, model.Filters, MvcAdminConstants.LabelCatalogId, MvcAdminConstants.CatalogName, MvcAdminConstants.LabelCatalog);
            }
            else
            {
                List<CatalogModel> catalogs = _productAgent.Getcatalog();
                FilterHelpers.CreateDropdown(ref gridModel, catalogs, model.Filters, MvcAdminConstants.LabelCatalogId, MvcAdminConstants.LabelName, MvcAdminConstants.LabelCatalog);
            }

            gridModel.TotalRecordCount = categoryList.TotalResults;
            return ActionView(gridModel);
        }

        /// <summary>
        /// To show add new category page
        /// </summary>
        /// <returns>add new category page</returns>
        [HttpGet]
        public ActionResult Create()
        {
            CategoryViewModel model = new CategoryViewModel();
            model.ChildCategories = true;
            return View(createEditView, model);
        }

        /// <summary>
        /// To save category details
        /// </summary>
        /// <param name="model">Category View Model</param>
        /// <returns>status true/false</returns>
        [HttpPost]
        public ActionResult Create(CategoryViewModel model)
        {
            if (ModelState.IsValid)
            {

                //PRFT Custom Code: Start
                if (model.IsManufacturer)
                {
                    var manufacturerModel = AddCategoryAsManufacturer(model);
                    if (manufacturerModel != null)
                    {
                        model.Custom1 = manufacturerModel.ManufacturerId.ToString();
                    }
                }
                else
                {
                    model.Custom1 = null;
                }
                //PRFT Custom Code: End

                string errorMessage = string.Empty;
                bool status = _categoryAgent.CreateCategory(model, out errorMessage);
                if (!string.IsNullOrEmpty(errorMessage))
                {
                    model.ChildCategories = true;
                    TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(errorMessage, NotificationType.error);
                    return View(createEditView, model);
                }
                TempData[MvcAdminConstants.Notifications] = status ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success) : !string.IsNullOrEmpty(errorMessage) ? GenerateNotificationMessages(errorMessage, NotificationType.error) : GenerateNotificationMessages(ZnodeResources.SaveErrorMessage, NotificationType.error);
                return RedirectToAction(MvcAdminConstants.ManageView, new { id = model.CategoryId });
            }
            return View(createEditView, model);
        }

        /// <summary>
        /// To show add new category page
        /// </summary>
        /// <returns>add new category page</returns>
        [HttpGet]
        public ActionResult Edit(int? id)
        {
            CategoryViewModel model = null;
            if (!Equals(id, null))
            {
                model = _categoryAgent.GetCategory(id.Value);
                model.IsEditMode = true;

            }
            return View(createEditView, model);
        }

        /// <summary>
        /// To update category details
        /// </summary>
        /// <param name="model">Category View Model</param>
        /// <returns>status true/false</returns>
        [HttpPost]
        public ActionResult Edit(CategoryViewModel model)
        {
            if (ModelState.IsValid)
            {

                //PRFT Custom Code: Start

                if (model.IsManufacturer)
                {
                    model.Custom1 = _categoryAgent.GetCategory(model.CategoryId).Custom1;
                    var manufacturerModel = AddCategoryAsManufacturer(model);
                    if (manufacturerModel != null)
                    {
                        model.Custom1 = Convert.ToString(manufacturerModel.ManufacturerId);
                    }
                }

                //PRFT Custom Code: End

                string errorMessage = string.Empty;
                bool status = _categoryAgent.UpdateCategory(model, out errorMessage);
                if (!string.IsNullOrEmpty(errorMessage))
                {
                    TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(errorMessage, NotificationType.error);
                    return View(createEditView, model);
                }
                TempData[MvcAdminConstants.Notifications] = status ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.SaveErrorMessage, NotificationType.error);
                return RedirectToAction(MvcAdminConstants.ManageView, new { id = model.CategoryId });
            }
            return View(createEditView, model);
        }

        /// <summary>
        /// Deletes an existing Category.
        /// </summary>
        /// <param name="id">nullable int categoryId</param>
        /// <returns>Returns json and Success/Fail Message</returns>
        [HttpPost]
        public JsonResult Delete(int? id)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();
            if (!Equals(null, id))
            {

                //PRFT Custom Code: Start
                var categoryModel = _categoryAgent.GetCategory(id.Value);
                bool manufacturerStatus = false;
                if (!string.IsNullOrEmpty(categoryModel.Custom1))
                {
                    manufacturerStatus = _manufacturersAgent.DeleteManufactuere(int.Parse(categoryModel.Custom1));
                    message = manufacturerStatus ? ZnodeResources.DeleteMessage : ZnodeResources.DeleteBrandErrorMessage;
                    if (!manufacturerStatus)
                    {
                        return Json(new { sucess = manufacturerStatus, message = message, id = id, isFadeOut = isFadeOut });
                    }
                }
                //PRFT Custom Code: End

                status = _categoryAgent.DeleteCategory(id.Value, out message);
                message = status ? ZnodeResources.DeleteMessage : message;
            }
            return Json(new { sucess = status, message = message, id = id, isFadeOut = isFadeOut });
        }

        /// <summary>
        /// To edit product details
        /// </summary>
        /// <param name="id">int categoryId</param>
        /// <returns>Category Details</returns>
        [HttpGet]
        public ActionResult Manage(int? id, string tabMode = "")
        {
            if (!string.IsNullOrEmpty(tabMode))
            {
                TempData[MvcAdminConstants.SetTabMode] = tabMode;
            }

            CategoryViewModel category = new CategoryViewModel();
            if (!Equals(id, null))
            {
                category = _categoryAgent.GetCategory(id.Value);
                return View(MvcAdminConstants.ManageView, category);
            }
            return View(MvcAdminConstants.ManageView, category);
        }

        /// <summary>
        /// Get the list of category associated Products
        /// </summary>
        /// <param name="model">Filter Model</param>
        /// <returns>List of products</returns>
        public ActionResult GetAssociateProduct([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model)
        {
            int categoryId = 0;
            foreach (KeyValuePair<string, string> item in model.Params)
            {
                if (item.Key.ToString().ToLower().Equals(MvcAdminConstants.CategoryId.ToLower()))
                {
                    categoryId = int.Parse(item.Value);
                }
            }
            ProductListViewModel product = _categoryAgent.GetProductsByCategoryId(categoryId, model.Filters, model.SortCollection, model.Page, model.RecordPerPage);
            if (!Equals(product, null))
            {
                product.CategoryId = categoryId;
                foreach (ProductViewModel item in product.Products)
                {
                    item.CategoryId = categoryId;
                }
                ReportModel gridModel = FilterHelpers.GetDynamicGridModel(model, product.Products, Convert.ToInt32(ListType.vw_ZNodeGetDistinctProductsByCategory));
                gridModel.TotalRecordCount = product.TotalResults;
                product.GridModel = gridModel;
                return ActionView(MvcAdminConstants.AssociatedProducts, product);
            }
            return ActionView(MvcAdminConstants.AssociatedProducts, new ProductListViewModel());
        }

        /// <summary>
        /// Get the list of products
        /// </summary>
        /// <param name="Id">int categoryId</param>
        /// <param name="model"></param>
        /// <returns>list of Products</returns>
        public ActionResult SearchAddAssociateProduct(int? id, [ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model)
        {
            CategoryAssociatedProductsListViewModel products = _productAgent.GetAllProducts(model.Filters, model.SortCollection, model.Page, model.RecordPerPage);
            if (!Equals(products, null))
            {
                products.CategoryName = _categoryAgent.GetCategory(id.Value).Name;
                products.CategoryId = id.Value;
                ReportModel gridModel = FilterHelpers.GetDynamicGridModel(model, products.AssociatedProducts, Convert.ToInt32(ListType.CategoryProductList));
                gridModel.TotalRecordCount = products.TotalResults;
                products.GridModel = gridModel;
                return ActionView(MvcAdminConstants.SearchAddAssociateProducts, products);
            }
            return View(MvcAdminConstants.SearchAddAssociateProducts);
        }

        /// <summary>
        /// Add Products associated to category
        /// </summary>
        /// <param name="id">int categoryId</param>
        /// <param name="productIds">string productIds</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult CategoryAssociatedProducts(int id, string productIds)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();
            if (!Equals(id, null) && !Equals(productIds, null))
            {
                string errorMessage = string.Empty;
                status = _categoryAgent.CategoryAssociatedProducts(id, productIds, out errorMessage);
                if (!string.IsNullOrEmpty(errorMessage))
                {
                    message = errorMessage;
                    TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(message, NotificationType.error);
                }
                else
                {
                    message = status ? ZnodeResources.SaveMessage : ZnodeResources.SaveErrorMessage;
                    TempData[MvcAdminConstants.Notifications] = status ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.SaveErrorMessage, NotificationType.error);
                }
            }
            return Json(new { sucess = status, message = message, id = id, isFadeOut = isFadeOut });
        }

        /// <summary>
        /// Edit setting of product category
        /// </summary>
        /// <param name="Id">int productId</param>
        /// <param name="categoryId">int categoryId</param>
        /// <returns>Product category model</returns>
        [HttpGet]
        public ActionResult Setting(int? productId, int categoryId = 0)
        {
            ProductCategoryViewModel model = _categoryAgent.GetProductCategory(productId.Value, categoryId);
            if (Equals(model.DisplayOrder, null))
            {
                model.DisplayOrder = MvcAdminConstants.DefaultDisplayOrder;
            }
            model.ThemeList = _categoryAgent.BindThemeList();
            model.CssList = _categoryAgent.BindCssList(0);

            return View(MvcAdminConstants.AssociateProductSetting, model);
        }

        /// <summary>
        /// update setting of Product Category
        /// </summary>
        /// <param name="model">Product category Model</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Setting(ProductCategoryViewModel model)
        {
            if (!Equals(model, null))
            {
                bool status = _productCategoryAgent.UpdateProductCategory(model);
                TempData[MvcAdminConstants.Notifications] = status ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.SaveErrorMessage, NotificationType.error);
                return RedirectToAction(MvcAdminConstants.ManageView, new { id = model.CategoryId, tabMode = SelectedTab.AssociatedProducts });
            }
            return View(MvcAdminConstants.SearchAddAssociateProducts);
        }

        /// <summary>
        /// Remoe product category
        /// </summary>
        /// <param name="id">int product Id</param>
        /// <returns>Returns json and Success/Fail Message</returns>
        [HttpPost]
        public JsonResult Remove(int? productId, int categoryId = 0)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();
            if (!Equals(null, productId))
            {
                CategoryViewModel categoryModel = new CategoryViewModel();
                categoryModel.ProductId = productId.Value;
                categoryModel.CategoryId = categoryId;

                status = _productAgent.UnAssociateProductCategory(categoryModel);
                message = status ? ZnodeResources.DeleteMessage : ZnodeResources.DeleteErrorMessage;
            }
            return Json(new { sucess = status, message = message, id = productId, isFadeOut = isFadeOut });
        }

        /// <summary>
        /// Bind css list to dropdown.
        /// </summary>
        /// <param name="CssThemeId">The id of theme.</param>
        /// <returns>Returns Json result.</returns>
        [HttpGet]
        public JsonResult BindCssList(int CssThemeId)
        {
            List<SelectListItem> list = _categoryAgent.BindCssList(CssThemeId);
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Binds master page list to dropdown.
        /// </summary>
        /// <param name="CssThemeId">The id of theme.</param>
        /// <returns>Returns Json result.</returns>
        [HttpGet]
        public JsonResult BindMasterPageList(int CssThemeId)
        {
            List<SelectListItem> list = _categoryAgent.BindMasterPageList(CssThemeId, ProductPageType);
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// To Get Category Tree by catalogId & name
        /// </summary>
        /// <param name="catalogId">int catalogId</param>
        /// <param name="portalId">string name</param>
        /// <returns></returns>
        public JsonResult GetCategoryTree(int catalogId, string name)
        {
            CategoryTreeListViewModel categories = _categoryAgent.GetCategoryTree(catalogId, name);
            return Json(categories.Categories, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region PRFT Custom Code

        private ManufacturersViewModel AddCategoryAsManufacturer(CategoryViewModel model)
        {
            FilterCollectionDataModel filterModel = new FilterCollectionDataModel();
            filterModel.Filters = new FilterCollection();
            filterModel.SortCollection = new SortCollection();
            filterModel.Filters.Add(FilterKeys.ManufacturerId, FilterOperators.Equals, model.Custom1);
            ManufacturersViewModel manufacturerViewModel = new ManufacturersViewModel();

            ManufacturersListViewModel list = _manufacturersAgent.GetManufacturers(filterModel.Filters, filterModel.SortCollection, null, null);
            manufacturerViewModel = list.Manufacturers.Find(m => m.ManufacturerId.ToString() == model.Custom1);

            if (manufacturerViewModel == null)
            {
                manufacturerViewModel = new ManufacturersViewModel();
                manufacturerViewModel.Name = model.Name;
                manufacturerViewModel.Description = model.Description;
                manufacturerViewModel.IsActive = true;
                manufacturerViewModel.DisplayOrder = model.DisplayOrder;
                manufacturerViewModel = _manufacturersAgent.CreateManufacturer(manufacturerViewModel);
            }
            else
            {
                manufacturerViewModel.Name = model.Name;
                manufacturerViewModel.Description = model.Description;
                manufacturerViewModel.IsActive = true;
                manufacturerViewModel.DisplayOrder = model.DisplayOrder;
                _manufacturersAgent.UpdateManufacturer(manufacturerViewModel);
            }

            return manufacturerViewModel;
        }

        #endregion
    }
}