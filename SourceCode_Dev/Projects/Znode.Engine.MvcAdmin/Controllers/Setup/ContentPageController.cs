﻿using Resources;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Models.Enum;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Controllers.SetUp
{
    public class ContentPageController : BaseController
    {
        #region Private Variables
        private readonly IContentPageAgent _contentPageAgent;

        private string createEditView = MvcAdminConstants.CreateEditView;
        private string listAction = MvcAdminConstants.ListView;
        private readonly string pageType = MvcAdminConstants.PageType;
        private readonly string revisionList = MvcAdminConstants.RevisionList;
        private readonly string portalId = "PortalId";
        private readonly string storeName = "StoreName";
        #endregion

        #region Default Constructor
        /// <summary>
        /// Default constructor.
        /// </summary>
        public ContentPageController()
        {
            _contentPageAgent = new ContentPageAgent();
        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Gets List Of Content Page.
        /// </summary>
        /// <returns>Returns view of list page</returns>
        public ActionResult List([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model)
        {
            ContentPageListViewModel contentPage = _contentPageAgent.GetContentPages(model.Filters, model.SortCollection, model.Page, model.RecordPerPage);

            var gridModel = FilterHelpers.GetDynamicGridModel(model, contentPage.ContentPages, (int)ListType.ContentPageList);
            FilterHelpers.CreateDropdown(ref gridModel, _contentPageAgent.GetPortalsList(), model.Filters, portalId, storeName, ZnodeResources.LabelStoreName);
            gridModel.TotalRecordCount = contentPage.TotalResults;

            return ActionView(gridModel);
        }

        /// <summary>
        /// Create ContentPage.
        /// </summary>
        /// <returns>Returns view of create page</returns>
        [HttpGet]
        public ActionResult Create()
        {
            ContentPageViewModel model = new ContentPageViewModel();

            model.PortalList = _contentPageAgent.GetPortalsList();
            model.ThemeList = _contentPageAgent.BindThemeList();
            model.CSSList = _contentPageAgent.BindCssList(null);     
            return View(createEditView, model);
        }

        /// <summary>
        /// Create ContentPage.
        /// </summary>
        /// <param name="model">model to create.</param>
        /// <returns>Returns view of action page.</returns>
        [HttpPost]
        public ActionResult Create(ContentPageViewModel model)
        {
            string errorMessage = string.Empty;
            bool status = false;
            if (ModelState.IsValid)
            {
                status = _contentPageAgent.CreateContentPage(model, out errorMessage);

                TempData[MvcAdminConstants.Notifications] = status
                ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success)
                : (string.IsNullOrEmpty(errorMessage))
                    ? (string.IsNullOrEmpty(model.SEOFriendlyPageName))
                        ? GenerateNotificationMessages(ZnodeResources.SaveErrorMessage, NotificationType.error)
                        : GenerateNotificationMessages(ZnodeResources.ErrorSeoUrlAlreadyExist, NotificationType.error)
                    : GenerateNotificationMessages(errorMessage, NotificationType.error);

                if (!status)
                {
                    model.PortalList = _contentPageAgent.GetPortalsList();
                    model.ThemeList = _contentPageAgent.BindThemeList();
                    model.CSSList = _contentPageAgent.BindCssList(null);
                    return View(createEditView, model);
                }
            }
            return RedirectToAction(listAction);

        }

        /// <summary>
        /// Edit Existing Content Page
        /// </summary>
        /// <param name="id">Id of the content page.</param>
        /// <returns>View of edit page.</returns>
        [HttpGet]
        public ActionResult Edit(int? id)
        {
            if (!Equals(id, null) && id > 0)
            {
                ContentPageViewModel model = _contentPageAgent.GetContentPage(id.Value);
                if (!Equals(model, null))
                {
                    model.PortalList = _contentPageAgent.GetPortalsList();
                    model.ThemeList = _contentPageAgent.BindThemeList();
                    model.IsEditMode = true;
                    return View(createEditView, model);
                }
            }

            return RedirectToAction(listAction);
        }

        /// <summary>
        ///  Edit Existing Content Page
        /// </summary>
        /// <param name="model">ContentPageViewModel model</param>
        /// <returns>View of List page.</returns>
        [HttpPost]
        public ActionResult Edit(ContentPageViewModel model)
        {
            string errorMessage = string.Empty;
            bool status = false;
            ModelState.Remove("OldHtml");

            if (ModelState.IsValid)
            {
                status = _contentPageAgent.UpdateContentPage(model, out errorMessage);

                TempData[MvcAdminConstants.Notifications] = status
                ? GenerateNotificationMessages(ZnodeResources.UpdateMessage, NotificationType.success)
                : (string.IsNullOrEmpty(errorMessage))
                    ? (string.IsNullOrEmpty(model.SEOFriendlyPageName))
                        ? GenerateNotificationMessages(ZnodeResources.UpdateErrorMessage, NotificationType.error)
                        : GenerateNotificationMessages(ZnodeResources.ErrorSeoUrlAlreadyExist, NotificationType.error)
                    : GenerateNotificationMessages(errorMessage, NotificationType.error);

                if (!status)
                {
                    model.PortalList = _contentPageAgent.GetPortalsList();
                    model.ThemeList = _contentPageAgent.BindThemeList();
                    model.CSSList = _contentPageAgent.BindCssList(null);
                    model.IsEditMode = true;
                    return View(createEditView, model);
                }
            }
            return RedirectToAction(listAction);
        }

        /// <summary>
        /// Delete existing content page
        /// </summary>
        /// <param name="id">id of the content page</param>
        /// <returns>JsonResult</returns>
        [HttpPost]
        public JsonResult Delete(int? id)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();

            if (!Equals(id > 0))
            {
                status = _contentPageAgent.DeleteContentPage(id.Value, out message);
                message = status ? ZnodeResources.DeleteMessage : message;
            }

            return Json(new { sucess = status, message = message, id = id, isFadeOut = isFadeOut });
        }

        /// <summary>
        /// Revert the content page revision.
        /// </summary>
        /// <param name="revisionId">Id of the revision.</param>
        /// <param name="contentPageId">Id of the content page.</param>
        /// <returns>View of Revision List.</returns>
        public ActionResult RevertRevision(int? revisionId, int contentPageId)
        {
            string errorMessage = string.Empty;
            if (ModelState.IsValid)
            {
                bool status = _contentPageAgent.RevertRevision(revisionId.Value, contentPageId, out errorMessage);
                TempData[MvcAdminConstants.Notifications] = status ? GenerateNotificationMessages(ZnodeResources.RevertPageRevisionMessage, NotificationType.success) : GenerateNotificationMessages(errorMessage, NotificationType.error);
            }
            return RedirectToAction(revisionList, new { id = contentPageId });
        }

        /// <summary>
        /// List of revisions.
        /// </summary>
        /// <param name="ContentPageId">Id of the content page.</param>
        /// <returns>View of Revision List.</returns>
        public ActionResult RevisionList(int? id, [ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model)
        {
            ContentPageRevisionListViewModel revisionList = _contentPageAgent.GetContentPageRevisionById(id.Value, model.Filters, model.SortCollection, model.Page, model.RecordPerPage);

            revisionList.GridModel = FilterHelpers.GetDynamicGridModel(model, revisionList.ContentPageRevisions, Convert.ToInt32(ListType.ContentPageRevisionList));
            revisionList.GridModel.TotalRecordCount = revisionList.TotalResults;
            return ActionView(revisionList);
        }

        /// <summary>
        /// Bind the Css List on theme id.
        /// </summary>
        /// <param name="CssThemeId">Id of the themeId</param>
        /// <returns>JsonResult</returns>
        [HttpGet]
        public JsonResult BindCssList(int CssThemeId)
        {
            FilterCollection filters = new FilterCollection();
            filters.Add(new FilterTuple(FilterKeys.CSSThemeId, FilterOperators.Equals, CssThemeId.ToString()));
            List<SelectListItem> list = _contentPageAgent.BindCssList(filters);
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Bind the master page List on theme id.
        /// </summary>
        /// <param name="CssThemeId">Id of the themeId</param>
        /// <returns>JsonResult</returns>
        [HttpGet]
        public JsonResult BindMasterPageList(int CssThemeId)
        {
            List<SelectListItem> list = _contentPageAgent.BindMasterPageList(CssThemeId, pageType);
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}