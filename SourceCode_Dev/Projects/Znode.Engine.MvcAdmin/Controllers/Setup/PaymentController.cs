﻿using Resources;
using System.Web.Mvc;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Controllers.SetUp
{
    public class PaymentController : BaseController
    {
        #region Private Variables
        private readonly IPaymentAgent _paymentAgent;
        private const string PaymentOptionModelSession = "PaymentOptionModel";
        private const string PaymentOptionView = "~/Views/Payment/PaymentOption.cshtml";
        private const string ListAction = "List";
        private const string IsExistsOption = "This Payment Option already exists for this Profile. Please select a different Payment Option.";
        #endregion

        #region Constructor
        /// <summary>
        /// Default Constructor
        /// </summary>
        public PaymentController()
        {
            _paymentAgent = new PaymentAgent();
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// DIsplay payment options List
        /// </summary>
        /// <param name="model">FilterCollectionDataModel for sorting and filtering</param>
        /// <returns>Returns Partial view as ActionResult</returns>
        public ActionResult List([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model)
        {
            PaymentOptionListViewModel Paymentmodel = _paymentAgent.GetPaymentOptions(model);
            var gridModel = FilterHelpers.GetDynamicGridModel(model, Paymentmodel.PaymentOptionList, 32);
            gridModel.TotalRecordCount = Paymentmodel.TotalResults;
            Session[PaymentOptionModelSession] = null;
            return ActionView(gridModel);
        }

        /// <summary>
        /// Get view for add new payment option 
        /// </summary>
        /// <returns>Returns ActionResult</returns>
        [HttpGet]
        public ActionResult AddPaymentOption()
        {
            PaymentOptionViewModel model = _paymentAgent.GetPaymentOptionViewData();
            model.IsActive = true;
            Session[PaymentOptionModelSession] = null;
            return View(PaymentOptionView, model);
        }

        /// <summary>
        /// Add new payment option 
        /// </summary>
        /// <param name="model">PaymentOptionViewModel</param>
        /// <returns>Redirect to List Action</returns>
        [HttpPost]
        public ActionResult AddPaymentOption(PaymentOptionViewModel model)
        {
            string errorStatus = string.Empty;
            var status = _paymentAgent.AddPaymentOption(model, out errorStatus);

            var listmodel = _paymentAgent.GetPaymentOptionViewData();
            model.Profiles = listmodel.Profiles;
            model.PaymentTypes = listmodel.PaymentTypes;
            model.PaymentGateways = listmodel.PaymentGateways;
            Session[PaymentOptionModelSession] = model;

            if (!string.IsNullOrEmpty(errorStatus))
            {
                TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(errorStatus, Models.NotificationType.error);
                return View(PaymentOptionView, model);
            }
            else if (!Equals(status.IsExists, null) && (bool)status.IsExists)
            {
                TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(IsExistsOption, Models.NotificationType.error);
                return View(PaymentOptionView, model);
            }
            TempData[MvcAdminConstants.Notifications] = status.PaymentOptionId > 0 ? GenerateNotificationMessages(ZnodeResources.SaveMessage, Models.NotificationType.success) : GenerateNotificationMessages(ZnodeResources.SaveErrorMessage, Models.NotificationType.error);
            return RedirectToAction(ListAction);
        }

        /// <summary>
        /// Display Payment Type form by PaymentTypeId
        /// </summary>
        /// <param name="PaymentTypeId">Payment type Id</param>
        /// <returns>Returns partial view</returns>
        public ActionResult GetPaymentTypeForm(int PaymentTypeId)
        {
            PaymentOptionViewModel model = new PaymentOptionViewModel();
            if (!Equals(Session[PaymentOptionModelSession], null))
                model = (PaymentOptionViewModel)Session[PaymentOptionModelSession];
            else
                model = _paymentAgent.GetPaymentOptionViewData();
            switch (PaymentTypeId)
            {
                case MvcAdminConstants.CreditCard:
                    return PartialView(MvcAdminConstants.CreditCardView, model);
                case MvcAdminConstants.PurchaseOrder:
                    return PartialView(MvcAdminConstants.PurchaseOrderView, model);
                case MvcAdminConstants.PayPalExpress:
                    return PartialView(MvcAdminConstants.PayPalExpressView, model);
                case MvcAdminConstants.Custom:
                    return PartialView(MvcAdminConstants.CustomView, model);
                case MvcAdminConstants.COD:
                    return PartialView(MvcAdminConstants.CODView, model);
            }
            return PartialView(MvcAdminConstants.CODView, model);
        }

        /// <summary>
        /// Display payment getway form 
        /// </summary>
        /// <param name="PaymentGatewayId">payment getway Id</param>
        /// <returns>Returns partial view</returns>
        public ActionResult GetPaymentGetwayForm(int PaymentGatewayId)
        {
            PaymentOptionViewModel model = new PaymentOptionViewModel();
            if (!Equals(Session[PaymentOptionModelSession], null))
                model = (PaymentOptionViewModel)Session[PaymentOptionModelSession];
            else
                model = _paymentAgent.GetPaymentOptionViewData();
            switch (PaymentGatewayId)
            {
                case MvcAdminConstants.AuthorizeNet:
                    return PartialView(MvcAdminConstants.AuthorizeNetView, model);
                case MvcAdminConstants.VerisignPayFlowPro:
                    return PartialView(MvcAdminConstants.VerisignPayFlowProView, model);
                case MvcAdminConstants.PaymentechOrbital:
                    return PartialView(MvcAdminConstants.PaymentechOrbitalView, model);
                case MvcAdminConstants.PayPalDirectPayment:
                    return PartialView(MvcAdminConstants.PayPalDirectPaymentView, model);
                case MvcAdminConstants.WorldPay:
                    return PartialView(MvcAdminConstants.WorldPayView, model);
                case MvcAdminConstants.CyberSource:
                    return PartialView(MvcAdminConstants.CyberSourceView, model);
                case MvcAdminConstants.Checkout2:
                    return PartialView(MvcAdminConstants.Checkout2View, model);
                case MvcAdminConstants.Stripe:
                    return PartialView(MvcAdminConstants.StripeView, model);

            }
            return PartialView();
        }

        /// <summary>
        /// Get payment option data for edit
        /// </summary>
        /// <param name="id">Payment option id</param>
        /// <returns>Retirns ActionResult</returns>        
        [HttpGet]
        public ActionResult Edit(int PaymentOptionId, int PaymentTypeId)
        {
            var model = _paymentAgent.GetPaymentOption(PaymentOptionId);
            Session[PaymentOptionModelSession] = model;
            return View(PaymentOptionView, model);
        }


        /// <summary>
        /// Update payment option
        /// </summary>
        /// <param name="model">PaymentOptionViewModel</param>
        /// <returns>Redirect to list action</returns>
        [HttpPost]
        public ActionResult Edit(PaymentOptionViewModel model)
        {
            string errorStatus = string.Empty;
            bool status = _paymentAgent.UpdatePaymentOption(model, out errorStatus);
            if (!string.IsNullOrEmpty(errorStatus))
            {
                TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(errorStatus, Models.NotificationType.error);
                return RedirectToAction(ListAction);
            }
            TempData[MvcAdminConstants.Notifications] = status.Equals(true) ? GenerateNotificationMessages(ZnodeResources.UpdateMessage, Models.NotificationType.success) : GenerateNotificationMessages(ZnodeResources.UpdateErrorMessage, Models.NotificationType.error);
            return RedirectToAction(ListAction);
        }

        /// <summary>
        /// Delete payment option 
        /// </summary>
        /// <param name="id">Payment option Id</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Delete(int PaymentOptionId, int PaymentTypeId)
        {
            bool status = false;
            string message = string.Empty;
            string errorStatus = string.Empty;
            bool isFadeOut = CheckIsFadeOut();
            if (!Equals(null, PaymentOptionId))
            {
                status = _paymentAgent.DeletePaymentOption(PaymentOptionId, out errorStatus);
                message = status ? ZnodeResources.DeleteMessage : ZnodeResources.DeleteErrorMessage;
            }
            return Json(new { sucess = status, message = message, id = PaymentOptionId, isFadeOut = isFadeOut });
        }

        #endregion
    }
}