﻿using Resources;
using System;
using System.Web.Mvc;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Models.Enum;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Controllers.SetUp
{
    /// <summary>
    /// Shipping Controller
    /// </summary>
    public class ShippingController : BaseController
    {
        #region Private Variables

        #region Private Agent Variables

        private readonly IShippingAgent _shippingAgent;

        #endregion

        #endregion

        #region Public Constructors

        /// <summary>
        /// Default Constructor.
        /// </summary>
        public ShippingController()
        {
            _shippingAgent = new ShippingAgent();
        }

        #endregion

        #region Action Methods

        /// <summary>
        /// This function will return the list of all the Shipping Option.
        /// </summary>
        /// <returns>ActionResult view of Shipping Option list</returns>
        public ActionResult List([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model)
        {
            //Get Shipping options
            ShippingOptionListViewModel list = _shippingAgent.GetShippingOptions(model.Filters, model.SortCollection, model.Page, model.RecordPerPage);

            var gridModel = FilterHelpers.GetDynamicGridModel(model, list.ShippingOptions, Convert.ToInt32(ListType.ShippingOptionList));
            gridModel.TotalRecordCount = list.TotalResults;
            return ActionView(gridModel);
        }

        /// <summary>
        /// View Shipping Options
        /// </summary>
        /// <param name="id">shippingOptionId</param>
        /// <returns>Returns shipping option details</returns>
        public ActionResult View(int id, [ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model)
        {
            ShippingOptionViewModel shippingRuleList = new ShippingOptionViewModel();
            if (!id.Equals(null))
            {
                shippingRuleList = _shippingAgent.GetShippingOption(id, model.Filters, model.SortCollection, model.Page, model.RecordPerPage);
                shippingRuleList.ShippingRuleList.shippingOptionId = shippingRuleList.ShippingOptionId;
            }
            var gridModel = FilterHelpers.GetDynamicGridModel(model, shippingRuleList.ShippingRuleList.ShippingRule, Convert.ToInt32(ListType.ShippingRuleList));
            gridModel.TotalRecordCount = shippingRuleList.ShippingRuleList.ShippingRule.Count;
            shippingRuleList.ReportModel = gridModel;
            return ActionView(MvcAdminConstants.ShippingView, shippingRuleList);
        }

        /// <summary>
        /// Create Shipping Option
        /// </summary>
        /// <returns>Returns create edit view for add shipping option</returns>
        [HttpGet]
        public ActionResult CreateShippingOption()
        {
            ShippingOptionViewModel model = new ShippingOptionViewModel();
            model.IsActive = true;
            model.ShippingTypeList = _shippingAgent.GetShippingTypes().ShippingTypeList;

            if (Equals(model, null) || Equals(model.ShippingTypeList.Count, 0))
            {
                TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.ErrorMessageAddShippingOptionWhenDisabled, NotificationType.error);
                return RedirectToAction(MvcAdminConstants.ListView);
            }

            FilterCollection filters = new FilterCollection();
            filters.Add(new FilterTuple(FilterKeys.ServiceShippingTypeId, FilterOperators.Equals, model.ShippingTypeId.ToString()));
            model.ProfileList = _shippingAgent.GetProfileList().ProfileList;
            model.CountryList = _shippingAgent.GetCountryList().CountryList;
            model.ShippingServiceCodeList = _shippingAgent.BindServiceList(null);
            return View(MvcAdminConstants.CreateEditShippingOptionView, model);
        }

        /// <summary>
        /// Create Shipping Option
        /// </summary>
        /// <param name="model">ShippingOptionViewModel</param>
        /// <returns>Returns create edit view for add shipping option</returns>
        [HttpPost]
        public ActionResult CreateShippingOption(ShippingOptionViewModel model)
        {
            int ShippingOptionId = 0;

            if ((model.ShippingType.ClassName.Equals((ShippingType.ZnodeShippingFedEx.ToString()))) || model.ShippingType.ClassName.Equals(ShippingType.ZnodeShippingUps.ToString()))
            {
                model.ShippingServiceCode = _shippingAgent.GetShippingServiceCode(model.ShippingServiceCodeId);
            }

            if (ModelState.IsValid)
            {
                string message = string.Empty;
                ShippingOptionId = _shippingAgent.SaveShippingOption(model, out message);
                TempData[MvcAdminConstants.Notifications] = ShippingOptionId > 0 ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success) : GenerateNotificationMessages(message, NotificationType.error);
                if (!Equals(ShippingOptionId, 0))
                {
                    if (model.ShippingType.ClassName.Equals(ShippingType.ZnodeShippingCustom.ToString()))
                    {
                        return RedirectToAction(MvcAdminConstants.CreateView, new { @id = ShippingOptionId });
                    }

                    else
                    {
                        return RedirectToAction(MvcAdminConstants.ShippingView, new { @id = ShippingOptionId });
                    }
                }
            }
            else
            {
                model.ShippingTypeList = _shippingAgent.GetShippingTypes().ShippingTypeList;
                model.ProfileList = _shippingAgent.GetProfileList().ProfileList;
                model.CountryList = _shippingAgent.GetCountryList().CountryList;
                return View(MvcAdminConstants.CreateEditShippingOptionView, model);
            }

            model.ShippingTypeList = _shippingAgent.GetShippingTypes().ShippingTypeList;
            FilterCollection filters = new FilterCollection();
            filters.Add(new FilterTuple(FilterKeys.ServiceShippingTypeId, FilterOperators.Equals, model.ShippingTypeId.ToString()));
            model.ProfileList = _shippingAgent.GetProfileList().ProfileList;
            model.CountryList = _shippingAgent.GetCountryList().CountryList;
            model.ShippingServiceCodeList = _shippingAgent.BindServiceList(filters);
            return View(MvcAdminConstants.CreateEditShippingOptionView, model);
        }

        /// <summary>
        /// Edit Shipping Option
        /// </summary>
        /// <param name="id">shippingOptionId</param>
        /// <param name="shippingServiceCodeId">shippingServiceCodeId</param>
        /// <returns>Returns create edit view for edit shipping option</returns>
        [HttpGet]
        public ActionResult EditShippingOption(int id)
        {
            ShippingOptionViewModel model = new ShippingOptionViewModel();
            if (!Equals(id, null) && id > 0)
            {
                model = _shippingAgent.GetShippingOption(int.Parse(id.ToString()));

                model.ShippingTypeList = _shippingAgent.GetShippingTypes(true).ShippingTypeList;
                FilterCollection filters = new FilterCollection();
                filters.Add(new FilterTuple(FilterKeys.ServiceShippingTypeId, FilterOperators.Equals, model.ShippingTypeId.ToString()));
                model.ProfileList = _shippingAgent.GetProfileList().ProfileList;
                model.CountryList = _shippingAgent.GetCountryList().CountryList;
                if ((model.ShippingType.ClassName.Equals(ShippingType.ZnodeShippingFedEx.ToString())) || model.ShippingType.ClassName.Equals(ShippingType.ZnodeShippingUps.ToString()))
                {
                    model.ShippingServiceCodeList = _shippingAgent.BindServiceList(filters);
                }
                else
                {
                    model.ShippingServiceCodeList = _shippingAgent.BindServiceList(null);
                }

                model.ShippingTypeId = model.ShippingTypeId;
            }

            return View(MvcAdminConstants.CreateEditShippingOptionView, model);
        }

        /// <summary>
        /// Edit Shipping Option
        /// </summary>
        /// <param name="model">ShippingOptionViewModel</param>
        /// <returns>Returns create edit view for edit shipping option</returns>
        [HttpPost]
        public ActionResult EditShippingOption(ShippingOptionViewModel model)
        {
            if ((model.ClassName.Equals(ShippingType.ZnodeShippingFedEx.ToString())) || model.ClassName.Equals(ShippingType.ZnodeShippingUps.ToString()))
            {
                model.ShippingServiceCode = _shippingAgent.GetShippingServiceCode(int.Parse(model.ShippingServiceCodeId.ToString()));
            }
            FilterCollection filters = new FilterCollection();
            filters.Add(new FilterTuple(FilterKeys.ServiceShippingTypeId, FilterOperators.Equals, model.ShippingTypeId.ToString()));
            if (ModelState.IsValid)
            {
                string message = string.Empty;
                bool status = _shippingAgent.UpdateShippingOption(model.ShippingOptionId, model, out message);
                TempData[MvcAdminConstants.Notifications] = status ? GenerateNotificationMessages(ZnodeResources.UpdateMessage, NotificationType.success) : GenerateNotificationMessages(message, NotificationType.error);

                if (status)
                {
                    return RedirectToAction(MvcAdminConstants.ShippingView, new { @id = model.ShippingOptionId });
                }
                else
                {
                    return RedirectToAction(MvcAdminConstants.EditShippingOption, new { id = model.ShippingOptionId });
                }
            }

            model.ShippingTypeList = _shippingAgent.GetShippingTypes(true).ShippingTypeList;
            model.ProfileList = _shippingAgent.GetProfileList().ProfileList;
            model.CountryList = _shippingAgent.GetCountryList().CountryList;
            model.ShippingServiceCodeList = _shippingAgent.BindServiceList(null);
            return View(MvcAdminConstants.CreateEditShippingOptionView, model);
        }

        /// <summary>
        /// Bind Shipping service code list
        /// </summary>
        /// <param name="serviceShippingTypeId">serviceShippingTypeId</param>
        /// <returns>JsonResult</returns>
        [HttpGet]
        public JsonResult BindServiceList(string serviceShippingTypeId)
        {
            ShippingOptionViewModel model = new ShippingOptionViewModel();
            FilterCollection filters = new FilterCollection();
            filters.Add(new FilterTuple(FilterKeys.ServiceShippingTypeId, FilterOperators.Equals, Convert.ToString(_shippingAgent.GetShippingTypeId(serviceShippingTypeId))));
            if ((serviceShippingTypeId.Equals((ShippingType.ZnodeShippingFedEx.ToString()))) || serviceShippingTypeId.Equals((ShippingType.ZnodeShippingUps.ToString())))
            {
                var list = _shippingAgent.BindServiceList(filters);
                return Json(list, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var list = _shippingAgent.BindServiceList(null);
                return Json(list, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Add Shipping Rule
        /// </summary>
        /// <param name="id">id</param>
        /// <returns>Returns Create View</returns>
        [HttpGet]
        public ActionResult Create(int id)
        {
            ShippingRuleViewModel model = new ShippingRuleViewModel();
            model.shippingId = id;
            model.ShippingOption = _shippingAgent.GetShippingOption(id);
            model.LowerLimit = Equals(model.LowerLimit, null) ? HelperMethods.FormatPrice(Convert.ToDecimal(0.00)) : model.LowerLimit;
            model.ShippingRuleTypeList = _shippingAgent.GetShippingRuleTypes().ShippingRuleTypeList;
            Session[MvcAdminConstants.ShippingRuleViewModel] = null;
            return View(MvcAdminConstants.CreateEditShippingRuleView, model);
        }

        /// <summary>
        /// Save added Shipping Rule to database
        /// </summary>
        /// <param name="model">ShippingRuleViewModel</param>
        /// <returns>Returns View containing shipping rule list</returns>
        [HttpPost]
        public ActionResult Create(ShippingRuleViewModel model)
        {
            string message = string.Empty;
            bool status = _shippingAgent.SaveShippingRule(model, out message);
            TempData[MvcAdminConstants.Notifications] = status ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success) : GenerateNotificationMessages(message, NotificationType.error);
            if (status)
            {
                return RedirectToAction(MvcAdminConstants.ShippingView, new { @id = model.shippingId });
            }
            model.ShippingRuleTypeList = _shippingAgent.GetShippingRuleTypes().ShippingRuleTypeList;
            return ActionView(MvcAdminConstants.CreateEditShippingRuleView, model);
        }

        /// <summary>
        /// Edit Shipping Rules
        /// </summary>
        /// <param name="id">ShippingRuleId</param>
        /// <param name="ShippingId">ShippingId</param>
        /// <returns>Returns Edit View</returns>
        [HttpGet]
        public ActionResult Edit(int id)
        {
            ShippingRuleViewModel model = new ShippingRuleViewModel();

            model.ShippingRuleId = id;
            if (!Equals(id, null) && id > 0)
            {
                model = _shippingAgent.GetShippingRule(int.Parse(id.ToString()));
                model.ShippingRuleTypeList = _shippingAgent.GetShippingRuleTypes().ShippingRuleTypeList;
                model.shippingId = model.shippingId;
                model.ShippingOption = _shippingAgent.GetShippingOption(model.shippingId);
            }
            Session[MvcAdminConstants.ShippingRuleViewModel] = model;
            return View(MvcAdminConstants.CreateEditShippingRuleView, model);
        }

        /// <summary>
        /// Save edited shipping rule to database
        /// </summary>
        /// <param name="model">ShippingRuleViewModel</param>
        /// <returns>Returns View containing shipping rule list</returns>
        [HttpPost]
        public ActionResult Edit(ShippingRuleViewModel model)
        {
            string message = string.Empty;
            bool status = _shippingAgent.UpdateShippingRule(model.ShippingRuleId, model, out message);
            TempData[MvcAdminConstants.Notifications] = status ? GenerateNotificationMessages(ZnodeResources.UpdateMessage, NotificationType.success) : GenerateNotificationMessages(message, NotificationType.error);
            if (status)
            {
                return RedirectToAction(MvcAdminConstants.ShippingView, new { @id = model.shippingId });
            }
            model.ShippingRuleTypeList = _shippingAgent.GetShippingRuleTypes().ShippingRuleTypeList;
            Session[MvcAdminConstants.ShippingRuleViewModel] = null;
            return ActionView(MvcAdminConstants.CreateEditShippingRuleView, model);
        }

        /// <summary>
        /// Delete Shipping Rule
        /// </summary>
        /// <param name="id">id</param>
        /// <returns>Returns Json Result</returns>
        [HttpPost]
        public JsonResult DeleteRule(int id)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();
            if (!Equals(null, id))
            {
                status = _shippingAgent.DeleteShippingRule(id);
                message = status ? ZnodeResources.DeleteMessage : ZnodeResources.DeleteErrorMessage;
            }
            return Json(new { sucess = status, message = message, id = id, isFadeOut = isFadeOut });
        }

        /// <summary>
        /// Delete Shipping Option
        /// </summary>
        /// <param name="id">id</param>
        /// <returns>JsonResult</returns>
        [HttpPost]
        public JsonResult DeleteShippingOption(int id)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();

            if (!Equals(null, id))
            {
                status = _shippingAgent.DeleteShippingOption(id, out message);
                message = status ? ZnodeResources.DeleteMessage : message;
            }

            return Json(new { sucess = status, message = message, id = id, isFadeOut = isFadeOut });
        }

        public ActionResult GetShippingTypeView(string shippingRuleTypeId)
        {
            ShippingRuleViewModel model = new ShippingRuleViewModel();

            if (!Equals(Session[MvcAdminConstants.ShippingRuleViewModel], null))
                model = (ShippingRuleViewModel)Session[MvcAdminConstants.ShippingRuleViewModel];
            switch (Convert.ToInt32(shippingRuleTypeId))
            {
                case 0:
                    return PartialView(MvcAdminConstants.FlatRatePerItemView, model);
                case 1:
                    return PartialView(MvcAdminConstants.QuantityBasedRateView, model);
                case 2:
                    return PartialView(MvcAdminConstants.WeightBasedRateView, model);
                case 3:
                    return PartialView(MvcAdminConstants.FixedRatePerItemView, model);

            }
            return new EmptyResult();
        }

        #endregion

        #region Private Enum
        /// <summary>
        /// Enum for Shipping Type
        /// </summary>
        private enum ShippingType
        {
            ZnodeShippingCustom = 1,
            ZnodeShippingFedEx = 2,
            ZnodeShippingUps = 3,
            ZnodeShippingUsps = 4
        }
        #endregion
    }
}