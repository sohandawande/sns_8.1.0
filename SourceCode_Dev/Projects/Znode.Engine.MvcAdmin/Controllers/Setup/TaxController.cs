﻿using Resources;
using System;
using System.Web.Mvc;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Models.Enum;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Controllers
{
    /// <summary>
    /// Tax Agent
    /// </summary>
    public class TaxController : BaseController
    {
        #region Private Variables

        #region Private Agent Variables
        private readonly ITaxClassAgent _taxClassAgent;
        private readonly IPortalAgent _portalAgent;
        private readonly ITaxRuleAgent _taxRuleAgent; 
        #endregion

        #region Private Constant Variables
        private const string createEditViewTaxClass = "CreateEditView";
        private const string createEditViewTaxRule = "CreateEditViewTaxRule";
        private const string viewTaxClassDetail = "ViewTaxClassDetails";
        private const string createTaxRuleAction = "CreateTaxRule";
        private const string salesTax = "SalesTax";
        #endregion

        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public TaxController()
        {
            _taxClassAgent = new TaxClassAgent();
            _taxRuleAgent = new TaxRuleAgent();
            _portalAgent = new PortalAgent();
        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Gets the List of all available tax class.
        /// </summary>
        /// <param name="model">Filter collection data model</param>
        /// <returns>Returns View with list of tax class.</returns>
        public ActionResult List([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model)
        {
            TaxClassListViewModel taxClassList = _taxClassAgent.GetAllTaxClasses(model.Filters, model.SortCollection, model.Page, model.RecordPerPage);

            var gridModel = FilterHelpers.GetDynamicGridModel(model, taxClassList.TaxClasses, Convert.ToInt32(ListType.vw_Znode_TaxClass));
            gridModel.TotalRecordCount = taxClassList.TotalResults;
            return ActionView(gridModel);
        }

        /// <summary>
        /// View Tax Details
        /// </summary>
        /// <param name="id">Id</param>
        /// <param name="model">model</param>
        /// <returns>Return taxClassViewModel</returns>
        public ActionResult ViewTaxClassDetails(int id, [ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model)
        {
            TaxClassViewModel taxClassViewModel = new TaxClassViewModel();

            if (!id.Equals(null))
            {
                taxClassViewModel = _taxClassAgent.GetTaxClassDetails(id, model.Filters, model.SortCollection, model.Page, model.RecordPerPage);
            }

            var gridModel = FilterHelpers.GetDynamicGridModel(model, taxClassViewModel.taxRuleList.TaxRules, Convert.ToInt32(ListType.TaxRuleList));
            gridModel.TotalRecordCount = taxClassViewModel.taxRuleList.TotalResults;
            taxClassViewModel.ReportModel = gridModel;
            return ActionView(taxClassViewModel);
        }

        /// <summary>
        /// Create Tax Class
        /// </summary>
        /// <returns>Returns create edit view for add tax class</returns>
        [HttpGet]
        public ActionResult Create()
        {
            TaxClassViewModel model = new TaxClassViewModel();
            model.IsActive = true;
            return View(createEditViewTaxClass, model);
        }

        /// <summary>
        /// Create Tax Class
        /// </summary>
        /// <param name="model">TaxClassViewModel</param>
        /// <returns>Returns create edit view for add tax class </returns>
        [HttpPost]
        public ActionResult Create(TaxClassViewModel model)
        {
            if (ModelState.IsValid)
            {
                string message = string.Empty;
                int taxId = 0;
                bool status = _taxClassAgent.SaveTaxClass(model, out taxId, out message);
                TempData[MvcAdminConstants.Notifications] = status ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success) : GenerateNotificationMessages(message, NotificationType.error);

                if (status)
                {
                    return RedirectToAction(createTaxRuleAction, new { @id = taxId });
                }
                return View(createEditViewTaxClass, model);
            }
            return View(createEditViewTaxClass, model);
        }

        /// <summary>
        /// Edit Tax Class
        /// </summary>
        /// <param name="addOnId">int taxClassId</param>
        /// <returns>Returns create edit view for add tax class</returns>
        public ActionResult Edit(int id)
        {
            if (id > 0)
            {
                TaxClassViewModel taxClass = new TaxClassViewModel();
                taxClass = _taxClassAgent.GetTaxClassDetails(id);

                return View(createEditViewTaxClass, taxClass);
            }
            return RedirectToAction(MvcAdminConstants.ListView);
        }

        /// <summary>
        /// Edit Tax Class
        /// </summary>
        /// <param name="model">TaxClassViewModel model</param>
        /// <returns>Returns create edit view for add tax class</returns>
        [HttpPost]
        public ActionResult Edit(TaxClassViewModel model)
        {
            if (ModelState.IsValid)
            {
                string message = string.Empty;
                bool status = _taxClassAgent.UpdateTaxClass(model, model.TaxClassId, out message);
                TempData[MvcAdminConstants.Notifications] = status ? GenerateNotificationMessages(ZnodeResources.UpdateMessage, NotificationType.success) : GenerateNotificationMessages(message, NotificationType.error);
                if (status)
                {
                    return RedirectToAction(viewTaxClassDetail, new { @id = model.TaxClassId });
                }
                return View(createEditViewTaxClass, model);
            }

            return View(createEditViewTaxClass, model);
        }

        /// <summary>
        /// Delete Tax Class
        /// </summary>
        /// <param name="id">id</param>
        /// <returns>JsonResult</returns>
        [HttpPost]
        public JsonResult DeleteTaxClass(int id)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();

            if (!Equals(null, id))
            {
                status = _taxClassAgent.DeleteTaxClass(id, out message);
                message = status ? ZnodeResources.DeleteMessage : message;
            }

            return Json(new { sucess = status, message = message, id = id, isFadeOut = isFadeOut });
        }

        /// <summary>
        /// Create Tax Rule
        /// </summary>
        /// <param name="id">int id</param>
        /// <returns>Returns create edit view for tax rule</returns>
        public ActionResult CreateTaxRule(int id)
        {
            TaxRulesViewModel model = new TaxRulesViewModel();
            model.TaxClassId = id;
            model.InclusiveTax = true;
            _taxRuleAgent.BindDropdownValues(model);

            if (Equals(model, null) || Equals(model.TaxRuleTypeList.Count, 0))
            {
                TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.ErrorMessageAddTaxRuleWhenDisabled, NotificationType.error);
                return RedirectToAction(viewTaxClassDetail, new { id = model.TaxClassId });
            }

            model.StateList = _taxRuleAgent.BindStateList(null);
            model.CountyList = _taxRuleAgent.BindCountyList(null);
            return View(createEditViewTaxRule, model);
        }

        /// <summary>
        /// Create Tax Rule 
        /// </summary>
        /// <param name="model">TaxRulesViewModel model</param>
        /// <returns>Returns create edit view for tax rule</returns>
        [HttpPost]
        public ActionResult CreateTaxRule(TaxRulesViewModel model)
        {
            ModelState.Remove(salesTax);
            if (ModelState.IsValid)
            {
                model.PortalId = 1;
                //PRFT Custom Code : Start
                model.Custom1 = string.Format("AccountID={0}|LicenseKey={1}|Company={2}|url={3}|Client={4}|Name={5}", model.AccountID, model.License, model.Company, model.WebserviceURL, model.ClientCode, model.ClientName);
                //PRFT Custom Code : End
                TempData[MvcAdminConstants.Notifications] = _taxRuleAgent.SaveTaxRules(model) ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.SaveErrorMessage, NotificationType.error);
                return RedirectToAction(viewTaxClassDetail, new { id = model.TaxClassId });
            }
            _taxRuleAgent.BindDropdownValues(model);

            return View(createEditViewTaxRule, model);
        }

        /// <summary>
        /// Edit Tax Rule
        /// </summary>
        /// <param name="addOnValueId">int taxRuleId</param>
        /// <returns>Returns create edit view for tax rule</returns>
        public ActionResult EditTaxRule(int id)
        {
            if (id > 0)
            {
                TaxRulesViewModel taxRule = new TaxRulesViewModel();
                taxRule = _taxRuleAgent.GetTaxRules(id);                
                _taxRuleAgent.BindDropdownValues(taxRule,true);
                taxRule.StateList = _taxRuleAgent.BindStateList(null, taxRule.StateCode);
                taxRule.CountyList = _taxRuleAgent.BindCountyList(null, taxRule.CountyFips);                
                return View(createEditViewTaxRule, taxRule);
            }
            return RedirectToAction(MvcAdminConstants.ListView);
        }

        /// <summary>
        /// Edit Tax Rule
        /// </summary>
        /// <param name="model">TaxRulesViewModel model</param>
        /// <returns>Returns </returns>
        [HttpPost]
        public ActionResult EditTaxRule(TaxRulesViewModel model)
        {
            ModelState.Remove(salesTax);
            if (ModelState.IsValid)
            {
                model.PortalId = 1;
                //PRFT Custom Code : Start
                model.Custom1 = string.Format("AccountID={0}|LicenseKey={1}|Company={2}|url={3}|Client={4}|Name={5}", model.AccountID, model.License, model.Company, model.WebserviceURL, model.ClientCode, model.ClientName);
                //PRFT Custom Code : End
                TempData[MvcAdminConstants.Notifications] = _taxRuleAgent.UpdateTaxRules(model) ? GenerateNotificationMessages(ZnodeResources.UpdateMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.UpdateErrorMessage, NotificationType.error);
                return RedirectToAction(viewTaxClassDetail, new { id = model.TaxClassId });
            }
            _taxRuleAgent.BindDropdownValues(model,true);
            return View(createEditViewTaxRule, model);
        }

        /// <summary>
        /// Delete Tax rule
        /// </summary>
        /// <param name="id">int id</param>
        /// <returns>Returns JsonResult</returns>
        public JsonResult DeleteTaxRule(int id)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();
            if (!Equals(null, id))
            {
                status = _taxRuleAgent.Delete(id);
                message = status ? ZnodeResources.DeleteMessage : ZnodeResources.DeleteErrorMessage;
            }
            return Json(new { sucess = status, message = message, id = id, isFadeOut = isFadeOut });
        }

        /// <summary>
        /// Bind the State List on country code.
        /// </summary>
        /// <param name="CountryCode">country code</param>
        /// <returns>JsonResult</returns>
        [HttpGet]
        public JsonResult BindStateList(string countryCode)
        {
            FilterCollection filters = new FilterCollection();
            filters.Add(new FilterTuple(FilterKeys.CountryCode, FilterOperators.Equals, countryCode.ToString()));
            var list = _taxRuleAgent.BindStateList(filters);
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Bind the County List on state code.
        /// </summary>
        /// <param name="StateCode">state code</param>
        /// <returns>JsonResult</returns>
        [HttpGet]
        public JsonResult BindCountyList(string stateCode)
        {
            FilterCollection filters = new FilterCollection();
            filters.Add(new FilterTuple(FilterKeys.StateCode, FilterOperators.Equals, stateCode.ToString()));
            var list = _taxRuleAgent.BindCountyList(filters);
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}