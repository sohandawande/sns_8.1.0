﻿using Resources;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Enum;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Controllers
{
    public class CatalogController : BaseController
    {
        #region Private Variables
        #region Private Agent Variables
        private readonly ICatalogAgent _catalogAgent;
        private readonly ICategoryAgent _categoryAgent;
        private readonly ICategoryNodeAgent _categoryNodeAgent;
        private readonly ICategoryProfileAgent _categoryProfileAgent;
        private readonly IProductSearchSettingAgent _productSearchSettingAgent;
        private readonly IProfilesAgent _profilesAgent;
        #endregion

        #region Private Constant Variables
        private const string deleteView = "Delete";
        private const string copyView = "Copy";
        private const string categoryPageType = "Category";
        private const string manageAction = "Manage";
        private const string associateCategoriesView = "AssociateCategories";
        private const string categoryNodeIdSessionVariable = "CategoryNodeID";
        private const string manageCategoryProfilesPartialView = "_ManageCategoryProfiles";
        private const string searchCategoriesPartialView = "_SearchCategories";
        private const string categoryListPartialView = "_CategoriesList";
        private const string copyCatalogName = "Copy Of {0}";
        private const string separatorForOrCondition = "|";
        private const string title = "Title";
        #endregion
        #endregion

        #region Constructor
        public CatalogController()
        {
            _catalogAgent = new CatalogAgent();
            _categoryAgent = new CategoryAgent();
            _categoryNodeAgent = new CategoryNodeAgent();
            _categoryProfileAgent = new CategoryProfileAgent();
            _productSearchSettingAgent = new ProductSearchSettingAgent();
            _profilesAgent = new ProfilesAgent();
        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Gets the catalogs list.
        /// </summary>
        /// <returns>Returns the catalogs list.</returns>            
        public ActionResult List([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model)
        {
            CatalogListViewModel catalogs = _catalogAgent.GetCatalogs(model.Filters, model.SortCollection, model.Page, model.RecordPerPage);
            var gridModel = FilterHelpers.GetDynamicGridModel(model, catalogs.Catalogs, Convert.ToInt32(ListType.CatalogList));
            gridModel.TotalRecordCount = catalogs.TotalResults;
            return ActionView(MvcAdminConstants.ListView, gridModel);
        }

        #region Manage Catalog
        /// <summary>
        /// Manages the existing catalog.
        /// </summary>
        /// <param name="id">The id of catalog.</param>
        /// <returns>Returns view for catalog name and associated categories in edit mode.</returns>
        [HttpGet]
        public ActionResult Manage(int id, [ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model, int categoryProfileId = 0, string tabMode = "")
        {
            if (!string.IsNullOrEmpty(tabMode))
            {
                TempData[MvcAdminConstants.SetTabMode] = tabMode;
            }
            CatalogViewModel catalog = _catalogAgent.GetCatalogDetails(id, categoryProfileId > 0 ? categoryProfileId : 0);
            return ActionView(manageAction, catalog);
        }

        /// <summary>
        /// Manages the existing catalog.
        /// </summary>
        /// <param name="catalogId">The Id of catalog.</param>
        /// <returns>Returns view.</returns>
        [HttpPost]
        public ActionResult Manage(CatalogViewModel model)
        {
            if (!Equals(model, null))
            {
                TempData[MvcAdminConstants.Notifications] = _catalogAgent.UpdateCatalog(model.CatalogId, model) ? GenerateNotificationMessages(ZnodeResources.UpdateMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.UpdateErrorMessage, NotificationType.error);
            }
            return RedirectToAction(MvcAdminConstants.ListView);
        }
        #endregion

        #region Create Catalog
        /// <summary>
        /// Creates a new catalog.
        /// </summary>
        /// <returns>Returns view of create-edit type for catalog.</returns>
        [HttpGet]
        public ActionResult Create()
        {
            CatalogViewModel model = new CatalogViewModel();
            return ActionView(MvcAdminConstants.CreateEditView, model);
        }

        /// <summary>
        /// Creates a new catalog.
        /// </summary>
        /// <param name="model">The model of type CatalogViewModel.</param>
        /// <returns>Returns view of create-edit type for catalog.</returns>
        [HttpPost]
        public ActionResult Create(CatalogViewModel model)
        {
            CatalogViewModel modelSaved = new CatalogViewModel();
            if (!Equals(model, null))
            {
                modelSaved = _catalogAgent.CreateCatalog(model);
                TempData[MvcAdminConstants.Notifications] = !Equals(modelSaved, null) ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.SaveErrorMessage, NotificationType.error);
            }
            return RedirectToAction(manageAction, new { id = modelSaved.CatalogId > 0 ? modelSaved.CatalogId : 0, tabMode = SelectedTab.Categories });
        }
        #endregion

        #region Copy Catalog
        /// <summary>
        /// Creates a copy of an existing catalog.
        /// </summary>
        /// <param name="id">The Id of catalog.</param>
        /// <returns>Returns view of copy of catalog.</returns>
        [HttpGet]
        public ActionResult Copy(int id)
        {
            CatalogViewModel catalogs = _catalogAgent.GetCatalog(id > 0 ? id : 0);
            catalogs.Name = string.Format(copyCatalogName, catalogs.Name);
            return ActionView(copyView, catalogs);
        }

        /// <summary>
        /// Creates a copy of an existing catalog.
        /// </summary>
        /// <param name="model">The model of type CatalogViewModel.</param>
        /// <returns>Redirects to action List.</returns>
        [HttpPost]
        public ActionResult Copy(CatalogViewModel model)
        {
            if (!Equals(model, null))
            {
                TempData[MvcAdminConstants.Notifications] = _catalogAgent.CopyCatalog(model) ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.SaveErrorMessage, NotificationType.error);
            }
            return RedirectToAction(MvcAdminConstants.ListView);
        }
        #endregion

        #region Delete Catalog
        /// <summary>
        /// Deletes an existing catalog.
        /// </summary>
        /// <param name="id">The id of the catalog.</param>
        /// <returns>Returns view of delete.</returns>
        [HttpGet]
        public ActionResult Delete(int id)
        {
            CatalogViewModel catalog = new CatalogViewModel();
            catalog = _catalogAgent.GetCatalog(id);
            catalog.PreserveCategories = true;
            return View(deleteView, catalog);
        }

        /// <summary>
        /// Deletes an existing catalog.
        /// </summary>
        /// <param name="model">The model of type CatalogViewModel.</param>
        /// <returns>Redirects to action List.</returns>
        [HttpPost]
        public ActionResult Delete(CatalogViewModel model)
        {
            if (!Equals(model, null))
            {
                string message = string.Empty;
                bool status = _catalogAgent.DeleteCatalog(model, out message);
                TempData[MvcAdminConstants.Notifications] = model.IsDefault ? GenerateNotificationMessages(ZnodeResources.DefaultCatalogFailedToDelete, NotificationType.error) : status ? GenerateNotificationMessages(ZnodeResources.DeleteMessage, NotificationType.success) : GenerateNotificationMessages(message, NotificationType.error);
            }
            return RedirectToAction(MvcAdminConstants.ListView);

        }
        #endregion

        #region Associate Category
        /// <summary>
        /// Associates an already created category to the catalog.
        /// </summary>
        /// <param name="catalogId">The id of the catalog.</param>
        /// <param name="categoryName">The name of the category.</param>
        /// <param name="categoryId">The id of the category.</param>
        /// <returns>Returns view.</returns>
        [HttpGet]
        public ActionResult AssociateCategories(int catalogId, string categoryName = "", int categoryId = 0, int page = 1, int recordPerPage = 10)
        {
            CategoryNodeViewModel model = new CategoryNodeViewModel();
            _catalogAgent.BindAssociatedCategoryNodeData(catalogId, categoryName, model);
            return View(model);
        }

        /// <summary>
        ///  Associates an already created category to the catalog.
        /// </summary>
        /// <param name="model">The model of the type CategoryNodeViewModel.</param>
        /// <returns>Retuns view.</returns>
        [HttpPost]
        public ActionResult AssociateCategories(CategoryNodeViewModel model)
        {
            if (!Equals(model, null))
            {
                if (Equals(model.CategoryId, 0))
                {
                    TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.SelectValidCategory, NotificationType.error);
                    return RedirectToAction(associateCategoriesView, new { catalogId = model.CatalogId, categoryName = model.Category.Name });
                }
                if (_categoryNodeAgent.AssociateCategory(model))
                {
                    TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success);
                    return RedirectToAction(manageAction, new { id = model.CatalogId, tabMode = SelectedTab.Categories });
                }
                TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.CategoryAlreadyExistsError, NotificationType.error);
                return RedirectToAction(associateCategoriesView, new { catalogId = model.CatalogId, categoryName = model.Category.Name });
            }
            return RedirectToAction(associateCategoriesView, new { catalogId = model.CatalogId });
        }
        #endregion

        #region Edit Associated Category
        /// <summary>
        /// Edit- Gets the category node data.
        /// </summary>
        /// <param name="id">The id of the category node</param>
        /// <param name="categoryProfileId">The id of category profile.</param>
        /// <returns>Returns view.</returns>      
        public ActionResult Edit(int id, int categoryProfileId = 0, [ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel formModel = null)
        {
            Session[categoryNodeIdSessionVariable] = id;
            CategoryNodeViewModel model = _categoryNodeAgent.EditCategoryNode(id);
            int profileCount = _catalogAgent.GetAssociateCategoryProfilesData(model.CategoryId).CategoryProfile.Count;
            if (Equals(profileCount, 0))
            {
                model.IsSuccess = true;
            }

            _catalogAgent.BindEditAssociateCategoryData(id, categoryProfileId, model, formModel);
            return View(associateCategoriesView, model);
        }

        /// <summary>
        /// Edit- Posts the category node data.
        /// </summary>
        /// <param name="model">The mdoel of type CategoryNodeViewModel</param>
        /// <returns>Returns view.</returns>
        [HttpPost]
        public ActionResult Edit(CategoryNodeViewModel model)
        {
            if (!Equals(model, null))
            {
                if (Equals(model.CategoryId, 0))
                {
                    TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.SelectValidCategory, NotificationType.error);
                    return RedirectToAction(MvcAdminConstants.EditAction, new { id = Session[categoryNodeIdSessionVariable] });
                }
                CategoryNodeViewModel updatedModel = _categoryNodeAgent.UpdateCategoryNode(model.CategoryNodeId, model);

                TempData[MvcAdminConstants.Notifications] = !Equals(updatedModel, null) && string.IsNullOrEmpty(updatedModel.ErrorMessage) ? GenerateNotificationMessages(ZnodeResources.UpdateMessage, NotificationType.success) : GenerateNotificationMessages(updatedModel.ErrorMessage, NotificationType.error);

            }
            return RedirectToAction(manageAction, new { id = model.CatalogId, categoryProfileId = MvcAdminConstants.ZerothIndex, tabMode = SelectedTab.Categories });
        }
        #endregion

        #region Manage Category Profiles

        /// <summary>
        /// Gets the category profiles data.
        /// </summary>
        /// <param name="categoryId">int category Id</param>
        /// <param name="categoryNodeId">int catgeoryNode Id</param>
        /// <returns>Returns View</returns>
        public ActionResult GetCatgoryProfilesData(int categoryId, int categoryNodeId = 0)
        {
            CategoryProfileViewModel model = _catalogAgent.GetAssociateCategoryProfilesData(categoryId);
            model.CategoryNodeId = categoryNodeId;
            return PartialView(manageCategoryProfilesPartialView, model);
        }

        /// <summary>
        /// Manage- Post the category profiles data.
        /// </summary>
        /// <param name="model">Mdoel of type CategoryProfileViewModel</param>
        /// <returns>Returns view.</returns>
        [HttpPost]
        public ActionResult ManageCategoryProfiles(CategoryProfileViewModel model)
        {
            CategoryProfileViewModel profileModel = _catalogAgent.PostCategoryProfileData(model);
            return RedirectToAction(MvcAdminConstants.EditAction, new { id = model.CategoryNodeId, categoryProfileId = profileModel.CategoryProfileID });
        }

        /// <summary>
        /// This method will show the update view of Associated profiles
        /// </summary>
        /// <param name="profileID">int Profile Id</param>
        /// <param name="categoryProfileID">int Category Profile Id</param>
        /// <param name="CategoryID">int Category Id</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ProfilesEdit(int profileID, int categoryProfileID, int categoryId)
        {
            CategoryProfileViewModel model = _catalogAgent.GetEditCategoryProfilesData(profileID, categoryProfileID, categoryId);
            model.CategoryNodeId = Convert.ToInt32(Session[categoryNodeIdSessionVariable]);
            return PartialView(manageCategoryProfilesPartialView, model);
        }

        /// <summary>
        /// This method will call the delete method of Associate profiles
        /// </summary>
        /// <param name="CategoryProfileID">int Category Profile Id</param>
        /// <param name="model">CategoryProfileViewModel model</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ProfilesEdit(int CategoryProfileID, CategoryProfileViewModel model)
        {
            if (CategoryProfileID > 0)
            {
                TempData[MvcAdminConstants.Notifications] = _categoryProfileAgent.UpdatecategoryProfile(CategoryProfileID, model) ? GenerateNotificationMessages(ZnodeResources.UpdateMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.UpdateErrorMessage, NotificationType.error);
            }
            return RedirectToAction(MvcAdminConstants.EditAction, new { id = model.CategoryNodeId, categoryProfileId = model.CategoryProfileID });
        }

        /// <summary>
        /// This method will delete the Category profile id
        /// </summary>
        /// <param name="CategoryProfileID">int Category Profile ID</param>
        /// <returns></returns>
        public JsonResult ProfilesDelete(int? id)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();
            if (!Equals(null, id))
            {
                status = _categoryProfileAgent.DeleteCategoryProfile(id.Value);
                message = status ? ZnodeResources.DeleteMessage : ZnodeResources.DeleteErrorMessage;
            }
            return Json(new { sucess = status, message = message, id = id, isFadeOut = isFadeOut });
        }
        #endregion

        /// <summary>
        /// Delete associated category node.
        /// </summary>
        /// <param name="id">The id of category node</param>
        /// <returns>Returns json result.</returns>
        public JsonResult DeleteAssociatedCategory(int id)
        {
            bool status = true;
            string message = string.Empty;
            string ErrorMessage = string.Empty;
            bool isFadeOut = CheckIsFadeOut();
            if (!Equals(null, id))
            {
                ZnodeDeleteViewModel viewModel = _categoryNodeAgent.DeleteCategoryNode(id);
                if (!viewModel.IsDeleted)
                {
                    status = viewModel.IsDeleted;
                    ErrorMessage = viewModel.ErrorMessage;
                }
                message = string.IsNullOrEmpty(ErrorMessage) ? ZnodeResources.DeleteMessage : ErrorMessage;
            }
            return Json(new { sucess = status, message = message, id = id, isFadeOut = isFadeOut });
        }

        /// <summary>
        /// Display category list and category search
        /// </summary>
        /// <param name="model">FilterCollectionDataModel model</param>
        /// <returns>Returns View</returns>       
        public ActionResult SearchCategory([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model)
        {
            //The filters is assigned to new filter collcetion so that we can reuse the original values for search
            FilterCollection originalFilter = model.Filters;
            SetFiltersForSearch(model);
            CatalogAssociatedCategoriesListViewModel categoriesList = _categoryAgent.GetAllCategories(model.Filters, model.SortCollection, model.Page, model.RecordPerPage);
            //Reassigning the original filters
            model.Filters = originalFilter;
            var gridModel = FilterHelpers.GetDynamicGridModel(model, categoriesList.AssociatedCategories, Convert.ToInt32(ListType.SearchCategoryList));
            List<CatalogModel> catalogs = _productSearchSettingAgent.GetCatalogList();
            FilterHelpers.CreateDropdown(ref gridModel, catalogs, model.Filters, MvcAdminConstants.LabelCatalogId, MvcAdminConstants.LabelName, MvcAdminConstants.LabelCatalog);
            gridModel.TotalRecordCount = categoriesList.TotalResults;
            return ActionView(searchCategoriesPartialView, gridModel);
        }

        /// <summary>
        /// Bind css list to dropdown.
        /// </summary>
        /// <param name="CssThemeId">The id of theme.</param>
        /// <returns>Returns Json result.</returns>      
        public JsonResult BindCssList(int CssThemeId)
        {            
            List<SelectListItem> list = _categoryAgent.BindCssList(CssThemeId);
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Binds master page list to dropdown.
        /// </summary>
        /// <param name="CssThemeId">The id of theme.</param>
        /// <returns>Returns Json result.</returns>        
        public JsonResult BindMasterPageList(int CssThemeId)
        {
            List<SelectListItem> list = _categoryAgent.BindMasterPageList(CssThemeId, categoryPageType);
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// To Get the Product Details based on Product Name & CatalogId
        /// </summary>
        /// <param name="searchText">Product Name</param>
        /// <param name="catalogId">Id of the Catalog</param>
        /// <returns></returns>
        public JsonResult AutoCompleteCategorySearch(string searchText, int catalogId)
        {
            try
            {
                FilterCollectionDataModel model = new FilterCollectionDataModel();
                FilterCollection filter = new FilterCollection();
                filter.Add(new FilterTuple(FilterKeys.Name, FilterOperators.Like, searchText.ToLower()));
                filter.Add(new FilterTuple(FilterKeys.CatalogId, FilterOperators.Equals, catalogId.ToString()));
                model.Filters = filter;
                SetFiltersForSearch(model);
                CatalogAssociatedCategoriesListViewModel categoryDetails = _categoryAgent.GetAllCategories(model.Filters, null, null, null);
                return Json(categoryDetails.AssociatedCategories, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

        }

        /// <summary>
        /// Sets the catalog associated categories.
        /// </summary>
        /// <param name="catalogId">The id of catalog</param>      
        /// <param name="catalogs">Model of type CatalogViewModel.</param>       
        public ActionResult CatalogAssociatedCategoriesList(int catalogId, [ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model)
        {
            CatalogAssociatedCategoriesListViewModel categoriesList = _categoryAgent.GetCategoryByCataLogId(catalogId, model.Filters, model.SortCollection, model.Page, model.RecordPerPage);
            categoriesList.CatalogId = catalogId;
            var gridModel = FilterHelpers.GetDynamicGridModel(model, categoriesList.AssociatedCategories, Convert.ToInt32(ListType.CatalogAssociatedCategoryList));
            gridModel.TotalRecordCount = categoriesList.TotalResults;
            categoriesList.Grid = gridModel;
            return ActionView(categoryListPartialView, categoriesList);
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Set filters for OR condition with Null check.
        /// </summary>
        /// <param name="model">FilterCollectionDataModel model, to fetch data on the basis of filters provided.</param>
        private void SetFiltersForSearch(FilterCollectionDataModel model)
        {
            if (!Equals(model.Filters, null) && model.Filters.Count > 0)
            {
                FilterCollection tempCollection = new FilterCollection();
                tempCollection = model.Filters;
                FilterCollection newCollection = new FilterCollection();

                foreach (var tuple in model.Filters)
                {
                    if (tuple.Item1.ToLower().Equals(MvcAdminConstants.LabelCatalogId.ToLower()))
                    {
                        string newKeyName = tuple.Item1;
                        newKeyName += separatorForOrCondition;
                        newCollection.Add(new FilterTuple(newKeyName, tuple.Item2, tuple.Item3));
                    }
                }
                foreach (var temp in tempCollection)
                {
                    if (!Equals(temp.Item1.ToLower(), MvcAdminConstants.LabelCatalogId.ToLower()))
                        newCollection.Add(temp);
                }
                model.Filters = newCollection;
            }
        }
        #endregion

    }
}
