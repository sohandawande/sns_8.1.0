﻿using Resources;
using System;
using System.Configuration;
using System.Net;
using System.Web.Mvc;
using Znode.Engine.Api.Models.Enum;
using Znode.Engine.Exceptions;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Controllers
{
    /// <summary>
    /// Store Controller
    /// </summary>
    public class StoresController : BaseController
    {
        #region Private Variables
        #region Readonly Properties
        private readonly IPortalAgent _portalAgent;
        private readonly IDomainAgent _domainAgent;
        private readonly IPortalCatalogAgent _portalCatalogAgent;
        private readonly IPortalProfileAgent _portalProfileAgent;
        private readonly IPortalCountryAgent _portalCountryAgent;
        private readonly IClearAPICacheAgent _clearApiCaheAgent;
        #endregion

        #region Constant Variables
        private const string AddUrlView = "CreateEditUrl";
        private const string ListView = "List";
        private const string EditDisplayView = "EditDisplay";
        private const string CreateEditView = "CreateEditStore";
        private const string EditSMTPView = "EditSMTP";
        private const string EditJavascriptView = "EditJavascript";
        private const string EditShippingView = "EditShipping";
        private const string EditUnitsView = "EditUnits";
        private const string EditPortalCatalogView = "EditPortalcatalog";
        private const string PortalCatalogPartialView = "_PortalCatalog";
        private const string PortalProfilePartialView = "_PortalProfileList";
        private const string CreateEditPortalProfileView = "CreateEditPortalProfile";
        private const string PortalId = "PortalId";
        private const string PortalCountryPartialView = "_PortalCountries";
        private const string ManageUrlList = "_Url";
        private const string CacheCleredMessage = "Cache Cleared.";
        private const string CacheNotCleredMessage = "Error occurred during clear cache: ";
        private const string ModelNotFoundMessage = "No data found related to the provided URL.";
        private const string SameURLErrorMessage = "Could not clear the cache of the current site.";
        private const int DefaultPortalId = 1;
        #endregion
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor for Stores Controller.
        /// </summary>
        public StoresController()
        {
            _portalAgent = new PortalAgent();
            _domainAgent = new DomainAgent();
            _portalCatalogAgent = new PortalCatalogAgent();
            _portalProfileAgent = new PortalProfileAgent();
            _portalCountryAgent = new PortalCountryAgent();
            _clearApiCaheAgent = new ClearAPICacheAgent();
        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Returns Index View.
        /// </summary>
        /// <returns>Index View.</returns>
        public ActionResult Index()
        {
            return View();
        }


        /// <summary>
        /// Gets Create a store page for creating a store.
        /// </summary>
        /// <returns>View for creating store.</returns>
        [HttpGet]
        public ActionResult Create()
        {
            PortalViewModel portalViewModel = new PortalViewModel();
            portalViewModel = _portalAgent.BindPortalInformation(portalViewModel);
            portalViewModel.IsEnableSinglePageCheckout = true;
            return View(CreateEditView, portalViewModel);
        }

        /// <summary>
        /// Creates a portal
        /// </summary>
        /// <param name="portalViewModel">Portal view model returned from the form.</param>
        /// <returns>Manage View after creation of portal.</returns>
        [HttpPost]
        public ActionResult Create(PortalViewModel portalViewModel)
        {
            string notification = string.Empty;
            if (!Equals(portalViewModel, null))
            {
                portalViewModel.LogoPathImageName = portalViewModel.LogoPath.FileName;


                bool isPortalCreated = _portalAgent.CreatePortal(portalViewModel, out notification);

                if (isPortalCreated)
                {
                    return RedirectToAction(MvcAdminConstants.ManageView, new { id = portalViewModel.PortalId });
                }
            }
            portalViewModel = _portalAgent.BindPortalInformation(portalViewModel);
            TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(notification, NotificationType.error);
            return View(CreateEditView, portalViewModel);
        }


        /// <summary>
        /// To show Manage View with all tabs.
        /// </summary>
        /// <param name="id">Portal Id</param>
        /// <returns>Gets the manage view of portal.</returns>
        [HttpGet]
        public ActionResult Manage(int id = 0, string tabMode = "")
        {
            if (!string.IsNullOrEmpty(tabMode))
            {
                TempData[MvcAdminConstants.SetTabMode] = tabMode;
            }
            TempData[PortalId] = id;
            PortalViewModel portalViewModel = new PortalViewModel();
            portalViewModel = _portalAgent.GetPortalInformationByPortalId(id);
            portalViewModel = _portalAgent.BindPortalInformation(portalViewModel);
            return View(MvcAdminConstants.ManageView, portalViewModel);
        }


        /// <summary>
        /// Copies a store.
        /// </summary>
        /// <param name="portalId">Portal Id of the store being copied.</param>
        /// <returns>Returns view of list of stores with the copied store and status message.</returns>
        [HttpGet]
        public ActionResult Copy(int id)
        {
            bool isPortalCopied = _portalAgent.CopyStore(id);
            string copiedStoreName = _portalAgent.GetPortalInformationByPortalId(id).StoreName;
            if (!isPortalCopied)
            {
                TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.ErrorCopyStore, NotificationType.error);
            }
            TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(string.Format(ZnodeResources.SuccessStoreCopied, copiedStoreName), NotificationType.success);
            return RedirectToAction(MvcAdminConstants.ListView);
        }

        /// <summary>
        /// Deletes the specified portal.
        /// </summary>
        /// <param name="portalId">Portal id of the store being deleted.</param>
        /// <returns>Redirects to list view with deleted portal.</returns>
        public JsonResult Delete(int id)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();

            if (!id.Equals(0))
            {
                if (!id.Equals(PortalAgent.CurrentPortal.PortalId))
                {
                    status = _portalAgent.DeletePortal(id);
                    message = status ? ZnodeResources.DeleteMessage : ZnodeResources.ErrorStoreDelete;
                }
                else
                {
                    return Json(new { sucess = false, message = ZnodeResources.ErrorCannotDeleteDefaultStore, id = id, isFadeOut = isFadeOut });
                }
            }
            return Json(new { sucess = status, message = message, id = id, isFadeOut = isFadeOut });
        }

        /// <summary>
        /// Gets the main page of the store.
        /// </summary>
        /// <returns>View of the store's main page.</returns>
        public ActionResult List([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model)
        {
            UrlHelper urlhelper = new UrlHelper();
            PortalListViewModel portals = _portalAgent.GetPortals(model.Filters, model.SortCollection, model.Page, model.RecordPerPage);
            var gridModel = FilterHelpers.GetDynamicGridModel(model, portals.Portals, 2);
            gridModel.TotalRecordCount = portals.TotalResults;
            return ActionView(gridModel);
        }

        /// <summary>
        /// Gets Css list according to Theme id to be selected while creating portal.
        /// </summary>
        /// <param name="CssThemeId">Css Id according to which CSS List is to be binded.</param>
        /// <returns>JSON response containing css list.</returns>
        [HttpGet]
        public JsonResult BindCssList(int cssThemeId)
        {
            var list = _portalAgent.BindCssList(cssThemeId);
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets currency information.
        /// </summary>
        /// <param name="currencyId">Informtion for a particular currency</param>
        /// <returns>Json value containing currency information.</returns>
        [HttpGet]
        public JsonResult GetCurrencyInformation(int currencyId)
        {
            var currencyInfo = _portalAgent.GetCurrencyInformationByCurrencyId(currencyId);
            return Json(currencyInfo, JsonRequestBehavior.AllowGet);
        }

        #region Manage General Information
        /// <summary>
        /// Gets the portal General Information for edit.
        /// </summary>
        /// <param name="id">Portal id of the portal to be edited.</param>
        /// <returns>View for edit store general settings.</returns>
        [HttpGet]
        public ActionResult EditStoreGeneral(int id = 0)
        {
            PortalViewModel portalViewModel = new PortalViewModel();
            portalViewModel = _portalAgent.GetPortalInformationByPortalId(id);
            return View(CreateEditView, portalViewModel);
        }

        /// <summary>
        /// Submits edited stores General information.
        /// </summary>
        /// <param name="portalViewModel">Portal View model.</param>
        /// <returns>Returns Manage view if store is edited successfully or stays on the same view.</returns>
        [HttpPost]
        public ActionResult EditStoreGeneral(PortalViewModel portalViewModel)
        {
            if (!Equals(portalViewModel, null))
            {
                portalViewModel = _portalAgent.UpdatePortal(portalViewModel, (int)SelectedTab.General);
                if (!Equals(portalViewModel, null) && portalViewModel.PortalId > 0)
                {
                    TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.SuccessUpdateStore, NotificationType.success);
                    return RedirectToAction(MvcAdminConstants.ManageView, new { id = portalViewModel.PortalId, tabMode = SelectedTab.General });
                }
                TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.ErrorUpdateStore, NotificationType.error);
            }
            return View(CreateEditView, portalViewModel);
        }
        #endregion

        #region Manage Catalog

        /// <summary>
        /// Gets the list of portal catalog.
        /// </summary>
        /// <param name="model">FilterCollectionDataModel</param>
        /// <param name="productId">Product id according to which portalcatalog list will be retrieved.</param>
        /// <returns>View containg portal catalog list.</returns>
        public ActionResult CatalogList(int portalId, [ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model)
        {
            PortalCatalogListViewModel portalCatalogs = _portalCatalogAgent.GetPortalCatalogList(portalId, model.Filters, model.SortCollection, model.Page, model.RecordPerPage);
            portalCatalogs.PortalId = portalId;
            if (!Equals(portalCatalogs, null))
            {
                ReportModel gridModel = FilterHelpers.GetDynamicGridModel(model, portalCatalogs.PortalCatalogs, Convert.ToInt32(ListType.PortalCatalogList));
                gridModel.TotalRecordCount = portalCatalogs.TotalResults;
                portalCatalogs.Grid = gridModel;
                return ActionView(PortalCatalogPartialView, portalCatalogs);
            }
            return ActionView(PortalCatalogPartialView, portalCatalogs);
        }

        /// <summary>
        /// Edits Portal catalog section for store.
        /// </summary>
        /// <param name="id">Portal catalog's id whose Portal catalog settings wll be edited.</param>
        /// <returns>View to edit Portal catalog settings.</returns>
        [HttpGet]
        public ActionResult EditPortalCatalog(int id = 0)
        {
            PortalCatalogViewModel portalCatalogViewModel = new PortalCatalogViewModel();
            portalCatalogViewModel = _portalCatalogAgent.GetPortalCatalogWithDropDowns(id);
            return View(EditPortalCatalogView, portalCatalogViewModel);
        }

        /// <summary>
        /// Edits Portal catalog section for store.
        /// </summary>
        /// <param name="portalViewModel">Edited portalcatalog model.</param>
        /// <returns>View according to status of portalcatalog edit.</returns>
        [HttpPost]
        public ActionResult EditPortalCatalog(PortalCatalogViewModel portalCatalogviewModel)
        {
            if (!Equals(portalCatalogviewModel, null))
            {
                if (_portalCatalogAgent.UpdatePortalCatalog(portalCatalogviewModel))
                {
                    TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.SuccessUpdateStore, NotificationType.success);
                    return RedirectToAction(MvcAdminConstants.ManageView, new { id = portalCatalogviewModel.PortalId, tabMode = SelectedTab.Catalog });
                }
                TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.ErrorUpdateStore, NotificationType.error);
            }
            return View(EditPortalCatalogView, portalCatalogviewModel);
        }
        #endregion

        #region Manage URLs

        /// <summary>
        /// Get Store Urls.
        /// </summary>
        /// <param name="portalId">portal id</param>
        /// <param name="model">FilterCollectionDataModel model.</param>
        /// <returns>URL List View.</returns>
        public ActionResult UrlList(int portalId, [ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model)
        {
            DomainListViewModel list = _domainAgent.GetDomains(portalId, model.Filters, model.SortCollection, model.Page, model.RecordPerPage);
            list.PortalId = portalId;
            if (!Equals(list, null))
            {
                var gridModel = FilterHelpers.GetDynamicGridModel(model, list.Domains, Convert.ToInt32(ListType.StoreManageUrlList));
                gridModel.TotalRecordCount = list.TotalResults;
                list.GridModel = gridModel;
                return ActionView(ManageUrlList, list);
            }
            return ActionView(ManageUrlList, list);

        }

        /// <summary>
        /// To Show Add/Edit Domain Url View.
        /// </summary>
        /// <param name="id">Portal Id</param>
        /// <returns>View to Add URL.</returns>
        [HttpGet]
        public ActionResult AddUrl(int id = 0)
        {
            DomainViewModel model = new DomainViewModel();
            model.PortalId = id;
            return View(AddUrlView, model);
        }

        /// <summary>
        /// Add Domain url
        /// </summary>
        /// <param name="Model">DomainViewModel</param>
        /// <returns>Returns Manage view if URL is added successfully or stays on the same view.</returns>
        [HttpPost]
        public ActionResult AddUrl(DomainViewModel model)
        {
            try
            {
                TempData[MvcAdminConstants.Notifications] = _domainAgent.createDomainUrl(model)
                    ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success)
                    : GenerateNotificationMessages(ZnodeResources.SaveErrorMessage, NotificationType.error);
                return RedirectToAction(MvcAdminConstants.ManageView, new { @id = model.PortalId, tabMode = SelectedTab.Url });
            }
            catch (ZnodeException ex)
            {
                ExceptionHandler(ex);
                TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ex.ErrorMessage, NotificationType.error);
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex);
                TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ex.Message, NotificationType.error);
            }
            return View(AddUrlView, model);
        }

        /// <summary>
        /// To show Add/edit Domain url view. 
        /// </summary>
        /// <param name="PortalId">int Portal id</param>
        /// <param name="domainId">int domain id</param>
        /// <returns>View to Edit URL.</returns>
        [HttpGet]
        public ActionResult Edit(int? PortalId, int domainId)
        {
            if (!Equals(PortalId, null))
            {
                DomainViewModel viewModel = _domainAgent.GetDomain(domainId);
                viewModel.OldDomainName = viewModel.DomainName;
                if (Equals(viewModel, null))
                {
                    return RedirectToAction(AddUrlView, new { @id = PortalId });
                }
                return View(AddUrlView, viewModel);
            }
            return View(AddUrlView);
        }

        /// <summary>
        /// To update Domain Url data
        /// </summary>
        /// <param name="model">ProductTypesViewModel model</param>
        /// <returns>Show susscess/fail message and redirect to list page</returns>
        [HttpPost]
        public ActionResult Edit(DomainViewModel viewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    TempData[MvcAdminConstants.Notifications] = _domainAgent.UpdateDomainUrl(viewModel)
                        ? GenerateNotificationMessages(ZnodeResources.UpdateMessage, NotificationType.success)
                        : GenerateNotificationMessages(ZnodeResources.UpdateErrorMessage, NotificationType.error);
                    return RedirectToAction(MvcAdminConstants.ManageView, new { @id = viewModel.PortalId, tabMode = SelectedTab.Url });
                }
                return View(viewModel);
            }
            catch (ZnodeException ex)
            {
                ExceptionHandler(ex);
                TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ex.ErrorMessage, NotificationType.error);
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex);
            }
            return View(AddUrlView, viewModel);
        }

        /// <summary>
        /// Deletes an existing Domain Url.
        /// </summary>
        /// <param name="id">Domain Id</param>
        /// <returns>Deletes Domain and returns Json.</returns>
        [HttpPost]
        public JsonResult DeleteDomain(int? id)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();
            if (!Equals(id, null))
            {
                status = _domainAgent.DeleteDomainUrl(id.Value);
                message = status ? ZnodeResources.DeleteMessage : ZnodeResources.ErrorDeleteProductType;
            }
            return Json(new { sucess = status, message = message, id = id, isFadeOut = isFadeOut });
        }

        #endregion

        #region Portal Profile
        /// <summary>
        /// Gets the Portal profile List.
        /// </summary>
        /// <param name="portalId">Portal ID</param>
        /// <param name="model">Portal Profile View model.</param>
        /// <returns>View.</returns>
        public ActionResult PortalProfileList(int portalId, [ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model)
        {
            PortalProfileListViewModel portalProfiles = _portalProfileAgent.GetPortalProfiles(portalId, model.Filters, model.SortCollection, model.Page, model.RecordPerPage);
            portalProfiles.PortalId = portalId;
            if (!Equals(portalProfiles, null))
            {
                var gridModel = FilterHelpers.GetDynamicGridModel(model, portalProfiles.PortalProfiles, Convert.ToInt32(ListType.vw_ZnodeGetProfilewithSerialNumber));
                gridModel.TotalRecordCount = portalProfiles.TotalResults;
                portalProfiles.Grid = gridModel;
                return ActionView(PortalProfilePartialView, portalProfiles);
            }
            return ActionView(PortalProfilePartialView, portalProfiles);
        }

        /// <summary>
        /// Creates Portal profie.
        /// </summary>
        /// <param name="id">Portal profile id.</param>
        /// <returns>View.</returns>
        [HttpGet]
        public ActionResult CreatePortalProfile(int id)
        {
            PortalViewModel portalViewModel = new PortalViewModel();
            portalViewModel.PortalId = id;
            PortalProfileViewModel portalProfileViewModel = new PortalProfileViewModel();
            _portalProfileAgent.BindPortalProfileInformation(portalProfileViewModel, id);
            if (portalProfileViewModel.Profiles.Count < 1)
            {
                TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.ErrorAllProfilesAddedToStore, NotificationType.error);
                return RedirectToAction(MvcAdminConstants.ManageView, new { id = id, tabMode = SelectedTab.Profile });
            }
            portalViewModel.PortalProfileViewModel = portalProfileViewModel;
            return View(CreateEditPortalProfileView, portalViewModel);
        }

        /// <summary>
        /// Creates a portal profile.
        /// </summary>
        /// <param name="portalViewModel">Portal View model.</param>
        /// <returns>View.</returns>
        [HttpPost]
        public ActionResult CreatePortalProfile(PortalViewModel portalViewModel)
        {
            if (!Equals(portalViewModel, null))
            {
                //Create portal profile
                PortalProfileViewModel portalProfileViewModel = new PortalProfileViewModel();
                portalProfileViewModel.PortalID = portalViewModel.PortalId;
                portalProfileViewModel.ProfileID = portalViewModel.PortalProfileViewModel.ProfileID;
                bool isPortalProfileCreated = _portalProfileAgent.CreatePortalProfile(portalProfileViewModel);

                //Update portal for anonymous profile id and registered profile id
                if (isPortalProfileCreated)
                {
                    portalViewModel = _portalAgent.UpdatePortal(portalViewModel, (int)SelectedTab.Profile);
                    if (!Equals(portalViewModel, null) && portalViewModel.PortalId > 0)
                    {
                        TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.SuccessUpdateStore, NotificationType.success);
                        return RedirectToAction(MvcAdminConstants.ManageView, new { id = portalViewModel.PortalId, tabMode = SelectedTab.Profile });
                    }
                }
            }
            TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.ErrorUpdateStore, NotificationType.error);
            return View(CreateEditPortalProfileView, portalViewModel);
        }
        #endregion

        #region Manage Catalog

        /// <summary>
        /// Edits portal Profile.
        /// </summary>
        /// <param name="id">Portal profile id.</param>
        /// <returns>Portal profile</returns>
        [HttpGet]
        public ActionResult EditPortalProfile(int id = 0)
        {
            PortalViewModel portalViewModel = new PortalViewModel();
            PortalProfileViewModel portalProfileViewModel = _portalProfileAgent.GetPortalProfile(id);

            portalViewModel.PortalId = portalProfileViewModel.PortalID;
            portalViewModel = _portalAgent.GetPortalInformationByPortalId(portalViewModel.PortalId);

            _portalProfileAgent.BindExistingPortalProfileInformation(portalProfileViewModel.ProfileID, portalProfileViewModel);

            portalViewModel.PortalProfileViewModel = portalProfileViewModel;
            portalViewModel.PortalProfileViewModel.ProfileID = portalProfileViewModel.ProfileID;
            portalViewModel.PortalProfileViewModel.PortalID = portalProfileViewModel.PortalID;

            if (portalViewModel.DefaultAnonymousProfileId.Equals(portalProfileViewModel.ProfileID))
            {
                portalViewModel.IsDefaultAnonymousProfileId = true;
            }
            if (portalViewModel.DefaultRegisteredProfileId.Equals(portalProfileViewModel.ProfileID))
            {
                portalViewModel.IsDefaultRegisteredProfileId = true;
            }
            return View(CreateEditPortalProfileView, portalViewModel);
        }

        /// <summary>
        /// Edit portal profile.
        /// </summary>
        /// <param name="portalViewModel">Portal profile view model.</param>
        /// <returns>Portal profile</returns>
        [HttpPost]
        public ActionResult EditPortalProfile(PortalViewModel portalViewModel)
        {
            //Update portal profile
            PortalProfileViewModel portalProfileViewModel = new PortalProfileViewModel();
            portalProfileViewModel.PortalID = portalViewModel.PortalId;
            portalProfileViewModel.ProfileID = portalViewModel.PortalProfileViewModel.ProfileID;
            portalViewModel.PortalProfileViewModel = portalProfileViewModel;
            //Update portal
            if (!Equals(portalViewModel.PortalProfileViewModel, null))
            {
                portalViewModel = _portalAgent.UpdatePortal(portalViewModel, (int)SelectedTab.Profile);
                if (!Equals(portalViewModel, null) && portalViewModel.PortalId > 0)
                {
                    TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.SuccessUpdateStore, NotificationType.success);
                    return RedirectToAction(MvcAdminConstants.ManageView, new { id = portalViewModel.PortalId, tabMode = SelectedTab.Profile });
                }
            }
            return View(CreateEditPortalProfileView, portalViewModel);
        }

        /// <summary>
        /// Deletes portal profile.
        /// </summary>
        /// <param name="id">Portal profile id.</param>
        /// <returns>View.</returns>
        public JsonResult DeletePortalProfile(int id)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();

            if (!id.Equals(0))
            {
                status = _portalProfileAgent.DeletePortalProfile(id);
                message = status ? ZnodeResources.DeleteMessage : ZnodeResources.ErrorStoreDelete;
            }
            return Json(new { sucess = status, message = message, id = id, isFadeOut = isFadeOut });
        }
        #endregion

        #region Display

        /// <summary>
        /// Edits display section for store.
        /// </summary>
        /// <param name="id">Portal's id whose display settings wll be edited.</param>
        /// <returns>View to edit display settings.</returns>
        [HttpGet]
        public ActionResult EditStoreDisplay(int id = 0)
        {
            PortalViewModel portalViewModel = new PortalViewModel();
            portalViewModel = _portalAgent.GetPortalInformationByPortalId(id);
            return View(EditDisplayView, portalViewModel);
        }

        /// <summary>
        /// Edits display section for store.
        /// </summary>
        /// <param name="portalViewModel">Edited portal model.</param>
        /// <returns>View according to status of portal edit.</returns>
        [HttpPost]
        public ActionResult EditStoreDisplay(PortalViewModel portalViewModel)
        {
            if (!Equals(portalViewModel, null))
            {
                PortalViewModel model = _portalAgent.UpdatePortal(portalViewModel, (int)SelectedTab.Display);

                if (!Equals(model, null) && model.PortalId > 0)
                {
                    TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.SuccessUpdateStore, NotificationType.success);
                    return RedirectToAction(MvcAdminConstants.ManageView, new { id = model.PortalId, tabMode = SelectedTab.Display });
                }
                TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.ErrorUpdateStore, NotificationType.error);
            }
            //return View(EditDisplayView, portalViewModel);
            return RedirectToAction(MvcAdminConstants.ManageView, new { id = portalViewModel.PortalId, tabMode = SelectedTab.Display });
        }

        #endregion

        #region Edit SMTP
        /// <summary>
        /// Edits SMTP section for store.
        /// </summary>
        /// <param name="id">Portal's id whose display settings wll be edited.</param>
        /// <returns>View to edit SMTP settings.</returns>
        [HttpGet]
        public ActionResult EditStoreSMTP(int id = 0)
        {
            PortalViewModel portalViewModel = new PortalViewModel();
            portalViewModel = _portalAgent.GetPortalInformationByPortalId(id);
            return View(EditSMTPView, portalViewModel);
        }

        /// <summary>
        /// Edits SMTP section for store.
        /// </summary>
        /// <param name="portalViewModel">Edited portal model.</param>
        /// <returns>View according to status of portal edit.</returns>
        [HttpPost]
        public ActionResult EditStoreSMTP(PortalViewModel portalViewModel)
        {
            if (!Equals(portalViewModel, null))
            {
                portalViewModel = _portalAgent.UpdatePortal(portalViewModel, (int)SelectedTab.Smtp);
                if (!Equals(portalViewModel, null) && portalViewModel.PortalId > 0)
                {
                    TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.SuccessUpdateStore, NotificationType.success);
                    return RedirectToAction(MvcAdminConstants.ManageView, new { id = portalViewModel.PortalId, tabMode = SelectedTab.Smtp });
                }
                TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.ErrorUpdateStore, NotificationType.error);
            }
            return View(EditSMTPView, portalViewModel);
        }
        #endregion

        #region Edit Javascript
        /// <summary>
        /// Edits Javascript section for store.
        /// </summary>
        /// <param name="id">Portal's id whose display settings wll be edited.</param>
        /// <returns>View to edit Javascript settings.</returns>
        [HttpGet]
        public ActionResult EditStoreJavaScript(int id = 0)
        {
            PortalViewModel portalViewModel = new PortalViewModel();
            portalViewModel = _portalAgent.GetPortalInformationByPortalId(id);
            return View(EditJavascriptView, portalViewModel);
        }

        /// <summary>
        /// Edits Javascript section for store.
        /// </summary>
        /// <param name="portalViewModel">Edited portal model.</param>
        /// <returns>View according to status of portal edit.</returns>
        [HttpPost]
        public ActionResult EditStoreJavaScript(PortalViewModel portalViewModel)
        {
            if (!Equals(portalViewModel, null))
            {
                portalViewModel = _portalAgent.UpdatePortal(portalViewModel, (int)SelectedTab.Javascript);
                if (!Equals(portalViewModel, null) && portalViewModel.PortalId > 0)
                {
                    TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.SuccessUpdateStore, NotificationType.success);
                    return RedirectToAction(MvcAdminConstants.ManageView, new { id = portalViewModel.PortalId, tabMode = SelectedTab.Javascript });
                }
                TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.ErrorUpdateStore, NotificationType.error);
            }
            return View(EditSMTPView, portalViewModel);
        }
        #endregion

        #region Edit Shipping
        /// <summary>
        /// Edits Shipping section for store.
        /// </summary>
        /// <param name="id">Portal's id whose Shipping settings wll be edited.</param>
        /// <returns>View to edit Shipping settings.</returns>
        public ActionResult EditStoreShipping(int id = 0)
        {
            PortalViewModel portalViewModel = new PortalViewModel();
            portalViewModel = _portalAgent.GetPortalInformationByPortalId(id);
            return View(EditShippingView, portalViewModel);
        }

        /// <summary>
        /// Edits Shipping section for store.
        /// </summary>
        /// <param name="portalViewModel">Edited portal model.</param>
        /// <returns>View according to status of portal edit.</returns>
        [HttpPost]
        public ActionResult EditStoreShipping(PortalViewModel portalViewModel)
        {
            if (!Equals(portalViewModel, null))
            {
                portalViewModel = _portalAgent.UpdatePortal(portalViewModel, (int)SelectedTab.Shipping);
                if (!Equals(portalViewModel, null) && portalViewModel.PortalId > 0)
                {
                    TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.SuccessUpdateStore, NotificationType.success);
                    return RedirectToAction(MvcAdminConstants.ManageView, new { id = portalViewModel.PortalId, tabMode = SelectedTab.Shipping });
                }
                TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.ErrorUpdateStore, NotificationType.error);
            }
            return View(EditShippingView, portalViewModel);
        }
        #endregion

        #region Edit Units
        /// <summary>
        /// Edits Units section for store.
        /// </summary>
        /// <param name="id">Portal's id whose Units settings wll be edited.</param>
        /// <returns>View to edit Units settings.</returns>
        public ActionResult EditStoreUnits(int id = 0)
        {
            PortalViewModel portalViewModel = new PortalViewModel();
            portalViewModel = _portalAgent.GetPortalInformationByPortalId(id);
            return View(EditUnitsView, portalViewModel);
        }

        /// <summary>
        /// Edits Shipping section for store.
        /// </summary>
        /// <param name="portalViewModel">Edited portal model.</param>
        /// <returns>View according to status of portal edit.</returns>
        [HttpPost]
        public ActionResult EditStoreUnits(PortalViewModel portalViewModel)
        {
            if (!Equals(portalViewModel, null))
            {
                portalViewModel = _portalAgent.UpdatePortal(portalViewModel, (int)SelectedTab.Units);
                if (!Equals(portalViewModel, null) && portalViewModel.PortalId > 0)
                {
                    TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.SuccessUpdateStore, NotificationType.success);
                    return RedirectToAction(MvcAdminConstants.ManageView, new { id = portalViewModel.PortalId, tabMode = SelectedTab.Units });
                }
                TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.ErrorUpdateStore, NotificationType.error);
            }
            return View(EditUnitsView, portalViewModel);
        }
        #endregion

        #region Portal Country

        /// <summary>
        /// Gets List of Portal Country.
        /// </summary>
        /// <param name="portalId">Portal id.</param>
        /// <param name="model">Filter model</param>
        /// <returns>View for portal country list.</returns>
        public ActionResult PortalCountryList(int portalId, [ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model)
        {
            PortalCountriesListViewModel portalCountries = _portalCountryAgent.GetAllPortalCountries(portalId, model.Filters, model.SortCollection, model.Page, model.RecordPerPage);
            portalCountries.PortalId = portalId;
            if (!Equals(portalCountries, null))
            {
                var gridModel = FilterHelpers.GetDynamicGridModel(model, portalCountries.PortalCountryList, Convert.ToInt32(ListType.vw_ZNodeGetPortalCountry));
                gridModel.TotalRecordCount = portalCountries.TotalResults;
                portalCountries.Grid = gridModel;
                return ActionView(PortalCountryPartialView, portalCountries);
            }
            return ActionView(PortalCountryPartialView, portalCountries);
        }

        /// <summary>
        /// Gets list of Countries.
        /// </summary>
        /// <param name="model">Filter collection model.</param>
        /// <returns>View for list of countries.</returns>
        public ActionResult CountryList([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model, int portalId = 0)
        {
            ViewData[PortalId] = portalId;
            CountryListViewModel countries = _portalCountryAgent.GetCountryList(model.Filters, model.SortCollection);
            countries.PortalId = portalId;
            var gridModel = FilterHelpers.GetDynamicGridModel(model, countries.Countries, Convert.ToInt32(ListType.CountryList));
            gridModel.TotalRecordCount = countries.TotalResults;
            countries.Grid = gridModel;
            return ActionView(countries);
        }

        /// <summary>
        /// Deletes portal profile.
        /// </summary>
        /// <param name="id">Portal profile id.</param>
        /// <returns>View.</returns>
        public JsonResult DeletePortalCountry(int id)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();

            if (!id.Equals(0))
            {
                status = _portalCountryAgent.DeletePortalCountry(id, out message);
                message = status ? ZnodeResources.DeleteMessage : message;
            }
            return Json(new { sucess = status, message = message, id = id, isFadeOut = isFadeOut });
        }

        /// <summary>
        /// Adds a portal country.
        /// </summary>
        /// <param name="billableCountryCodes">List of billable country code.</param>
        /// <param name="shippableCountryCodes">List of shippable country code.</param>
        /// <param name="portalId">Portal Id.</param>
        /// <returns>Json result regarding portal countries are added or not.</returns>
        public JsonResult AddPortalCountry(int id, string billableCountryCodes, string shippableCountryCodes)
        {
            string message = string.Empty;

            bool isCountryAdded = _portalCountryAgent.AddPortalCountries(id, billableCountryCodes, shippableCountryCodes, out message);
            bool isFadeOut = CheckIsFadeOut();
            message = isCountryAdded ? ZnodeResources.CountryAddedSuccessfully : message;

            return Json(new { sucess = isCountryAdded, message = message, id = id, isFadeOut = isFadeOut, selectedTab = SelectedTab.Countries.ToString() });
        }

        #endregion

        #region Clear Cache

        /// <summary>
        /// This method will delete the cache of the demo store
        /// </summary>
        /// <param name="DomainName">string Domain Name</param>
        /// <param name="DomainId">int Domain Id</param>
        /// <returns>It will clear the cache of domain table and will do nothing in Admin site</returns>
        public ActionResult ClearDemoWebsiteCache(string domainName, int domainId)
        {
            DomainViewModel model = _domainAgent.GetDomain(domainId);
            int portalId = PortalAgent.CurrentPortal.PortalId;
            string message = string.Empty;

            if (!Equals(model, null))
            {
                if (model.DomainName.Contains(Request.Url.Authority))
                {
                    TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(SameURLErrorMessage, NotificationType.error);
                    return RedirectToAction(MvcAdminConstants.ManageView, new { id = portalId, tabMode = Convert.ToString(SelectedTab.Url) });
                }

                portalId = model.PortalId;

                string apiDomainName = ConfigurationManager.AppSettings["ZnodeApiRootUri"].ToString();
                if (Equals(apiDomainName.ToLower(), domainName.ToLower()))
                {
                    TempData[MvcAdminConstants.Notifications] = _clearApiCaheAgent.ClearApiCache() ? GenerateNotificationMessages(CacheCleredMessage, NotificationType.success) : GenerateNotificationMessages(CacheNotCleredMessage, NotificationType.error);
                    return RedirectToAction(MvcAdminConstants.ManageView, new { id = portalId, tabMode = Convert.ToString(SelectedTab.Url) });
                }
                else
                {
                    string webUrl = string.Concat(domainName, "/api/Service/ClearCache");
                    using (WebClient client = new WebClient())
                    {
                        try
                        {
                            message = client.DownloadString(webUrl);
                        }
                        catch (Exception ex)
                        {
                            TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(string.Concat(CacheNotCleredMessage, ex.Message.ToString()), NotificationType.error);
                            return RedirectToAction(MvcAdminConstants.ManageView, new { id = portalId, tabMode = Convert.ToString(SelectedTab.Url) });
                        }
                    }
                }

                message = message.Replace("\"", "");
                TempData[MvcAdminConstants.Notifications] = message.Length.Equals(0) ? GenerateNotificationMessages(CacheNotCleredMessage, NotificationType.error) : message.ToLower().IndexOf("error") > 0 ? 
                    GenerateNotificationMessages(message, NotificationType.error) :
                    GenerateNotificationMessages(message, NotificationType.success);
            }
            else
            {
                TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ModelNotFoundMessage, NotificationType.error);
            }

            return RedirectToAction(MvcAdminConstants.ManageView, new { id = portalId, tabMode = Convert.ToString(SelectedTab.Url) });
        }

        #endregion

        #endregion
    }
}
