﻿using Resources;
using System;
using System.Web.Mvc;
using Znode.Engine.Api.Models.Enum;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Controllers.SetUp
{
    public class ManageThemeController : BaseController
    {

        #region Private Agent variables
        private readonly ManageThemeAgent _manageThemeAgent;
        #endregion

        /// <summary>
        /// Public constructor of ManageThemeController
        /// </summary>
        public ManageThemeController()
        {
            _manageThemeAgent = new ManageThemeAgent();
        }

        /// <summary>
        /// This function will return the list of all the Themes.
        /// </summary>
        /// <param name="model">FilterCollectionDataModel</param>
        /// <returns>ActionResult view of theme list</returns>
        public ActionResult List([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model)
        {
            ThemeListViewModel themeList = _manageThemeAgent.GetThemeList(model.Filters, model.SortCollection, model.Page, model.RecordPerPage);
            var gridModel = FilterHelpers.GetDynamicGridModel(model, themeList.ThemeList, Convert.ToInt32(ListType.ThemeList));
            gridModel.TotalRecordCount = themeList.TotalResults;
            return ActionView(gridModel);
        }

        /// <summary>
        /// To show add new Theme
        /// </summary>
        /// <returns>ActionResult view of create theme</returns>   
        [HttpGet]
        public ActionResult Create()
        {
            return ActionView(MvcAdminConstants.CreateEditView, new ThemeViewModel());
        }

        /// <summary>
        /// To save theme details
        /// </summary>
        /// <param name="model">ThemeViewModel</param>
        /// <returns>returns theme view model to the view</returns>
        [HttpPost]
        public ActionResult Create(ThemeViewModel model)
        {
            if (!Equals(model, null))
            {
                string message = string.Empty;
                bool status = _manageThemeAgent.CreateTheme(model, out message);
                if (!status)
                {
                    TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(message, NotificationType.error);
                    return RedirectToAction(MvcAdminConstants.CreateView);
                }
                else
                {
                    TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success);
                    return RedirectToAction(MvcAdminConstants.ListView);
                }
            }
            return View(MvcAdminConstants.CreateEditView, model);
        }

        /// <summary>
        /// To Show edit page by theme id
        /// </summary>
        /// <param name="id">theme ID</param>
        ///<returns>ActionResult view of edit theme</returns>
        [HttpGet]
        public ActionResult Edit(int? id)
        {
            ThemeViewModel model = new ThemeViewModel();
            if (id > 0)
            {
                model = _manageThemeAgent.GetThemenById(id.Value);
            }
            return View(MvcAdminConstants.CreateEditView, model);
        }

        /// <summary>
        /// To update Theme data
        /// </summary>
        /// <param name="model">Theme view model</param>
        /// <returns>returns theme view model to the view</returns>
        [HttpPost]
        public ActionResult Edit(ThemeViewModel model)
        {
            if (!Equals(model, null))
            {
                string message = string.Empty;
                bool status = _manageThemeAgent.UpdateTheme(model, out message);
                if (!status)
                {
                    TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(message, NotificationType.error);
                    return RedirectToAction(MvcAdminConstants.EditAction, new { @id = model.ThemeId });
                }
                else
                {
                    TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success);
                    return RedirectToAction(MvcAdminConstants.ListView);
                }
            }
            return View(MvcAdminConstants.CreateEditView, model);
        }

        /// <summary>
        /// This action method delete the theme
        /// </summary>
        /// <param name="id">theme ID</param>
        /// <returns>returns Json result</returns>
        [HttpPost]
        public JsonResult Delete(int? id)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();
            if (!Equals(null, id))
            {
                status = _manageThemeAgent.DeleteTheme(id.Value, out message);
                message = status ? ZnodeResources.DeleteMessage : message;
            }
            return Json(new { sucess = status, message = message, id = id, isFadeOut = isFadeOut });
        }
    }
}