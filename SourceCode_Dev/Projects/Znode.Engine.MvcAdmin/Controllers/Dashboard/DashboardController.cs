﻿using System;
using System.Web.Mvc;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Controllers.Dashboard
{
    /// <summary>
    /// This controller will encapsulate all the methods necessary for the dashboard page
    /// </summary>
    public class DashboardController : BaseController
    {
        #region Private Variables
        private readonly IDashboardAgent _dashboardAgent;
        #endregion

        #region Constructor
        /// <summary>
        /// Default Constructor
        /// </summary>
        public DashboardController()
        {
            _dashboardAgent = new DashboardAgent();
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Show the Dashboard view
        /// </summary>
        /// <returns>ActionResult to display view</returns>
        public ActionResult Dashboard()
        {
            DashboardViewModel model = new DashboardViewModel();
            if (PortalId.HasValue)
            {
                model = _dashboardAgent.GetDashboardItemsByPortalId(PortalId.Value);
            }
            return View(model);
        } 
        #endregion
    }
}
