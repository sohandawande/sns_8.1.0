﻿using Resources;
using System;
using System.Net;
using System.Web.Mvc;
using Znode.Engine.Api.Models.Enum;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Controllers.Marketing
{
    public class EmailTemplateController : BaseController
    {
        #region Private Variables
        private readonly IEmailTemplateAgent _emailTemplateAgent;
        private const string PreviewPartialView = "_Preview";
        #endregion

        #region Public Constructors

        /// <summary>
        /// Constructor for the Email Template
        /// </summary>
        public EmailTemplateController()
        {
            _emailTemplateAgent = new EmailTemplateAgent();
        }

        #endregion

        #region Public methods
        // GET: EmailTemplate
        /// <summary>
        /// This function will return the list of all the templates.
        /// </summary>
        /// <returns>ActionResult view of template list</returns>
        public ActionResult List([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model)
        {
            EmailTemplateListViewModel list = _emailTemplateAgent.GetTemplateList(model.Filters, model.SortCollection, model.Page, model.RecordPerPage);
            ReportModel gridModel = FilterHelpers.GetDynamicGridModel(model, list.EmailTemplateList, Convert.ToInt32(ListType.EmailTemplateList));
            gridModel.TotalRecordCount = list.TotalResults;
            return ActionView(gridModel);
        }

        /// <summary>
        /// This method will use to add Email Template
        /// </summary>
        /// <returns>View of create page.</returns>
        public ActionResult Create()
        {
            EmailTemplateViewModel model = new EmailTemplateViewModel();
            model.DeletedTemplateNames = _emailTemplateAgent.GetAllDeletedTemplates();
            if (Equals(model.DeletedTemplateNames, null) || !(model.DeletedTemplateNames.Count > 0))
            {
                model.IsNewAdd = true;
            }
            return View(MvcAdminConstants.CreateEditView, model);
        }

        /// <summary>
        /// This method will use to add Email Template
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(EmailTemplateViewModel model)
        {
            if (!model.IsNewAdd)
            {
                ModelState.Remove(MvcAdminConstants.TemplateName);
            }
            ModelState.Remove(MvcAdminConstants.Html);
            ModelState.Remove(MvcAdminConstants.HtmlStringForm);
            if (ModelState.IsValid)
            {
                model.Html = WebUtility.HtmlDecode(model.HtmlStringForm);

                string errorMessage = string.Empty;
                bool status = false;
                status = _emailTemplateAgent.AddEmailTemplate(model, out errorMessage);
                TempData[MvcAdminConstants.Notifications] = status ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success) : GenerateNotificationMessages(errorMessage, NotificationType.error);
                model.DeletedTemplateNames = _emailTemplateAgent.GetAllDeletedTemplates();

                if (status)
                {
                    return RedirectToAction(MvcAdminConstants.ListView);
                }
                else
                {
                    model.Html = WebUtility.HtmlEncode(model.Html);
                    return View(MvcAdminConstants.CreateEditView, model);
                }
            }
            return View(MvcAdminConstants.CreateEditView, model);
        }

        /// <summary>
        /// Edit existing template page.
        /// </summary>
        /// <param name="id">Id of the template page.</param>
        /// <returns>View of edit page.</returns>
        [HttpGet]
        public ActionResult Edit(string name, string extension)
        {
            if (!string.IsNullOrEmpty(name))
            {
                EmailTemplateViewModel model = _emailTemplateAgent.GetTemplatePage(name, extension);
                if (!Equals(model, null))
                {
                    model.Html = WebUtility.HtmlEncode(model.Html);
                    model.IsEditMode = true;
                    return View(MvcAdminConstants.CreateEditView, model);
                }

                return View(MvcAdminConstants.CreateEditView, new EmailTemplateViewModel() { IsEditMode = true });
            }
            return RedirectToAction(MvcAdminConstants.ListView);
        }

        /// <summary>
        ///  Edit Existing Template Page
        /// </summary>
        /// <param name="model">EmailTemplateViewModel model</param>
        /// <returns>View of List page.</returns>
        [HttpPost]
        public ActionResult Edit(EmailTemplateViewModel model)
        {
            ModelState.Remove(MvcAdminConstants.Html);
            ModelState.Remove(MvcAdminConstants.HtmlStringForm);
            if (ModelState.IsValid)
            {
                string errorMessage = string.Empty;
                bool status = false;
                model.Html = WebUtility.HtmlDecode(model.HtmlStringForm);

                status = _emailTemplateAgent.UpdateTemplatePage(model, out errorMessage);
                TempData[MvcAdminConstants.Notifications] = status ? GenerateNotificationMessages(ZnodeResources.UpdateMessage, NotificationType.success) : string.IsNullOrEmpty(errorMessage) ? GenerateNotificationMessages(ZnodeResources.UpdateEmailTemplateFail, NotificationType.error) : GenerateNotificationMessages(errorMessage, NotificationType.error);
                if (!status)
                {
                    model.IsEditMode = true;
                    model.Html = WebUtility.HtmlEncode(model.Html);
                    return View(MvcAdminConstants.CreateEditView, model);
                }
            }

            return RedirectToAction(MvcAdminConstants.ListView);
        }


        /// <summary>
        /// Get html content of template by template name.
        /// </summary>
        /// <param name="templatename">name of template</param>
        /// <returns>categoryview model json result </returns>
        [HttpPost]
        public JsonResult GetHtmlContent(string templatename)
        {
            EmailTemplateViewModel model = _emailTemplateAgent.GetHtmlContent(templatename);
            model.Html = WebUtility.HtmlEncode(model.Html);
            return Json(model.Html, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Delete existing Template page
        /// </summary>
        /// <param name="name">name of template</param>
        /// <param name="extension">extension of template</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Delete(string name, string extension)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();

            if (!string.IsNullOrEmpty(name))
            {
                status = _emailTemplateAgent.DeleteTemplatePage(name, extension, out message);
                message = status ? ZnodeResources.DeleteMessage : string.IsNullOrEmpty(message) ? ZnodeResources.DeleteEmailTemplateFail : message;
            }

            return Json(new { sucess = status, message = message, id = name, isFadeOut = isFadeOut });
        }


        /// <summary>
        /// Edit an existing Reason For Return
        /// </summary>
        /// <param name="reasonForReturnId">reasonForReturnId to get single ReasonForReturn Detail</param>
        /// <returns>Action View</returns>
        [HttpPost]
        public JsonResult PreviewTemplate(string templateName)
        {
            EmailTemplateViewModel model = _emailTemplateAgent.GetHtmlContent(templateName);

            return Json(new
            {
                success = true,
                data = new
                {
                    html = Equals(model, null) ? string.Empty : RenderHelper.PartialView(this, PreviewPartialView, model),
                }
            }, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}