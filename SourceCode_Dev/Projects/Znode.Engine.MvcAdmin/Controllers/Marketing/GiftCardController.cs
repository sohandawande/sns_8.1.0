﻿using Resources;
using System;
using System.Web.Mvc;
using Znode.Engine.Api.Models.Enum;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.Agents.Agents;
using Znode.Engine.MvcAdmin.Agents.IAgents;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Controllers.Marketing
{

    public class GiftCardController : BaseController
    {
        #region Private Variables
        private readonly IGiftCardAgent _giftCardAgent;
        private readonly IRMARequestAgent _rmaRequestAgent;

        private string createEditView = MvcAdminConstants.CreateEditView;
        private string GiftCardListAction = MvcAdminConstants.ListView;

        private string AccountId = MvcAdminConstants.AccountId;

        private const string RmaMode = "rma";
        private const string rmaController = "rmamanager";
        private const string rmaListAction = "rmalist";

        #endregion

        #region Constructor
        public GiftCardController()
        {
            _giftCardAgent = new GiftCardAgent();
            _rmaRequestAgent = new RMARequestAgent();
        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Gets the main page of the GiftCards.
        /// </summary>
        /// <returns>Returns the object of GiftCardListViewModel.</returns>
        public ActionResult List([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model)
        {
            GiftCardViewModel giftCardViewModel = new GiftCardViewModel();

            var expired = _giftCardAgent.GetExpirationStatus();
            GiftCardListViewModel GiftCards = _giftCardAgent.GetGiftCards(model.Filters, model.SortCollection, model.Page, model.RecordPerPage);
            var gridModel = FilterHelpers.GetDynamicGridModel(model, GiftCards.GiftCards, Convert.ToInt32(ListType.GiftCardList));
            FilterHelpers.CreateDropdown(ref gridModel, expired, model.Filters, MvcAdminConstants.ExpirationDate, MvcAdminConstants.ExcludeText, MvcAdminConstants.ExcludeExpired);

            gridModel.FilterColumn.DropdownList[0].Dropdown[0].Value = "0";

            gridModel.TotalRecordCount = GiftCards.TotalResults;
            return ActionView(gridModel);
        }
        /// <summary>
        /// Delete GiftCard
        /// </summary>
        /// <param name="id"> int id</param>
        /// <returns>Returns to the GiftCard list view</returns>
        [HttpPost]
        public JsonResult Delete(int id)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();
            if (!Equals(null, id))
            {
                status = _giftCardAgent.DeleteGiftCard(id);
                message = status ? ZnodeResources.DeleteMessage : ZnodeResources.DeleteErrorMessage;
            }
            return Json(new { sucess = status, message = message, id = id, isFadeOut = isFadeOut });
        }

        /// <summary>
        /// Create GiftCard
        /// </summary>
        /// <returns>Returns view of create-edit for GiftCard</returns>
        [HttpGet]
        public ActionResult Create(string mode = "", int item = 0, string items = "", string qty = "", int orderid = 0, string flag = "", decimal amount = 0.0m)
        {
            GiftCardViewModel model = new GiftCardViewModel();
            if (mode.ToLower().Equals(RmaMode))
            {
                _giftCardAgent.SetRMAData(mode, item, items, qty, amount, model);
            }
            model.PortalList = _giftCardAgent.GetAllPortals().PortalList;
            model.CardNumber = _giftCardAgent.GetNextGiftCardNumber().CardNumber;
            model.OrderId = item;
            return ActionView(createEditView, model);
        }
      
        /// <summary>
        /// Create GiftCard
        /// </summary>
        /// <param name="model">The model of type GiftCardViewModel.</param>
        /// <returns>Returns the object of GiftCardViewModel.</returns>
        [HttpPost]
        public ActionResult Create(GiftCardViewModel model)
        {
            string message = string.Empty;
            if (ModelState.IsValid)
            {
                if (!Equals(model, null) && (!Equals(model.AccountId, null)))
                {
                    int accountId = Convert.ToInt32(model.AccountId);
                    if (_giftCardAgent.IsAccountActive(accountId))
                    {
                        GiftCardViewModel giftCard = _giftCardAgent.Create(model);
                        model.GiftCardId = giftCard.GiftCardId;

                        if (!Equals(giftCard, null) && giftCard.GiftCardId > 0)
                        {
                            if (!Equals(model.Mode, null) && (model.Mode.Equals(RmaMode, StringComparison.InvariantCultureIgnoreCase)))
                            {
                               if(_giftCardAgent.UpdateRMA(model,out message))
                               {
                                   TempData[MvcAdminConstants.Notifications] = (string.IsNullOrEmpty(message)) ? GenerateNotificationMessages(string.Empty, NotificationType.success) : GenerateNotificationMessages(message, NotificationType.error);
                                   return RedirectToAction(rmaListAction, rmaController);
                               }
                            }
                        }
                        TempData[MvcAdminConstants.Notifications] = (!Equals(giftCard, null) && giftCard.GiftCardId > 0) ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.SaveErrorMessage, NotificationType.error);
                        return RedirectToAction(GiftCardListAction);
                    }
                    else
                    {
                        ModelState.AddModelError(AccountId, ZnodeResources.ValidAccountId);
                    }
                }
                else
                {
                    GiftCardViewModel giftCard = _giftCardAgent.Create(model);
                    TempData[MvcAdminConstants.Notifications] = (!Equals(giftCard, null) && giftCard.GiftCardId > 0) ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.SaveErrorMessage, NotificationType.error);
                    return RedirectToAction(GiftCardListAction);
                }
            }
            model.PortalList = _giftCardAgent.GetAllPortals().PortalList;
            return View(createEditView, model);
        }


        /// <summary>
        /// Edit GiftCard
        /// </summary>
        /// <param name="giftCardId">giftCardId os selected GiftCard</param>
        /// <returns>Returns view of create-edit for GiftCard and object of GiftCardViewModel</returns>
        public ActionResult Edit(int id)
        {
            if (id > 0)
            {
                GiftCardViewModel giftCard = new GiftCardViewModel();
                giftCard = _giftCardAgent.GetGiftCard(id);
                giftCard.EnableToCustomerAccount = !Equals(giftCard.AccountId, null) ? true : false;
                giftCard.PortalList = _giftCardAgent.GetAllPortals().PortalList;
                return View(createEditView, giftCard);
            }
            return RedirectToAction(GiftCardListAction);
        }

        /// <summary>
        /// Edit GiftCard
        /// </summary>
        /// <param name="model">The model of type GiftCardViewModel</param>
        /// <returns>Returns the GiftCards List</returns>
        [HttpPost]
        public ActionResult Edit(GiftCardViewModel model)
        {
            if (ModelState.IsValid)
            {
                if ((!Equals(model, null) && (model.AccountId > 0)))
                {
                    int accountId = Convert.ToInt32(model.AccountId);
                    if (_giftCardAgent.IsAccountActive(accountId))
                    {
                        TempData[MvcAdminConstants.Notifications] = _giftCardAgent.Update(model) ? GenerateNotificationMessages(ZnodeResources.UpdateMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.UpdateErrorMessage, NotificationType.error);
                        return RedirectToAction(GiftCardListAction);
                    }
                    else
                    {
                        ModelState.AddModelError(AccountId, ZnodeResources.ValidAccountId);
                        model.PortalList = _giftCardAgent.GetAllPortals().PortalList;
                        return View(createEditView, model);
                    }
                }
                else
                {
                    TempData[MvcAdminConstants.Notifications] = _giftCardAgent.Update(model) ? GenerateNotificationMessages(ZnodeResources.UpdateMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.UpdateErrorMessage, NotificationType.error);
                    return RedirectToAction(GiftCardListAction);
                }
            }
            model.PortalList = _giftCardAgent.GetAllPortals().PortalList;
            return View(createEditView, model);
        }

        #endregion
    }
}