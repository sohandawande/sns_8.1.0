﻿using Resources;
using System;
using System.Web.Mvc;
using Znode.Engine.Api.Models.Enum;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Controllers
{
    public class PromotionController : BaseController
    {
        #region Private Variables
        private readonly IPromotionAgent _promotionAgent;
        private readonly IProductAgent _productAgent;
        private readonly IPortalAgent _portalAgent;
        private readonly IProviderEngineAgent _providerEngineAgent;
        private readonly IManufacturersAgent _manufacturersAgent;
        private string createEditView = MvcAdminConstants.CreateEditView;
        private string promotionListAction = MvcAdminConstants.ListView;
        private string productListPartialView = "_ProductList";
        private const string promotionModelSession = "PromotionModel";
        private const string couponView = "_Coupons";
        private const string enableCouponView = "_EnableCouponUrl";
        private const string callForPriceView = "_CallForPricing";
        #endregion

        #region Public Constructor
        /// <summary>
        /// Constructor for Promotion
        /// </summary>
        public PromotionController()
        {
            _promotionAgent = new PromotionAgent();
            _productAgent = new ProductAgent();
            _portalAgent = new PortalAgent();
            _manufacturersAgent = new ManufacturersAgent();
            _providerEngineAgent = new ProviderEngineAgent();
        }
        #endregion

        #region Action Methods
        /// <summary>
        /// This function will return the list of all the Promotions.
        /// </summary>
        /// <returns>ActionResult view of promotion list</returns>
        public ActionResult List([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model)
        {
            PromotionsListViewModel viewModel = new PromotionsListViewModel();
            var promotionTypes = _promotionAgent.GetPromotionModelTypeList(string.Empty, true);

            viewModel = _promotionAgent.GetPromotions(model.Filters, model.SortCollection, model.Page, model.RecordPerPage);
            var gridModel = FilterHelpers.GetDynamicGridModel(model, viewModel.Promotions, Convert.ToInt32(ListType.PromotionList));
            gridModel.TotalRecordCount = viewModel.TotalResults;
            FilterHelpers.CreateDropdown(ref gridModel, promotionTypes, model.Filters, MvcAdminConstants.LabelPromotionTypeId, MvcAdminConstants.LabelName, MvcAdminConstants.LabelDiscountType);
            return ActionView(gridModel);
        }

        /// <summary>
        /// To show add new Promotion page
        /// </summary>
        /// <returns></returns>   
        public ActionResult Create()
        {
            PromotionsViewModel promotion = new PromotionsViewModel();

            promotion = _promotionAgent.GetPromotionInformation(promotion);
            if (Equals(promotion.PromotionTypeList.Count, 0))
            {
                TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.AddPromotionErrorMessage, NotificationType.error);
                return RedirectToAction(MvcAdminConstants.ListView);
            }
            promotion.StartDate = HelperMethods.ViewDateFormat(DateTime.Now);
            promotion.EndDate = HelperMethods.ViewDateFormat(DateTime.Now.AddDays(30));
            Session[promotionModelSession] = null;
            return View(createEditView, promotion);
        }

        /// <summary>
        /// To save Promotion details
        /// </summary>
        /// <param name="model">PromotionviewModel</param>
        /// <returns>returns promotion view model to the view</returns>
        [HttpPost]
        public ActionResult Create(PromotionsViewModel model)
        {
            if (!Equals(model, null))
            {
                string message = string.Empty;
                var status = _promotionAgent.SavePromotion(model, out message);
                TempData[MvcAdminConstants.Notifications] = status ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success) : GenerateNotificationMessages(message, NotificationType.error);
                if (!status)
                {
                    return RedirectToAction(MvcAdminConstants.CreateView, new { @id = model.PromotionId });
                }
                else
                {
                    return RedirectToAction(promotionListAction, new { @id = model.PromotionId });
                }

            }
            return View(createEditView, model);
        }

        /// <summary>
        /// Get the list of Products
        /// </summary>
        /// <param name="model">FilterCollectionDataModel</param>
        /// <returns>List of Products</returns>
        public ActionResult GetProductList([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model)
        {
            ProductListViewModel products = new ProductListViewModel();
            var categories = _promotionAgent.GetCategoryModelList();
            var manufacturers = _promotionAgent.GetManufacturerModelList();
            products = _promotionAgent.GetProducts(model.Filters, model.SortCollection, model.Page, model.RecordPerPage);

            var gridModel = FilterHelpers.GetDynamicGridModel(model, products.Products, Convert.ToInt32(ListType.PromotionProductList));
            gridModel.TotalRecordCount = products.TotalResults;
            FilterHelpers.CreateDropdown(ref gridModel, categories, model.Filters, MvcAdminConstants.LabelCategoryId, MvcAdminConstants.LabelName, MvcAdminConstants.LabelCategory);
            FilterHelpers.CreateDropdown(ref gridModel, manufacturers, model.Filters, MvcAdminConstants.LabelManufacturerId, MvcAdminConstants.LabelName, MvcAdminConstants.LabelManufacturer);
            return ActionView(productListPartialView, gridModel);
        }

        /// <summary>
        /// Display promotion form on the basis of promotionType
        /// </summary>
        /// <param name="promotionType">Selected Text Value of PromotionType from dropdown</param>
        /// <returns>Returns partial view</returns>
        public ActionResult GetPromotionTypeForm(int promotionType)
        {
            PromotionsViewModel model = new PromotionsViewModel();

            if (!Equals(Session[promotionModelSession], null))
            {
                model = (PromotionsViewModel)Session[promotionModelSession];
            }
            else
            {
                model = _promotionAgent.GetPromotionInformation(model);
            }

            string promotionTypeClassName = _providerEngineAgent.GetPromotionType(promotionType).ClassName;

            switch (promotionTypeClassName)
            {
                case MvcAdminConstants.AmountOfBrand:
                    return PartialView(MvcAdminConstants.AmountOfBrandView, model);
                case MvcAdminConstants.AmountOfCatalog:
                    return PartialView(MvcAdminConstants.AmountOfCatalogView, model);
                case MvcAdminConstants.AmountOfCategory:
                    return PartialView(MvcAdminConstants.AmountOfCategoryView, model);
                case MvcAdminConstants.AmountOfDisplayProductPrice:
                    return PartialView(MvcAdminConstants.AmountOfDisplayProductPriceView, model);
                case MvcAdminConstants.AmountOfOrder:
                    return PartialView(MvcAdminConstants.AmountOfOrderView, model);
                case MvcAdminConstants.AmountOfProduct:
                    return PartialView(MvcAdminConstants.AmountOfProductView, model);
                case MvcAdminConstants.AmountOfShipping:
                    return PartialView(MvcAdminConstants.AmountOfShippingView, model);
                case MvcAdminConstants.AmountOfXIfYPurchased:
                    return PartialView(MvcAdminConstants.AmountOfXIfYPurchasedView, model);
                case MvcAdminConstants.CallForPricing:
                    return PartialView(MvcAdminConstants.CallForPricingView, model);
                case MvcAdminConstants.PercentOfBrand:
                    return PartialView(MvcAdminConstants.PercentOfBrandView, model);
                case MvcAdminConstants.PercentOfCatalog:
                    return PartialView(MvcAdminConstants.PercentOfCatalogView, model);
                case MvcAdminConstants.PercentOfCategory:
                    return PartialView(MvcAdminConstants.PercentOfCategoryView, model);
                case MvcAdminConstants.PercentOfDisplayProductPrice:
                    return PartialView(MvcAdminConstants.PercentOfDisplayProductPriceView, model);
                case MvcAdminConstants.PercentOfOrder:
                    return PartialView(MvcAdminConstants.PercentOfOrderView, model);
                case MvcAdminConstants.PercentOfProduct:
                    return PartialView(MvcAdminConstants.PercentOfProductView, model);
                case MvcAdminConstants.PercentOfShipping:
                    return PartialView(MvcAdminConstants.PercentOfShippingView, model);
                case MvcAdminConstants.PercentOfXIfYPurchased:
                    return PartialView(MvcAdminConstants.PercentOfXIfYPurchasedView, model);
                case MvcAdminConstants.ProductDetails:
                    return PartialView(MvcAdminConstants.ProductDetailsView, model);
            }
            return PartialView(MvcAdminConstants.ProductDetailsView, model);
        }

        /// <summary>
        /// Display Coupon code form 
        /// </summary>
        /// <returns>Returns partial view</returns>
        public ActionResult GetCouponCodeForm()
        {
            PromotionsViewModel model = new PromotionsViewModel();
            if (!Equals(Session[promotionModelSession], null))
            {
                model = (PromotionsViewModel)Session[promotionModelSession];
            }

            return PartialView(couponView, model);
        }

        /// <summary>
        /// Display Coupon Url form 
        /// </summary>
        /// <returns>Returns partial view</returns>
        public ActionResult GetEnableUrlForm()
        {
            PromotionsViewModel model = new PromotionsViewModel();
            if (!Equals(Session[promotionModelSession], null))
            {
                model = (PromotionsViewModel)Session[promotionModelSession];
            }
            return PartialView(enableCouponView, model);
        }

        /// <summary>
        /// Get the profileList on the basis storeId
        /// </summary>
        /// <param name="storeId">storeId to retrieve ProfileList</param>
        /// <returns>Json Result</returns>
        [HttpGet]
        public JsonResult GetProfileListByPortalId(int storeId)
        {
            PromotionsViewModel model = new PromotionsViewModel();
            var list = _promotionAgent.GetProfileList(storeId);
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get the CatalogList on the basis storeId
        /// </summary>
        /// <param name="storeId">storeId to retrieve CatalogList</param>
        /// <returns>Json Result</returns>
        [HttpGet]
        public JsonResult GetCatalogListByPortalId(int storeId)
        {
            var list = _promotionAgent.GetCatalogList(storeId);
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get the CategoryList on the basis storeId
        /// </summary>
        /// <param name="storeId">storeId to retrieve CategoryList</param>
        /// <returns>Json Result</returns>
        [HttpGet]
        public JsonResult GetCategoryListByPortalId(int storeId)
        {
            var list = _promotionAgent.GetCategoryList(storeId);
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get the list of Skus on the basis of productId
        /// </summary>
        /// <param name="productId">productId to retrieve Sku List</param>
        /// <returns>Partial View</returns>

        public ActionResult GetSkuListByProductId(int productId)
        {
            PromotionsViewModel model = new PromotionsViewModel();
            if (!Equals(Session[promotionModelSession], null))
                model = (PromotionsViewModel)Session[promotionModelSession];
            model.SkuList = _promotionAgent.GetSkuList(productId);
            return PartialView(callForPriceView, model);
        }

        /// <summary>
        /// To Show edit page by id
        /// </summary>
        /// <returns>edit promotion</returns>
        [HttpGet]
        public ActionResult Edit(int? id)
        {
            PromotionsViewModel promotion = new PromotionsViewModel();
            promotion = _promotionAgent.GetPromotionById(id.Value);
            promotion = _promotionAgent.GetPromotionInformation(promotion, true);
            if (Equals(promotion.PortalId, null))
            {
                promotion.PortalId = 0;
            }
            promotion.ProfileList = _promotionAgent.GetProfileList(promotion.PortalId.Value);
            promotion.CatalogList = _promotionAgent.GetCatalogList(promotion.PortalId.Value);
            promotion.CategoryList = _promotionAgent.GetCategoryList(promotion.PortalId.Value);
            if (promotion.RequiredProductId > 0)
                promotion.RequireProduct = _productAgent.GetProduct(promotion.RequiredProductId.Value).Name;
            if (promotion.DiscountedProductId > 0)
                promotion.DiscountProduct = _productAgent.GetProduct(promotion.DiscountedProductId.Value).Name;
            if (promotion.RequiredProductId > 0)
            {
                promotion.CallForPriceProduct = _productAgent.GetProduct(promotion.RequiredProductId.Value).Name;
                promotion.SkuList = _promotionAgent.GetSkuList(promotion.RequiredProductId.Value);
            }

            Session[promotionModelSession] = promotion;
            return View(createEditView, promotion);
        }

        /// <summary>
        /// To update Promotion data
        /// </summary>
        /// <param name="model">Promotion view Model</param>
        /// <returns>returns promotion view model to the view</returns>
        [HttpPost]
        public ActionResult Edit(PromotionsViewModel model)
        {
            if (!Equals(model, null))
            {
                string message = string.Empty;
                var status = _promotionAgent.UpdatePromotion(model, out message);
                TempData[MvcAdminConstants.Notifications] = status ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success) : GenerateNotificationMessages(message, NotificationType.error);
                if (!status)
                {
                    return RedirectToAction(MvcAdminConstants.EditAction, new { @id = model.PromotionId });
                }
                else
                {
                    return RedirectToAction(promotionListAction, new { @id = model.PromotionId });
                }
            }
            return View(createEditView, model);
        }

        /// <summary>
        /// This action method delete the promotion
        /// </summary>
        /// <param name="keyword">integer Promotion Id</param>
        /// <returns>Returns partial view of promotions list</returns>
        [HttpPost]
        public JsonResult Delete(int? id)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();
            if (!Equals(null, id))
            {
                status = _promotionAgent.DeletePromotion(id.Value);
                message = status ? ZnodeResources.DeleteMessage : ZnodeResources.DeleteErrorMessage;
            }
            return Json(new { sucess = status, message = message, id = id, isFadeOut = isFadeOut });
        }

        /// <summary>
        /// Display Multiple coupon check box. 
        /// </summary>
        /// <returns>Returns true or false value for multiple coupon check box display.</returns>
        public JsonResult GetMultipleCouponCheckboxEnabled(string selectedPortalId)
        {
            int portalId = !string.IsNullOrEmpty(selectedPortalId) ? Convert.ToInt32(selectedPortalId) : 0;
            return Json(new
            {
                enabled = portalId > 0 ? _portalAgent.GetPortalByPortalId(portalId).IsMultipleCouponCodeAllowed : false
            }, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}