﻿using Resources;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Mvc;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Controllers.Marketing
{
    public class AffiliateTrackingController : BaseController
    {
        #region Private Variables
        private readonly IAffiliateTrackingAgent _affiliateTrackingAgent;
        private string affiliateTrackingView = "AffiliateTracking";
        #endregion

        #region Constructor
        public AffiliateTrackingController()
        {
            _affiliateTrackingAgent = new AffiliateTrackingAgent();
        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Gets the tracking data.
        /// </summary>
        /// <returns>Returns view.</returns>
        public ActionResult GetTrackingData()
        {
            AffiliateTrackingViewModel model = new AffiliateTrackingViewModel();
            model.FileTypes = _affiliateTrackingAgent.FileTypeList();
            return View(affiliateTrackingView, model);
        }

        /// <summary>
        /// Gets the tracking data on the basis of dates provided and performs download.
        /// </summary>
        /// <param name="model">Model of type AffiliateTrackingViewModel.</param>
        /// <returns>Returns either downloaded file or error message.</returns>
        [HttpPost]
        public ActionResult GetTrackingData(AffiliateTrackingViewModel model)
        {
            if (!Equals(model, null))
            {
                DataSet trackingData = _affiliateTrackingAgent.GetAffiliateTrackingData(Convert.ToDateTime(model.StartDate).ToShortDateString(), Convert.ToDateTime(model.EndDate).ToShortDateString());
                if (Equals(trackingData, null) || Equals(trackingData.Tables, null) || Equals(trackingData.Tables[0], null) || trackingData.Tables[0].Rows.Count <= 0)
                {
                    return ReturnAffiliateErrorView();
                }
                //below code downloads the file.
                _affiliateTrackingAgent.DownloadFile(model, trackingData, Response);
                return View(model);
            }
            return View(new AffiliateTrackingViewModel() { FileTypes = new List<SelectListItem>() });
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Returns affiliate view with error message.
        /// </summary>
        /// <returns>Returns affiliate view with error message.</returns>
        private ActionResult ReturnAffiliateErrorView()
        {
            TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.NoRecordFoundMessageText, NotificationType.error);
            AffiliateTrackingViewModel returnModel = new AffiliateTrackingViewModel();
            returnModel.FileTypes = _affiliateTrackingAgent.FileTypeList();
            return View(affiliateTrackingView, returnModel);
        }

        #endregion

    }
}