﻿using Newtonsoft.Json;
using System;
using System.Web.Mvc;
using Znode.Engine.Api.Models.Enum;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.ViewModels;
namespace Znode.Engine.MvcAdmin.Controllers.Marketing
{
    public class ProductSearchSettingsController : BaseController
    {
        #region Private Variables
        private readonly IProductSearchSettingAgent _productSearchSettingAgent;
        private string ListProductLevelSettings = "_ProductLevelSearchSettings";
        private string ListCategoryLevelSettings = "_CategoryLevelSearchSettings";
        private string ListFieldLevelSettings = "_FieldLevelSearchSettings";
        #endregion

        #region Constructor
        public ProductSearchSettingsController()
        {
            _productSearchSettingAgent = new ProductSearchSettingAgent();
        }
        #endregion

        #region Public Methods

        public ActionResult ManageProductSearchSetting()
        {
            return ActionView();
        }


        /// <summary>
        /// Get all product level setting
        /// </summary>
        /// <param name="model">FilterCollectionDataModel model</param>
        /// <returns></returns>
        public ActionResult GetProductLevelSetting([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model)
        {
            var catalogs = _productSearchSettingAgent.GetCatalogList();
            var manufacturers = _productSearchSettingAgent.GetManufacturerList();
            var categories = _productSearchSettingAgent.GetCategoryList();
            var productTypes = _productSearchSettingAgent.GetProductTypeList();
            ProductLevelSettingListViewModel productList = _productSearchSettingAgent.GetProductLevelSettings(model.Filters, model.SortCollection, model.Page, model.RecordPerPage);
            var gridModel = FilterHelpers.GetDynamicGridModel(model, productList.Products, Convert.ToInt32(ListType.vw_ZNodeGetProductWithProductBoost));
            FilterHelpers.CreateDropdown(ref gridModel, catalogs, model.Filters, MvcAdminConstants.LabelCatalogId, MvcAdminConstants.LabelName, MvcAdminConstants.LabelCatalog);
            FilterHelpers.CreateDropdown(ref gridModel, manufacturers, model.Filters, MvcAdminConstants.ManufacturerId, MvcAdminConstants.LabelName, MvcAdminConstants.Brand);
            FilterHelpers.CreateDropdown(ref gridModel, productTypes, model.Filters, MvcAdminConstants.productTypeId, MvcAdminConstants.LabelName, MvcAdminConstants.ProductTypeDropdownName);
            FilterHelpers.CreateDropdown(ref gridModel, categories, model.Filters, MvcAdminConstants.LabelCategoryId, MvcAdminConstants.LabelName, MvcAdminConstants.ProductCategoryDropdownName);
            gridModel.TotalRecordCount = productList.TotalResults;
            return ActionView(ListProductLevelSettings, gridModel);
        }

        /// <summary>
        /// Get all category level setting
        /// </summary>
        /// <param name="model">FilterCollectionDataModel model</param>
        /// <returns></returns>
        public ActionResult GetCategoryLevelSetting([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model)
        {
            var catalogs = _productSearchSettingAgent.GetCatalogList();
            var manufacturers = _productSearchSettingAgent.GetManufacturerList();
            var categories = _productSearchSettingAgent.GetCategoryList();
            var productTypes = _productSearchSettingAgent.GetProductTypeList();
            CategoryLevelSettingListViewModel productList = _productSearchSettingAgent.GetCategoryLevelSettings(model.Filters, model.SortCollection, model.Page, model.RecordPerPage);
            var gridModel = FilterHelpers.GetDynamicGridModel(model, productList.Category, Convert.ToInt32(ListType.ProductLevelSettingList));
            FilterHelpers.CreateDropdown(ref gridModel, catalogs, model.Filters, MvcAdminConstants.LabelCatalogId, MvcAdminConstants.LabelName, MvcAdminConstants.LabelCatalog);
            FilterHelpers.CreateDropdown(ref gridModel, manufacturers, model.Filters, MvcAdminConstants.ManufacturerId, MvcAdminConstants.LabelName, MvcAdminConstants.Brand);
            FilterHelpers.CreateDropdown(ref gridModel, productTypes, model.Filters, MvcAdminConstants.productTypeId, MvcAdminConstants.LabelName, MvcAdminConstants.ProductTypeDropdownName);
            FilterHelpers.CreateDropdown(ref gridModel, categories, model.Filters, MvcAdminConstants.LabelCategoryId, MvcAdminConstants.LabelName, MvcAdminConstants.ProductCategoryDropdownName);
            gridModel.TotalRecordCount = productList.TotalResults;
            return ActionView(ListCategoryLevelSettings, gridModel);
        }

        /// <summary>
        /// Get all field level setting
        /// </summary>
        /// <param name="model">FilterCollectionDataModel model</param>
        /// <returns>FilterCollectionDataModel model</returns>
        public ActionResult GetFieldLevelSetting([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model)
        {

            FieldLevelSettingListViewModel fieldList = _productSearchSettingAgent.GetFieldLevelSettings(model.Filters, model.SortCollection, model.Page, model.RecordPerPage);
            var gridModel = FilterHelpers.GetDynamicGridModel(model, fieldList.Fields, Convert.ToInt32(ListType.FieldLevelSettingList));
            gridModel.TotalRecordCount = fieldList.TotalResults;
            return ActionView(ListFieldLevelSettings, gridModel);
        }

        /// <summary>
        /// Save Product search boost values.
        /// </summary>
        /// <param name="ids">Product/Categories/Filds ids</param>
        /// <param name="values">Product/Categories/Filds Boost values</param>
        /// <param name="name">Boost Name</param>
        /// <returns>Returns success</returns>
        public JsonResult SaveProductBoostValues(string ids, string values, string name)
        {
            string[] _ids = JsonConvert.DeserializeObject<string[]>(ids);
            string[] _values = JsonConvert.DeserializeObject<string[]>(values);

            bool result = _productSearchSettingAgent.SaveProductBoostValues(_ids, _values, name);
            
            return Json(new { status = "success", responce = result.Equals(true) ? name : "false" }, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}