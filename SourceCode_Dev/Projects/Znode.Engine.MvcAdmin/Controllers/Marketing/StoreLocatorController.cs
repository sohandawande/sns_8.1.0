﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.Api.Models.Enum;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Controllers.Marketing
{
    /// <summary>
    /// Store Locator Controller
    /// </summary>
    public class StoreLocatorController : BaseController
    {
        #region Private Variables
        private readonly IStoreLocatorAgent _storeLocatorAgent;
        #endregion

        #region Public Constructors

        /// <summary>
        /// Constructor for the store locator.
        /// </summary>
        public StoreLocatorController()
        {
            _storeLocatorAgent = new StoreLocatorAgent();
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Gets the main page of the store.
        /// </summary>
        /// <returns>View of the store's main page.</returns>
        public ActionResult List([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model)
        {
            StoreLocatorListViewModel storeLocators = _storeLocatorAgent.GetStoreLocators(model.Filters, model.SortCollection, model.Page, model.RecordPerPage);

            var gridModel = FilterHelpers.GetDynamicGridModel(model, storeLocators.StoreLocators, Convert.ToInt32(ListType.StoreLocatorList));
            gridModel.TotalRecordCount = (storeLocators.TotalResults > 0) ? storeLocators.TotalResults : storeLocators.StoreLocators.Count;
            return ActionView(gridModel);
        }

        /// <summary>
        /// This method will use to add Store.
        /// </summary>
        /// <returns>View of create new store locator</returns>
        public ActionResult Create()
        {
            StoreLocatorViewModel storeLocator = new StoreLocatorViewModel();
            storeLocator.PortalList = _storeLocatorAgent.GetPortalsList();
            return ActionView(MvcAdminConstants.CreateEditView, storeLocator);
        }

        /// <summary>
        /// This method will use to add store.
        /// </summary>
        /// <returns>Create new store locator</returns>
        [HttpPost]
        public ActionResult Create(StoreLocatorViewModel model)
        {
            if (ModelState.IsValid)
            {
                _storeLocatorAgent.SetStoreLocatorProperty(model);
                TempData[MvcAdminConstants.Notifications] = _storeLocatorAgent.SaveStore(model) ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.SaveErrorMessage, NotificationType.error);
                return RedirectToAction(MvcAdminConstants.ListView);
            }
            model.PortalList = _storeLocatorAgent.GetPortalsList();
            return View(MvcAdminConstants.CreateEditView, model);
        }

        /// <summary>
        /// This method will populate the store data to be edited.
        /// </summary>
        /// <param name="id">integer Store ID</param>
        /// <returns>Returns the edit view of Store</returns>
        public ActionResult Manage(int? id)
        {
            StoreLocatorViewModel model = null;
            if (!Equals(id, null) && id > 0)
            {
                model = _storeLocatorAgent.GetStoreLocatorByID(int.Parse(id.ToString()));
                model.PortalList = _storeLocatorAgent.GetPortalsList();
                model.IsEditMode = true;
            }
            return ActionView(MvcAdminConstants.CreateEditView, model);
        }

        /// <summary>
        ///  Edit existing store locator.
        /// </summary>
        /// <param name="model">StoreLocatorViewModel model</param>
        /// <returns>Update store locator data.</returns>
        [HttpPost]
        public ActionResult Manage(StoreLocatorViewModel model)
        {
            if (ModelState.IsValid)
            {
                _storeLocatorAgent.SetStoreLocatorProperty(model);
                TempData[MvcAdminConstants.Notifications] = _storeLocatorAgent.UpdateStoreLocator(model) ? GenerateNotificationMessages(ZnodeResources.UpdateMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.UpdateErrorMessage, NotificationType.error);
                return RedirectToAction(MvcAdminConstants.ListView);
            }
            model.PortalList = _storeLocatorAgent.GetPortalsList();
            return ActionView(MvcAdminConstants.CreateEditView, model);
        }

        /// <summary>
        /// Delete an existing store locator.
        /// </summary>
        /// <param name="id">nullable int store locator id</param>
        /// <returns>Returns json and Success/Fail Message</returns>
        public JsonResult Delete(int? id)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();

            if (!Equals(id, null))
            {
                status = _storeLocatorAgent.DeleteStoreLocator(id.Value);
                message = status ? ZnodeResources.DeleteMessage : ZnodeResources.DeleteErrorMessage;
            }
            return Json(new { sucess = status, message = message, id = id, isFadeOut = isFadeOut });
        } 
        #endregion
    }
}