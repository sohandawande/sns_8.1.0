﻿using Resources;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Enum;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Models;
using Znode.Engine.MvcAdmin.ViewModels;
using Znode.Engine.Api.Models;

namespace Znode.Engine.MvcAdmin.Controllers.Marketing
{
    public class PersonalizationController : BaseController
    {
        #region Private Variables
        private readonly IPersonalizationAgent _personalisationAgent;
        private readonly ICategoryAgent _categoryAgent;
        private readonly IProductAgent _productAgent;
        // private string searchYouMayLike = MvcAdminConstants.YouMayLike;
        private const string YouMayLikeAlsoProductListView = "_YouMayLikeAlsoProductList";
        private const string FrequentlyBoughtView = "FrequentlyBought";
        private const string ManageCrossSellView = "ManageCrossSell";
        private const string SearchCommand = "Search";
        private const string ManageFrequentlyBoughtAction = "ManageFrequentlyBought";
        private const string ManageYouMayLikeAlsoAction = "ManageYouMayLikeAlso";
        private const string partialdeleteAction = "partialdelete";
        private const string PersonalizationAction = "Personalization";
        private const string FrequentlyBoughtProductListPartialView = "_FrequentlyBoughtProductList";
        private const string ProductName = "ProductName";
        private const string Name = "name";
        private const string crosssellprodname = "crosssellprodname";
        private const string CrossSellProducts = "private const string";
        private const string sort = "sort";
        private const string crosssellprodname1 = "crosssellprodname1";
        private const string FrequentlyBoughtProduct1 = "FrequentlyBoughtProduct1";
        private const string CrossSellProductListAssignedId = "CrossSellProductList.AssignedId";
        #endregion

        #region Constructor
        public PersonalizationController()
        {
            _personalisationAgent = new PersonalizationAgent();
            _categoryAgent = new CategoryAgent();
            _productAgent = new ProductAgent();
        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Personalization Main page.
        /// </summary>
        /// <returns></returns>
        public ActionResult Personalization()
        {
            return View();
        }

        #region You May Like Also
        /// <summary>
        /// Get You may also like page.
        /// </summary>
        /// <param name="portalId">int portalId</param>
        /// <param name="model">Filter Collection Data Model</param>
        /// <returns>you may also like view</returns>
        public ActionResult GetYouMayLikeAlsoProductList([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model, int portalId = 0, int categoryId = 0)
        {
            PersonalizationViewModel personalizationModel = _personalisationAgent.GetCrossSellList(portalId, categoryId, (int)ProductRelationCode.YouMayAlsoLike);

            //Get list of products according to category id. 
            ProductCrossSellListViewModel productCrossSellItems = _personalisationAgent.GetProductListWithAssociatedItems(personalizationModel.PortalId, categoryId, (int)ProductRelationCode.YouMayAlsoLike, model.Filters, model.SortCollection, model.Page, model.RecordPerPage);

            if (!Equals(model.SortCollection, null) && Convert.ToString(model.SortCollection[sort]).ToLower().Equals(Name))
            {
                model.SortCollection[sort] = ProductName;
            }

            ReportModel gridModel = FilterHelpers.GetDynamicGridModel(model, productCrossSellItems.CrossSellProducts, Convert.ToInt32(ListType.YouMayLikeProductList));
            gridModel.TotalRecordCount = productCrossSellItems.TotalResults;
            personalizationModel.GridModel = gridModel;
            FilterHelpers.CreateDropdown(ref gridModel, personalizationModel.CategoryList, model.Filters, MvcAdminConstants.LabelCategoryId, MvcAdminConstants.LabelName, MvcAdminConstants.LabelCategory);
            return ActionView(YouMayLikeAlsoProductListView, personalizationModel);
        }

        /// <summary>
        /// Manage YouMay Like Also for a products.
        /// </summary>
        /// <param name="productId">Product ID for which youmay like also has to be managed.</param>
        /// <param name="portalId">Portal ID</param>
        /// <param name="categoryId">Category ID</param>
        /// <param name="productName">Product Name as search field</param>
        /// <param name="sku">SKU as search field.</param>
        /// <returns>Manage View.</returns>
        [HttpGet]
        public ActionResult ManageYouMayLikeAlso(int? productId, int? portalId, int? categoryId, string productName = "", string sku = "")
        {
            ProductCrossSellListViewModel productList;
            PersonalizationViewModel personalizationModel = _personalisationAgent.GetPersonalizationModel(productId, portalId, categoryId, productName, sku, out productList);
            personalizationModel.CrossSellProductList = _personalisationAgent.BindCrossSellProducts(productId.Value, productList, Convert.ToInt32(ProductRelationCode.YouMayAlsoLike));

            //Display the the Header titles for Associated & Un associated categories.
            ViewBag.UnAssignedTitle = ZnodeResources.LabelUnassociatedProducts;
            ViewBag.AssignedTitle = ZnodeResources.LabelAssociatedProducts;

            if (!Equals(TempData["SelectItems"], null) && !string.IsNullOrEmpty(TempData["SelectItems"].ToString()))
            {
                ViewBag.message = TempData["SelectItems"].ToString();
            }
            personalizationModel.CrossSellType = (int)ProductRelationCode.YouMayAlsoLike;
            return ActionView(ManageCrossSellView, personalizationModel);
        }

        /// <summary>
        /// Posts the value of Manage page.
        /// </summary>
        /// <param name="model">Personalization Model.</param>
        /// <param name="Command">Command type of the post method.</param>
        /// <returns>Returns result of save or search view.</returns>
        [HttpPost]
        public ActionResult ManageYouMayLikeAlso(PersonalizationViewModel model, string Command)
        {
            bool isCreated = false;
            if (Command.Equals(SearchCommand))
            {
                return RedirectToAction(ManageYouMayLikeAlsoAction, new { productId = model.ProductId, portalId = model.PortalId, categoryId = model.CategoryId, productName = model.ProductName, sku = model.SKU });
            }
            else
            {
                if (!Equals(model, null) && !Equals(model.CrossSellProductList, null) && !Equals(model.CrossSellProductList.AssignedId, null))
                {
                    isCreated = _personalisationAgent.CreateProductCrossSellList(model, (int)ProductRelationCode.YouMayAlsoLike);
                    TempData[MvcAdminConstants.Notifications] = (isCreated)
                                                         ? GenerateNotificationMessages(ZnodeResources.SuccessCreateAssociateProduct, NotificationType.success)
                                                         : GenerateNotificationMessages(ZnodeResources.ErrorCreateAssociatedProduct, NotificationType.error);
                    return RedirectToAction(PersonalizationAction);
                }
            }
            //Get category list according to catalog id.
            _personalisationAgent.BindManageCrossSellData(model, (int)ProductRelationCode.YouMayAlsoLike);
            return View(ManageCrossSellView, model);
        }

        /// <summary>
        /// Delete You May Like Also Products.
        /// </summary>
        /// <param name="listModel">Product ID for which cross sells will be deleted. id</param>
        /// <returns>Json result containg delete result</returns>
        public JsonResult DeleteYouMayLikeAlsoProducts(int id)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();

            if (!id.Equals(0))
            {
                int associatedProductCount = _personalisationAgent.GetCrossSellProducts(id, Convert.ToInt32(ProductRelationCode.YouMayAlsoLike)).CrossSellProducts.Count;
                if (associatedProductCount > 0)
                {
                    status = _personalisationAgent.DeleteCrossSellProducts(id, Convert.ToInt32(ProductRelationCode.YouMayAlsoLike));
                    message = status ? ZnodeResources.DeleteMessage : ZnodeResources.ErrorDeleteCrossSellProduct;
                }
                else
                {
                    message = ZnodeResources.DeleteNoAssociatedItemsMessage;
                }
            }
            return Json(new { sucess = status, message = message, action = partialdeleteAction, id = id, isFadeOut = isFadeOut });
        }

        #endregion

        #region Frequently Brought
        /// <summary>
        /// Gets Frequently bought product list.
        /// </summary>
        /// <param name="model">Filter collection model containing filter values.</param>
        /// <param name="portalId">Portal Id according to which product list will be retrieved.</param>
        /// <param name="categoryId">Category Id according to which products will be retrieved.</param>
        /// <returns>View for Frequently bought product list.</returns>
        public ActionResult GetFrequentlyBoughtProductList([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model, int portalId = 0, int categoryId = 0)
        {
            PersonalizationViewModel personalizationModel = _personalisationAgent.GetCrossSellList(portalId, categoryId, (int)ProductRelationCode.FrequentlyBoughtTogether);

            //Get list of products according to category id. 
            ProductCrossSellListViewModel productCrossSellItems = _personalisationAgent.GetProductListWithAssociatedItems(personalizationModel.PortalId, categoryId, (int)ProductRelationCode.FrequentlyBoughtTogether, model.Filters, model.SortCollection, model.Page, model.RecordPerPage);

            if (!Equals(model.SortCollection, null) && Convert.ToString(model.SortCollection[sort]).ToLower().Equals(Name))
            {
                model.SortCollection[sort] = ProductName;
            }

            ReportModel gridModel = FilterHelpers.GetDynamicGridModel(model, productCrossSellItems.CrossSellProducts, Convert.ToInt32(ListType.FrequentlyBoughtProducts));
            gridModel.TotalRecordCount = productCrossSellItems.TotalResults;
            personalizationModel.GridModel = gridModel;
            FilterHelpers.CreateDropdown(ref gridModel, personalizationModel.CategoryList, model.Filters, MvcAdminConstants.LabelCategoryId, MvcAdminConstants.LabelName, MvcAdminConstants.LabelCategory);

            return ActionView(FrequentlyBoughtProductListPartialView, personalizationModel);
        }

        /// <summary>
        /// Manage Frequently bought for a products.
        /// </summary>
        /// <param name="productId">Product ID for which frequently bought has to be managed.</param>
        /// <param name="portalId">Portal ID</param>
        /// <param name="categoryId">Category ID</param>
        /// <param name="productName">Product Name as search field</param>
        /// <param name="sku">SKU as search field.</param>
        /// <returns>Manage View.</returns>
        [HttpGet]
        public ActionResult ManageFrequentlyBought(int? productId, int? portalId, int? categoryId, string productName = "", string sku = "")
        {
            ProductCrossSellListViewModel productList;
            PersonalizationViewModel personalizationModel = _personalisationAgent.GetPersonalizationModel(productId, portalId, categoryId, productName, sku, out productList);
            personalizationModel.CrossSellProductList = _personalisationAgent.BindCrossSellProducts(productId.Value, productList, Convert.ToInt32(ProductRelationCode.FrequentlyBoughtTogether));

            //Display the the Header titles for Associated & Un associated categories.
            ViewBag.UnAssignedTitle = ZnodeResources.LabelUnassociatedProducts;
            ViewBag.AssignedTitle = ZnodeResources.LabelAssociatedProducts;

            if (!Equals(TempData["SelectItems"], null) && !string.IsNullOrEmpty(TempData["SelectItems"].ToString()))
            {
                ViewBag.message = TempData["SelectItems"].ToString();
            }
            personalizationModel.CrossSellType = (int)ProductRelationCode.FrequentlyBoughtTogether;
            return ActionView(ManageCrossSellView, personalizationModel);
        }



        /// <summary>
        /// Posts the value of Manage page.
        /// </summary>
        /// <param name="model">Personalization Model.</param>
        /// <param name="Command">Command type of the post method.</param>
        /// <returns>Returns result of save or search view.</returns>
        [HttpPost]
        public ActionResult ManageFrequentlyBought(PersonalizationViewModel model, string Command)
        {
            bool isCreated = false;
            if (Command.Equals(SearchCommand))
            {
                return RedirectToAction(ManageFrequentlyBoughtAction, new { productId = model.ProductId, portalId = model.PortalId, categoryId = model.CategoryId, productName = model.ProductName, sku = model.SKU });
            }
            else
            {

                if (!Equals(model, null) && !Equals(model.CrossSellProductList, null) && !Equals(model.CrossSellProductList.AssignedId, null))
                {
                    isCreated = _personalisationAgent.CreateProductCrossSellList(model, (int)ProductRelationCode.FrequentlyBoughtTogether);
                    TempData[MvcAdminConstants.Notifications] = (isCreated)
                                                         ? GenerateNotificationMessages(ZnodeResources.SuccessCreateAssociateProduct, NotificationType.success)
                                                         : GenerateNotificationMessages(ZnodeResources.ErrorCreateAssociatedProduct, NotificationType.error);
                    return RedirectToAction(PersonalizationAction);
                }
            }
            //Get category list according to catalog id.
            _personalisationAgent.BindManageCrossSellData(model, (int)ProductRelationCode.FrequentlyBoughtTogether);
            ModelState.AddModelError(CrossSellProductListAssignedId, ZnodeResources.revListViewAssignedId);
            return View(ManageCrossSellView, model);
        }



        /// <summary>
        /// Delete Frequently Bought Products.
        /// </summary>
        /// <param name="listModel">Product ID for which cross sells will be deleted. id</param>
        /// <returns>Json result containg delete result</returns>    
        public JsonResult DeleteFrequentlyBroughtProducts(int id)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();



            if (!id.Equals(0))
            {
                int associatedProductCount = _personalisationAgent.GetCrossSellProducts(id, Convert.ToInt32(ProductRelationCode.FrequentlyBoughtTogether)).CrossSellProducts.Count;
                if (associatedProductCount > 0)
                {
                    status = _personalisationAgent.DeleteCrossSellProducts(id, Convert.ToInt32(ProductRelationCode.FrequentlyBoughtTogether));
                    message = status ? ZnodeResources.DeleteMessage : @ZnodeResources.ErrorDeleteCrossSellProduct;
                }
                else
                {
                    message = ZnodeResources.DeleteNoAssociatedItemsMessage;
                    return Json(new { sucess = status, message = message, action = partialdeleteAction, id = id, isFadeOut = isFadeOut });
                }
            }
            return Json(new { sucess = status, message = message, action = partialdeleteAction, id = id, isFadeOut = isFadeOut });
        }
        #endregion

        #endregion
    }
}
