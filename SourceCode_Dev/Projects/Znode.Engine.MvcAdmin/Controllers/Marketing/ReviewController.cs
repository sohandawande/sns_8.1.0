﻿using Resources;
using System;
using System.Web.Mvc;
using Znode.Engine.Api.Models.Enum;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Controllers.Marketing
{
    public class ReviewController : BaseController
    {
        #region Private Variables
        private readonly IReviewAgent _reviewAgent;
        private string customerReviewsList = "List";
        #endregion

        #region Constructor
        public ReviewController()
        {
            _reviewAgent = new ReviewAgent();
        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Get Customer Reviews.
        /// </summary>
        /// <param name="model">FilterCollectionDataModel</param>
        /// <returns>List Of Customer Review</returns>
        public ActionResult List([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model) 
        {
            ReviewViewModel reviews = _reviewAgent.GetReviews(model.Filters, model.SortCollection, model.Page, model.RecordPerPage);

            var gridModel = FilterHelpers.GetDynamicGridModel(model, reviews.ReviewItems, Convert.ToInt32(ListType.vw_ZNodeGetcustReview));
            reviews.GridModel = gridModel;
            //Dropdown for Review Status
            FilterHelpers.CreateDropdown(ref gridModel, _reviewAgent.BindStatus().GetReviewStatusList, model.Filters, MvcAdminConstants.Value, MvcAdminConstants.Text, MvcAdminConstants.StatusDropdownName);
            reviews.GridModel.TotalRecordCount = !Equals(reviews, null) ? reviews.TotalResults : 0;
            return ActionView(reviews);
        }

        /// <summary>
        /// Change Status of Review
        /// </summary>
        /// <param name="id">Review Id</param>
        /// <returns>Current Status</returns>
        public ActionResult ChangeReviewStatus(int id)
        {
            ReviewItemViewModel review = _reviewAgent.GetReview(id);
            review.ReviewId = id;
            return View(review);
        }

        /// <summary>
        /// Change Status of Review
        /// </summary>
        /// <param name="model">ReviewItemViewModel</param>
        /// <returns>Current Status</returns>
        [HttpPost]
        public ActionResult ChangeReviewStatus(ReviewItemViewModel model)
        {
            TempData[MvcAdminConstants.Notifications] = (_reviewAgent.UpdateReviewStatus(model))
                                                       ? GenerateNotificationMessages(ZnodeResources.UpdateReviewStatusSuccess, NotificationType.success)
                                                       : GenerateNotificationMessages(ZnodeResources.UpdateReviewStatusError, NotificationType.error);
            return RedirectToAction(customerReviewsList);
        }

        /// <summary>
        /// Edit Review Status.
        /// </summary>
        /// <param name="id">Review Id</param>
        /// <returns>Return Edit View for review</returns>
        public ActionResult EditReview(int id)
        {
            if (id > 0)
            {
                ReviewItemViewModel review = _reviewAgent.GetReview(id);
                if (Equals(review, null))
                {
                    return RedirectToAction(customerReviewsList);
                }
                review.ReviewId = id;
                return View(review);
            }
            return RedirectToAction(customerReviewsList);
        }

        /// <summary>
        /// Edit Review Status.
        /// </summary>
        /// <param name="model">ReviewItemViewModel</param>
        /// <returns>True / False</returns>
        [HttpPost]
        public ActionResult EditReview(ReviewItemViewModel model)
        {
            if (ModelState.IsValid)
            {
                TempData[MvcAdminConstants.Notifications] = (_reviewAgent.UpdateReview(model))
                                                        ? GenerateNotificationMessages(ZnodeResources.UpdateReviewSuccess, NotificationType.success)
                                                        : GenerateNotificationMessages(ZnodeResources.UpdateReviewError, NotificationType.error);
                return RedirectToAction(customerReviewsList);
            }
            return View(model);
        }

        /// <summary>
        /// Delete Existing Customer Review
        /// </summary>
        /// <param name="id">Review Id</param>
        /// <returns>success message on delete</returns>
        [HttpPost]
        public JsonResult DeleteReview(int? id)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();
            if (!Equals(null, id))
            {
                status = _reviewAgent.DeleteReview(id.Value);
                message = status ? ZnodeResources.DeleteReviewSuccess : ZnodeResources.DeleteReviewError;
            }
            return Json(new { sucess = status, message = message, id = id, isFadeOut = isFadeOut });
        }
        #endregion
    }
}