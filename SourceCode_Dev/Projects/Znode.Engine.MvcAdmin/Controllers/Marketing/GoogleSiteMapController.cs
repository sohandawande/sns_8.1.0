﻿using Resources;
using System.Web.Mvc;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Controllers.Marketing
{
    /// <summary>
    /// Google Site Map Controller
    /// </summary>    
    public class GoogleSiteMapController : BaseController
    {
        #region Private Variables

        private readonly IGoogleSiteMapAgent _googleSiteMapAgent;
        private readonly IPermissionAgent _permissionAgent;
        private string CreateGoogleSiteMapAction = MvcAdminConstants.CreateGoogleSiteMap;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor for Google Site Map
        /// </summary>
        /// <returns></returns>        
        public GoogleSiteMapController()
        {
            _googleSiteMapAgent = new GoogleSiteMapAgent();
            _permissionAgent = new PermissionAgent();
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Create Google Site Map to generate XML file
        /// </summary>
        /// <returns>Returns view for generating XML file</returns>
        public ActionResult CreateGoogleSiteMap()
        {
            return View(_googleSiteMapAgent.BindViewModel());
        }

        /// <summary>
        /// Generates XML file 
        /// </summary>
        /// <param name="model">GoogleSiteMapViewModel</param>
        /// <returns>Returns view containing generated XML file</returns>
        [HttpPost]
        public ActionResult CreateGoogleSiteMap(GoogleSiteMapViewModel model)
        {
            if (Equals(model.StoreList,null))
            {
                TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.ValidationStoreSelect, NotificationType.error);
            }
            string message = string.Empty;
            if (ModelState.IsValid)
            {
                var domainName = string.Concat(Request.Url.Scheme, System.Uri.SchemeDelimiter, Request.Url.Host);
                if (HttpContext.IsDebuggingEnabled)
                {
                    domainName = string.Concat(domainName, ":", Request.Url.Port);
                }

                GoogleSiteMapViewModel status = _googleSiteMapAgent.CreateGoogleSiteMap(model, domainName,out message);

                if (!string.IsNullOrEmpty(status.ErrorMessage))
                {
                    TempData[MvcAdminConstants.Notifications] = (GenerateNotificationMessages(status.ErrorMessage, NotificationType.error));
                }
                else
                {
                    TempData[MvcAdminConstants.Notifications] = (!Equals(status.SuccessXMLGenerationMessage, null)) ? GenerateNotificationMessages(ZnodeResources.SuccessXMLGenerationMessage + "<br/> <a href='" + status.SuccessXMLGenerationMessage + "' target='_blank'>" + status.SuccessXMLGenerationMessage + "</a>", NotificationType.success) : GenerateNotificationMessages(string.Empty, NotificationType.error);
                }
                model = _googleSiteMapAgent.BindViewModel();
            }
            model =_googleSiteMapAgent.BindViewModel();
            return View(model);
        }    

        /// <summary>
        /// Gets the list of stores
        /// </summary>
        /// <returns>Returns list of stores</returns>
        [HttpGet]
        public ActionResult GetStoreList()
        {
            GoogleSiteMapViewModel googleSiteMap = new GoogleSiteMapViewModel();
            googleSiteMap = _googleSiteMapAgent.BindViewModel();

            return ActionView(CreateGoogleSiteMapAction, googleSiteMap);
        }

        #endregion
    }
}