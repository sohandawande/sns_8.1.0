﻿using Resources;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Enum;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Controllers
{
    /// <summary>
    /// Manage SEO Controller
    /// </summary>
    public class ManageSEOController : BaseController
    {
        #region Private Variables
        private readonly IPortalAgent _portalAgent;
        private readonly IManageSEOAgent _manageSEOAgent;
        private readonly IProductAgent _productAgent;
        private readonly IContentPageAgent _contentPageAgent;
        private readonly ICategoryAgent _categoryAgent;
        private readonly ICatalogAgent _catalogAgent;
        private readonly IPortalCatalogAgent _portalCatalogAgent;
        private readonly ISearchAgent _searchAgent;
        private string manageSeoView = "ManageSEO";
        private string actionGetSettings = "GetSettings";
        private string productListPartialView = "_ProductList";
        private string editProductView = "EditProduct";
        private string categoryListPartialView = "_CategoryList";
        private string editCategoryView = "EditCategory";
        private string actionDefaultSetting = "DefaultSetting";
        private readonly IURLRedirectAgent _urlRedirectAgent;
        private string seoProductListPartialView = "_SEOProductList";
        private string editContentPage = "EditContentPage";
        private string urlRedirectsList = "_UrlRedirectsList";
        private string listContentPagesSEO = "_ContentPagesList";
        private string createEditURLRedirect = "CreateEditURLRedirect";
        private string trueValue = "True";
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor for Manage SEO
        /// </summary>
        public ManageSEOController()
        {
            _portalAgent = new PortalAgent();
            _manageSEOAgent = new ManageSEOAgent();
            _productAgent = new ProductAgent();
            _contentPageAgent = new ContentPageAgent();
            _categoryAgent = new CategoryAgent();
            _urlRedirectAgent = new URLRedirectAgent();
            _catalogAgent = new CatalogAgent();
            _portalCatalogAgent = new PortalCatalogAgent();
            _searchAgent = new SearchAgent();
        }
        #endregion

        #region Default Setting

        /// <summary>
        /// To show Manage SEO - Default Setting page
        /// </summary>
        /// <returns>returns Default Setting page</returns>
        [HttpGet]
        public ActionResult DefaultSetting(string tabMode = "")
        {
            if (!string.IsNullOrEmpty(tabMode))
            {
                TempData[MvcAdminConstants.SetTabMode] = tabMode;
            }
            ManageSEOViewModel model = new ManageSEOViewModel();
            List<SelectListItem> list = _manageSEOAgent.BindPortalList();
            if (!Equals(list, null) && list.Count > 0)
            {
                model.PortalId = Convert.ToInt32(list[0].Value);
                model = _manageSEOAgent.GetPortalById(model.PortalId);
            }
            model.PortalList = list;
            return View(manageSeoView, model);
        }

        /// <summary>
        /// To get portal wise default settings
        /// </summary>
        /// <param name="id">int id = portal id</param>
        /// <returns>returns default setting for page</returns>
        [HttpGet]
        public ActionResult GetSettings(int? id)
        {
            ManageSEOViewModel model = new ManageSEOViewModel();
            if (!Equals(id, null))
            {
                model = _manageSEOAgent.GetPortalById(id.Value);
            }
            if (Equals(model, null))
            {
                model = new ManageSEOViewModel();
                model.PortalId = id.Value;
            }
            model.PortalList = _manageSEOAgent.BindPortalList();
            return View(manageSeoView, model);
        }

        /// <summary>
        /// To update default setting 
        /// </summary>
        /// <param name="model">ManageSEOViewModel model</param>
        /// <returns>returns default setting page</returns>
        [HttpPost]
        public ActionResult SaveDefaultSetting(ManageSEOViewModel model)
        {
            if (ModelState.IsValid)
            {
                bool status = _portalAgent.UpdatePortalDefaultSettings(model);
                TempData[MvcAdminConstants.Notifications] = status ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.SaveErrorMessage, NotificationType.error);
            }
            return RedirectToAction(actionGetSettings, new { id = model.PortalId });
        }

        #endregion

        #region Products

        /// <summary>
        /// To show SEO Product list
        /// </summary>
        /// <param name="formModel">FilterCollectionDataModel formModel</param>
        /// <returns>returns product list</returns>
        public ActionResult ProductList([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel formModel)
        {
            ManageSEOProductListViewModel model = new ManageSEOProductListViewModel();

            formModel.Filters.Add(new FilterTuple(FilterKeys.Username, FilterOperators.Equals, "\'" + HttpContext.User.Identity.Name + "\'"));

            model.ProductList = _productAgent.GetProducts(formModel.Filters, formModel.SortCollection, formModel.Page, formModel.RecordPerPage);
            if (Equals(model.ProductList, null))
            {
                return ActionView(productListPartialView, new ManageSEOProductListViewModel());
            }

            List<ProductTypeModel> productTypes = _productAgent.GetProductType();
            List<ManufacturerModel> manufacturerType = _productAgent.GetManufacturer();
            CatalogAssociatedCategoriesListViewModel Categories = _categoryAgent.GetCategoryList();

            ReportModel gridModel = FilterHelpers.GetDynamicGridModel(formModel, model.ProductList.Products, Convert.ToInt32(ListType.vw_ZNodeGetDistinctProductsByCriteria));

            #region Dynamic Dropdown Methods

            FilterHelpers.CreateDropdown(ref gridModel, _portalCatalogAgent.GetCatalogListByUserName(), formModel.Filters, MvcAdminConstants.LabelCatalogId, MvcAdminConstants.CatalogName, ZnodeResources.LabelCatalogs);
            manufacturerType.Insert(0, new ManufacturerModel { Name = MvcAdminConstants.LabelNotApplicable });
            FilterHelpers.CreateDropdown(ref gridModel, manufacturerType, formModel.Filters, MvcAdminConstants.LabelManufacturerId, MvcAdminConstants.LabelName, MvcAdminConstants.LabelBrand);
            FilterHelpers.CreateDropdown(ref gridModel, productTypes, formModel.Filters, MvcAdminConstants.productTypeId, MvcAdminConstants.LabelName, MvcAdminConstants.LabelProductType);
            FilterHelpers.CreateDropdown(ref gridModel, Categories.AssociatedCategories, formModel.Filters, MvcAdminConstants.LabelCategoryId, MvcAdminConstants.LabelName, MvcAdminConstants.LabelProductCategory);
            #endregion
            model.GridModel = gridModel;
            model.GridModel.TotalRecordCount = (model.ProductList.TotalResults > 0) ? model.ProductList.TotalResults : model.ProductList.Products.Count;
            return ActionView(productListPartialView, model);
        }

        /// <summary>
        /// To edit Product seo details
        /// </summary>
        /// <param name="id">int id product Id</param>
        /// <returns>returns product details page for edit</returns>
        [HttpGet]
        public ActionResult EditProduct(int? id)
        {
            ManageSEOProductViewModel model = new ManageSEOProductViewModel();
            if (!Equals(id, null))
            {
                model = _manageSEOAgent.GetProductSEODetails(id.Value);
            }
            return View(editProductView, Equals(model, null) ? new ManageSEOProductViewModel() : model);
        }

        /// <summary>
        /// To edit product seo setting 
        /// </summary>
        /// <param name="model">ManageSEOProductViewModel model</param>
        /// <returns>returns Product list if success else Edit Product page</returns>
        [HttpPost]
        public ActionResult EditProduct(ManageSEOProductViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (!string.IsNullOrEmpty(model.SeoPageUrl))
                {
                    if (_searchAgent.IsRestrictedSeoUrl(model.SeoPageUrl))
                    {
                        TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.ErrorRestrictedSeoUrlExists, NotificationType.error);
                        return View(editProductView, model);
                    }

                    bool isSeoUrlExist = _productAgent.IsSeoUrlExist(model.ProductId, model.SeoPageUrl);
                    if (isSeoUrlExist)
                    {
                        TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.ErrorSeoUrlAlreadyExist, NotificationType.error);
                        return View(editProductView, model);
                    }
                }
                bool status = _manageSEOAgent.UpdateProductSEODetails(model);
                TempData[MvcAdminConstants.Notifications] = status ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.SaveErrorMessage, NotificationType.error);
            }
            return RedirectToAction(actionDefaultSetting, new { tabMode = Convert.ToString(SelectedTab.Product) });
        }
        #endregion

        #region Category

        /// <summary>
        /// To show seo category list
        /// </summary>
        /// <param name="formModel">FilterCollectionDataModel formModel</param>
        /// <returns>returns category list</returns>
        public ActionResult CategoryList([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel formModel)
        {
            ManageSEOCategoryListViewModel model = new ManageSEOCategoryListViewModel();
            formModel.Filters.Add(new FilterTuple(FilterKeys.IsManageSEO, FilterOperators.Equals, trueValue));

            model.CategoryList = _categoryAgent.GetCategoryList(formModel.Filters, formModel.SortCollection, formModel.Page, formModel.RecordPerPage);
            if (Equals(model.CategoryList, null))
            {
                return ActionView(categoryListPartialView, new ManageSEOCategoryListViewModel());
            }
            ReportModel gridModel = FilterHelpers.GetDynamicGridModel(formModel, model.CategoryList.AssociatedCategories, Convert.ToInt32(ListType.vw_ZNodeSearchCategoriesByPortalCatalog));

            FilterHelpers.CreateDropdown(ref gridModel, _portalCatalogAgent.GetCatalogListByUserName(), formModel.Filters, MvcAdminConstants.LabelCatalogId, MvcAdminConstants.CatalogName, ZnodeResources.LabelCatalogs);

            model.GridModel = gridModel;
            model.GridModel.TotalRecordCount = (model.CategoryList.TotalResults > 0) ? model.CategoryList.TotalResults : model.CategoryList.AssociatedCategories.Count;
            return ActionView(categoryListPartialView, model);
        }

        /// <summary>
        /// To get category seo details by categoryId
        /// </summary>
        /// <param name="id">int id </param>
        /// <returns>returns cate seo edit page</returns>
        [HttpGet]
        public ActionResult EditCategory(int? id)
        {
            ManageSEOCategoryViewModel model = new ManageSEOCategoryViewModel();
            if (!Equals(id, null))
            {
                model = _manageSEOAgent.GetCategory(id.Value);
            }
            return View(editCategoryView, Equals(model, null) ? new ManageSEOCategoryViewModel() : model);
        }

        /// <summary>
        /// To update Category SEO setting
        /// </summary>
        /// <param name="model">CategoryViewModel model</param>
        /// <returns>returns Category list if success else Edit category page</returns>
        [HttpPost]
        public ActionResult EditCategory(ManageSEOCategoryViewModel model)
        {
            string errorCode = string.Empty;
            if (ModelState.IsValid)
            {
                if (!string.IsNullOrEmpty(model.SeoUrl) && _searchAgent.IsRestrictedSeoUrl(model.SeoUrl))
                {
                    TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.ErrorRestrictedSeoUrlExists, NotificationType.error);
                    return View(editCategoryView, model);
                }

                errorCode = _categoryAgent.UpdateCategorySEODetails(model);
                string message = string.Empty;
                switch (errorCode)
                {
                    case "103":
                        message = GenerateNotificationMessages(ZnodeResources.ErrorCategorySeoUrlAlreadyExist, NotificationType.error);
                        break;
                    default:
                        message = GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success);
                        break;
                }
                TempData[MvcAdminConstants.Notifications] = message;
            }

            if (string.IsNullOrEmpty(errorCode))
            {
                return RedirectToAction(actionDefaultSetting, new { tabMode = Convert.ToString(SelectedTab.Category) });
            }
            else
            {
                return View(editCategoryView, model);
            }
        }
        #endregion

        #region Content pages

        /// <summary>
        /// Gets List Of Content Page
        /// </summary>
        /// <returns>View of content page SEO</returns>
        public ActionResult ListContentPages([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model)
        {
            ContentPageListViewModel contentPage = _contentPageAgent.GetContentPages(model.Filters, model.SortCollection, model.Page, model.RecordPerPage);
            ReportModel gridModel = FilterHelpers.GetDynamicGridModel(model, contentPage.ContentPages, Convert.ToInt32(ListType.ContentPageSEOList));
            gridModel.TotalRecordCount = contentPage.TotalResults;
            return ActionView(listContentPagesSEO, gridModel);
        }

        /// <summary>
        /// Edit existing Content Page SEO.
        /// </summary>
        /// <param name="id">Id of the content page.</param>
        /// <returns>View of the Content page SEO</returns>
        [HttpGet]
        public ActionResult EditContentPageSEO(int? id)
        {
            ContentPageViewModel model = null;
            if (!Equals(id, null) && id > 0)
            {
                model = _contentPageAgent.GetContentPage(id.Value);
                model.IsEditMode = true;
            }
            return View(editContentPage, model);
        }

        /// <summary>
        /// Edit existing Content Page SEO.
        /// </summary>
        /// <param name="model">model to update.</param>
        /// <returns>Action Result</returns>
        [HttpPost]
        public ActionResult EditContentPageSEO(ContentPageViewModel model)
        {
            string errorMessage = string.Empty;
            bool status = false;
            if (!Equals(model, null) && (model.ContentPageId > 0))
            {
                status = _manageSEOAgent.UpdateContentPageSEOInformation(model, out errorMessage);
                TempData[MvcAdminConstants.Notifications] = status ? GenerateNotificationMessages(ZnodeResources.UpdateMessage, NotificationType.success) : (!string.IsNullOrEmpty(errorMessage)) ? GenerateNotificationMessages(errorMessage, NotificationType.error) : GenerateNotificationMessages(ZnodeResources.ErrorSeoUrlAlreadyExist, NotificationType.error);
            }
            if (!status)
            {
                model.IsEditMode = true;
                return View(editContentPage, model);
            }
            return RedirectToAction(actionDefaultSetting, new { tabMode = Convert.ToString(SelectedTab.ContentPage) });
        }

        #endregion

        #region URL Redirects

        /// <summary>
        /// Gets a list of URL Redirects.
        /// </summary>
        /// <param name="model">FilterCollectionDataModel model.</param>
        /// <returns>List of URL Redirects.</returns>
        public ActionResult ListUrlRedirects([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model)
        {
            URLRedirectListViewModel urlRedirect = _urlRedirectAgent.GetUrlRedirects(model.Filters, model.SortCollection, model.Page, model.RecordPerPage);
            ReportModel gridModel = FilterHelpers.GetDynamicGridModel(model, urlRedirect.UrlRedirects, Convert.ToInt32(ListType.UrlRedirectSEOList));
            FilterHelpers.CreateDropdown(ref gridModel, _urlRedirectAgent.GetStatusList(), model.Filters, MvcAdminConstants.Value, MvcAdminConstants.Text, ZnodeResources.LabelUrlStatus);
            gridModel.TotalRecordCount = urlRedirect.TotalResults;
            return ActionView(urlRedirectsList, gridModel);
        }

        /// <summary>
        /// Create Url Redirect.
        /// </summary>
        /// <returns>View of create Url Redirect</returns>
        [HttpGet]
        public ActionResult CreateURLRedirect()
        {
            URLRedirectViewModel uRLRedirectViewModel = new URLRedirectViewModel();
            uRLRedirectViewModel.IsActive = true;
            return View(createEditURLRedirect, uRLRedirectViewModel);
        }

        /// <summary>
        /// Create url Redirection.
        /// </summary>
        /// <param name="model">Model to create.</param>
        /// <returns>Returns the View of URl Redirect Tab.</returns>
        [HttpPost]
        public ActionResult CreateURLRedirect(URLRedirectViewModel model)
        {
            string errorMessage = string.Empty;
            bool status = false;
            if (ModelState.IsValid)
            {
                status = _urlRedirectAgent.CreateUrlRedirect(model, out errorMessage);
                TempData[MvcAdminConstants.Notifications] = status ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.SaveErrorMessage, NotificationType.error);
            }
            if (status.Equals(false) && !string.IsNullOrEmpty(errorMessage))
            {
                TempData[MvcAdminConstants.Notifications] = status ? GenerateNotificationMessages(ZnodeResources.UpdateMessage, NotificationType.success) : GenerateNotificationMessages(errorMessage, NotificationType.error);
                return View(createEditURLRedirect, model);
            }
            return RedirectToAction(actionDefaultSetting, new { tabMode = Convert.ToString(SelectedTab.URLRedirect) });
        }

        /// <summary>
        /// Edit the URL Redirect.
        /// </summary>
        /// <param name="id">Id of the URL Redirect.</param>
        /// <returns>View of Edit url Redirect.</returns>
        [HttpGet]
        public ActionResult EditURLRedirect(int? id)
        {
            URLRedirectViewModel model = null;
            if (!Equals(id, null) && id > 0)
            {
                model = _urlRedirectAgent.GetUrlRedirect(id.Value);
            }
            return View(createEditURLRedirect, model);
        }

        /// <summary>
        /// Edit the existing Url Redirect.
        /// </summary>
        /// <param name="model">updated model.</param>
        /// <returns>Returns the View of URl Redirect Tab.</returns>
        [HttpPost]
        public ActionResult EditURLRedirect(URLRedirectViewModel model)
        {
            string errorMessage = string.Empty;
            bool status = false;
            if (ModelState.IsValid)
            {
                status = _urlRedirectAgent.UpdateUrlRedirect(model, out errorMessage);
                TempData[MvcAdminConstants.Notifications] = status ? GenerateNotificationMessages(ZnodeResources.UpdateMessage, NotificationType.success) : GenerateNotificationMessages(errorMessage, NotificationType.error);
            }
            if (status.Equals(false) && !string.IsNullOrEmpty(errorMessage))
            {
                TempData[MvcAdminConstants.Notifications] = status ? GenerateNotificationMessages(ZnodeResources.UpdateMessage, NotificationType.success) : GenerateNotificationMessages(errorMessage, NotificationType.error);
                return View(createEditURLRedirect, model);
            }
            return RedirectToAction(actionDefaultSetting, new { tabMode = Convert.ToString(SelectedTab.URLRedirect) });
        }

        /// <summary>
        /// Delete the url redirect.
        /// </summary>
        /// <param name="id">Id of the url redirect.</param>
        /// <returns>JsonResult</returns>
        public JsonResult DeleteURLRedirect(int? id)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();

            if (!Equals(id > 0))
            {
                status = _urlRedirectAgent.DeleteUrlRedirect(id.Value, out message);
                message = status ? ZnodeResources.DeleteMessage : message;
            }

            return Json(new { sucess = status, message = message, id = id, isFadeOut = isFadeOut });
        }

        #endregion

        #region Private Method

        private enum SelectedTab
        {
            Product,
            Category,
            ContentPage,
            URLRedirect
        }

        #endregion
    }
}