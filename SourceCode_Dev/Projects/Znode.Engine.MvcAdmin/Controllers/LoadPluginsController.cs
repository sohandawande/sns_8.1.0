﻿using System.Web.Mvc;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.Agents.IAgents;
using Znode.Engine.MvcAdmin.Helpers;

namespace Znode.Engine.MvcAdmin.Controllers
{
    public class LoadPluginsController : BaseController
    {
        private readonly ILoadPluginsAgent _loadPluginsAgent;

        public LoadPluginsController()
        {
            _loadPluginsAgent = new LoadPluginsAgent();
        }

        /// <summary>
        /// Load the newly added plugins into the temp folder and updates the Golble.asax file because when the new assamblies are loaded
        /// at runtime even after restarting the site the changes are not reflected. This happnes because when the MVC application is loaded 
        /// a xml file is created in root folder which contains name of all the controllers in the Application.
        /// This file is updated only when we make changes in global.asax or in bin folder.
        /// </summary>
        /// <returns>Redirect to DeepLoadPlugins method</returns>
        public ActionResult LoadPlugins()
        {
            _loadPluginsAgent.IsPluginsLoaded();
            return RedirectToAction("DeepLoadPlugins");
        }


        /// <summary>
        /// Load all the dll from temp folder into the project as reference and update Global.asax file
        /// </summary>
        /// <returns>Redirect to Login Screen</returns>
        public ActionResult DeepLoadPlugins()
        {
            _loadPluginsAgent.IsPluginsLoaded();
            return RedirectToAction(MvcAdminConstants.LoginAction,MvcAdminConstants.AccountController);
        }

    }
}