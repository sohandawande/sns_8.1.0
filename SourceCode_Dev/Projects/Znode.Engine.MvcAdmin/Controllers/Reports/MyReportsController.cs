﻿using Resources;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web.Mvc;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Models.Enum;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.Agents.IAgents;
using Znode.Engine.MvcAdmin.Extensions;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Models;
using Znode.Engine.MvcAdmin.ViewModels;
namespace Znode.Engine.MvcAdmin.Controllers.Reports
{
    public class MyReportsController : BaseController
    {
        #region Private Variables
        private readonly IReportsAgent _reportsAgent;
        private const string ReportNoKey = "reportno";
        private const string IndexView = "~/Views/Reports/Index.cshtml";
        private const string ReportView = "~/Views/Reports/Report.cshtml";
        private const string FilterCollections = "FilterCollections";
        private const string IsChart = "IsChart";
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public MyReportsController()
        {
            _reportsAgent = new ReportsAgent();
        }
        #endregion

        #region Public Methods

        public ActionResult Index()
        {
            return View(IndexView);
        }
        /// <summary>
        /// Method to generate Reports
        /// </summary>
        /// <param name="model">FilterCollectionDataModel model</param>
        /// <returns>Returns view containing Reports</returns>
        public ActionResult Report([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model)
        {

            int reportno = 0;
            model.Params.ToList().ForEach(x =>
            {
                if (x.Key.Equals(ReportNoKey))
                {
                    Session[ReportNoKey] = int.Parse(x.Value);
                }
            });

            reportno = int.Parse(Session[ReportNoKey].ToString());
            ReportDataViewModel reportmodel = new ReportDataViewModel();
            reportmodel = _reportsAgent.GetReportData(reportno, model.Filters, model.SortCollection, model.Page, model.RecordPerPage, 0);

            var gridModel = FilterHelpers.GetDynamicGridModel(model, reportmodel.ReportDataSet, reportno);

            if (!Equals(reportmodel.StoreList, null) && !Equals(reportmodel.StoreList.Count, 0))
            {
                FilterHelpers.CreateDropdown(ref gridModel, reportmodel.StoreList, model.Filters, MvcAdminConstants.PortalId, MvcAdminConstants.StoreName, MvcAdminConstants.StoreDropdownName);
            }

            if (!Equals(reportmodel.SupplierList, null) && !Equals(reportmodel.SupplierList.Suppliers.Count, 0))
            {
                FilterHelpers.CreateDropdown(ref gridModel, reportmodel.SupplierList.Suppliers, model.Filters, MvcAdminConstants.SupplierId, MvcAdminConstants.Name, MvcAdminConstants.LabelSupplierName);
            }

            if (!Equals(reportmodel.OrderStateList, null) && !Equals(reportmodel.OrderStateList.Count, 0))
            {
                FilterHelpers.CreateDropdown(ref gridModel, reportmodel.OrderStateList, model.Filters, MvcAdminConstants.OrderStateId, MvcAdminConstants.OrderStateName, MvcAdminConstants.LabelOrderState);
            }

            if (!Equals(reportmodel.DurationList, null) && !Equals(reportmodel.DurationList.Count, 0))
            {
                FilterHelpers.CreateDropdown(ref gridModel, reportmodel.DurationList, model.Filters, MvcAdminConstants.DurationId, MvcAdminConstants.DurationName, MvcAdminConstants.LabelGetSearchesForTheLast);
            }

            if (!Equals(reportmodel.CategoryList, null) && !Equals(reportmodel.CategoryList.Count, 0))
            {
                FilterHelpers.CreateDropdown(ref gridModel, reportmodel.CategoryList, model.Filters, MvcAdminConstants.LabelCategoryName, MvcAdminConstants.LabelCategoryName, MvcAdminConstants.LabelCategory);
            }

            if (!Equals(reportmodel.GroupByList, null) && !Equals(reportmodel.GroupByList.Count, 0))
            {
                FilterHelpers.CreateDropdown(ref gridModel, reportmodel.GroupByList, model.Filters, MvcAdminConstants.DurationId, MvcAdminConstants.DurationName, MvcAdminConstants.LabelGroupBy);
            }

            gridModel.TotalRecordCount = reportmodel.TotalResults;

            if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["title"])))
            {
                ViewBag.Title = Convert.ToString(Request.QueryString["title"]);
            }
            else
            {
                ViewBag.Title = reportmodel.ReportTitle;
            }

            if (Equals(reportno, (int)ListType.ZNode_ReportsCustom) || Equals(reportno, (int)ListType.ZNode_ReportsRecurring) || Equals(reportno, (int)ListType.ZNode_ReportsSupplierList) || Equals(reportno, (int)ListType.ZNode_ReportsPicklist))
            {
                gridModel.SubRecordFilterKeyName = "OrderID";
                gridModel.DelegateTypeName = "Znode.Engine.MvcAdmin.Agents.ReportsAgent";
                gridModel.DelegateTypeMethodName = "GetSubRecords";
            }

            Session[FilterCollections] = model.Filters;

            if (HelperMethods.EnableChartReport)
            {
                //Get X and Y Axis
                GetGraphAxis(gridModel);
            }

            gridModel.ExportFileType = _reportsAgent.GetFileTypes();
            return ActionView(ReportView, gridModel);
        }

        public JsonResult GetCharts(int reportId, string groupByClause, string countClause)
        {
            //Note :  groupByClause represent X Axis of graph and countClause represent y Axis of graph
            string graphTitle = string.Empty;

            int totalRecordCount = 0;
            List<ChartModel> reportGraph = new List<ChartModel>();
            FilterCollection filters = (FilterCollection)Session[FilterCollections];
            var _isChart = filters.FirstOrDefault(x => Equals(x.Item1, IsChart));
            if (Equals(_isChart, null))
            {
                filters.Add(new FilterTuple(IsChart, FilterOperators.Equals, "1"));
            }
            ReportDataViewModel reportmodel = _reportsAgent.GetReportData(reportId, filters, null, null, null, 0);
            filters.Remove(_isChart);
            var ListByOwner = reportmodel.ReportDataSet.GroupBy(l => l.GetDynamicProperty(groupByClause))
                          .Select(lg =>
                                new
                                {
                                    GroupName = lg.Key,
                                    Count = lg.Count(),
                                    Total = lg.Sum(w => Convert.ToDecimal(Convert.ToString(w.GetDynamicProperty(countClause))))
                                });

            if (!Equals(ListByOwner, null))
            {
                foreach (var _recored in ListByOwner)
                {
                    string _group = _recored.GroupName.ToString();
                    string _total = _recored.Total.ToString();
                    int _recordCount = Convert.ToInt32(Convert.ToDecimal(_total));
                    reportGraph.Add(new ChartModel() { RecordCount = _recordCount, GroupClause = _group });
                    totalRecordCount = totalRecordCount + _recordCount;
                }
            }
            return Json(new { status = MvcAdminConstants.Success, reportGraphData = reportGraph, totalRecordCount = totalRecordCount, graphTitle = graphTitle }, JsonRequestBehavior.AllowGet);
        }

        public void GetGraphAxis(ReportModel reportModel)
        {
            _reportsAgent.GetGraphAxis(reportModel);
        }

        /// <summary>
        /// Export report data to excel on the basis of reportid.
        /// </summary>
        /// <param name="reportId">int reportId.</param>
        /// <param name="exportFileTypeId">string exportFileTypeId</param>
        /// <returns></returns>
        public ActionResult ExportReports(int reportId, string exportFileTypeId)
        {
            reportId = reportId.Equals(0) ? Convert.ToInt32(Session[DynamicGridConstants.listTypeSesionKey]) : reportId;
            //Get the Export Details.
            FilterCollection filters = (FilterCollection)Session["FilterCollections"];
            ReportDataViewModel reportmodel = _reportsAgent.GetReportData(reportId, filters, null, null, null, 0);
            ExportViewModel model = new ExportViewModel();
            //Check for the Error.
            if (!Equals(reportmodel, null) && reportmodel.HasError)
            {
                TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(reportmodel.ErrorMessage, Models.NotificationType.error);
            }
            if (!Equals(reportmodel, null) && !Equals(reportmodel.ReportDataSet, null))
            {
                DownloadHelper downloadHelper = new DownloadHelper();
                //Set the Export File Name
                string fileName = Equals(exportFileTypeId, "1") ? string.Format("{0}.xls", Convert.ToString((ListType)reportId)) : string.Format("{0}.csv", Convert.ToString((ListType)reportId));
                model.FileType = exportFileTypeId;
                //Download the Export Data in Specified File.
                downloadHelper.ExportDownload(CreateDataSource(reportmodel.ReportDataSet), model.FileType, Response, null, fileName, false);
            }
            else
            {
                TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.ExportNoRecordFound, Models.NotificationType.info);
            }
            return View(model);
        }

        /// <summary>
        /// Method to create a data set that contains column name exactly as in grid.
        /// </summary>
        /// <param name="dataModel">dataModel</param>
        /// <returns>Returns required data set.</returns>
        private List<dynamic> CreateDataSource(List<dynamic> dataModel)
        {
            List<dynamic> XMLConfigurableList = (List<dynamic>)Session[DynamicGridConstants.ColumnListSesionKey];

            var _sortedXmlList = XMLConfigurableList.FindAll(x => x.isvisible.Equals(DynamicGridConstants.Yes));
            List<dynamic> _resultList = new List<dynamic>();

            dataModel.ForEach(row =>
            {
                var obj = (IDictionary<string, object>)new ExpandoObject();
                _sortedXmlList.ForEach(col =>
                {
                    obj.Add(col.headertext, row[col.name]);
                });
                _resultList.Add(obj);
            });

            return _resultList;
        }

        #endregion
    }
}