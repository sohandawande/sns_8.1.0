﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Znode.Engine.MvcAdmin.Helpers;

namespace Znode.Engine.MvcAdmin.Controllers
{
    public class AdvanceSearchController : BaseController
    {
        #region Private Variables

        private const string AdvanceSearchView = "~/Views/Filter/AdvanceSearch.cshtml";

        #endregion

        #region Public Methods

        [HttpGet]
        public ActionResult AdvanceSearch()
        {
            var model = FilterHelpers.GetAdvanceSearchView();
            return View(AdvanceSearchView, model);
        }

        [HttpPost]
        public ActionResult AdvanceSearch(string returnUrl)
        {
            FilterHelpers.SaveAdvanceSearch(Request.Form);
            RouteValueDictionary routeValues = new RouteValueDictionary();
            if (returnUrl.IndexOf('?') >= 0)
            {
                string querystr = returnUrl.Split('?')[1];
                string[] param = null;
                string keyName = string.Empty;
                string keyVal = string.Empty;
                if (querystr.IndexOf("&") >= 0)
                {
                    param = querystr.Split('&');

                    param.ToList().ForEach(x =>
                    {
                        keyName = x.ToString().Split('=')[0];
                        keyVal = x.ToString().Split('=')[1];
                        routeValues.Add(keyName, keyVal);
                    });
                }
                else
                {
                    querystr.Split('=');
                    routeValues.Add(querystr.Split('=')[0], querystr.Split('=')[1]);
                }
                return RedirectToAction(returnUrl.Split('?')[0], routeValues);
            }
            return RedirectToAction(returnUrl);

        }

        public ActionResult GetOperatorList(string id)
        {
            return Json(new { status = "success", OperatorString = FilterHelpers.GetOperators(id) }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ClearAdvanceSearch(string returnUrl)
        {
            FilterHelpers.ClearAdvanceSearch();
            RouteValueDictionary routeValues = new RouteValueDictionary();
            if (returnUrl.IndexOf('?') >= 0)
            {
                string querystr = returnUrl.Split('?')[1];
                string[] param = null;
                string keyName = string.Empty;
                string keyVal = string.Empty;
                if (querystr.IndexOf("&") >= 0)
                {
                    param = querystr.Split('&');

                    param.ToList().ForEach(x =>
                    {
                        keyName = x.ToString().Split('=')[0];
                        keyVal = x.ToString().Split('=')[1];
                        routeValues.Add(keyName, keyVal);
                    });
                }
                else
                {
                    querystr.Split('=');
                    routeValues.Add(querystr.Split('=')[0], querystr.Split('=')[1]);
                }
                return RedirectToAction(returnUrl.Split('?')[0], routeValues);
            }
            return RedirectToAction(returnUrl);

        }

        public ActionResult ClearAdvanceSearchAjax(string indexId)
        {
            FilterHelpers.ClearAdvanceSearch();
            var model = FilterHelpers.GetAdvanceSearchView();
            return PartialView("~/Views/Filter/_AdvanceSearch.cshtml", model);
        }


        public ActionResult GetSubGrid(string recoredid, string type, string method)
        {
            var result = FilterHelpers.InvokeStringMethod(type, method, recoredid);
            Models.ReportModel model = new Models.ReportModel();
            model.DataTableToList = result;
            return PartialView("~/Views/Filter/_GridControl.cshtml", model);
        }


        private List<dynamic> ConvertintoDymamic<T>(List<T> dataModel)
        {
            List<dynamic> dynamicList = new List<dynamic>();
            dataModel.ForEach(x =>
            {
                dynamicList.Add((dynamic)x);
            });
            return dynamicList;
        }

        public ActionResult DestroySession()
        {
            Session[MvcAdminConstants.AdvanceSearchReturnUrl] = null;
            return null;
        }

        #endregion
    }
}