﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Models.Enum;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Controllers.XMLGenerator
{
    public class XMLGeneratorController : BaseController
    {

        #region variables
        private readonly IXmlGeneratorAgent _xmlGeneratorAgent;
        private bool isSuccess = false;
        private string message = string.Empty;
        private string status = string.Empty;
        private const string Id = "Id";
        private const int PageNo = 1;
        private const int RecoredPerPage = 10;
        #endregion

        #region Constructor

        public XMLGeneratorController()
        {
            _xmlGeneratorAgent = new XmlGeneratorAgent();
        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Create XML 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult CreateXML()
        {
            return View(new WebGridViewModel());
        }

        /// <summary>
        /// Display List of XML configuration
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult List([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model)
        {
            ApplicationSettingListViewModel modelList = _xmlGeneratorAgent.ApplicationsSettinglList(model);

            var gridModel = FilterHelpers.GetDynamicGridModel(model, modelList.List, (int)ListType.vw_ZNodeApplicationSetting);
            gridModel.TotalRecordCount = modelList.TotalResults;
            return ActionView(gridModel);
        }

        /// <summary>
        /// Get column list for create XML
        /// </summary>
        /// <param name="entityType">Type of Entity for column list</param>
        /// <param name="entityName">Name of Entity for column list</param>
        /// <param name="columnListJson"></param>
        /// <returns></returns>
        public ActionResult GetColumnList(string entityType = "", string entityName = "", string columnListJson = null)
        {
            ListViewModel listModel = GetListViewModel(entityType, entityName, columnListJson);

            return PartialView(DynamicGridConstants.ListBoxView, listModel);
        }

        /// <summary>
        /// Save XMl configuration
        /// </summary>
        /// <param name="columnCollection">Selected column collection</param>
        /// <param name="viewOptions">View Option Name</param>
        /// <param name="entityType">Type of entity</param>
        /// <param name="entityName">Name of entity</param>
        /// <param name="frontPageName">front Page Name</param>
        /// <param name="frontObjectName">front Object Name</param>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult SaveColumnXML(string columnCollection, string viewOptions, string entityType, string entityName, string frontPageName, string frontObjectName, int id = 0)
        {
            List<WebGridColumnModel> list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<WebGridColumnModel>>(columnCollection);
            string xmlSTring = _xmlGeneratorAgent.CreateXMLFile(list);
            isSuccess = _xmlGeneratorAgent.SaveXmlConfiguration(xmlSTring, viewOptions, entityType, entityName, frontPageName, frontObjectName, id);
            TempData[MvcAdminConstants.Notifications] = isSuccess ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.SaveErrorMessage, NotificationType.error);
            return Json(new { IsSuccess = isSuccess, message = message, status = status }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Edit XMl configuration
        /// </summary>
        /// <param name="columnCollection">Selected column collection</param>
        /// <param name="viewOptions">View Option Name</param>
        /// <param name="entityType">Type of entity</param>
        /// <param name="entityName">Name of entity</param>
        /// <param name="frontPageName">front Page Name</param>
        /// <param name="frontObjectName">front Object Name</param>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult EditApplicationSetting(string columnCollection, string viewOptions, string entityType, string entityName, string frontPageName, string frontObjectName, int id = 0)
        {
            if (ModelState.IsValid)
            {
                List<WebGridColumnModel> list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<WebGridColumnModel>>(columnCollection);
                string xmlSTring = _xmlGeneratorAgent.CreateXMLFile(list);
                isSuccess = _xmlGeneratorAgent.UpdateXmlConfiguration(xmlSTring, viewOptions, entityType, entityName, frontPageName, frontObjectName, id);
            }

            TempData[MvcAdminConstants.Notifications] = isSuccess ? GenerateNotificationMessages(ZnodeResources.UpdateMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.UpdateErrorMessage, NotificationType.error);

            return Json(new { IsSuccess = isSuccess, message = message, status = status }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Delete XML Configuration
        /// </summary>
        /// <param name="id">Xml Configuration Id</param>
        /// <returns></returns>
        public ActionResult Delete(int id = 0)
        {
            try
            {
                isSuccess = _xmlGeneratorAgent.DeleteXmlConfiguration(id);

                if (isSuccess)
                {
                    message = DynamicGridConstants.DeleteSuccessMsg;
                    status = DynamicGridConstants.Success;
                }
                else
                {
                    message = DynamicGridConstants.DeleteErrorMsg;
                    status = DynamicGridConstants.Error;
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex);
            }

            return Json(new { sucess = status, message = message, id = id, isFadeOut = true });
        }

        /// <summary>
        /// Get Autocomplete Data 
        /// </summary>
        /// <param name="term"></param>
        /// <param name="entityType">Type of entity</param>
        /// <returns></returns>
        public JsonResult AutoCompleteEntityName(string term = "", string entityType = "")
        {
            var entityNameList = _xmlGeneratorAgent.GetEntityNames(term, entityType);
            return Json(entityNameList, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Set column setting for create xml
        /// </summary>
        /// <param name="columnNames">Names of columns</param>
        /// <param name="viewMode">View mode </param>
        /// <param name="id">XML config Id</param>
        /// <param name="viewOptions">view Options</param>
        /// <param name="pageName">Page Name</param>
        /// <param name="objectName">Object Name</param>
        /// <param name="columnListJson">column List Json</param>
        /// <returns>Returns Partial View</returns>
        public ActionResult SetColumnSettings(string columnNames = "", string viewMode = "", int id = 0, string viewOptions = "", string pageName = "", string objectName = "", string columnListJson = "")
        {
            WebGridViewModel gridViewModel = null;
            try
            {
                if (!string.IsNullOrEmpty(columnNames))
                {
                    int count = 0;
                    gridViewModel = new WebGridViewModel();

                    List<WebGridColumnModel> currentColumnList = null;
                    List<WebGridColumnModel> newColumnList = GetWebGridColumnList(columnNames); 
                    if (!string.IsNullOrEmpty(columnListJson) && !Equals(columnListJson, "null"))
                    {

                        currentColumnList = new List<WebGridColumnModel>();
                        currentColumnList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<WebGridColumnModel>>(columnListJson);

                        foreach (var _column in newColumnList)
                        {
                            if (!currentColumnList.Any(x => Equals(x.Name, _column.Name)))
                            {
                                currentColumnList.Add(_column);
                            }
                            int index = currentColumnList.FindIndex(x => Equals(x.Name, _column.Name));
                            var record = currentColumnList[index];
                            currentColumnList.RemoveAt(index);
                            currentColumnList.Insert(count, record);
                            count++;
                        }
                        List<WebGridColumnModel> changesColList = new List<WebGridColumnModel>();
                        changesColList.AddRange(currentColumnList);
                        foreach (var item in changesColList)
                        {
                            if (!newColumnList.Any(x => Equals(x.Name, item.Name)))
                            {
                                currentColumnList.Remove(item);
                            }
                        }

                        gridViewModel.WebGridColumnModelList = currentColumnList;
                    }
                    else
                    {
                        gridViewModel.WebGridColumnModelList = newColumnList;
                    }

                    gridViewModel.ViewMode = viewMode;
                    gridViewModel.Id = id;
                    gridViewModel.ViewOption = viewOptions;
                    gridViewModel.FrontPageName = pageName;
                    gridViewModel.FrontObjectName = objectName;
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex);
            }

            return PartialView(DynamicGridConstants.GridColumnSetting, gridViewModel);

        }

        /// <summary>
        /// View XML 
        /// </summary>
        /// <param name="id">Xml config Id</param>
        /// <returns>String XML</returns>
        public string View(int id)
        {
            FilterCollectionDataModel model = new FilterCollectionDataModel();
            model.Page = PageNo;
            model.RecordPerPage = RecoredPerPage;
            model.Filters = new FilterCollection();
            model.Filters.Add(new FilterTuple(Id, FilterOperators.Equals, id.ToString()));
            ApplicationSettingListViewModel modelList = _xmlGeneratorAgent.ApplicationsSettinglList(model);
            string xmlString = modelList.List[0].Setting.Trim();
            xmlString = xmlString.Replace(" ", "");
            return xmlString;
        }

        /// <summary>
        /// Get Xml data for Edit XML configuration
        /// </summary>
        /// <param name="id">Application Setting Id</param>
        /// <returns>Returns Create/Edit View</returns>
        public ActionResult Edit(int id)
        {
            WebGridViewModel webGridModel = new WebGridViewModel();

            FilterCollectionDataModel filterModel = new FilterCollectionDataModel();
            filterModel.Page = PageNo;
            filterModel.RecordPerPage = RecoredPerPage;
            filterModel.Filters = new FilterCollection();
            filterModel.Filters.Add(new FilterTuple(Id, FilterOperators.Equals, id.ToString()));
            var xmlData = _xmlGeneratorAgent.ApplicationsSettinglList(filterModel);
            string xmlString = xmlData.List[0].Setting.Trim();
            List<WebGridColumnModel> list = _xmlGeneratorAgent.GetListFromXMLString(xmlString);
            ListViewModel listModel = GetListViewModel(xmlData.List[0].GroupName, xmlData.List[0].ItemName, "", list);
            webGridModel.Id = id;
            webGridModel.WebGridColumnModelList = list;
            webGridModel.ViewMode = ViewMode.Edit.ToString();
            webGridModel.ViewOption = xmlData.List[0].ViewOptions;
            webGridModel.FrontPageName = xmlData.List[0].FrontPageName;
            webGridModel.FrontObjectName = xmlData.List[0].FrontObjectName;
            webGridModel.EntityType = xmlData.List[0].GroupName;
            webGridModel.EntityName = xmlData.List[0].ItemName;
            webGridModel.listViewModel = listModel;

            return View(DynamicGridConstants.CreateXMLView, webGridModel);
        }
        #endregion

        #region Private Methods

        /// <summary>
        /// Get list view model for display selected and unselected columns.
        /// </summary>
        /// <param name="entityType">Type of entity</param>
        /// <param name="entityName">Name of entity</param>
        /// <param name="columnListJson">column List Json</param>
        /// <param name="columnList"> column List</param>
        /// <returns>Returns ListViewModel</returns>
        private ListViewModel GetListViewModel(string entityType = "", string entityName = "", string columnListJson = null, List<WebGridColumnModel> columnList = null)
        {
            ListViewModel listModel = new ListViewModel();
            listModel.AssignedList = null;
            listModel.UnAssignedList = null;
            try
            {
                if (!string.IsNullOrEmpty(entityType) && !string.IsNullOrEmpty(entityName))
                    listModel.AssignedList = _xmlGeneratorAgent.GetEntityColumnList(entityType, entityName);

                if (!string.IsNullOrEmpty(columnListJson) && columnListJson != "null")
                {
                    columnList = new List<WebGridColumnModel>();
                    columnList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<WebGridColumnModel>>(columnListJson);
                }
                if (columnList != null)
                {
                    if (columnList.Count > 0)
                    {
                        IEnumerable<SelectListItem> entityNameList = columnList.Select(item => new SelectListItem()
                        {
                            Value = item.Name,
                            Text = item.Name
                        });
                        listModel.UnAssignedList = entityNameList;

                        if (listModel.AssignedList != null)
                        {
                            if (listModel.AssignedList.Count() > 0)
                            {
                                listModel.AssignedList = listModel.AssignedList.ToList().Select(X => X.Value).Except(entityNameList.ToList().Select(X => X.Value)).Select(item => new SelectListItem()
                                {
                                    Value = item,
                                    Text = item
                                });
                            }
                        }
                    }
                }
                if (listModel.AssignedList == null)
                    listModel.AssignedList = new List<SelectListItem>();
                if (listModel.UnAssignedList == null)
                    listModel.UnAssignedList = new List<SelectListItem>();
            }
            catch (Exception ex)
            {

            }
            return listModel;
        }

        /// <summary>
        /// Create Column list from string
        /// </summary>
        /// <param name="columnNames"></param>
        /// <returns></returns>
        private List<WebGridColumnModel> GetWebGridColumnList(string columnNames)
        {
            List<WebGridColumnModel> List = new List<WebGridColumnModel>();
            string[] columnNameList = columnNames.Split(',');
            foreach (string name in columnNameList)
            {
                WebGridColumnModel model = new WebGridColumnModel();
                model.Name = name;
                List.Add(model);
            }
            return List;
        }
        #endregion
    }
}