﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.Agents.IAgents;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Controllers
{
    public class DynamicGridTestController : BaseController
    {
        private readonly IPortalAgent _portalAgent;
        private readonly IDomainAgent _domainAgent;
        private readonly IReportsAgent _reportsAgent;

        public DynamicGridTestController()
        {
            _portalAgent = new PortalAgent();
            _domainAgent = new DomainAgent();
            _reportsAgent = new ReportsAgent();
        }

        // GET: DynamicGridTest
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Gets the main page of the store.
        /// </summary>
        /// <returns>View of the store's main page.</returns>
        public ActionResult List([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model)
        {
            //PortalListViewModel portals = _portalAgent.GetPortals(model.Filters, model.SortCollection, model.Page, model.RecordPerPage);
            //var gridModel = FilterHelpers.GetDynamicGridModel(model, portals.Portals, 0);
            //List<PortalViewModel> data = new List<PortalViewModel>();

            //if (Session["tempList"] == null)
            //{
            //    Session["tempList"] = portals.Portals;
            //    data.AddRange(portals.Portals);
            //}
            //else
            //{
            //    data.AddRange((List<PortalViewModel>)Session["tempList"]);
            //}
            //FilterHelpers.CreateDropdown(ref gridModel, data, model.Filters, "StoreName", "StoreName", "Store List");
          


            int reportno = 98;
           

          

            ReportDataViewModel reportmodel = new ReportDataViewModel();

            reportmodel = _reportsAgent.GetReportData(reportno, model.Filters, model.SortCollection, model.Page, model.RecordPerPage, 0);

            var gridModel = FilterHelpers.GetDynamicGridModel(model, reportmodel.ReportDataSet, reportno);

            gridModel.TotalRecordCount = reportmodel.TotalResults;

            gridModel.SubRecordFilterKeyName = "OrderID";
            gridModel.DelegateTypeName = "Znode.Engine.MvcAdmin.Agents.ReportsAgent";
            gridModel.DelegateTypeMethodName = "GetSubRecords";
            return ActionView("~/Views/Filter/TabTest.cshtml", gridModel);

            //return ActionView("~/Views/Filter/TabTest.cshtml", new ReportModel());
        }


        public ActionResult Edit(string id)
        {
            return RedirectToAction("List");
        }
    }
}