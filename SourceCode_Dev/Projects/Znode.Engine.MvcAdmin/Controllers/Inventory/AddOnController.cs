﻿using Resources;
using System;
using System.Web.Mvc;
using Znode.Engine.Api.Models.Enum;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Controllers.Inventory
{
    public class AddOnController : BaseController
    {
        #region Private Variables
        private readonly IAddOnAgent _addOnAgent;
        private readonly IAddOnValueAgent _addOnValueAgent;
        private string createEditView = MvcAdminConstants.CreateEditView;
        private string AddOnValueCreateEditView = MvcAdminConstants.AddOnValueCreateEditView;
        private string AddOnListView = MvcAdminConstants.ListView;
        private string AddOnManageView = MvcAdminConstants.ManageView;

        #endregion

        #region Constructor

        public AddOnController()
        {
            _addOnAgent = new AddOnAgent();
            _addOnValueAgent = new AddOnValueAgent();
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Get the list of AddOns
        /// </summary>
        /// <param name="model">FilterCollectionDataModel model</param>
        /// <returns>Returns view of AddOn list</returns>
        public ActionResult List([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model)
        {
            AddOnListViewModel addOns = _addOnAgent.GetAddOns(model.Filters, model.SortCollection, model.Page, model.RecordPerPage);
            var gridModel = FilterHelpers.GetDynamicGridModel(model, addOns.AddOns, Convert.ToInt32(ListType.vw_ZNodeSearchAddons));
            gridModel.TotalRecordCount = addOns.TotalResults;
            return ActionView(gridModel);
        }

        /// <summary>
        /// Delete AddOn
        /// </summary>
        /// <param name="id">int id</param>
        /// <returns>Returns to the AddOn list view</returns>
        [HttpPost]
        public JsonResult Delete(int id)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();
            if (id > 0)
            {
                status = _addOnAgent.DeleteAddOn(id, out message);
                message = status ? ZnodeResources.DeleteMessage : message;
            }
            return Json(new { sucess = status, message = message, id = id, isFadeOut = isFadeOut });
        }

        /// <summary>
        /// Create AddOn
        /// </summary>
        /// <returns>Returns Addon create view</returns>
        [HttpGet]
        public ActionResult Create()
        { 
            return View(createEditView, new AddOnViewModel());
        }

        /// <summary>
        /// Create AddOn
        /// </summary>
        /// <param name="model">AddOnViewModel model</param>
        /// <returns>Returns to the AddOn list view</returns>
        [HttpPost]
        public ActionResult Create(AddOnViewModel model)
        {
            if (ModelState.IsValid)
            {
                int addonId = 0;
                _addOnAgent.SetOutOfStockOption(model);
                TempData[MvcAdminConstants.Notifications] = _addOnAgent.Create(model, out addonId) ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.SaveErrorMessage, NotificationType.error);
                return RedirectToAction(AddOnManageView, new { id = addonId });
            }
            return View(createEditView, model);
        }

        /// <summary>
        /// Edit Addon
        /// </summary>
        /// <param name="addOnId">int addOnId</param>
        /// <returns>Return AddOn Edit view</returns>
        public ActionResult Edit(int addOnId)
        {
            if (addOnId > 0)
            {
                AddOnViewModel addOn = _addOnAgent.GetAddOn(addOnId);
                _addOnAgent.GetOutOfStockOption(addOn);
                return View(createEditView, addOn);
            }
            return RedirectToAction(AddOnListView);
        }

        /// <summary>
        /// Edit Addon
        /// </summary>
        /// <param name="model">AddOnViewModel model</param>
        /// <returns>Returns to the AddOn list view</returns>
        [HttpPost]
        public ActionResult Edit(AddOnViewModel model)
        {
            if (ModelState.IsValid)
            {
                _addOnAgent.SetOutOfStockOption(model);
                TempData[MvcAdminConstants.Notifications] = _addOnAgent.Update(model) ? GenerateNotificationMessages(ZnodeResources.UpdateMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.UpdateErrorMessage, NotificationType.error);
                return RedirectToAction(AddOnManageView, new { id = model.AddOnId });
            }
            return View(createEditView, model);
        }

        /// <summary>
        /// Manage Addon 
        /// </summary>
        /// <param name="id">int addOnId</param>
        /// <param name="model">Filter Collection DataModel</param>
        /// <returns>Returns to the AddOn manage view</returns>
        public ActionResult Manage(int id, [ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model)
        {
            if (id > 0)
            {
                AddOnViewModel addOn = _addOnAgent.GetAddOn(id);
                _addOnAgent.GetInventorySetting(addOn);
                var addOnValueList = _addOnValueAgent.GetAddOnValueByAddOnId(id, model.Filters, model.SortCollection, model.Page, model.RecordPerPage);
                var gridModel = FilterHelpers.GetDynamicGridModel(model, addOnValueList.AddOnValues, Convert.ToInt32(ListType.vw_ZNodeGetQtyReorderLverl));
                gridModel.TotalRecordCount = addOnValueList.TotalResults;
                addOn.ReportModel = gridModel;
                return ActionView(AddOnManageView, addOn);
            }
            return RedirectToAction(AddOnListView);
        }

        /// <summary>
        /// Delete AddOn value
        /// </summary>
        /// <param name="id">int id</param>
        /// <returns>Returns to the AddOn manage view</returns>
        public JsonResult DeleteAddOnValue(int id)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();
            if (!Equals(null, id))
            {
                status = _addOnValueAgent.Delete(id);
                message = status ? ZnodeResources.DeleteMessage : ZnodeResources.DeleteErrorMessage;
            }
            return Json(new { sucess = status, message = message, id = id, isFadeOut = isFadeOut });
        }

        /// <summary>
        /// Create AddOn value
        /// </summary>
        /// <param name="id">int id</param>
        /// <returns>Returns to the AddOn value create view</returns>
        public ActionResult CreateAddOnValue(int id)
        {
            AddOnValueViewModel model = new AddOnValueViewModel();
            model.AddOnId = id;
            _addOnValueAgent.BindDropdownValues(model);
            return View(AddOnValueCreateEditView, model);
        }

        /// <summary>
        /// Create AddOn value
        /// </summary>
        /// <param name="model">AddOnValueViewModel model</param>
        /// <returns>Returns to the AddOn manage view</returns>
        [HttpPost]
        public ActionResult CreateAddOnValue(AddOnValueViewModel model)
        {
            if (ModelState.IsValid)
            {
                _addOnValueAgent.SetRecurringBillingValues(model);
                TempData[MvcAdminConstants.Notifications] = _addOnValueAgent.Create(model) ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.SaveErrorMessage, NotificationType.error);
                return RedirectToAction(AddOnManageView, new { id = model.AddOnId });
            }
            _addOnValueAgent.BindDropdownValues(model);
            return View(AddOnValueCreateEditView, model);
        }

        /// <summary>
        /// Edit AddOn value
        /// </summary>
        /// <param name="addOnValueId">int addOnValueId</param>
        /// <returns>Returns to the AddOn value edit view</returns>
        public ActionResult EditAddOnValue(int id)
        {
            if (id > 0)
            {
                AddOnValueViewModel addOnValue = _addOnValueAgent.GetAddOnValue(id);
                _addOnValueAgent.BindDropdownValues(addOnValue);
                return View(AddOnValueCreateEditView, addOnValue);
            }
            return RedirectToAction(AddOnListView);
        }

        /// <summary>
        /// Edit AddOn value
        /// </summary>
        /// <param name="model">AddOnValueViewModel model</param>
        /// <returns>Returns to the AddOn manage view</returns>
        [HttpPost]
        public ActionResult EditAddOnValue(AddOnValueViewModel model)
        {
            if (ModelState.IsValid)
            {
                _addOnValueAgent.SetRecurringBillingValues(model);
                TempData[MvcAdminConstants.Notifications] = _addOnValueAgent.Update(model) ? GenerateNotificationMessages(ZnodeResources.UpdateMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.UpdateErrorMessage, NotificationType.error);
                return RedirectToAction(AddOnManageView, new { id = model.AddOnId });
            }
            _addOnValueAgent.BindDropdownValues(model);
            return View(AddOnValueCreateEditView, model);
        }
        #endregion
    }
}