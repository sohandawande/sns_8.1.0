﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Enum;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Controllers
{
    public partial class ProductController : BaseController
    {
        /// <summary>
        /// To show product Inventory from ERP
        /// </summary>
        /// <returns></returns>
        public ActionResult InventoryListFromERP([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model)
        {
            PRFTInventoryListViewModel inventories = _productAgent.GetInventoryListFromERP(model.Filters, model.SortCollection, model.Page, model.RecordPerPage);
            if (Equals(inventories, null))
            {
                return new EmptyResult();
            }

            ReportModel gridModel = FilterHelpers.GetDynamicGridModel(model, inventories.InventoryList, Convert.ToInt32(ListType.InventoryListFromErp));
            gridModel.TotalRecordCount = (inventories.TotalResults > 0) ? inventories.TotalResults : inventories.InventoryList.Count;
            return ActionView(gridModel);
        }
	}
}