﻿using Resources;
using System;
using System.Web.Mvc;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Models;
using Znode.Engine.MvcAdmin.ViewModels;
using Znode.Engine.Api.Models.Enum;

namespace Znode.Engine.MvcAdmin.Controllers
{
    /// <summary>
    /// This is the controller for the brands operations
    /// </summary>
    public class ManufacturersController : BaseController
    {
        #region Private Variables
        private readonly IManufacturersAgent _manufacturereAgent;
        #endregion

        #region Public Constructors

        /// <summary>
        /// Constructor for the Manufacturers
        /// </summary>
        public ManufacturersController()
        {
            _manufacturereAgent = new ManufacturersAgent();
        }

        #endregion        

        #region Action Methods

        /// <summary>
        /// This function will return the list of all the brands.
        /// </summary>
        /// <returns>ActionResult view of brand list</returns>
        public ActionResult List([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model)
        {

            //PRFT Custom Code :  Start
            //Redirect to category page as we used manufacturer as a category
            return RedirectToAction(MvcAdminConstants.ListView, MvcAdminConstants.category);
            #region ZNode code commented by PRFT
            //ManufacturersListViewModel list = _manufacturereAgent.GetManufacturers(model.Filters, model.SortCollection,model.Page, model.RecordPerPage);
            //var gridModel = FilterHelpers.GetDynamicGridModel(model, list.Manufacturers, Convert.ToInt32(ListType.ManufacturerList));
            //gridModel.TotalRecordCount = list.TotalResults;
            //return ActionView(gridModel);
            #endregion
            //PRFT Custom Code :  End
        }

        /// <summary>
        /// This method will use to add manufacturer
        /// </summary>
        /// <returns></returns>
        public ActionResult Create()
        {
            return View(MvcAdminConstants.CreateEditView, new ManufacturersViewModel());
        }

        /// <summary>
        /// This method will use to add manufacturer
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(ManufacturersViewModel model)
        {
            if(ModelState.IsValid)
            {
                bool status = _manufacturereAgent.SaveManufacturer(model);

                TempData[MvcAdminConstants.Notifications] = status ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.SaveErrorMessage, NotificationType.error);
                return RedirectToAction(MvcAdminConstants.ListView);
            }
            return View(MvcAdminConstants.CreateEditView, model);
        }

        /// <summary>
        /// This method will populate the manufacturer data to be edited
        /// </summary>
        /// <param name="id">integer Manufacturer ID</param>
        /// <returns>Returns the edit view of manufacturers</returns>
        public ActionResult Edit(int? id)
        {
            ManufacturersViewModel model = null;
            if (!Equals(id, null) && id > 0)
            {
                model = _manufacturereAgent.GetManufacturerByID(int.Parse(id.ToString()));                
            }
            return View(MvcAdminConstants.CreateEditView, model);
        }

        /// <summary>
        /// This method will update the data of Manufacturer
        /// </summary>
        /// <param name="model">ManufacturersViewModel view model</param>
        /// <returns>Returns back to the Manufacturer list view</returns>
        [HttpPost]
        public ActionResult Edit(ManufacturersViewModel model)
        {
            if (ModelState.IsValid)
            {
                bool status = _manufacturereAgent.UpdateManufacturer(model);
                TempData[MvcAdminConstants.Notifications] = status ? GenerateNotificationMessages(ZnodeResources.UpdateMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.UpdateErrorMessage, NotificationType.error);
            }
            return RedirectToAction(MvcAdminConstants.ListView);
        }

        /// <summary>
        /// This method will delete the manufacturer
        /// </summary>
        /// <param name="id">integer Manufacturer Id</param>
        /// <returns>Returns to the manufactureres list view</returns>
        [HttpPost]
        public JsonResult Delete(int? id)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();
            if (!Equals(null, id))
            {
                status = _manufacturereAgent.DeleteManufactuere(id.Value);
                message = status ? ZnodeResources.DeleteMessage : ZnodeResources.DeleteBrandErrorMessage;
            }
            return Json(new { sucess = status, message = message, id = id, isFadeOut = isFadeOut });
        }

        #endregion
    }
}