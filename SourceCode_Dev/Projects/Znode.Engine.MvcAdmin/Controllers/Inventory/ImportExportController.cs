﻿using Resources;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.Agents.IAgents;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.ViewModels;
using Znode.Libraries.Helpers.Constants;
using ZNode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.MvcAdmin.Controllers.Inventory
{
    public class ImportExportController : BaseController
    {
        #region Private Variables
        private readonly IImportExportAgent _importExportAgent;
        private const string ListAction = "List";
        private const string ImportAction = "Import";
        private const string ImportErrorPartialView = "_ImportError";
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public ImportExportController()
        {
            _importExportAgent = new ImportExportAgent();
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Get the Export Type List.
        /// </summary>
        /// <returns>Retrun Import/Export List View</returns>
        public ActionResult List()
        {
            return View();
        }

        /// <summary>
        /// To Get the Export Type View based on the filter.
        /// </summary>
        /// <param name="filter">Key for the Export Type</param>
        /// <returns>Return the Export View.</returns>
        public ActionResult Export(string filter = "")
        {
            ExportViewModel model = new ExportViewModel();
            if (!string.IsNullOrEmpty(filter))
            {
                model.Type = filter;
                //Gets the Default Export Details based on the Export Type.
                model = _importExportAgent.GetDefaultExportDetails(filter);
                return View(model);
            }
            return RedirectToAction(ListAction);

        }

        /// <summary>
        /// Get the Export details based on the filter criteria.
        /// </summary>
        /// <param name="model">Model of the ExportViewModel</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Export(ExportViewModel model)
        {
            //Get the Export Details.
            ExportViewModel exportmodel = _importExportAgent.GetExportData(model);

            //Check for the Error.
            if (!Equals(exportmodel, null) && exportmodel.HasError)
            {
                TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(exportmodel.ErrorMessage, Models.NotificationType.error);
            }
            if (!Equals(exportmodel, null) && !Equals(exportmodel.ExportDataSet, null))
            {
                DownloadHelper downloadHelper = new DownloadHelper();

                //Set the Export File Name
                string fileName = (Equals(model.FileType, Convert.ToInt32(FileTypes.Excel).ToString())) ? string.Format("{0}.xls", model.Type) : string.Format("{0}.csv", model.Type);
                bool hasControls = Equals(model.Type, Constant.ExportAttribute);
                //Download the Export Data in Specified File.
                downloadHelper.ExportDownload(exportmodel.ExportDataSet, model.FileType, Response, null, fileName, hasControls);
            }
            else
            {
                TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.ExportNoRecordFound, Models.NotificationType.info);
            }
            model.Type = model.Type;
            //Gets the Default Export Details based on the Export Type.
            int CatalogId = model.SelectedCatalogId;
            model = _importExportAgent.GetDefaultExportDetails(model.Type);
            if (CatalogId > 0)
                model.Categories = _importExportAgent.GetCategories(CatalogId);
            return View(model);
        }

        /// <summary>
        /// To Get the Attribute Types based on Catalog Id.
        /// </summary>
        /// <param name="catalogId">Id of the Catalog</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetAttributesTypesByCatalogId(int catalogId)
        {
            //Gets the List of Attribures Types baed on Catalog Id.
            List<SelectListItem> result = _importExportAgent.GetAttributeTypes(catalogId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// To Get the Categories based on Catalog Id.
        /// </summary>
        /// <param name="catalogId">Id of the Catalog</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetCategoriesByCatalogId(int catalogId)
        {
            //Gets the List of Catagories baed on Catalog Id.
            List<SelectListItem> result = _importExportAgent.GetCategories(catalogId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// To Get the Product Details based on Product Name & CatalogId
        /// </summary>
        /// <param name="searchText">Product Name</param>
        /// <param name="catalogId">Id of the Catalog</param>
        /// <returns></returns>
        public JsonResult AutoCompleteSearchProduct(string searchText, int catalogId)
        {
            try
            {
                //Gets the Product Details matches with search Text & Catalog Id.
                List<ProductViewModel> productDetails = _importExportAgent.GetProductDetailsByName(searchText, catalogId);
                return Json(productDetails, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

        }

        /// <summary>
        /// To Get the Import Type View based on the filter.
        /// </summary>
        /// <param name="filter">Key for the Import Type</param>
        /// <returns></returns>
        public ActionResult Import(string filter = "")
        {
            ImportViewModel model = new ImportViewModel();
            if (!string.IsNullOrEmpty(filter))
            {
                model = _importExportAgent.GetImportDefaultData(filter);
                model.Type = filter;
                return View(model);
            }
            return RedirectToAction(ListAction);
        }

        /// <summary>
        /// To Import the Data.
        /// </summary>
        /// <param name="model">Model of the ImportViewModel</param>
        /// <param name="command">Key to check type of Action </param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Import(ImportViewModel model)
        {
            //ZNodeLogging.LogMessage("Step1 - ImportExportController In MVC Admin");
            if (ModelState.IsValid)
            {
                //ZNodeLogging.LogMessage("Step2 - ImportExportController In MVC Admin - ModalState Is Valid and called to Agent");
                //Imports the Uploaded Data & Return the Import status Or Errors in ImportViewModel.
                ImportViewModel importModel = _importExportAgent.ImportData(model);
                //ZNodeLogging.LogMessage("Step25- ImportExportController In MVC Admin - Retruned from agent - "+ importModel.HasError);
                importModel.Type = model.Type;
                importModel.ImportConfigData = _importExportAgent.GetImportDefaultData(model.Type).ImportConfigData;
                TempData[MvcAdminConstants.Notifications] = (importModel.HasError)
                    ? (!string.IsNullOrEmpty(importModel.ErrorMessage)) ? GenerateNotificationMessages(importModel.ErrorMessage, Models.NotificationType.error) : GenerateNotificationMessages(ZnodeResources.ImportError, Models.NotificationType.error)
                    : (importModel.ImportStatus) ? GenerateNotificationMessages(ZnodeResources.ImportSuccess, Models.NotificationType.success) : GenerateNotificationMessages(ZnodeResources.ImportError, Models.NotificationType.error);
                return View(importModel);
            }
            model.ImportConfigData = _importExportAgent.GetImportDefaultData(model.Type).ImportConfigData;
            return View(model);
        }

        /// <summary>
        /// To Downlod Excel Template.
        /// </summary>
        /// <returns>Return Excel Template if present else return Error.</returns>
        public ActionResult DownloadSampleExcel(string type = "")
        {
            if (!string.IsNullOrEmpty(type))
            {
                // Set download path in string
                string path = _importExportAgent.GetImportTemplateFilePath(type);
                // Check whether the file exists or not
                if (FileOrDirectoryExists(Server.MapPath(path)))
                {
                    //Read the file content.
                    byte[] fileBytes = System.IO.File.ReadAllBytes(Server.MapPath(path));
                    //Downloads the File from the File Path.
                    return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, Path.GetFileName(path));
                }
                else
                {
                    TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.ImportTemplateNotFound, Models.NotificationType.info);
                    return RedirectToAction(ImportAction, new { @filter = type });
                }
            }
            return RedirectToAction(ListAction);
        }

        /// <summary>
        /// To Get Import File Preview.
        /// </summary>
        /// <param name="file">Uploaded File Data</param>
        /// <param name="type">Import File Type</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ImportPreview(HttpPostedFileBase file, string type)
        {
            ImportViewModel importModel = new ImportViewModel();
            importModel.ImportFile = file;
            importModel.Type = type;
            //Set the uploaded file data in List Format.
            importModel = _importExportAgent.PreviewImportData(importModel);
            if (!Equals(importModel.HasError, true))
            {
                return PartialView(ImportErrorPartialView, importModel);
            }
            else
            {
                return Json(new { ErrorMessage = importModel.ErrorMessage, HasError = importModel.HasError }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// To Check whether File or Directory exists.
        /// </summary>
        /// <param name="path">Path of the File.</param>
        /// <returns>Return true or false</returns>
        private bool FileOrDirectoryExists(string path)
        {
            return (Directory.Exists(path) || System.IO.File.Exists(path));
        }
        #endregion
    }
}