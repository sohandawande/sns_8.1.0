﻿using Resources;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Znode.Engine.Api.Models.Enum;
using Znode.Engine.Exceptions;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Controllers.Inventory
{
    /// <summary>
    /// Product Type Controller
    /// </summary>
    public class ProductTypeController : BaseController
    {
        #region Private Variables
        private readonly IProductTypeAgent _productTypeAgent;
        private readonly IProductTypeAttributeAgent _productTypeAttributeAgent;
        private readonly IAttributeTypesAgent _attributeTypeAgent;
        private string createEditView = MvcAdminConstants.CreateEditView;
        private string productTypeListAction = MvcAdminConstants.ListView;
        private const string attributeTypeList = "AttributesList";
        private const string listAction = "List";
        private const string addAttributeAction = "AddAttribute";
        #endregion

        #region Public Constructor
        /// <summary>
        /// Constructor for ProductTypeController.
        /// </summary>
        /// <returns></returns>
        public ProductTypeController()
        {
            _productTypeAgent = new ProductTypeAgent();
            _productTypeAttributeAgent = new ProductTypeAttributeAgent();
            _attributeTypeAgent = new AttributeTypesAgent();
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Get List of Product Types.
        /// </summary>
        /// <returns>Show Add new productType page</returns>
        public ActionResult List([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model)
        {
            ProductTypeListViewModel list = _productTypeAgent.GetProductTypes(model.Filters, model.SortCollection, model.Page, model.RecordPerPage);
            var gridModel = FilterHelpers.GetDynamicGridModel(model, !Equals(list, null) ? list.ProductTypes : new List<ProductTypeViewModel>(), (int)ListType.ProductTypeList);
            gridModel.TotalRecordCount = !Equals(list, null) ? list.TotalResults : 0;
            return ActionView(gridModel);
        }

        /// <summary>
        /// Create New Product Type.
        /// </summary>
        /// <returns>Show Create Edit View</returns>
        public ActionResult Create()
        {
            return View(createEditView);
        }

        /// <summary>
        /// To save Product Type details
        /// </summary>
        /// <param name="model">ProductTypesViewModel</param>
        /// <returns>show add new page</returns>
        [HttpPost]
        public ActionResult Create(ProductTypeViewModel viewModel)
        {
            string message = string.Empty;
            bool status = _productTypeAgent.CreateProductType(viewModel, out message);
            TempData[MvcAdminConstants.Notifications] = status
                ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success)
                : GenerateNotificationMessages(message, NotificationType.error);

            if (status)
            {
                return RedirectToAction(productTypeListAction);
            }
            return ActionView(createEditView, viewModel);

        }

        /// <summary>
        /// To Show edit page by productTypeId
        /// </summary>
        /// <param name="id">productTypeId</param>
        /// <returns>Edit Page</returns>
        [HttpGet]
        public ActionResult Edit(int? id)
        {
            if (!Equals(id, null))
            {
                ProductTypeViewModel viewModel = _productTypeAgent.GetProductType(id.Value);
                if (!Equals(viewModel.IsGiftCard, true))
                {
                    viewModel.OldProductTypeName = viewModel.Name;
                    if (Equals(viewModel, null))
                    {
                        return RedirectToAction(productTypeListAction);
                    }
                    return ActionView(createEditView, viewModel);
                }
                else
                {
                    TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.ErrorEditDefaultProductType, NotificationType.error);
                    return RedirectToAction(listAction);
                }
            }
            return View(productTypeListAction);
        }

        /// <summary>
        /// To update ProductTypes data
        /// </summary>
        /// <param name="model">ProductTypesViewModel model</param>
        /// <returns>show susscess/fail message and redirect to list page</returns>
        [HttpPost]
        public ActionResult Edit(ProductTypeViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                string message = string.Empty;
                bool status = _productTypeAgent.UpdateProductType(viewModel, out message);
                TempData[MvcAdminConstants.Notifications] = status
                    ? GenerateNotificationMessages(ZnodeResources.UpdateMessage, NotificationType.success)
                    : GenerateNotificationMessages(message, NotificationType.error);

                if (status)
                {
                    return RedirectToAction(productTypeListAction);
                }
                return ActionView(createEditView, viewModel);
            }
            return View(createEditView, viewModel);
        }

        /// <summary>
        /// Deletes an existing Product Type.
        /// </summary>
        /// <param name="id">nullable int productTypeId</param>
        /// <returns>Returns json and Success/Fail Message</returns>
        public JsonResult Delete(int? id)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();

            if (!Equals(id, null))
            {
                if (!Equals(_productTypeAgent.GetProductType(id.Value).IsGiftCard, true))
                {
                    //Check if Attribute type is associated with Product Type 
                    var associate = _productTypeAttributeAgent.IsAttributeAssociatedProductType(id.Value);
                    //if False, not allow to delete.
                    if (!associate)
                    {
                        status = _productTypeAgent.DeleteProductType(id.Value, out message);
                        message = status ? ZnodeResources.DeleteMessage : message;
                        return Json(new { sucess = status, message = message, id = id, isFadeOut = isFadeOut });
                    }
                    message = status ? ZnodeResources.DeleteMessage : ZnodeResources.ErrorAttributeAssociatedProductType;
                }
                else
                {
                    return Json(new { sucess = false, message = ZnodeResources.ErrorDeleteDefaultProductType, id = id, isFadeOut = isFadeOut });
                }
            }
            return Json(new { sucess = status, message = message, id = id, isFadeOut = isFadeOut });
        }

        #region Attribute Associated with Product Type Methods.

        /// <summary>
        /// Get List for Attributes associated with selected Product Type.
        /// </summary>
        /// <param name="model">FilterCollectionDataModel</param>
        /// <param name="id">Product type id</param>
        /// <returns>show susscess/fail message and redirect to list page</returns>
        public ActionResult AttributesList([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model, int? id)
        {
            ProductTypeViewModel productType = new ProductTypeViewModel();
            productType = _productTypeAgent.GetProductType(id.Value);
            if (!Equals(productType.IsGiftCard, true))
            {
                ProductTypeAssociatedAttributeTypesListViewModel attributeTypeList = null;
                if (Equals(productType.ProductTypeId, id))
                {
                    attributeTypeList = _productTypeAttributeAgent.GetAttributesByProductTypeId(id.Value, model.Filters, model.SortCollection, model.Page, model.RecordPerPage);
                    bool status = _productTypeAttributeAgent.IsProductAssociatedProductType(id.Value);

                    if (status)
                    {
                        productType.IsSuccess = true;
                    }
                }

                var gridModel = FilterHelpers.GetDynamicGridModel(model, attributeTypeList.AssociatedAttributes, (int)ListType.vw_GetAttributeTypesBy);
                gridModel.TotalRecordCount = (attributeTypeList.TotalResults > 0) ? attributeTypeList.TotalResults : attributeTypeList.AssociatedAttributes.Count;
                productType.reportModel = gridModel;
            }
            else
            {
                TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.ErrorAddAttributeForProductType, NotificationType.error);
                return RedirectToAction(listAction);
            }
            return ActionView(productType);
        }

        /// <summary>
        /// To show list of Attributes associated with Product Type.
        /// </summary>
        /// <param name="id">product type id</param>
        /// <returns>Show attribute list</returns>
        public ActionResult AddAttribute(int? id)
        {
            AttributeTypesViewModel attributeType = _attributeTypeAgent.GetAttributeTypeList();
            attributeType.ProductTypeId = id.Value;
            if (Equals(attributeType, null))
            {
                return new EmptyResult();
            }
            return View(MvcAdminConstants.AssociateAttribute, attributeType);
        }

        /// <summary>
        /// To Save Attribute Associated with Product Type.
        /// </summary>
        /// <param name="productTypeId">productTypeId</param>
        /// <param name="model">AttributeTypesViewModel</param>
        /// <returns>Attribute Dropdown list</returns>
        [HttpPost]
        public ActionResult AddAttribute(int? productTypeId, AttributeTypesViewModel model)
        {

            if (!Equals(model.AttributeTypeId, null))
            {
                string message = string.Empty;
                ProductTypeAssociatedAttributeTypesViewModel addAttributes = new ProductTypeAssociatedAttributeTypesViewModel();

                if (!Equals(productTypeId.Value, null))
                {
                    addAttributes.ProductTypeId = productTypeId.Value;
                    addAttributes.AttributeTypeId = model.AttributeTypeId;

                    bool status = _productTypeAttributeAgent.CreateProductType(addAttributes, out message);
                    TempData[MvcAdminConstants.Notifications] = status
                        ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success)
                        : GenerateNotificationMessages(message, NotificationType.error);

                    if (status)
                    {
                        return RedirectToAction(attributeTypeList, new { id = productTypeId });
                    }
                    return RedirectToAction(addAttributeAction, new { id = productTypeId });
                }
            }

            model = _attributeTypeAgent.GetAttributeTypeList();
            return View(MvcAdminConstants.AssociateAttribute, model);
        }

        /// <summary>
        /// To delete Attribute associated with Product Type Id.
        /// </summary>
        /// <param name="productAttributeTypeID">nullable int AttributeId</param>
        /// <param name="productTypeId">int attributeTypeId</param>
        /// <returns>show susscess/fail message and redirect to list page</returns>
        [HttpPost]
        public JsonResult DeleteAttribute(int? productAttributeTypeID, int productTypeId)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();

            if (!Equals(productAttributeTypeID, null))
            {
                //To Check is Product Type associated with Product.
                var associate = _productTypeAttributeAgent.IsProductAssociatedProductType(productTypeId);
                if (!associate)
                {
                    status = _productTypeAttributeAgent.DeleteAttributeType(productAttributeTypeID.Value);
                }
                message = status ? ZnodeResources.DeleteMessage : ZnodeResources.ErrorDeleteProductTypeAssociatedProduct;
            }
            return Json(new { sucess = status, message = message, id = productAttributeTypeID, isFadeOut = isFadeOut });
        }
        #endregion
        #endregion
    }
}