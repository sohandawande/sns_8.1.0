﻿using Resources;
using System;
using System.Web.Mvc;
using Znode.Engine.Api.Models.Enum;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Controllers.Inventory
{
    public class HighlightController : BaseController
    {
        #region Private Variables
        private string createEditView = MvcAdminConstants.CreateEditView;
        private string HighlightListAction = MvcAdminConstants.ListView;
        private readonly IHighlightAgent _highlightAgent;
        private int localeId = MvcAdminConstants.LocaleId;
        private string highlightTypeId = "HighlightTypeId";
        private string name = "Name";
        #endregion

        #region Constructor
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public HighlightController()
        {
            _highlightAgent = new HighlightAgent();
        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Gets List Of Highlight
        /// </summary>
        /// <returns>ActionResult</returns>
        public ActionResult List([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model)
        {
            HighlightsListViewModel highlights = _highlightAgent.GetHighlights(model.Filters, model.SortCollection, model.Page, model.RecordPerPage);

            ReportModel gridModel = FilterHelpers.GetDynamicGridModel(model, highlights.Highlights, Convert.ToInt32(ListType.HighlightList));
            FilterHelpers.CreateDropdown(ref gridModel, _highlightAgent.GetHighlightTypeList(), model.Filters, highlightTypeId, name, ZnodeResources.LabelHighlightType);

            gridModel.TotalRecordCount = highlights.TotalResults;

            return ActionView(gridModel);
        }

        /// <summary>
        /// Create highlight
        /// </summary>
        /// <returns>ActionResult</returns>
        public ActionResult Create()
        {
            HighlightViewModel model = new HighlightViewModel();

            model.HighlightTypeList = _highlightAgent.GetHighlightTypeList();
            model.IsActive = true;

            return View(createEditView, model);
        }

        /// <summary>
        /// Create Highlight
        /// </summary>
        /// <param name="model">HighlightViewModel model</param>
        /// <returns>ActionResult</returns>
        [HttpPost]
        public ActionResult Create(HighlightViewModel model, string ShowPopupFlag = null)
        {
            string errorMessage = string.Empty;
            if (ModelState.IsValid)
            {
                model.ShowPopup = !Equals(ShowPopupFlag, "0") ? true : false;
                _highlightAgent.SetHighlightProperty(model);
                bool status = _highlightAgent.CreateHighlight(model, out errorMessage);
                TempData[MvcAdminConstants.Notifications] = status ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success) : GenerateNotificationMessages(errorMessage, NotificationType.error);
            }
            return RedirectToAction(HighlightListAction);
        }

        /// <summary>
        /// Edit Existing Highlight
        /// </summary>
        /// <param name="id">int? highlightId</param>
        /// <returns>ActionResult</returns>
        [HttpGet]
        public ActionResult Edit(int? id)
        {
            HighlightViewModel model = new HighlightViewModel();
            if (!Equals(id, null) && id > 0)
            {
                model = _highlightAgent.GetHighlight(int.Parse(id.ToString()));
                model.HighlightTypeList = _highlightAgent.GetHighlightTypeList();
                model.IsEditMode = true;
            }
            return View(createEditView, model);
        }

        /// <summary>
        ///  Edit Existing Highlight
        /// </summary>
        /// <param name="model">HighlightViewModel model</param>
        /// <returns>ActionResult</returns>
        [HttpPost]
        public ActionResult Edit(HighlightViewModel model, string ShowPopupFlag = null)
        {
            string errorMessage = string.Empty;
            //Remove the server side validation.
            ModelState.Remove("HighlightImage");

            if (ModelState.IsValid)
            {
                model.ShowPopup = !Equals(ShowPopupFlag, "0") ? true : false;
                _highlightAgent.SetHighlightProperty(model);
                bool status = _highlightAgent.UpdateHighlight(model, out errorMessage);
                TempData[MvcAdminConstants.Notifications] = status ? GenerateNotificationMessages(ZnodeResources.UpdateMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.UpdateErrorMessage, NotificationType.error);
            }
            return RedirectToAction(HighlightListAction);
        }

        /// <summary>
        /// Delete existing highlight
        /// </summary>
        /// <param name="id">int highlightId</param>
        /// <returns>ActionResult</returns>
        [HttpPost]
        public JsonResult Delete(int? id)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();

            if (id.HasValue && id.Value > 0)
            {
                if (_highlightAgent.CheckAssociatedProduct(id.Value))
                {
                    message = ZnodeResources.DeleteHighlightErrorMessage;
                }
                else
                {
                    status = _highlightAgent.DeleteHighlight(id.Value, out message);
                    message = status ? ZnodeResources.DeleteMessage : ZnodeResources.DeleteErrorMessage;
                }
            }
            return Json(new { sucess = status, message = message, id = id, isFadeOut = isFadeOut });
        }

        #endregion
    }
}