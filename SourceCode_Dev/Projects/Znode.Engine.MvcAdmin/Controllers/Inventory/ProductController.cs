﻿using Resources;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Enum;
using Znode.Engine.Exceptions;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Controllers
{
    public partial class ProductController : BaseController
    {        
        #region Private Variables
        private readonly IProductAgent _productAgent;
        private readonly ICategoryAgent _categoryAgent;
        private readonly ICustomerBasedPricingAgent _CustomerBasedPricingAgent;
        private readonly IProfilesAgent _profilesAgent;
        private readonly IHighlightAgent _highlightAgent;
        private readonly ISearchAgent _searchAgent;

        private string createEditView = MvcAdminConstants.CreateEditView;

        private string ProductTagsPartialView = "_ProductTags";
        private string ProductFacetsPartialView = "_ProductFacets";
        private string ProductAssociateFacetsPartialView = "_AssociatedFacets";
        private string ProductFacetsCheckBoxListPartialView = "_FacetCheckBoxList";
        private string ProductSettingsPartialView = "_ProductSettings";
        private string EditSettingsPartialView = "_EditProductSettings";
        private string ProductAlternateImageListPartialView = "_ProductAlternateImageList";
        private string AddAlternateProductImagePartialView = "_AddAlternateProductImage";
        private string ProductCategoryListPartialView = "_ProductCategoryList";
        private string AssociateCategoriesListPartialView = "_AssociateCategoriesList";
        private string ProductAssociateBundleListPartialView = "_ProductAssociateBundleList";
        private string AddBundleProductPartialView = "_AddBundleProduct";
        private string ProductSkuListPartialView = "_ProductSkuList";
        private string createEditSkuView = "CreateEditSku";
        private string ProductAddOnListView = "_ProductAddOnList";
        private string ProductAssociateSKUFacetsPartialView = "_AssociateSKUFacets";
        private string SkuFacetsPartialView = "_SkuFacetsList";
        private string AddEditTierPriceView = "_AddEditTierPrice";
        private string AddDigitalAssetView = "_AddDigitalAsset";
        private string DigitalAssetListView = "_DigitalAssetList";

        private const string CustomerPricingView = "_GetCustomerBasedPricingList";
        private const string giftCardType = "Gift Card Type";
        private const string expirationPeriod = "ExpirationPeriod";

        private string UnassociatedAddOnListView = "_UnassociatedAddOnList";
        private string TierPricingListView = "_TierPricingList";
        private string ProductHighlightsListView = "_ProductHighlights";
        private string UnassociatedHighlightsView = "_UnassociatedHighlights";
        private string skuProfileCreateEditView = "_CreateEditSkuProfile";
        private string skuProfileListView = "SkuProfileList";
        private string customerPricingFileName = "CustomerPricing.csv";
        private string UnassociatedAddOnsAction = "UnassociatedAddOns";
        private string productControllerName = "Product";
        private const string shippingRateError = "ShippingRate";
        private const string vendorRetailtPriceError = "VendorRetailPrice";

        private const string case100 = "100";
        private const string case101 = "101";
        private const string case102 = "102";

        #endregion

        #region Constructor
        public ProductController()
        {
            _productAgent = new ProductAgent();
            _categoryAgent = new CategoryAgent();
            _CustomerBasedPricingAgent = new CustomerBasedPricingAgent();
            _profilesAgent = new ProfilesAgent();
            _highlightAgent = new HighlightAgent();
            _searchAgent = new SearchAgent();
        }
        #endregion

        #region Product Details

        /// <summary>
        /// To show product List
        /// </summary>
        /// <returns></returns>
        public ActionResult List([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model)
        {
            ProductListViewModel products = _productAgent.GetProducts(model.Filters, model.SortCollection, model.Page, model.RecordPerPage);
            if (Equals(products, null))
            {
                return new EmptyResult();
            }

            List<ManufacturerModel> manufacturerType = _productAgent.GetManufacturer();

            var gridModel = FilterHelpers.GetDynamicGridModel(model, products.Products, (int)ListType.ProductsList);

            #region Dynamic Dropdown Methods
            FilterHelpers.CreateDropdown(ref gridModel, _productAgent.Getcatalog(), model.Filters, MvcAdminConstants.LabelCatalogId, MvcAdminConstants.LabelName, MvcAdminConstants.LabelCatalog);
            manufacturerType.Insert(0, new ManufacturerModel { Name = MvcAdminConstants.NotApplicable });
            FilterHelpers.CreateDropdown(ref gridModel, manufacturerType, model.Filters, MvcAdminConstants.ManufacturerId, MvcAdminConstants.LabelName, MvcAdminConstants.Brand);
            FilterHelpers.CreateDropdown(ref gridModel, _productAgent.GetProductType(), model.Filters, MvcAdminConstants.productTypeId, MvcAdminConstants.LabelName, MvcAdminConstants.ProductTypeDropdownName);
            FilterHelpers.CreateDropdown(ref gridModel, _productAgent.GetCategories(), model.Filters, MvcAdminConstants.LabelCategoryId, MvcAdminConstants.LabelName, MvcAdminConstants.ProductCategoryDropdownName);
            #endregion

            gridModel.TotalRecordCount = (products.TotalResults > 0) ? products.TotalResults : products.Products.Count;
            return ActionView(gridModel);
        }

        /// <summary>
        /// Get Product Category by Catalog id
        /// </summary>
        /// <param name="catalogId">catalogId</param>
        /// <returns>categoryview model json result </returns>
        [HttpPost]
        public JsonResult GetCategoryList(int catalogId)
        {

            CategoryListViewModel productCatalogs = _categoryAgent.GetCategoryByCatalogId(catalogId);
            if (Equals(productCatalogs, null) || Equals(productCatalogs.Categories, null))
            {
                return Json(new List<CategoryViewModel>(), JsonRequestBehavior.AllowGet);
            }
            return Json(productCatalogs.Categories, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// To show add new product detail page
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Create()
        {
            ProductViewModel model = new ProductViewModel();
            _productAgent.BindProductPageDropdown(model, PortalId.Value);
            model.ShippingRate = 0;
            model.TaxClassId = 1;
            return View(createEditView, model);
        }

        /// <summary>
        /// To save product details
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(ProductViewModel model)
        {
            ModelState.Remove(vendorRetailtPriceError);
            ModelState.Remove(shippingRateError);
            if (ModelState.IsValid)
            {
                string errorMessage = string.Empty;
                bool status = _productAgent.SaveProduct(model, out errorMessage);
                if (!status)
                {
                    TempData[MvcAdminConstants.Notifications] = string.IsNullOrEmpty(errorMessage) ? GenerateNotificationMessages(ZnodeResources.SaveErrorMessage, NotificationType.error) : GenerateNotificationMessages(errorMessage, NotificationType.error);
                    _productAgent.BindProductPageDropdown(model, PortalId.Value);
                    model.IsEditMode = true;
                    model.IsInvalidMode = true;
                    return View(createEditView, model);
                }
                TempData[MvcAdminConstants.Notifications] = status ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.SaveErrorMessage, NotificationType.error);
            }
            else
            {              
                _productAgent.BindProductPageDropdown(model, PortalId.Value);   
                return View(createEditView, model);
            }
            return RedirectToAction(MvcAdminConstants.ManageView, new { id = model.ProductId });
        }

        /// <summary>
        /// To Edit product details
        /// </summary>
        /// <param name="id">int product id</param>
        /// <returns>returns edit product view</returns>
        [HttpGet]
        public ActionResult Edit(int? id)
        {
            if (!Equals(id, null))
            {
                ProductViewModel model = _productAgent.GetProduct(id.Value);                
                _productAgent.BindProductPageDropdown(model, PortalId.Value);
                model.IsEditMode = true;
                return View(createEditView, model);
            }
            return View(createEditView);
        }

        /// <summary>
        /// To Edit product
        /// </summary>
        /// <param name="model">ProductViewModel</param>
        /// <returns>returns product list page</returns>
        [HttpPost]
        public ActionResult Edit(ProductViewModel model)
        {
            ModelState.Remove(shippingRateError);
            ModelState.Remove(vendorRetailtPriceError);
            string producTypeName = string.Empty;
            if (!model.ProductTypeName.Equals(giftCardType, StringComparison.InvariantCultureIgnoreCase))
            {
                ModelState.Remove(expirationPeriod);
            }
            if (ModelState.IsValid)
            {               
                string message = string.Empty;
                bool status = _productAgent.UpdateProduct(model, out message);
                if (!status)
                {
                    TempData[MvcAdminConstants.Notifications] = status ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success) : GenerateNotificationMessages(message, NotificationType.error);
                    _productAgent.BindProductPageDropdown(model, PortalId.Value);
                    model.IsEditMode = true;
                    model.IsInvalidMode = true;
                    return View(createEditView, model);
                }
                else
                {
                    TempData[MvcAdminConstants.Notifications] = status ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.SaveErrorMessage, NotificationType.error);
                    return RedirectToAction(MvcAdminConstants.ManageView, new { id = model.ProductId });
                }
            }
            else
            {
                TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.SaveErrorMessage, NotificationType.error);
                _productAgent.BindProductPageDropdown(model, PortalId.Value);
                model.IsEditMode = true;
                model.IsInvalidMode = true;
                return View(createEditView, model);
            }
        }

        /// <summary>
        /// To edit product details
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Manage(int? id, string tabMode = "")
        {

            if (!string.IsNullOrEmpty(tabMode))
            {
                TempData[MvcAdminConstants.SetTabMode] = tabMode;
            }
            if (!Equals(id, null))
            {
                ProductViewModel model = _productAgent.GetProduct(id.Value);
                model.EnableCustomerPricing = (EnableCustomerPricing.HasValue) ? Convert.ToBoolean(EnableCustomerPricing) : false;
                Session[MvcAdminConstants.ProductName] = model.Name;
                return View(MvcAdminConstants.ManageView, model);
            }
            return View(MvcAdminConstants.ManageView);
        }

        /// <summary>
        /// To get Product Attributes list by ProductTypeId
        /// </summary>
        /// <param name="productTypeId">int ProductTypeId</param>
        /// <returns>json collection of attributes list and its values</returns>       
        public JsonResult GetProductAttributesByProductTypeId(int? productTypeId)
        {
            var result = _productAgent.GetProductAttributesByProductTypeId(productTypeId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Copies a Product.
        /// </summary>
        /// <param name="productId">Product Id of the product to be copied.</param>
        /// <returns>Manage view.</returns>
        public ActionResult CopyProduct(int id = 0)
        {
            ProductViewModel model = _productAgent.CopyProduct(id);
            return View(MvcAdminConstants.ManageView, model);
        }

        /// <summary>
        /// Deletes the specified Product.
        /// </summary>
        /// <param name="portalId">Product id</param>
        /// <returns>Redirects to list view with deleted product.</returns>
        public JsonResult Delete(int id)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();

            if (!id.Equals(0))
            {
                status = _productAgent.DeleteProduct(id, out message);
                if (!string.IsNullOrEmpty(message))
                {
                    message = status ? ZnodeResources.DeleteMessage : message;
                }
                else
                {
                    message = status ? ZnodeResources.DeleteMessage : "There was an error in deleting the product.";
                }
            }
            return Json(new { sucess = status, message = message, id = id, isFadeOut = isFadeOut });
        }

        #endregion

        #region Product Settings
        /// <summary>
        /// To get Product Settings in view mode
        /// </summary>
        /// <param name="productId">int productId</param>
        /// <returns>returns Product Settings view </returns>
        [HttpGet]
        public ActionResult GetProductSettings(int productId)
        {
            ProductViewModel model = _productAgent.GetProduct(productId);
            _productAgent.SetInventorySettings(model);
            if (Equals(model.ProductId, 0))
            {
                model.ProductId = productId;
            }
            return ActionView(ProductSettingsPartialView, model);
        }

        /// <summary>
        ///  To Edit Product Settings 
        /// </summary>
        /// <param name="productId">int productId</param>
        /// <returns>returns Edit Product Settings page</returns>
        [HttpGet]
        public ActionResult EditProductSettings(int productId)
        {
            ProductViewModel model = _productAgent.GetProduct(productId);
            model.ExpirationFrequencyList = _productAgent.GetDurationList();
            _productAgent.SetInventorySettings(model);
            if (Equals(model.ProductId, 0))
            {
                model.ProductId = productId;
            }
            return ActionView(EditSettingsPartialView, model);
        }

        [HttpPost]
        public ActionResult EditProductSettings(ProductViewModel model)
        {
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();
            if (!string.IsNullOrEmpty(model.SeoPageUrl) && _searchAgent.IsRestrictedSeoUrl(model.SeoPageUrl))
            {
                model.ExpirationFrequencyList = _productAgent.GetDurationList();
                return ProductSettingHandler(ZnodeResources.ErrorRestrictedSeoUrlExists, model, EditSettingsPartialView, false);
            }
            bool isSeoUrlExist = _productAgent.IsSeoUrlExist(model.ProductId, model.SeoPageUrl);
            if (isSeoUrlExist)
            {
                model.ExpirationFrequencyList = _productAgent.GetDurationList();
                return ProductSettingHandler(ZnodeResources.ErrorSeoUrlAlreadyExist, model, EditSettingsPartialView, false);
            }

            bool status = _productAgent.UpdateProductSetting(model);
            message = status ? ZnodeResources.UpdateMessage : ZnodeResources.UpdateErrorMessage;
            return ProductSettingHandler(message, model, ProductSettingsPartialView, status);
        }
        #endregion

        #region Product Category

        /// <summary>
        /// To get Product Category by productId
        /// </summary>
        /// <param name="productId">int productId</param>
        /// <returns>returns list of associated caltegory</returns>
        public ActionResult GetProductCategory([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel formModel)
        {
            int productId = _productAgent.GetProductIdFromFormCollection(formModel);
            if (!Equals(productId, 0))
            {
                ProductCategoryListViewModel model = new ProductCategoryListViewModel();
                model.CategoryList = _productAgent.GetProductCategoriesByProductId(productId, 0, formModel.Filters, formModel.SortCollection, formModel.Page, formModel.RecordPerPage);
                if (!Equals(model, null))
                {
                    model.ProductId = productId;

                    model.GridModel = FilterHelpers.GetDynamicGridModel(formModel, model.CategoryList.Categories, Convert.ToInt32(ListType.vw_ZNodeGetProductCategoryByProductID));
                    model.GridModel.TotalRecordCount = model.CategoryList.TotalResults;
                    return ActionView(ProductCategoryListPartialView, model);
                }
            }
            return ActionView(ProductCategoryListPartialView, new ProductCategoryListViewModel() { ProductId = productId });
        }

        /// <summary>
        /// To get list of category that can be assosiated to product 
        /// </summary>
        /// <param name="productId">int productId</param>
        /// <returns>returns list of all category</returns>
        public ActionResult GetUnAssociateCategories([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel formModel)
        {
            int productId = _productAgent.GetProductIdFromFormCollection(formModel);
            ProductCategoryListViewModel model = new ProductCategoryListViewModel();

            if (!Equals(productId, 0))
            {
                model.CategoryList = _productAgent.GetProductUnAssociatedCategoryByProductId(productId, formModel.Filters, formModel.SortCollection, formModel.Page, formModel.RecordPerPage);
                if (!Equals(model, null))
                {
                    model.ProductId = productId;

                    var gridModel = FilterHelpers.GetDynamicGridModel(formModel, model.CategoryList.Categories, Convert.ToInt32(ListType.vw_ZNodeGetProductCategoryNonAssociated));
                    model.GridModel = gridModel;
                    model.GridModel.TotalRecordCount = (model.CategoryList.TotalResults > 0) ? model.CategoryList.TotalResults : model.CategoryList.Categories.Count;
                    model.ProductName = Convert.ToString(Session[MvcAdminConstants.ProductName]);
                    return ActionView(AssociateCategoriesListPartialView, model);
                }
            }
            return ActionView(AssociateCategoriesListPartialView, new ProductCategoryListViewModel() { ProductId = productId });
        }

        /// <summary>
        /// To delete product category
        /// </summary>
        /// <param name="id">int? id</param>
        /// <returns>returns true/false</returns>
        [HttpPost]
        public JsonResult DeleteProductCategory(int? CategoryId, int ProductId)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();
            if (!Equals(null, CategoryId))
            {
                CategoryViewModel categoryModel = new CategoryViewModel();
                categoryModel.ProductId = ProductId;
                categoryModel.CategoryId = CategoryId.Value;
                status = _productAgent.UnAssociateProductCategory(categoryModel);
                message = status ? ZnodeResources.DeleteMessage : ZnodeResources.DeleteErrorMessage;
            }
            return Json(new { sucess = status, message = message, id = CategoryId, isFadeOut = isFadeOut });
        }

        /// <summary>
        /// To Associate Product Categories
        /// </summary>
        /// <param name="id">int id - product Id</param>
        /// <param name="categoryIds">string categoryIds</param>
        /// <returns> returns true /false</returns>
        [HttpPost]
        public JsonResult AssociateProductCategories(int id, string categoryIds)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();

            if (!Equals(id, null))
            {
                CategoryViewModel viewModel = new CategoryViewModel();
                viewModel.ProductId = id;
                viewModel.CategoryIds = categoryIds;
                status = _productAgent.AssociateProductCategory(viewModel);
                message = status ? ZnodeResources.SaveMessage : ZnodeResources.SaveErrorMessage;
                TempData[MvcAdminConstants.Notifications] = status ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.SaveErrorMessage, NotificationType.error);
            }
            TempData[MvcAdminConstants.SetTabMode] = Convert.ToString(SelectedTab.Category);
            return Json(new { sucess = status, message = message, id = id, isFadeOut = isFadeOut });
        }

        #endregion

        #region Product Sku

        /// <summary>
        /// To show product skus list
        /// </summary>
        /// <param name="formModel">FilterCollectionDataModel formModel</param>
        /// <returns>returns product sku list</returns>
        public ActionResult GetProductSku([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel formModel)
        {
            int productId = _productAgent.GetProductIdFromFormCollection(formModel);
            ProductSkuListViewModel model = new ProductSkuListViewModel();
            if (!Equals(productId, 0))
            {
                model.SkuList = _productAgent.GetProductSkuByProductId(productId, formModel.Filters, formModel.SortCollection, formModel.Page, formModel.RecordPerPage);
                if (!Equals(model, null))
                {
                    ProductViewModel productModel = _productAgent.GetProduct(productId);

                    model.AttributeCount = productModel.AttributeCount;
                    model.ParentProduct = productModel.ParentProduct.GetValueOrDefault();
                    model.IsGiftCard = productModel.IsGiftCard;

                    model.ProductId = productId;
                    var gridModel = FilterHelpers.GetDynamicGridModel(formModel, model.SkuList.Skus, Convert.ToInt32(ListType.vw_ZNodeProductSKUDetails));
                    model.GridModel = gridModel;
                    model.GridModel.TotalRecordCount = model.SkuList.TotalResults;
                    return ActionView(ProductSkuListPartialView, model);
                }
            }
            return ActionView(ProductSkuListPartialView, new ProductSkuListViewModel() { ProductId = productId });
        }

        /// <summary>
        /// To show Add sku page 
        /// </summary>
        /// <param name="productId">int productId</param>
        /// <returns>returns Add Sku page</returns>
        [HttpGet]
        public ActionResult CreateSku(int? productId)
        {
            if (!Equals(productId, null))
            {
                int _productId = productId.GetValueOrDefault();
                SkuViewModel model = _productAgent.GetProductSkuInfo(_productId);
                model.ProductId = _productId;
                model.IsActive = true;
                _productAgent.BindProductSkuDropdown(model);
                TempData[MvcAdminConstants.ReturnUrl] = model.ReturnUrl;
                return View(createEditSkuView, model);
            }
            else if (!Equals(TempData[MvcAdminConstants.ReturnUrl], null))
            {
                return Redirect(HttpUtility.UrlDecode(Convert.ToString(TempData[MvcAdminConstants.ReturnUrl])));
            }
            else
            {
                return RedirectToAction(MvcAdminConstants.ListView);
            }
        }

        /// <summary>
        /// To save sku deatils in database
        /// </summary>
        /// <param name="model">SkuViewModel model</param>
        /// <returns>returns true/false </returns>
        [HttpPost]
        public ActionResult CreateSku(SkuViewModel model)
        {
            string status = string.Empty;
            if (ModelState.IsValid)
            {
                status = _productAgent.SaveProductSku(model);
                string message = string.Empty;
                switch (status)
                {
                    case case100:
                        message = GenerateNotificationMessages(ZnodeResources.SaveErrorMessage, NotificationType.error);
                        break;
                    case case101:
                        message = GenerateNotificationMessages(ZnodeResources.ErrorSkuAlreadyExist, NotificationType.error);
                        break;
                    case case102:
                        message = GenerateNotificationMessages(ZnodeResources.SkuCombinationExist, NotificationType.error);
                        break;
                    default:
                        message = GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success);
                        break;
                }
                TempData[MvcAdminConstants.Notifications] = message;
            }
            if (string.IsNullOrEmpty(status))
            {
                return RedirectToAction(MvcAdminConstants.ManageView, new { id = model.ProductId, tabMode = Convert.ToString(SelectedTab.Skus) });
            }
            else
            {
                _productAgent.BindProductSkuDropdown(model);
                return View(createEditSkuView, model);
            }
        }

        /// <summary>
        /// To show edit sku page
        /// </summary>
        /// <param name="skuId">int skuId</param>
        /// <returns>returns sku edit page</returns>
        [HttpGet]
        public ActionResult EditSku(int id)
        {
            SkuViewModel model = _productAgent.GetProductSkuDetailsBySkuId(id);            
            _productAgent.BindProductSkuDropdown(model);
            return View(createEditSkuView, model);
        }

        /// <summary>
        /// To edit sku
        /// </summary>
        /// <param name="model">SkuViewModel model</param>
        /// <returns>returns sku list page</returns>
        [HttpPost]
        public ActionResult EditSku(SkuViewModel model)
        {
            //PRFT Custom Code : Start
            if (ModelState.ContainsKey("Custom1"))
                ModelState["Custom1"].Errors.Clear(); //Removing  “Illegal characters in path” error
            //PRFT Custom Code : End
            string status = string.Empty;
            if (ModelState.IsValid)
            {                
                if (model.ProductTypeName.ToLower().Equals("default") || model.ProductTypeName.ToLower().Equals("gift card type"))
                {
                    model.IsActive = true;
                }
                _productAgent.SetNoImageForSkuProperty(model);
                status = _productAgent.UpdateProductSku(model);
                string message = string.Empty;
                if (status.Length > 10)
                {
                    message = GenerateNotificationMessages(status, NotificationType.error);
                }
                else
                {
                    switch (status)
                    {
                        case case100:
                            message = GenerateNotificationMessages(ZnodeResources.UpdateErrorMessage, NotificationType.error);
                            break;
                        case case101:
                            message = GenerateNotificationMessages(ZnodeResources.ErrorSkuAlreadyExist, NotificationType.error);
                            break;
                        case case102:
                            message = GenerateNotificationMessages(ZnodeResources.SkuCombinationExist, NotificationType.error);
                            break;
                        default:
                            message = GenerateNotificationMessages(ZnodeResources.UpdateMessage, NotificationType.success);
                            break;
                    }
                }
                TempData[MvcAdminConstants.Notifications] = message;
            }
            if (string.IsNullOrEmpty(status))
            {
                return RedirectToAction(MvcAdminConstants.ManageView, new { id = model.ProductId, tabMode = Convert.ToString(SelectedTab.Skus) });
            }
            else
            {
                _productAgent.BindProductSkuDropdown(model);
                return View(createEditSkuView, model);
            }
        }

        /// <summary>
        /// To delete sku by SkuId 
        /// </summary>
        /// <param name="id">int? id</param>
        /// <returns>returns true/false</returns>
        [HttpPost]
        public JsonResult DeleteSku(int? id)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();
            if (!Equals(null, id))
            {
                string errorMessage = string.Empty;
                status = _productAgent.DeleteSku(id.Value, out errorMessage);
                message = !string.IsNullOrEmpty(errorMessage) ? errorMessage : status ? ZnodeResources.DeleteMessage : ZnodeResources.DeleteErrorMessage;
            }
            return Json(new { sucess = status, message = message, id = id, isFadeOut = isFadeOut });
        }

        #endregion

        #region Product SKU Facets

        /// <summary>
        /// To get Sku associated facets list by sku id
        /// </summary>
        /// <param name="formModel">FilterCollectionDataModel formModel</param>
        /// <returns>returns Sku associate facets list</returns>       
        public ActionResult GetSKUFacetsList([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel formModel)
        {
            int productId = _productAgent.GetProductIdFromFormCollection(formModel);
            int skuId = _productAgent.GetSkuIdFromFormCollection(formModel);
            ProductFacetListViewModel model = _productAgent.GetSKUFacets(skuId, formModel.Filters, formModel.SortCollection, formModel.Page, formModel.RecordPerPage);
            if (!Equals(model, null))
            {
                model.GridModel = FilterHelpers.GetDynamicGridModel(formModel, model.Facets, (int)ListType.SkuFacetsList);
                model.GridModel.TotalRecordCount = (model.TotalResults > 0) ? model.TotalResults : model.Facets.Count;
                model.ProductId = productId;
                model.SKUId = skuId;
                return ActionView(SkuFacetsPartialView, model);
            }
            return ActionView(SkuFacetsPartialView, new ProductFacetListViewModel() { ProductId = productId });
        }

        /// <summary>
        /// To get facets list of product for add new record
        /// </summary>
        /// <param name="productId">int productId</param>
        /// <param name="skuId"> int skuId</param>
        /// <returns>returns Sku associate facets list</returns>
        [HttpGet]
        public ActionResult GetAssociateSKUFacets(int productId, int skuId)
        {
            ProductFacetsViewModel model = _productAgent.GetAssociatedFacetGroup(productId);
            if (!Equals(model, null))
            {
                model.ProductId = productId;
                model.SKUId = skuId;
                return ActionView(ProductAssociateSKUFacetsPartialView, model);
            }
            return ActionView(ProductAssociateSKUFacetsPartialView, new ProductFacetsViewModel());
        }

        /// <summary>
        /// To save sku facets
        /// </summary>
        /// <param name="id">int id Skuid</param>
        /// <param name="associatedIds">string associatedIds</param>
        /// <param name="unAssociatedIds">string unAssociatedIds</param>
        /// <returns>returns true /false</returns>
        [HttpPost]
        public JsonResult AssociateSKUFacets(int id, string associatedIds, string unAssociatedIds)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();
            if (!Equals(id, null))
            {
                status = _productAgent.AssociateSkuFacets(id, associatedIds, unAssociatedIds);
                message = status ? ZnodeResources.SaveMessage : ZnodeResources.SaveErrorMessage;
                TempData[MvcAdminConstants.Notifications] = status ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.SaveErrorMessage, NotificationType.error);
            }
            TempData[MvcAdminConstants.SetTabMode] = Convert.ToString(SelectedTab.Category);
            return Json(new { sucess = status, message = message, id = id, isFadeOut = isFadeOut });
        }

        /// <summary>
        /// To get FacetGroupId by Sku Id
        /// </summary>
        /// <param name="productId">int productId</param>
        /// <param name="skuId"> int skuId</param>
        /// <param name="facetGroupId">int facetGroupId</param>
        /// <returns>returns list of Facets</returns>
        [HttpGet]
        public ActionResult GetFacetsBySKUFacetGroupId(int productId, int skuId, int facetGroupId)
        {
            ProductFacetsViewModel model = new ProductFacetsViewModel();
            if (productId > 0 && skuId > 0 && facetGroupId > 0)
            {
                model = _productAgent.GetSKUAssociatedFacetGroup(productId, skuId, facetGroupId);
            }
            return ActionView(ProductFacetsCheckBoxListPartialView, model);
        }

        /// <summary>
        /// To edit associated facet of sku
        /// </summary>
        /// <param name="id">int id - facetgroupId</param>
        /// <param name="productId">int productId</param>
        /// <param name="skuId">int skuId</param>
        /// <returns>returns edit sku facets</returns>
        [HttpGet]
        public ActionResult EditSkuAssociatedFacet(int id, int productId, int skuId)
        {
            if (id > 0 && productId > 0 && skuId > 0)
            {
                ProductFacetsViewModel model = _productAgent.GetSKUAssociatedFacetGroup(productId, skuId, id);
                if (!Equals(model, null))
                {
                    model.ProductId = productId;
                    model.FacetGroupId = id;
                    model.SKUId = skuId;
                    return ActionView(ProductAssociateSKUFacetsPartialView, model);
                }
            }
            return ActionView(ProductAssociateSKUFacetsPartialView, new ProductFacetsViewModel());
        }

        /// <summary>
        ///  To edit delete associated facet of sku
        /// </summary>
        /// <param name="id">int id - facetgroupId</param>
        /// <param name="skuId">int skuId</param>
        /// <returns>returns true/false</returns>
        [HttpPost]
        public JsonResult DeleteSkuAssociatedFacets(int? FacetGroupID, int skuId)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();
            if (!Equals(null, skuId) && !(Equals(FacetGroupID, null)))
            {
                status = _productAgent.DeleteSkuAssociatedFacets(skuId, FacetGroupID.Value);
                message = status ? ZnodeResources.DeleteMessage : ZnodeResources.DeleteErrorMessage;
            }
            return Json(new { sucess = status, message = message, id = FacetGroupID, isFadeOut = isFadeOut });
        }
        #endregion

        #region Product Tags

        /// <summary>
        /// Gets the product tags based on product id.
        /// </summary>
        /// <param name="productId">int productId</param>
        /// <returns>Return Product Tags</returns>
        [HttpGet]
        public ActionResult GetProductTags(int productId)
        {
            ProductTagsViewModel model = _productAgent.GetProductTags(productId);
            if (Equals(model.ProductId, 0))
            {
                model.ProductId = productId;
            }
            return ActionView(ProductTagsPartialView, model);
        }

        /// <summary>
        /// Method Insert/Update/Delete the product tags.
        /// </summary>
        /// <param name="model">ProductTagsViewModel model</param>
        /// <returns>Return status.</returns>
        public ActionResult ProductTags(ProductTagsViewModel model)
        {
            ModelState.Remove("ProductTags");
            if (!string.IsNullOrEmpty(model.ProductTags))
            {
                model.ProductTags = model.ProductTags.Trim();
            }
            ProductTagsViewModel productTagModel = _productAgent.GetProductTags(model.ProductId);

            return (!string.IsNullOrEmpty(productTagModel.ProductTags))
                ? (!string.IsNullOrEmpty(model.ProductTags))
                //update Tags
                    ? ProudctTagsHandler(model, productTagModel, ZnodeResources.UpdateTagSuccess, ZnodeResources.UpdateTagError, ZnodeCRUDMode.Update)
                //Delete Tags
                    : ProudctTagsHandler(new ProductTagsViewModel { ProductId = model.ProductId }, productTagModel, ZnodeResources.DeleteTagSuccess, ZnodeResources.DeleteTagError, ZnodeCRUDMode.Delete)
                : (!string.IsNullOrEmpty(model.ProductTags))
                //Insert Tags
                    ? ProudctTagsHandler(model, productTagModel, ZnodeResources.CreateTagSuccess, ZnodeResources.CreateTagError, ZnodeCRUDMode.Insert)
                //Required Validation 
                    : ProudctTagsHandler(new ProductTagsViewModel { ProductId = model.ProductId }, productTagModel, string.Empty, ZnodeResources.RequiredProductTags, ZnodeCRUDMode.Error);
        }

        #endregion

        #region Product Facets
        /// <summary>
        /// Gets the product Facets based on product id.
        /// </summary>
        /// <param name="formModel">FilterCollectionDataModel formModel</param>
        /// <returns>Return Product Facets</returns>
        public ActionResult GetProductFacets([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel formModel)
        {
            int productId = _productAgent.GetProductIdFromFormCollection(formModel);
            ProductFacetListViewModel model = _productAgent.GetProductFacets(productId, formModel.Filters, formModel.SortCollection, formModel.Page, formModel.RecordPerPage);
            if (!Equals(model, null))
            {
                model.ProductId = productId;

                model.GridModel = FilterHelpers.GetDynamicGridModel(formModel, model.Facets, (int)ListType.ProductFacetList);
                model.GridModel.TotalRecordCount = (model.TotalResults > 0) ? model.TotalResults : model.Facets.Count;
                return ActionView(ProductFacetsPartialView, model);
            }
            return ActionView(ProductFacetsPartialView, new ProductFacetListViewModel() { ProductId = productId });
        }

        /// <summary>
        /// Get the Associated facets by product Id.
        /// </summary>
        /// <param name="productId">int productId</param>
        /// <returns>Return the associated facets</returns>
        [HttpGet]
        public ActionResult GetAssociatedFacets(int productId)
        {
            ProductFacetsViewModel model = _productAgent.GetAssociatedFacetGroup(productId);
            if (!Equals(model, null))
            {
                model.ProductId = productId;
                return ActionView(ProductAssociateFacetsPartialView, model);
            }
            return ActionView(ProductAssociateFacetsPartialView, new ProductFacetsViewModel());
        }

        /// <summary>
        /// Get the Associated facets by product Id & Facet group id.
        /// </summary>
        /// <param name="productId">int productId</param>
        /// <returns>Return the associated facets</returns>
        [HttpGet]
        public ActionResult EditAssociatedFacets(int FacetGroupID, int ProductId)
        {
            if (FacetGroupID > 0 && ProductId > 0)
            {
                ProductFacetsViewModel model = _productAgent.GetAssociatedFacetGroup(ProductId, FacetGroupID);
                if (!Equals(model, null))
                {
                    model.ProductId = ProductId;
                    model.FacetGroupId = FacetGroupID;
                    return ActionView(ProductAssociateFacetsPartialView, model);
                }
            }
            return ActionView(ProductAssociateFacetsPartialView, new ProductFacetsViewModel());
        }

        /// <summary>
        /// Method Insert or Deletes the associated facets.
        /// </summary>
        /// <param name="model">ProductFacetsViewModel model</param>
        /// <returns>Return Status.</returns>
        public ActionResult AssociatedFacets(ProductFacetsViewModel model)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();
            var selectedFacets = _productAgent.GetAssociatedFacetGroup(model.ProductId, model.FacetGroupId).SelectedFacets;
            if ((Equals(selectedFacets, null) || (!Equals(selectedFacets, null) && Equals(selectedFacets.Count, 0))) && !(Equals(model.PostedFacets, null)))
            {
                //Insert Associated Facets.
                status = _productAgent.BindAssociatedFacets(model);
            }
            else
            {
                if (!Equals(selectedFacets, null) && selectedFacets.Count > 0)
                {
                    //Insert or Delete Associated Facets.
                    status = _productAgent.BindAssociatedFacets(model);
                }
                else
                {
                    //Error for Required associated facets.
                    message = ZnodeResources.RequiredAssocateFacets;
                    TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(message, NotificationType.error);
                }
            }
            if (status)
            {
                return RedirectToAction(MvcAdminConstants.ManageView, "Product", new { @id = model.ProductId, @tabMode = SelectedTab.Facet.ToString() });
            }
            else
            {
                ProductFacetsViewModel modelfacet = _productAgent.GetAssociatedFacetGroup(model.ProductId, model.FacetGroupId);
                modelfacet.ProductId = model.ProductId;
                return View(ProductAssociateFacetsPartialView, modelfacet);
            }

        }

        /// <summary>
        /// Get the facets by facet & product id.
        /// </summary>
        /// <param name="productId">int productId</param>
        /// <param name="facetGroupId">int facetGroupId</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetFacetsByFacetGroupId(int productId, int facetGroupId)
        {
            ProductFacetsViewModel model = new ProductFacetsViewModel();
            if (productId > 0 && facetGroupId > 0)
            {
                model = _productAgent.GetAssociatedFacetGroup(productId, facetGroupId);
            }
            return ActionView(ProductFacetsCheckBoxListPartialView, model);
        }


        /// <summary>
        /// To Delete the product associated facets based on productid & facetgroupid.
        /// </summary>
        /// <param name="productId">int productId</param>
        /// <param name="facetGroupId">int facetGroupId</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult DeleteProductAssociatedFacets(int? FacetGroupID, int ProductId)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();
            if (!Equals(null, ProductId) && !(Equals(FacetGroupID, null)))
            {
                status = _productAgent.DeleteProductAssociatedFacets(ProductId, FacetGroupID.Value);
                message = status ? ZnodeResources.DeleteAssociatedFacetSuccess : ZnodeResources.DeleteAssociatedFacetError;
            }
            return Json(new { sucess = status, message = message, id = FacetGroupID, isFadeOut = isFadeOut });
        }

        #endregion

        #region Product Images
        /// <summary>
        /// To Get all the Product images
        /// </summary>
        /// <param name="formModel">FilterCollectionDataModel formModel</param>
        /// <returns>Return Product Images.</returns>
        public ActionResult GetProductImages([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel formModel)
        {
            int productId = _productAgent.GetProductIdFromFormCollection(formModel);

            if (productId > 0)
            {
                ProductAlternateImageListViewModel model = _productAgent.GetProductAlternateImages(productId, formModel.Filters, formModel.SortCollection, formModel.Page, formModel.RecordPerPage);
                if (!Equals(model, null))
                {
                    model.ProductId = productId;
                    model.GridModel = FilterHelpers.GetDynamicGridModel(formModel, model.Images, (int)ListType.ProductImage);
                    model.GridModel.TotalRecordCount = (model.TotalResults > 0) ? model.TotalResults : model.Images.Count;
                    return ActionView(ProductAlternateImageListPartialView, model);
                }
            }
            return ActionView(ProductAlternateImageListPartialView, new ProductAlternateImageListViewModel() { ProductId = productId });
        }

        /// <summary>
        /// Get Action for Add Alternate Image.
        /// </summary>
        /// <param name="productId">int productId</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult AddAlternateImage(int productId)
        {
            ProductAlternateImageViewModel model = new ProductAlternateImageViewModel();
            if (productId > 0)
            {
                var productDetails = _productAgent.GetProduct(productId);
                model.ProductName = productDetails.Name;
                model.ImageTypes = _productAgent.GetProductImageTypes();
                model.ProductId = productDetails.ProductId;
            }
            return ActionView(AddAlternateProductImagePartialView, model);
        }

        /// <summary>
        /// Post Action for Add Alternate Image.
        /// </summary>
        /// <param name="imageModel">ProductAlternateImageViewModel imageModel</param>
        /// <returns></returns>
        public ActionResult AddAlternateImage(ProductAlternateImageViewModel imageModel)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();
            imageModel.PortalId = PortalId.Value;
            if (ModelState.IsValid)
            {
                status = _productAgent.InsertProductAlternateImage(imageModel);
                if (status)
                {
                    TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.AddProductImageSuccess, NotificationType.success);
                    return RedirectToAction(MvcAdminConstants.ManageView, productControllerName, new { @id = imageModel.ProductId, @tabMode = SelectedTab.Images.ToString() });
                }
                else
                {
                    TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.AddProductImageError, NotificationType.error);
                }
            }
            imageModel.ProductName = _productAgent.GetProduct(imageModel.ProductId).Name;
            imageModel.ImageTypes = _productAgent.GetProductImageTypes(imageModel.ProductImageTypeID);
            return View(AddAlternateProductImagePartialView, imageModel);
        }

        /// <summary>
        /// Get Action for Update Alternate Image.
        /// </summary>
        /// <param name="productId">int productId</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult UpdateAlternateImage(int id)
        {
            ProductAlternateImageViewModel model = new ProductAlternateImageViewModel();
            if (id > 0)
            {
                model = _productAgent.GetProductAlternateImage(id);
                model.ProductName = _productAgent.GetProduct(model.ProductId).Name;
                model.ImageTypes = _productAgent.GetProductImageTypes(model.ProductImageTypeID);
            }
            model.IsEditMode = true;
            return ActionView(AddAlternateProductImagePartialView, model);
        }
        /// <summary>
        /// Post Action for Update Alternate Image.
        /// </summary>
        /// <param name="imageModel">ProductAlternateImageViewModel imageModel</param>
        /// <returns></returns>
        public ActionResult UpdateAlternateImage(ProductAlternateImageViewModel imageModel)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();
            imageModel.PortalId = PortalId.Value;
            ProductAlternateImageViewModel model = null;
            if (Equals(imageModel.SelectedImageStatus, SelectImageStatus.CurrentImage))
            {
                ModelState.Remove(MvcAdminConstants.ProductImage);
            }
            if (ModelState.IsValid)
            {
                status = _productAgent.UpdateProductAlternateImage(imageModel);
                if (status)
                {
                    TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.UpdateProductImageSuccess, NotificationType.success);
                    return RedirectToAction(MvcAdminConstants.ManageView, "Product", new { @id = imageModel.ProductId, @tabMode = SelectedTab.Images });
                }
                else
                {
                    TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.UpdateProductImageError, NotificationType.error);
                }
            }
            model = _productAgent.GetProductAlternateImage(imageModel.ProductImageId);
            model.ProductName = _productAgent.GetProduct(model.ProductId).Name;
            model.ImageTypes = _productAgent.GetProductImageTypes(model.ProductImageTypeID);
            model.ProductId = imageModel.ProductId;
            model.IsEditMode = true;
            model.IsInvalidMode = true;
            return View(AddAlternateProductImagePartialView, model);

        }

        /// <summary>
        /// To Delete the product image based on product image id.
        /// </summary>
        /// <param name="productImageId">int productImageId</param>
        /// <returns></returns>

        public JsonResult DeleteProductImage(int id)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();
            if (!Equals(null, id))
            {
                status = _productAgent.DeleteProductAlternateImage(id);
                message = status ? ZnodeResources.DeleteProductImageSuccess : ZnodeResources.DeleteProductImageError;
            }
            return Json(new { sucess = status, message = message, id = id, isFadeOut = isFadeOut });
        }

        #endregion

        #region Product Bundles
        /// <summary>
        /// To Get all associated Product Bundles list.
        /// </summary>
        /// <param name="formModel">FilterCollectionDataModel formModel</param>
        /// <returns>Return Product Product Bundles list,</returns>
        public ActionResult GetProductBundleList([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel formModel)
        {
            int productId = _productAgent.GetProductIdFromFormCollection(formModel);
            if (productId > 0)
            {
                ProductBundlesListViewModel model = _productAgent.GetProductBundleList(productId, formModel.Filters, formModel.SortCollection, formModel.Page, formModel.RecordPerPage);
                if (!Equals(model, null))
                {
                    model.ProductId = productId;
                    model.GridModel = FilterHelpers.GetDynamicGridModel(formModel, model.ProductBundles, (int)ListType.vw_ZnodeGetChildProduct);
                    model.GridModel.TotalRecordCount = (model.TotalResults > 0) ? model.TotalResults : model.ProductBundles.Count;
                    return ActionView(ProductAssociateBundleListPartialView, model);
                }
            }
            return ActionView(ProductAssociateBundleListPartialView, new ProductBundlesListViewModel() { ProductId = productId });
        }

        /// <summary>
        /// To Get all associated Product Bundles list.
        /// </summary>
        /// <param name="formModel">FilterCollectionDataModel formModel</param>
        /// <returns>Return Product Product Bundles list,</returns>
        [HttpPost]
        public ActionResult GetProductsList([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel formModel)
        {
            int productId = _productAgent.GetProductIdFromFormCollection(formModel);

            ProductBundlesViewModel products = _productAgent.GetProductList(formModel.Filters, formModel.SortCollection, formModel.Page, formModel.RecordPerPage);
            var gridModel = FilterHelpers.GetDynamicGridModel(formModel, products.Products.Products, (int)ListType.AssociateBundleProductList);
            gridModel.TotalRecordCount = products.Products.Products.Count;
            gridModel.TotalRecordCount = (products.Products.TotalResults > 0) ? products.Products.TotalResults : products.Products.Products.Count;
            return ActionView(gridModel);
        }


        /// <summary>
        /// To Get list of Product to associate with the parent product.
        /// </summary>
        /// <param name="formModel">FilterCollectionDataModel formModel</param>
        /// <returns></returns>
        public ActionResult AddBundleProduct([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel formModel)
        {

            int productId = _productAgent.GetProductIdFromFormCollection(formModel);
            ProductBundlesViewModel model = new ProductBundlesViewModel();
            if (productId > 0)
            {
                model = _productAgent.BindProductSearchDropDown(formModel);
                if (!Equals(model, null))
                {
                    model.ParentProductID = productId;
                    FilterCollectionDataModel Filtermodel = new FilterCollectionDataModel();
                    Filtermodel.Page = formModel.Page;
                    Filtermodel.RecordPerPage = formModel.RecordPerPage;

                    var gridModel = FilterHelpers.GetDynamicGridModel(formModel, model.Products.Products, (int)ListType.AssociateBundleProductList);
                    var productIds = model.Products.Products.FindAll(x => x.ParentProduct == true).Select(y => y.ProductId).ToList();
                    model.AssociatedProductIds = String.Join(",", productIds);
                    FilterHelpers.CreateDropdown(ref gridModel, model.ProductTypes.ProductTypes, formModel.Filters, MvcAdminConstants.productTypeId, MvcAdminConstants.LabelName, ZnodeResources.LabelProductTypes);
                    FilterHelpers.CreateDropdown(ref gridModel, model.Catalogs.Catalogs, formModel.Filters, MvcAdminConstants.LabelCatalogId, MvcAdminConstants.LabelName, ZnodeResources.LabelCatalogs);
                    model.GridModel = gridModel;
                    model.GridModel.TotalRecordCount = (model.Products.TotalResults > 0) ? model.Products.TotalResults : model.Products.Products.Count;
                }
            }
            return ActionView(AddBundleProductPartialView, model);
        }

        /// <summary>
        /// To Get list of Product to associate with the parent product.
        /// </summary>
        /// <param name="productId">int productId</param>
        /// <param name="productIds">string productIds</param>
        /// <returns></returns>
        public ActionResult AssociateBundleProduct(int id, string productIds)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();
            if (!Equals(id, null) && !Equals(productIds, null))
            {
                ProductViewModel productModel = new ProductViewModel();
                productModel.ProductId = id;
                productModel.ProductIds = productIds;
                status = _productAgent.AssociateBundleProduct(productModel);
                message = status ? ZnodeResources.SaveMessage : ZnodeResources.AssociateBundleError;
                TempData[MvcAdminConstants.Notifications] = status ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.SaveErrorMessage, NotificationType.error);
            }
            TempData[MvcAdminConstants.SetTabMode] = SelectedTab.Bundle.ToString();
            return Json(new { sucess = status, message = message, id = id, isFadeOut = isFadeOut });
        }

        /// <summary>
        /// To Delete the bundle product based on 
        /// </summary>
        /// <param name="id">int productBundleId</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult DeleteBundleProduct(int? id)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();
            if (!Equals(null, id))
            {
                status = _productAgent.DeleteBundleProduct(id.Value);
                message = status ? ZnodeResources.DeleteBundleProductSuccess : ZnodeResources.DeleteBundleProductError;
            }
            return Json(new { sucess = status, message = message, id = id, isFadeOut = isFadeOut });
        }

        #endregion

        #region AddOns

        public ActionResult AddOns(int productId, [ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel formModel)
        {
            AddOnListViewModel viewModel = null;
            try
            {
                viewModel = _productAgent.GetProductAddOns(productId, formModel.Filters, formModel.SortCollection, formModel.Page, formModel.RecordPerPage);
                if (!Equals(viewModel, null))
                {
                    viewModel.ProductId = productId;

                    viewModel.GridModel = FilterHelpers.GetDynamicGridModel(formModel, viewModel.AddOns, (int)ListType.vw_ZNodeGetAssociatedAddOn);
                    viewModel.GridModel.TotalRecordCount = viewModel.TotalResults;
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex);
                TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.GenericErrorMessage, NotificationType.error);
            }

            return ActionView(ProductAddOnListView, viewModel);
        }

        public ActionResult RemoveAddOn(int id)
        {
            string message = string.Empty;
            bool status = false;
            bool isFadeOut = CheckIsFadeOut();
            if (id > 0)
            {
                status = _productAgent.RemoveProductAddOn(id);
                message = status ? ZnodeResources.DeleteMessage : ZnodeResources.DeleteErrorMessage;
            }
            return Json(new { sucess = status, message = message, id = id, isFadeOut = isFadeOut });
        }

        public ActionResult UnassociatedAddOns(int productId, [ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel formModel)
        {
            AddOnListViewModel viewModel = null;
            try
            {
                viewModel = _productAgent.GetUnassociatedAddOns(productId, (int)PortalId, formModel.Filters, formModel.SortCollection, formModel.Page, formModel.RecordPerPage);
                if (!Equals(viewModel, null))
                {
                    viewModel.ProductId = productId;
                    viewModel.ProductName = _productAgent.GetProduct(productId).Name;
                    viewModel.GridModel = FilterHelpers.GetDynamicGridModel(formModel, viewModel.AddOns, (int)ListType.vw_ZNodeGetAvailableAddOn);
                    viewModel.GridModel.TotalRecordCount = viewModel.TotalResults;
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex);
                TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.GenericErrorMessage, NotificationType.error);
            }

            return ActionView(UnassociatedAddOnListView, viewModel);
        }

        public ActionResult AssociateAddOn(string SelectedAddOns, int productId)
        {
            AddOnListViewModel viewModel = null;

            try
            {
                if (String.Equals(SelectedAddOns, ""))
                {
                    TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.ValidateUnAssociatedAddOn, NotificationType.error);
                    return RedirectToAction(UnassociatedAddOnsAction, new { productId = productId });
                }
                string[] idArray = SelectedAddOns.Split(',');
                var listofIDs = idArray.Where(x => x != "").ToList();
                IEnumerable<int> ids = listofIDs.Select(int.Parse);

                viewModel = new AddOnListViewModel();
                foreach (var item in ids)
                {
                    AddOnViewModel model = new AddOnViewModel();
                    model.AddOnId = item;
                    viewModel.ProductId = productId;
                    viewModel.AddOns.Add(model);
                }

                _productAgent.AssociateAddOns(viewModel);
                TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success);
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex);
                TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.SaveErrorMessage, NotificationType.error);
            }

            TempData[MvcAdminConstants.SetTabMode] = SelectedTab.AddOns;
            return RedirectToAction(MvcAdminConstants.ManageView, new { id = productId });
        }

        #endregion

        #region Highlights

        public ActionResult Highlights(int productId, [ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel formModel)
        {
            HighlightsListViewModel viewModel = null;
            try
            {

                viewModel = _productAgent.GetProductHighlights(productId, formModel.Filters, formModel.SortCollection, formModel.Page, formModel.RecordPerPage);
                if (!Equals(viewModel, null))
                {
                    viewModel.ProductId = productId;
                    viewModel.GridModel = FilterHelpers.GetDynamicGridModel(formModel, viewModel.Highlights, (int)ListType.vw_ZNodeGetAssocitedHighLights);
                    viewModel.GridModel.TotalRecordCount = viewModel.TotalResults;
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex);
                TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.GenericErrorMessage, NotificationType.error);
            }
            return View(ProductHighlightsListView, viewModel);
        }

        public JsonResult RemoveHighlight(int ProductHighlightId, int ProductId)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();
            try
            {
                status = _productAgent.RemoveProductHighlight(ProductHighlightId);
                message = status ? ZnodeResources.DeleteMessage : ZnodeResources.DeleteErrorMessage;
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex);
                TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.GenericErrorMessage, NotificationType.error);
            }
            return Json(new { sucess = status, message = message, id = ProductHighlightId, isFadeOut = isFadeOut });
        }

        public ActionResult UnassociatedHighlights(int productId, [ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel formModel)
        {
            HighlightsListViewModel viewModel = new HighlightsListViewModel();
            try
            {
                viewModel = _productAgent.GetUnassociatedHighlights(productId, (int)PortalId, formModel.Filters, formModel.SortCollection, formModel.Page, formModel.RecordPerPage);
                viewModel.ProductName = _productAgent.GetProduct(productId).Name;

                if (!Equals(viewModel, null))
                {
                    ReportModel gridModel = FilterHelpers.GetDynamicGridModel(formModel, viewModel.Highlights, (int)ListType.vw_ZNodeGetAvailableHighlights);
                    gridModel.TotalRecordCount = viewModel.TotalResults;
                    FilterHelpers.CreateDropdown(ref gridModel, _highlightAgent.GetHighlightTypeList(), formModel.Filters, MvcAdminConstants.HighlightTypeId, MvcAdminConstants.LabelName, MvcAdminConstants.TypeDropdownName);
                    viewModel.GridModel = gridModel;
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex);
                TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.GenericErrorMessage, NotificationType.error);
            }

            return ActionView(UnassociatedHighlightsView, viewModel);
        }

        public JsonResult AssociateHighlight(int? id, string selectedHighlights)
        {
            HighlightsListViewModel viewModel = null;
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();

            if (!Equals(id, null) && !Equals(selectedHighlights, null))
            {
                string[] idArray = selectedHighlights.Split(',');
                var listofIDs = idArray.Where(x => x != "").ToList();
                IEnumerable<int> ids = listofIDs.Select(int.Parse);

                viewModel = new HighlightsListViewModel();
                foreach (var item in ids)
                {
                    HighlightViewModel model = new HighlightViewModel();
                    model.HighlightId = item;
                    model.ProductId = id.Value;
                    viewModel.Highlights.Add(model);
                }

                status = _productAgent.AssociateHighlights(viewModel);
                message = status ? ZnodeResources.SaveMessage : ZnodeResources.SaveErrorMessage;
                TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success);
            }
            return Json(new { sucess = status, message = message, id = id, isFadeOut = isFadeOut });
        }

        #endregion

        #region Product Tier Pricing


        public ActionResult TierPriceList(int productId, [ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel formModel)
        {
            ProductTierListViewModel viewModel = new ProductTierListViewModel();

            try
            {
                viewModel = _productAgent.GetProductTiers(productId, formModel.Filters, formModel.SortCollection, formModel.Page, formModel.RecordPerPage);
                viewModel.ProductId = productId;
                viewModel.GridModel = FilterHelpers.GetDynamicGridModel(formModel, viewModel.Tiers, (int)ListType.vw_ZnodeProductTier);
                viewModel.GridModel.TotalRecordCount = viewModel.TotalResults;
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex);
                TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.GenericErrorMessage, NotificationType.error);
            }

            return View(TierPricingListView, viewModel);
        }

        [HttpGet]
        public ActionResult AddTierPrice(int productId)
        {
            ProductTierPricingViewModel viewModel = new ProductTierPricingViewModel();

            try
            {
                viewModel.Profiles = _categoryAgent.BindProfiles();
                viewModel.ProductId = productId;
                viewModel.ProductName = _productAgent.GetProduct(productId).Name;
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex);
                TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.GenericErrorMessage, NotificationType.success);
            }

            return View(AddEditTierPriceView, viewModel);
        }

        [HttpPost]
        public ActionResult AddTierPrice(ProductTierPricingViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _productAgent.CreateProductTier(viewModel);
                    viewModel.Profiles = _categoryAgent.BindProfiles();

                    TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success);

                    TempData[MvcAdminConstants.SetTabMode] = SelectedTab.Tiers;
                    return RedirectToAction(MvcAdminConstants.ManageView, new { id = viewModel.ProductId });
                }
                catch (ZnodeException ex)
                {
                    ExceptionHandler(ex);
                    TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ex.ErrorMessage, NotificationType.error);
                }
                catch (Exception ex)
                {
                    ExceptionHandler(ex);
                    TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.SaveErrorMessage, NotificationType.error);
                }
            }
            viewModel.Profiles = _categoryAgent.BindProfiles();
            return View(AddEditTierPriceView, viewModel);
        }

        [HttpGet]
        public ActionResult EditTierPrice(int ProductTierId, int ProductId)
        {
            ProductTierPricingViewModel viewModel = null;
            try
            {
                viewModel = _productAgent.GetProductTier(ProductId, ProductTierId);
                viewModel.ProductName = _productAgent.GetProduct(ProductId).Name;

                if (!Equals(viewModel, null))
                {
                    viewModel.Profiles = _categoryAgent.BindProfiles(Convert.ToInt32(viewModel.ProfileId));
                }
                else
                {
                    viewModel = new ProductTierPricingViewModel();
                    viewModel.Profiles = _categoryAgent.BindProfiles();
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex);
                TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.GenericErrorMessage, NotificationType.error);
            }

            viewModel.ProductId = ProductId;
            viewModel.IsEditMode = true;
            return View(AddEditTierPriceView, viewModel);
        }

        [HttpPost]
        public ActionResult EditTierPrice(ProductTierPricingViewModel viewModel)
        {

            if (ModelState.IsValid)
            {
                try
                {
                    _productAgent.UpdateProductTier(viewModel);
                    viewModel.Profiles = _categoryAgent.BindProfiles(Convert.ToInt32(viewModel.ProfileId));

                    TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.UpdateMessage, NotificationType.success);

                    TempData[MvcAdminConstants.SetTabMode] = SelectedTab.Tiers;
                    return RedirectToAction(MvcAdminConstants.ManageView, new { id = viewModel.ProductId });
                }
                catch (Exception ex)
                {
                    ExceptionHandler(ex);
                    viewModel.Profiles = _categoryAgent.BindProfiles();
                    TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.UpdateErrorMessage, NotificationType.error);
                }
            }
            return View(AddEditTierPriceView, viewModel);
        }

        [HttpPost]
        public ActionResult DeleteTierPrice(int id)
        {
            bool status = false;
            string message = string.Empty;

            try
            {
                status = _productAgent.DeleteProductTier(id);

                if (status)
                {
                    message = status ? ZnodeResources.DeleteMessage : ZnodeResources.DeleteErrorMessage;
                }
                return Json(new { sucess = status, message = message, id = id });
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex);
                return Json(new { sucess = status, message = ZnodeResources.GenericErrorMessage, id = id });
            }
        }

        #endregion

        #region Digital Asset


        public ActionResult DigitalAssetList(int productId, [ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel formModel)
        {
            DigitalAssetListViewModel viewModel = new DigitalAssetListViewModel();
            viewModel = _productAgent.GetDigitalAssets(productId, formModel.Filters, formModel.SortCollection, formModel.Page, formModel.RecordPerPage);
            if (!Equals(viewModel, null))
            {
                viewModel.ProductId = productId;

                viewModel.GridModel = FilterHelpers.GetDynamicGridModel(formModel, viewModel.DigitalAssets, (int)ListType.vw_ZNodeGetDigitalAsset);
                viewModel.GridModel.TotalRecordCount = viewModel.TotalResults;
                return View(DigitalAssetListView, viewModel);
            }
            return View(DigitalAssetListView, new DigitalAssetListViewModel());
        }

        [HttpGet]
        public ActionResult AddDigitalAsset(int productId)
        {
            DigitalAssetViewModel viewModel = null;
            viewModel = new DigitalAssetViewModel() { ProductID = productId };
            viewModel.ProductName = _productAgent.GetProduct(productId).Name;
            return View(AddDigitalAssetView, viewModel);
        }

        [HttpPost]
        public ActionResult AddDigitalAsset(DigitalAssetViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                DigitalAssetViewModel digitalAsset = _productAgent.AddDigitalAsset(viewModel);
                TempData[MvcAdminConstants.Notifications] = (!Equals(digitalAsset, null) && (digitalAsset.DigitalAssetID > 0)) ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.SaveErrorMessage, NotificationType.error);
                return RedirectToAction(MvcAdminConstants.ManageView, new { id = viewModel.ProductID });
            }
            return View(AddDigitalAssetView, viewModel);
        }

        /// <summary>
        /// Delete digital asset.
        /// </summary>
        /// <param name="id">int id</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Deletedigiatalasset(int id)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();
            if (id > 0)
            {
                status = _productAgent.DeleteDigitlAsset(id, out message);
                message = status ? ZnodeResources.DeleteMessage : message;
            }
            return Json(new { sucess = status, message = message, id = id, isFadeOut = isFadeOut });
        }

        #endregion

        #region Private Actions
        private ActionResult ProductSettingHandler(string message, ProductViewModel model, string viewName, bool status)
        {
            bool isFadeOut = CheckIsFadeOut();
            string ResponsePage = RenderRazorViewToString(viewName, model);
            return Json(new { sucess = status, message = message, Response = ResponsePage, isFadeOut = isFadeOut });
        }

        /// <summary>
        /// Method Returns the Json response based on the CRUD operation on Product Tags.
        /// </summary>
        /// <param name="model"></param>
        /// <param name="productTagModel"></param>
        /// <param name="msgSuccess"></param>
        /// <param name="msgError"></param>
        /// <param name="mode"></param>
        /// <returns></returns>
        private ActionResult ProudctTagsHandler(ProductTagsViewModel model, ProductTagsViewModel productTagModel, string msgSuccess, string msgError, ZnodeCRUDMode mode)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();
            model.TagId = productTagModel.TagId;
            status = (Equals(mode, ZnodeCRUDMode.Update))
                ? _productAgent.UpdateProductTags(model)
                : (Equals(mode, ZnodeCRUDMode.Delete))
                    ? _productAgent.DeleteProductTags(productTagModel.TagId)
                    : (Equals(mode, ZnodeCRUDMode.Insert))
                        ? _productAgent.InsertProductTags(model)
                        : false;

            message = status ? msgSuccess : msgError;
            var ResponseData = RenderRazorViewToString(ProductTagsPartialView, model);
            return Json(new { sucess = status, message = message, Response = ResponseData, isFadeOut = isFadeOut });
        }
        #endregion

        #region Private Enum
        /// <summary>
        /// Enum for Selected tab.
        /// </summary>
        private enum SelectedTab
        {
            Category,
            Skus,
            Images,
            Bundle,
            Facet,
            AddOns,
            Tiers,
            HighLights,
            DigitalAssets
        }
        #endregion

        #region Sku Profile

        /// <summary>
        /// Gets List Of Sku profile.
        /// </summary>
        /// <returns>ActionResult</returns>
        public ActionResult ListSkuProfile([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel formModel)
        {
            int skuId = _productAgent.GetSkuIdFromFormCollection(formModel);
            SkuProfileEffectiveListViewModel model = _productAgent.GetSkuProfileEffectivesBySkuId(skuId, formModel.Filters, formModel.SortCollection, formModel.Page, formModel.RecordPerPage);
            if (!Equals(model, null))
            {
                model.GridModel = FilterHelpers.GetDynamicGridModel(formModel, model.SkuProfileEffectives, (int)ListType.SkuProfileList);
                model.GridModel.TotalRecordCount = (model.TotalResults > 0) ? model.TotalResults : model.SkuProfileEffectives.Count;
                model.SKUId = skuId;
                return ActionView(skuProfileListView, model);
            }
            return ActionView(skuProfileListView, new SkuProfileEffectiveListViewModel() { SKUId = skuId });
        }

        /// <summary>
        /// Create Sku Profile Effective.
        /// </summary>
        /// <returns>ActionResult</returns>
        [HttpGet]
        public ActionResult CreateSkuProfile(int skuId)
        {
            SkuProfileEffectiveViewModel model = new SkuProfileEffectiveViewModel();
            model.SkuId = skuId;
            model.EffectiveDate = HelperMethods.ViewDateFormat(DateTime.Now);
            model.ProfileList = _productAgent.GetSkuProfilesList(skuId, 0);
            return ActionView(skuProfileCreateEditView, model);
        }

        /// <summary>
        /// Edit Existing Sku Profile.
        /// </summary>
        /// <param name="profileId">int? highlightId</param>
        /// <returns>ActionResult</returns>
        [HttpGet]
        public ActionResult EditSkuProfile(int? profileId)
        {
            SkuProfileEffectiveViewModel model = new SkuProfileEffectiveViewModel();
            if (!Equals(profileId, null) && profileId > 0)
            {
                model = _productAgent.GetSkuProfileEffective(profileId.Value);
                model.IsEditMode = true;
            }
            model.ProfileList = _productAgent.GetSkuProfilesList(model.SkuId, model.SkuProfileEffectiveID, model.ProfileId);
            return ActionView(skuProfileCreateEditView, model);
        }

        /// <summary>
        /// Delete existing Sku Profile Effective.
        /// </summary>
        /// <param name="id">Id of the Sku Profile Effective.</param>
        /// <returns>JsonResult</returns>
        public JsonResult DeleteSkuProfile(int? id)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();

            if (!Equals(null, id) && id.Value > 0)
            {
                status = _productAgent.DeleteSkuProfileEffective(id.Value, out message);
                message = status ? ZnodeResources.DeleteMessage : message;
            }

            return Json(new { sucess = status, message = message, id = id, isFadeOut = isFadeOut });
        }

        /// <summary>
        /// To Associate SKU Profile
        /// </summary>
        /// <param name="profile">SkuProfileEffectiveViewModel profile</param>
        /// <returns>returns true/false</returns>
        public JsonResult AssociateSKUProfile(SkuProfileEffectiveViewModel profile = null)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();
            int profileId = 0;
            if (!Equals(profile, null))
            {
                if (profile.SkuProfileEffectiveID > 0)
                {
                    status = _productAgent.UpdateSkuProfileEffective(profile);
                    message = status ? ZnodeResources.UpdateMessage : ZnodeResources.UpdateErrorMessage;
                }
                else
                {
                    status = _productAgent.CreateSkuProfileEffective(profile);
                    message = status ? ZnodeResources.SaveMessage : ZnodeResources.SaveErrorMessage;
                }
                profileId = profile.SkuProfileEffectiveID;
            }
            else
            {
                message = status ? ZnodeResources.SaveMessage : ZnodeResources.SaveErrorMessage;
            }
            return Json(new { sucess = status, message = message, id = profileId, isFadeOut = isFadeOut });
        }

        #endregion

        #region Customer based Pricing
        /// <summary>
        /// Gets the product Customer based pricing on product id.
        /// </summary>
        /// <param name="productId">product Id</param>
        /// <returns>customer Pricing list</returns>
        public ActionResult CustomerBasedPricingList([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model)
        {
            int productId = _productAgent.GetProductIdFromFormCollection(model);
            if (productId > 0)
            {
                CustomerBasedPricingListViewModel customerPricingModel = _CustomerBasedPricingAgent.GetCustomerPricingList(productId, model.Filters, model.SortCollection, model.Page, model.RecordPerPage);
                Session[MvcAdminConstants.Filters] = model.Filters;
                if (!Equals(model, null))
                {
                    customerPricingModel.ProductId = productId;

                    var storeList = _productAgent.GetStoreList();

                    customerPricingModel.GridModel = FilterHelpers.GetDynamicGridModel(model, customerPricingModel.CustomerBasedPricing, (int)ListType.vw_ZNodeGetCustomerBasedPricing);
                    var grid = customerPricingModel.GridModel;
                    FilterHelpers.CreateDropdown(ref grid, storeList, model.Filters, MvcAdminConstants.PortalId, MvcAdminConstants.StoreName, MvcAdminConstants.StoreDropdownName);
                    customerPricingModel.GridModel.TotalRecordCount = (customerPricingModel.TotalResults > 0) ? customerPricingModel.TotalResults : customerPricingModel.CustomerBasedPricing.Count;

                    return ActionView(CustomerPricingView, customerPricingModel);
                }
            }
            return ActionView(CustomerPricingView, new CustomerBasedPricingListViewModel() { ProductId = productId });
        }

        /// <summary>
        /// This method for download customer pricing details.
        /// </summary>
        /// <param name="model">CustomerBasedPricingListViewModel</param>
        /// <param name="viewModel">FilterCollectionDataModel</param>
        /// <returns>Download CSV file</returns>
        [HttpPost]
        public ActionResult DownloadCustomerBasedPricingList(CustomerBasedPricingListViewModel model, [ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel viewModel)
        {
            if (!Equals(model, null))
            {
                model.CustomerBasedPricing = _CustomerBasedPricingAgent.GetCustomerPricingList(model.ProductId, (Znode.Engine.Api.Client.Filters.FilterCollection)Session[MvcAdminConstants.Filters], viewModel.SortCollection, viewModel.Page, viewModel.RecordPerPage).CustomerBasedPricing;

                //below code downloads the file.
                _productAgent.DownloadFile(model, Response, customerPricingFileName);

            }
            return Content(string.Empty);
        }


        #endregion


    }
}
