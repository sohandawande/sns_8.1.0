﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.ViewModels;
using Znode.Libraries.Helpers.Constants;

namespace Znode.Engine.MvcAdmin.Helpers
{
    public class SessionProxyHelper
    {
        /// <summary>
        /// To Get the Users Action Permission List.
        /// </summary>
        /// <returns>Return the List of action Permission list in List<string> format.</returns>
        public static List<string> GetUserPermission()
        {
            List<string> lstUserPermission = null;
            try
            {
                if (Equals(HttpContext.Current.Session[MvcAdminConstants.UserPermissionListSessionKey], null))
                {
                    //Get the User Roles Permission List
                    AccountAgent accountAgent = new AccountAgent();
                    RolePermissionListViewModel permissionList = accountAgent.GetRolePermission();
                    if (!Equals(permissionList, null) && !Equals(permissionList.UserRolePermissionList, null))
                    {
                        lstUserPermission = permissionList.UserRolePermissionList.Select(x => x.EntityAction.ToLower()).ToList();
                    }
                    HttpContext.Current.Session[MvcAdminConstants.UserPermissionListSessionKey] = lstUserPermission;
                    accountAgent = null;
                }
                else
                {
                    //Set value from Session Variable
                    lstUserPermission = HttpContext.Current.Session[MvcAdminConstants.UserPermissionListSessionKey] as List<string>;
                }
            }
            catch { }
            return lstUserPermission;
        }

        /// <summary>
        /// To Check whether Current login user is Admin or Not.
        /// </summary>
        /// <returns>Return true or false</returns>
        public static bool IsAdminUser()
        {
            bool isAdminUser = false;
            if (Equals(HttpContext.Current.Session[MvcAdminConstants.IsAdminUserSessionKey], null))
            {
                //Check for the User Role is Admin or not.
                AccountAgent accountAgent = new AccountAgent();
                isAdminUser = accountAgent.IsUserInRole(HttpContext.Current.User.Identity.Name, Constant.RoleAdmin);
                HttpContext.Current.Session[MvcAdminConstants.IsAdminUserSessionKey] = isAdminUser;
                accountAgent = null;
            }
            else
            {
                //Set value from Session Variable
                isAdminUser = Convert.ToBoolean(HttpContext.Current.Session[MvcAdminConstants.IsAdminUserSessionKey]);
            }
            return isAdminUser;
        }

        /// <summary>
        /// To Get the User Menu list based on the Logged in userName.
        /// </summary>
        /// <returns>Return the List of User Menu List in RoleMenuListViewModel format</returns>
        public RoleMenuListViewModel GetMenuListByUserName()
        {
            RoleMenuListViewModel roleMenuList = null;
            try
            {
                if (Equals(HttpContext.Current.Session[MvcAdminConstants.UserMenuListSessionKey], null))
                {
                    //Get the User Menu List
                    AccountAgent accountAgent = new AccountAgent();
                    roleMenuList = accountAgent.GetRoleMenuList(HttpContext.Current.User.Identity.Name);
                    HttpContext.Current.Session[MvcAdminConstants.UserMenuListSessionKey] = roleMenuList;
                    accountAgent = null;
                }
                else
                {
                    //Set value from Session Variable
                    roleMenuList = HttpContext.Current.Session[MvcAdminConstants.UserMenuListSessionKey] as RoleMenuListViewModel;
                }
            }
            catch { }
            return roleMenuList;
        }

    }
}