﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Web.Mvc;
using System.Web.Routing;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Helpers
{
    /// <summary>
    /// Helper for Dropdown
    /// </summary>
    public static class DropdownHelper
    {
        public static MvcHtmlString AttributeDropDownList(this HtmlHelper helper, string name,
                                                            IEnumerable<AttributeSelectListItem> items,
                                                            bool filterOptions,
                                                            object htmlAttributes)
        {
            var dropdown = new TagBuilder("select");
            dropdown.Attributes.Add("name", name);
            dropdown.Attributes.Add("id", name);

            var options = new StringBuilder();
            if (filterOptions) items = items.Where(x => x.Highlighted);
            foreach (var item in items)
            {
                options.AppendFormat("<option {2} value='{1}' style='color:{3}'>{0}</option>",
                                     item.Text,
                                     item.Value,
                                     item.Selected && item.Highlighted ? "selected='selected'" : "",
                                     (!item.Highlighted && filterOptions) ? "gray" : "black");
            }

            dropdown.InnerHtml = options.ToString();
            dropdown.MergeAttributes(new RouteValueDictionary(htmlAttributes));

            return MvcHtmlString.Create(dropdown.ToString(TagRenderMode.Normal));
        }

        public static MvcHtmlString AddOnDropDownList(this HtmlHelper helper, string name,
                                                            IEnumerable<AddOnValueViewModel> items,
                                                            int selectedAddOnValue, string optionlabel,
                                                            object htmlAttributes)
        {
            var dropdown = new TagBuilder("select");
            dropdown.Attributes.Add("name", name);
            dropdown.Attributes.Add("id", name);

            var options = new StringBuilder();

            if (!string.IsNullOrEmpty(optionlabel))
            {
                options.AppendFormat("<option>Select {0}</option>", optionlabel);
            }

            foreach (var item in items)
            {
                options.AppendFormat("<option {2} value='{1}'>{0}</option>",
                                     item.AddOnValueText,
                                     item.AddOnValueId,
                                     item.AddOnValueId == selectedAddOnValue ? "selected='selected'" : "");
            }

            dropdown.InnerHtml = options.ToString();
            dropdown.MergeAttributes(new RouteValueDictionary(htmlAttributes));

            return MvcHtmlString.Create(dropdown.ToString(TagRenderMode.Normal));
        }


        public static MvcHtmlString AttributeDropDownListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, IEnumerable<AttributeSelectListItem> items, bool filterOptions, object htmlAttributes)
        {
            return AttributeDropDownList(htmlHelper, htmlHelper.ViewData.TemplateInfo.GetFullHtmlFieldName(ExpressionHelper.GetExpressionText(expression)), items, filterOptions, htmlAttributes);
        }

        public static MvcHtmlString AddOnDropDownListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, IEnumerable<AddOnValueViewModel> items, int selectedAddOnValue, string optionlabel, object htmlAttributes)
        {
            return AddOnDropDownList(htmlHelper,
                                     htmlHelper.ViewData.TemplateInfo.GetFullHtmlFieldName(
                                         ExpressionHelper.GetExpressionText(expression)), items, selectedAddOnValue, optionlabel,
                                     htmlAttributes);
        }
    }
}

public class AttributeSelectListItem : SelectListItem
{
    public bool Highlighted { get; set; }
}