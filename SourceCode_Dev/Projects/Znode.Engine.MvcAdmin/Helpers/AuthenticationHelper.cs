﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.ViewModels;
using System.Linq;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Libraries.Helpers.Constants;
using System.IO;

namespace Znode.Engine.MvcAdmin.Helpers
{
    public class AuthenticationHelper : AuthorizeAttribute
    {
        #region Variable Declaration
        private string defaultControllerName = "Account";
        private string defaultActionName = "Login";
        private string permision;
        private string actionName = string.Empty;
        private string controllerName = string.Empty;
        public string PermissionKey
        {
            get
            {
                return permision;
            }
            set
            {
                permision = value;
            }
        }

        #endregion

        /// <summary>
        /// Set Autheication cookied for the logged in user
        /// </summary>
        /// <param name="userName">UserName of the User</param>
        /// <param name="createPersistantCookie">Flag to Set Persistant cookier</param>
        public void SetAuthCookie(string userName, bool createPersistantCookie)
        {
            FormsAuthentication.SetAuthCookie(userName, createPersistantCookie);
        }

        /// <summary>
        /// Redirect to login view in case user is not authenticate.
        /// </summary>
        /// <param name="userName">UserName of the User</param>
        /// <param name="createPersistantCookie">Flag to Set Persistant cookier</param>
        public void RedirectFromLoginPage(string userName, bool createPersistantCookie)
        {
            FormsAuthentication.RedirectFromLoginPage(userName, createPersistantCookie);
        }

        #region OnAuthorization
        /// <summary>
        /// Overloaded method for Authorize attribute, user to authenticate & authorize the user for each action.
        /// </summary>
        /// <param name="filterContext">AuthorizationContext filterContext</param>
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            //Method to Authenticate & Authorize the current logged in user.
            AuthenticateUser(filterContext);
        }
        #endregion

        #region AuthenticateUser
        /// <summary>
        /// Method Used to Authenticate the user.
        /// </summary>
        /// <param name="filterContext">AuthorizationContext filterContext</param>
        public void AuthenticateUser(AuthorizationContext filterContext)
        {
            var isAuthorized = base.AuthorizeCore(filterContext.HttpContext);
            //skipAuthorization get sets to true when the action has the [AllowAnonymous] attributes, If true then skip authentication.
            bool skipAuthorization = filterContext.ActionDescriptor.IsDefined(typeof(AllowAnonymousAttribute), inherit: true)
                            || filterContext.ActionDescriptor.ControllerDescriptor.IsDefined(typeof(AllowAnonymousAttribute), inherit: true);
            if (!skipAuthorization)
            {
                if (!isAuthorized && !filterContext.HttpContext.Request.IsAuthenticated && (string.IsNullOrEmpty(filterContext.HttpContext.User.Identity.Name)))
                {
                    HandleUnauthorizedRequest(filterContext);
                }
                else
                {
                    if (!SessionProxyHelper.IsAdminUser())
                    {
                        PermissionManagerHelper.UserPermissionList = SessionProxyHelper.GetUserPermission;
                        CreateKey(filterContext);
                        if ((!Equals(PermissionManagerHelper.UserPermissionList, null)) && (!PermissionManagerHelper.HasUserPermissionRights(PermissionKey)))
                        {
                            HandleUnauthorizedRequest(filterContext);
                        }
                    }
                }
            }
        }
        #endregion

        #region HandleUnauthorizedRequest
        /// <summary>
        /// Redirect User to Index page in case the un authorized access.
        /// </summary>
        /// <param name="filterContext">AuthorizationContext filterContext</param>
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            string returnUrl = (filterContext.RequestContext.HttpContext.Request.IsAjaxRequest())
                ? (!Equals(filterContext.RequestContext.HttpContext.Request.UrlReferrer, null)) ? filterContext.RequestContext.HttpContext.Request.UrlReferrer.AbsolutePath : string.Empty : filterContext.RequestContext.HttpContext.Request.RawUrl;

            if (filterContext.RequestContext.HttpContext.Request.IsAjaxRequest())
            {
                filterContext.HttpContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                string routeName = (Equals(filterContext.RequestContext.HttpContext.Request.RequestContext.RouteData.DataTokens[MvcAdminConstants.AreaKey], null)) ? string.Empty : Convert.ToString(filterContext.RequestContext.HttpContext.Request.RequestContext.RouteData.DataTokens[MvcAdminConstants.AreaKey]);
                routeName = (string.IsNullOrEmpty(routeName)) ? GetAreaNameFromUrlReferrer(filterContext) : routeName;
                filterContext.Result = new JsonResult
                {
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                    Data = new
                    {
                        ErrorCode = "101",
                        ReturnUrl = HttpUtility.HtmlEncode(returnUrl),
                        Area = routeName,
                    }
                };
            }
            else
            {
                filterContext.Result = new RedirectToRouteResult(
                          new RouteValueDictionary {
                        { MvcAdminConstants.Controller, defaultControllerName },
                        { MvcAdminConstants.Action, defaultActionName },
                        { "returnUrl", HttpUtility.UrlEncode(returnUrl)}
                          });
            }
        }

        #endregion

        #region CreateKey
        /// <summary>
        /// Retrieves the Current Action & Controller Name.
        /// </summary>
        /// <param name="filterContext">AuthorizationContext filterContext</param>
        private void CreateKey(AuthorizationContext filterContext)
        {
            controllerName = filterContext.HttpContext.Request.RequestContext.RouteData.Values[MvcAdminConstants.Controller].ToString();
            actionName = filterContext.HttpContext.Request.RequestContext.RouteData.Values[MvcAdminConstants.Action].ToString();
            PermissionKey = controllerName + "/" + actionName;
        }
        #endregion

        /// <summary>
        /// Get Area name from the current request UrlReferrer
        /// </summary>
        /// <param name="filterContext">AuthorizationContext filterContext</param>
        /// <returns>Return the Area Name</returns>
        private string GetAreaNameFromUrlReferrer(AuthorizationContext filterContext)
        {
            string areaName = string.Empty;
            var fullUrl = filterContext.RequestContext.HttpContext.Request.UrlReferrer.ToString();
            var questionMarkIndex = fullUrl.IndexOf('?');
            string queryString = null;
            string url = fullUrl;
            if (questionMarkIndex != -1) // There is a QueryString
            {
                url = fullUrl.Substring(0, questionMarkIndex);
                queryString = fullUrl.Substring(questionMarkIndex + 1);
            }
            // Arranges
            var request = new HttpRequest(null, url, queryString);
            var response = new HttpResponse(new StringWriter());
            var httpContext = new HttpContext(request, response);
            var routeData = RouteTable.Routes.GetRouteData(new HttpContextWrapper(httpContext));
            return areaName = (Equals(routeData.DataTokens[MvcAdminConstants.AreaKey], null)) ? string.Empty : Convert.ToString(routeData.DataTokens[MvcAdminConstants.AreaKey]);
        }
    }
}