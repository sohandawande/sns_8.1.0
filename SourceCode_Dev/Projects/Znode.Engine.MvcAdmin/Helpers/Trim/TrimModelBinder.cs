﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace Znode.Engine.MvcAdmin.Helpers.Trim
{
    public class TrimModelBinder : DefaultModelBinderWithHtmlValidation
    {
        protected override void SetProperty(ControllerContext controllerContext, ModelBindingContext bindingContext, PropertyDescriptor propertyDescriptor, object value)
        {
            if ((propertyDescriptor.PropertyType.Equals(typeof(string))) && (!propertyDescriptor.Attributes.Cast<object>().Any(a => a.GetType().Equals(typeof(NoTrimAttribute)))))
            {
                string stringValue = Convert.ToString(value);

                if (!string.IsNullOrEmpty(stringValue))
                {
                    stringValue = (Regex.Replace(stringValue, @"\s+", " ")).Trim();
                }
                value = stringValue;
            }

            base.SetProperty(controllerContext, bindingContext,
                                propertyDescriptor, value);
        }
    }


    public class DefaultModelBinderWithHtmlValidation : DefaultModelBinder
    {

        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            try
            {
                return base.BindModel(controllerContext, bindingContext);
            }
            catch (HttpRequestValidationException ex)
            {
                bindingContext.ModelState.AddModelError(bindingContext.ModelName, string.Format("Illegal characters were found."));            
            }

            //Cast the value provider to an IUnvalidatedValueProvider, which allows to skip validation
            IUnvalidatedValueProvider provider = bindingContext.ValueProvider as IUnvalidatedValueProvider;
            if (Equals(provider, null)) return null;

            //Get the attempted value, skiping the validation
            ValueProviderResult result = provider.GetValue(bindingContext.ModelName, skipValidation: true);
           
            return result.AttemptedValue;
        }
    }

}
