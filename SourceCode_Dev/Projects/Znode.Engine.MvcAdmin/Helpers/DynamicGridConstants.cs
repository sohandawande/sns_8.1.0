﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcAdmin.Helpers
{
    public class DynamicGridConstants
    {
        #region Messages
        // Constants for notification message
        public const string ActionNotifications = "ActionNotifications";
        public const string Notifications = "Notifications";

        public const string DisplayOpionErrReq = "Please enter display options.";
        public const string PageErrReq = "Please enter front end page name.";
        public const string ObjectErrReq = "Please enter front end object name.";
        public const string EntityNameErrReq = "Please enter entity name.";
        public const string EditSuccessMsg = "XML configuration settings updated successfully.";
        public const string EditErrorMsg = "Unable to update XML configuration settings.";
        public const string AddSuccessMsg = "XML configuration settings saved successfully.";
        public const string AddErrorMsg = "Unable to save XML configuration settings.";//

        public const string DeleteSuccessMsg = "XML configuration settings deleted successfully.";
        public const string DeleteErrorMsg = "Unable to delete XML configuration settings.";//

        #endregion
        #region View
        public const string ListBoxView = "~/Views/XMLGenerator/_ListViewBox.cshtml";
        public const string CreateXMLView = "~/Views/XMLGenerator/CreateXML.cshtml";
        public const string GridColumnSetting = "~/Views/XMLGenerator/_GridColumnSettings.cshtml";
        #endregion
        #region Json Status
        public const string Success = "success";
        public const string Fail = "fail";
        public const string Error = "error";
        #endregion
        public const string IdKey = "id";
        public const string FormatKey = "format";
        public const string ColumntypeKey = "columntype";
        public const string AllowsortingKey = "allowsorting";
        public const string AllowpagingKey = "allowpaging";
        public const string MustshowKey = "mustshow";
        public const string MusthideKey = "musthide";
        public const string MaxlengthKey = "maxlength";
        public const string IsconditionalKey = "isconditional";
        public const string WidthKey = "width";
        public const string imageactionurlKey = "imageactionurl";
        public const string Date = "Date";
        public const string SmallDate = "date";
        public const string Time = "Time";
        public const string DateTime = "DateTime";
        public const string DateFormat = "dd-MMM-yyyy";
        public const string TableName = "column";
        public const string DefaultText = "All";
        public const string Yes = "y";
        public const string No = "n";
        public const string GroupNameKey = "Grid";
        public const string OperatordefinitionKey = "operatordefinition";
        public const string EditKey = "Edit";
        public const string DeleteKey = "Delete";
        public const string ViewKey = "View";
        public const string ImageKey = "Image";
        public const string CopyKey = "Copy";
        public const string ManageKey = "Manage";
        public const string ActionKey = "Action";
        public const string CheckboxKey = "Checkbox";
        public const string ControlKey = "Control";
        public const string IsLinkKey = "IsLink";
        public const string columnKey = "column";
        public const string nameKey = "name";
        public const string headertextKey = "headertext";
        public const string datatypeKey = "datatype";
        public const string isallowsearchKey = "isallowsearch";
        public const string isvisibleKey = "isvisible";
        public const string editactionurlKey = "editactionurl";
        public const string editparamfieldKey = "editparamfield";
        public const string deleteactionurlKey = "deleteactionurl";
        public const string deleteparamfieldKey = "deleteparamfield";
        public const string viewactionurlKey = "viewactionurl";
        public const string viewparamfieldKey = "viewparamfield";
        public const string imageparamfieldKey = "imageparamfield";
        public const string manageactionurlKey = "manageactionurl";
        public const string manageparamfieldKey = "manageparamfield";
        public const string copyactionurlKey = "copyactionurl";
        public const string copyparamfieldKey = "copyparamfield";
        public const string sortKey = "sort";
        public const string sortDirKey = "sortDir";
        public const string DESCKey = "DESC";
        public const string ASCKey = "ASC";
        public const string xmlConfigDocSessionKey = "xmlConfigDoc";
        public const string XMLStringSesionKey = "XMlstring";
        public const string listTypeSesionKey = "listType";
        public const string ColumnListSesionKey = "ColumnList";
        public const string AdvanceFilterCollectionKey = "AdvanceFilterCollection";
        public const string checkboxactionurlKey = "checkboxactionurl";
        public const string checkboxparamfieldKey = "checkboxparamfield";
        public const string displaytextKey = "displaytext";
        public const string ischeckboxKey = "ischeckbox";
        public const string isallowlinkKey = "isallowlink";
        public const string islinkactionurlKey = "islinkactionurl";
        public const string islinkparamfieldKey = "islinkparamfield";
        public const string iscontrolKey = "iscontrol";
        public const string controltypeKey = "controltype";
        public const string controlparamfieldKey = "controlparamfield";
        public const string xaxis = "xaxis";
        public const string yaxis = "yaxis";
        public const string isadvancesearch = "isadvancesearch";
        
        

        public const string CheckboxHtml = "<input type=\"checkbox\" name=\"{1}\" id=\"rowcheck_{0}\" class=\"grid-row-checkbox\">  <span class=\"lbl padding-8\"></span>";
        public const string DeleteHtml = "<a id=\"spanDelete_{3}\" href=\"javascript:void(0);\" class=\"z-delete active-icon\" title=\"Delete\"  onclick=\"CommonHelper.BindDeleteConfirmDialog('{1}','{2}','{0}')\"></a>";
        public const string EditHtml = "<a href=\"{0}\"  class=\"z-edit active-icon\" title=\"Edit\"></a>";
        public const string ViewHtml = "<a href=\"{0}\" class=\"z-view active-icon\" title=\"View\"></a>";
        public const string ManageHtml = "<a href=\"{0}\" class=\"z-manage active-icon\" title=\"Manage\"></a>";
        public const string CopyHtml = "<a href=\"{0}\" class=\"z-copy active-icon\" title=\"Copy\"></a>";
        public const string ImageHtml = "<img src=\"{0}\" class=\"grid-img\"/>";
        public const string LinkHtml = "<a href=\"{0}\">{1}</a>";
        public const string DynamicOptionString = "<option title='{3}' data-datype='{0}' data-keyname='{1}' value='{2}'>{3} </option>";
        public const string DisableHtml = "<a id=\"spanDisable_{4}\" title='{5}' href=\"javascript:void(0);\" class=\"z-{3} active-icon\"  onclick=\"CommonHelper.BindDeleteConfirmDialog('{1}','{2}','{0}')\"></a>";
        public const string IconTrue = "<i class='glyphicon glyphicon-ok'></i>";
        public const string IconFalse = "<i class='glyphicon glyphicon-remove'></i>";
        public const string ControlHtml = "<input type=\"{0}\" id=\"control_{2}\" value=\"{1}\"/>";
        public const string SubgridHtml = "<input id=\"recored-id\" type=\"hidden\" value=\"{0}\"/><input id=\"type-name\" type=\"hidden\" value=\"{1}\"/><input id=\"method-name\" type=\"hidden\" value=\"{2}\"/>";
        public const string AssignedId = "FilterColumnList.AssignedId";
        public const string FieldName = "FieldName";
        public const string Operator = "Operator";
        public const string Values = "Values";
        public const string Clause = "Clause";

        public const string FieldsCollectionSessionKey = "FieldsCollection";
        public const string OperatorsCollectionSessionKey = "OperatorsCollection";
        public const string ValuesCollectionSessionKey = "ValuesCollection";



        public const string DisableKey = "Disable";
        public const string enabledisableactionurlKey = "enabledisableactionurl";
        public const string enabledisableparamfieldKey = "enabledisableparamfield";
      

        //Operators
        public const string IsOperator = "is";
        public const string BeginswithOperator = "begins with";
        public const string EndswithOperator = "ends with";
        public const string ContainsOperator = "contains";
        public const string EqualsOperator = "equals";
        public const string GreaterthanOperator = "greater than";
        public const string GreaterorequalOperator = "greater or equal";
        public const string LessthanOperator = "less than";
        public const string LessorequalOperator = "less or equal";

        //Data Types
        public const string StringKey = "string";
        public const string Int16Key = "int16";
        public const string Int32Key = "int32";
        public const string Int64Key = "int64";
        public const string BooleanKey = "boolean";
        public const string DateTimeKey = "datetime";
        public const string DecimalKey = "decimal";
        public const string DoubleKey = "double";
        public const string SingleKey = "single";

        //Css Class Name
        public const string ClassCenter = "center-align";
        public const string ClassRight = "right-align";
    }
}