﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Caching;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Helpers
{
    public class MvcAdminUnits
    {
        #region Private Variable
        private readonly IPortalAgent _portalAgent;
        #endregion

        #region Construcor
        public MvcAdminUnits()
        {
            _portalAgent = new PortalAgent();
        }
        #endregion

        #region Public Static Properties

        public static string WeightUnit
        {
            get
            {
                return PortalAgent.CurrentPortal.WeightUnit;
            }
        }

        public static string DimensionUnit
        {
            get
            {
                return PortalAgent.CurrentPortal.DimensionUnit;
            }
        }

        public static string CurrencySymbol
        {
            get
            {
                MvcAdminUnits adminUnits = new MvcAdminUnits();
                return adminUnits.GetCurrencySymbol();
            }
        }

        public static string CurrencySuffix
        {
            get
            {
                MvcAdminUnits adminUnits = new MvcAdminUnits();
                return adminUnits.GetCurrencySuffix();
            }
        }

        #endregion

        #region Public methods
        public static string GetCurrencyValue(decimal priceValue)
        {
            MvcAdminUnits mvcadminUnits = new MvcAdminUnits();
            return mvcadminUnits.GetCurrencyUnit(priceValue);
        }

        public static decimal ConvertCurrencyToDecimalValue(string priceValue)
        {
            MvcAdminUnits mvcadminUnits = new MvcAdminUnits();
            return mvcadminUnits.ConvertCurrencyUnitToDecimal(priceValue);
        }
        #endregion

        #region Private Methods

        /// <summary>
        /// Gets Currency Symbol
        /// </summary>
        /// <returns>Currency symbol.</returns>
        private string GetCurrencySymbol()
        {
            int currencyId = PortalAgent.CurrentPortal.CurrencyTypeId.GetValueOrDefault();
            CurrencyTypeModel currencyType = _portalAgent.GetCurrencyType(currencyId);

            string currencySymbol = currencyType.Name;
            CultureInfo info = new CultureInfo(currencySymbol);

            return info.NumberFormat.CurrencySymbol;
        }

        /// <summary>
        /// Gets Currency Suffix.
        /// </summary>
        /// <returns>Currency Suffix.</returns>
        private string GetCurrencySuffix()
        {
            int currencyId = PortalAgent.CurrentPortal.CurrencyTypeId.GetValueOrDefault();
            CurrencyTypeModel currencyType = _portalAgent.GetCurrencyType(currencyId);

            return currencyType.CurrencySuffix;
        }

        /// <summary>
        /// Get Currency Unit for a price.
        /// </summary>
        /// <param name="priceValue">Price value.</param>
        /// <returns>Price with currency symbol and currency suffix.</returns>
        private string GetCurrencyUnit(decimal priceValue)
        {
            int currencyId = PortalAgent.CurrentPortal.CurrencyTypeId.GetValueOrDefault();

            if (Equals(HttpContext.Current.Cache[HttpContext.Current.Request.Url.Authority +MvcAdminConstants.CurrencyUnits], null))
            {
                PortalAgent agent = new PortalAgent();
                CurrencyTypeModel model = agent.GetCurrencyType(currencyId);

                if (!Equals(model, null))
                {
                    HttpContext.Current.Cache.Insert(HttpContext.Current.Request.Url.Authority + MvcAdminConstants.CurrencyUnits, model, null, DateTime.Now.AddMinutes(5),
                                                     Cache.NoSlidingExpiration);
                }
            }
            CurrencyTypeModel currencyType = HttpContext.Current.Cache[HttpContext.Current.Request.Url.Authority + MvcAdminConstants.CurrencyUnits] as CurrencyTypeModel;

            string currencySymbol = currencyType.Name;
            string currencyValue = string.Empty;

            CultureInfo info = new CultureInfo(currencySymbol);

            decimal price = priceValue;

            currencyValue = price.ToString("c", info.NumberFormat);

            return currencyValue;
        }


        /// <summary>
        /// Converts Currency Unit to Decimal.
        /// </summary>
        /// <param name="priceValue">string Price value.</param>
        /// <returns>Price in decimal</returns>
        private decimal ConvertCurrencyUnitToDecimal(string priceValue)
        {
            int currencyId = PortalAgent.CurrentPortal.CurrencyTypeId.GetValueOrDefault();
            CurrencyTypeModel currencyType = _portalAgent.GetCurrencyType(currencyId);

            string currencySymbol = currencyType.Name;
            decimal currencyValue = 0.0M;

            CultureInfo info = new CultureInfo(currencySymbol);

            currencyValue = Decimal.Parse(priceValue, NumberStyles.Currency, info.NumberFormat);

            return currencyValue;
        }
        #endregion
    }
}