﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Helpers
{
    public static class PRFTHelperMethods
    {
        public static bool IsB2BUser(AccountViewModel accountModel)
        {
            bool isB2BUser = false;            

            if (accountModel != null && !string.IsNullOrEmpty(accountModel.Custom2))
            {
                string customerType = Convert.ToString(Convert.ToInt32(CustomValue.One));
                isB2BUser = accountModel.Custom2.Equals(customerType) ? true : false;
            }
            return isB2BUser;
        }
    }
}