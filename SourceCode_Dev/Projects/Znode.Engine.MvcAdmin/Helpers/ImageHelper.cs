﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.ViewModels;
using Znode.Libraries.Helpers;
using ZNode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.MvcAdmin.Helpers
{
    /// <summary>
    /// To helper for Upload Image
    /// </summary>
    public class ImageHelper
    {
        #region Private Variables

        private bool _UseSiteConfig = false;
        private bool isAlreadyChecked = false;
        private string imageNotAvailablePath = Path.GetFileName(PortalAgent.CurrentPortal.ImageNotAvailablePathUrl);
        private string newFileName = string.Empty;
        private PortalViewModel portal = PortalAgent.CurrentPortal;
        private EnvironmentConfigModel environmentConfig = Equals(GetEnvironmentConfig<EnvironmentConfigModel>(MvcAdminConstants.EnvironmentConfigKey), null) ? PortalAgent.GetEnvironmentConfig : GetEnvironmentConfig<EnvironmentConfigModel>(MvcAdminConstants.EnvironmentConfigKey);

        #endregion

        #region Save Image

        /// <summary>
        /// To Save image to physical path by Id
        /// </summary>
        /// <param name="portalId">int? portalId</param>
        /// <param name="accountId">int? accountId</param>
        /// <param name="entityId">int entityId</param>
        /// <param name="validExtension">string validExtension</param>
        /// <param name="postedFile">HttpPostedFileBase postedFile</param>
        /// <returns>returns imagePath</returns>
        public string SaveImageToPhysicalPathById(int? portalId, int? accountId, int entityId, string validExtension, HttpPostedFileBase postedFile)
        {
            string imageFliePath = string.Empty;
            if (!Equals(postedFile, null))
            {

                if (IsValidFile(postedFile, validExtension))
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(portalId)))
                    {
                        imageFliePath = this.SaveImage(entityId, "fadmin", Convert.ToString(portalId), "0", postedFile);
                        imageFliePath = string.IsNullOrEmpty(imageFliePath) ? string.Empty : "Turnkey/" + portalId + "/" + imageFliePath;
                    }
                    else if (!string.IsNullOrEmpty(Convert.ToString(accountId)))
                    {
                        imageFliePath = this.SaveImage(entityId, "madmin", "0", Convert.ToString(accountId), postedFile);
                        imageFliePath = string.IsNullOrEmpty(imageFliePath) ? string.Empty : "Mall/" + accountId + "/" + imageFliePath;
                    }
                    else
                    {
                        imageFliePath = this.SaveImage(entityId, "sadmin", Convert.ToString(portal.PortalId), "0", postedFile);
                        imageFliePath = string.IsNullOrEmpty(imageFliePath) ? string.Empty : "Turnkey/" + portal.PortalId + "/" + imageFliePath;
                    }
                }
                else
                {
                    imageFliePath = MvcAdminConstants.FileUploadErrorCode;
                }
            }
            return imageFliePath;
        }

        /// <summary>
        /// To Save image on physical path by userdefined name
        /// </summary>
        /// <param name="portalId">int? portalId</param>
        /// <param name="accountId">int? accountId</param>
        /// <param name="validExtension">string validExtension</param>
        /// <param name="fileName">string fileName</param>
        /// <param name="postedFile">HttpPostedFileBase postedFile</param>
        /// <returns>returns filepath</returns>
        public string SaveImageToPhysicalPathByName(int? portalId, int? accountId, string validExtension, string fileName, HttpPostedFileBase postedFile,string adminType)
        {
            string imageFliePath = string.Empty;
            newFileName = fileName;
            if (!Equals(postedFile, null))
            {

                if (IsValidFile(postedFile, validExtension))
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(portalId)))
                    {
                        if (!string.IsNullOrEmpty(adminType))
                        {
                            imageFliePath = this.SaveImage(0, adminType, Convert.ToString(portalId), "0", postedFile);
                            imageFliePath = string.IsNullOrEmpty(imageFliePath) ? string.Empty : "Turnkey/" + portalId + "/" + imageFliePath;
                        }
                        else
                        {
                            imageFliePath = this.SaveImage(0, "fadmin", Convert.ToString(portalId), "0", postedFile);
                            imageFliePath = string.IsNullOrEmpty(imageFliePath) ? string.Empty : "Turnkey/" + portalId + "/" + imageFliePath;
                        }
                    }
                    else if (!string.IsNullOrEmpty(Convert.ToString(accountId)))
                    {
                        imageFliePath = this.SaveImage(0, "madmin", "0", Convert.ToString(accountId), postedFile);
                        imageFliePath = string.IsNullOrEmpty(imageFliePath) ? string.Empty : "Mall/" + accountId + "/" + imageFliePath;
                    }
                    else
                    {
                        imageFliePath = this.SaveImage(0, "sadmin", Convert.ToString(portal.PortalId), "0", postedFile);
                        imageFliePath = string.IsNullOrEmpty(imageFliePath) ? string.Empty : "Turnkey/" + portal.PortalId + "/" + imageFliePath;
                    }
                }
                else
                {
                    imageFliePath = MvcAdminConstants.FileUploadErrorCode;
                }
            }
            return imageFliePath;
        }

        /// <summary>
        /// To check file extension isValid
        /// </summary>
        /// <param name="Image">HttpPostedFileBase image</param>
        /// <param name="commaSeparatedExtension">string commaSeparatedExtension</param>
        /// <returns>returns true/false</returns>
        private bool IsValidFile(HttpPostedFileBase image, string commaSeparatedExtension)
        {
            if (FileUploadHelper.IsValidFileExtension(image.FileName, commaSeparatedExtension))
            {
                return true;
            }
            return false; ;
        }

        /// <summary>
        /// Get the file and return the full physical path with filename.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        /// <returns>Returns the full physical path with file name.</returns>
        private string GetFullName(string fileName)
        {
            if (this._UseSiteConfig)
            {
                return environmentConfig.OriginalImagePath + imageNotAvailablePath;
            }
            else
            {
                return environmentConfig.OriginalImagePath + fileName;
            }
        }

        /// <summary>
        /// Get the file and return the full physical path with filename.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        /// <returns>Returns the full physical path with file name.</returns>
        private string GetFullName(string portalId, string accountId, string fileName, string userType)
        {
            if (this._UseSiteConfig)
            {
                return imageNotAvailablePath + fileName;
            }
            else
            {
                if (userType == "sadmin")
                {
                    return environmentConfig.OriginalImagePath + "Turnkey/" + portalId + "/" + fileName;
                }
                else if (userType == "madmin")
                {
                    return environmentConfig.OriginalImagePath + "Mall/" + accountId + "/" + fileName;
                }
                else if (userType == "fadmin")
                {
                    return environmentConfig.OriginalImagePath + "Turnkey/" + portalId + "/" + fileName;
                }
                else
                {
                    return environmentConfig.OriginalImagePath + fileName;
                }
            }
        }

        /// <summary>
        /// Save Image in folder structure
        /// </summary>
        /// <returns>returns image path if image saved otheriwse empty.</returns>
        private string SaveImage(int entityId, string userType, string portalId, string accountId, HttpPostedFileBase postedFile)
        {
            string fileName = string.Empty;
            if (entityId > 0)
            {
                fileName = string.Format("{0}-{1}{2}", Path.GetFileNameWithoutExtension(postedFile.FileName), entityId, Path.GetExtension(postedFile.FileName));
            }
            else if (!string.IsNullOrEmpty(newFileName))
            {
                if (!newFileName.Contains("."))
                {
                    fileName = string.Format("{0}{1}", newFileName, Path.GetExtension(postedFile.FileName));
                }
                else
                {
                    fileName = newFileName;
                }
            }
            else
            {
                fileName = postedFile.FileName;
            }

            string fullName = this.GetFullName(portalId, accountId, fileName, userType);
            try
            {
                ZNodeStorageProviderFile storageProvider = new ZNodeStorageProviderFile();
                if (!storageProvider.Exists(fullName))
                {
                    byte[] imageData = new byte[postedFile.InputStream.Length];
                    postedFile.InputStream.Read(imageData, 0, (int)postedFile.InputStream.Length);
                    storageProvider.WriteBinaryStorage(imageData, fullName);
                }
                else
                {
                    fullName = this.GetFullName(postedFile.FileName);
                    byte[] imageData = new byte[postedFile.InputStream.Length];
                    postedFile.InputStream.Read(imageData, 0, (int)postedFile.InputStream.Length);
                    storageProvider.WriteBinaryStorage(imageData, fullName);
                }
            }
            catch (Exception ex)
            {
                var annotatedException = new Exception(string.Empty, ex);
                Elmah.ErrorSignal.FromCurrentContext().Raise(annotatedException);
            }
            return fileName;
        }

        #endregion

        #region Get Image

        /// <summary>
        /// Get the large size image relative path.
        /// </summary>
        /// <param name="imageFileName">Image file name.</param>
        /// <returns>Returns the large size image relative path.</returns>
        public string GetImageHttpPathLarge(string imageFileName)
        {
            return this.GetRelativeImageUrl(portal.MaxCatalogItemLargeWidth, imageFileName, false);
        }

        /// <summary>
        /// Get the small medium size image relative path.
        /// </summary>
        /// <param name="imageFileName">Image file name.</param>
        /// <returns>Returns the medium size image relative path.</returns>
        public string GetImageHttpPathMedium(string imageFileName)
        {
            return this.GetRelativeImageUrl(portal.MaxCatalogItemMediumWidth, imageFileName, false);
        }

        /// <summary>
        /// Get the small image relative path.
        /// </summary>
        /// <param name="imageFileName">Image file name.</param>
        /// <returns>Returns the small image relative path.</returns>
        public string GetImageHttpPathSmall(string imageFileName)
        {
            return this.GetRelativeImageUrl(portal.MaxCatalogItemSmallWidth, imageFileName, false);
        }

        /// <summary>
        /// Get the thumbnail image relative path.
        /// </summary>
        /// <param name="imageFileName">Image file name.</param>
        /// <returns>Returns the thumbnail image relative path.</returns>
        public string GetImageHttpPathThumbnail(string imageFileName)
        {
            return this.GetRelativeImageUrl(portal.MaxCatalogItemThumbnailWidth, imageFileName, false);
        }

        /// <summary>
        /// Get the cross sell image relative path.
        /// </summary>
        /// <param name="imageFileName">Image file name.</param>
        /// <returns>Returns the cross sell image relative path.</returns>
        public string GetImageHttpPathCrossSell(string imageFileName)
        {
            return this.GetRelativeImageUrl(portal.MaxCatalogItemCrossSellWidth, imageFileName, false);
        }

        /// <summary>
        /// Get the small thumbnail image relative path.
        /// </summary>
        /// <param name="imageFileName">Image file name.</param>
        /// <returns>Returns the small thumbnail image relative path.</returns>
        public string GetImageHttpPathSmallThumbnail(string imageFileName)
        {
            return this.GetRelativeImageUrl(portal.MaxCatalogItemSmallThumbnailWidth, imageFileName, true);
        }

        /// <summary>
        /// Get Image By size
        /// </summary>
        /// <param name="imageSize">Image file size</param>
        /// <param name="imageFileName">Image file name.</param>
        /// <returns>Returns the image by specified size</returns>
        public string GetImageBySize(int imageSize, string imageFileName)
        {
            return this.GetRelativeImageUrl(imageSize, imageFileName, false);
        }

        /// <summary>
        /// To get store logo by size
        /// </summary>
        /// <param name="imageSize">System.Drawing.Size imageSize</param>
        /// <param name="imageFileName">string imageFileName</param>
        /// <returns>returns Store Logo By Size</returns>
        public string GetStoreLogoBySize(System.Drawing.Size imageSize, string imageFileName)
        {
            string orginalImageDirectory = string.Format("{0}images/catalog/original/turnkey/{1}/", environmentConfig.DataPath, portal.PortalId);
            string returnUrl = string.Empty;
            string imageFileFullName = string.Empty;
            ZNodeStorageProviderFile storageProvider = new ZNodeStorageProviderFile();
            if (imageFileName == null)
            {
                imageFileName = string.Empty;
            }
            else
            {
                imageFileFullName = Path.Combine(orginalImageDirectory, imageFileName);
            }
            if (!string.IsNullOrEmpty(imageFileName))
            {
                // If file exists and resized image folder and no new image uploaded in original folder then use already resized image.

                if (!storageProvider.Exists(imageFileFullName))
                {
                    string originalImageDirectory = string.Format("{0}images/catalog/{1}/", environmentConfig.DataPath, imageSize.Width);
                    string originalImageFileFullName = Path.Combine(originalImageDirectory, imageFileName);
                    string LogoOrignalImagePath = Path.Combine(environmentConfig.OriginalImagePath, imageFileName);
                    if (storageProvider.Exists(originalImageFileFullName))
                    {
                        returnUrl = originalImageFileFullName;
                    }
                    else
                    {
                        returnUrl = this.ResizeImage(originalImageFileFullName, imageSize.Height, imageSize.Width, originalImageDirectory);
                    }
                }
                else
                {
                    string resizedImageDirectory = string.Format("{0}images/catalog/{2}/turnkey/{1}/", environmentConfig.DataPath, portal.PortalId, imageSize.Width);
                    string resizedImageFullName = Path.Combine(resizedImageDirectory, imageFileName);
                    if (storageProvider.Exists(resizedImageFullName))
                    {
                        returnUrl = resizedImageFullName;
                    }
                    else
                    {
                        returnUrl = this.ResizeImage(imageFileFullName, imageSize.Height, imageSize.Width, resizedImageDirectory);
                    }
                }
            }
            else
            {
                returnUrl = GetImageHttpPathSmall(string.Format("Turnkey/{0}/{1}", portal.PortalId, Path.GetFileName(imageFileName)));
            }

            return returnUrl;

        }

        #endregion

        #region Helper Methods

        /// <summary>
        /// Get the relative image path to a resized image. If the resized image does not exist then it will be created. If the image does not exist.
        /// then the "No Image" image will be returned.
        /// </summary>
        /// <param name="imageSize">Image Size you want to get.</param>
        /// <param name="imageFileName">The name of the image you want to get.</param>
        /// <param name="cropImage">Indicates whether to crop the image.</param>
        /// <param name="imageFileExists">Returns true if image file exists else false.</param>
        /// <returns>Returns the resized image file path.</returns>
        public string GetImageHttpPath(int imageSize, string imageFileName, bool cropImage, out bool imageFileExists)
        {
            string imageFileFullName = string.Empty;
            string imageFilePath = string.Empty;
            string fileName = string.Empty;
            if (Equals(environmentConfig, null))
            {
                imageFileExists = false;
                return fileName;
            }
            // Build up a path for our resized image.
            imageFilePath = Path.Combine(environmentConfig.ImagePath, imageSize.ToString()) + "/";
            imageFileFullName = Path.Combine(imageFilePath, imageFileName);
            ZNodeStorageProviderFile storageProvider = new ZNodeStorageProviderFile();
            // If file exists and resized image folder and no new image uploaded in original folder then use already resized image.
            if (storageProvider.Exists(imageFileFullName))
            {
                imageFileExists = true;
                return imageFileFullName;
            }
            else
            {
                string originalFileName = cropImage ? imageFileName.ToLower().Replace("-swatch.", ".") : imageFileName;

                string orignalImagePath = string.Empty;
                if (this._UseSiteConfig)
                {
                    orignalImagePath = Path.Combine(Path.GetDirectoryName(portal.ImageNotAvailablePathUrl), imageFileName);
                }
                else
                {
                    orignalImagePath = Path.Combine(environmentConfig.OriginalImagePath, originalFileName);
                }

                imageFileExists = false;

                // Check is file exist in Original folder.
                if (storageProvider.Exists(orignalImagePath))
                {
                    // Resize the image for the current request image size.
                    fileName = this.ResizeImage(orignalImagePath, imageSize, imageFilePath);

                    // Crop the image only for swatch.
                    if (cropImage)
                    {
                        int maxSmallThumnailWidthHeight = portal.MaxCatalogItemSmallThumbnailWidth;

                        string croppedImageFileName = Path.GetFileName(this.CropImage(orignalImagePath, maxSmallThumnailWidthHeight, maxSmallThumnailWidthHeight, fileName.ToLower().Replace("-swatch.", ".")));
                        fileName = Path.Combine(imageFilePath, croppedImageFileName);
                    }
                }
                else
                {
                    // If source file doesn't exist in Original folder then return the Image Not Available file.
                    string noImageRelativePath = string.Empty;

                    // Get the no image file from store settings.
                    if (portal.ImageNotAvailablePathUrl.Trim().Length > 0)
                    {
                        noImageRelativePath = portal.ImageNotAvailablePathUrl;
                        if (storageProvider.Exists(noImageRelativePath))
                        {
                            // Set to use the SiteConfig for source file lookup.
                            this._UseSiteConfig = true;

                            // Call the current method recursively with the site "Not Available" image name.
                            fileName = this.GetImageHttpPath(imageSize, Path.GetFileName(noImageRelativePath), cropImage, out imageFileExists);

                            if (cropImage)
                            {
                                fileName = Path.Combine(imageFilePath, Path.GetFileNameWithoutExtension(fileName) + "-swatch" + Path.GetExtension(fileName));
                            }
                        }
                        else
                        {
                            fileName = imageFileFullName;
                        }
                    }
                    else
                    {
                        noImageRelativePath = Path.Combine(environmentConfig.OriginalImagePath, this.imageNotAvailablePath);
                        if (storageProvider.Exists(noImageRelativePath))
                        {
                            // Call the current method recursively with the "Original" folder this.noImageFileName.
                            fileName = this.GetImageHttpPath(imageSize, this.imageNotAvailablePath, cropImage, out imageFileExists);

                            if (cropImage)
                            {
                                fileName = Path.Combine(imageFilePath, Path.GetFileNameWithoutExtension(fileName) + "-swatch" + Path.GetExtension(fileName));
                            }
                        }
                        else
                        {
                            // Image not available in Data/Default/Images and Data/Default/Images/Catalog/Original folders.
                            fileName = imageFileFullName;
                        }
                    }
                }

                // Reset to use EnvironmentConfig for Original file folder.
                this._UseSiteConfig = false;

                return fileName;
            }
        }

        /// <summary>
        /// Resizing the image size and storing it in the respective folder.
        /// </summary>
        /// <param name="relativeImageFilePath">Relative Image file path.</param>
        /// <param name="maxHeight">Maximum Height of the image.</param>
        /// <param name="maxWidth">Maximum width of the image.</param>
        /// <param name="saveToFullPath">Physical path</param>
        /// <returns>Returns the resised image relative path.</returns>
        public string ResizeImage(string relativeImageFilePath, int maxHeight, int maxWidth, string saveToFullPath)
        {
            string returnUrl = string.Empty;
            string fullName = string.Empty;
            string fileName = Path.GetFileName(relativeImageFilePath);
            string[] str = relativeImageFilePath.Split(new char[] { '/' });
            if (this._UseSiteConfig)
            {
                fullName = Path.Combine(Path.GetDirectoryName(portal.ImageNotAvailablePathUrl), fileName);
            }
            else
            {
                if (str.Length == 9)
                {
                    fullName = environmentConfig.OriginalImagePath + str[6] + "/" + str[7] + "/" + fileName;
                }
                else
                {
                    fullName = environmentConfig.OriginalImagePath + fileName;
                }
            }
            ZNodeStorageProviderFile storageProvider = new ZNodeStorageProviderFile();
            byte[] fileData = storageProvider.ReadBinaryStorage(fullName);
            MemoryStream ms = new MemoryStream(fileData);
            Image sourceImage = Image.FromStream(ms);
            decimal scaleFactor;
            decimal originalProportion = sourceImage.Width / sourceImage.Height;
            decimal resizeProportion = maxWidth / maxHeight;
            if (originalProportion > resizeProportion)
            {
                scaleFactor = Convert.ToDecimal(maxWidth) / Convert.ToDecimal(sourceImage.Width);
            }
            else
            {
                scaleFactor = Convert.ToDecimal(maxHeight) / Convert.ToDecimal(sourceImage.Height);
            }

            int newWidth = (int)Math.Round((sourceImage.Width * scaleFactor));
            int newHeight = (int)Math.Round((sourceImage.Height * scaleFactor));
            Bitmap thumbnailBitmap = new Bitmap(newWidth, newHeight);

            Graphics thumbnailGraph = Graphics.FromImage(thumbnailBitmap);
            thumbnailGraph.CompositingQuality = CompositingQuality.HighQuality;
            thumbnailGraph.SmoothingMode = SmoothingMode.HighQuality;
            thumbnailGraph.InterpolationMode = InterpolationMode.HighQualityBicubic;
            thumbnailGraph.Clear(Color.White);

            Rectangle imageRectangle = new Rectangle(0, 0, newWidth, newHeight);
            thumbnailGraph.DrawImage(sourceImage, imageRectangle);

            MemoryStream stream = new MemoryStream();
            Image imageToSave = thumbnailBitmap;
            imageToSave.Save(stream, ImageFormat.Jpeg);
            imageToSave.Dispose();
            stream.Seek(0, SeekOrigin.Begin);
            fileData = new byte[stream.Length];
            stream.Read(fileData, 0, fileData.Length);

            storageProvider.WriteBinaryStorage(fileData, Path.Combine(saveToFullPath, fileName));

            thumbnailGraph.Dispose();
            thumbnailBitmap.Dispose();
            sourceImage.Dispose();

            ms.Close();

            return Path.Combine(saveToFullPath, fileName);
        }

        /// <summary>
        /// Resize the image without losing quality
        /// </summary>
        /// <param name="orignalImagePath">Original Image Path</param>
        /// <param name="maxSize">Maximum file resize size.</param>
        /// <param name="saveToFullPath">Save to physical path.</param>
        /// <returns>Returns the image physical path with file name.</returns>
        public string ResizeImage(string orignalImagePath, int maxSize, string saveToFullPath)
        {
            string returnUrl = string.Empty;
            string fullName = string.Empty;
            string fileName = Path.GetFileName(orignalImagePath);
            string[] str = orignalImagePath.Split(new char[] { '/' });
            ZNodeStorageProviderFile storageProvider = new ZNodeStorageProviderFile();
            if (this._UseSiteConfig)
            {
                fullName = Path.Combine(Path.GetDirectoryName(portal.ImageNotAvailablePathUrl), fileName);
            }
            else
            {
                if (str.Length == 9)
                {
                    fullName = environmentConfig.OriginalImagePath + str[6] + "/" + str[7] + "/" + fileName;
                    saveToFullPath = saveToFullPath + str[6] + "/" + str[7] + "/";
                }
                else
                {
                    fullName = environmentConfig.OriginalImagePath + fileName;
                }
            }

            byte[] bytes = storageProvider.ReadBinaryStorage(fullName);

            if (Equals(bytes, null) || (Equals(bytes.Length, 0)))
            {
                return string.Empty;
            }

            Image sourceImage;
            MemoryStream ms = new MemoryStream(bytes);
            sourceImage = Image.FromStream(ms);

            decimal scaleFactor;
            if (sourceImage.Width >= sourceImage.Height)
            {
                scaleFactor = Convert.ToDecimal(maxSize) / Convert.ToDecimal(sourceImage.Width);
            }
            else
            {
                scaleFactor = Convert.ToDecimal(maxSize) / Convert.ToDecimal(sourceImage.Height);
            }

            int newWidth = (int)Math.Round((sourceImage.Width * scaleFactor));
            int newHeight = (int)Math.Round((sourceImage.Height * scaleFactor));
            Bitmap thumbnailBitmap = new Bitmap(newWidth, newHeight);

            Graphics thumbnailGraph = Graphics.FromImage(thumbnailBitmap);
            thumbnailGraph.CompositingQuality = CompositingQuality.HighQuality;
            thumbnailGraph.SmoothingMode = SmoothingMode.HighQuality;
            thumbnailGraph.InterpolationMode = InterpolationMode.HighQualityBicubic;
            thumbnailGraph.Clear(Color.White);

            Rectangle imageRectangle = new Rectangle(0, 0, newWidth, newHeight);
            thumbnailGraph.DrawImage(sourceImage, imageRectangle);

            MemoryStream stream = new MemoryStream();
            Image imageToSave = thumbnailBitmap;
            imageToSave.Save(stream, ImageFormat.Jpeg);
            imageToSave.Dispose();
            stream.Seek(0, SeekOrigin.Begin);
            bytes = new byte[stream.Length];
            stream.Read(bytes, 0, bytes.Length);

            storageProvider.WriteBinaryStorage(bytes, Path.Combine(saveToFullPath, fileName));

            thumbnailGraph.Dispose();
            thumbnailBitmap.Dispose();
            sourceImage.Dispose();

            ms.Close();

            return Path.Combine(saveToFullPath, fileName);
        }

        /// <summary>
        /// Method for cropping an image.
        /// </summary>
        /// <param name="orignalImagePath">Original Image path</param>
        /// <param name="width">New width of the image.</param>
        /// <param name="height">New height of the image</param>
        /// <param name="saveToFullPath">Save to full path.</param>        
        /// <returns>Returns the image relative path.</returns>
        public string CropImage(string orignalImagePath, int width, int height, string saveToFullPath)
        {
            string fileName = orignalImagePath;
            ZNodeStorageProviderFile storageProvider = new ZNodeStorageProviderFile();
            try
            {
                // Original image
                byte[] fileData = storageProvider.ReadBinaryStorage(fileName);
                MemoryStream stream = new MemoryStream(fileData);
                Image imgPhoto = Image.FromStream(stream);

                int targetW = width;
                int targetH = height;
                int targetX = 0;
                int targetY = 0;

                int pointX = imgPhoto.Width / 2;
                int pointY = imgPhoto.Height / 2;

                targetX = pointX - (targetW / 2);
                targetY = pointY - (targetH / 2) - 2;

                Bitmap bmpPhoto = new Bitmap(targetW, targetH, PixelFormat.Format24bppRgb);
                bmpPhoto.SetResolution(80, 60);

                Graphics gfxPhoto = Graphics.FromImage(bmpPhoto);
                gfxPhoto.CompositingQuality = CompositingQuality.HighQuality;
                gfxPhoto.SmoothingMode = SmoothingMode.HighQuality;
                gfxPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;
                gfxPhoto.PixelOffsetMode = PixelOffsetMode.HighQuality;

                gfxPhoto.DrawImage(imgPhoto, new Rectangle(0, 0, targetW, targetH), targetX, targetY, targetW, targetH, GraphicsUnit.Pixel);

                fileName = Path.Combine(Path.GetDirectoryName(saveToFullPath), Path.GetFileNameWithoutExtension(saveToFullPath) + "-Swatch" + Path.GetExtension(fileName));

                stream = new MemoryStream();
                Image imageToSave = bmpPhoto;
                imageToSave.Save(stream, ImageFormat.Jpeg);
                imageToSave.Dispose();
                stream.Seek(0, SeekOrigin.Begin);
                fileData = new byte[stream.Length];
                stream.Read(fileData, 0, fileData.Length);

                storageProvider.WriteBinaryStorage(fileData, fileName);

                // Dispose of all the objects to prevent memory leaks
                imgPhoto.Dispose();
                bmpPhoto.Dispose();
                gfxPhoto.Dispose();
            }
            catch (Exception ex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.StackTrace);
            }

            return fileName;
        }

        /// <summary>
        /// Resize the mobile splash images. Splash image width hard-coded as 320 and 640.
        /// </summary>
        /// <param name="relativeImageFilePath">Relative Image File Path.</param>
        public void ResizeMobileSplashImage(string relativeImageFilePath)
        {
            string splashImageWidthSmallDir = Path.Combine(environmentConfig.DataPath, "images/splash/320");
            string splashImageWidthLargeDir = Path.Combine(environmentConfig.DataPath, "images/splash/640");

            this.ResizeImage(relativeImageFilePath, 320, splashImageWidthSmallDir + "/");
            this.ResizeImage(relativeImageFilePath, 640, splashImageWidthLargeDir + "/");
        }

        /// <summary>
        /// Get a resized image relative path.
        /// </summary>
        /// <param name="imageSize">Image Size</param>
        /// <param name="imageFileName">Image file name.</param>
        /// <param name="cropImage">Crop Image</param>
        /// <returns>Returns the resized relative image path.</returns>
        public string GetRelativeImageUrl(int imageSize, string imageFileName, bool cropImage)
        {
            string returnFileName = string.Empty;
            bool isImageFileExists = true;

            if (string.IsNullOrEmpty(imageFileName))
            {
                imageFileName = this.imageNotAvailablePath;
            }

            returnFileName = this.GetImageHttpPath(imageSize, imageFileName, cropImage, out isImageFileExists);

            if (!isImageFileExists && !this.isAlreadyChecked)
            {
                isImageFileExists = this.isAlreadyChecked = true;

                // Call the current function recursively to load image on first time.
                returnFileName = this.GetRelativeImageUrl(imageSize, imageFileName, cropImage);
            }

            // Reset the recursion check to default value.
            this.isAlreadyChecked = false;
            ZNodeStorageProviderFile storageProvider = new ZNodeStorageProviderFile();
            return storageProvider.HttpPath(returnFileName);
        }

        /// <summary>
        /// Create a Directory here.
        /// </summary>
        /// <param name="path">Directory name to create.</param>
        private void CreateDirectory(string path)
        {
            if (!System.IO.Directory.Exists(path))
            {
                System.IO.Directory.CreateDirectory(path);
            }
        }

        #endregion

        #region Private Method

        /// <summary>
        /// To get environment config from session 
        /// </summary>
        /// <typeparam name="T">T</typeparam>
        /// <param name="key">string key</param>
        /// <returns>returns environment config from session</returns>
        private static T GetEnvironmentConfig<T>(string key)
        {
            if (Equals(HttpContext.Current.Session[MvcAdminConstants.AccountKey], null) || Equals(HttpContext.Current.Session[MvcAdminConstants.EnvironmentConfigKey], null))
            {
                HttpContext.Current.Response.Redirect("/account/login?returnUrl=" + HttpContext.Current.Request.RawUrl + "");
            }

            if (HttpContext.Current.Session != null)
            {
                var o = HttpContext.Current.Session[key];
                if (o is T)
                {
                    return (T)o;
                }
            }

            return default(T);
        }

        #endregion
    }
}
