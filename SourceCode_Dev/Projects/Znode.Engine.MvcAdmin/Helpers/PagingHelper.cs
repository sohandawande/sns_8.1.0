﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using Znode.Engine.MvcAdmin.Models;

namespace Znode.Engine.MvcAdmin.Helpers
{
    public static class PagingHelper
    {
        public static MvcHtmlString CustomPager(this WebGrid grid, HtmlHelper html, string recordPerPageFieldName = "recordPerPage", string pageFieldName = "page", string SortFieldName = "sort", string SortDirFieldName = "sortdir")
        {
            GridPagerModel gridModel = new GridPagerModel();
            gridModel.PageCount = grid.PageCount;
            gridModel.PageIndex = grid.PageIndex;
            gridModel.PageSize = grid.RowsPerPage;
            gridModel.Sort = grid.SortColumn;
            gridModel.SortDir = grid.SortDirection == SortDirection.Ascending ? "ASC" : "DESC";
            gridModel.SortFieldName = SortFieldName;
            gridModel.SortDirFieldName = SortDirFieldName;

            gridModel.RecordPerPageFieldName = recordPerPageFieldName;
            gridModel.PageFieldName = pageFieldName;

            gridModel.PageSizeOptions = GetPageSizeOptions();

            MvcHtmlString result = html.Partial("_GridPager", gridModel);
            return result;
        }

        private static IEnumerable<SelectListItem> GetPageSizeOptions()
        {
            IEnumerable<SelectListItem> PageSize = Enumerable.Empty<SelectListItem>();

            string[] pageSize = new string[] { "10", "20", "50", "100" };

            PageSize = pageSize.Select(c => new SelectListItem
            {
                Value = c,
                Text = c,
            });
            return PageSize;
        }
    }
}