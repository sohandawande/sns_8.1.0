﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Xml.Linq;
using System.Xml.Serialization;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Models.Enum;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.Extensions;
using Znode.Engine.MvcAdmin.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Helpers
{
    public static class FilterHelpers
    {
        /// <summary>
        /// Get filter criteria column list
        /// </summary>
        /// <param name="filterCondition">filter collection</param>
        /// <param name="listType">integer listType</param>
        /// <returns></returns>
        public static FilterColumnListModel GetFilterColumnList(FilterCollection filterCondition, int listType)
        {
            Dictionary<string, string> filterKeyValue = SeparateKeyValuePair(filterCondition, null);

            FilterColumnListModel model = GetConvertedFilterColumnModelFromXml(listType, filterKeyValue);

            return model;
        }

        /// <summary>
        /// Get XML configuration
        /// </summary>
        /// <param name="listType">List type</param>
        /// <returns></returns>
        public static string GetXmlConfiguration(int listType)
        {
            ConfigurationReaderAgent objReader = new ConfigurationReaderAgent();
            return objReader.GetFilterConfigurationXML(listType);
        }

        /// <summary>
        /// Get dynamic column and filters criteria from configuration XML
        /// </summary>
        /// <param name="listType">integer list type</param>
        /// <param name="filterKeyValue">filter citeria values</param>
        /// <returns>Returns FilterColumnListModel</returns>
        private static FilterColumnListModel GetConvertedFilterColumnModelFromXml(int listType, Dictionary<string, string> filterKeyValue)
        {

            string XMlstring = string.Empty;
            if (Equals(HttpContext.Current.Session[DynamicGridConstants.XMLStringSesionKey], null) || Convert.ToString(HttpContext.Current.Session[DynamicGridConstants.listTypeSesionKey]) != listType.ToString())
            {
                XMlstring = GetXmlConfiguration(listType);
                HttpContext.Current.Session[DynamicGridConstants.XMLStringSesionKey] = XMlstring;
            }
            else
            {
                XMlstring = (string)HttpContext.Current.Session[DynamicGridConstants.XMLStringSesionKey];
            }
            XDocument xmlDoc = XDocument.Parse(XMlstring);

            HttpContext.Current.Session[DynamicGridConstants.xmlConfigDocSessionKey] = xmlDoc;

            var serializer = new XmlSerializer(typeof(FilterColumnModel));
            int[] _fieldscollection = (int[])HttpContext.Current.Session[DynamicGridConstants.FieldsCollectionSessionKey];

            List<FilterColumnModel> model = new List<FilterColumnModel>();
            if (!Equals(_fieldscollection, null))
            {
                model = (from xml in xmlDoc.Descendants(DynamicGridConstants.columnKey)
                         where xml.Element(DynamicGridConstants.isallowsearchKey).Value.Equals(Convert.ToString(DynamicGridConstants.Yes)) && !_fieldscollection.Contains(int.Parse(xml.Element("id").Value))
                         select new FilterColumnModel
                         {
                             ColumnName = xml.Element(DynamicGridConstants.nameKey).Value,
                             HeaderText = xml.Element(DynamicGridConstants.headertextKey).Value,
                             DataType = xml.Element(DynamicGridConstants.datatypeKey).Value,
                             Value = Equals(filterKeyValue, null) ? string.Empty : filterKeyValue.ContainsKey(xml.Element(DynamicGridConstants.nameKey).Value) ? filterKeyValue[xml.Element(DynamicGridConstants.nameKey).Value] : string.Empty
                         }).ToList();
            }
            else
            {
                model = (from xml in xmlDoc.Descendants(DynamicGridConstants.columnKey)
                         where xml.Element(DynamicGridConstants.isallowsearchKey).Value.Equals(Convert.ToString(DynamicGridConstants.Yes))
                         select new FilterColumnModel
                         {
                             ColumnName = xml.Element(DynamicGridConstants.nameKey).Value,
                             HeaderText = xml.Element(DynamicGridConstants.headertextKey).Value,
                             DataType = xml.Element(DynamicGridConstants.datatypeKey).Value,
                             Value = Equals(filterKeyValue, null) ? string.Empty : filterKeyValue.ContainsKey(xml.Element(DynamicGridConstants.nameKey).Value) ? filterKeyValue[xml.Element(DynamicGridConstants.nameKey).Value] : string.Empty
                         }).ToList();
            }

            return new FilterColumnListModel { FilterColumnList = model, GridColumnList = GetDynamicGridView(XMlstring, DynamicGridConstants.columnKey) };
        }

        /// <summary>
        /// Get Dynamic grid action key values
        /// </summary>
        /// <param name="model">Reference of FilterCollectionDataModel</param>
        private static void GetActionKeyValues(FilterCollectionDataModel model)
        {
            Dictionary<string, string> actionKeys = new Dictionary<string, string>();
            XDocument xmlDoc = (XDocument)HttpContext.Current.Session[DynamicGridConstants.xmlConfigDocSessionKey];

            foreach (var xml in xmlDoc.Descendants(DynamicGridConstants.columnKey))
            {
                if (xml.Element(DynamicGridConstants.isvisibleKey).Value.Equals(DynamicGridConstants.Yes))
                {
                    switch (xml.Element(DynamicGridConstants.nameKey).Value)
                    {
                        case DynamicGridConstants.EditKey:
                            model.EditActionUrl = xml.Element(DynamicGridConstants.editactionurlKey).Value;
                            model.EditParamField = xml.Element(DynamicGridConstants.editparamfieldKey).Value;
                            model.DisplayTextEdit = xml.Element(DynamicGridConstants.displaytextKey).Value;
                            break;
                        case DynamicGridConstants.DeleteKey:
                            model.DeleteActionUrl = xml.Element(DynamicGridConstants.deleteactionurlKey).Value;
                            model.DeleteParamField = xml.Element(DynamicGridConstants.deleteparamfieldKey).Value;
                            model.DisplayTextDelete = xml.Element(DynamicGridConstants.displaytextKey).Value;
                            break;
                        case DynamicGridConstants.ViewKey:
                            model.ViewActionUrl = xml.Element(DynamicGridConstants.viewactionurlKey).Value;
                            model.ViewParamField = xml.Element(DynamicGridConstants.viewparamfieldKey).Value;
                            model.DisplayTextView = xml.Element(DynamicGridConstants.displaytextKey).Value;
                            break;
                        case DynamicGridConstants.ImageKey:
                            model.ImageActionUrl = string.Empty;
                            model.ImageParamField = xml.Element(DynamicGridConstants.imageparamfieldKey).Value;
                            break;
                        case DynamicGridConstants.ManageKey:
                            model.ManageActionUrl = xml.Element(DynamicGridConstants.manageactionurlKey).Value;
                            model.ManageParamField = xml.Element(DynamicGridConstants.manageparamfieldKey).Value;
                            model.DisplayTextManage = xml.Element(DynamicGridConstants.displaytextKey).Value;
                            break;
                        case DynamicGridConstants.CopyKey:
                            model.CopyActionUrl = xml.Element(DynamicGridConstants.copyactionurlKey).Value;
                            model.CopyParamField = xml.Element(DynamicGridConstants.copyparamfieldKey).Value;
                            model.DisplayTextCopy = xml.Element(DynamicGridConstants.displaytextKey).Value;
                            break;
                        case DynamicGridConstants.CheckboxKey:
                            model.CheckboxActionUrl = xml.Element(DynamicGridConstants.checkboxactionurlKey).Value;
                            model.CheckboxParamField = xml.Element(DynamicGridConstants.checkboxparamfieldKey).Value;
                            break;
                        case DynamicGridConstants.DisableKey:
                            model.EnableDisableActionUrl = xml.Element(DynamicGridConstants.enabledisableactionurlKey).Value;
                            model.EnableDisableParamField = xml.Element(DynamicGridConstants.enabledisableparamfieldKey).Value;
                            break;


                    }
                    //Generate link column

                    if (xml.Element(DynamicGridConstants.isallowlinkKey).Value.Equals(DynamicGridConstants.Yes))
                    {
                        model.IsLinkActionUrl = xml.Element(DynamicGridConstants.islinkactionurlKey).Value;
                        model.IsLinkParamField = xml.Element(DynamicGridConstants.islinkparamfieldKey).Value;
                        model.IsLinkFieldName = xml.Element(DynamicGridConstants.nameKey).Value;
                    }
                    if (xml.Element(DynamicGridConstants.ischeckboxKey).Value.Equals(DynamicGridConstants.Yes))
                    {
                        model.CheckboxParamField = xml.Element(DynamicGridConstants.checkboxparamfieldKey).Value;
                    }
                    if (xml.Element(DynamicGridConstants.iscontrolKey).Value.Equals(DynamicGridConstants.Yes))
                    {
                        model.ControlParamField = xml.Element(DynamicGridConstants.controlparamfieldKey).Value;
                    }

                }
            }
        }

        /// <summary>
        /// Format Action links / Set Query string single parameter and multi parameter.
        /// </summary>
        /// <typeparam name="T">Generic type</typeparam>
        /// <param name="x">Reference of dynamic object</param>
        /// <param name="keyName">Action key name</param>
        /// <param name="displayText">Display text in column cell</param>
        /// <param name="actionUrl">Redirect action Url</param>
        /// <param name="paramField">Parameter field for action link</param>
        private static void SetQueryString<T>(T x, string keyName, string displayText, string actionUrl, string paramField)
        {
            string queryParm = string.Empty;
            string _actionurl = string.Empty;
            string _firstDeleteId = string.Empty;
            if (paramField.IndexOf(',') >= 0)
            {
                foreach (string fieldname in paramField.Split(','))
                {
                    if (queryParm.Equals(string.Empty))
                    {
                        queryParm = "?" + fieldname + "=" + x.GetProperty(fieldname).ToString();
                        _firstDeleteId = Equals(DynamicGridConstants.DeleteKey, keyName) ? x.GetProperty(fieldname).ToString() : string.Empty;
                    }
                    else
                    {
                        queryParm += "&" + fieldname + "=" + x.GetProperty(fieldname).ToString();
                    }
                }

                _actionurl = Equals(DynamicGridConstants.DeleteKey, keyName) ? string.IsNullOrEmpty(displayText) ? actionUrl + queryParm : actionUrl + queryParm + "$" + _firstDeleteId :
                    string.IsNullOrEmpty(displayText) ? actionUrl + queryParm : actionUrl + queryParm;

                x.SetPropertyValue(keyName, _actionurl);
            }
            else
            {
                _firstDeleteId = Equals(DynamicGridConstants.DeleteKey, keyName) ? x.GetProperty(paramField).ToString() : string.Empty;
                x.SetPropertyValue(keyName, Equals(DynamicGridConstants.DeleteKey, keyName) ? actionUrl + "/" + x.GetProperty(paramField).ToString() + "$" + _firstDeleteId : actionUrl + "/" + x.GetProperty(paramField).ToString());
            }
        }

        /// <summary>
        /// Format Action links / Set Query string single parameter and multi parameter. 
        /// </summary>
        /// <typeparam name="T">Generic type</typeparam>
        /// <param name="x">Reference of dynamic object</param>
        /// <param name="keyName">Action key name</param>
        /// <param name="actionUrl">Redirect action Url</param>
        /// <param name="paramField">Parameter field for action link</param>
        private static void SetQueryString<T>(T x, string keyName, string actionUrl, string paramField)
        {
            string displayText = string.Empty;
            string _actionurl = string.Empty;
            if (paramField.IndexOf(',') >= 0)
            {
                displayText = Convert.ToBoolean(x.GetProperty(paramField.Split(',')[1])) ? "Disable" : "Enable";
                _actionurl = displayText + "$" + actionUrl + "/" + x.GetProperty(paramField.Split(',')[0]).ToString();

                x.SetPropertyValue(keyName, _actionurl);
            }
            else
            {
                x.SetPropertyValue(keyName, actionUrl + "/" + x.GetProperty(paramField).ToString());
            }
        }

        /// <summary>
        /// Format Action links / Set Query string single parameter and multi parameter.
        /// </summary>
        /// <typeparam name="T">Generic type</typeparam>
        /// <param name="x">Reference of dynamic object</param>
        /// <param name="keyName">Action key name</param>
        /// <param name="displayText">Display text in column cell</param>
        /// <param name="actionUrl">Redirect action Url</param>
        /// <param name="paramField">Parameter field for action link</param>
        /// <param name="paramField">IsLinkFieldName for create link</param>
        private static void SetQueryString<T>(T x, string keyName, string displayText, string actionUrl, string paramField, string IsLinkFieldName)
        {
            string queryParm = string.Empty;
            string _actionurl = string.Empty;
            if (paramField.IndexOf(',') >= 0)
            {
                foreach (string fieldname in paramField.Split(','))
                {
                    if (queryParm.Equals(string.Empty))
                        queryParm = "?" + fieldname + "=" + x.GetProperty(fieldname).ToString();
                    else
                        queryParm += "&" + fieldname + "=" + x.GetProperty(fieldname).ToString();
                }

                _actionurl = x.GetProperty(IsLinkFieldName).ToString() + "$" + actionUrl + queryParm;

                x.SetPropertyValue(keyName, _actionurl);
            }
            else
            {
                if (!string.IsNullOrEmpty(paramField))
                    x.SetPropertyValue(keyName, x.GetProperty(IsLinkFieldName).ToString() + "$" + actionUrl + "/" + x.GetProperty(paramField).ToString());
                else
                    x.SetPropertyValue(keyName, x.GetProperty(IsLinkFieldName).ToString() + "$" + actionUrl);

            }
        }

        /// <summary>
        /// Format Action links / Set Query string single parameter and multi parameter.
        /// </summary>
        /// <typeparam name="T">Generic type</typeparam>
        /// <param name="x">Reference of dynamic object</param>
        /// <param name="keyName">Action key name</param>
        /// <param name="paramField">Parameter field for action link</param>
        private static void SetQueryString<T>(T x, string keyName, string paramField)
        {
            string queryParm = string.Empty;
            string _actionurl = string.Empty;
            if (paramField.IndexOf(',') >= 0)
            {
                x.SetPropertyValue(keyName, x.GetProperty(paramField.Split(',')[0]).ToString() + "$" + x.GetProperty(paramField.Split(',')[1]).ToString());
            }
            else
            {
                x.SetPropertyValue(keyName, x.GetProperty(paramField).ToString());
            }
        }

        /// <summary>
        /// Seperate filters key values
        /// </summary>
        /// <param name="filterCondition">filter collection</param>
        /// <param name="filterKeyValue">filters key values</param>
        /// <returns>Returns Dictionary</returns>
        private static Dictionary<string, string> SeparateKeyValuePair(FilterCollection filterCondition, Dictionary<string, string> filterKeyValue)
        {
            if (!Equals(filterCondition, null))
            {
                filterKeyValue = new Dictionary<string, string>();
                foreach (var item in filterCondition)
                {
                    if (!filterKeyValue.ContainsKey(item.FilterName))
                    {
                        filterKeyValue.Add(item.FilterName, item.FilterValue);
                    }

                }
            }
            return filterKeyValue;
        }

        /// <summary>
        /// Get dynamic gris column names
        /// </summary>
        /// <param name="xmlString">xmlString</param>
        /// <param name="tableName">tableName</param>
        /// <returns>Returns dynamic List</returns>
        public static List<dynamic> GetDynamicGridView(string xmlString, string tableName)
        {
            try
            {
                return ConvertXmlToList(xmlString, tableName);
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        /// <summary>
        /// Method is used for convert Xml to dynamic list
        /// </summary>
        /// <param name="xmlString">xml string</param>
        /// <param name="tableName">tableName</param>
        /// <returns>Returns dynamic list</returns>
        public static List<dynamic> ConvertXmlToList(string xmlString, string tableName)
        {

            DataSet ds = new DataSet();
            try
            {
                using (StringReader stringReader = new StringReader(xmlString))
                {
                    ds.ReadXml(stringReader);
                }
                return DataTableToList(ds.Tables[tableName]);
            }
            catch (Exception ex)
            {
                return null;
            }

            finally
            {
                ds.Dispose();
            }
        }

        /// <summary>
        /// Get column table format to dynamic list with dateFormat
        /// </summary>
        /// <param name="dataTable">DataTable</param>
        /// <returns>Returns dynamic list</returns>
        public static List<dynamic> DataTableToList(DataTable dataTable)
        {
            var result = new List<dynamic>();
            try
            {
                if (!Equals(dataTable, null) && dataTable.Rows.Count > 0)
                {
                    foreach (DataRow row in dataTable.Rows)
                    {
                        var obj = (IDictionary<string, object>)new ExpandoObject();
                        foreach (DataColumn col in dataTable.Columns)
                        {
                            if (col.ColumnName.Contains(DynamicGridConstants.Date))
                            {
                                DateTime startDate = Convert.ToDateTime(row[col.ColumnName]);
                                obj.Add(col.ColumnName, startDate.ToString(DynamicGridConstants.DateFormat));
                            }
                            else
                            {
                                obj.Add(col.ColumnName, row[col.ColumnName]);
                            }

                        }
                        result.Add(obj);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            finally
            {
                dataTable.Dispose();
            }

            return result;
        }

        /// <summary>
        /// Get dymanic grid model.
        /// function help to generate grid structure as dynamic.
        /// </summary>
        /// <typeparam name="T">Generic type</typeparam>
        /// <param name="model">filter collection model</param>
        /// <param name="dataModel">Generic type list model</param>
        /// <param name="listType">integer list type</param>
        /// <returns>Returns ReportModel</returns>
        public static ReportModel GetDynamicGridModel<T>(FilterCollectionDataModel model, List<T> dataModel, int listType)
        {
            ReportModel reportModel = new ReportModel();
            List<dynamic> gridList = new List<dynamic>();
            int tableWidth = 0;


            if (Convert.ToString(HttpContext.Current.Session[DynamicGridConstants.listTypeSesionKey]) != listType.ToString())
            {
                HttpContext.Current.Session[DynamicGridConstants.ColumnListSesionKey] = null;
                HttpContext.Current.Session[DynamicGridConstants.AdvanceFilterCollectionKey] = null;
                reportModel.FilterColumn = FilterHelpers.GetFilterColumnList(model.Filters, listType);
                gridList = reportModel.FilterColumn.GridColumnList;
                HttpContext.Current.Session[DynamicGridConstants.ColumnListSesionKey] = gridList;
            }
            else
            {
                reportModel.FilterColumn = FilterHelpers.GetFilterColumnList(model.Filters, listType);
                gridList = (List<dynamic>)HttpContext.Current.Session[DynamicGridConstants.ColumnListSesionKey];
            }

            HttpContext.Current.Session[DynamicGridConstants.listTypeSesionKey] = listType;



            List<WebGridColumn> columns = new List<WebGridColumn>();
            int pageSize = model.RecordPerPage;

            tableWidth = GenerateDynamicGridView(gridList, tableWidth, columns);
            reportModel.WebGridColumn = columns;

            dataModel = SetSortOrder(model, reportModel, dataModel);

            List<dynamic> dynamicList = new List<dynamic>();

            GetActionKeyValues(model);

            dataModel.ForEach(x =>
            {
                if (!Equals(model.EditParamField, null) && !Equals(model.EditActionUrl, null))
                {
                    SetQueryString(x, DynamicGridConstants.EditKey, model.DisplayTextEdit, model.EditActionUrl, model.EditParamField);
                }
                if (!Equals(model.DeleteParamField, null) && !Equals(model.DeleteActionUrl, null))
                {
                    SetQueryString(x, DynamicGridConstants.DeleteKey, model.DisplayTextDelete, model.DeleteActionUrl, model.DeleteParamField);
                }
                if (!Equals(model.ViewParamField, null) && !Equals(model.ViewActionUrl, null))
                {
                    SetQueryString(x, DynamicGridConstants.ViewKey, model.DisplayTextView, model.ViewActionUrl, model.ViewParamField);
                }
                if (!Equals(model.ManageParamField, null) && !Equals(model.ManageActionUrl, null))
                {
                    SetQueryString(x, DynamicGridConstants.ManageKey, model.DisplayTextManage, model.ManageActionUrl, model.ManageParamField);
                }
                if (!Equals(model.CopyParamField, null) && !Equals(model.CopyActionUrl, null))
                {
                    SetQueryString(x, DynamicGridConstants.CopyKey, model.DisplayTextCopy, model.CopyActionUrl, model.CopyParamField);
                }
                if (!Equals(model.ImageParamField, null))
                {
                    x.SetPropertyValue(DynamicGridConstants.ImageKey, x.GetProperty(model.ImageParamField).ToString());
                }
                if (!Equals(model.CheckboxParamField, null))
                {
                    SetQueryString(x, DynamicGridConstants.CheckboxKey, model.CheckboxParamField);
                }
                if (!Equals(model.ControlParamField, null))
                {
                    SetQueryString(x, DynamicGridConstants.ControlKey, model.ControlParamField);
                }
                if (!Equals(model.IsLinkActionUrl, null))
                {
                    SetQueryString(x, DynamicGridConstants.IsLinkKey, string.Empty, model.IsLinkActionUrl, model.IsLinkParamField, model.IsLinkFieldName);
                }
                if (!Equals(model.EnableDisableActionUrl, null) && !Equals(model.EnableDisableParamField, null))
                {
                    SetQueryString(x, DynamicGridConstants.DisableKey, model.EnableDisableActionUrl, model.EnableDisableParamField);
                }

                dynamicList.Add((dynamic)x);
            });

            reportModel.DataTableToList = dynamicList;
            reportModel.TotalRowCount = dataModel.Count;
            reportModel.RowPerPageCount = pageSize;
            reportModel.TableWidth = pageSize == 10 ? tableWidth : tableWidth + 24;
            return reportModel;
        }

        /// <summary>
        /// Set Model recored ordering ASC/DSC
        /// </summary>
        /// <typeparam name="T">Generic type</typeparam>
        /// <param name="model">Reference of FilterCollectionDataModel</param>
        /// <param name="reportModel"></param>
        /// <param name="dataModel"></param>
        private static List<T> SetSortOrder<T>(FilterCollectionDataModel model, ReportModel reportModel, List<T> dataModel)
        {
            string sort = string.Empty;
            string sortDir = string.Empty;

            if (!Equals(model.SortCollection, null) && model.SortCollection.Count > 0)
            {
                sort = model.SortCollection[DynamicGridConstants.sortKey];
                sortDir = model.SortCollection[DynamicGridConstants.sortDirKey];

                if (sortDir.Equals(DynamicGridConstants.ASCKey))
                {
                    reportModel.GridSortDirection = SortDirection.Ascending;
                    reportModel.GridSortColumn = sort;
                }
                else
                {
                    reportModel.GridSortDirection = SortDirection.Descending;
                    reportModel.GridSortColumn = sort;
                }
            }

            return dataModel;
        }

        /// <summary>
        /// Generate dynamic grid column
        /// </summary>
        /// <param name="gridList">dynamic grid column list </param>
        /// <param name="tableWidth">table width</param>
        /// <param name="columns">WebGridColumn column object</param>
        /// <returns>returns integer table width</returns>
        private static int GenerateDynamicGridView(List<dynamic> gridList, int tableWidth, List<WebGridColumn> columns)
        {
            if (!Equals(gridList, null) && gridList.Count > 0)
            {
                string columnsName = string.Empty;
                int rowIndex = 0;

                string graphField1 = string.Empty;
                string graphField2 = string.Empty;
                List<string> ids = null as List<string>;
                if (Equals(ids, null) || ids.Count.Equals(0))
                {
                    GetWebGridColumn(gridList, columns, ref  columnsName, ref tableWidth, ref rowIndex);
                }
                else
                {
                    foreach (string SelectedItem in ids)
                    {
                        foreach (var item in gridList)
                        {
                            if (SelectedItem.Equals(Convert.ToString(item.id)))
                            {
                                columnsName = Convert.ToString(item.name);
                                columns.Add(new WebGridColumn() { ColumnName = columnsName, Header = Convert.ToString(item.headertext), CanSort = true });
                            }
                        }
                    }
                }
            }
            return tableWidth;
        }

        /// <summary>
        /// Create webgridcolumn with HTML format. 
        /// </summary>
        /// <param name="gridList">Grid Column lisrt</param>
        /// <param name="columns">WebGridColumn object</param>
        /// <param name="columnsName">Reference columnsName</param>
        /// <param name="tableWidth">Refenence tableWidth</param>
        /// <param name="rowIndex">Reference rowIndex</param>
        private static void GetWebGridColumn(List<dynamic> gridList, List<WebGridColumn> columns, ref string columnsName, ref int tableWidth, ref int rowIndex)
        {
            foreach (var item in gridList)
            {
                columnsName = Convert.ToString(item.name);

                if (!Equals(Convert.ToString(item.isvisible), DynamicGridConstants.No))
                {
                    if (Convert.ToString(item.ischeckbox).Equals(DynamicGridConstants.Yes))
                    {
                        columns.Add(new WebGridColumn()
                        {
                            ColumnName = columnsName,
                            Header = Convert.ToString(item.headertext),
                            CanSort = false,
                            Format = (item1) => new HtmlString(String.Format(DynamicGridConstants.CheckboxHtml, item1.Checkbox, Convert.ToString(item.name))),
                            Style = DynamicGridConstants.ClassCenter
                        });
                    }
                    else if (Convert.ToString(item.iscontrol).Equals(DynamicGridConstants.Yes))
                    {
                        columns.Add(new WebGridColumn()
                        {
                            ColumnName = columnsName,
                            Header = Convert.ToString(item.headertext),
                            CanSort = false,
                            Format = (item1) => new HtmlString(String.Format(DynamicGridConstants.ControlHtml, Convert.ToString(item.controltype), item1.Control.IndexOf('$') >= 0 ? item1.Control.Split('$')[0] : item1.Control, item1.Control.IndexOf('$') >= 0 ? item1.Control.Split('$')[1] : item1.Control)),
                            Style = DynamicGridConstants.ClassCenter
                        });
                    }
                    else
                    {
                        switch (columnsName)
                        {
                            case DynamicGridConstants.EditKey:
                                columns.Add(new WebGridColumn()
                                {
                                    ColumnName = columnsName,
                                    Header = Convert.ToString(item.headertext),
                                    CanSort = false,
                                    Format = (item1) => new HtmlString(String.Format(DynamicGridConstants.EditHtml, item1.Edit)),
                                    Style = DynamicGridConstants.ClassCenter
                                });
                                break;
                            case DynamicGridConstants.DeleteKey:
                                columns.Add(new WebGridColumn()
                                {
                                    ColumnName = columnsName,
                                    Header = Convert.ToString(item.headertext),
                                    CanSort = false,
                                    Format = (item1) => new HtmlString(String.Format(DynamicGridConstants.DeleteHtml, item1.Delete.Split('$')[0], Resources.ZnodeResources.ConfirmDeleteTitle, Resources.ZnodeResources.ConfirmDeleteMessage, item1.Delete.Split('$')[1])),
                                    Style = DynamicGridConstants.ClassCenter
                                });
                                break;
                            case DynamicGridConstants.ViewKey:
                                columns.Add(new WebGridColumn()
                                {
                                    ColumnName = columnsName,
                                    Header = Convert.ToString(item.headertext),
                                    CanSort = false,
                                    Format = (item1) => new HtmlString(String.Format(DynamicGridConstants.ViewHtml, item1.View)),
                                    Style = DynamicGridConstants.ClassCenter
                                });
                                break;
                            case DynamicGridConstants.ManageKey:
                                columns.Add(new WebGridColumn()
                                {
                                    ColumnName = columnsName,
                                    Header = Convert.ToString(item.headertext),
                                    CanSort = false,
                                    Format = (item1) => new HtmlString(String.Format(DynamicGridConstants.ManageHtml, item1.Manage)),
                                    Style = DynamicGridConstants.ClassCenter
                                });
                                break;
                            case DynamicGridConstants.CopyKey:
                                columns.Add(new WebGridColumn()
                                {
                                    ColumnName = columnsName,
                                    Header = Convert.ToString(item.headertext),
                                    CanSort = false,
                                    Format = (item1) => new HtmlString(String.Format(DynamicGridConstants.CopyHtml, item1.Copy)),
                                    Style = DynamicGridConstants.ClassCenter
                                });
                                break;
                            case DynamicGridConstants.ImageKey:
                                columns.Add(new WebGridColumn()
                                {
                                    ColumnName = columnsName,
                                    Header = Convert.ToString(item.headertext),
                                    CanSort = false,
                                    Format = (item1) => new HtmlString(String.Format(DynamicGridConstants.ImageHtml, item1.Image)),
                                    Style = DynamicGridConstants.ClassCenter
                                });
                                break;
                            case DynamicGridConstants.DisableKey:
                                columns.Add(new WebGridColumn()
                                {
                                    ColumnName = columnsName,
                                    Header = Convert.ToString(item.headertext),
                                    CanSort = false,
                                    Format = (item1) => new HtmlString(String.Format(DynamicGridConstants.DisableHtml, item1.Disable.Split('$')[1],
                                        Resources.ZnodeResources.EnableDisableConfirmTitle, string.Format(Resources.ZnodeResources.EnableDisableConfirmMessage, Convert.ToString(item1.Disable.Split('$')[0]).ToLower()),
                                        item1.Disable.Split('$')[0], (item1.Disable.Split('$')[1]).Split('/')[(item1.Disable.Split('$')[1]).Split('/').Length - 1], Convert.ToString(item1.Disable.Split('$')[0]))),
                                    Style = DynamicGridConstants.ClassCenter
                                });
                                break;
                            default:
                                if (Convert.ToString(item.isallowlink).Equals(DynamicGridConstants.Yes))
                                {
                                    columns.Add(new WebGridColumn()
                                    {
                                        ColumnName = columnsName,
                                        Header = Convert.ToString(item.headertext),
                                        CanSort = Convert.ToBoolean(item.allowsorting),
                                        Format = (item1) => new HtmlString(String.Format(DynamicGridConstants.LinkHtml, (item1.IsLink).Split('$')[1], (item1.IsLink).Split('$')[0])),
                                    });
                                }
                                else
                                {
                                    string datatype = Convert.ToString(item.datatype);

                                    if (datatype.Equals("Boolean"))
                                    {
                                        columns.Add(new WebGridColumn()
                                        {
                                            ColumnName = columnsName,
                                            Header = Convert.ToString(item.headertext),
                                            CanSort = false,
                                            Format = (item1) => new HtmlString(String.Format("{0}", FilterHelpers.FindRowValues(item1, Convert.ToString(item.name)))),
                                            Style = DynamicGridConstants.ClassCenter
                                        });
                                    }
                                    else if (datatype.Equals(DynamicGridConstants.DateTime) || datatype.Equals(DynamicGridConstants.Date) || datatype.Equals(DynamicGridConstants.Time))
                                    {
                                        columns.Add(new WebGridColumn()
                                        {
                                            ColumnName = columnsName,
                                            Header = Convert.ToString(item.headertext),
                                            CanSort = false,
                                            Format = (item1) => new HtmlString(String.Format("{0}", FilterHelpers.SetRuntimeDateFormat(item1, Convert.ToString(item.name), datatype))),
                                        });
                                    }
                                    else
                                    {
                                        columns.Add(new WebGridColumn() { ColumnName = columnsName, Header = Convert.ToString(item.headertext), CanSort = Convert.ToBoolean(item.allowsorting), Style = datatype.Equals("Int32") ? DynamicGridConstants.ClassRight : string.Empty });
                                    }
                                }
                                break;
                        }

                    }

                    tableWidth = tableWidth + 300;
                }

                rowIndex++;
            }
        }

        /// <summary>
        /// Find Row level values for html formating
        /// </summary>
        /// <param name="row">Dynamic grid row</param>
        /// <param name="name">column name</param>
        /// <returns>Returns Html string</returns>
        public static string FindRowValues(object row, string name)
        {
            var val = row.GetDynamicProperty(name);

            if (!Equals(val, null) && val.ToString().ToLower().Equals("true"))
            {
                return DynamicGridConstants.IconTrue;
            }
            return DynamicGridConstants.IconFalse;
        }

        public static string SetRuntimeDateFormat(object row, string name, string type)
        {
            string val = Convert.ToString(row.GetDynamicProperty(name));
            switch (type)
            {
                case DynamicGridConstants.DateTime:
                    return !string.IsNullOrEmpty(val) ? Convert.ToDateTime(val).ToString(HelperMethods.GetStringDateTimeFormat()) : string.Empty;
                case DynamicGridConstants.Date:
                    return !string.IsNullOrEmpty(val) ? Convert.ToDateTime(val).ToString(HelperMethods.GetStringDateFormat()) : string.Empty;
                case DynamicGridConstants.Time:
                    return !string.IsNullOrEmpty(val) ? Convert.ToDateTime(val).ToString(HelperMethods.GetStringDateFormat()) : string.Empty;
                default:
                    return !string.IsNullOrEmpty(val) ? Convert.ToDateTime(val).ToString(HelperMethods.GetStringDateFormat()) : string.Empty;

            }

        }

        /// <summary>
        /// Create Run time dropdown list for filter criteria.
        /// </summary>
        /// <typeparam name="T">Generic type</typeparam>
        /// <param name="gridModel">Reference of ReportModel</param>
        /// <param name="sourceModel">List for dinding data</param>
        /// <param name="valueField">Dropdown Value field name</param>
        /// <param name="textField">Dropdown text field name</param>
        /// <param name="Label">Dropdown Label</param>
        public static void CreateDropdown<T>(ref ReportModel gridModel, List<T> sourceModel, string valueField, string textField, string Label)
        {
            DropdownListModel itemList = new DropdownListModel();
            itemList.Name = valueField.ToString();
            itemList.Label = Label.ToString();

            itemList.Dropdown.Add(new DropdownModel()
            {
                Text = DynamicGridConstants.DefaultText,
                Value = null,
            });

            sourceModel.ForEach(x =>
            {
                itemList.Dropdown.Add(new DropdownModel()
                {
                    Text = x.GetProperty(textField).ToString(),
                    Value = x.GetProperty(valueField).ToString(),
                });
            });
            gridModel.FilterColumn.DropdownList.Add((dynamic)itemList);
        }

        /// <summary>
        /// Create Run time dropdown list for filter criteria.
        /// </summary>
        /// <typeparam name="T">Generic type</typeparam>
        /// <param name="gridModel">Reference of ReportModel</param>
        /// <param name="sourceModel">List for dinding data</param>
        /// <param name="filters">Filter collection for set selected value</param>
        /// <param name="valueField">Dropdown Value field name</param>
        /// <param name="textField">Dropdown text field name</param>
        /// <param name="Label">Dropdown Label</param>
        public static void CreateDropdown<T>(ref ReportModel gridModel, List<T> sourceModel, FilterCollection filters, string valueField, string textField, string Label)
        {
            string selectedItem = string.Empty;
            if (!Equals(filters, null))
            {
                filters.ToList().ForEach(x =>
                {
                    if (x.FilterName.Equals(valueField))
                    {
                        selectedItem = x.FilterValue;
                    }
                });
            }

            DropdownListModel itemList = new DropdownListModel();
            itemList.Name = valueField.ToString();
            itemList.Label = Label.ToString();
            itemList.Dropdown.Add(new DropdownModel()
            {
                Text = DynamicGridConstants.DefaultText,
                Value = null,
            });
            sourceModel.ForEach(x =>
            {
                itemList.Dropdown.Add(new DropdownModel()
                {
                    Text = x.GetProperty(textField).ToString(),
                    Value = x.GetProperty(valueField).ToString(),
                    Selected = x.GetProperty(valueField).ToString().Equals(selectedItem) ? true : false
                });
            });
            gridModel.FilterColumn.DropdownList.Add((dynamic)itemList);
        }

        #region Advance Search

        /// <summary>
        /// Get View model for advance search
        /// </summary>
        /// <returns>Returns ReportModel</returns>
        public static ReportModel GetAdvanceSearchView()
        {
            string XMlstring = (string)HttpContext.Current.Session[DynamicGridConstants.XMLStringSesionKey];

            List<dynamic> gridList = new List<dynamic>();

            if (Equals(HttpContext.Current.Session[DynamicGridConstants.ColumnListSesionKey], null))
            {
                gridList = GetDynamicGridView(XMlstring, DynamicGridConstants.columnKey);
                HttpContext.Current.Session[DynamicGridConstants.ColumnListSesionKey] = gridList;
            }
            else
            {
                gridList = (List<dynamic>)HttpContext.Current.Session[DynamicGridConstants.ColumnListSesionKey];
            }
            ReportModel model = new ReportModel()
            {
                ReportSettingModel = new ReportSettingModel()
                {
                    GroupName = DynamicGridConstants.GroupNameKey,
                    ItemName = Convert.ToString((ListType)Convert.ToInt32(HttpContext.Current.Session[DynamicGridConstants.listTypeSesionKey])),
                    Setting = XMlstring
                },

                FilterColumnList = new ListViewModel() { AssignedList = GetSelectListItem(gridList, DynamicGridConstants.Yes), UnAssignedList = GetSelectListItem(gridList, DynamicGridConstants.No) },
                FieldList = new DropdownListModel() { Dropdown = GetFieldList(gridList) }
            };
            return model;
        }

        /// <summary>
        /// Get Operators for advance search
        /// </summary>
        /// <param name="id">XML field id</param>
        /// <returns></returns>
        public static string GetOperators(string id)
        {
            string dynamicOperatorString = string.Empty;
            if (!string.IsNullOrEmpty(id))
            {
                List<dynamic> operatorList = new List<dynamic>();
                List<dynamic> columnList = (List<dynamic>)HttpContext.Current.Session[DynamicGridConstants.ColumnListSesionKey];

                string dataType = columnList.SingleOrDefault(x => x.id.Equals(id)).datatype;

                string operatorXMl = GetXmlConfiguration(1);

                if (!string.IsNullOrEmpty(operatorXMl))
                {
                    operatorList = ConvertXmlToList(operatorXMl, DynamicGridConstants.OperatordefinitionKey);
                    operatorList = operatorList.FindAll(x => (x.datatype.Equals(dataType)));
                    foreach (var dr in operatorList)
                    {
                        dynamicOperatorString += string.Format(DynamicGridConstants.DynamicOptionString, dr.datatype, dr.filterconditionformat, dr.id, dr.displayname);
                    }
                }
            }
            return dynamicOperatorString;
        }

        /// <summary>
        /// Get fields list for advance search
        /// </summary>
        /// <param name="model">Dynamic List of grid column</param>
        /// <returns>Returns List of DropdownModel</returns>
        public static List<DropdownModel> GetFieldList(List<dynamic> model)
        {

            List<DropdownModel> items = new List<DropdownModel>();
            bool isError = false;

            try
            {
                model.FindAll(y => !Equals(y.isadvancesearch, DynamicGridConstants.No));
            }
            catch
            {
                isError = true;
            }

            if (!isError)
            {
                model = model.FindAll(y => !Equals(y.name, DynamicGridConstants.CopyKey)
                    && !Equals(y.name, DynamicGridConstants.ManageKey)
                    && !Equals(y.name, DynamicGridConstants.EditKey)
                    && !Equals(y.name, DynamicGridConstants.DeleteKey)
                    && !Equals(y.name, DynamicGridConstants.ViewKey)
                    && !Equals(y.name, DynamicGridConstants.ImageKey)
                    && !Equals(y.name, DynamicGridConstants.ActionKey)
                    && !Equals(y.name, DynamicGridConstants.CheckboxKey)
                    && !Equals(y.isadvancesearch, DynamicGridConstants.No));
            }
            else
            {
                model = model.FindAll(y => !Equals(y.name, DynamicGridConstants.CopyKey)
                   && !Equals(y.name, DynamicGridConstants.ManageKey)
                   && !Equals(y.name, DynamicGridConstants.EditKey)
                   && !Equals(y.name, DynamicGridConstants.DeleteKey)
                   && !Equals(y.name, DynamicGridConstants.ViewKey)
                   && !Equals(y.name, DynamicGridConstants.ImageKey)
                   && !Equals(y.name, DynamicGridConstants.ActionKey)
                   && !Equals(y.name, DynamicGridConstants.CheckboxKey));
            }

            if (!Equals(model, null))
            {
                items = model.Select(x => new DropdownModel
                {
                    Value = x.id,
                    Text = x.headertext,
                    Selected = false
                }).ToList();

                items.Insert(0, new DropdownModel { Text = null, Value = null });
            }
            return items;
        }

        /// <summary>
        /// Get Selected column list for advance search
        /// </summary>
        /// <param name="model">Grid column list</param>
        /// <param name="isvisible">Flag for check visible mode</param>
        /// <returns>Returns list of SelectListItem</returns>
        public static List<SelectListItem> GetSelectListItem(List<dynamic> model, string isvisible)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            try
            {
                if (!Equals(model, null))
                {
                    model.ForEach(x =>
                    {
                        if (x.isvisible == isvisible && !string.IsNullOrEmpty(x.headertext) && Equals(Convert.ToString(x.musthide), DynamicGridConstants.No))
                        {
                            items.Add(new SelectListItem
                            {
                                Value = x.id,
                                Text = x.headertext,
                                Selected = false
                            });
                        }
                    });
                }
            }
            catch (Exception ex)
            {
                items = new List<SelectListItem>();
            }
            return items;
        }

        /// <summary>
        /// Save advance search criteria and dynamic grid column.
        /// </summary>
        /// <param name="formCollection">NameValueCollection</param>
        /// <returns>Flag true/false</returns>
        public static bool SaveAdvanceSearch(NameValueCollection formCollection)
        {
            List<dynamic> columnList = (List<dynamic>)HttpContext.Current.Session[DynamicGridConstants.ColumnListSesionKey];

            int[] _assignedId = null;
            int[] _fieldsId = null;
            int[] _operatorId = null;
            string[] _values = null;
            string[] _clause = null;

            foreach (var _keys in formCollection)
            {
                var keyname = _keys.ToString();
                var keyValue = formCollection[keyname].ToString();
                if (!Equals(keyValue.ToString().Replace(",", string.Empty), string.Empty))
                {
                    switch (keyname)
                    {
                        case DynamicGridConstants.AssignedId:
                            _assignedId = Array.ConvertAll(keyValue.Split(','), s => !Equals(s, string.Empty) ? int.Parse(s) : 0);
                            break;
                        case DynamicGridConstants.FieldName:
                            _fieldsId = Array.ConvertAll(keyValue.Split(','), s => !Equals(s, string.Empty) ? int.Parse(s) : 0);
                            break;
                        case DynamicGridConstants.Operator:
                            _operatorId = Array.ConvertAll(keyValue.Split(','), s => !Equals(s, string.Empty) ? int.Parse(s) : 0);
                            break;
                        case DynamicGridConstants.Values:
                            _values = Array.ConvertAll(keyValue.Split(','), s => s.ToString());
                            break;
                        case DynamicGridConstants.Clause:
                            _clause = Array.ConvertAll(keyValue.Split(','), s => s.ToString());
                            break;
                    }
                }
            }
            columnList.ForEach(x =>
            {
                x.isvisible = DynamicGridConstants.No;
                _assignedId.ToList().ForEach(y =>
                {
                    if (x.id.Equals(y.ToString()))
                    {
                        x.isvisible = DynamicGridConstants.Yes;
                    }
                });
            });
            columnList = SetIndexOrder(columnList, _assignedId);

            HttpContext.Current.Session[DynamicGridConstants.ColumnListSesionKey] = columnList;

            if (!Equals(_fieldsId, null) && !Equals(_values, null))
            {
                FormatFilterCollection(_fieldsId, _values, _operatorId);
                HttpContext.Current.Session[DynamicGridConstants.FieldsCollectionSessionKey] = _fieldsId;
                HttpContext.Current.Session[DynamicGridConstants.OperatorsCollectionSessionKey] = _operatorId;
                HttpContext.Current.Session[DynamicGridConstants.ValuesCollectionSessionKey] = _values;
            }
            else
            {
                HttpContext.Current.Session[DynamicGridConstants.FieldsCollectionSessionKey] = null;
                HttpContext.Current.Session[DynamicGridConstants.OperatorsCollectionSessionKey] = null;
                HttpContext.Current.Session[DynamicGridConstants.ValuesCollectionSessionKey] = null;
                HttpContext.Current.Session[DynamicGridConstants.AdvanceFilterCollectionKey] = null;
            }

            return true;
        }

        /// <summary>
        /// Get formated filter collection for advance search.
        /// </summary>
        /// <param name="_fieldsId">Array of selected fields column</param>
        /// <param name="_values">Array Selected column values</param>
        /// <param name="_operatorId">Array of applied operators</param>
        /// <returns>Returns FilterCollection</returns>
        public static FilterCollection FormatFilterCollection(int[] _fieldsId, string[] _values, int[] _operatorId)
        {
            FilterCollection filters = new FilterCollection();
            List<dynamic> operatorList = new List<dynamic>();
            List<dynamic> columnList = (List<dynamic>)HttpContext.Current.Session[DynamicGridConstants.ColumnListSesionKey];
            string operatorXMl = GetXmlConfiguration(1);

            if (!string.IsNullOrEmpty(operatorXMl))
            {
                operatorList = ConvertXmlToList(operatorXMl, DynamicGridConstants.OperatordefinitionKey);
            }
            columnList.ForEach(x =>
            {
                int counter = 0;
                _fieldsId.ToList().ForEach(y =>
                {
                    if (x.id.Equals(y.ToString()))
                    {
                        string operatorstr = operatorList.FirstOrDefault(op => op.id.Equals((_operatorId[counter]).ToString())).displayname;
                        filters.Add(new FilterTuple(x.name, SetFilterOperators(operatorstr), _values[counter]));
                    }
                    counter++;
                });

            });
            HttpContext.Current.Session[DynamicGridConstants.AdvanceFilterCollectionKey] = filters;
            return filters;
        }

        public static string GetDataTypeByKeyName(string keyName)
        {
            string _datatype = string.Empty;
            List<dynamic> columnList = (List<dynamic>)HttpContext.Current.Session[DynamicGridConstants.ColumnListSesionKey];
            var data = columnList.FirstOrDefault(x => x.name.ToString().ToLower() == keyName.ToLower());
            if (!Equals(data, null))
            {
                _datatype = data.datatype;
            }

            return _datatype;
        }

        /// <summary>
        /// Get filter collection operator name.
        /// </summary>
        /// <param name="operatorString">Selected Operator String</param>
        /// <returns>Returns filter collection operator name</returns>
        public static string SetFilterOperators(string operatorString)
        {
            switch (operatorString.Trim().ToLower())
            {
                case DynamicGridConstants.IsOperator:
                    return FilterOperators.StartsWith;
                case DynamicGridConstants.BeginswithOperator:
                    return FilterOperators.StartsWith;
                case DynamicGridConstants.EndswithOperator:
                    return FilterOperators.EndsWith;
                case DynamicGridConstants.ContainsOperator:
                    return FilterOperators.Contains;
                case DynamicGridConstants.EqualsOperator:
                    return FilterOperators.Equals;
                case DynamicGridConstants.GreaterthanOperator:
                    return FilterOperators.GreaterThan;
                case DynamicGridConstants.GreaterorequalOperator:
                    return FilterOperators.GreaterThanOrEqual;
                case DynamicGridConstants.LessthanOperator:
                    return FilterOperators.LessThan;
                case DynamicGridConstants.LessorequalOperator:
                    return FilterOperators.LessThanOrEqual;
                default:
                    return FilterOperators.Like;
            }
        }


        public static string GetFilterOperatorByDataType(string dataType, string keyName = null, string value = null)
        {
            string _operator = string.Empty;
            switch (dataType.Trim().ToLower())
            {
                case DynamicGridConstants.StringKey:
                    _operator = FilterOperators.Contains;
                    break;
                case DynamicGridConstants.Int16Key:
                    _operator = FilterOperators.Equals;
                    break;
                case DynamicGridConstants.Int32Key:
                    _operator = FilterOperators.Equals;
                    break;
                case DynamicGridConstants.Int64Key:
                    _operator = FilterOperators.Equals;
                    break;
                case DynamicGridConstants.BooleanKey:
                    _operator = FilterOperators.Equals;
                    break;
                case DynamicGridConstants.DateTimeKey:
                    if (!Equals(keyName, null) && (keyName.ToLower().Contains("start") || keyName.ToLower().Contains("begin")))
                        _operator = FilterOperators.GreaterThanOrEqual;
                    else if (!Equals(keyName, null) && keyName.ToLower().Contains("end"))
                        _operator = FilterOperators.LessThanOrEqual;
                    else
                        _operator = FilterOperators.GreaterThanOrEqual;
                    break;
                case DynamicGridConstants.SmallDate:
                    if (!Equals(keyName, null) && (keyName.ToLower().Contains("start") || keyName.ToLower().Contains("begin")))
                        _operator = FilterOperators.GreaterThanOrEqual;
                    else if (!Equals(keyName, null) && keyName.ToLower().Contains("end"))
                        _operator = FilterOperators.LessThanOrEqual;
                    else
                        _operator = FilterOperators.GreaterThanOrEqual;
                    break;
                case DynamicGridConstants.DecimalKey:
                    _operator = FilterOperators.Equals;
                    break;
                case DynamicGridConstants.DoubleKey:
                    _operator = FilterOperators.Equals;
                    break;
                case DynamicGridConstants.SingleKey:
                    _operator = FilterOperators.Equals;
                    break;
                default:
                    int val = 0;
                    bool canConvert = int.TryParse(value, out val);
                    if (canConvert == true)
                        _operator = FilterOperators.Equals;
                    else
                        _operator = FilterOperators.Contains;
                    break;
            }

            return _operator;
        }

        /// <summary>
        /// Set sequence of dynamic grid column 
        /// </summary>
        /// <param name="columnList"></param>
        /// <param name="_assignedId"></param>
        /// <returns></returns>
        public static List<dynamic> SetIndexOrder(List<dynamic> columnList, int[] _assignedId)
        {
            List<dynamic> tempList = new List<dynamic>();
            tempList.AddRange(columnList);
            List<dynamic> newList = new List<dynamic>();

            _assignedId.ToList().ForEach(x =>
            {
                var item = columnList.FirstOrDefault(y => y.id == x.ToString());
                newList.Add(item);
                tempList.Remove(item);
            });
            newList.AddRange(tempList);
            return newList;
        }

        /// <summary>
        /// Clear all session of dynamic grid
        /// </summary>
        /// <returns></returns>
        public static bool ClearAdvanceSearch()
        {
            HttpContext.Current.Session[DynamicGridConstants.FieldsCollectionSessionKey] = null;
            HttpContext.Current.Session[DynamicGridConstants.OperatorsCollectionSessionKey] = null;
            HttpContext.Current.Session[DynamicGridConstants.ValuesCollectionSessionKey] = null;
            HttpContext.Current.Session[DynamicGridConstants.AdvanceFilterCollectionKey] = null;
            HttpContext.Current.Session[DynamicGridConstants.ColumnListSesionKey] = null;
            HttpContext.Current.Session[DynamicGridConstants.AdvanceFilterCollectionKey] = null;
            HttpContext.Current.Session[DynamicGridConstants.listTypeSesionKey] = null;
            return true;
        }
        #endregion

        #region Sub grid

        /// <summary>
        /// Get nested grid sub records
        /// </summary>
        /// <param name="row">grid object row</param>
        /// <param name="keyName">field name for get records </param>
        /// <param name="typeName">class name</param>
        /// <param name="methodName">target method name</param>
        /// <returns>returns dynamic list</returns>
        public static List<SelectListItem> GetSubRecords(object row, string keyName, string typeName, string methodName)
        {
            string val = Convert.ToString(row.GetDynamicProperty(keyName));
            List<SelectListItem> subGridlist = new List<SelectListItem>();
            subGridlist.Add(new SelectListItem() { Text = string.Format(DynamicGridConstants.SubgridHtml, val, typeName, methodName) });
            return subGridlist;
        }

        /// <summary>
        /// Invoke delegets methods from string parameter.
        /// </summary>
        /// <param name="typeName">class name</param>
        /// <param name="methodName">target method name</param>
        /// <param name="stringParam">method parameter</param>
        /// <returns></returns>
        public static List<dynamic> InvokeStringMethod(string typeName, string methodName, string stringParam)
        {
            // Get the Type for the class
            Type calledType = Type.GetType(typeName);

            // Invoke the method itself. The string returned by the method winds up in s.
            // Note that stringParam is passed via the last parameter of InvokeMember,
            // as an array of Objects.
            var result = calledType.InvokeMember(methodName, BindingFlags.InvokeMethod | BindingFlags.Public | BindingFlags.Static, null, null, new Object[] { stringParam });

            // Return the string that was returned by the called method.
            return (List<dynamic>)result;
        }
        #endregion
    }
}