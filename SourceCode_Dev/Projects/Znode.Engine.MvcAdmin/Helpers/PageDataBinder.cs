﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Helpers
{
    public class PageDataBinder : DefaultModelBinder
    {
        Dictionary<string, string> _params = new Dictionary<string, string>();
        /// <summary>
        /// Bind Model for filter and sort results
        /// </summary>
        /// <param name="controllerContext">controllerContext</param>
        /// <param name="bindingContext">bindingContext</param>
        /// <returns>Returns FilterCollectionDataModel</returns>
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            if (bindingContext.ModelType.IsSubclassOf(typeof(BaseViewModel)))
            {
                HttpRequestBase request = controllerContext.HttpContext.Request;

                int _page = 1;
                int _recordPerPage = HelperMethods.GridPagingStartValue;
                string _sortKey = string.Empty;
                string _sortDir = "ASC";

                SortCollection sortCollection = GetSortCollection(request, ref _page, ref _recordPerPage, ref _sortKey, ref _sortDir);

                return new FilterCollectionDataModel()
                {
                    Page = _page,
                    RecordPerPage = _recordPerPage,
                    SortCollection = sortCollection,
                    Filters = GetFilterCollection(request),
                    Params = _params
                };
            }
            else
            {
                return base.BindModel(controllerContext, bindingContext);
            }
        }

        /// <summary>
        /// Get sort collection from request
        /// </summary>
        /// <param name="request">Http Request</param>
        /// <param name="_page">ref _page</param>
        /// <param name="_recordPerPage">ref _recordPerPage</param>
        /// <param name="_sortKey">ref _sortKey</param>
        /// <param name="_sortDir">ref _sortDir</param>
        /// <returns>Returns SortCollection</returns>
        private SortCollection GetSortCollection(HttpRequestBase request, ref int _page, ref int _recordPerPage, ref string _sortKey, ref string _sortDir)
        {
            SortCollection sortCollection = new SortCollection();
            Dictionary<string, string> _paginationcollection = new Dictionary<string, string>();

            var queryStringCollection = request.QueryString;

            if (queryStringCollection.Count > 0)
            {
                foreach (var keys in queryStringCollection)
                {
                    if (!Equals(keys, null))
                    {
                        var kayname = keys.ToString();

                        var kayValue = queryStringCollection[kayname].ToString();

                        _paginationcollection.Add(kayname, kayValue);
                    }
                }
            }

            if (!Equals(_paginationcollection, null))
            {
                foreach (var x in _paginationcollection)
                {
                    switch (x.Key.ToLower())
                    {
                        case "page":
                            if (x.Value != "" && x.Value != null)
                                _page = int.Parse(x.Value);
                            else
                                _page = 1;
                            break;

                        case "recordperpage":
                            if (x.Value != "" && x.Value != null)
                                _recordPerPage = int.Parse(x.Value);
                            else
                                _recordPerPage = 10;
                            break;
                        case "sort":
                            if (x.Value != "" && x.Value != null)
                                _sortKey = x.Value;
                            break;
                        case "sortdir":
                            if (x.Value != "" && x.Value != null)
                                _sortDir = x.Value;
                            else
                                _sortDir = "ASC";
                            break;

                    }

                    if (x.Key != "page" && x.Key != "recordperpage" && x.Key != "sort" && x.Key != "sortdir")
                    {

                        _params.Add(x.Key, x.Value);
                    }

                }

            }

            if (string.IsNullOrEmpty(_sortKey))
            {
                return null;
            }
            else
            {
                sortCollection.Add("sort", _sortKey);
                sortCollection.Add("sortDir", _sortDir);
            }
            return sortCollection;
        }





        /// <summary>
        /// Get Filter Collection
        /// </summary>
        /// <param name="request">Http request</param>
        /// <returns>Returns FilterCollection</returns>
        private FilterCollection GetFilterCollection(HttpRequestBase request)
        {
            FilterCollection filters = new FilterCollection();
            var formCollection = request.Form;

            foreach (var _keys in formCollection)
            {

                string keyname = _keys.ToString();
                string keyValue = formCollection[keyname].ToString();

                if (!string.IsNullOrEmpty(keyValue) && !Equals(keyname, "X-Requested-With"))
                {
                    if (!Equals((List<dynamic>)HttpContext.Current.Session[DynamicGridConstants.ColumnListSesionKey], null))
                    {
                        string _datatype = FilterHelpers.GetDataTypeByKeyName(keyname);
                        if (_datatype.Equals("DateTime"))
                            keyValue = "'" + HelperMethods.ConvertStringToDate(keyValue).ToString() + "'";
                        else if (_datatype.Equals("String") && keyValue.IndexOf("'") >= 0)
                        {
                            keyValue = keyValue.Replace("'", "''");
                        }
                        else if (_datatype.Equals("Boolean"))
                        {
                            if (keyValue.Equals("true",StringComparison.OrdinalIgnoreCase))
                            {
                                keyValue = "1";
                            }
                            else if (keyValue.Equals("false",StringComparison.OrdinalIgnoreCase))
                            {
                                keyValue = "0";
                            }
                        }
                        filters.Add(new FilterTuple(keyname, FilterHelpers.GetFilterOperatorByDataType(_datatype, keyname, keyValue), keyValue));

                    }
                    else
                    {
                        filters.Add(new FilterTuple(keyname, FilterOperators.Contains, keyValue));
                    }

                }
            }

            if (Convert.ToString(HttpContext.Current.Session["relativePath"]).Equals(request.AppRelativeCurrentExecutionFilePath))
            {
                if (!Equals(HttpContext.Current.Session[DynamicGridConstants.AdvanceFilterCollectionKey], null))
                {
                    FilterCollection _advanceSearchCollection = (FilterCollection)HttpContext.Current.Session[DynamicGridConstants.AdvanceFilterCollectionKey];
                    foreach (var filterTuple in _advanceSearchCollection)
                    {
                        string keyValue = filterTuple.Item3.IndexOf("'") >= 0 ? filterTuple.Item3.Replace("'", "''") : filterTuple.Item3;

                        filters.Add(new FilterTuple(filterTuple.Item1, filterTuple.Item2, keyValue));
                    }
                }
            }
            else
            {
                HttpContext.Current.Session["relativePath"] = request.AppRelativeCurrentExecutionFilePath;
                HttpContext.Current.Session[DynamicGridConstants.AdvanceFilterCollectionKey] = null;
                HttpContext.Current.Session[DynamicGridConstants.FieldsCollectionSessionKey] = null;
                HttpContext.Current.Session[DynamicGridConstants.OperatorsCollectionSessionKey] = null;
                HttpContext.Current.Session[DynamicGridConstants.ValuesCollectionSessionKey] = null;
            }

            return filters;
        }
    }
}