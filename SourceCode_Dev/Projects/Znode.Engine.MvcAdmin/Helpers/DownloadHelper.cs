﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Znode.Engine.MvcAdmin.Helpers
{
    public class DownloadHelper
    {
        #region Private Variables

        private string defaultExcelFileName = "DownloadedTrackingData.xls";
        private string defaultCsvFileName = "DownloadedTrackingData.csv";
        private string defaultDelimiter = "|";
        #endregion

        #region Public Methods

        /// <summary>
        /// Downloads the file on the basis of data.
        /// </summary>
        /// <param name="data">DataSet containing data to be downloaded</param>
        /// <param name="fileType">String type of file to be downloaded</param>
        /// <param name="response">Response object to write data to file.</param>
        public void Download(DataSet data, string fileType, HttpResponseBase response, string delimiter = null, string fileName = null)
        {
            DataSet source = data;
            CallSpecificDownloadOption(fileType, source, response, delimiter, fileName);
        }

        /// <summary>
        /// Downloads the file on the basis of list of data.
        /// </summary>
        /// <typeparam name="T">Any class/model type</typeparam>
        /// <param name="list">List<T> of any class/model type</param>
        /// <param name="fileType">String type of file to be downloaded</param>
        /// <param name="response">Response object to write data to file.</param>
        public void Download<T>(List<T> list, string fileType, HttpResponseBase response, string delimiter = null, string fileName = null)
        {
            DataSet source = new DataSet();
            if (!Equals(list, null))
            {
                DataTable dataTable = ToDataTable(list);
                if (!Equals(dataTable, null))
                {
                    source.Tables.Add(dataTable);
                }
            }
            else
            {
                source = null;
            }
            CallSpecificDownloadOption(fileType, source, response, delimiter, fileName);
        }

        /// <summary>
        /// Downloads the file on the basis of list of data.
        /// </summary>
        /// <typeparam name="T">Any class/model type</typeparam>
        /// <param name="list">List<T> of any class/model type</param>
        /// <param name="fileType">String type of file to be downloaded</param>
        /// <param name="response">Response object to write data to file.</param>
        public void ExportDownload<T>(List<T> list, string fileType, HttpResponseBase response, string delimiter = null, string fileName = null, bool hasConstrols = false)
        {
            DataSet source = new DataSet();
            if (!Equals(list, null))
            {
                DataTable dataTable = ToDataTable(list, true);
                if (!Equals(dataTable, null))
                {
                    source.Tables.Add(dataTable);
                }
            }
            else
            {
                source = null;
            }
            CallSpecificDownloadOption(fileType, source, response, delimiter, fileName, hasConstrols);
        }




        /// <summary>
        /// Converts the generic list to datatable.
        /// </summary>
        /// <typeparam name="TSource">Any class/model type</typeparam>
        /// <param name="data">IList<TSource> data</param>
        /// <returns>Returns dataTable.</returns>
        public DataTable ToDataTable<TSource>(IList<TSource> data)
        {
            DataTable dataTable = new DataTable(typeof(TSource).Name);
            PropertyInfo[] props = typeof(TSource).GetProperties(BindingFlags.DeclaredOnly |
                        BindingFlags.Public |
                        BindingFlags.Instance);

            foreach (PropertyInfo prop in props)
            {
                dataTable.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ??
                    prop.PropertyType);
            }

            foreach (TSource item in data)
            {
                var values = new object[props.Length];
                for (int i = 0; i < props.Length; i++)
                {
                    values[i] = props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            return dataTable;
        }

        /// <summary>
        /// Converts the generic list to datatable.
        /// </summary>
        /// <typeparam name="TSource">Any class/model type</typeparam>
        /// <param name="data">IList<TSource> data</param>
        /// <param name="IsJson">Set for Json string</param>
        /// <returns>Returns dataTable.</returns>
        public DataTable ToDataTable<TSource>(IList<TSource> data, bool IsJson)
        {
            DataTable dt = new DataTable();
            try
            {
                string jsonString = JsonConvert.SerializeObject(data);
                dt = JsonConvert.DeserializeObject<DataTable>(jsonString);
            }
            catch (Exception ex) { }
            return dt;

        }



        /// <summary>
        /// Exports data to excel file.
        /// </summary>
        /// <param name="strFileName">string name of file</param>
        /// <param name="gridViewControl">Grid view control binded with dataset</param>
        /// <param name="Response">HttpResponseBase object to perform the export.</param>
        public void ExportDataToExcel(string strFileName, GridView gridViewControl, HttpResponseBase Response, bool hasConstrols = false)
        {
            Response.Clear();
            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment; filename=" + strFileName);

            // Set as Excel as the primary format
            Response.AddHeader("Content-Type", "application/Excel");
            Response.ContentType = "application/Excel";
            System.IO.StringWriter sw = new System.IO.StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            if (hasConstrols)
            {
                RemoveGridControls(ref gridViewControl);
            }
            gridViewControl.RenderControl(htw);
            Response.Write(sw.ToString());
            gridViewControl.Dispose();

            Response.End();
        }

        /// <summary>
        ///  Exports data to csv file.
        /// </summary>
        /// <param name="fileName">The value of fileName</param>
        /// <param name="Data">The value of Data</param>
        /// <param name="Response">HttpResponseBase object to perform the export.</param>
        public void ExportDataToCSV(string fileName, byte[] Data, HttpResponseBase Response)
        {
            Response.Clear();
            Response.ClearHeaders();
            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
            Response.AddHeader("Content-Type", "application/Excel");

            // Set as text as the primary format
            Response.ContentType = "text/csv";
            Response.ContentType = "application/vnd.xls";
            Response.AddHeader("Pragma", "public");

            Response.BinaryWrite(Data);
            Response.End();
        }

        /// <summary>
        /// Makes the enum values have user friendly names(i.e enum values with spaces) that can be displayed in enum values binded to dropdown.
        /// </summary>
        /// <param name="type">Object of any enum type</param>
        /// <returns>Returns string having names with spaces.</returns>
        public string ToEnumString(object type)
        {
            var enumType = type.GetType();
            var name = Enum.GetName(enumType, type);
            var enumMemberAttribute = ((EnumMemberAttribute[])enumType.GetField(name).GetCustomAttributes(typeof(EnumMemberAttribute), true)).SingleOrDefault();

            if (enumMemberAttribute == null)
            {
                return name;
            }
            return enumMemberAttribute.Value;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// call the specific download method on the basis of file type provided.
        /// </summary>
        /// <param name="fileType">String type of file to be downloaded</param>
        /// <param name="source">Dataset having data</param>
        /// <param name="Response">HttpResponseBase object to perform the export.</param>
        private void CallSpecificDownloadOption(string fileType, DataSet source, HttpResponseBase response, string delimiter, string fileName, bool hasConstrols = false)
        {
            if (!Equals(source, null))
            {
                switch (fileType)
                {
                    case "1":
                        GridView gridView = new GridView();
                        gridView.DataSource = source;
                        gridView.DataBind();
                        this.ExportDataToExcel(string.IsNullOrEmpty(fileName) ? defaultExcelFileName : fileName, gridView, response, hasConstrols);
                        break;
                    case "2":
                        string strData = this.Export(source, true, string.IsNullOrEmpty(delimiter) ? defaultDelimiter : delimiter, string.Empty);
                        byte[] byteData = System.Text.ASCIIEncoding.ASCII.GetBytes(strData);
                        this.ExportDataToCSV(string.IsNullOrEmpty(fileName) ? defaultCsvFileName : fileName, byteData, response);
                        break;
                }
            }
        }

        /// <summary>
        ///  Returns string for a Given Dataset Values
        /// </summary>
        /// <param name="ds">DataSet to export.</param>
        /// <param name="exportColumnHeadings">Indicates whether to export column has headings or not.</param>
        /// <param name="delimeter">Column delimeter string.</param>
        /// <param name="constraint">Filter constraint.</param>
        /// <returns>Returns the string in CSV format</returns>
        private string Export(DataSet ds, bool exportColumnHeadings, string delimeter, string constraint)
        {
            if (!Equals(ds, null) && !Equals(ds.Tables, null) && ds.Tables.Count > 0 && !Equals(ds.Tables[0], null) && ds.Tables[0].Rows.Count > 0)
            {
                StringBuilder header = new StringBuilder();
                StringBuilder body = new StringBuilder();
                StringBuilder record = new StringBuilder();

                // If you want column Heading  to be part of the CSV
                if (exportColumnHeadings)
                {
                    foreach (DataColumn col in ds.Tables[0].Columns)
                    {
                        header.Append(col.ColumnName);
                        header.Append(delimeter);
                    }

                    header = new StringBuilder(header.ToString().Substring(0, header.Length - 1));
                }

                // Iterate into the rows
                DataRow[] dataRows;
                if (string.IsNullOrEmpty(constraint))
                {
                    dataRows = ds.Tables[0].Select();
                }
                else
                {
                    dataRows = ds.Tables[0].Select(constraint);
                }

                foreach (DataRow row in dataRows)
                {
                    object[] arr = row.ItemArray;

                    for (int i = 0; i < arr.Length; i++)
                    {
                        if (arr[i].ToString().IndexOf(",") > 0)
                        {
                            record.Append(row[i].ToString());
                            record.Append(delimeter);
                        }
                        else
                        {
                            record.Append(row[i].ToString());
                            record.Append(delimeter);
                        }
                    }

                    body.Append(record.ToString().Substring(0, record.Length - 1));
                    body.Append(Environment.NewLine);

                    record = new StringBuilder();
                }

                if (!string.IsNullOrEmpty(body.ToString()))
                {
                    if (exportColumnHeadings)
                    {
                        return string.Concat(header.ToString(), Environment.NewLine, body.ToString());
                    }
                    else
                    {
                        return body.ToString();
                    }
                }
                else
                {
                    return string.Concat(header.ToString(), Environment.NewLine);
                    //return string.Empty;
                }
            }
            return string.Empty;
        }

        /// <summary>
        /// Method to Remove the Checkbox Controls from the Grid View, So that the Downloded excel dont show the checkbox in it.
        /// </summary>
        /// <param name="gridViewControl">Control of type GridView</param>
        private void RemoveGridControls(ref GridView gridViewControl)
        {
            // Remove controls from Column Headers
            if (gridViewControl.HeaderRow != null && gridViewControl.HeaderRow.Cells != null)
            {
                for (int rw = 0; rw < gridViewControl.Rows.Count; rw++)
                {
                    GridViewRow row = gridViewControl.Rows[rw];
                    for (int ct = 0; ct < row.Cells.Count; ct++)
                    {
                        // Save header text if found
                        string headerText = row.Cells[ct].Text;

                        // Check for controls in header
                        if (row.Cells[ct].HasControls())
                        {
                            // Check for link buttons (used in sorting)
                            if (row.Cells[ct].Controls[0].GetType().ToString() == "System.Web.UI.WebControls.CheckBox")
                            {
                                // Link button found, get text
                                headerText = ((CheckBox)row.Cells[ct].Controls[0]).Checked.ToString();
                            }

                            // Remove controls from header
                            row.Cells[ct].Controls.Clear();
                        }

                        // Reassign header text
                        row.Cells[ct].Text = headerText;
                    }
                }
            }
        }

        #endregion
    }
}