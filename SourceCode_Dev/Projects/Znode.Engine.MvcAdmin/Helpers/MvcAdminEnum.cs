﻿using System.Runtime.Serialization;

namespace Znode.Engine.MvcAdmin.Helpers
{
    #region Reset Password Status Types
    /// <summary>
    /// Enum for Reset Password Status Types
    /// </summary>
    public enum ResetPasswordStatusTypes
    {
        Continue = 2001,
        LinkExpired = 2002,
        TokenMismatch = 2003,
        NoRecord = 2004,
    }
    #endregion

    #region MessageType
    /// <summary>
    /// Enum for Message Type
    /// </summary>
    public enum MessageType
    {
        MessageBlock = 1,
        Banner = 2
    }
    #endregion

    #region DurationType
    /// <summary>
    /// Enum for Duration Type
    /// </summary>
    public enum DurationType
    {
        Days = 1,
        Weeks = 7,
        Months = 30,
        Years = 365
    }
    #endregion

    #region ProductReviewState
    /// <summary>
    /// Represent the product review state enum
    /// Enumeration ID value must match with ZNodeProductReviewState table ProductReviewStateID column.
    /// </summary>
    public enum ZNodeProductReviewState
    {
        /// <summary>
        /// Product is awaiting for reviewer Aproval.
        /// </summary>
        PendingApproval = 10,

        /// <summary>
        /// Product is approved from Reviewer.
        /// </summary>
        Approved = 20,

        /// <summary>
        /// Product is declined by Reviewer.
        /// </summary>
        Declined = 30,

        /// <summary>
        /// New product added to edit.
        /// </summary>
        ToEdit = 40
    }
    #endregion

    #region Znode CRUD Mode
    public enum ZnodeCRUDMode
    {
        Insert,
        Update,
        Delete,
        Error
    }
    #endregion

    /// <summary>
    /// Represent the Product image state.
    /// </summary>
    public enum SelectImageStatus
    {
        CurrentImage,
        NewImage,
        NoImage
    }

    #region DownloadFileTypes
    /// <summary>
    /// The default file types for download
    /// </summary>
    public enum FileTypes
    {
        [EnumMember(Value = "Microsoft Excel (.xls)")]
        Excel = 1,
        [EnumMember(Value = "Delimited File Format")]
        CSV = 2
    }
    #endregion

    /// <summary>
    /// Enum of status Type.
    /// </summary>
    public enum StatusType
    {
        Enabled = 1,
        Disabled = 0,
    }

    /// <summary>
    /// Represents the Product Image Type
    /// </summary>
    public enum ProductImageType
    {
        Alternate = 1,
        Swatch = 2,
    }

    #region Entity Enum

    /// <summary>
    /// Enum for entity Name
    /// </summary>
    public enum EntityName
    {
        Product,
        SKU,
        Highlight,
        Portal,
        NoImage,
        Category,
        Store,
    }
    #endregion

    /// <summary>
    /// Enum for Product Inventory Options
    /// </summary>
    public enum InventoryOptionsState
    {
        DisablePurchasingForOutOfStockProducts,
        AllowBackOrderingOfProducts,
        DontTrackInventory
    }

    #region Active Status Type
    /// <summary>
    /// Enum of active status list.
    /// </summary>
    public enum ActiveStatusType
    {
        Active = 1,
        Inactive = 0
    }
    #endregion

    #region

    /// <summary>
    /// Enum for Selected tab.
    /// </summary>
    public enum SelectedTab
    {
        Category,
        Skus,
        Images,
        Bundle,
        Facet,
        AddOns,
        Tiers,
        HighLights,
        DigitalAssets,
        Marketing,
        ProductImage,
        ProductFacet,
        CategoryInformation,
        AssociatedProducts,
        Settings,
        Categories,
        General,
        Display,
        Shipping,
        Javascript,
        Catalog,
        Url,
        Profile,
        Units,
        Countries,
        Smtp

    }

    #endregion
    /// <summary>
    /// Represents the PaymentType enumeration
    /// </summary>
    public enum EnumPaymentType
    {
        CreditCard = 1,
        PurchaseOrder = 2,
        PaypalExpressCheckout = 3,
        GoogleCheckout = 4,
        COD = 5,
        Custom = 7
    }

    /// <summary>
    /// Enum for Znode Order State
    /// </summary>
    public enum OrderState
    {
        [EnumMember(Value = "SUBMITTED")]
        Submitted,
        [EnumMember(Value = "SHIPPED")]
        Shipped,
        [EnumMember(Value = "RETURNED")]
        Returned,
        [EnumMember(Value = "CANCELLED")]
        Cancelled,
        [EnumMember(Value = "PENDING APPROVAL")]
        Pending,
    }

    /// <summary>
    /// Represent the product review state enum
    /// Enumeration ID value must match with ZNodeProductReviewState table ProductReviewStateID column.
    /// </summary>
    public enum ZNodeRequestState
    {

        /// <summary>
        /// RMARequest is awaiting for Approval.
        /// </summary>
        PendingAuthorization = 1,

        /// <summary>
        /// RMARequest is approved.
        /// </summary>
        Authorized = 2,

        /// <summary>
        ///RMARequest is Returned Or Refunded.
        /// </summary>
        ReturnedOrRefunded = 3,

        /// <summary>
        /// RMARequest is cancelled.
        /// </summary>
        Void = 4
    }

    /// <summary>
    /// Represents the  default import file templates path for download
    /// </summary>
    public enum ImportFileTypes
    {
        [EnumMember(Value = @"..\Data\SampleExcel\Inventory.xlsx")]
        Inventory = 1,
        [EnumMember(Value = @"..\Data\SampleExcel\Pricing.xlsx")]
        Pricing = 2,
        [EnumMember(Value = @"..\Data\SampleExcel\Product.xlsx")]
        Product = 3,
        [EnumMember(Value = @"..\Data\SampleExcel\Attributes.xlsx")]
        Attribute = 4,
        [EnumMember(Value = @"..\Data\SampleExcel\Facets.xlsx")]
        Facet = 5,
        [EnumMember(Value = @"..\Data\SampleExcel\ShippingStatus.xlsx")]
        Shipping = 6,
        [EnumMember(Value = @"..\Data\SampleExcel\CustomerPricing.xlsx")]
        CustomerPricing = 7,
        [EnumMember(Value = @"..\Data\SampleExcel\SKUs.xlsx")]
        Sku = 8,
        [EnumMember(Value = @"..\Data\SampleExcel\ZipCode.xlsx")]
        ZipCode = 9,
        [EnumMember(Value = @"..\Data\SampleExcel\Customer.xlsx")]
        Customer = 10,
        [EnumMember(Value = @"..\Data\SampleExcel\State.xlsx")]
        State = 11
    }

    public enum Paymentoptions
    {
        [EnumMember(Value = "void")]
        PaymentVoid,
        [EnumMember(Value = "refund")]
        PaymentRefund,
        [EnumMember(Value = "capture")]
        PaymentCapture
    }
    /// <summary>
    /// This enum holds the values for the Znode activation license type
    /// </summary>
    public enum LicenseType
    {
        FreeTrial,
        StoreMultiplier,
        Marketplace,
        MultiFront,
        MallAdmin
    }

    /// <summary>
    /// This enum have Product compare value
    /// </summary>
    public enum ProductCompare
    {
        GlobalLevelCompare,
        CategoryLevelCompare
    }

    //PRFT Custom Code: Start
    public enum CustomValue
    {
        One = 1,
        Two = 2,
        Three = 3
    }
    //PRFT Custom Code: End
}