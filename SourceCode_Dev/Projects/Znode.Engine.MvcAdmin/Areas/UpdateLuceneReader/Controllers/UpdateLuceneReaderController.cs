﻿using Resources;
using System.Web.Mvc;
using Znode.Engine.MvcAdmin.Agents;

namespace Znode.Engine.MvcAdmin.Areas.UpdateLuceneReader.Controllers
{
    [AllowAnonymous]
    public class UpdateLuceneReaderController : Controller
    {
        #region Variables
        private IUpdateLuceneReaderAgent _agent;
        #endregion

        #region Constructor
        public UpdateLuceneReaderController()
        {
            _agent = new UpdateLuceneReaderAgent();
        }
        #endregion

        #region Index
        /// <summary>
        /// This method updates the lucene reader
        /// </summary>
        /// <returns>returns null value if updated successfull, or returns the error message.</returns>
        public ActionResult Index()
        {
            try
            {
                _agent.UpdateReader();
                return null;
            }
            catch
            {
                return Content(ZnodeResources.ErrorUpdatingLuceneReader);
            }
        }
        #endregion
    }
}