﻿using System.Web.Mvc;

namespace Znode.Engine.MvcAdmin.Areas.UpdateLuceneReader
{
    public class UpdateLuceneReaderAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "UpdateLuceneReader";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "UpdateLuceneReader_default",
                "UpdateLuceneReader/{controller}/{action}/{id}",
                new { controller = "UpdateLuceneReader", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}