﻿using Resources;
using System.ComponentModel.DataAnnotations;
using Znode.Libraries.Helpers;

namespace Znode.Engine.MvcDemo.Areas.Diagnostics.Models
{
    public class DiagnosticsViewModel
    {
        [LocalizedDisplayName(Name = RZnodeResources.LabelProductVersion)]
        public string ProductVersion { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelDatabaseStatus)]
        public bool DatabaseStatus { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelFolderPermission)]
        public bool PublicPermissions { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelLicenseStatus)]
        public bool LicenseStatus { get; set; }

        public string LicenseDescription { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelSMTPStatus)]
        public bool SMTPAccountStatus { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelExceptionLog)]
        public string ExceptionSummary { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelCaseNumber)]
        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "RequiredCaseNumber")]
        [MaxLength(6, ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "ErrorCaseNumberRange")]
        public string CaseNumber { get; set; }
    }
}