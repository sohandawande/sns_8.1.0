﻿using Resources;
using System;
using System.Configuration;
using System.Text;
using System.Web.Mvc;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Agents.Agents;
using Znode.Engine.MvcAdmin.Agents.IAgents;
using Znode.Engine.MvcAdmin.Controllers;
using Znode.Engine.MvcDemo.Areas.Diagnostics.Models;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.MvcAdmin.Areas.Diagnostics.Controllers
{
    [AllowAnonymous]
    public class DiagnosticsController : BaseController
    {
        #region Variables

        private int errorCount = 0;
        private StringBuilder exceptionLog = new StringBuilder();
        private IDiagnosticsAgent _diagnosticsAgent;

        #endregion

        #region Constructor

        public DiagnosticsController()
        {
            _diagnosticsAgent = new DiagnosticsAgent();
        }

        #endregion

        #region Index

        /// <summary>
        /// This method gets the status of the database, license, product version etc and pass on that data to diagnostics page
        /// </summary>
        /// <returns>Redirects to the Diagnostics page</returns>
        [HttpGet]
        public ActionResult Index()
        {
            if (Equals(Convert.ToInt32(ConfigurationManager.AppSettings["EnableDiagnosticsPage"]) , 0))
            {
                throw new ApplicationException(ZnodeResources.ErrorEnableDiagnosticsPage);
            }
            DiagnosticsViewModel model = new DiagnosticsViewModel();

            model.DatabaseStatus = CheckDataBaseConnection();

            model.PublicPermissions = CheckPermissions("~/Data/Default/");

            model.SMTPAccountStatus = CheckEmailAccount();

            GetLicenseData(model);

            GetVersionDetails(model);

            if (this.exceptionLog.Length > 0)
            {
                model.ExceptionSummary = Convert.ToString(this.exceptionLog);
            }
            return View(model);
        }

        #endregion

        #region Email diagnostics

        /// <summary>
        /// This method sends the diagnostics email
        /// </summary>
        /// <param name="caseNumber">Case number for diagnostics</param>
        /// <returns>Returns the json result which contains the email sent status and exception log</returns>
        [HttpPost]
        public JsonResult EmailDiagnostics(string caseNumber)
        {
            bool status = false;
            if(string.IsNullOrEmpty(caseNumber))
            {
                return Json(new { Result = status, Log = ZnodeResources.RequiredCaseNumber, IsModelError = true }, JsonRequestBehavior.AllowGet);
            }
            string log = string.Empty;
            try
            {
                System.Net.WebClient webClient = new System.Net.WebClient();
                byte[] diagnosticsData = webClient.DownloadData(Request.UrlReferrer.AbsoluteUri);
                if (!_diagnosticsAgent.EmailDiagnostics(new DiagnosticsEmailModel { MergedText = Encoding.Default.GetString(diagnosticsData), CaseNumber = caseNumber }).HasError)
                {
                    log = ZnodeResources.SuccessSendingDiagnosticsMail;
                    status = true;
                }
                else
                {
                    log = string.Format(ZnodeResources.DiagnosticsExceptionLogFormat, ++errorCount, ZnodeResources.ErrorSendingDiagnosticsMail);
                }
            }
            catch (Exception generalException)
            {
                log = string.Format(ZnodeResources.DiagnosticsExceptionLogFormat, ++errorCount, Convert.ToString(generalException));
            }
            return Json(new { Result = status, Log = log }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Helper methods
        /// <summary>
        /// Check for Database connection String
        /// </summary>
        /// <returns>Returns the status of database connection</returns>
        private bool CheckDataBaseConnection()
        {
            try
            {
                return _diagnosticsAgent.CheckDataBaseConnection();
            }
            catch (Exception generalException)
            {
                this.exceptionLog.Append(string.Format(ZnodeResources.DiagnosticsExceptionLogFormat, ++errorCount, Convert.ToString(generalException)));
                return false;
            }
        }

        /// <summary>
        /// This method validates the license and get the type of license installed
        /// </summary>
        /// <param name="model">It takes DiagnosticsViewModel in which license status and description is to be set</param>
        private void GetLicenseData(DiagnosticsViewModel model)
        {
            try
            {
                ZNodeLicenseType licenseStatus = _diagnosticsAgent.ValidateLicense();
                model.LicenseStatus = (licenseStatus != ZNodeLicenseType.Invalid);
                model.LicenseDescription = _diagnosticsAgent.GetLicenseStatusDescription();
            }
            catch (Exception generalException)
            {
                this.exceptionLog.Append(string.Format(ZnodeResources.DiagnosticsExceptionLogFormat, ++errorCount, Convert.ToString(generalException)));
                model.LicenseStatus = false;
                model.LicenseDescription = ZnodeResources.ErrorGettingLicense;
            }
        }

        /// <summary>
        /// Check Public Folder for Read + Write Permissions
        /// </summary>
        /// <param name="path">Represents the path </param>
        /// <returns>true if the folder have read+write permission</returns>
        private bool CheckPermissions(string path)
        {
            try
            {
                return _diagnosticsAgent.CheckFolderPermissions(path);
            }
            catch
            {
                this.exceptionLog.Append(string.Format(ZnodeResources.DiagnosticsExceptionLogFormat, ++errorCount, ZnodeResources.ErrorFolderPermission));
                return false;
            }
        }

        /// <summary>
        /// Check SMTP Account
        /// </summary>
        /// <returns>Returns the status of smtp server</returns>
        private bool CheckEmailAccount()
        {
            try
            {
                return !_diagnosticsAgent.CheckEmailAccount().HasError;
            }
            catch (Exception generalException)
            {
                this.exceptionLog.Append(string.Format(ZnodeResources.DiagnosticsExceptionLogFormat, ++errorCount, Convert.ToString(generalException)));
                return false;
            }
        }

        /// <summary>
        /// Display Version detail of product from database
        /// </summary>
        /// <param name="model">DiagnosticsViewModel in which version details are to be set</param>
        private void GetVersionDetails(DiagnosticsViewModel model)
        {
            model.ProductVersion = _diagnosticsAgent.GetProductVersionDetails().ProductVersion;
        }

        #endregion
    }
}
