﻿using Resources;
using System.Web.Mvc;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.Agents.Agents;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Areas.Activate.Controllers
{
    [AllowAnonymous]
    public class ActivateController : Controller
    {
        #region Private Variables

        private readonly IActivateAgent _activateAgent;

        #endregion

        #region Controller

        /// <summary>
        /// ActivateController used to Activate the Znode license and retriving the License information which may require in Diagnostics page.
        /// </summary>
        public ActivateController()
        {
            _activateAgent = new ActivateAgent();
        }

        #endregion

        #region Index

        /// <summary>
        /// This method first validates whether the licence is active for current server or not
        /// If License is not activated, it redirects to the Activation page else redirects to the Login page
        /// </summary>
        /// <returns>Redirects to the License activation/Login page</returns>
        [HttpGet]
        public ActionResult Index()
        {
            ActivateLicenseResponse response = _activateAgent.ValidateLicense();
            if (!Equals(response, null) && response.HasError)
            {
                return View();
            }
            else
            {
                return RedirectToAction(MvcAdminConstants.LoginAction, MvcAdminConstants.AccountController, new { area = string.Empty });
            }
        }

        #endregion

        #region License Information

        /// <summary>
        /// This method redirects to the licence information page if user confirms the licence activation process
        /// </summary>
        /// <returns>Redirects to the licence information page</returns>
        public ActionResult LicenseInformation(bool ConfirmedActivation = false)
        {
            ActivateLicenseResponse response = _activateAgent.ValidateLicense();
            if (!Equals(response, null) && response.HasError)
            {
                return View(MvcAdminConstants.LicenseInformationView, new ActivatePageApiModel());
            }
            else
            {
                return View(MvcAdminConstants.LicenseInformationView, new ActivatePageApiModel { Message = ZnodeResources.LicenseActivatedSuccess });
            }
        }

        #endregion

        #region Create License

        /// <summary>
        /// This method is used to install the License.
        /// This creates the .dlsc file in the Licence folder on the server specified the model
        /// This method internally call the ZNode.Libraries.Framework.Business.ZNodeLicenseManager class to install the license
        /// </summary>
        /// <param name="model">ActivatePageApiModel which should contain the type of the license user want to activate, and detail information of the user</param>
        /// <returns>This method redirects to the LicenseInformation page with the notification message whether the license activated successfully or not</returns>
        [HttpPost]
        public ActionResult Create(ActivatePageApiModel model)
        {
            if (!Equals(model.LicenseType, LicenseType.FreeTrial) && string.IsNullOrEmpty(model.SerialNumber))
            {
                string serialNumberProperty = "SerialNumber";
                ModelState.AddModelError(serialNumberProperty, string.Format(ZnodeResources.ErrorRequired, ZnodeResources.LabelSerialNumber));
            }
            if (ModelState.IsValid)
            {
                ActivateLicenseResponse response = _activateAgent.InstallLicense(model);
                if (!Equals(response, null) && !response.HasError)
                {
                    model.Message = ZnodeResources.LicenseActivatedSuccess;
                }
                else
                {
                    model.ErrorMessage = string.Concat(ZnodeResources.LicenseActivatedError, response.ErrorMessage);
                }
            }
            return View(MvcAdminConstants.LicenseInformationView, model);
        }

        #endregion
    }
}
