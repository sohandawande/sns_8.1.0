﻿using Microsoft.CSharp.RuntimeBinder;
using System;
using System.Dynamic;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Xml.Linq;

namespace Znode.Engine.MvcAdmin.Extensions
{
    public static class ObjectExtension
    {
        /// <summary>
        ///   Searches for the public property with the specified name.
        /// </summary>
        /// <param id="name">The string containing the name of the public property to get.</param>
        /// <returns> An object representing the public property with the specified name, if found otherwise, null.</returns>
        /// <exception cref="System.Reflection.AmbiguousMatchException">if name already exist. "></exception>
        /// <exception cref="System.ArgumentNullException">name is null.</exception>
        public static object GetProperty(this object obj, string name)
        {
            object propValue = null;
            PropertyInfo propInfo = obj.GetType().GetProperty(name);
            if (!Equals(propInfo, null))
            {
                propValue = propInfo.GetValue(obj, null);
            }
            return propValue;
        }


        public static void SetPropertyValue(this object obj, string propertyName, object value)
        {
            PropertyInfo propInfo = obj.GetType().GetProperty(propertyName);
            if (!Equals(propInfo, null))
            {
                propInfo.SetValue(obj, value);
            }
        }

        public static string TryGetElementValue(this XElement parentEl, string elementName, string defaultValue = null, string type = null)
        {
            var foundEl = parentEl.Element(elementName);
            string result = string.Empty;
            if (!Equals(foundEl, null))
            {
                result = foundEl.Value;
                if (string.IsNullOrEmpty(result.Trim()))
                {
                    if (Equals(type, "Char"))
                    {
                        return "n";
                    }
                    else if (Equals(type, "Int"))
                    {
                        return "0";
                    }
                }
                return result;
            }

            if (Equals(type, "Char"))
            {
                return "n";
            }
            else if (Equals(type, "Int"))
            {
                return "0";
            }
            return defaultValue;
        }

        public static object GetDynamicProperty(this object o, string member)
        {
            try
            {
                if (o == null) throw new ArgumentNullException("o");
                if (member == null) throw new ArgumentNullException("member");
                Type scope = o.GetType();
                IDynamicMetaObjectProvider provider = o as IDynamicMetaObjectProvider;
                if (provider != null)
                {
                    ParameterExpression param = Expression.Parameter(typeof(object));
                    DynamicMetaObject mobj = provider.GetMetaObject(param);
                    GetMemberBinder binder = (GetMemberBinder)Microsoft.CSharp.RuntimeBinder.Binder.GetMember(0, member, scope, new CSharpArgumentInfo[] { CSharpArgumentInfo.Create(0, null) });
                    DynamicMetaObject ret = mobj.BindGetMember(binder);
                    BlockExpression final = Expression.Block(
                        Expression.Label(CallSiteBinder.UpdateLabel),
                        ret.Expression
                    );
                    LambdaExpression lambda = Expression.Lambda(final, param);
                    Delegate del = lambda.Compile();
                    return del.DynamicInvoke(o);
                }
                else
                {
                    return o.GetType().GetProperty(member, BindingFlags.Public | BindingFlags.Instance).GetValue(o, null);
                }

            }
            catch(Exception ex)
            {
                return null;
            }
        }


    }
}