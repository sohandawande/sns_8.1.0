﻿using System;
using Znode.Engine.MvcAdmin.ViewModels;
namespace Znode.Engine.MvcAdmin.Models
{
    public class ApplicationSettingModel : BaseViewModel
    {
        public Int64 Id { get; set; }
        public string SettingTableName { get; set; }
        public string GroupName { get; set; }
        public string ItemName { get; set; }
        public string Setting { get; set; }
        public int StatusId { get; set; }
        public Nullable<DateTime> CreatedDate { get; set; }
        public Nullable<DateTime> ModifiedDate { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public string CreatedByName { get; set; }
        public string ModifiedByName { get; set; }
        public string ViewOptions { get; set; }
        public string FrontPageName { get; set; }
        public string FrontObjectName { get; set; }
        public bool IsCompressed { get; set; }
        public string ActionMode { get; set; }
    }
}