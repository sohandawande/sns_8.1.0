﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcAdmin.Models
{
    public class ChartModel
    {
        public string CountClause { get; set; }
        public string GroupClause { get; set; }
        public int RecordCount { get; set; }
    }
}