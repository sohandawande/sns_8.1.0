﻿namespace Znode.Engine.MvcAdmin.Models
{
    /// <summary>
    /// Model to update imagepath
    /// </summary>
    public class UpdateImageModel
    {
        public int Id { get; set; }
        public string EntityName { get; set; }
        public string ImagePath { get; set; }
    }
}