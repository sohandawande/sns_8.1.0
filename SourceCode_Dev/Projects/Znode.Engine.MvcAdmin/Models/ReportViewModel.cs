﻿
namespace Znode.Engine.MvcAdmin.Models
{
    public class ReportViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string HeaderText { get; set; }
        public string IsVisible { get; set; }
        public string MustHide { get; set; }
        public string MustShow { get; set; }
        public string ForSearch { get; set; }
        public string DataType { get; set; }
        public string GraphX { get; set; }
        public string GraphY { get; set; }
    }
}