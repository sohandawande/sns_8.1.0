﻿namespace Znode.Engine.MvcAdmin.Models
{
    public class CartItemModel
    {
        public decimal ProductAmount { get; set; }
        public string ProductDescription { get; set; }
        public string ProductName { get; set; }
        public string ProductNumber { get; set; }
        public int ProductQuantity { get; set; }
    }
}