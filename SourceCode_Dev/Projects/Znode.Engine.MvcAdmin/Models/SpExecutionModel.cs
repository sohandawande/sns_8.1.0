﻿using System.Collections.Generic;

namespace Znode.Engine.MvcAdmin.Models
{
    public class SpExecutionModel
    {
        public string SelectClause { get; set; }
        public string WhereClause { get; set; }
        public string CountClause { get; set; }
        public string GroupByClause { get; set; }
        public string HavingClause { get; set; }
        public string OrderBy { get; set; }
        public string SpName { get; set; }
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public int TotalRowCount { get; set; }
        public int TotalRecordCount { get; set; }
        public List<dynamic> GetReportDetails { get; set; }
        public List<ReportGraph> ReportGraph { get; set; }
    }
}