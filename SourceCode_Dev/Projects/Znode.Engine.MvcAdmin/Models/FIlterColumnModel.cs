﻿using System;
using System.Xml.Serialization;

namespace Znode.Engine.MvcAdmin.Models
{
    [Serializable]
    [XmlRoot("columns"), XmlType("columns")]
    public class FilterColumnModel
    {
        [XmlElement("name")]
        public string ColumnName { get; set; }

        [XmlElement("headertext")]
        public string HeaderText { get; set; }

        [XmlElement("datatype")]
        public string DataType { get; set; }

        public string Value { get; set; }

       
    }
}


