﻿using System.Collections.Generic;

namespace Znode.Engine.MvcAdmin.Models
{
    public class FilterColumnListModel
    {
        public List<FilterColumnModel> FilterColumnList { get; set; }

        public List<dynamic> GridColumnList { get; set; }

        public List<dynamic> DropdownList { get; set; }

        public string FilterDateFormat
        {
            get
            {
                return Helpers.HelperMethods.DatePickDateFormat();
            }
        }

        public FilterColumnListModel()
        {
            FilterColumnList = new List<FilterColumnModel>();
            DropdownList = new List<dynamic>();
        }
    }
}