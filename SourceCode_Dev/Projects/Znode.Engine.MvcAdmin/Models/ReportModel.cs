﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Helpers;
using System.Web.Mvc;
namespace Znode.Engine.MvcAdmin.Models
{
    public class ReportModel
    {

        public List<WebGridColumn> WebGridColumn { get; set; }
        public List<dynamic> DataTableToList { get; set; }
        public string XmlOperatorString { get; set; }
        public int RowCount { get; set; }
        public string FilterCondition { get; set; }
        public int TotalRowCount { get; set; }
        public int RowPerPageCount { get; set; }
        public List<dynamic> TotalOpratorList { get; set; }
        public int TableWidth { get; set; }
        public int TotalRecordCount { get; set; }
        public ReportSettingModel ReportSettingModel { get; set; }
        public FilterColumnListModel FilterColumn { get; set; }

        public SortDirection GridSortDirection { get; set; }

        public string GridSortColumn { get; set; }

        [UIHint("TwoListView")]
        public ListViewModel FilterColumnList { get; set; }
        public DropdownListModel FieldList { get; set; }

        /// <summary>
        /// Field name for fetch details records.
        /// </summary>
        public string SubRecordFilterKeyName { get; set; }

        /// <summary>
        /// Class name with namespace ex. : Znode.Engine.MvcAdmin.Agents.ReportsAgent
        /// </summary>
        public string DelegateTypeName { get; set; }

        /// <summary>
        /// Method name for fetching detail recoreds.
        /// </summary>
        public string DelegateTypeMethodName { get; set; }

        /// <summary>
        /// Fields collection for graph X Axis 
        /// </summary>
        public List<SelectListItem> XAxisFields { get; set; }

        /// <summary>
        /// Fields collection for graph y Axis 
        /// </summary>
        public List<SelectListItem> YAxisFields { get; set; }
        public List<SelectListItem> ExportFileType { get; set; }

        public int ExportFileTypeId { get; set; }

        public bool IsAdvanceSearchHide { get; set; }

        public ReportModel()
        {
            FilterColumnList = new ListViewModel();
        }

    }
}