﻿
namespace Znode.Engine.MvcAdmin.Models
{
    public class ReportGraph
    {
        public string CountClause { get; set; }
        public string GroupClause { get; set; }
        public int RecordCount { get; set; }
    }
}