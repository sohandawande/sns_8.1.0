﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.WebPages;
public static class WebGridExtensions
{

    public static HelperResult PagerList(
    this WebGrid webGrid,
    WebGridPagerModes mode = WebGridPagerModes.NextPrevious | WebGridPagerModes.Numeric,
    string firstText = null,
    string previousText = null,
    string nextText = null,
    string lastText = null,
    bool showPageNo = false,
    int PageSize = 5,
    bool showPageSize = false,
    int numericLinksCount = 3)
    {
        return PagerList(webGrid, mode, firstText, previousText, nextText, lastText, showPageNo, PageSize, showPageSize, numericLinksCount, explicitlyCalled: true);
    }

    private static HelperResult PagerList(
    WebGrid webGrid,
    WebGridPagerModes mode,
    string firstText,
    string previousText,
    string nextText,
    string lastText,
    bool showPageNo,
    int PageSize,
    bool showPageSize,
    int numericLinksCount,
    bool explicitlyCalled)
    {

        int currentPage = webGrid.PageIndex;
        int totalPages = webGrid.PageCount;
        int lastPage = totalPages - 1;
        //if (lastPage > 0)
        //{
        try
        {
            var ul = new TagBuilder("ul");
            var li = new List<TagBuilder>();
            var option = new TagBuilder("select");

            #region First
            if (ModeEnabled(mode, WebGridPagerModes.FirstLast))
            {
                if (String.IsNullOrEmpty(firstText))
                {
                    firstText = "";
                }

                var part = new TagBuilder("li")
                {
                    InnerHtml = GridLink(webGrid, webGrid.GetPageUrl(0), firstText, "<span class='first' title='First'></span>")
                };

                if (currentPage == 0)
                {
                    part.MergeAttribute("class", "disabled");
                }
                //else
                //{
                //    part.MergeAttribute("class", "first");
                //}

                part.MergeAttribute("title", "First");

                li.Add(part);

            }
            #endregion

            #region Prev
            if (ModeEnabled(mode, WebGridPagerModes.NextPrevious))
            {
                if (String.IsNullOrEmpty(previousText))
                {
                    previousText = "";
                }

                int page = currentPage == 0 ? 0 : currentPage - 1;

                var part = new TagBuilder("li")
                {
                    InnerHtml = GridLink(webGrid, webGrid.GetPageUrl(page), previousText, "<span class='prev' title='Prev'></span>")
                };

                if (currentPage == 0)
                {
                    part.MergeAttribute("class", "disabled prev-border");
                }
                else
                {
                    //part.MergeAttribute("class", "prev");
                    part.MergeAttribute("class", "prev-border");
                }

                part.MergeAttribute("title", "Prev");
                li.Add(part);

            }
            #endregion

            #region Main Pages
            if (ModeEnabled(mode, WebGridPagerModes.Numeric))
            {
                int last = currentPage + (numericLinksCount / 2);
                int first = last - numericLinksCount + 1;
                if (last > lastPage)
                {
                    first -= last - lastPage;
                    last = lastPage;
                }
                if (first < 0)
                {
                    last = Math.Min(last + (0 - first), lastPage);
                    first = 0;
                }
                for (int i = first; i <= last; i++)
                {

                    var pageText = (i + 1).ToString(CultureInfo.InvariantCulture);
                    var part = new TagBuilder("li");

                    if (showPageNo)
                    {
                        // show page number
                        part = new TagBuilder("li")
                        {
                            InnerHtml = GridLink(webGrid, webGrid.GetPageUrl(i), pageText)
                        };
                        if (i == currentPage)
                        {
                            part.MergeAttribute("class", "active");
                        }
                    }
                    else
                    {
                        // show textbox to move page number
                        if (i == currentPage)
                        {
                            part = new TagBuilder("li")
                            {
                                InnerHtml = "Page" + GridLinkText(webGrid, webGrid.GetPageUrl(i), pageText, totalPages) + "Of " + totalPages
                            };

                            part.MergeAttribute("class", "active");
                        }
                    }

                    li.Add(part);

                }
            }
            #endregion

            #region Next
            if (ModeEnabled(mode, WebGridPagerModes.NextPrevious))
            {
                if (String.IsNullOrEmpty(nextText))
                {
                    nextText = "";
                }

                int page = currentPage == lastPage ? lastPage : currentPage + 1;

                var part = new TagBuilder("li")
                {
                    InnerHtml = GridLink(webGrid, webGrid.GetPageUrl(page), nextText, "<span class='next' title='Next'></span>")
                };

                if (currentPage == lastPage)
                {
                    part.MergeAttribute("class", "disabled next-border");
                }
                else
                {
                    //part.MergeAttribute("class", "next");
                    part.MergeAttribute("class", "next-border");
                }

                part.MergeAttribute("title", "Next");
                li.Add(part);

            }
            #endregion

            #region Last
            if (ModeEnabled(mode, WebGridPagerModes.FirstLast))
            {
                if (String.IsNullOrEmpty(lastText))
                {
                    lastText = "";
                }

                var part = new TagBuilder("li")
                {
                    InnerHtml = GridLink(webGrid, webGrid.GetPageUrl(lastPage), lastText, "<span class='last' title='Last'></span>")
                };

                if (currentPage == lastPage)
                {
                    part.MergeAttribute("class", "disabled");
                }
                //else
                //{
                //    part.MergeAttribute("class", "last");
                //}
                part.MergeAttribute("title", "Last");
                li.Add(part);

            }
            #endregion

            #region Page Size Container
            if (showPageSize)
            {
                option = new TagBuilder("select")
                {
                    InnerHtml = GridPageSizeLink(PageSize)
                };
                option.Attributes["class"] = "webGridPageSize";
            }
            #endregion

            ul.InnerHtml = string.Join("", li);

            var html = "";
            if (explicitlyCalled && webGrid.IsAjaxEnabled)
            {
                var span = new TagBuilder("span");
                span.MergeAttribute("data-swhgajax", "true");
                span.MergeAttribute("data-swhgcontainer", webGrid.AjaxUpdateContainerId);
                span.MergeAttribute("data-swhgcallback", webGrid.AjaxUpdateCallback);

                span.InnerHtml = ul.ToString();
                html = span.ToString();

            }
            else
            {
                html = ul.ToString();
            }
            return new HelperResult(writer =>
            {
                var optionText = "";
                if (showPageSize)
                {
                    optionText = "<span>Show </span>" + option.ToString() + "<span> Per Page</span>";
                }
                writer.Write("<div class='pagination clearfix'>" + html + optionText + "</div>");
            });
        }
        catch (Exception ex)
        {
            return new HelperResult(writer =>
            {
                writer.Write("");
            });
        }
        //}
        //return new HelperResult(writer =>
        //{
        //    writer.Write("");
        //});
    }

    private static String GridLink(WebGrid webGrid, string url, string text, string span = "")
    {
        TagBuilder builder = new TagBuilder("a");
        builder.SetInnerText(text);
        builder.MergeAttribute("href", url);
        builder.Attributes["title"] = text;
        builder.InnerHtml = span;
        if (webGrid.IsAjaxEnabled)
        {
            builder.MergeAttribute("data-swhglnk", "true");
        }
        return builder.ToString(TagRenderMode.Normal);
    }
    private static String GridLinkText(WebGrid webGrid, string url, string text, int totalPages)
    {
        TagBuilder builder = new TagBuilder("input");
        builder.Attributes["value"] = text;
        builder.Attributes["maxlength"] = Convert.ToString(Convert.ToString(totalPages).Count());
        builder.Attributes["type"] = "text";
        builder.Attributes["data-href"] = url;
        builder.Attributes["data-totalpage"] = Convert.ToString(totalPages);
        if (totalPages == 1)
        {
            builder.Attributes["readonly"] = "readonly";
        }
        // builder.Attributes["style"] = "width:40px;";
        builder.Attributes["class"] = "WebGridPaginTextBox";
        if (webGrid.IsAjaxEnabled)
        {
            builder.MergeAttribute("data-swhglnk", "true");
        }
        // Hidden a tag

        TagBuilder aTagbuilder = new TagBuilder("a");
        aTagbuilder.SetInnerText(text);
        aTagbuilder.MergeAttribute("href", url);
        aTagbuilder.Attributes["style"] = "display:none;";
        aTagbuilder.Attributes["class"] = "ATagWebGridPagin";
        if (webGrid.IsAjaxEnabled)
        {
            aTagbuilder.MergeAttribute("data-swhglnk", "true");
        }
        return builder.ToString(TagRenderMode.Normal) + aTagbuilder.ToString(TagRenderMode.Normal);
    }

    private static String GridPageSizeLink(int pageSize)
    {
        StringBuilder optionText = new StringBuilder();
        int pageno = 10;
        for (int i = 1; i <= 5; i++)
        {
            TagBuilder tagBuilder = new TagBuilder("option");
            tagBuilder.MergeAttribute("value", pageno.ToString());
            if (pageno == pageSize)
            {
                tagBuilder.MergeAttribute("selected", "selected");
            }
            tagBuilder.SetInnerText(pageno.ToString());
            optionText.AppendFormat(tagBuilder.ToString());
            pageno = pageno + 10;
        }

        return optionText.ToString();
    }

    private static bool ModeEnabled(WebGridPagerModes mode, WebGridPagerModes modeCheck)
    {
        return (mode & modeCheck) == modeCheck;
    }
}
