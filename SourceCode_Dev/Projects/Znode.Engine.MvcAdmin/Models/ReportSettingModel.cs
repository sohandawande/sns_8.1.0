﻿
namespace Znode.Engine.MvcAdmin.Models
{
    public class ReportSettingModel
    {
        public string GroupName { get; set; }
        public string ItemName { get; set; }
        public string Setting { get; set; }
        public string ViewOptions { get; set; }
    }
}