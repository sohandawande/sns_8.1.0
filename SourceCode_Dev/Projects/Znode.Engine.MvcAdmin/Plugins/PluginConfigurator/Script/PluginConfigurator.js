PluginConfigurator = {
    ResetAccountPassword: function (title, message, url) {
        this.ShowResetPasswordPopUp(title, message, url);
        return false;
    },
    ShowResetPasswordPopUp: function (title, message, url) {
        if ($("BODY").find("div#popup_container").length == 0) {
            this.CreatePopUp(title);
        }

        $("#popup_title").html(title);
        $("#popup_message").html(message);

        $("#popup_container").dialog({
            resizable: true,
            title: title,
            modal: true,
            stack: true,
            width: 400,
            buttons: {
                "OK": function () {
                    $(this).dialog("close");
                    window.location.href = url;
                },
                Cancel: function () {
                    $(this).dialog("close");
                }
            }
        });
    },
    CreatePopUp: function (title) {
        var popup = '<div id="popup_container" class="ConfirmPopup" style="display:none;">' +
                        '<div class="PopupHeder ClearFix">' +
                        '</div>' +
                        '<div id="popup_content" class="PopupContent ClearFix">' +
                          '<div id="popup_message"></div>' +
                        '</div>' +
            '</div>';
        $("BODY").append(popup);
    },
}