﻿GoogleSiteMap = {
    Init: function () {
        $("#modifieddate-content input[type=radio]").click(function () {
            GoogleSiteMap.DisplayCustomDate(this);
        });
        GoogleSiteMap.DisplayXMLSiteMapType();
    },
    DisplayXMLSiteMapType: function () {
        if ($('#ddlXMLSiteMap').val() == '1') {
            $('#rdbXMLSiteMapType').show();
            $('#GoogleFeedFields').hide();
        } else if ($('#ddlXMLSiteMap').val() == '2') {
            $('#rdbXMLSiteMapType').hide();
            $('#GoogleFeedFields').show();
        } else if ($('#ddlXMLSiteMap').val() == '3') {
            $('#rdbXMLSiteMapType').hide();
            $('#GoogleFeedFields').hide();
        }
    },

    DisplayCustomDate: function (control) {        
        if ($.trim($(control).val()) === "Use date / time of this update") {
            $('#CustomDate-content').show();
            var dt = new Date(Date.now());
            var date = dt.toLocaleDateString() + " " + dt.toLocaleTimeString();
            $('#DBDate').val(date);
        }
        else {
            $('#CustomDate-content').hide();
        }

        if ($.trim($(control).val()) === "Use the database update date") {
            var dt = new Date(Date.now());
            var date = dt.toLocaleDateString() + " " + dt.toLocaleTimeString();
            $('#DBDate').val(date);
        }
    }
}

$(document).ready(function () {
    GoogleSiteMap.Init();
});