﻿ContentPage = {
    BindCssList: function (cssList, cssThemeId, userType) {

        ContentPage.DropDownSelect(cssThemeId.value);
        var url = "";
        var currentUserType = userType;
        if (userType == "FranchiseAdmin") {
            url = "/FranchiseAdmin/FranchiseContentPage/BindCssList";
        }
        else {
            url = "/ContentPage/BindCssList";
        }
        App.Api.getCssListByThemeId(url, cssThemeId.value, function (response) {
            $('#' + cssList.id).empty();
            for (var i = 0; i < response.length; i++) {
                $('#' + cssList.id).append("<option value='" + response[i].Value + "'>" + response[i].Text + "</option>");
            }
            if (response.length < 1) {
                $('#' + cssList.id).append("<option value=''>Same as store</option>");
            }
        });
    },

    BindMasterPageList: function (masterPageList, cssThemeId, userType) {
        var url = "";
        if (userType == "FranchiseAdmin") {
            url = "/FranchiseAdmin/FranchiseContentPage/BindMasterPageList";
        }
        else {
            url = "/ContentPage/BindMasterPageList";
        }
        App.Api.getMasterPageListByThemeId(url, cssThemeId, function (response) {
            $('#' + masterPageList.id).empty();
            for (var i = 0; i < response.length; i++) {
                $('#' + masterPageList.id).append("<option value='" + response[i].Value + "'>" + response[i].Text + "</option>");
            }
            if (response.length < 1) {
                $('#' + masterPageList.id).append("<option value=''>Same as store</option>");
            }
        });
    },

    DropDownSelect: function (id) {
        if (id > 0) {
            $("#css_list").show();
        }
        else {
            $("#css_list").hide();
        }
    }
}

$(document).ready(function () {
    var selectedIndex = $("#themeList").val()
    if (selectedIndex > 0) {
        $("#css_list").show();
    }
    else {
        $("#css_list").hide();
    }
})