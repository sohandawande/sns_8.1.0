﻿var newselectedPortalId = "";
var newselectedCatalogId = "";

var Orders = {
    lastSelectedAttributeId: 0,
    productId: 0,
    Load: function () {

        $("#SelectedPortalId").on('change', Orders.PortalOnChangeHandler);
        $(document).on('change', '#catalogDropdown', Orders.CatalogOnChangeHandler);
        $(document).on('click', '#dialogCancelBtn', Orders.CloseAddNewCustomerDialog);
        $(document).off("CustomerAddedSuccessEvent");
        $(document).on("CustomerAddedSuccessEvent", function (e) {
            var accountId = parseInt(e.message);
            var url = Orders.GetRoleBaseUrl("/Orders/GetCustomerInformation");
            App.Api.GetCustomerInformation(url, accountId, Orders.CustomerInformationCallBack);
        });

        $(document).on('click', '.accountIdLink', function (e) {
            e.preventDefault();
            var accountId = parseInt($(this).attr('href').match(/\d+/));
            var url = Orders.GetRoleBaseUrl("/Orders/GetCustomerInformation");
            App.Api.GetCustomerInformation(url, accountId, Orders.CustomerInformationCallBack);
        });

        $(document).on("change", "#Quantity", function (e) {
            e.preventDefault();
            $(this).closest("form").attr("guid")
            $(this).closest("form").submit();
            Orders.ApplyPromoCode(false);
        });

        $(document).on("click", "#RemoveCartItem", function (e) {
            e.preventDefault();
            $(this).closest("form").attr("guid")
            $(this).closest("form").submit();
        });

        $(document).on("click", "#paypal-express-checkout", function (e) {
            e.preventDefault();
            Orders.PaypalExpressCheckout();
        });

        try {
            if (userType.toLowerCase() == "franchiseadmin") {
                Orders.ShowFranchiseOrder();
            }
        }
        catch (err) { }
    },
    PortalOnChangeHandler: function () {
        $("#MessageBoxContainerId").hide();
        var selectedPortalId = $(this).val();
        Orders.SetProtalCatalogIdInUrl("portalId", selectedPortalId);
        App.Api.GetCatalogSelectList(selectedPortalId, function (response) {
            $("#catalogDropdown").html(response);

            var anchorHref = $("#addNewCustomerLink").attr('href');
            var url = window.location.protocol + "//" + window.location.host + anchorHref;

            var newUrl = new jurl(url);
            var oldPortalId = newUrl.getQueryParameter("portalId");

            if (oldPortalId != null) {
                anchorHref = anchorHref.replace("portalId=" + oldPortalId, "portalId=" + selectedPortalId);
            }
            else {
                anchorHref = anchorHref + "?portalId=" + selectedPortalId;
            }
            newselectedPortalId = selectedPortalId;
            $("#addNewCustomerLink").attr('href', anchorHref);
            $("#SelectedCatalogId").on('change', Orders.CatalogOnChangeHandler);
            $("#addNewCustomerLinkContainer").show();
            $("#divAdditionalInfo").hide();
        });
        Orders.ClearShoppingCart();
    },
    CatalogOnChangeHandler: function () {
        var selectedCatalogId = $(this).val();
        Orders.SetProtalCatalogIdInUrl("catalogId", selectedCatalogId);
        newselectedCatalogId = selectedCatalogId;
        $("#customerLinkContainer").show();
        $("#shoppingCartContainer").show();
    },
    ShippingChangeHandler: function (obj) {
        var selectedId = $('#' + obj.id + ' option:selected').val();
        var url = Orders.GetRoleBaseUrl("/Orders/CalculateShippingCharges");
        App.Api.CalculateShippingCharges(url, selectedId, function (response) {
            $("#divTotal").html(response);
        });
    },
    ClearShoppingCart: function () {
        $("#shoppingCartContainer").hide();
        $("#addressDisplayContainer").html('');
        $("#CartItemContainer").html('');
    },
    AddNewCustomerDialog: function () {
        $("#addNewCustomerContainer").dialog({
            title: "Add New Customer",
            resizable: false,
            modal: true,
            create: function () { $(this).closest(".ui-dialog").addClass("ui-lg-popup add-new-customer"); }
        });
    },
    ChangeAddressDialog: function () {
        $("#addNewCustomerContainer").dialog({
            title: "Change Customer Address",
            resizable: false,
            modal: true,
            create: function () { $(this).closest(".ui-dialog").addClass("ui-lg-popup add-new-customer"); }
        });
    },
    SearchProducts: function () {
        var address = $("#addressDisplayContainer").html().trim();
        var validaddress = Orders.ValidateAddress();
        if (validaddress == false) {
            return false;
        }
        if (address.length > 1) {
            $("#searchProductsContainer").dialog({
                title: "Search Products List",
                resizable: false,
                modal: true,
                create: function () { $(this).closest(".ui-dialog").addClass("ui-lg-popup search-product-container"); }
            });
            $(".ui-dialog-titlebar-close").click(function () {
                $("#searchProductsContainer").html('');
            });
        }
        else {
            CommonHelper.DisplayNotificationMessagesHelper(customererror, "error", "true", 5000);
            return false;
        }
    },
    CustomerInformationCallBack: function (result) {
        $("#addressDisplayContainer").html(result);
        $("#customerListContainer").html('');
        $("#customerListContainer").dialog("close");
    },
    ShippingSameAsBillingHandler: function () {
        if ($("#shippingSameAsBillingAddress").is(':checked')) {
            $("#shippingAddressContainer").hide();
        }
        else {
            $("#shippingAddressContainer").show();
        }
    },
    CloseAddNewCustomerDialog: function () {
        $("#addNewCustomerContainer").dialog("close");
    },
    SearchCustomerDialog: function (response) {
        $("#customerListContainer").dialog({
            title: "Customer Search",
            modal: true,
            create: function () { $(this).closest(".ui-dialog").addClass("ui-lg-popup search-product-container"); }
        });
        $(".ui-dialog-titlebar-close").click(function () {
            $("#customerListContainer").html('');
        });
        $("#customerListContainer tr td:first-child a").addClass("accountIdLink");
    },
    AddCustomerSuccessCallback: function (response) {
        if (response.indexOf("field-validation-error") < 0) {
            $("#addNewCustomerContainer").dialog("close");

            $.event.trigger({
                type: "CustomerAddedSuccessEvent",
                message: response
            });
        }
        else {
            $("#addNewCustomerContainer").html(response);
            $("#shippingSameAsBillingAddress").attr('checked', false);
        }
    },
    SetAddonsValues: function (len, addonClass, addonvalueid, addonvaluecustomtext, selectedIds, selectedCustomText, response) {
        if (len > 0) {
            for (var i = 0; i < len; i++) {
                var id = $("." + addonClass).attr("id");
                var value = $("#" + id + " option:selected").val();
                $('#' + id + ' option[value="' + value + '"]').prop('selected', true);
                $("#" + addonvalueid).val(value);
                if (selectedIds != undefined && selectedCustomText != undefined) {
                    $("#" + addonvalueid).val(selectedIds);
                    $("#" + addonvaluecustomtext).val(selectedCustomText);
                }
            }
        }
    },
    SetTotalOnQuantityChange: function (ProductDetailsQuantity, RetailPrice, dynamicQuantity, Total, response) {
        var selectedValue = $("#" + ProductDetailsQuantity).val();
        var price = $("#" + RetailPrice).text();
        if (!price) {
            price = 0;
        }
        var curSymbol = price.substring(0, 1);
        var unitPrice = price.substring(1);
        var extendedPrice = parseFloat(unitPrice) * parseInt(selectedValue);
        extendedPrice = extendedPrice.toFixed(2);
        $("#" + dynamicQuantity).val(selectedValue);
        $("#" + Total).val(extendedPrice);
        $("#" + Total).text(curSymbol + extendedPrice);
    },
    AddToCart: function (formaddtocart, response) {
        var IsDetails = Orders.getProductDetails();
        if (IsDetails) {
            overlayforwaitblock();
            var cartItem = $("#" + formaddtocart).serialize();
            var url = "/Orders/AddToCart";
            try {
                if (userType.toLowerCase() == "franchiseadmin") {
                    url = "/FranchiseOrders/AddToCart";
                }
            }
            catch (err) {
            }
            App.Api.addToCart(url, cartItem, function (response) {
                $("#searchProductsContainer").html('');
                $("#searchProductsContainer").dialog("close");
                $("#CartItemContainer").html('<h4>Product Details</h4>' + response);
                $("#CartItemContainer").show();
                $("#divAdditionalInfo").show();
                overlayforwaitnone();
            });
        }
    },
    AddAnotherProduct: function (formaddtocart, response) {
        var cartItem = $("#" + formaddtocart).serialize();
        var url = Orders.GetRoleBaseUrl("/Orders/AddAnotherProduct");
        App.Api.addAnotherProduct(url, cartItem, function (response) {
            $("#searchProductsLink").click();
        });
    },
    ProductDetails: function (ProductDetailsContainer, grid, response) {
        $("." + ProductDetailsContainer).html("");
        $("#" + grid).each(function (e) {
            $($(this).find("td")[0]).on("click", function () {
                var url = $(this).find("a").attr('href');
                $(this).find("a").attr('href', 'javascript:void(0);');
                $.ajax({
                    type: "POST",
                    url: url,
                    dataType: "html",
                    success: function (response) {
                        $(".DynamicGrid").hide();
                        $(".pagination").hide();
                        $(".ProductDetailsContainer").html(response);
                        $(".ProductDetailsContainer").show();
                        Orders.lastSelectedAttributeId = 0;
                        Orders.productAddOnsHandler();
                        Orders.productAttributeHandler();
                        Orders.productQuantityHandler();
                        $("#ProductDetailsQuantity").change();
                    },
                    error: function () {
                        ajax.errorOut("API Endpoint not available: " + url);
                    }
                });
            });
        });
    },
    RemoveCartItem: function (classRamove) {
        $("." + classRamove).closest("form").submit();
    },
    GetUrlParameters: function () {
        var anchorHref = $("#searchProductsLink").attr('href');
        var vars = [], hash;
        var hashes = anchorHref.slice(anchorHref.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            var items = []
            items.push(hash[0]);
            items.push(hash[1]);
            vars.push(items);
        }

        return vars;
    },
    SetProtalCatalogIdInUrl: function (key, value) {
        if (value.trim().length > 0) {
            var params = Orders.GetUrlParameters();
            var newUrl = '';
            if (params.length > 0) {
                var baseHref = $("#searchProductsLink").attr('href').split('?')[0];
                newUrl = baseHref;
                for (var i = 0; i < params.length; i++) {
                    if (key.toLowerCase() != params[i][0].toLowerCase()) {
                        var prevValue = params[i][1];
                        if (i == 0) {
                            newUrl += "?" + params[i][0] + "=" + prevValue;
                        }
                        else {
                            newUrl += "&" + params[i][0] + "=" + prevValue;
                        }
                    }
                    else {
                        if (i == 0) {
                            newUrl += "?" + params[i][0] + "=" + value;
                        }
                        else {
                            newUrl += "&" + params[i][0] + "=" + value;
                        }
                    }

                }
            }
            $("#searchProductsLink").attr('href', newUrl);
        }
    },
    ApplyPromoCode: function (isCouponApplied) {
        var url = Orders.GetRoleBaseUrl("/Orders/ApplyCoupon");
        if (isCouponApplied || $("#promocode").val()) {
            App.Api.useCouponCode(url, $("#promocode").val(), function (response) {
                var result = JSON.parse(response);
                $("#divTotal").html("");
                $("#divTotal").html(result.html);

                var coupons = result.data;
                htmlString = "<div>";
                for (var couponIndex = 0; couponIndex < coupons.length; couponIndex++) {
                    var style = coupons[couponIndex].CouponApplied ? "green-color" : "field-validation-error";
                    var message = coupons[couponIndex].CouponMessage;
                    var couponCode = coupons[couponIndex].Coupon;
                    htmlString = htmlString + "<p class='" + style + "'>" + "<b><span class='coupon'>" + couponCode + "</span></b>" + " - " + message + "</p>";
                    if (coupons[couponIndex].CouponApplied) {
                        $("#promocode").val(coupons[couponIndex].Coupon);
                    }
                }
                htmlString = htmlString + "</div>";
                $("#couponContainer").show();
                $("#couponContainer").html("");
                $("#couponContainer").html(htmlString);
            });
        }
    },
    ApplyGiftCard: function () {
        var url = Orders.GetRoleBaseUrl("/Orders/ApplyGiftCard");
        App.Api.useGiftCard(url, $("#txtgiftcard").val(), function (response) {
            $("#divTotal").html(response);
            $("#valgiftcard").val("true");
            $("#cart-giftcard-status").hide().html("");
            if ($("#txtgiftcard").val().trim().length > 0) {
                var success = $("#hdnGiftCardApplied").val();
                var message = $("#hdnGiftCardMessage").val();
                if (success.toLowerCase() == "true") {
                    $("#cart-giftcard-status").show().html(message);
                } else {
                    $("#cart-giftcard-status").show().html(message);
                    $("#valgiftcard").val("false");
                }
            }
        });
    },
    ShowAddressOnHover: function (orderShipAddress, isMultipleShipping) {
        if (!isMultipleShipping) {
            var address = orderShipAddress;
            var htmlAddress = $.parseHTML(address);
            $("#orderLineItemsDv").find("#grid > tbody > tr").find("a:first").attr("class", "grid-popup-link");
            $("#orderLineItemsDv").find("#grid > tbody > tr").find("a:first").before('<div class="grid-popup">' + htmlAddress[0].data + '</div>');
            Orders.DisplayAddress();
        }
        else {
            var address = orderShipAddress;
            var htmlAddress = [];
            var headerIndex;
            for (var index = 0; index < address.length; index++) {
                htmlAddress[index] = $.parseHTML(address[index]);
            }
            $("#grid .grid-header").find("th").each(function () {
                var txt = $(this).text();
                if ($.trim(txt) === "Ship Address") {
                    headerIndex = $(this).index();
                }
            })
            $("#grid tbody tr").find('td:eq(' + headerIndex + ')').each(function (counterIndex) {
                var element = $(this).find("a");
                element.attr("class", "grid-popup-link");
                element.before('<div class="grid-popup" id=dvShippingAddress' + counterIndex + '>' + htmlAddress[counterIndex][0].data + '</div>');
            })
            Orders.DisplayAddress();
        }
    },
    DisplayAddress: function () {
        $("#orderLineItemsDv").find("#grid > tbody > tr").find("a:first").mouseover(function () { $(this).prev().css('display', 'block'); });
        $("#orderLineItemsDv").find("#grid > tbody > tr").find("a:first").mouseout(function () { $(this).prev().css('display', 'none'); });
        $("#orderLineItemsDv").find("#grid > tbody > tr").find("a:first").mouseover(function () {
            var pos = $(this).offset().top;
            var w = $(window);
            var relativePos = pos - w.scrollTop();
            $(this).prev().css('top', +relativePos + 'px');
        });

        $("#grid > tbody > tr").find(".z-edit").text("change status");
        $("#grid > tbody > tr").find(".z-edit").attr("title", "change status");
        $("#grid > tbody > tr").find(".z-edit").removeClass("z-edit");
    },
    ShowFranchiseOrder: function () {
        $("#customerLinkContainer").show();
        var protalId = $("#hdnPortalId").val();
        var catalogId = $("#hdnCatalogId").val();
        Orders.SetProtalCatalogIdInUrl("portalId", protalId);
        var addCustomerHref = "/FranchiseOrders/AddNewCustomer?portalId=" + protalId;
        var searchCustomerHref = "/FranchiseOrders/CustomerList";
        $("#addNewCustomerLink").attr('href', addCustomerHref);
        $("#searchCustomerLink").attr('href', searchCustomerHref);
    },
    CanclePayment: function () {
        $("#ajaxBusy").dialog('close');
        $("#customPaymentPopup").hide();
        $("#customPaymentPopup [data-payment='number']").val('');
        $("#customPaymentPopup [data-payment='cvc']").val('');
        $("#customPaymentPopup [data-payment='exp-month']").val('');
        $("#customPaymentPopup [data-payment='exp-year']").val('');
        $("#ddlPaymentTypes").val('');
        $("#ddlPaymentTypes option").each(function () {
            if ($(this).html() == "Select Payment") {
                $(this).attr("selected", "selected");
                return;
            }
        });
    },
    IsOrderTotalGreaterThanZero: function (total) {
        if (total > 0.00) {
            return true;
        } else {
            var paymentURL = String.format(GlobalVariables.ZnodeSiteAdminPaymentSuccessUrl, 0);
            paymentURL = paymentURL + "&token=null&shippingId=0&additionalNotes=" + $("#txtAdditionalInstructions").val();
            window.location = paymentURL;
        }
    },
    ShowPaymentHtml: function (control) {
        var selectionId = "#" + control.id + " option:selected";
        var paymentType = $("#ddlPaymentTypes option:selected").text();
        $("#divPONumber").hide();
        $("#div-paypal-express-checkout").hide();
        if (paymentType.toLowerCase() != "credit card") {
            $("#divOrderSavePage").show();
            $("#frmOrderSave").show();
            $("#customPaymentPopup").hide();
            if (paymentType.toLowerCase() == "purchase order") {
                $("#divPONumber").show();
            }
            else if (paymentType.toLowerCase() == "paypal express") {
                $("#div-paypal-express-checkout").show();
                $("#frmOrderSave").hide();
            }
        }
        else {
            $("#divOrderSavePage").hide();
            $("#frmOrderSave").hide();

            $("#PaymentSettingId").val($(selectionId).val());
            $("#hdnGatwayName").val('');
            Orders.GetPaymentGatwayNameByPaymetOption($(selectionId).val(), function (res) {
                if (res) {
                    if ($("#hdnGatwayName").val() != undefined && $("#hdnGatwayName").val().length > 0) {
                        var gatewayName = $("#hdnGatwayName").val();
                        var paymentAppGatewayName = $("#hdnGatwayName").val().toLowerCase();

                        if (gatewayName.toLowerCase() == 'authorize.net') {
                            paymentAppGatewayName = 'authorizenet';
                        }

                        if (gatewayName.toLowerCase() == 'paymentech orbital') {
                            paymentAppGatewayName = 'paymenttech';
                        }

                        var profileId = null;
                        if ($("#paymentProfileId").val().length > 0) {
                            profileId = $("#paymentProfileId").val();
                        }

                        var paymentUrl = GlobalVariables.PaymentApplicationJSCall;
                        var paymenttwocoUrl = GlobalVariables.ZnodeDemoPaymentTwoCoUrl;
                        if (userType.toLowerCase() == "franchiseadmin") {
                            paymenttwocoUrl = "/franchiseorders/twoco?paymentOptionId={0}";
                        }
                        var twoCOUrl = String.format(paymenttwocoUrl, $('#PaymentSettingId').val());
                        paymentUrl = String.format(paymentUrl, paymentAppGatewayName, profileId, twoCOUrl);
                        $.ajax({
                            url: paymentUrl,
                            dataType: "script",
                            success: function (response) {
                                var cancelURL = "";
                                var successUrl = "";

                                if (location.href.toLowerCase().indexOf("franchiseadmin") >= 0) {
                                    cancelURL = GlobalVariables.ZnodeFranchiseAdminPaymentCancelUrl;
                                    successUrl = String.format(GlobalVariables.ZnodeFranchiseAdminPaymentSuccessUrl, $("#ddlPaymentTypes option:selected").val());
                                } else {
                                    cancelURL = GlobalVariables.ZnodeSiteAdminPaymentCancelUrl;
                                    successUrl = String.format(GlobalVariables.ZnodeSiteAdminPaymentSuccessUrl, $("#ddlPaymentTypes option:selected").val());
                                }

                                $("#ReturnUrl").val(successUrl);
                                $("#CancelUrl").val(cancelURL);
                                if ($(selectionId).text().toLowerCase() == "credit card") {
                                    Orders.SetCreditCardValidations();
                                    $("#customPaymentPopup").show();
                                    $("#divOrderSavePage").hide();
                                    $("#frmOrderSave").hide();
                                } else {
                                    Orders.HideShowHtmlElements('Unable to contact payment application.');
                                }

                                return false;
                            },
                            error: function () {
                                Orders.HideShowHtmlElements('Unable to contact payment application.');
                                return false;
                            }
                        });
                    } else {
                        Orders.HideShowHtmlElements('Could not procced with payment as no payment gateway available with this selection.');
                        return false;
                    }
                } else {
                    Orders.HideShowHtmlElements('Could not procced with payment as no payment gateway available with this selection.');
                    return false;
                }

                return false;
            });
            return false;
        }
    },
    SetCreditCardValidations: function () {

        $('input[data-payment="exp-month"]').on("keypress", function (e) {
            if (e.which != 8 && e.which != 0 && (e.wchich < 48 || e.which > 57)) {
                return false;
            }
        });

        $('input[data-payment="exp-month"]').on("focusout", function (e) {
            var monthVal = $('input[data-payment="exp-month"]').val();
            if (monthVal.length == 1 && (monthVal >= 1 || monthVal <= 9)) {
                monthVal = 0 + monthVal;
                $('input[data-payment="exp-month"]').val(monthVal);
            }
        });

        $('input[data-payment="exp-year"]').on("keypress", function (e) {
            if (e.which != 8 && e.which != 0 && (e.wchich < 48 || e.which > 57)) {
                return false;
            }
        });

        $('input[data-payment="cvc"]').on("keypress", function (e) {
            if (e.which != 8 && e.which != 0 && (e.wchich < 48 || e.which > 57)) {
                return false;
            }
        });
    },
    SubmitPayment: function (e) {
        var Total = $("#hdnTotalAmt").val();
        if (this.IsOrderTotalGreaterThanZero(Total)) {
            var isValid = true;
            $('input[data-payment="number"],input[data-payment="exp-month"],input[data-payment="exp-year"],input[data-payment="cvc"]').each(function () {
                if ($.trim($(this).val()) == '') {
                    isValid = false;
                    $(this).css({
                        "border": "1px solid red",
                        "background": "#FFCECE"
                    });
                } else {
                    $(this).css({
                        "border": "1px solid black",
                        "background": ""
                    });
                }
            });

            if (!this.Mod10($('input[data-payment="number"]').val())) {
                isValid = false;
                $('#errornumber').show();
                $('input[data-payment="number"]').css({
                    "border": "1px solid red",
                    "background": "#FFCECE"
                });
            }

            if ($('input[data-payment="exp-month"]').val() > 12 || $('input[data-payment="exp-month"]').val() < 1) {
                isValid = false;
                $('#errormonth').show();
                $('input[data-payment="exp-month"]').css({
                    "border": "1px solid red",
                    "background": "#FFCECE"
                });
            }

            var currentYear = (new Date).getFullYear();
            var currentMonth = (new Date).getMonth() + 1;

            if ($('input[data-payment="exp-year"]').val() < currentYear) {
                isValid = false;
                $('#erroryear').show();
                $('input[data-payment="exp-year"]').css({
                    "border": "1px solid red",
                    "background": "#FFCECE"
                });
            }

            if ($('input[data-payment="exp-year"]').val() == currentYear && $('input[data-payment="exp-month"]').val() < currentMonth) {
                isValid = false;
                $('#errormonth').show();
                $('input[data-payment="exp-month"]').css({
                    "border": "1px solid red",
                    "background": "#FFCECE"
                });
                $('#erroryear').show();
                $('input[data-payment="exp-year"]').css({
                    "border": "1px solid red",
                    "background": "#FFCECE"
                });
            }

            if (isValid == false) {
                return false;
            }

            if (isValid) {
                $('body').append('<div id="ajaxBusy"><p id="ajaxBusyMsg">Your payment is processing. Please wait and do not close this window</p></div>');
                $("#ajaxBusy").dialog({
                    title: 'Payment Application',
                    resizable: false,
                    modal: true,
                    closeOnEscape: false,
                    open: function (event, ui) { $("#ajaxBusy").parent().find(" .ui-dialog-titlebar-close").hide(); }
                });

                var cardNumber = $("#customPaymentPopup [data-payment='number']").val();
                var cardCvcNum = $("#customPaymentPopup [data-payment='cvc']").val();
                var cardExpMonth = $("#customPaymentPopup [data-payment='exp-month']").val();
                var cardExpYear = $("#customPaymentPopup [data-payment='exp-year']").val();

                var guid = $('#GUID').val();

                var discount = $('#dynamic-discount-amount').text().substring(1);
                var ShippingCost = $('#ShippingCost').text();
                var TaxCost = "0.00";
                if ($('#dynamic-tax-cost') != undefined) {
                    TaxCost = $('#dynamic-tax-cost').text().substring(1);
                }

                var SubTotal = $('#dynamic-subtotal-amount').text().substring(1);

                var cardType = this.detectCardType(cardNumber);
                var OrderId = 1;

                var BillingFirstName = "";
                var BillingLastName = "";
                var BillingName = $('#divShippingAddress #addrFullName').text();
                var BillingStreetAddress1 = $('#divShippingAddress #addrStreet1').text();
                var BillingStreetAddress2 = $('#divShippingAddress #addrStreet2').text();
                var BillingCity = $('#divShippingAddress #addrCity').text();
                var BillingStateCode = $('#divShippingAddress #addrStateCode').text();
                var BillingCountryCode = $('#divShippingAddress #addrCountryCode').text();
                var BillingPhoneNumber = $('#divShippingAddress #addrPhoneNumber').text();
                var BillingPostalCode = $('#divShippingAddress #addrPostalCode').text();
                var BillingEmailId = $('#divShippingAddress #addrEmailAddress').text();

                if (BillingName.length > 0) {
                    var splittedBillingName = BillingName.split(' ');
                    if (splittedBillingName.length == 1) {
                        BillingFirstName = splittedBillingName[0];
                    } else if (splittedBillingName.length == 2) {
                        BillingFirstName = splittedBillingName[0];
                        BillingLastName = splittedBillingName[1]
                    }
                }

                var paymentSettingId = $('#PaymentSettingId').val();
                var GatewayLoginName = $('#GatewayLoginName').val();
                var GatewayLoginPassword = $('#GatewayLoginPassword').val();
                var GatewayTestMode = $('#GatewayTestMode').val();
                var GatewayCustom1 = $('#GatewayCustom1').val();
                var GatewayPreAuthorize = $('#GatewayPreAuthorize').val();
                var GatewayTransactionKey = $('#GatewayTransactionKey').val();
                var GatewayCurrencyCode = $('#GatewayCurrencyCode').val();
                var Gateway = $('#GatewayType').val();

                var ReturnUrl = $('#ReturnUrl').val();
                var CancelUrl = $('#CancelUrl').val();

                if ($("#ddlShippingTypes") != undefined) {
                    ReturnUrl = ReturnUrl + "&shippingId=" + $("#ddlShippingTypes option:selected").val();
                }

                var CustomerPaymentProfileId = $('#CustomerPaymentProfileId').val();
                var CustomerProfileId = $('#CustomerProfileId').val();
                var CardDataToken = $('#CardDataToken').val();

                var Subscriptions = new Array();

                if ($('#RecurringBillingFrequency').val() != undefined) {
                    var loopCount = $('#RecurringBillingFrequency').length;
                    for (var iCount = 0; iCount < loopCount; iCount++) {

                        if ($($('#RecurringBillingInd')[iCount]).val() != "False") {

                            var period = $($('#RecurringBillingFrequency')[iCount]).val();
                            var billingInd = $($('#RecurringBillingInd')[iCount]).val();
                            var billingIndinInt = 0;

                            if (billingInd == 'True') {
                                billingIndinInt = 1;
                            }

                            var item = {
                                Amount: $($('#RecurringBillingInitialAmount')[iCount]).val(),
                                Frequency: period,
                                GUID: "",
                                InitialAmount: $($('#RecurringBillingInitialAmount')[iCount]).val(),
                                InstallmentInd: billingIndinInt,
                                InvoiceNo: 1,
                                Period: "DAY",
                                TotalCycles: 1,
                                ProfileName: $($('#RecurringProfileName')[iCount]).val()
                            }

                            Subscriptions.push(item);
                        }
                    }
                }

                var gatewayName = $("#hdnGatwayName").val();
                var paymentAppGatewayName = $("#hdnGatwayName").val().toLowerCase();

                if (gatewayName.toLowerCase() == 'authorize.net') {
                    paymentAppGatewayName = 'authorizenet';
                }
                else if (gatewayName.toLowerCase() == 'paymentech orbital') {
                    paymentAppGatewayName = 'paymenttech';
                }
                else if (gatewayName.toLowerCase() == 'worldpay') {
                    paymentAppGatewayName = 'securenet';
                }

                var payment = {
                    "GUID": guid,
                    "GatewayLoginName": GatewayLoginName,
                    "GatewayLoginPassword": GatewayLoginPassword,
                    "GatewayTestMode": GatewayTestMode,
                    "GatewayCustom1": GatewayCustom1,
                    "GatewayPreAuthorize": GatewayPreAuthorize,
                    "GatewayTransactionKey": GatewayTransactionKey,
                    "GatewayCurrencyCode": GatewayCurrencyCode,
                    "GatewayType": paymentAppGatewayName,
                    "OrderId": OrderId,
                    "BillingCity": BillingCity,
                    "BillingCountryCode": BillingCountryCode,
                    "BillingFirstName": BillingFirstName,
                    "BillingLastName": BillingLastName,
                    "BillingName": BillingName,
                    "BillingPhoneNumber": BillingPhoneNumber,
                    "BillingPostalCode": BillingPostalCode,
                    "BillingStateCode": BillingStateCode,
                    "BillingStreetAddress1": BillingStreetAddress1,
                    "BillingStreetAddress2": BillingStreetAddress2,
                    "BillingEmailId": BillingEmailId,
                    "ShippingCost": ShippingCost,
                    "SubTotal": SubTotal,
                    "TaxCost": TaxCost,
                    "Total": Total,
                    "Discount": discount,
                    "CardSecurityCode": cardCvcNum,
                    "CardNumber": cardNumber,
                    "CardExpirationMonth": cardExpMonth,
                    "CardExpirationYear": cardExpYear,
                    "CancelUrl": CancelUrl,
                    "ReturnUrl": ReturnUrl,

                    "CustomerPaymentProfileId": CustomerPaymentProfileId,
                    "CustomerProfileId": CustomerProfileId,
                    "CardDataToken": CardDataToken,
                    "CardType": cardType,
                    "PaymentSettingId": paymentSettingId,
                    "Subscriptions": Subscriptions
                };

                $("#customPaymentPopup").hide();

                submitCard(payment, function (response) {
                    if (response.Data == undefined) {
                        if (response.indexOf("Unauthorized") > 0) {
                            Orders.HideShowHtmlElements('We were unable to process your credit card payment. <br /><br />Reason:<br />' + response + '<br /><br />If the problem persists, contact us to complete your order.');
                        }
                    } else {
                        var isSuccess = response.Data.IsSuccess;
                        if (isSuccess) {
                            var customerProfileId = response.Data.CustomerProfileId;
                            var customerPaymentId = response.Data.CustomerPaymentProfileId;
                            var paymentSettingId = $('#PaymentSettingId').val();
                            var submitUrl = "/orders/submitpayment";
                            if (userType.toLowerCase() == "franchiseadmin") {
                                submitUrl = "/franchiseorders/submitpayment";
                            }
                            App.Api.processCreditCardPayment(submitUrl, paymentSettingId, customerProfileId, customerPaymentId, function (response) {

                                if (response.Data.indexOf("http") == -1) {
                                    $("#ajaxBusy").dialog('close');
                                    CommonHelper.DisplayNotificationMessagesHelper(response.Data, "error", "true", 5000);
                                    return false;
                                }
                                else {
                                    var newLocation = response.Data;
                                    if (newLocation != undefined && newLocation.toLowerCase().indexOf("missing card data") >= 0) {
                                        Orders.HideShowHtmlElements('Error occurred during processing an order. Order could not placed as card data is missing.');
                                    } else if (newLocation != undefined && newLocation.indexOf("Message=") >= 0) {
                                        Orders.HideShowHtmlElements(newLocation.substr(newLocation.indexOf("=") + 1));
                                        $("#frmOrderSave").show();
                                    } else if (newLocation != undefined) {
                                        newLocation = newLocation + "&additionalNotes=" + $("#txtAdditionalInstructions").val();
                                        window.location = newLocation;
                                    } else {
                                        Orders.HideShowHtmlElements('Error occurred during processing an order. Order could not placed.');
                                    }
                                }
                            });
                        }
                        else {
                            var newLocation = response.Data.ResponseText;
                            if (newLocation == undefined) {
                                newLocation = response.Data.GatewayResponseData;
                                if (newLocation == undefined) {
                                    newLocation = response.Data;
                                }
                            }

                            if (newLocation != undefined && newLocation.toLowerCase().indexOf("missing card data") >= 0) {
                                Orders.HideShowHtmlElements('Error occurred during processing an order. Order could not placed as card data is missing.');
                            } else if (newLocation != undefined && newLocation.indexOf("Message=") >= 0) {
                                Orders.HideShowHtmlElements(newLocation.substr(newLocation.indexOf("=") + 1));
                                $("#frmOrderSave").show();
                            } else if (newLocation.indexOf('customer') > 0) {
                                Orders.HideShowHtmlElements(newLocation);
                            } else {
                                Orders.HideShowHtmlElements('Error occurred during processing an order. Order could not placed.');
                            }
                        }
                    }
                });
            }
        }
    },
    Mod10: function (ccNum) {
        var valid = "0123456789";  // Valid digits in a credit card number
        var len = ccNum.length;  // The length of the submitted cc number
        var iCCN = parseInt(ccNum);  // integer of ccNum
        var sCCN = ccNum.toString();  // string of ccNum

        sCCN = sCCN.replace(/^\s+|\s+$/g, '');  // strip spaces

        var iTotal = 0;  // integer total set at zero
        var bNum = true;  // by default assume it is a number
        var bResult = false;  // by default assume it is NOT a valid cc
        var temp;  // temp variable for parsing string
        var calc;  // used for calculation of each digit

        // Determine if the ccNum is in fact all numbers
        for (var j = 0; j < len; j++) {
            temp = "" + sCCN.substring(j, j + 1);
            if (valid.indexOf(temp) == "-1") {
                bNum = false;
            }
        }

        // if it is NOT a number, you can either alert to the fact, or just pass a failure
        if (!bNum) {
            bResult = false;
        }

        // Determine if it is the proper length
        if ((len == 0) && (bResult)) {  // nothing, field is blank AND passed above # check
            bResult = false;
        } else {  // ccNum is a number and the proper length - let's see if it is a valid card number
            if (len >= 15) {  // 15 or 16 for Amex or V/MC
                for (var i = len; i > 0; i--) {  // LOOP throught the digits of the card
                    calc = parseInt(iCCN) % 10;  // right most digit
                    calc = parseInt(calc);  // assure it is an integer
                    iTotal += calc;  // running total of the card number as we loop - Do Nothing to first digit
                    i--;  // decrement the count - move to the next digit in the card
                    iCCN = iCCN / 10;                               // subtracts right most digit from ccNum
                    calc = parseInt(iCCN) % 10;    // NEXT right most digit
                    calc = calc * 2;                                 // multiply the digit by two
                    // Instead of some screwy method of converting 16 to a string and then parsing 1 and 6 and then adding them to make 7,
                    // I use a simple switch statement to change the value of calc2 to 7 if 16 is the multiple.
                    switch (calc) {
                        case 10: calc = 1; break;       //5*2=10 & 1+0 = 1
                        case 12: calc = 3; break;       //6*2=12 & 1+2 = 3
                        case 14: calc = 5; break;       //7*2=14 & 1+4 = 5
                        case 16: calc = 7; break;       //8*2=16 & 1+6 = 7
                        case 18: calc = 9; break;       //9*2=18 & 1+8 = 9
                        default: calc = calc;           //4*2= 8 &   8 = 8  -same for all lower numbers
                    }
                    iCCN = iCCN / 10;  // subtracts right most digit from ccNum
                    iTotal += calc;  // running total of the card number as we loop
                }  // END OF LOOP
                if ((iTotal % 10) == 0) {  // check to see if the sum Mod 10 is zero
                    bResult = true;  // This IS (or could be) a valid credit card number.
                } else {
                    bResult = false;  // This could NOT be a valid credit card number
                }
            }
        }
        return bResult; // Return the results
    },
    detectCardType: function (number) {
        var re = {
            electron: /^(4026|417500|4405|4508|4844|4913|4917)\d+$/,
            maestro: /^(5018|5020|5038|5612|5893|6304|6759|6761|6762|6763|0604|6390)\d+$/,
            dankort: /^(5019)\d+$/,
            interpayment: /^(636)\d+$/,
            unionpay: /^(62|88)\d+$/,
            visa: /^4[0-9]{12}(?:[0-9]{3})?$/,
            mastercard: /^5[1-5][0-9]{14}$/,
            amex: /^3[47][0-9]{13}$/,
            diners: /^3(?:0[0-5]|[68][0-9])[0-9]{11}$/,
            discover: /^6(?:011|5[0-9]{2})[0-9]{12}$/,
            jcb: /^(?:2131|1800|35\d{3})\d{11}$/
        };
        if (re.electron.test(number)) {
            return 'ELECTRON';
        } else if (re.maestro.test(number)) {
            return 'MAESTRO';
        } else if (re.dankort.test(number)) {
            return 'DANKORT';
        } else if (re.interpayment.test(number)) {
            return 'INTERPAYMENT';
        } else if (re.unionpay.test(number)) {
            return 'UNIONPAY';
        } else if (re.visa.test(number)) {
            return 'VISA';
        } else if (re.mastercard.test(number)) {
            return 'MASTERCARD';
        } else if (re.amex.test(number)) {
            return 'AMEX';
        } else if (re.diners.test(number)) {
            return 'DINERS';
        } else if (re.discover.test(number)) {
            return 'DISCOVER';
        } else if (re.jcb.test(number)) {
            return 'JCB';
        } else {
            return undefined;
        }
    },
    ValidateVoidOrRefund: function () {
        $("#subVoid").click(function () {
            $("#valRefundAmount").removeClass("field-validation-error").removeClass("field-validation-valid");
            $("#submitRefund").submit();
        })
        $("#subRefund").click(function (e) {
            var refundAmount = $("#txtRefundAmount").val();
            if (refundAmount === undefined || refundAmount == null || refundAmount == "") {
                $("#valRefundAmount").text("Enter Amount To Refund").addClass("field-validation-error").removeClass("field-validation-valid");
                return false;
            }
            $("#submitRefund").submit();
        })
    },
    ValidateFranchiseVoidOrRefund: function () {
        $("#franchiseSubVoid").click(function () {
            $("#valRefundAmount").removeClass("field-validation-error").removeClass("field-validation-valid");
            $("#FranchiseSubmitRefund").submit();
        })
        $("#franchiseSubRefund").click(function (e) {
            var refundAmount = $("#txtRefundAmount").val();
            if (refundAmount === undefined || refundAmount == null || refundAmount == "") {
                $("#valRefundAmount").text("Enter Amount To Refund").addClass("field-validation-error").removeClass("field-validation-valid");
                return false;
            }
            $("#FranchiseSubmitRefund").submit();
        })
    },
    // When attribute dropdown is changed, update region with new HTML
    productAttributeHandler: function () {
        $("#dynamic-product-variations").on("change", "select.attribute", function (ev) {
            Orders.lastSelectedAttributeId = $(this).val();
            var quantity = 1;
            Orders.updateProductVariations("#dynamic-product-variations", quantity, function (response) {
                Orders.updateProductValues(response, quantity);
                // Update display
                Orders.refreshPrice(response.data.unitPrice);
                Orders.inventoryStatus(response);
                Orders.calculateTotalAmount(response.data.price);
                Orders.refreshMainProductImage(response.data);
            });
        });

    },
    // When add-on input control is changed, update region with new HTML
    productAddOnsHandler: function () {
        $("#dynamic-product-addons").on("change", ".AddOn", function (ev) {
            var quantity = $("#ProductDetailsQuantity").val();
            Orders.updateProductVariations(false, quantity, function (response) {
                Orders.updateProductValues(response, quantity);
                Orders.refreshPrice(response.data.unitPrice);
                Orders.inventoryStatus(response);
                $("#Total").val(response.data.price);
                $("#Total").text(response.data.price);
                $("#Total").html(response.data.price);
            });
        });
    },
    // When quantity is changed, update region with new HTML
    productQuantityHandler: function () {
        $("#ProductDetailsQuantity").on("change", function (ev) {
            Orders.updateProductVariations(false, $(this).val(), function (response) {
                Orders.refreshPrice(response.data.unitPrice);
                $("#Total").html(response.data.price);
                Orders.inventoryStatus(response);
            });
        });

    },
    updateProductVariations: function (htmlContainer, quantity, callback) {
        var selectedIds = Orders.getAttributeIds();
        var selectedAddOnIds = Orders.getAddOnIds();

        if (Orders.lastSelectedAttributeId <= 0) {
            Orders.lastSelectedAttributeId = selectedIds;
        }

        var params = "attributeId=" + Orders.lastSelectedAttributeId + "&selectedIds=" + selectedIds + "&quantity=" + quantity + "&selectedAddOnIds=" + selectedAddOnIds;
        var url = Orders.GetRoleBaseUrl("/Orders/GetAttributes/") + Orders.setProductId();
        App.Api.getProductAttributes(url, params, function (response) {
            if (response.data.addOnId != undefined && response.data.addOnMessage != undefined && response.data.isOutOfStock != undefined) {
                for (var index = 0; index < response.data.addOnId.length; index++) {
                    if (response.data.isOutOfStock[index]) {
                        $("#dynamic-addOninventory_" + response.data.addOnId[index]).show();
                        $("#dynamic-addOninventory_" + response.data.addOnId[index]).html(response.data.addOnMessage[index]);
                    }
                    else {
                        $("#dynamic-addOninventory_" + response.data.addOnId[index]).hide();
                        $("#dynamic-addOninventory_" + response.data.addOnId[index]).html("");
                    }
                }
            }
            if (htmlContainer) {
                $(htmlContainer).html(response.data.html);
            }
            if (callback) {
                callback(response);
            }
        });
    },
    // Updates total price element
    refreshPrice: function (amount) {
        $("#RetailPrice").html(amount);
    },
    inventoryStatus: function (response) {
        if (response.success) {
            // In stock
            $("#dynamic-inventory").hide();
            $("#button-addtocart").removeAttr("disabled");
        } else {
            // Out of stock
            $("#button-addtocart").attr("disabled", "disabled");
            $("#dynamic-inventory").show().html(response.message);
        }
        var isShowAddToCart = $("#isShowAddToCart").val();
        if (isShowAddToCart.toLowerCase() == "false") {
            $("#button-addtocart").attr("disabled", "disabled");
            $("#dynamic-inventory").show().html("This item is out of stock");
        }
    },
    updateProductValues: function (response, quantity) {
        var selectedAddOnIds = Orders.getAddOnIds();
        // Set form values for submit       
        $("#dynamic-sku").val(response.data.sku);
        $("#Quantity").val(quantity);
        $("#dynamic-addonvalueid").val(selectedAddOnIds);
        if (response.data.addOnId != undefined && response.data.addOnMessage != undefined && response.data.isOutOfStock != undefined) {
            for (var index = 0; index < response.data.addOnId.length; index++) {
                if (response.data.isOutOfStock[index]) {
                    $("#dynamic-addOninventory_" + response.data.addOnId[index]).show();
                    $("#dynamic-addOninventory_" + response.data.addOnId[index]).html(response.data.addOnMessage[index]);
                }
                else {
                    $("#dynamic-addOninventory_" + response.data.addOnId[index]).hide();
                    $("#dynamic-addOninventory_" + response.data.addOnId[index]).html("");
                }
            }
        }
    },
    getAttributeIds: function (parentSelector) {
        var selectedIds = [];
        if (typeof parentSelector == "undefined") { parentSelector = ""; }

        $(parentSelector + " select.attribute").each(function () {
            selectedIds.push($(this).val());
        });

        return (selectedIds.join());
    },
    getAddOnIds: function (parentSelector) {
        var selectedIds = [];

        if (typeof parentSelector == "undefined") { parentSelector = ""; }

        $(parentSelector + " select.AddOn").each(function () {
            selectedIds.push($(this).val());
        });

        $(parentSelector + " input.AddOn:checked").each(function () {
            selectedIds.push($(this).val());
            /*
            if ($("input[id=" + input.attr("id") + "]:checked + label").css("background-position")) {
                selectedIds.push(input.val());
            }
            */
        });

        return (selectedIds.join());
    },
    setProductId: function () {
        Orders.productId = $(".product-options #dynamic-productId").val();
        return (Orders.productId);
    },
    calculateTotalAmount: function (price) {
        var quantity = $("#ProductDetailsQuantity").val();
        var curSymbol = price.substring(0, 1);
        var unitPrice = price.substring(1);
        var totalPrice = parseFloat(unitPrice) * parseInt(quantity);
        totalPrice = totalPrice.toFixed(2);
        $("#Total").val(totalPrice);
        $("#Total").text(curSymbol + totalPrice);
        $("#Total").html(curSymbol + totalPrice);
    },
    HideShowHtmlElements: function (message) {
        this.CanclePayment();
        CommonHelper.DisplayNotificationMessagesHelper(message, "error", false, 5000);
    },
    refreshMainProductImage: function (data) {
        if (data.imagePath != null && data.imagePath != "") {
            $("#product-image").attr('src', data.imageMediumPath);
            $("#product-image").attr('data-src', data.imageMediumPath);
            //$("#product-lens-image").attr('data-lens-image', data.imagePath);
        }
    },
    ManageOrderLineItemsGrid: function () {
        var index;
        $("#grid .grid-header").find("th").each(function () {
            var txt = $(this).text();
            if ($.trim(txt) === "Description") {
                index = $(this).index();
            }
        })
        $("#grid tbody tr").find('td:eq(' + index + ')').each(function () {
            var txt = $(this).text();
            $(this).html(txt);
        })
    },
    SetFinalOrderTotal: function () {
        var orderTotal = $("#dynamic-order-total").text();
        $("#orderTotal").text(orderTotal);
    },
    GetRoleBaseUrl: function (url) {
        try {
            if (userType.toLowerCase() == "franchiseadmin") {
                url = url.replace("Orders", "FranchiseOrders");
                return url;
            }
        }
        catch (err) { }
        return url;
    },
    ShowSendEmailButton: function () {
        if ($("#ddlOrderLineItemStatus").val() == orderShippedStatusId) {
            $("#btnSendEmail").css("display", "block");
        }
        else {
            $("#btnSendEmail").css("display", "none");
        }
    },
    SendEmail: function () {
        if ($("#ddlOrderLineItemStatus").val() == orderShippedStatusId) {
            $("#btnSendEmail").css("display", "block");
        }
        else {
            $("#btnSendEmail").css("display", "none");
        }
        $("#btnSendEmail").click(function (e) {
            e.preventDefault();
            $("#formMallOrderStatus").attr("action", "/MallAdmin/MallOrders/SendEmail");
            $("#formMallOrderStatus").serialize();
            $("#formMallOrderStatus").submit();
        })
    },
    GetPaymentGatwayNameByPaymetOption: function (paymetId, return_fun) {
        var url = Orders.GetRoleBaseUrl("/Orders/GetPaymentGatwayNameByPaymetOptionId");
        App.Api.GetPaymentGatwayName(url, paymetId, function (response) {
            if (response.success) {
                $("#hdnGatwayName").val(response.name);
                $("#paymentProfileId").val(response.paymentProfileId);
            }
            return_fun(true);
        });
    },
    SubmitOrder: function () {
        var isValidPayment = $("#ddlPaymentTypes").val();
        var isValidGiftCard = $("#valgiftcard").val();
        var isValidCoupon = $("#valpromocode").val();
        var isValidShipping = $("#ddlShippingTypes").val();

        $("#valPaymentTypes").removeClass("field-validation-error").removeClass("field-validation-valid").hide();
        if (isValidPayment == "0" || isValidPayment.trim().length < 1) {
            $("#valPaymentTypes").text(paymenterror).addClass("field-validation-error").removeClass("field-validation-valid").show();
            return false;
        }

        $("#valShippingTypes").removeClass("field-validation-error").removeClass("field-validation-valid").hide();
        if (isValidShipping == undefined || isValidShipping == "0" || isValidShipping.trim().length < 1) {
            $("#valShippingTypes").text(shippingerror).addClass("field-validation-error").removeClass("field-validation-valid").show();
            return false;
        }
        if ($("#txtgiftcard").val().trim().length > 0 && isValidGiftCard.toLowerCase() == "false") {
            return false;
        }

        $("#cart-ponumber-status").hide();
        if ($("#divPONumber").is(":visible")) {
            if ($("#txtPONumber").val().trim().length <= 0) {
                $("#cart-ponumber-status").show();
                return false;
            }
        }
        overlayforwaitblock();

        $("#paymentOptionId").val($("#ddlPaymentTypes").attr("selected", "selected").val());
        $("#PaymentSettingId").val($("#ddlPaymentTypes").attr("selected", "selected").val());
        $("#shippingTypeId").val($("#ddlShippingTypes").attr("selected", "selected").val());
        $("#additionalInstructions").val($("#txtAdditionalInstructions").val());
        $("#purchaseOrderNumber").val($("#txtPONumber").val());
        $("#frmOrderSave").submit();
    },
    ValidateAddress: function () {
        if (!Orders.IsAddressExist('divShippingAddress')) {
            CommonHelper.DisplayNotificationMessagesHelper("Please enter shipping address", "error", false, 5000);
            return false;
        }
        else if (!Orders.IsAddressExist('divBillingAddress')) {
            CommonHelper.DisplayNotificationMessagesHelper("Please enter billing address", "error", false, 5000);
            return false;
        }
        return true;
    },
    IsAddressExist: function (addressType) {
        if ($('#' + addressType + ' #addrStreet1').html() != undefined && $('#' + addressType + ' #addrStreet1').html().trim().length < 1) {
            return false;
        }
        else if ($('#' + addressType + ' #addrCity').html() != undefined && $('#' + addressType + ' #addrCity').html().trim().length < 1) {
            return false;
        }
        else if ($('#' + addressType + ' #addrStateCode').html() != undefined && $('#' + addressType + ' #addrStateCode').html().trim().length < 1) {
            return false;
        }
        else if ($('#' + addressType + ' #addrPostalCode').html() != undefined && $('#' + addressType + ' #addrPostalCode').html().trim().length < 1) {
            return false;
        }
        else if ($('#' + addressType + ' #addrCountryCode').html() != undefined && $('#' + addressType + ' #addrCountryCode').html().trim().length < 1) {
            return false;
        }
        else {
            return true;
        }
    },
    PaypalExpressCheckout: function () {
        var isValid = true;
        var isValidPayment = $("#ddlPaymentTypes").val();
        var isValidGiftCard = $("#valgiftcard").val();
        var isValidCoupon = $("#valpromocode").val();
        var isValidShipping = $("#ddlShippingTypes").val();

        $("#valPaymentTypes").removeClass("field-validation-error").removeClass("field-validation-valid").hide();
        if (isValidPayment == "0" || isValidPayment.trim().length < 1) {
            $("#valPaymentTypes").text(paymenterror).addClass("field-validation-error").removeClass("field-validation-valid").show();
            return false;
        }

        $("#valShippingTypes").removeClass("field-validation-error").removeClass("field-validation-valid").hide();
        if (isValidShipping == undefined || isValidShipping == "0" || isValidShipping.trim().length < 1) {
            $("#valShippingTypes").text(shippingerror).addClass("field-validation-error").removeClass("field-validation-valid").show();
            return false;
        }
        if ($("#txtgiftcard").val().trim().length > 0 && isValidGiftCard.toLowerCase() == "false") {
            return false;
        }

        if ($('#divShippingAddress').is(':visible') == false || $('#divBillingAddress').is(':visible') == false || $('#layout-cart').is(':visible') == false) {
            return false;
        }
        return Orders.PayPalPaymentProcess();
    },
    PayPalPaymentProcess: function () {
        var Total = $('#hdnTotalAmt').val();
        if (this.IsOrderTotalGreaterThanZero(Total)) {
            var isValid = true;
            if (isValid) {
                $('body').append('<div id="ajaxBusy"><p id="ajaxBusyMsg">Your payment is processing. Please wait and do not close this window</p></div>');
                $("#ajaxBusy").dialog({
                    title: 'Payment Application',
                    resizable: false,
                    modal: true,
                    closeOnEscape: false,
                    open: function (event, ui) { $("#ajaxBusy").parent().find(" .ui-dialog-titlebar-close").hide(); }
                });
                var paymentOptionId = $('#ddlPaymentTypes').val();
                var CancelUrl = "";
                var ReturnUrl = "";

                if (location.href.toLowerCase().indexOf("franchiseadmin") >= 0) {
                    CancelUrl = GlobalVariables.ZnodeFranchiseAdminPaymentCancelUrl;
                    ReturnUrl = String.format(GlobalVariables.ZnodeFranchiseAdminPaymentSuccessUrl, $("#ddlPaymentTypes option:selected").val());
                } else {
                    CancelUrl = GlobalVariables.ZnodeSiteAdminPaymentCancelUrl;
                    ReturnUrl = String.format(GlobalVariables.ZnodeSiteAdminPaymentSuccessUrl, $("#ddlPaymentTypes option:selected").val());
                }

                if ($("#ddlShippingTypes") != undefined) {
                    ReturnUrl = ReturnUrl + "&shippingId=" + $("#ddlShippingTypes option:selected").val() + "&additionalNotes=" + $("#txtAdditionalInstructions").val();
                }
                else {
                    ReturnUrl = ReturnUrl + "&shippingId=0&additionalNotes=" + $("#txtAdditionalInstructions").val();
                }
            }
            var url = "/Orders/ProcessPayPalPayment"
            if (userType.toLowerCase() == "franchiseadmin") {
                url = "/franchiseorders/ProcessPayPalPayment";
            }
            App.Api.ProcessPayPalPayment(url, paymentOptionId, ReturnUrl, CancelUrl, function (response) {
                var message = response;
                if (message != undefined && message.indexOf('Message=') >= 0) {
                    var errorMessage = message.substr(message.indexOf('=') + 1);
                    Orders.HideShowHtmlElements(errorMessage);
                    $("#ddlPaymentTypes").val('');
                } else if (message.indexOf("http") != -1) {
                    window.location = response;
                }
                else {
                    Orders.HideShowHtmlElements(message);
                    $("#ddlPaymentTypes").val(paymentOptionId);
                }
            });
            return false;
        }
        else {
            return false;
        }
    },

    getProductDetails: function () {
        var addToCart = Orders.GetDetails()
        if (addToCart) {
            return true;
        }
        else {
            return false
        }
    },

    GetDetails: function () {
        var selectedIds = [];
        var addOnsCustomText = [];
        var addOnValueIds = [];
        var addToCart = false;
        var checkBoxaAddOnCart = true;
        var textBoxAddOn = true;
        if (typeof parentSelector == "undefined") { parentSelector = ""; }

        //for dropdown
        $(parentSelector + " select.AddOn").each(function () {
            selectedIds.push($(this).val());
            addOnsCustomText.push("undefined");
        });

        //for radioButtons
        $(parentSelector + " input.AddOn:checked").each(function () {
            selectedIds.push($(this).val());
            addOnsCustomText.push("undefined");
        });

        ////for checkbox   
        $(".chk-product-addons").each(function () {
            var optional = $(this).data("isoptional");
            var id = $(this).attr("id");
            var addOnId = $(this).data("addonid");
            if (optional == "True") {               
                checkBoxaAddOnCart = true;
            }
            else {
                var isError = true;
                $('#' + id).find(':checkbox').each(function () {
                    if ($(this).is(':checked')) {                       
                        isError = false;
                    }
                });
                if (!isError) {                    
                    $("#paraComment-" + addOnId).css("display", "none");
                }
                else {
                    $("#paraComment-" + addOnId).removeAttr("style");
                    checkBoxaAddOnCart = false;
                }
            }            
        });

        //for textbox
        $(".AddOnValues").each(function () {
            var optional = $(this).data("isoptional");
            var displayType = $(this).data("addondisplaytype");
            var id = $(this).attr("id");
            var number = id.split('-');
            if (displayType == "TextBox" && optional == "False") {
                if ($("#" + id).val() == "") {
                    $("#paraComment-" + number[1]).removeAttr("style");
                    textBoxAddOn = false;
                }
                else {
                    selectedIds.push($(this).data("addonvalueid"));
                    addOnValueIds.push($(this).data("addonvalueid"));
                    addOnsCustomText.push($(this).val());
                    Orders.SetAddonsValues(3, this, "dynamic-addonvalueid", "dynamic-addonvaluecustomtext", selectedIds, addOnsCustomText)
                    $("#paraComment-" + number[1]).css("display", "none");
                    textBoxAddOn = true;
                }
            }
            else if (displayType == "TextBox" && optional == "True" && $(this).val() != "") {
                selectedIds.push($(this).data("addonvalueid"));
                addOnsCustomText.push($(this).val());
                textBoxAddOn = true;
            }
        });
        if (textBoxAddOn && checkBoxaAddOnCart) {
            addToCart = true;
        }
        return addToCart;
    },
}

