﻿StoreLocator = {
    
    DisplayText: function () {
        $('#divHyperlink').hide();
        $('#divDisplayText').show().removeClass("hidden");
    },

    Hyperlink: function () {
        $('#divDisplayText').hide();
        $('#divHyperlink').show().removeClass("hidden");
    },

    UploadNewImage: function () {
        $('#editImageSettings').show().removeClass("hidden");
        $('#imgStore').removeClass("hidden");
        $('#hdnNoImage').val(false);
    },

    KeepCurrentImage: function () {
        $('#editImageSettings').addClass("hidden");
        $('#imgStore').removeClass("hidden");
        $('#hdnNoImage').val(false);
    },

    NoImage: function () {
        $('#editImageSettings').addClass("hidden");
        $('#imgStore').addClass("hidden");
        $('#hdnNoImage').val(true);
    }
}