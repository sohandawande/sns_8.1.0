﻿License = {

    ShowSerialNumber: function () {
        var selected = $("input[type='radio'][name='LicenseType']:checked");
        if (selected.length > 0) {
            var selectedValue = selected.val();
            if (selectedValue == 'FreeTrial') {
                $("#serialNumberRow").hide();
            } else {
                $("#serialNumberRow").show();
            }
        }
    },

    ShowHideSerialNumberRow: function (value) {
        if (value == 'FreeTrial') {
            $("#serialNumberRow").hide();
        } else {
            $("#serialNumberRow").show();
        }
    },

    ToggleConfirmedActivationCheckbox: function(status) {
        $('#ActivateButton').attr("disabled", status);
    }
}