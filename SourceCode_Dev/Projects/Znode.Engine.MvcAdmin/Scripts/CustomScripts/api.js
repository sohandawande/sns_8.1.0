﻿/*
 | Ajax API Module
 | All ajax calls should be in this module
 |
 | 2015; Znode, Inc.
*/
App.Api = (function ($, Modernizr, App) {
    var ajax = {
        settings: {
            errorAsAlert: false // True will show error as alert dialog
        },

        // Common ajax request; includes cache buster for IE
        request: function (url, method, parameters, successCallback, responseType) {
            if (!method) { method = "GET"; }
            if (!responseType) { responseType = "json"; }

            // Requests must have callback method
            if (typeof successCallback != "function") {
                ajax.errorOut("Callback is not defined. No request made.");
            } else {
                $.ajax({
                    type: method,
                    url: url,
                    data: ajax.cachestamp(parameters),
                    dataType: responseType,
                    success: function (response) {
                        // _debug(response); // Always send response to console after request
                        successCallback(response);
                    },
                    error: function () {
                        ajax.errorOut("API Endpoint not available: " + url);
                    }
                });
            }
        },

        // Timestamp for cache busting
        cachestamp: function (data) {
            var d = new Date();

            if (typeof data == "string") {
                data += "&_=" + d.getTime();
            } else if (typeof data == "object") {
                data["_"] = d.getTime();
            } else {
                data = { "_": d.getTime() };
            }


            return (data);
        },

        // Error output
        errorOut: function (message) {
            console.log(message);

            if (this.settings.errorAsAlert) {
                alert(message);
            }
        }
    }

    // App API endpoints
    // Add methods here as needed for additional API calls
    // All endpoint methods are public
    var endpoints = {
        // get list of State bt CountryCode .
        getManufacturerListByKeyword: function (keyword, callbackMethod) {
            ajax.request("/Manufacturers/ManufacturerSearchList", "post", { "keyword": keyword }, callbackMethod, "html");
        },
        //Get list of Categories based on Catalog Id
        getUnAssignedCategoriesListByCatalogId: function (catalogId, callbackMethod) {
            ajax.request("/Facet/AssignedCategoriesList", "post", { "catalogId": catalogId }, callbackMethod, "html");
        },
        deleteRecordById: function (url, callbackMethod) {
            ajax.request(url, "post", {}, callbackMethod);
        },
        // Get product attributes and its values collection by ProductTypeId; Returns JSON
        getProductAttributesByProductTypeId: function (productTypeId, callbackMethod) {
            ajax.request("/Product/GetProductAttributesByProductTypeId", "get", { "productTypeId": productTypeId }, callbackMethod, "json");
        },

        getCssListByThemeId: function (url, cssThemeId, callbackMethod) {
            ajax.request(url, "get", { "CssThemeId": cssThemeId }, callbackMethod, "json");
        },

        getMasterPageListByThemeId: function (url, cssThemeId, callbackMethod) {
            ajax.request(url, "get", { "CssThemeId": cssThemeId }, callbackMethod, "json");
        },
        getPortalCssListByThemeId: function (cssThemeId, callbackMethod) {

            ajax.request("/Stores/BindCssList", "get", { "cssThemeId": cssThemeId }, callbackMethod, "json");
        },
        getTabDetails: function (url, queryParam, callbackMethod) {
            ajax.request(url, "get", queryParam, callbackMethod, "html");
        },

        getCategoryCssListByThemeId: function (cssThemeId, callbackMethod) {
            ajax.request("/Catalog/BindCssList", "get", { "CssThemeId": cssThemeId }, callbackMethod, "json");
        },
        getCategoryMasterPageListByThemeId: function (cssThemeId, callbackMethod) {
            ajax.request("/Catalog/BindMasterPageList", "get", { "CssThemeId": cssThemeId }, callbackMethod, "json");
        },

        getServiceListByShippingTypeId: function (serviceShippingTypeId, callbackMethod) {
            ajax.request("/Shipping/BindServiceList", "get", { "ServiceShippingTypeId": serviceShippingTypeId }, callbackMethod, "json");
        },
        bindFacetListByFacetGroupId: function (productId, facetGroupId, callbackMethod) {
            ajax.request("/Product/GetFacetsByFacetGroupId", "get", { "productId": productId, "facetGroupId": facetGroupId }, callbackMethod, "html");
        },
        getDynamicGridModel: function (url, callbackMethod) {
            ajax.request(url, "post", {}, callbackMethod, "html");
        },
        getCurrencyInformation: function (currencyId, callbackMethod) {
            ajax.request("/Stores/GetCurrencyInformation", "get", { "currencyId": currencyId }, callbackMethod, "json");

        },

        getCategoryListById: function (catalogid, callbackMethod) {
            ajax.request("/Product/GetCategoryList", "post", { "catalogid": catalogid }, callbackMethod, "json");
        },

        getHtmlContent: function (ddlDeleteTemplateKeys, callbackMethod) {
            ajax.request("/EmailTemplate/GetHtmlContent", "post", { "templatename": ddlDeleteTemplateKeys }, callbackMethod, "json");
        },

        associateProductCategory: function (url, categoryIds, callbackMethod) {
            ajax.request(url, "post", { "categoryIds": categoryIds }, callbackMethod);
        },
        getPaymentTypeForm: function (url, PaymentTypeId, callbackMethod) {
            ajax.request(url, "post", { "PaymentTypeId": PaymentTypeId }, callbackMethod, "html");
        },
        getPaymentGetwayForm: function (url, PaymentGatewayId, callbackMethod) {
            ajax.request(url, "post", { "PaymentGatewayId": PaymentGatewayId }, callbackMethod, "html");
        },

        addPortalCountries: function (url, billingCountryCodes, shippingCountryCodes, callbackMethod) {
            ajax.request(url, "post", { "billableCountryCodes": billingCountryCodes, "shippableCountryCodes": shippingCountryCodes }, callbackMethod);
        },

        categoryAssociatedProduct: function (url, productIds, callbackMethod) {
            ajax.request(url, "post", { "productIds": productIds }, callbackMethod);
        },

        accountAssociatedProfile: function (url, profileIds, callbackMethod) {            
            ajax.request(url, "post", { "profileIds": profileIds }, callbackMethod);
        },

        getProductCssListByThemeId: function (cssThemeId, callbackMethod) {
            ajax.request("/Category/BindCssList", "get", { "CssThemeId": cssThemeId }, callbackMethod, "json");
        },

        getProductMasterPageListByThemeId: function (cssThemeId, callbackMethod) {
            ajax.request("/Category/BindMasterPageList", "get", { "CssThemeId": cssThemeId }, callbackMethod, "json");
        },
        associateBundleProduct: function (url, productIds, callbackMethod) {
            ajax.request(url, "post", { "productIds": productIds }, callbackMethod);
        },
        bindFileTypeDropDown: function (callbackMethod) {
            ajax.request("/Download/FileTypeList", "get", {}, callbackMethod, "json");
        },
        getAssociateSkuFacetByProductId: function (productId, skuId, callbackMethod) {
            ajax.request("/Product/GetAssociateSKUFacets", "get", { "productId": productId, "skuId": skuId }, callbackMethod, "html");
        },

        associateSKUFacets: function (url, associatedIds, unAssociatedIds, callbackMethod) {
            ajax.request(url, "post", { "associatedIds": associatedIds, "unAssociatedIds": unAssociatedIds }, callbackMethod);
        },
        getSkuFacetBySkuId: function (skuId, productId, callbackMethod) {
            ajax.request("/Product/GetSKUFacetsList?skuId=" + skuId + "&productId=" + productId, "post", { "skuId": skuId, "productId": productId }, callbackMethod, "html");
        },
        bindFacetListBySkuFacetGroupId: function (productId, skuId, facetGroupId, callbackMethod) {
            ajax.request("/Product/GetFacetsBySKUFacetGroupId", "get", { "productId": productId, "skuId": skuId, "facetGroupId": facetGroupId }, callbackMethod, "html");
        },
        getAssociateSkuFacetByGroupId: function (facetgroupId, productId, skuId, callbackMethod) {
            ajax.request("/Product/EditSkuAssociatedFacet", "get", { "id": facetgroupId, "productId": productId, "skuId": skuId }, callbackMethod, "html");
        },

        getCategoriesByPortalId: function (url, portalId, callbackMethod) {
            ajax.request(url, "get", { "portalId": portalId }, callbackMethod, "json");
        },

        getStateListByCountryCode: function (countryCode, callbackMethod) {
            ajax.request("/Tax/BindStateList", "get", { "countryCode": countryCode }, callbackMethod, "json");
        },

        getFranchiseStateListByCountryCode: function (countryCode, callbackMethod) {
            ajax.request("/FranchiseTax/BindStateList", "get", { "countryCode": countryCode }, callbackMethod, "json");
        },

        getCountyListByStateCode: function (stateCode, callbackMethod) {
            ajax.request("/Tax/BindCountyList", "get", { "stateCode": stateCode }, callbackMethod, "json");
        },

        getFranchiseCountyListByStateCode: function (stateCode, callbackMethod) {
            ajax.request("/FranchiseTax/BindCountyList", "get", { "stateCode": stateCode }, callbackMethod, "json");
        },
        getSkuProfileList: function (skuId, callbackMethod) {
            ajax.request("/Product/ListSkuProfile?skuId=" + skuId, "post", { "skuId": skuId }, callbackMethod, "html");
        },
        createSkuProfile: function (skuId, url, callbackMethod) {
            ajax.request(url, "get", { "skuId": skuId }, callbackMethod, "html");
        },
        editSkuProfile: function (profileId, callbackMethod) {
            ajax.request("/Product/EditSkuProfile", "get", { "profileId": profileId }, callbackMethod, "html");
        },
        associateSKUProfile: function (data, url, callbackMethod) {
            ajax.request(url, "post", data, callbackMethod, "json");
        },
        createReasonForReturn: function (callbackMethod) {
            ajax.request("/RMAConfiguration/CreateReasonForReturn", "get", {}, callbackMethod, "html");
        },

        addReasonForReturn: function (data, callbackMethod) {
            ajax.request("/RMAConfiguration/CreateEditReasonForReturn", "post", data, callbackMethod);
        },

        editReasonForReturn: function (reasonForReturnId, callbackMethod) {
            ajax.request("/RMAConfiguration/EditReasonForReturn", "get", { "reasonForReturnId": reasonForReturnId }, callbackMethod, "html");
        },

        previewTemplate: function (templateName, callbackMethod) {
            ajax.request("/EmailTemplate/PreviewTemplate", "post", { "templateName": templateName }, callbackMethod, "json");
        },

        editRequestStatus: function (requestStatusId, callbackMethod) {
            ajax.request("/RMAConfiguration/EditRequestStatus", "get", { "requestStatusId": requestStatusId }, callbackMethod, "html");
        },

        addEditRequestStatus: function (data, callbackMethod) {
            ajax.request("/RMAConfiguration/EditRequestStatus", "post", data, callbackMethod);
        },
        getSearchIndexStatus: function (indexId, callbackMethod) {
            ajax.request("/ManageSearchIndex/GetSearchIndexStatus", "post", { "indexId": indexId }, callbackMethod, "html");
        },

        getPromotionTypeValueByPromotionName: function (Name, callbackMethod) {
            ajax.request("/ProviderEngine/GetPromotionTypeByClassName", "get", { "Name": Name }, callbackMethod, "json");
        },

        getTaxTypeValueByName: function (Name, callbackMethod) {
            ajax.request("/ProviderEngine/GetTaxRuleTypeByClassName", "get", { "Name": Name }, callbackMethod, "json");
        },

        GetSupplierTypeByName: function (Name, callbackMethod) {
            ajax.request("/ProviderEngine/GetSupplierTypeByClassName", "get", { "Name": Name }, callbackMethod, "json");
        },

        GetShippingTypeByName: function (Name, callbackMethod) {
            ajax.request("/ProviderEngine/GetShippingTypeByClassName", "get", { "Name": Name }, callbackMethod, "json");
        },

        clearAdvanceSearch: function (callbackMethod) {
            ajax.request("/AdvanceSearch/ClearAdvanceSearchAjax", "post", { indexId: "0" }, callbackMethod, "html");
        },

        clearAdvanceSearchFranchise: function (callbackMethod) {
            ajax.request("/FranchiseAdvanceSearch/ClearAdvanceSearchAjax", "post", { indexId: "0" }, callbackMethod, "html");
        },

        getProductList: function (url, callbackMethod) {
            ajax.request(url, "get", {}, callbackMethod, "html");
        },

        bindAttributeTypes: function (catalogId, callbackMethod) {
            ajax.request("/ImportExport/GetAttributesTypesByCatalogId", "get", { "catalogId": catalogId }, callbackMethod, "json");
        },
        bindCategories: function (catalogId, callbackMethod) {
            ajax.request("/ImportExport/GetCategoriesByCatalogId", "get", { "catalogId": catalogId }, callbackMethod, "json");
        },
        autoCompleteSearchProduct: function (searchText, catalogId, callbackMethod) {
            ajax.request("/ImportExport/AutoCompleteSearchProduct", "post", { "searchText": searchText, "catalogId": catalogId }, callbackMethod, "json");
        },
        addToCart: function (url, cartItem, callbackMethod) {
            ajax.request(url, "post", cartItem, callbackMethod, "html");
        },

        issueGiftCard: function (requestModel, callbackMethod) {
            ajax.request("/RmaManager/IssueGiftCard", "post", requestModel, callbackMethod);

        },
        addAnotherProduct: function (url, cartItem, callbackMethod) {
            ajax.request(url, "post", cartItem, callbackMethod, "html");
        },

        getPromotionTypeForm: function (url, promotionType, callbackMethod) {
            ajax.request(url, "post", { "promotionType": promotionType }, callbackMethod, "html");
        },

        getProfileListByPortalId: function (storeId, callbackMethod) {
            ajax.request("/Promotion/GetProfileListByPortalId", "get", { "storeId": storeId }, callbackMethod, "json");
        },

        getCatalogListByPortalId: function (storeId, callbackMethod) {
            ajax.request("/Promotion/GetCatalogListByPortalId", "get", { "storeId": storeId }, callbackMethod, "json");
        },

        getCategoryListByPortalId: function (storeId, callbackMethod) {
            ajax.request("/Promotion/GetCategoryListByPortalId", "get", { "storeId": storeId }, callbackMethod, "json");
        },

        getCouponCodeForm: function (url, callbackMethod) {
            ajax.request(url, "post", {}, callbackMethod, "html");
        },
        getProfilesAssociatedWithCategory: function (url, callbackMethod) {
            ajax.request(url, "get", {}, callbackMethod, "html");
        },

        getSkuListByProductId: function (productId, callbackMethod) {
            ajax.request("/Promotion/GetSkuListByProductId", "post", { "productId": productId }, callbackMethod, "html");
        },
        autoCompleteCategorySearch: function (url, searchText, catalogId, callbackMethod) {
            ajax.request(url, "post", { "searchText": searchText, "catalogId": catalogId }, callbackMethod, "json");
        },
        searchCategoryList: function (callbackMethod) {
            ajax.request("/Catalog/SearchCategory", "get", {}, callbackMethod, "html");
        },
        searchCategoryListFranchiseAdmin: function (callbackMethod) {
            ajax.request("/FranchiseAdmin/FranchiseCatalog/SearchCategory", "get", {}, callbackMethod, "html");
        },
        // Apply coupon code to cart order
        useCouponCode: function (url, couponCode, callbackMethod) {
            ajax.request(url, "get", { "Coupon": couponCode }, callbackMethod, "html");
        },
        // Apply gift card to order during checkout
        useGiftCard: function (url, gcNumber, callbackMethod) {
            ajax.request(url, "get", { "giftCardNumber": gcNumber }, callbackMethod, "html");
        },
        GetPaymentGatwayName: function (url, paymentOptionId, callbackMethod) {
            ajax.request(url, "get", { "paymentOptionId": paymentOptionId }, callbackMethod);
        },
        GetCatalogSelectList: function (data, callbackMethod) {
            ajax.request("/Orders/CatalogSelectList", "get", { "portalId": data }, callbackMethod, "html");
        },
        CalculateShippingCharges: function (url, shippingId, callbackMethod) {
            ajax.request(url, "get", { "shippingId": shippingId }, callbackMethod, "html");
        },
        // Update product attributes
        getProductAttributes: function (url, data, callbackMethod) {
            ajax.request(url, "get", data, callbackMethod);
        },
        GetCustomerInformation: function (url, data, callbackMethod) {
            ajax.request(url, "get", { "accountId": data }, callbackMethod, "html");
        },
        getShippingTypeView: function (shippingRuleTypeId, callbackMethod) {
            ajax.request("/Shipping/GetShippingTypeView", "post", { "shippingruletypeId": shippingRuleTypeId }, callbackMethod, "html");
        },
        getCategoryAssociatedProfilesData: function (categoryId, categoryNodeId, callbackMethod) {
            ajax.request("/Catalog/GetCatgoryProfilesData", "get", { "categoryId": categoryId, "categoryNodeId": categoryNodeId }, callbackMethod, "html");
        },
        createRMARequest: function (data, callbackMethod) {
            ajax.request("/RMAManager/CreateRMA", "post", data, callbackMethod);
        },
        getProductSearchResult: function (url, productId, portalId, categoryId, productName, sku, callbackMethod) {
            ajax.request(url, "get", { "productId": productId, "portalId": portalId, "categoryId": categoryId, "productName": productName, "sku": sku }, callbackMethod, "html");
        },
        productAssociatedHilights: function (url, selectedHighlights, callbackMethod) {
            ajax.request(url, "post", { "selectedHighlights": selectedHighlights }, callbackMethod);
        },
        getEnableUrlForm: function (url, callbackMethod) {
            ajax.request(url, "post", {}, callbackMethod, "html");
        },
        getFranchiseShippingTypeView: function (shippingRuleTypeId, callbackMethod) {
            ajax.request("/FranchiseShipping/GetShippingTypeView", "post", { "shippingruletypeId": shippingRuleTypeId }, callbackMethod, "html");
        },

        getFranchiseServiceListByShippingTypeId: function (serviceShippingTypeId, callbackMethod) {
            ajax.request("/FranchiseShipping/BindServiceList", "get", { "ServiceShippingTypeId": serviceShippingTypeId }, callbackMethod, "json");
        },
        getCountyListByStateCode: function (stateCode, callbackMethod) {
            ajax.request("/FranchiseTax/BindCountyList", "get", { "stateCode": stateCode }, callbackMethod, "json");
        },

        getConvertedDecimalValues: function (price, callbackMethod) {
            ajax.request("/RmaManager/GetConvertedCurrencyValues", "post", { "decimalValue": price }, callbackMethod);
        },

        GetAccountPaymentList: function (url, referralcommissiontype, accountid, callbackMethod) {
            ajax.request(url, "post", { "refrralCommissionType": referralcommissiontype, "id": accountid }, callbackMethod, "html");
        },

        GetReferralCommissionList: function (url, accountid, callbackMethod) {
            ajax.request(url, "post", { "accountId": accountid }, callbackMethod, "html");
        },
        getEmailDiagnostics: function (caseNumber, callbackMethod) {
            ajax.request("/Diagnostics/Diagnostics/EmailDiagnostics", "post", { "caseNumber": caseNumber }, callbackMethod, "json");
        },
        showDiagnosticsTrace: function (url, callbackMethod) {
            ajax.request(url, "get", {}, callbackMethod, "html");
        },
        getProfilesByPortalId: function (storeId, callbackMethod) {
            ajax.request("/Franchise/GetProfileListByPortalId", "get", { "storeId": storeId }, callbackMethod, "json");
        },
        GetSubGridPartial: function (id, type, method, callbackMethod) {
            ajax.request("/AdvanceSearch/GetSubGrid", "get", { "recoredid": id, "type": type, "method": method }, callbackMethod, "html");
        },

        GetXml: function (id, callbackMethod) {
            ajax.request("/XMLGenerator/View", "get", { "id": id }, callbackMethod, "html");
        },
        GetColumnsList: function (entityType, entityName, columnListJson, callbackMethod) {
            ajax.request("/XMLGenerator/GetColumnList", "get", { "entityType": entityType, "entityName": entityName, "columnListJson": columnListJson }, callbackMethod, "html");
        },

        GetEntityName: function (term, entityType, callbackMethod) {
            ajax.request("/XMLGenerator/AutoCompleteEntityName", "POST", { "term": term, "entityType": entityType }, callbackMethod, "json");
        },
        SaveXmlData: function (url, _griddata, txtviewOptions, entityType, entityName, txtfrontPageName, txtfrontObjectName, id, callbackMethod) {
            ajax.request(url, "POST", { "columnCollection": _griddata, "viewOptions": txtviewOptions, "entityType": entityType, "entityName": entityName, "frontPageName": txtfrontPageName, "frontObjectName": txtfrontObjectName, "id": id }, callbackMethod, "json");
        },
        GetGraphData: function (url,reportId, xAxis, yAxis, callbackMethod) {
            ajax.request(url, "POST", { "reportId": reportId, "groupByClause": xAxis, "countClause": yAxis }, callbackMethod, "json");
        },
        getEnableMultipleCouponCheckBox: function (url, selectedPortalId, callbackMethod) {
            ajax.request(url, "get", { "selectedPortalId": selectedPortalId }, callbackMethod, "json");
        },
        GetCategoryTree: function (url, catalogId, name, callbackMethod) {
            ajax.request(url, "get", { "catalogId": catalogId, "name": name }, callbackMethod, "json");
        },
        ProcessPayPalPayment: function (url, paymentOptionId, returnUrl, cancelUrl, callbackMethod) {
            ajax.request(url, "get", { "paymentOptionId": paymentOptionId, "returnUrl": returnUrl, "cancelUrl": cancelUrl }, callbackMethod, "json");
        },
        processCreditCardPayment: function (url, paymentSettingId, customerProfileId, customerPaymentId, callBackMethod) {
            ajax.request(url, "get", { "paymentSettingId": paymentSettingId, "customerProfileId": customerProfileId, "customerPaymentId": customerPaymentId }, callBackMethod, "json");
        },
        //PRFT Custom Code : Start
        userAssociatedCustomer: function (url, customerAccountIds, callbackMethod) {
            ajax.request(url, "post", { "customerAccountIds": customerAccountIds }, callbackMethod);
        },
        //PRFT Custom Code : End
    }
    return (endpoints);

}(jQuery, Modernizr, App));

// END module