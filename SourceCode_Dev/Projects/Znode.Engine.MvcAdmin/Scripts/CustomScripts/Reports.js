﻿var globalPageIndex = 1;
var globalPageSize = 10;
var reportTypeTileGrid = "";
$(document).on("click", "#btnMoveLeft", function ()
{   
    $('#SelectedCities .selected').each(function () {
         $("#RequestedSelectedCities").children("li").removeClass("selected");
            $("#RequestedSelectedCities").append("<li data-id=" + $(this).attr("data-id") + " class='selected' title='" + $(this).html() + "' data-datatype='" + $(this).attr("data-datatype") + "' data-mustshow='n' data-value=" + $(this).attr("data-value") + ">" + $(this).html() + "</li>");
            var dynamicHtmlTestNext = $(this).next("li:[data-mustshow='n']");
            var dynamicHtmlTestPre = $(this).prev("li:[data-mustshow='n']");
            if (dynamicHtmlTestNext.length != 0) {
                $(this).next("li:[data-mustshow='n']").attr("class", "selected");
                $(this).remove();
            }
            else if (dynamicHtmlTestPre.length != 0) {
                $(this).prev("li:[data-mustshow='n']").attr("class", "selected");
                $(this).remove();
            }
            else {
                $(this).remove();
                $("#SelectedCities li:[data-mustshow='n']").first().attr("class", "selected");
            }
        });    
});
$(document).on("click", "#btnMoveRight", function ()
{
    if ($("#RequestedSelectedCities li").length > 1)
    {
        $('#RequestedSelectedCities .selected').each(function () {
        $("#SelectedCities").children("li").removeClass("selected");
        $("#SelectedCities").append("<li data-id=" + $(this).attr("data-id") + " title='" + $(this).html() + "' class='selected' data-datatype='" + $(this).attr("data-datatype") + "' data-mustshow='n' data-value=" + $(this).attr("data-value") + ">" + $(this).html() + "</li>");
        var dynamicHtmlTestNext = $(this).next("li:[data-mustshow='n']");
        var dynamicHtmlTestPre = $(this).prev("li:[data-mustshow='n']");      
        if (dynamicHtmlTestNext.length != 0) {
            $(this).next("li:[data-mustshow='n']").attr("class", "selected");
            $(this).remove();
        }
        else if (dynamicHtmlTestPre.length != 0) {
            $(this).prev("li:[data-mustshow='n']").attr("class", "selected");
            $(this).remove();
        }
        else {
            $(this).remove();
            $("#RequestedSelectedCities li:[data-mustshow='n']").first().attr("class", "selected");
        }     
           
    });
    }
    else
    {
       return false;
    }
});
$(document).on("click", "#btnAllMoveLeft", function () {
    $("#SelectedCities li").each(function () {
        $("#RequestedSelectedCities").append("<li data-id=" + $(this).attr("data-id") + " title='" + $(this).html() + "' data-value=" + $(this).attr("data-value") + ">" + $(this).html() + "</li>");
        $(this).remove();
    });

});
$(document).on("click", "#btnAllMoveRight", function () {   
    $("#RequestedSelectedCities li").each(function () {
        if ($(this).attr("data-mustshow") != "y") {
            $("#SelectedCities").append("<li data-id=" + $(this).attr("data-id") + " title='" + $(this).html() + "' data-value=" + $(this).attr("data-value") + ">" + $(this).html() + "</li>");
            $(this).remove();
        }

    });

});
$(document).on("click", "#btnMoveUp", function () {
    listbox_move("RequestedSelectedCities", "up");
});
$(document).on("click", "#btnMoveDown", function () {
    listbox_move("RequestedSelectedCities", "down");
});
$(document).on("click", "#btnAllUp", function () {
    listbox_move("RequestedSelectedCities", "First");
});
$(document).on("click", "#btnAllDown", function () {
    listbox_move("RequestedSelectedCities", "Last");
});
$(document).on("click", "#btnSaveFilter", function () {
    SaveReportView();  
});
$(document).on("change", "[data-select='operator']", function () {    
    var selIndex = this.selectedIndex;  
    var selectedOperator = this.options[selIndex];
    var count = $(selectedOperator).attr("data-count");  
    if ($(selectedOperator).attr("data-datatype") != "" && $(selectedOperator).attr("data-datatype") != null) {
        LoadOperatorList($(selectedOperator).attr("data-datatype"), count);
    } else
    {
        $('#reportViewOperatorList_' + count).html("");
        $('#reportViewFilterValue_' + count).val("");
        $("#reportViewFilterAndOr_" + count + " option").each(function ()
        {
            if ($(this).attr("value")=="") {
                $(this).attr("selected", "selected");              
            }
        });
        
    }
});

$(document).on("change", ".webGridPageSize", function () {
    overlayforwaitblock();
});

$(document).on("keydown", "#searchTextReport", function (e) {
    if (e.charCode == 13 || e.keyCode == 13 || e.which == 13) {
        $("#searchTextReportButton").click();
    }
});

$(document).on("click", "#searchTextReportButton", function (e) {
    var reportId = window.location.href.split("ReportId=");
    ReportStudentTestGrid(reportId[1]);
});
$(document).on("click", "#clearTextReportButton", function (e) {
    $("#searchTextReport").val("");
    var reportId = window.location.href.split("ReportId=");
    ReportStudentTestGrid(reportId[1]);
});

$(document).on("click", "#SelectedCities li,#RequestedSelectedCities li", function (e) {
    if(!$(this).attr("disabled")){
    $(this).parent("ul").children("li").removeClass("selected");
    $(this).addClass("selected");
     }
});

$(document).on("click", ".webgrid-headerAssignStudent", function (e) {

});

$(document).on("change", "#graphFieldFirst", function () { 
    var graphField = $("#graphFieldFirst").attr("value");  
    $("#graphFieldSecond option").each(function () {
        if (graphField == $(this).attr("value")) {
            $(this).hide();
        } else {
            $(this).show();
        }
    });
});

$(document).on("change", "#graphFieldSecond", function () {
    var graphField = $("#graphFieldSecond").attr("value");  
    $("#graphFieldFirst option").each(function () {     
        if (graphField == $(this).attr("value")) {
            $(this).hide();
        } else {
            $(this).show();
        }
    });
});

function listbox_move(listID, direction)
{ 
    var selValue = $("#" + listID + " .selected").attr("data-value");  
    var selText = $("#" + listID + " .selected").html();
    var selId = $("#" + listID + " .selected").attr("data-id");
    var mustShow = $("#" + listID + " .selected").attr("data-mustshow");
    var datatype = $("#" + listID + " .selected").attr("data-datatype");
    if (selValue != undefined && selId != undefined) {
        var dynamicHtml = "<li data-id='" + selId + "' title='" + selText + "' data-datatype='" + datatype + "' data-mustshow='" + mustShow + "' data-value='" + selValue + "'>" + selText + "</li>";
        if (direction == "First") {
            $("#" + listID).prepend(dynamicHtml);
        }
        else if (direction == "Last") {
            $("#" + listID).append(dynamicHtml);
        }
        else if (direction == "down")
        {
            var dynamicHtmlTest = $("#" + listID + " .selected").next("li");        
            if (dynamicHtmlTest.length != 0) {
                $("#" + listID + " .selected").next("li").after(dynamicHtml);
            } else { return false;}
        }
        else
        {
            var dynamicHtmlTest = $("#" + listID + " .selected").prev("li");          
            if (dynamicHtmlTest.length != 0) {
                $("#" + listID + " .selected").prev("li").before(dynamicHtml);
            } else { return false; }
        }
        $("#" + listID + " .selected").remove();
        $("#" + listID + " li:[data-id='" + selId + "']").attr("class", "selected");
    }
}
function SaveReportView() {
    var xmlString = "<root version='1.0' type='XML'> <columns>";
    $("#RequestedSelectedCities li").each(function () {
        var id = $(this).attr("data-id");
        xmlString += "<column>" +
                    "<id>" + id + "</id>" +
                    "</column>";
    });
    xmlString += "</columns></root>";
    var graphFields = $("#graphFieldFirst").attr("value") + "," + $("#graphFieldSecond").attr("value");

    $.ajax({
        url: "/TeacherReports/SaveSelectedColumnData",
        contentType: 'application/html; charset=utf-8',
        data: { selectClause: xmlString, showGraph: graphFields },
        type: 'GET',
        success: function (data) {
            SaveReportFilterView();
        },
        error: function () {
            log("!!!OOPS Error Occurred  ~/TeacherReports/SaveSelectedColumnData");
        }
    });

}
function ReportStudentTestGrid(reportId, reportViewType)
{  
    var pageSize = $(".webGridPageSize option:selected").attr("value"); 
    var pageIndex = globalPageIndex;
    var searchText = $("#searchTextReport").val();
    reportViewType=reportTypeTileGrid;
    if (reportViewType==undefined) {
        reportViewType = "";
    }  
    overlayforwaitblock();
    $.ajax({
        url: "/TeacherReports/ReportStudentTestGrid",
        contentType: 'application/html; charset=utf-8',
        data: { reportId: reportId, pageSize: pageSize, page: pageIndex, searchText: searchText, reportViewType: reportViewType },
        type: 'GET',
        success: function (data)
        {         
            $("#graphCheckboxContainer").show();
            var graphType="Pie";
            $("#charttype input:radio").each(function ()
            {                
                if (document.getElementById($(this).attr("id")).checked == true)
                {                   
                    graphType= $(this).attr("data-graphtype");
                }
            });           
            graphReport.GraphReport(reportId, graphType);           
            $('#ReportTestGrid').html("");
            $('#ReportTestGrid').html(data);
            SetBorderToReportGrid();
            overlayforwaitnone();
            HideNiceScrollExceptBody();
            SetCustomScrolling();            
           
        },
        error: function () {
            log("!!!OOPS Error Occurred  ~/Assignment/AssignStudentListGrid");
        }
    });
}
function LoadOperatorList(dataType,count)
{    
    $.ajax({
        url: "/TeacherReports/GetOperatorList",
        contentType: 'application/html; charset=utf-8',
        data: { xmlOperatorString: "", dataType: dataType },
        type: 'GET',
        success: function (data)
        {
            $('#reportViewOperatorList_' + count).html("");
            $('#reportViewOperatorList_' + count).html(data.dynamicOperatorString);
            $("#filterError_" + count).text("");
            SetValidationForValue(dataType, count);        
        },
        error: function () {
            log("!!!OOPS Error Occurred  ~/Assignment/AssignStudentListGrid");
        }
    });
}
function SetValidationForValue(dataType, count)
{
    $('#reportViewFilterValue_' + count).removeAttr("onkeypress");
    $('#reportViewFilterValue_' + count).removeAttr("onclick");
    $('#reportViewFilterValue_' + count).removeAttr("maxlength");
    $('#reportViewFilterValue_' + count).show();
    $('#dateTimeFilterContainer_' + count).hide();
    if (dataType == "Int32" || dataType == "Int64" || dataType == "Int64" || dataType == "Int16" || dataType == "Double") 
    {
        $('#reportViewFilterValue_' + count).attr("onkeypress", "return validateNumber(event);");
        $('#reportViewFilterValue_' + count).attr("maxlength", "10");
    }
    else if (dataType == "String")
    {       
        $('#reportViewFilterValue_' + count).attr("maxlength", "50");
    }
    else if (dataType == "DateTime")
    {
        $('#reportViewFilterValue_' + count).hide();
        $('#dateTimeFilterContainer_' + count).show();
        DatePicker.BindDateTimePicker('reportViewFilterDate_' + count);
    }   

}
function SaveReportFilterView()
{   
    var list = [];
    var flagFilter = true;
    for (var i = 1; i <= 5; i++)
    {
        var field = $("#reportViewFieldList_" + i).attr("value");
        var operator = $("#reportViewOperatorList_" + i).attr("value");
        var dataType = $("#reportViewFieldList_" + i + " option:selected").attr("data-datatype");
        var value = "";
        value = (dataType == "DateTime") ? $("#reportViewFilterDate_" + i).attr("value") : $("#reportViewFilterValue_" + i).attr("value");
        var filterAndOr = $("#reportViewFilterAndOr_" + i).attr("value");       
        filterAndOr = (filterAndOr == "") ? "AND" : filterAndOr;
        $("#filterError_" + i).text("");     
        if (field != "" && field != null)
        {
            if (value == "")
            {            
                $("#filterError_" + i).text(" Filter value is required");
                flagFilter = false;
            }
            else
            {
                var model = {};
                model.Field = field; model.Operator = operator; model.Value = value; model.AndOr = filterAndOr;               
                list.push(model);
            }
        }  
    }
    if (flagFilter)
    {
        $.ajax({
            url: "/TeacherReports/SaveFilterCondition",
            type: 'POST',
            dataType: 'json',
            traditional: true,
            data: { list: JSON.stringify(list) },
            success: function (data) {
                window.location.href = "/TeacherReports/Report";
            }
        });
    }
    else
    {
        return false;
    }
}
function UpdateReportGrid() { 
    /*SetPositionPageSize();*/
    /* SetSelectedPageSize(globalPageSize);*/
    overlayforwaitnone();
    HideNiceScrollExceptBody();
    SetBorderToReportGrid();
    SetCustomScrolling();   
    graphReport.GraphReport(0, globalReportGraphType);
}

function SetPositionPageSize(){
    var pageDyhtml = "<label style='float: left;'>Page Size</label> <select id='reportPageSizeSelection' class='report-dropdown'>" +
               " <option value='10'>10</option>"+
                "<option value='20'>20</option>"+
                "<option value='30'>30</option>"+
                "<option value='40'>40</option>"+
                "<option value='50'>50</option>"+
                "</select>";  
    $("#pageSizeContainer").prepend(pageDyhtml);  
   
}
function SetSelectedPageSize(pageSize)
{
    if (pageSize != "" && pageSize != undefined)
    {
        $('.webGridPageSize option').each(function () {
            if (pageSize == $(this).attr("value"))
            {
                $(this).attr("selected","selected");
            }
        });
    }
}
function DetectFilterConditon()
{
    var reportFilterConditions = globalReportfilter.split("|");
    for (var i = 0; i <= reportFilterConditions.length; i++)
    {
        var j = i + 1;       
        var filterCondition = reportFilterConditions[i].split(",");       

        var field = filterCondition[0];
        var operator = filterCondition[1];
        var filterValue = filterCondition[2];
        var andOr = filterCondition[3];
        $("#reportViewFilterValue_" + j).val(filterValue);     
         $("#reportViewFieldList_"+j+" option").each(function ()
         {
             if (field==$(this).attr("value"))
             {
                 $(this).attr("selected", "selected");              
                 DetectOperatorSelection(operator, j, $(this).attr("data-datatype"));
             }    
         });
         $("#reportViewFilterAndOr_" + j + " option").each(function ()
         {
             if (andOr == $(this).attr("value")) {
                 $(this).attr("selected", "selected");              
             }
         });
    }    
}
function DetectOperatorSelection(operator, count, dataType)
{  
    try{
        if (count != null && count!="") {
            var dynamicOptionList = "";
            $("#reportViewOperatorList option").each(function ()
            {             
                var keyName = $(this).attr("data-keyname");
                var thisDatatype = $(this).attr("data-datatype");
                var filterConditionFormat = $(this).attr("data-keyname");
                var value = $(this).attr("value");
                var showText = $(this).text();           
                if (thisDatatype == dataType)
                {                   
                    if (operator == value)
                    {           
                        dynamicOptionList += "<option selected='selected' title='" + showText + "' data-datatype='" + thisDatatype + "' data-keyname='" + filterConditionFormat + "' value='" + value + "'>" + showText + "</option>";
                    }
                    else
                    {
                        dynamicOptionList += "<option data-datatype='" + thisDatatype + "' title='" + showText + "' data-keyname='" + filterConditionFormat + "' value='" + value + "'>" + showText + "</option>";
                    }
                }
            });
          
            $("#reportViewOperatorList_" + count).html("");
            $("#reportViewOperatorList_" + count).html(dynamicOptionList);
        }
    }catch(err){}

}

function SetBorderToReportGrid()
{
 
    var ReportTestGridWidth = $("#ReportTestGrid").width();
    var globalTableWidth=0;  
    $(".webgrid-headerAssignStudent th").each(function () {
        $(this).find("a").attr("title",$(this).find("a").text());
        globalTableWidth++;
    });
    if (globalTableWidth > 8)
    {
        ReportTestGridWidth = parseInt(globalTableWidth) * 150;
        $(".DynamicTableClass").attr("style", "width:" + ReportTestGridWidth + "px; ! important");
    }
    var bVertical = $("#ReportTestGrid").height() < document.getElementById("ReportTestGrid").scrollHeight;   
    if (bVertical)
    {
        $("#ReportTestGrid").attr("style", "border-bottom:1px solid #c3c3c3;");
    } 
}

function tileGridCallWithPagging(ActionNameWithPageNo) {
    var strSplitter = ActionNameWithPageNo.split('/');
    var pageno = 1;
    try {
        var SplitValue = ActionNameWithPageNo.split('?page=');
        pageno = SplitValue[1];
    } catch (err) { }
    globalPageIndex = pageno;
    var reportViewType = "Tile";
    ReportStudentTestGrid(0, reportViewType);
}
