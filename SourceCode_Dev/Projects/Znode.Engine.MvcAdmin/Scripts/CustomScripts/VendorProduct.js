﻿
VendorProduct = {
    Load: function () {
        var selectedItems = $("#ProductModel_ProductAttributes").val();
        if (selectedItems && selectedItems.length > 0) {
            var productTypeId = $("#ProductModel_ProductTypeId").val();
            VendorProduct.BindAttributes(productTypeId);
        }
        VendorProduct.ShowHideGiftCard();
    },
    GetProductAttribute: function (obj) {
        $("#hdnProductName").val($('#ProductTypeId :selected').text());
        $("#ProductModel_ProductAttributes").val('');
        var productTypeId = $('#' + obj.id + ' option:selected').val();
        VendorProduct.BindAttributes(productTypeId);
    },
    BindAttributes: function (productTypeId) {
        $("#divAttributes").hide();
        $("#divGiftCards").hide();
        $("#divProductAttributes").html('');
        App.Api.getProductAttributesByProductTypeId(productTypeId, function (response) {

            if (response.length > 0) {
                var isGiftCard = response[0].IsGiftCard;
                if (isGiftCard == 1) {
                    $("#IsProductTypeGiftCards").val(isGiftCard);
                    $("#divGiftCards").show();
                }
                else {
                    VendorProduct.BindAttributeList(response);
                    isAttributeLoad = true;
                    $("#divAttributes").show();
                }
            }
            else {
                var productType = $('#ProductModel_ProductTypeId option:selected').text();
                if (productType.toLowerCase().indexOf("gift") != -1) {
                    $("#divGiftCards").show();
                }
            }
        });
    },
    CheckSelectedAttribute: function (attributId) {
        var selectedItems = $("#ProductModel_ProductAttributes").val();
        var arrSelectedItems = [];
        if (selectedItems.length > 0) {
            arrSelectedItems = selectedItems.split(',');
            for (var i = 0; i < arrSelectedItems.length; i++) {
                if (parseInt(arrSelectedItems[i]) == parseInt(attributId)) {
                    return true;
                }
            }
        }
        return false;
    },
    BindAttributeList: function (data) {
        for (var i = 0; i < data.length; i++) {
            var drpId = "ddlAttribute_" + data[i].AttributeTypeId;
            var ddl = '<select name="' + drpId + '" id="' + drpId + '" class="ddlAttributeList"  onchange="VendorProduct.ClearValidation(this);"><option value="">' + data[i].AttributeName + '</option>';
            for (var j = 0; j < data[i].AttributeValueList.length; j++) {
                var attributId = data[i].AttributeValueList[j].Value;
                var attributName = data[i].AttributeValueList[j].Text;
                var selected = VendorProduct.CheckSelectedAttribute(attributId);
                if (selected) {
                    ddl += '<option value="' + attributId + '"selected="selected">' + attributName + '</option>';
                }
                else {
                    ddl += '<option value="' + attributId + '">' + attributName + '</option>';
                }
            }
            ddl += '</select>'
            var errId = "val" + drpId;
            ddl += '<span id="' + errId + '" class="field-validation-valid" data-valmsg-replace="true" data-valmsg-for="' + drpId + '"></span>'
            $("#divProductAttributes").append(ddl);
        }
    },
    CheckSucess: function (data) {
        var type = "error";
        if (data.sucess) {
            type = "success";
        }
        document.getElementById("panel7").innerHTML = data.Response;
        try { if (data.message == "") { $('#MessageBoxContainerId').html(''); } } catch (err) { }
        CommonHelper.DisplayNotificationMessagesHelper(data.message, type, data.isFadeOut, 5000);
    },
    AssociateBundleProduct: function (reviewState) {
        var productIds = DynamicGrid.GetSelectedCheckBoxValue().toString();
        if (productIds.length > 0) {
            var type = "error";
            if (productIds.length > 0) {
                $("#SelectedProductIds").attr('value', productIds);
                $("#reviewState").attr('value', reviewState);
                $("#AssociateVendorProduct").submit();
                localStorage.selectedchkboxItems = "";
            }
        }
        else {
            CommonHelper.DisplayNotificationMessagesHelper("At least one Product should be selected.", type, false, 5000);
        }
    },
    ChangeReviewImageStatus: function (reviewState) {
        var productIds = DynamicGrid.GetSelectedCheckBoxValue().toString();
        if (productIds.length > 0) {

            var type = "error";
            if (productIds.length > 0) {
                $("#SelectedProductIds").attr('value', productIds);
                $("#reviewState").attr('value', reviewState);
                $("#AssociateVendorProductImage").submit();
            }
        }
        else {
            CommonHelper.DisplayNotificationMessagesHelper("At least one Product Image should be selected.", type, false, 5000);
        }
    },
    ChangeProductStatus: function (reviewState) {
        $("#reviewState").attr('value', reviewState);
        $("#AssociateVendorProduct").submit();
    },
    AssociateSelectedHandler: function (inputTypeId, formId) {
        var productIds = DynamicGrid.GetSelectedCheckBoxValue().toString();
        var type = "error";
        if (productIds.length > 0) {
            $("#" + inputTypeId).attr('value', productIds);
            $("#" + formId).submit();
            localStorage.selectedchkboxItems = "";
        }
        else {
            CommonHelper.DisplayNotificationMessagesHelper("Please check an option displayed within the Add-On List below in order to continue.", type, false, 5000);
        }
    },
    PreviewTabHandler: function (data) {
        $("." + data + " li").not(this).removeClass("active");
        $(this).addClass("active");
    },
    ShowSelectedTab: function () {
        try {
            if (selectedTabMode == "Marketing") {
                $("#productmarketing").click();
            }
            else if (selectedTabMode == "ProductImage") {
                $("#productimage").click();
            }
            else if (selectedTabMode == "Preview") {
                $("#productpreview").click();
            }
            else if (selectedTabMode == "ProductFacet") {
                $("#productfacet").click();
            }
            else if (selectedTabMode == "AddOns") {
                $("#productAddOnslist").click();
            }
        }
        catch (err) { }
    },
    ValidateAttributes: function () {

        var formIsvalid = $("#frmmallProduct").valid();
        if (!formIsvalid) {
            return false;
        }
        var attributeIds = '';
        var isValid = true;
        $('select.ddlAttributeList').each(function () {
            var attributeId = $('#' + $(this).attr("id") + ' option:selected').val();
            var errorId = "val" + $(this).attr("id");
            $("#" + errorId).text('').removeClass("field-validation-error");
            $("#" + errorId).text('').removeClass("field-validation-error").hide();
            if (attributeId.length > 0) {
                attributeIds += attributeId + ',';
            }
            else {
                isValid = false;
                var errmsg = $('#' + $(this).attr("id") + ' option:selected').text() + " is required.";
                $("#" + errorId).text('').text(errmsg).addClass("field-validation-error");
                $("#" + errorId).text('').text(errmsg).addClass("field-validation-error").show();
            }
        });
        if (attributeIds.length > 0) {
            $("#ProductModel_ProductAttributes").val(attributeIds.slice(0, -1));
        }
        if (isValid == false) {
            return false;
        }

        $("#valSalePrice").removeClass("field-validation-error").hide();
        if ($("#ProductModel_VendorRetailPrice").val().trim().length < 1) {
            var error = $("#ProductModel_VendorRetailPrice").attr('data-val-regex');
            $("#valSalePrice").text(error).addClass("field-validation-error").show();
            return false;
        }

        return VendorProduct.BindVendorProductCategories();
        return true;
    },
    ClearValidation: function (ctrl) {
        var errorId = "val" + ctrl.id;
        $("#" + errorId).text('').removeClass("field-validation-error");
    },
    BindVendorProductCategories: function () {
        try {
            $("#valCategoryIds").removeClass("field-validation-error").hide();
            var categoryIds = "";
            $("#vendorproductcategories input[type='checkbox']:checked").each(function () {
                categoryIds = (categoryIds == "") ? $(this).attr('data-categoryid') : categoryIds + "," + $(this).attr('data-categoryid');
            });
            $("#vendorproductCategoryIds").attr('value', categoryIds);
            if (categoryIds.length < 1) {
                $("#valCategoryIds").text(emptyCategory).addClass("field-validation-error").show();
                return false;
            }
        }
        catch (err) { }
        return true;
    },
    SkuValidateAttributes: function () {
        var sku = $("#SKU").val();
        var qty = $("#QuantityOnHand").val();
        if (sku.length > 0 && qty.length > 0) {
            return VendorProduct.ValidateAttributes();
        }
    },
    ShowHideGiftCard: function () {
        var isGiftCard = $("#IsGiftCard").val();
        if (isGiftCard != null && isGiftCard.toLowerCase() == "true") {
            $("#divGiftCards").show();
        }
        else {
            $("#divGiftCards").hide();
        }
    }

}