﻿ManageSEO = {
    
    GetDefaultSetting: function (obj) {
        var portalId = $('#' + obj.id + ' option:selected').val();
        this.BindPortalSettings(portalId);
    },
    BindPortalSettings: function (portalId) {
        window.location.href = "/ManageSEO/GetSettings/" + portalId;
    }
   
}
