﻿/*
 | Controller: Facet
 | Views: This is a single view
 |
 | 2015; Znode, LLC.
*/
App.Facet = (function ($, Modernizr, App) {
    // INIT	
    var init = function () {
        _debug("facet.init()");
        bind.GetUnAssignedCategories();
        bind.CheckAssociateCategories();
    }

    // BINDINGS
    var bind = {
        GetUnAssignedCategories: function () {           
            $("#ddlCatalog").on("change", function () {
                var catalogId = $(this).val();
                App.Api.getUnAssignedCategoriesListByCatalogId(catalogId, function (response) {
                    _debug(response);
                    $("#associatedcategories").html(response);
                });
            });
        },
        CheckAssociateCategories: function () {
           
            $("#btnCreateFacetGroup").on("click", function () {
                try {
                    return Validate();
                }
                catch (err)
                {
                    return true;
                }
            });
        }
    }

    return {
        init: init
    };

}(jQuery, Modernizr, App));

// END module