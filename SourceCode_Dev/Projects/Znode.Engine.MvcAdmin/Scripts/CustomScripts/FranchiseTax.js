﻿var stateSelectedVal = '';
FranchiseTaxRule = {
    BindStateList: function (stateList, stateCountryCode) {
        App.Api.getFranchiseStateListByCountryCode(stateCountryCode, function (response) {
            $('#' + stateList).empty();
            for (var i = 0; i < response.length; i++) {
                $('#' + stateList).append("<option value='" + response[i].Value + "'>" + response[i].Text + "</option>");
            }
            if (response.length < 1) {
                $('#' + stateList).append("<option value=''></option>");
            }
            if (stateSelectedVal !== '') {
                $('#' + stateList).val(stateSelectedVal);
                var countyVal = $("#countyList").val();
                FranchiseTaxRule.BindCountyList("countyList", $('#' + stateList), countyVal);
                stateSelectedVal = '';
            }

            if ($('#' + stateList).val() === "0") {
                $('#countyList').empty();
                $('#countyList').append("<option value='0'>Apply to ALL Counties</option>");
            }
        });
    },

    BindCountyList: function (countyList, countyStateCode, selectedval) {
        var value = $(countyStateCode).val();
        App.Api.getFranchiseCountyListByStateCode(value, function (response) {
            $('#' + countyList).empty();
            for (var i = 0; i < response.length; i++) {
                $('#' + countyList).append("<option value='" + response[i].Value + "'>" + response[i].Text + "</option>");
            }
            if (response.length < 1) {
                $('#' + countyList).append("<option value=''></option>");
            }
            if (selectedval !== undefined)
                $("#countyList").val(selectedval);
        });
    },
}

$(document).ready(function () {
    stateSelectedVal = $("#stateList").val();
    var countryListName = $("#countryList").val();
    FranchiseTaxRule.BindStateList("stateList", countryListName);
});


