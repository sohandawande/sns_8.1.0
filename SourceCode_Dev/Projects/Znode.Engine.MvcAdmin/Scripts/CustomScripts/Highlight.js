﻿Highlight = {
    DisplayText: function () {
        $('#divHyperlink').hide();
        $('#divDisplayText').show().removeClass("hidden");
    },

    Hyperlink: function () {
        $('#divDisplayText').hide();
        $('#divHyperlink').show().removeClass("hidden");
    },

    UploadNewImage: function () {
        $('#editImageSettings').show().removeClass("hidden");
        $('#imgHighlight').removeClass("hidden");
        $('#hdnNoImage').val(false);
    },

    KeepCurrentImage: function () {
        $('#editImageSettings').addClass("hidden");
        $('#imgHighlight').removeClass("hidden");
        $('#hdnNoImage').val(false);
    },

    NoImage: function () {
        $('#editImageSettings').addClass("hidden");
        $('#imgHighlight').addClass("hidden");
        $('#hdnNoImage').val(true);
    },

    OverWriteFileName: function () {
        $('#NewImageFileName').prop('readonly', true)
    },

    UseFileName: function () {
        $('#NewImageFileName').prop('readonly', false)
    }
}
$(document).ready(function () {
    $("#displayText,#hyperlink").click(function () {
        if (this.id === "displayText") {
            $("#ShowPopupFlag").val(1);
        }
        else {
            $("#ShowPopupFlag").val(0);
        }
    })
})