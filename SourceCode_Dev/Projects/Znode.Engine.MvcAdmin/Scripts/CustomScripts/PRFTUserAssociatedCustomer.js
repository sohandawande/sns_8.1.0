﻿
PRFTUserAssociatedCustomer = {

    AssociatedCustomers: function (accountId) {        
        if (accountId > 0) {
            var customerAccountIds = "";

            $("#grid input[name=Checkbox]:checked").each(function () {
                customerAccountIds = customerAccountIds + $(this).attr('id').split('_')[1] + ",";
            });

            customerAccountIds = customerAccountIds.substr(0, customerAccountIds.length - 1);
            var type = "error";
            if (customerAccountIds.length > 0) {
                var url = "/Customers/SaveAssociatedCustomer/" + accountId;
                App.Api.userAssociatedCustomer(url, customerAccountIds, function (response) {
                    if (response.sucess) {
                        type = "success";
                        CommonHelper.DisplayNotificationMessagesHelper(response.message, type, response.isFadeOut, 5000);
                        var url = "/Customers/Manage/" + response.id + "?tabMode=CustomerMapping";
                        window.location.href = url;
                    }
                    CommonHelper.DisplayNotificationMessagesHelper(response.message, type, response.isFadeOut, 5000);
                });
            }
            else {
                CommonHelper.DisplayNotificationMessagesHelper("At least one Customer should be selected.", type, false, 5000);
            }
        }
    }
}






