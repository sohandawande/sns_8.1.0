﻿FranchiseAdmin = {
    BindProfile: function (fun) {
        $("#PortalId").unbind("change");
        $("#PortalId").change(function () {
            var portalId = $('#PortalId :selected').val();
            if (portalId === "" || portalId === null) {
                portalId = 0;
            }
            //start ajax request
            App.Api.getProfilesByPortalId(portalId, function (response) {
                var ddl = '<option value= "" selected="selected">All</option>';
                for (var i = 0; i < response.length; i++) {
                    var ProfileId = response[i].ProfileId;
                    var ProfileName = response[i].Name;
                    ddl += '<option value="' + ProfileId + '">' + ProfileName + '</option>';
                }
                $("#ProfileId").html(ddl);
                if (portalId !== 0) fun(true);
            });
        });
    },

    PreventSearchButton: function (fun) {
        var portalId = $('#PortalId :selected').val();
        if (portalId != "") {
            App.Api.getProfilesByPortalId(portalId, function (response) {
                var ddl = '<option value= "" selected="selected">All</option>';
                for (var i = 0; i < response.length; i++) {
                    var ProfileId = response[i].ProfileId;
                    var ProfileName = response[i].Name;
                    ddl += '<option value="' + ProfileId + '">' + ProfileName + '</option>';
                }
                $("#ProfileId").html(ddl);
                fun(true);
            });
        }
    },
}