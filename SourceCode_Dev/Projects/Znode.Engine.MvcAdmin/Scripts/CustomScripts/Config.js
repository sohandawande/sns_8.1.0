﻿GlobalVariables = {
    PaymentApplicationUrl: SetPaymentUrl(true) + "/",
    PaymentApplicationJSCall: SetPaymentUrl(true) + "/script/znodeapijs?gateway={0}&profileId={1}&twoCoUrl={2}",
    PaymentApplicationPayNowUrl: SetPaymentUrl(true) + "/script/Paynow?callback=?",
    PymentApplicationPaypalCall: SetPaymentUrl(true) + "/payment/paypal?callback=?",
    ZnodeDemoPaymentTwoCoUrl: SetPaymentUrl(false) + "/orders/twoco?paymentOptionId={0}",
    ZnodeSiteAdminUrl: SetPaymentUrl(false),
    ZnodeSiteAdminPaymentCancelUrl: SetPaymentUrl(false) + "/orders/create",
    ZnodeSiteAdminPaymentSuccessUrl: SetPaymentUrl(false) + "/orders/custom?paymentOptionId={0}",
    ZnodeFranchiseAdminUrl: SetPaymentUrl(false) + "/franchiseadmin",
    ZnodeFranchiseAdminPaymentCancelUrl: SetPaymentUrl(false) + "/franchiseadmin/franchiseorders/create",
    ZnodeFranchiseAdminPaymentSuccessUrl: SetPaymentUrl(false) + "/franchiseadmin/franchiseorders/custom?paymentOptionId={0}",
}

function SetPaymentUrl(isPayment) {
    var payment_Url = "http://localhost:56247";
    var admin_Url = "http://localhost:6766";
    return (isPayment) ? payment_Url : admin_Url;
}