﻿var App = {};


/*
| Global App Module
|
| This module is always loaded and contains global onready events
|
| Use the instant() method to execute actions inline when the script loads in <head>
| Use the onready() method to execute actions on page load
|
| 2014; Znode, Inc.
*/

App.global = (function ($, Modernizr, App) {

    // Local data
    var data = {
        menuUsed: false
    }

    // Instant method runs inline when this script loads
    var instant = function () {
        _debug("global.instant()");

    }

    // Onready method runs when document is loaded and ready
    var onready = function () {
        _debug("global.onready()");

        Gluten.init({ small: 601, medium: 850, large: 1120 }); // See Gluten library for reference
        bind.init();

        // Set the controller and action names
        var modules = $("body").data("controller").split(".");
        var action = $("body").data("view");

        // Loads modules based on controller and view
        // If init() methods are present, they will be run on load
        modules.forEach(function (module) {
            if (typeof App[module] !== 'undefined') {
                if (typeof App[module].init !== 'undefined') {
                    App[module].init();
                }

                if (typeof App[module][action] !== 'undefined') {
                    if (typeof App[module][action].init !== 'undefined') {
                        App[module][action].init();
                    }
                }
            }
        });

        // Refresh RWD bindings
        Gluten.refresh();

    };



    // LOCAL METHODS
    var local = {
        
    }


    // TYPEAHEAD
    // Available on every page through header search (medium+)
    var typeahead = {
        timer: false,
        start: function (input) {
            // Unset any started timers from previous keyup
            if (typeahead.timer != undefined) {
                clearTimeout(typeahead.timer);
            }

            // Set delay timer, won't fire ajax until timeout reached
            typeahead.timer = setTimeout(function () { typeahead.request(input); }, 300);
        },

        request: function (input) {
            var keyword = $(input).val();

            _debug("> typeahead request for " + keyword);

            // Only make a request if there is a string
            if (keyword != "") {
                App.Api.getSearchSuggestions(keyword, function (response) {
                    //var prevMatch = ""; // Clear prev matches
                    var matches = { "matches": [] }; // Reset matches set
                    var template = $("#tmpl-typeahead-results").html(); // Get inline template

                    // Loop through response
                    $.each(response, function (k, i) {
                        matches.matches.push({ "suffix": (i.CategoryName != '' ? " in " + i.CategoryName : ""), "product": i.ProductName, "term": encodeURIComponent(i.ProductName), "category": encodeURIComponent(i.CategoryName) });
                        prevMatch = i.ProductName;
                    });

                    if (matches.matches.length > 0) {
                        // Toss matches to the inline template
                        var render = Mustache.render(template, matches);

                        // Insert rendered HTML onto page
                        $("#typeahead-results").html(render).addClass("active");
                    } else {
                        $("#typeahead-results").removeClass("active"); // Hide overlay if empty search (when deleting)
                    }
                });
            } else {
                $("#typeahead-results").removeClass("active"); // Hide overlay if empty search (when deleting)
            }
        },

        close: function () {
            $("#typeahead-results").empty().removeClass("active");
        }
    }



    // BINDINGS
    var bind = {
        init: function () {
            _debug("global.bind.init()");

            // Reponsive bindings for header
            Gluten.rules([
            {
                selector: "#toggle-mobilemenu",
                event: "click.mobilemenu",
                sizes: "small",
                callback: function (e) {
                    e.preventDefault();

                    if (!data.menuUsed) {
                        data.menuUsed = true;
                        var html = $("#footer-shortcuts").html();
                        $("#mobile-menu-clone").append(html);
                    }

                    $("#mobile-menu").addClass("active");
                }
            },
            {
                selector: "#mobile-menu .mobile-menu-closetop, #mobile-menu .mobile-menu-closebottom",
                event: "click.closemobilemenu",
                sizes: "small",
                callback: function (e) {
                    e.preventDefault();
                    $("html, body").animate({ scrollTop: $('#layout-header').offset().top }, 250);
                    $("#mobile-menu").removeClass("active");
                }
            },
            {
                selector: "#header-search-input",
                event: "keyup.typeahead",
                sizes: "medium,large",
                callback: function (e) {
                    e.preventDefault();
                    typeahead.start(this);
                }
            },
            {
                selector: "#typeahead-results",
                live: ".close",
                event: "click.close-typeahead",
                sizes: "medium,large",
                callback: function (e) {
                    e.preventDefault();
                    typeahead.close();
                }
            }
            ]);


            // Click for "log off" in main menu, submits form with session token
            $(".account-logout").on("click", function (ev) {
                ev.preventDefault();
                $("#logout-form").submit();
            });


            // Generic toggle binding
            this.attachToggle();
        },

        // Generic element toggler binding
        attachToggle: function () {
            _debug("bind.attachToggle()");

            $(".toggle").on("click", function (e) {
                e.preventDefault();
                local.toggleElements($(this));
            });
        }
    }

    // Public
    return {
        instant: instant,
        onready: onready,
        data: data
    };

}(jQuery, Modernizr, App));

// END module

App.global.instant();

$(document).ready(function () {
    App.global.onready();
});
