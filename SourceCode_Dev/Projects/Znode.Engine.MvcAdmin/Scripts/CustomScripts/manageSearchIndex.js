﻿var SearchIndex = {
    Init: function () {

    },
    ViewIndexStatus: function (indexId) {
        App.Api.getSearchIndexStatus(indexId, function (result) {
            $("#search-index-status-container").html(result);
            $("#search-index-status-container").dialog({
                title: "Search Index Status",
                resizable: false,
                height: 450,
                width: 700,
                modal: true
            });
        });
        
        return false;
    }
}