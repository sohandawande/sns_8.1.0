﻿var _textboxId, _hiddenFieldId;
var _produtId, _productName;
var _callFunction;
Promotion = {

    Init: function (userType) {
        $("#ddlPromotionType").unbind("change")
        $("#ddlPromotionType").change(function () {
            Promotion.getPromotionTypeForm($(this).find("option:selected").val(), userType);
            Promotion.getCategoryListByPortalId(ddlCategory, $("#ddlPortal option:selected").val());
            Promotion.getCatalogListByPortalId(ddlCatalog, $("#ddlPortal option:selected").val());
        });

        $("#ddlPortal").unbind("change")
        $("#ddlPortal").change(function () {
            Promotion.getProfileListByPortalId(ddlProfile, $(this).find("option:selected").val());
            Promotion.getCategoryListByPortalId(ddlCategory, $(this).find("option:selected").val());
            Promotion.getCatalogListByPortalId(ddlCatalog, $(this).find("option:selected").val());
        });
    },

    ShowHideCouponInfo: function (userType) {
        var url = ""
        if ($('#CouponInd').prop("checked") == true) {
            if (userType == "FranchiseAdmin") {
                url = "/FranchiseAdmin/FranchisePromotion/GetCouponCodeForm";
            }
            else {
                url = "/Promotion/GetCouponCodeForm";
            }
            App.Api.getCouponCodeForm(url, function (response) {
                $("#Coupon-Container").html(response);
                Promotion.EnableCouponUrl(userType);
            });
        }
        else
            $("#Coupon-Container").html("");
        $("#CouponUrl-Container").html("");
    },

    EnableCouponUrl: function (userType) {
        var url = ""
        if ($('#EnableCouponUrl').prop("checked") == true) {
            if (userType == "FranchiseAdmin") {
                url = "/FranchiseAdmin/FranchisePromotion/GetEnableUrlForm";
            }
            else {
                url = "/Promotion/GetEnableUrlForm";
            }
            App.Api.getEnableUrlForm(url, function (response) {
                $("#CouponUrl-Container").html(response);
            });
        }
        else
            $("#CouponUrl-Container").html("");
    },

    ValidatePromotionAttributes: function () {
        var form = $("#frmPromotion")
        form.valid();
        $("#valDiscountPercent").text('').removeClass("field-validation-error").hide();
        $("#valCounponQuantity").text('').removeClass("field-validation-error").hide();
        $("#regexCouponCodeError").text('').removeClass("field-validation-error").hide();
        var couponQuantity = $("#txtCouponQuantity").val();
        var percent = $("#DiscountPercent").val();
        var couponCodeRegex = $("#txtCouponCode").attr("data-val-regex-pattern");
        var couponCode = $("#txtCouponCode").val();
        if (couponCode != "" && couponCode != null && couponCode != undefined && !CommonHelper.IsMatchRegularExpressionString(couponCode, couponCodeRegex)) {
            $("#regexCouponCodeError").text('').text('Enter valid coupon code.').addClass("field-validation-error").show();
            return false
    }

        if (percent != undefined) {
            if (percent.length < 1) {
                $("#valDiscountPercent").text('').text('Enter Discount Percent').addClass("field-validation-error").show();
                return false
            }
            else if (parseFloat(percent) <= 0) {
                $("#valDiscountPercent").text('').text('Enter Discount Percent between 0.01-100').addClass("field-validation-error").show();
                return false;
            }
            else if (parseFloat(percent) > 100) {
                $("#valDiscountPercent").text('').text('Enter Discount Percent between 0.01-100').addClass("field-validation-error").show();
                return false;
            }
        }

        if (couponQuantity != undefined) {
            if (couponQuantity.length < 1) {
                $("#valCounponQuantity").text('').text('Enter Coupon Quantity').addClass("field-validation-error").show();
                return false
            }
            else if (isNaN(parseInt(couponQuantity))) {
                $("#valCounponQuantity").text('').text('Enter valid quantity between 0 - 9999').addClass("field-validation-error").show();
                return false;
            }
        }
        return true;
    },

    RequireProductValidation: function (requireProduct) {
        $("#valRequiredProduct").text('').removeClass("field-validation-error").hide();

        if (requireProduct.length < 1) {
            $("#valRequiredProduct").text('').text('Select Product').addClass("field-validation-error").show();
            return false;
        }
    },

    ShowProductListPopUp: function (textboxId, hiddenFieldId, userType) {
        _textboxId = textboxId;
        _hiddenFieldId = hiddenFieldId;
        $("#divShowProduct").html('');
        $("#grid-container").remove();
        var url = ""
        if (userType == "FranchiseAdmin") {
            url = "/FranchiseAdmin/FranchisePromotion/GetProductList";
        }
        else {
            url = "/Promotion/GetProductList";
        }
        App.Api.getProductList(url, function (response) {
            $("#divShowProduct").html(response);
            $("#divShowProduct").dialog({
                title: "Search Product",
                resizable: false,
                modal: true,
                    create: function () { $(this).closest(".ui-dialog").addClass("ui-lg-popup promotion-popup");
            }
            });
        });
        _callFunction = function () {
        };
        return false;
    },

    StartEndDateValidation: function () {
        $("div#search-validation-summary").html("");
        var beginDate = $('[data-columnname="StartDate"]').val();
        var endDate = $('[data-columnname="EndDate"]').val();

        beginDate = CommonHelper.GetFormat(beginDate);
        endDate = CommonHelper.GetFormat(endDate);

        var firstDate = (isNaN(beginDate) ? Date.parse(beginDate) : eval(beginDate));
        var lastDate = (isNaN(endDate) ? Date.parse(endDate) : eval(endDate));

        if ((firstDate != null || firstDate != "") && (lastDate != null || lastDate != ""))
            if (!(lastDate == null || lastDate == "")) {
                if (firstDate > lastDate) {
                    $("div#search-validation-summary").html("End Date Should be greater than Begin Date.");
                    return false;
                } else {
                    return true;
                }
            }
    },

    PreventSearchSubmit: function () {
        if ($("div#search-validation-summary").html().length > 0) {
            return false;
        } else {
            return true;
        }
    },

    ShowSkuProductListPopUp: function (ddlSku, textboxId, hiddenFieldId) {
        $("#divShowProduct").html('');
        $("#grid-container").remove();
        var url = "/Promotion/GetProductList";
        App.Api.getProductList(url, function (response) {
            $("#divShowProduct").html(response);
            $("#divShowProduct").dialog({
                title: "Search Product",
                resizable: false,
                height: 700,
                width: 900,
                modal: true
            });


            _callFunction = function () {
                App.Api.getSkuListByProductId(_produtId, function (response) {
                    $("#callforprice-container").html(response);
                    $("#callforprice-container #" +hiddenFieldId).val(_produtId);
                    $("#callforprice-container #" +textboxId).val(_productName);
                });
            };
        });
        return false;
    },

    getPromotionTypeForm: function (promotionType, userType) {
        var url = ""
        if (userType == "FranchiseAdmin") {
            url = "/FranchiseAdmin/FranchisePromotion/GetPromotionTypeForm";
        }
        else {
            url = "/Promotion/GetPromotionTypeForm";
        }
        App.Api.getPromotionTypeForm(url, promotionType, function (response) {
            $("#PromotionTypeForm-container").html(response);
            Promotion.ShowHideCouponInfo(userType);
            Promotion.EnableCouponUrl(userType);
        });
    },

    getProfileListByPortalId: function (ddlProfile, PortalId) {
        if (PortalId == "") {
            PortalId = "0";
        }
        App.Api.getProfileListByPortalId(PortalId, function (response) {
            $('#' + ddlProfile.id).empty();
            $('#' + ddlProfile.id).append("<option value=''>All Profiles</option>");
            for (var i = 0; i < response.length; i++) {
                if (parseInt(response[i].Value) > 0) {
                    $('#' + ddlProfile.id).append("<option value='" + response[i].Value + "'>" + response[i].Text + "</option>");
                }
                else {
                    $('#' + ddlProfile.id).append("<option value='" + response[i].Value + "'>" + response[i].Text + "</option>");
                }
            }
        });
    },

    getCategoryListByPortalId: function (ddlCategory, PortalId) {
        if (PortalId == "") {
            PortalId = "0";
        }
        App.Api.getCategoryListByPortalId(PortalId, function (response) {
            $('#' + ddlCategory.id).empty();
            if (PortalId == "0") {
                $('#' + ddlCategory.id).append("<option value=''>All Categories</option>");
            }
            if (parseInt(response.length) == 0) {
                $('#' + ddlCategory.id).append("<option value=''>All Categories</option>");
            }
            else {
                for (var i = 0; i < response.length; i++) {
                    if (parseInt(response[i].Value) > 0) {
                        $('#' + ddlCategory.id).append("<option value='" + response[i].Value + "'>" + response[i].Text + "</option>");
                    }
                    else {
                        $('#' + ddlCategory.id).append("<option value='" + response[i].Value + "'>" + response[i].Text + "</option>");
                    }
                }
            }

        });
    },

    getCatalogListByPortalId: function (ddlCatalog, PortalId) {
        if (PortalId == "") {
            PortalId = "0";
        }
        App.Api.getCatalogListByPortalId(PortalId, function (response) {
            $('#' + ddlCatalog.id).empty();
            if (PortalId == "0") {
                $('#' + ddlCatalog.id).append("<option value=''>All Catalogs</option>");
            }
            if (parseInt(response.length) == 0) {
                $('#' + ddlCatalog.id).append("<option value=''>All Catalogs</option>");
            }
            else {
                for (var i = 0; i < response.length; i++) {

                    if (parseInt(response[i].Value) > 0) {
                        $('#' + ddlCatalog.id).append("<option value='" + response[i].Value + "'>" + response[i].Text + "</option>");
                    }
                    else {
                        $('#' + ddlCatalog.id).append("<option value=''>All Catalogs</option>");
                        $('#' + ddlCatalog.id).append("<option value='" + response[i].Value + "'>" + response[i].Text + "</option>");
                    }
                }
            }
        });
    },

    GridPaging: function () {
        $("#grid > tbody > tr > td > a").click(function () {
            var id = parseInt($(this).attr('href').replace('/', ''));
            var name = $(this).html();
            _produtId = id;
            _productName = name;
            _callFunction();
            $("#" +_hiddenFieldId).val(id);
            $("#" +_textboxId).val(name);
            $("#divShowProduct").dialog('close');
            $(".ui-dialog-titlebar-close").click();
            return false;
        });
    },

        EnableAllowMultipleCouponCheckbox: function (userType) {
        var url ="";
         if (userType == "FranchiseAdmin") {
                url = "/FranchiseAdmin/FranchisePromotion/GetMultipleCouponCheckboxEnabled";
         }
         else {
                url = "/Promotion/GetMultipleCouponCheckboxEnabled";
        }
        var selectedId = $('#ddlPortal').val();
        if (selectedId != "") {
            App.Api.getEnableMultipleCouponCheckBox(url, selectedId, function (response) {
            if (!response.enabled) {
                $("#multipleCoupon").hide();
            }
            else {
                $("#multipleCoupon").show();
            }
        });
    }
        else {
            $("#multipleCoupon").show();
        }
}
}



