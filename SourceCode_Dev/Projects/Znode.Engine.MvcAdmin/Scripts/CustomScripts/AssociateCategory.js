﻿AssociateCategory = {
    Init: function () {
        $("#grid > tbody > tr > td > a.z-edit").on("click", function () {
            var url = $(this).attr('href');

            App.Api.getProfilesAssociatedWithCategory(url, function (response) {
                $("#dvHideShowPartialView").html('');
                $("#dvHideShowPartialView").html(response)
                $("#dvHideShowPartialView").dialog({
                    title: "Category",
                    resizable: false,
                    modal: true,
                    create: function () { $(this).closest(".ui-dialog").addClass("ui-sm-popup"); }
                });
            });
            return false;
        });
    },

    BindCssList: function (cssList, cssThemeId) {
        if (cssThemeId.value == 0) {
            $('#' + cssList.id).empty();
            $('#' + cssList.id).append("<option value='0'>Same as store</option>");
            $('#' + cssList.id).append("<option value='1'>Default</option>");
            return;
        }
        App.Api.getCategoryCssListByThemeId(cssThemeId.value, function (response) {
            $('#' +cssList.id).empty();
            $('#' +cssList.id).append("<option value='0'>Same as store</option>");
            for (var i = 0; i < response.length; i++) {
                if (parseInt(response[i].Value) > 0) {
                    $('#' + cssList.id).append("<option value='" + response[i].Value + "'>" + response[i].Text + "</option>");
                }
            }
        });        
    },

    BindMasterPageList: function (masterPageList, cssThemeId) {
        App.Api.getCategoryMasterPageListByThemeId(cssThemeId, function (response) {
            $('#' + masterPageList.id).empty();
            for (var i = 0; i < response.length; i++) {
                if (parseInt(response[i].Value) > 0) {
                    var defaultValue = parseInt(response[i].Value) - 1;
                    $('#' + masterPageList.id).append("<option value='" + defaultValue + "'>Select Template</option>");
                    $('#' + masterPageList.id).append("<option value='" + response[i].Value + "'>" + response[i].Text + "</option>");
                }
                else {
                    $('#' + masterPageList.id).append("<option value='0'>Select Template</option>");
                    $('#' + masterPageList.id).append("<option value='" + response[i].Value + "'>" + response[i].Text + "</option>");
                }
            }
            if (response.length < 1) {
                $('#' + masterPageList.id).append("<option value='0'>Select Template</option>");
            }
        });
    },

    HideShowManageCategoryProfilePartialView: function (categoryId, categoryNodeId) {
        App.Api.getCategoryAssociatedProfilesData(categoryId, categoryNodeId, function (response) {
            $("#dvHideShowPartialView").html('');
            $("#dvHideShowPartialView").html(response);
            $("#dvHideShowPartialView").dialog({
                title: "Category Details",
                resizable: false,
                modal: true,
                create: function () { $(this).closest(".ui-dialog").addClass("ui-sm-popup"); }
            });
        });
    },

    HideManageCategoryProfile: function () {
        $("#dvHideShowPartialView").dialog('close');
    },

    ShowProductListPopUp: function (id) {

        $(id).dialog({
            resizable: false,
            height: 500,
            width: 1000,
            modal: true
        });
    },

    ShowCategoriesListPopUp: function (textboxId, hiddenFieldId) {
        App.Api.searchCategoryList(function (response) {
            $("#dvSearchCategory").html(response);
            $("#dvSearchCategory").dialog({
                title: "Search Categories",
                modal: true,
                create: function () { $(this).closest(".ui-dialog").addClass("ui-lg-popup search-product-container"); }
            });


        });
        return false;
    },

    ShowCategoriesListPopUpFranchiseAdmin: function (textboxId, hiddenFieldId) {
        App.Api.searchCategoryListFranchiseAdmin(function (response) {
            $("#dvSearchCategory").html(response);
            $("#dvSearchCategory").dialog({
                title: "Search Categories",
                modal: true,
                create: function () { $(this).closest(".ui-dialog").addClass("ui-lg-popup"); }
            });


        });
        return false;
    },

    ValidateDisplayOrderAndCategoryName: function () {
        var intRegex = new RegExp("[^0-9]");
        var displayOrderValue = $('#displayOrder').val();
        var categoryNameValue = $("#txtCategoryName").val();
        var returnVariable = true;
        if (displayOrderValue == null || displayOrderValue === undefined || displayOrderValue == "" && (categoryNameValue == null || categoryNameValue === undefined || categoryNameValue == "")) {
            $("#valDisplayOrder").text("Enter Display Order").addClass("field-validation-error").removeClass("field-validation-valid");
            $("#valCategoryName").text("Select Category").addClass("field-validation-error").removeClass("field-validation-valid");
            returnVariable = false;
            return returnVariable;
        }
        if (displayOrderValue == null || displayOrderValue === undefined || displayOrderValue == "") {
            $("#valDisplayOrder").text("Enter Display Order").addClass("field-validation-error").removeClass("field-validation-valid");
            returnVariable = false;
            return returnVariable;
        }
        if (categoryNameValue == null || categoryNameValue === undefined || categoryNameValue == "") {
            $("#valCategoryName").text("Select Category").addClass("field-validation-error").removeClass("field-validation-valid");
            returnVariable = false;
            return returnVariable;
        }
        return returnVariable;
    },
    ValidateDisplayOrderToAcceptOnlyNumbers: function () {
        var intRegex = new RegExp("[^0-9]");
        var displayOrderValue = $('#displayOrder').val();
        var returnVariable = true;
        if (intRegex.test(displayOrderValue)) {
            $("#valDisplayOrder").text("Enter a valid numeric display order").addClass("field-validation-error").removeClass("field-validation-valid");
            returnVariable = false;
            return returnVariable;
        }
        return returnVariable;
    },

    AutoComplete: function () {
        var userType = $('input:hidden[name=UserType]').val();
        $("#txtCategoryName").autocomplete({
            source: function (request, response) {
                var catalogId = $('#hdnCatalogId').val();
                var url = "";
                try {
                    if (userType == "FranchiseAdmin") {
                        url = "/FranchiseAdmin/FranchiseCatalog/AutoCompleteCategorySearch/";
                    }
                    else {
                        url = "/Catalog/AutoCompleteCategorySearch";
                    }
                    overlayforwaitblock();
                    App.Api.autoCompleteCategorySearch(url, request.term, catalogId, function (data) {
                        overlayforwaitnone();
                        response($.map(data, function (item) {
                            return { label: item.Name, value: item.CategoryId }
                        }));
                    });
                } catch (err) {
                }
            },
            select: function (event, ui) {
                $('#txtCategoryName').val(ui.item.label);
                $('#txtCategoryName').attr('data-CategoryId', ui.item.value);
                $('#hdnCategoryId').val(ui.item.value);
                overlayforwaitnone();
                return false;
            },
            search: function () {
                //reset every time a search starts.
                $('#txtCategoryName').attr('data-categoryId', 0);
                $('#hdnCategoryId').val(0);
            },
            messages: {
                noResults: "", results: ""
            }
        });
    }
}
$(document).ready(function () {
    AssociateCategory.AutoComplete();
})
