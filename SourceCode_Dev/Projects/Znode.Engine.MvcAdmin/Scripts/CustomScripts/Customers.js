﻿Customers = {

    Init: function () {
        $("#ddlStatus").unbind("change")
        $("#ddlStatus").change(function () {
            Customers.ReferralStatusChange($(this).find("option:selected").val());
        });
    },

    ReferralStatusChange: function (status) {        
        if (status === "A") {
            $("#trackingLink").show();
            $("#Nolink").hide();
        }
        else {
            $("#Nolink").show();
            $("#trackingLink").hide();
        }
    },

    CustomerDetails: function (enableCustomerPricing, grid, response) {
        if (enableCustomerPricing === "False") {

            $("#" + grid).each(function (e) {
                $('th:nth-child(3)').hide();
                $('td:nth-child(3)').hide();
            });
        }
        $("#Dynamic_Grid #grid thead tr").find('th:eq(0)').hide();
        $("#Dynamic_Grid #grid tbody tr").find('td:eq(0)').each(function () {
            $(this).hide();
            if ($(this).html() != undefined && $(this).html().trim() == "") {
                $(this).parent().find('.z-Enable').hide();
            }

        });
    },

    SetCommisionValidation: function () {
        referralCommissionType = $("#ddlReferralCommisionType").val();
        var commission = $("#ReferralCommission").val();
        if (referralCommissionType == "1") {
            if (parseFloat(commission) > 100) {
                $("#valReferralCommission").text('').text('Enter valid numbers only and Enter commission percentage upto 100').addClass("field-validation-error").show();
                return false
            }
            return true;
        }
        else if (referralCommissionType == "2") {
            if (parseFloat(commission) > 9999999) {
                $("#valReferralCommission").text('').text('Enter valid numbers only and Enter commission amount between 0.00- 9999999').addClass("field-validation-error").show();
                return false
            }
            return true;
        }
        return true;
    },

    GetAccountPaymentList: function (evt, control, userType) {
        evt.preventDefault();
        $("#customers-customeraffiliate").closest('li').addClass("active");       
        $("#referralcommissionlist").closest('li').removeClass("active");
        $("#accountpaymentlist").closest('li').addClass("active");

        var ReferralCommissionType = $("#ReferralCommissionType").val();
        var AccountId = $("#AccountId").val();
        var url = "";

        if (userType == "FranchiseAdmin") {
            url = "/FranchiseAdmin/FranchiseCustomer/GetAccountPaymentList";
            }
            else {
            url = "/Customers/GetAccountPaymentList";
            }
        App.Api.GetAccountPaymentList(url, ReferralCommissionType, AccountId, function (res) {
            $("#panel-accountpaymentlist").html(res);
            localStorage.setItem('selectedtab', "#customers-customeraffiliate");
        });
    },

    GetReferralCommissionList: function (evt, control,userType) {
        evt.preventDefault();
        $("#customers-customeraffiliate").closest('li').addClass("active");
       
        $("#accountpaymentlist").closest('li').removeClass("active");

        $("#referralcommissionlist").closest('li').addClass("active");

        var AccountId = $("#AccountId").val();
        var url = "";
        if (userType == "FranchiseAdmin") {
            url = "/FranchiseAdmin/FranchiseCustomer/GetReferralCommissionList";
            }
            else {
            url = "/Customers/GetReferralCommissionList";
            }
        App.Api.GetReferralCommissionList(url, AccountId, function (res) {
            $("#panel-accountpaymentlist").html(res);
            localStorage.setItem('selectedtab', "#customers-customeraffiliate");
        });
    },

    AccountPaymentValidation: function () {
     $("#valPaymentDate").removeClass("field-validation-error");
        var date = $("#PaymentDate").val();
        var paymentDate = CommonHelper.GetFormat(date)
        var date = (isNaN(paymentDate) ? Date.parse(paymentDate) : eval(paymentDate));
        if (date > new Date()) {
            $("#valPaymentDate").text("Receive date should not be greater than current date").addClass("field-validation-error");
            $("#valPaymentDate").show();
            return false;
        }      
    },
}

$(document).ready(function () {
    Customers.Init();
});



