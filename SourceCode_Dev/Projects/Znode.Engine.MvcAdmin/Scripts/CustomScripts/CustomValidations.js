﻿$.validator.addMethod("genericcompare", function (value, element, params) {
    var propelename = params.split(",")[0];
    var operName = params.split(",")[1];
    if (params == undefined || params == null || params.length == 0 ||
    value == undefined || value == null || value.length == 0 ||
    propelename == undefined || propelename == null || propelename.length == 0 ||
    operName == undefined || operName == null || operName.length == 0)
        return true;
    var valueOther = $(propelename).val();


    value = getMonth(value);
    valueOther = getMonth(valueOther);

    var val1 = (isNaN(value) ? Date.parse(value) : eval(value));
    var val2 = (isNaN(valueOther) ? Date.parse(valueOther) : eval(valueOther));

    if (operName == "GreaterThan")
        return val1 > val2;
    if (operName == "LessThan")
        return val1 < val2;
    if (operName == "GreaterThanOrEqual")
        return val1 >= val2;
    if (operName == "LessThanOrEqual")
        return val1 <= val2;
});
function getMonth(date) {
    var _brouser = window.navigator.userAgent;
    var msie = _brouser.indexOf("MSIE");

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
        return date = date.split('-')[2] + "/" + date.split('-')[1] + "/" + date.split('-')[0];
    }
    else if (_brouser.indexOf("Safari") > -1) {
        return date;
    } else {
        if (date.indexOf('-') >= 0) {
            date = date.split('-')[0] + "/" + date.split('-')[1] + "/" + date.split('-')[2];
        }
    }
    return date;
}

$.validator.unobtrusive.adapters.add("genericcompare",
["comparetopropertyname", "operatorname"], function (options) {
    options.rules["genericcompare"] = "#" +
    options.params.comparetopropertyname + "," + options.params.operatorname;
    options.messages["genericcompare"] = options.message;
});