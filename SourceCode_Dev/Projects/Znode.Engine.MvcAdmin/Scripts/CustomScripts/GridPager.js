﻿var WebgridHelper = {
    GridUpdateHandler: function (data) {
        $("#grid-container").html(data);
        DynamicGrid.SetSortOrder();
        WebgridPagerHelper.UpdateHandler();
        $("#grid .grid-header th a").unbind("click");
        $("#grid .grid-header th a").click(function (e) {
            WebgridPagerHelper.SortingHandler(e, this);
        });
        $("#pageSizeList").unbind("change");
        $("#pageSizeList").change(function () {
            WebgridPagerHelper.SelectedPageSize();
        });
        $('#pagerTxt').unbind("keyup");
        $('#pagerTxt').keyup(function (e) {
            if (e.keyCode == 13) {
                WebgridPagerHelper.UrlHandler();
            }
        });
    }
};

var WebgridPagerHelper = {
    UpdateHandler: function () {
        this.SetArrows();
        this.PreviousAndNextUpdateHandler();
        this.SetPagingInput();
        this.PagingHandler();
        this.SelectedValueHandler();
    },
    Init: function () {
        WebgridPagerHelper.UpdateHandler();
        var pageCount = PageCount;
        if (pageCount == 0) {
            $('#pageSizeList').attr('disabled', '');
            $('#pagerTxt').attr('disabled', '');
        }

        $('#pagerTxt').unbind("keyup");
        $('#pagerTxt').keyup(function (e) {
            if (e.keyCode == 13) {
                //$(this).trigger("enterKey");
                WebgridPagerHelper.UrlHandler();
            }
        });

        $("#pageSizeList").unbind("change");
        $("#pageSizeList").change(function () {
            WebgridPagerHelper.SelectedPageSize();
        });
        $("#grid .grid-header th a").unbind("click");

        $("#grid .grid-header th a").click(function (e) {
            WebgridPagerHelper.SortingHandler(e, this);
        });
    },
    SetArrows: function (url) {
        var dir = $('#dir').val();
        var col = $('#col').val();
        var sortCol = "sort=" + col;

        var header = null;
        $('th a').each(function () {
            var href = $(this).attr('href');
            if (href.indexOf(sortCol) != -1) {
                header = $(this);
                return false;
            }
        });

        var newUrl = new jurl(url);
        var sortDir = newUrl.getQueryParameter(SortDirFieldName);

        if (header != null) {
            if (sortDir != null && col != "" && typeof col != typeof undefined && col != null) {

                if (dir == 'Ascending') {
                    header.removeClass('Descending');
                    header.addClass('Ascending');
                    header.html(header.text() + '<i class="sorting-arrow">▲</i>');
                    //header.href = header.href.replace("DESC", "ASC");
                }
                if (dir == 'Descending') {
                    header.removeClass('Ascending');
                    header.addClass('Descending');
                    header.html(header.text() + '<i class="sorting-arrow">▼</i>');
                    //header.href = header.href.replace("ASC", "DESC");
                }
            }
        }
    },
    PreviousAndNextUpdateHandler: function () {
        var MaxPages = PageCount;
        var currentPageNumber = PageIndex + 1;

        if (currentPageNumber <= 1) {
            $('#previousPage').on('click', false);
            $('#previousPage').addClass('DisableArrow');
        }
        else {
            $('#previousPage').off('click', false);
            $('#previousPage').removeClass('DisableArrow');
        }

        if (currentPageNumber >= MaxPages) {
            $('#nextPage').on('click', false);
            $('#nextPage').addClass('DisableArrow');
        }
        else {
            $('#nextPage').off('click', false);
            $('#nextPage').removeClass('DisableArrow');
        }
    },
    SetPagingInput: function () {
        var currentPageNumber = PageIndex + 1;
        var MaxPages = PageCount;

        $("#pagerTxt").attr('value', currentPageNumber);

        if (MaxPages == 1) {
            $("#pageCountLabel").html('Page');
        }
        else {
            $("#pageCountLabel").html("Pages");
        }
    },
    GetRedirectUrl: function () {
        var redirecturl = $("#grid th a:eq(1)").attr('href');
        if (redirecturl === undefined && $("#grid").closest(".tab-container").find('li[class=active]').length > 0) {
            redirecturl = $("#grid").closest(".tab-container").find('li[class=active]').find('a').attr('href');
            var paramVal = $("#grid").closest(".tab-container").find('li[class=active]').find('a').attr('data-queryparam');
            if (paramVal !== undefined)
                redirecturl += "?" + paramVal;
        }
        else if (redirecturl !== undefined)
            redirecturl = redirecturl.substring(0, $("#grid th a:eq(1)").attr('href').indexOf('sort') - 1);
        if (redirecturl === undefined || redirecturl === "")
            redirecturl = window.location.href;
        else
            redirecturl = window.location.protocol + "//" + window.location.host + redirecturl; //$("#grid th a:eq(1)").attr('href').substring(0, $("#grid th a:eq(1)").attr('href').indexOf('sort') - 1);//window.location.href;
        return redirecturl
    },

    PagingHandler: function () {
        $("#nextPage").off("click");
        $("#nextPage").on('click', function (e) {
            e.preventDefault();
            var MaxPages = PageCount;
            var currentPageNumber = PageIndex + 1;
            var requestedPage = parseInt(currentPageNumber) + 1;
            var url = WebgridPagerHelper.GetRedirectUrl();

            if (!(requestedPage <= parseInt(MaxPages))) {
                requestedPage = currentPageNumber;
            }

            var newUrl = new jurl(url);
            newUrl.setQueryParameter(RecordPerPageFieldName, PageSize);
            newUrl.setQueryParameter(PageFieldName, requestedPage);
            if (Sort != null) {
                newUrl.setQueryParameter(SortFieldName, Sort);
                newUrl.setQueryParameter(SortDirFieldName, SortDir);
            }
            WebgridPagerHelper.pagingUrlHandler(newUrl.build());
        });

        $("#previousPage").off("click");
        $("#previousPage").on('click', function (e) {

            e.preventDefault();
            var MaxPages = PageCount;
            var currentPageNumber = PageIndex + 1;
            var requestedPage = parseInt(currentPageNumber) - 1;
            var url = WebgridPagerHelper.GetRedirectUrl(); //window.location.protocol + "//" + window.location.host + $("#grid th a:eq(1)").attr('href').substring(0, $("#grid th a:eq(1)").attr('href').indexOf('sort') - 1);//window.location.href;

            if (requestedPage <= 0) {
                requestedPage = currentPageNumber;
            }

            var newUrl = new jurl(url);
            newUrl.setQueryParameter(RecordPerPageFieldName, PageSize);
            newUrl.setQueryParameter(PageFieldName, requestedPage);
            if (Sort != null) {
                newUrl.setQueryParameter(SortFieldName, Sort);
                newUrl.setQueryParameter(SortDirFieldName, SortDir);
            }
            WebgridPagerHelper.pagingUrlHandler(newUrl.build());

        });

        $("#first").off('click');
        $("#first").on('click', function (e) {
            e.preventDefault();
            var currentPageNumber = PageIndex + 1;
            var requestedPage = 1;
            var url = WebgridPagerHelper.GetRedirectUrl(); //window.location.protocol + "//" + window.location.host + $("#grid th a:eq(1)").attr('href').substring(0, $("#grid th a:eq(1)").attr('href').indexOf('sort') - 1);//window.location.href;

            if (requestedPage <= 0) {
                requestedPage = currentPageNumber;
            }

            var newUrl = new jurl(url);
            newUrl.setQueryParameter(RecordPerPageFieldName, PageSize);
            newUrl.setQueryParameter(PageFieldName, requestedPage);
            if (Sort != null) {
                newUrl.setQueryParameter(SortFieldName, Sort);
                newUrl.setQueryParameter(SortDirFieldName, SortDir);
            }
            WebgridPagerHelper.pagingUrlHandler(newUrl.build());
        });

        $("#last").off('click');
        $("#last").on('click', function (e) {
            e.preventDefault();
            var currentPageNumber = PageIndex + 1;
            var requestedPage = PageCount;
            var url = WebgridPagerHelper.GetRedirectUrl(); //window.location.protocol + "//" + window.location.host + $("#grid th a:eq(1)").attr('href').substring(0, $("#grid th a:eq(1)").attr('href').indexOf('sort') - 1);// window.location.href;

            if (requestedPage <= 0) {
                requestedPage = currentPageNumber;
            }

            var newUrl = new jurl(url);
            newUrl.setQueryParameter(RecordPerPageFieldName, PageSize);
            newUrl.setQueryParameter(PageFieldName, requestedPage);
            if (Sort != null) {
                newUrl.setQueryParameter(SortFieldName, Sort);
                newUrl.setQueryParameter(SortDirFieldName, SortDir);
            }
            WebgridPagerHelper.pagingUrlHandler(newUrl.build());
        });
    },
    SortingHandler: function (e, control) {
        e.preventDefault();
        e.stopPropagation();
        var requestedPage = parseInt($("#pagerTxt").val());
        var url = e.currentTarget.attributes[1].value;

        if (!window.location.origin) {
            window.location.origin = window.location.protocol + "//" + window.location.host;
        }

        url = window.location.origin + url;
        var browserUrl = new jurl(url);
        var pageNumber = PageIndex;
        var sortDir = SortDir;
        var anchorUrl = $(control).attr('href');

        if (pageNumber != null) {
            anchorUrl = anchorUrl + "&" + PageFieldName + "=" + 1;
        }

        var absoluteUrl = window.location.origin + anchorUrl;
        var newUrl = new jurl(absoluteUrl);
        newUrl.setQueryParameter(RecordPerPageFieldName, PageSize);
        newUrl.setQueryParameter(PageFieldName, requestedPage);
        WebgridPagerHelper.pagingUrlHandler(newUrl.build());
    },
    pagingUrlHandler: function (newUrl) {
        var formData = $("#searchform").serialize();
        $.ajax({
            type: "POST",
            url: newUrl,
            data: formData,
        }).success(function (html) {
            WebgridHelper.GridUpdateHandler(html);
            WebgridPagerHelper.SetArrows(newUrl);
        });
    },
    UrlHandler: function () {
        var MaxPages = PageCount;
        var url = WebgridPagerHelper.GetRedirectUrl(); //window.location.protocol + "//" + window.location.host + $("#grid th a:eq(1)").attr('href').substring(0, $("#grid th a:eq(1)").attr('href').indexOf('sort') - 1); //window.location.href;
        var requestedPage = parseInt($("#pagerTxt").val());
        if (!(requestedPage > 0 && requestedPage <= parseInt(MaxPages))) {
            requestedPage = 1;
        }

        var newUrl = new jurl(url);
        newUrl.setQueryParameter(RecordPerPageFieldName, PageSize);
        newUrl.setQueryParameter(PageFieldName, requestedPage);
        WebgridPagerHelper.pagingUrlHandler(newUrl.build());
    },
    SelectedPageSize: function () {
        var recordPerPage = $('#pageSizeList').val();
        var url = WebgridPagerHelper.GetRedirectUrl(); //window.location.protocol + "//" + window.location.host + $("#grid th a:eq(1)").attr('href').substring(0, $("#grid th a:eq(1)").attr('href').indexOf('sort') - 1); //window.location.href;
        var newUrl = new jurl(url);
        newUrl.setQueryParameter(RecordPerPageFieldName, recordPerPage);
        newUrl.setQueryParameter(PageFieldName, 1);
        WebgridPagerHelper.pagingUrlHandler(newUrl.build());

    },
    SelectedValueHandler: function () {
        if (localStorage.getItem("selectedchkboxItems") != "") {
            var checkBoxCount = $(".grid-row-checkbox").length;
            $(".grid-row-checkbox").each(function () {
                WebgridPagerHelper.SetSelectedChechboxValue($(this).attr('id'));
            });
        }
    },
    SetSelectedChechboxValue: function (chkboxId) {

        var checkBoxCount = $(".grid-row-checkbox").length;
        var checkBoxCheckedCount = $(".grid-row-checkbox:checked").length;

        if (checkBoxCount === checkBoxCheckedCount) {
            $("#check-all").prop('checked', true);
        }
        else {
            $("#check-all").prop('checked', false);
        }

        selectedIds = JSON.parse(localStorage.getItem("selectedchkboxItems"));
        for (var items = 0; items < selectedIds.length; items++) {
            if (selectedIds[items] === chkboxId) {
                $("#grid #" + chkboxId + "").prop('checked', true);
            }
        }
    }
}

$(document).ready(function () {
    if ($("#grid-container").length > 0)
        WebgridPagerHelper.Init();
});