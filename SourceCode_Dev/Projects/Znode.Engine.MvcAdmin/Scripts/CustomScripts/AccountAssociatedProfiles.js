﻿AccountAssociatedProfiles = {
    
    AssociatedProfiles: function (accountId) {
        if (accountId > 0) {
            var profileIds = "";

            $("#grid input[name=Checkbox]:checked").each(function () {
                profileIds = profileIds + $(this).attr('id').split('_')[1] + ",";
            });

            profileIds = profileIds.substr(0, profileIds.length - 1);
            var type = "error";
            if (profileIds.length > 0) {
                var url = "/Customers/SaveAssociatedProfile/" + accountId ;
                App.Api.accountAssociatedProfile(url, profileIds, function (response) {
                    if (response.sucess) {
                        type = "success";
                        CommonHelper.DisplayNotificationMessagesHelper(response.message, type, response.isFadeOut, 5000);
                        var url = "/Customers/Manage/" + response.id + "?tabMode=Profiles";
                        window.location.href = url;
                    }
                    CommonHelper.DisplayNotificationMessagesHelper(response.message, type, response.isFadeOut, 5000);
                });
            }
            else {
                CommonHelper.DisplayNotificationMessagesHelper("At least one Product should be selected.", type, false, 5000);
            }
        }
    }
}