﻿/***************************************************************
*********Region for global variables, Do not delete *********/
var PageIndex, PageCount, PageSize, RecordPerPageFieldName, PageFieldName, Sort, SortDir, SortFieldName, SortDirFieldName;
var toggleMessage, toggleTitle, RefreshLocationOndelete = false;
/****************************************************************/

var checkVal = 0;
CommonHelper = {
    BindDeleteConfirmDialog: function (title, message, deleteUrl) {
        this.ShowPopUp(title, message, deleteUrl);
        return false;
    },
    BlockHtmlTagForTextBox: function () {
        /* validated all text box in project*/
        $(':input').not('.AllowHtml').bind("paste keypress change", function (e) {
            if ($(this).val().indexOf("~") != -1) {
                var _inputValue = CommonHelper.Removetildslashfromstring($(this).val(), "~");
                $(this).val(_inputValue);
            }
            if ($(this).val().indexOf("<") != -1) {
                var _inputValue = CommonHelper.Removetildslashfromstring($(this).val(), "<");
                $(this).val(_inputValue);
            }
            if ($(this).val().indexOf(">") != -1) {
                var _inputValue = CommonHelper.Removetildslashfromstring($(this).val(), ">");
                $(this).val(_inputValue);
            }
            /*new validation*/
            var key = [e.keyCode || e.which];
            if (key[0] != undefined) {
                var keychar = String.fromCharCode([e.keyCode || e.which]).toLowerCase();
                if ((key == null) || (key == 0) || (key == 126) || (key == 60) || (key == 62)) {
                    return false;
                }
            }
        });

        /* validated all text box in project*/
        $(':input').not('.AllowHtml').bind("paste", function (e) {
            if ($(this).attr('data-datype') === "Int32" || $(this).attr('data-datype') === "Decimal") {
                return false;
            }
        });

        $(":input").not('.AllowHtml').on("keypress", function (e) {
            if (e.which === 32 && !this.value.length)
                e.preventDefault();
        });
    },
    Removetildslashfromstring: function (str, char) {
        var notildslash = "";
        var newstr = str.split(char);
        for (var i = 0; i < newstr.length; i++) {
            notildslash += newstr[i];
        }
        return notildslash;
    },
    ShowPopUp: function (title, message, url) {
        if ($("BODY").find("div#popup_container").length == 0) {
            this.CreatePopUp(title);
        }

        $("#popup_title").html(title);
        if (toggleMessage === undefined || toggleMessage === '') {
            $("#popup_message").html(message);
        }
        else {
            $("#popup_message").html(toggleMessage);
        }

        $("#popup_container").dialog({
            resizable: true,
            title: title,
            modal: true,
            stack: true,
            width: 400,
            buttons: {
                "OK": function () {
                    $(this).dialog("close");
                    App.Api.deleteRecordById(url, function (response) {
                        var type = "error";
                        if (response.sucess) {
                            type = "success";
                            if (parseInt($("#grid tbody tr").length) === 1) {
                                $("#pagerTxt").val(parseInt($("#pagerTxt").val()) - 1);
                            }
                            if (RefreshLocationOndelete)
                                window.location.reload();
                            else
                                WebgridPagerHelper.UrlHandler();
                        }
                        CommonHelper.DisplayNotificationMessagesHelper(response.message, type, response.isFadeOut, 5000);
                    });
                },
                Cancel: function () {
                    $(this).dialog("close");
                }
            }
        });
    },
    GetNoRecordHtml: function () {
        return "<div class=\"col-sm-12 no-record nopadding\"><p>No Record Found !!!</p></div>";
    },
    CreatePopUp: function (title) {
        var popup = '<div id="popup_container" class="ConfirmPopup" style="display:none;">' +
                        '<div class="PopupHeder ClearFix">' +
                        '</div>' +
                        '<div id="popup_content" class="PopupContent ClearFix">' +
                          '<div id="popup_message"></div>' +
                        '</div>' +
            '</div>';
        $("BODY").append(popup);
    },
    LoadLayout: function () {
        overlayforwaitnone();
        this.CommonControl();
        this.DisplayNotificationMessages();
        this.BlockHtmlTagForTextBox();
        this.BindPrintOption();
        //$(localStorage.getItem('selectedmenu')).closest('ul').closest('li').addClass('current-page');
    },
    LoadOuterLayout: function () {
        this.DisplayNotificationMessages();
        this.BlockHtmlTagForTextBox();
    },
    CommonControl: function () {
        //for showing selected menu                     
        $(document).ready(function () {
            var str = localStorage.getItem('selectedheadermenu');
            if (str != null) {
                str = str.toLowerCase();
            }
            var status = 0;
            if (str === '' || str == null) {
                $("#nav-menu .sub-menu:eq(0)").addClass("current-page");
            }
            else {
                $("#nav-menu .sub-menu").each(function () {
                    if (str.indexOf($(this).attr('href').toLowerCase()) >= 0) {
                        $(this).addClass("current-page");
                        status = 1;
                    }
                });
                if (status === 0) {
                    $("#nav-menu li a").each(function () {
                        if (str.indexOf($(this).attr('href').toLowerCase()) >= 0) {
                            $("li a.current-page").removeClass("current-page");
                            $(this).closest('ul').closest('li').find('a:eq(0)').addClass("current-page");
                        }
                    });
                }
            }
        })

        //Top Header Clone on Scroll
        $(document).ready(function () {
            var $header = $("header"),
                $clone = $header.before($header.clone().addClass("clone"));

            $(window).on("scroll", function () {
                var fromTop = $(window).scrollTop();
                $("body").toggleClass("down", (fromTop > 65));
            });
            if (!navigator.userAgent.match(/Trident\/7\./)) {
                $('.table-responsive').addClass('scroll-default');
            }
        });

    },
    DisplayNotificationMessages: function () {
        var element = $(".MessageBoxContainer");
        if (element.length) {
            var msgObj = element.data('message');
            if (msgObj !== "") {
                this.DisplayNotificationMessagesHelper(msgObj.Message, msgObj.Type, msgObj.IsFadeOut, msgObj.FadeOutMilliSeconds);
            }
        }
    },
    DisplayNotificationMessagesHelper: function (message, type, isFadeOut, fadeOutMilliSeconds) {
        var element = $(".MessageBoxContainer");
        $(".MessageBoxContainer").removeAttr("style");
        var closeBtnHtml = "<div><span onclick='CommonHelper.CloseMessageNotificationContainer(this);' title='Close' class='close right glyphicon glyphicon-remove-circle'></span></div>";
        $(window).scrollTop(0);
        $(document).scrollTop(0);
        if (element.length) {
            if (message !== "" && message != null) {

                element.html("<div class='col-sm-12 message-box alert'><p>" + message + closeBtnHtml + "</p></div></div>");

                switch (type) {
                    case "success":
                        {
                            element.find('div').addClass('alert-success');
                            break;
                        }
                    case "error":
                        {
                            element.find('div').addClass('alert-danger');
                            break;
                        }
                    default:
                        {
                            element.find('div').addClass('alert-info');
                        }
                }

                if (isFadeOut === "" || isFadeOut == null || typeof isFadeOut === "undefined") isFadeOut = true;
                if (fadeOutMilliSeconds === "" || fadeOutMilliSeconds == null || typeof fadeOutMilliSeconds === "undefined") fadeOutMilliSeconds = 10000;

                if (isFadeOut) {
                    setTimeout(function () {
                        element.fadeOut().empty();
                    }, fadeOutMilliSeconds);
                }
            }
        }
    },
    CloseDisplayBox: function () {
        var $target = $(".message-box");
        $target.slideUp('normal', function () {
            $target.remove();
        });
    },
    CloseMessageNotificationContainer: function (messageContainer) {
        $(messageContainer).parent("div").parent("div").parent("div").hide();
    },
    CloseMessageContainerOnTab: function () {
        $("#MessageBoxContainerId").hide();
    },
    CheckGiftCardCreateCheckBox: function () {
        if ($("#EnableToCustomerAccount").length > 0) {
            if ($($("#EnableToCustomerAccount")[0]).is(":checked")) {
                $("#ShowAccountId").removeClass("hidden");
            } else {
                $("#ShowAccountId").addClass("hidden");
                $("#AccountId").val('');
            }
        }
    },
    ValidateGiftCardAttributes: function () {
        $("#valAccountId").text('').removeClass("field-validation-error");
        $("#valExpirationDate").text('').removeClass("field-validation-error");
        var returnVariable = true;
        if ($("#ExpirationDate").val() != "") {
            today = new Date();
            var expirationDate = $("#ExpirationDate").val();
            var date = new Date(expirationDate);
            var enteredYear = date.getFullYear();
            var currentYear = today.getFullYear();
            var enteredMonth = date.getMonth();
            var currentMonth = today.getMonth();
            var enteredDay = date.getDay();
            var currentDay = today.getDay();
            date = Date.parse(expirationDate);
            if (currentYear == enteredYear && enteredMonth == currentMonth && enteredDay == currentDay) {
                returnVariable = CommonHelper.ValidateAccountId();
            }
            else if (date < today) {
                $("#valExpirationDate").text("Expiration date must be greater than current date.").addClass("field-validation-error");
                $("#valExpirationDate").show();
                returnVariable = CommonHelper.ValidateAccountId();
                returnVariable = false;
            }
            else {
                $("#valExpirationDate").hide();
                returnVariable = CommonHelper.ValidateAccountId();
            }
        }
        else {
            returnVariable = CommonHelper.ValidateAccountId();
        }
        return returnVariable;
    },
    ValidateAccountId: function () {
        if ($($("#EnableToCustomerAccount")[0]).is(":checked")) {
            var accountId = $("#AccountId").val();
            if (accountId.length < 1) {
                $("#valAccountId").text("Enter valid Account ID").addClass("field-validation-error");
                $("#valAccountId").show();
                return false;
            }
            else {
                $("#valAccountId").hide();
                return true;
            }
        }

    },
    ShowTextBox: function () {
        if ($('#EnableToCustomerAccount').prop("checked") == true) {
            $('#ShowAccountId').removeClass("hidden");
        } else {
            $('#ShowAccountId').addClass("hidden");
            $("#AccountId").val('');
        }
    },
    ShowRecurringBillingOptions: function () {
        if ($('#EnableRecurringBilling').prop("checked") == true) {
            $('#ShowBillingOptions').removeClass("hidden");
        } else {
            $('#ShowBillingOptions').addClass("hidden");
            $("#BillingAmount").val('');
        }
    },
    CheckAddOnRecurringBillingCheckBox: function () {
        if ($("#EnableRecurringBilling").length > 0) {
            if ($($("#EnableRecurringBilling")[0]).is(":checked")) {
                $("#ShowBillingOptions").removeClass("hidden");
            } else {
                $("#ShowBillingOptions").addClass("hidden");
                $("#BillingAmount").val('');
            }
        }
    },
    ValidateAddOnValueAttributes: function () {
        $("#valWeight").text('').removeClass("field-validation-error");
        var returnVariable = true;
        if ($("#ShippingRuleTypeId").val() == '2') {
            var weight = $("#Weight").val();
            if (weight < 1) {
                $("#valWeight").text("This Shipping Type requires that Weight be greater than 0").addClass("field-validation-error");
                $("#valWeight").show();
                returnVariable = CommonHelper.ValidateBillingAmount();
                returnVariable = false;
            }
            else {
                $("#valWeight").hide();
                returnVariable = CommonHelper.ValidateBillingAmount();
            }
        }
        return returnVariable;
    },
    ValidateBillingAmount: function () {
        $("#valBillingAmount").text('').removeClass("field-validation-error");
        if ($($("#EnableRecurringBilling")[0]).is(":checked")) {
            var billingAmount = $("#BillingAmount").val();
            if (billingAmount.length < 1) {
                $("#valBillingAmount").text("Enter a recurring billing amount").addClass("field-validation-error");
                $("#valBillingAmount").show();
                return false;
            }
            else {
                $("#valBillingAmount").hide();
                return true;
            }
        }
    },
    Tab_Change_Handler: function ($this) {
        var queryParam = $($this).attr("data-queryparam");
        var targetItemId = $($this).attr("data-targetitemid");
        var url = $($this).attr("href");
        var ignorGridCheck = false;
        try {
            ignorGridCheck = $($this).attr("data-isignorgrid");
        }
        catch (ex) {
            ignorGridCheck = false;
        }
        App.Api.getTabDetails(url, queryParam, function (response) {
            if (response.length > 0) {
                var activeTabclassName = "active";
                var activePanelClassName = "in active";
                $("." + activeTabclassName).removeClass(activeTabclassName);
                $("." + activePanelClassName).removeClass(activePanelClassName);
                $("#" + targetItemId).addClass(activePanelClassName);
                $($this).parent("li").addClass("active");
                try {
                    if (ignorGridCheck) {
                        $("#" + targetItemId).html(response);
                    }
                    else {
                        $("#" + targetItemId).html("<div id='grid-container'></div>");
                        $("#grid-container").html(response);
                    }
                    WebgridPagerHelper.Init();
                    DynamicGrid.SetSortOrder();
                }
                catch (ex) {
                    WebgridPagerHelper.Init();
                    DynamicGrid.SetSortOrder();

                }
            }
        });


        return false;
    },
    DisplayTabHandler: function () {
        $(".nav-tabs li").not(this).removeClass("active");
        $(this).addClass("active");
    },
    ShowImageButton: function () {

        if ($('#rdoKeepCurrent').prop("checked") == true) {
            $("#image-div").hide();
        }
        else if ($('#rdoUploadNew').prop("checked") == true) {
            $("#image-div").show();
        }
    },
    AssociateProductCategory: function (productId, userType) {
        if (productId > 0) {
            var categoryIds = DynamicGrid.GetSelectedCheckBoxValue().toString();
            var type = "error";
            if (categoryIds.length > 0) {
                var url = "";
                if (userType == "FranchiseAdmin") {
                    url = "/FranchiseAdmin/FranchiseProduct/AssociateProductCategories/" + productId;
                }
                else {
                    url = "/Product/AssociateProductCategories/" + productId;
                }
                App.Api.associateProductCategory(url, categoryIds, function (response) {
                    if (response.sucess) {
                        type = "success";
                        var url = "";
                        if (userType == "FranchiseAdmin") {
                            url = "/FranchiseAdmin/FranchiseProduct/Manage/" + response.id;
                        }
                        else {
                            url = "/Product/Manage/" + response.id;
                        }

                        window.location.href = url;
                    }
                    CommonHelper.DisplayNotificationMessagesHelper(response.message, type, response.isFadeOut, 5000);
                });
            }
            else {
                CommonHelper.DisplayNotificationMessagesHelper("At least one Category should be selected.", type, false, 5000);
            }
        }
    },
    AssociateBundleProduct: function (productId, userType) {
        if (productId > 0) {
            var productIds = DynamicGrid.GetSelectedCheckBoxValue().toString();

            var type = "error";
            if (productIds.length > 0) {
                var url = "";
                if (userType == "FranchiseAdmin") {
                    url = "/FranchiseAdmin/FranchiseProduct/AssociateBundleProduct/" + productId;
                }
                else {
                    url = "/Product/AssociateBundleProduct/" + productId;
                }
                App.Api.associateBundleProduct(url, productIds, function (response) {
                    if (response.sucess) {
                        type = "success";
                        var url = "";
                        if (userType == "FranchiseAdmin") {
                            url = "/FranchiseAdmin/FranchiseProduct/Manage/" + response.id;
                        }
                        else {
                            url = "/Product/Manage/" + response.id;
                        }
                        window.location.href = url;
                    }
                    CommonHelper.DisplayNotificationMessagesHelper(response.message, type, response.isFadeOut, 5000);
                });
            }
            else {
                CommonHelper.DisplayNotificationMessagesHelper("At least one Product should be selected.", type, false, 5000);
            }
        }
    },
    BindImageControl: function (data) {
        $('.thumb').show();

        if ($(data).attr("value").toLowerCase() == "currentimage") {
            $("#imgUploader").hide();
            $('#hdnNoImage').val(false);
        }
        else if ($(data).attr("value").toLowerCase() == "newimage") {
            $("#imgUploader").show();
            $('#hdnNoImage').val(false);
        }
        else {
            $('.thumb').hide();
            $("#imgUploader").hide();
            $('#hdnNoImage').val(true);
        }
    },
    BindFacetsList: function (data, productId) {
        var facetGroupId = $(data).val();
        if (productId > 0 && facetGroupId > 0) {
            App.Api.bindFacetListByFacetGroupId(productId, facetGroupId, function (response) {
                $("#chklistFacets").html(response);
            });
        }
    },
    OrderStatusTrackingNumHideShow: function (ShippingTypeName, LabelFedExTrackingNo, LabelUPSTrackingNo, LabelUSPSTrackingNo, txtTrackingNumber, dvTrackingNumber) {
        var shippingTypeName = ShippingTypeName;
        if (shippingTypeName.toLowerCase().indexOf("fedex") >= 0) {
            $("#" + txtTrackingNumber).text(LabelFedExTrackingNo);
        }
        else if (shippingTypeName.toLowerCase().indexOf("ups") >= 0) {
            $("#" + txtTrackingNumber).text(LabelUPSTrackingNo);
        }
        else if (shippingTypeName.toLowerCase().indexOf("usps") >= 0) {
            $("#" + txtTrackingNumber).text(LabelUSPSTrackingNo);
        }
        if (shippingTypeName.toLowerCase().indexOf("fedex") >= 0 || shippingTypeName.toLowerCase().indexOf("ups") >= 0 || shippingTypeName.toLowerCase().indexOf("usps") >= 0) {
            $("#" + dvTrackingNumber).css("display", "block");
        }
    },
    ResetAccountPassword: function (title, message, url) {
        this.ShowResetPasswordPopUp(title, message, url);
        return false;
    },
    ShowResetPasswordPopUp: function (title, message, url) {
        if ($("BODY").find("div#popup_container").length == 0) {
            this.CreatePopUp(title);
        }

        $("#popup_title").html(title);
        $("#popup_message").html(message);

        $("#popup_container").dialog({
            resizable: true,
            title: title,
            modal: true,
            stack: true,
            width: 400,
            buttons: {
                "OK": function () {
                    $(this).dialog("close");
                    window.location.href = url;
                },
                Cancel: function () {
                    $(this).dialog("close");
                }
            }
        });
    },
    ShowHideStoreList: function (ctrl) {
        if (ctrl != '') {
            if (ctrl.checked) {
                $(".chkStoresList").find("input[type=checkbox]").each(function () {
                    $(this).prop('checked', false);
                });
                $(".chkStoresList").hide();
            } else {
                $(".chkStoresList").show();
            }
        }
    },
    SubmitForm: function (formid, validate) {
        //PRFT Custom Code: Start
        var externalID = $("#externalId").val();       
        if ($("#IsB2B").is(":checked") && (externalID === "" || externalID === 'undefined' || externalID.length < 1)) {
            $(".field-validation-error").text("External ID is required");
            return false;
        }
        //PRFT Custom Code: Start

        var bSuccess = true;
        if (typeof validate !== 'undefined' || validate != null) {
            bSuccess = validate();
        }
        if (bSuccess || typeof bSuccess === 'undefined') {
            $('#' + formid).on("submit", function () {
                var checkStatus;
                if (typeof $('#' + formid).valid == 'function') {
                    checkStatus = $('#' + formid).valid();
                }
                else {
                    checkStatus = $('#' + formid).valid;
                }
                ////for showing loader
                //if (checkStatus == true) { 
                //    startLoading('Please Wait...');
                //}
            });
            $('#' + formid).submit();
        }
    },
    StartDateEndDateValidation: function () {
        $("div#search-validation-summary").html("");
        var beginDate = $('[data-columnname="CreateDate"]').val();
        var endDate = $('[data-columnname="EndDate"]').val();

        beginDate = CommonHelper.GetFormat(beginDate);
        endDate = CommonHelper.GetFormat(endDate);

        var firstDate = (isNaN(beginDate) ? Date.parse(beginDate) : eval(beginDate));
        var lastDate = (isNaN(endDate) ? Date.parse(endDate) : eval(endDate));

        if ((firstDate != null || firstDate != "") && (lastDate != null || lastDate != ""))
            if (!(lastDate == null || lastDate == "")) {
                if (firstDate > lastDate) {
                    $("div#search-validation-summary").html("End Date Should be greater than Begin Date.");
                    return false;
                } else {
                    return true;
                }
            }
    },
    VadidateDate: function (startDateControl, endDateControl) {
        $("div#search-validation-summary").html("");
        var beginDate = new Date(startDateControl.val());
        var endDate = new Date(endDateControl.val());
        if ((beginDate != null || beginDate != "") && (endDate != null || endDate != ""))
            if (!(endDate == null || endDate == "")) {
                if (beginDate > endDate) {
                    $("div#search-validation-summary").html("End Date Should be greater than Begin Date.");
                    return false;
                } else {
                    return true;
                }
            }
    },
    GridSearchDateValidation: function (startDateControl, endDateControl) {
        $("div#search-validation-summary").html("");
        beginDate = CommonHelper.GetFormat(startDateControl);
        endDate = CommonHelper.GetFormat(endDateControl);

        var firstDate = (isNaN(beginDate) ? Date.parse(beginDate) : eval(beginDate));
        var lastDate = (isNaN(endDate) ? Date.parse(endDate) : eval(endDate));

        if ((firstDate != null || firstDate != "") && (lastDate != null || lastDate != ""))
            if (!(lastDate == null || lastDate == "")) {
                if (firstDate > lastDate) {
                    $("div#search-validation-summary").html("End Date Should be greater than Begin Date.");
                    return false;
                } else {
                    return true;
                }
            }
    },

    GetFormat: function (date) {
        var _brouser = window.navigator.userAgent;
        var msie = _brouser.indexOf("MSIE");

        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
            return date = date.split('-')[2] + "/" + date.split('-')[1] + "/" + date.split('-')[0];
        }
        else if (_brouser.indexOf("Safari") > -1) {
            return date;
        } else {
            if (date.indexOf('-') >= 0) {
                date = date.split('-')[0] + "/" + date.split('-')[1] + "/" + date.split('-')[2];
            }
        }
        return date;
    },

    IsMatchRegularExpressionString: function (str, regax) {
        if (str.match(regax)) {
            return true;
        }
        else {
            return false;
        }
    },
    PrintHandler: function (e) {
        e.preventDefault();
        window.print();
    },
    BindPrintOption: function () {
        $(".PrintLink").off('click').on('click', CommonHelper.PrintHandler);
    },
    PreventSearchSubmit: function () {

        if ($("div#search-validation-summary").html().length > 0) {
            return false;
        } else {
            return true;
        }
    },
    ValidatePerminssionsAndRoles: function (RoleAndPermissionErrorMsg, RolesErrorMsg, StoresErrorMsg) {
        if ($("input[name='StoreList']").is(":checked") == false && $("input[name='Roles']").is(":checked") == false) {
            this.DisplayNotificationMessagesHelper(RoleAndPermissionErrorMsg, "error", false, 5000);
            return false;
        } else if ($("input[name='Roles']").is(":checked") == false) {
            this.DisplayNotificationMessagesHelper(RolesErrorMsg, "error", false, 5000);
            return false;
        } else if ($("input[name='StoreList']").is(":checked") == false) {
            this.DisplayNotificationMessagesHelper(StoresErrorMsg, "error", false, 5000);
            return false;
        }
        return true;
    },
    MallAssociateBundleProduct: function (productId) {
        if (productId > 0) {
            var productIds = DynamicGrid.GetSelectedCheckBoxValue().toString();
            var type = "error";
            if (productIds.length > 0) {
                var url = "/MallAdmin/MallProduct/AssociateBundleProduct/" + productId;
                App.Api.associateBundleProduct(url, productIds, function (response) {
                    if (response.sucess) {
                        type = "success";
                        var url = "/MallAdmin/MallProduct/Manage/" + response.id;
                        window.location.href = url;
                    }
                    CommonHelper.DisplayNotificationMessagesHelper(response.message, type, response.isFadeOut, 5000);
                });
            }
            else {
                CommonHelper.DisplayNotificationMessagesHelper("At least one Product should be selected.", type, false, 5000);
            }
        }
    },
    SetNewCaseName: function () {
        var caseName = $('#CaseStatusId :selected').text();
        $("#CaseNewStatusName").val(caseName);
    },
    ManageProductStatusinGrid: function () {
        var index;
        $("#grid .grid-header").find("th").each(function () {
            var txt = $(this).text();
            if ($.trim(txt) === "Status") {
                index = $(this).index();
            }
        })
        $("#grid tbody tr").find('td:eq(' + index + ')').each(function () {
            var txt = $(this).text();
            $(this).html(txt);
        })
    },
    ManageUnAssociatedAddOnValueNamesinGrid: function () {
        var index;
        $("#grid .grid-header").find("th").each(function () {
            var txt = $(this).text();
            if ($.trim(txt) === "Add On Value") {
                index = $(this).index();
            }
        })
        $("#grid tbody tr").find('td:eq(' + index + ')').each(function () {
            var txt = $(this).text();
            $(this).html(txt);
        })
    },
    ManageImageStatusinGrid: function () {
        var index;
        $("#grid .grid-header").find("th").each(function () {
            var txt = $(this).text();
            if ($.trim(txt) === "Status") {
                index = $(this).index();
            }
        })
        $("#grid tbody tr").find('td:eq(' + index + ')').each(function () {
            var txt = $(this).text();
            var htmlString = "";
            switch (txt) {
                case "10":
                    htmlString = "<div class='img-circle symbol-circle yellow'></div>";
                    break;
                case "20":
                    htmlString = "<div class='img-circle symbol-circle green'></div>";
                    break;
                case "30":
                    htmlString = "<div class='img-circle symbol-circle red'></div>";
                    break;
                case "40":
                    htmlString = "<div class='img-circle symbol-circle grey'></div>";
                    break;
                default:
                    htmlString = "<div class='img-circle symbol-circle red'></div>";
                    break;
            }
            $(this).html(htmlString);
        })
    }

}

// Global debug
var enableDebug = true;

function _debug(s) {
    if (enableDebug) {
        if (typeof console === "undefined") {
            //console = { log: function(s){ alert(s); } }
        } else {
            console.log(s);
        }
    }
}

// In-window scrolling, uses jQuery
function scrollToTop(speed, selector) {
    if (!speed) {
        speed = 300;
    }

    if (!selector) {
        selector = "body";
    }

    $("html, body").animate({
        scrollTop: $(selector).offset().top
    }, speed);
}


// jQuery plugin for disabled attribute on element
(function ($) {
    $.fn.toggleDisabled = function () {
        return this.each(function () {
            var $this = $(this);
            if ($this.attr('disabled')) {
                $this.removeAttr('disabled');
            } else {
                $this.attr('disabled', 'disabled');
            }
        });
    };
})(jQuery);

//Nav Menu selected tab 
$(document).on("click", "#nav-menu li a", function () { localStorage.setItem('selectedheadermenu', $(this).attr('href')); localStorage.setItem('selectedtab', "#"); });

$(document).on("click", "#nav-menu .sub-menu", function () { localStorage.setItem('selectedheadermenu', $(this).attr('href')); localStorage.setItem('selectedtab', "#"); });

$(document).on("click", ".home-page-container a", function () { localStorage.setItem('selectedheadermenu', $(this).attr('href')); localStorage.setItem('selectedtab', "#"); });

$(document).on("click", ".report-link", function () { localStorage.setItem('selectedheadermenu', '/MyReports/Index'); localStorage.setItem('selectedtab', "#"); });

$(document).on("click", ".home-link", function () { localStorage.setItem('selectedheadermenu', $(this).attr('href')); localStorage.setItem('selectedtab', "#"); });

$(document).on("click", ".log-out", function () { localStorage.setItem('selectedheadermenu', ''); localStorage.setItem('selectedtab', "#"); });


//Reset selected tab 
$(document).on("click", "#redirect-back", function () { localStorage.setItem('selectedtab', "#"); });

//Loader on ajax request
$(document).ajaxStart(function () { overlayforwaitblock(); }); $(document).ajaxSuccess(function () { overlayforwaitnone(); });

/*code for Overlay*/
function overlayforwaitnone() { $("#backgroundddivWait").hide(); } function overlayforwaitblock() { $("#backgroundddivWait").show(); }
/*code for Overlay*/

$(document).ajaxError(function (e, jqxhr, settings, exception) {
    overlayforwaitnone();
    e.stopPropagation();
    if (jqxhr != null) {
        var data = $.parseJSON(jqxhr.responseText);
        if (data.ErrorCode == 101 && data.ReturnUrl != "") {

            if (data.Area != undefined && data.Area != "") {
                window.location.href = "/" + data.Area + "/Account/Login?returnUrl=" + data.ReturnUrl;
            }
            else {
                window.location.href = "/Account/Login?returnUrl=" + data.ReturnUrl;
            }
        }
    }
});

String.format = function () {
    var theString = arguments[0];

    for (var i = 1; i < arguments.length; i++) {
        var regEx = new RegExp("\\{" + (i - 1) + "\\}", "gm");
        theString = theString.replace(regEx, arguments[i]);
    }

    return theString;
}



