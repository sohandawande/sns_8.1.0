﻿var FranchiseShipping = {
    Init: function () {
        $("#ddlShippingRuleList").change(function () {
            var a = ($(this).val());
            FranchiseShipping.getShippingTypeView(a);
        });
    },

    getShippingTypeView: function (a) {
        App.Api.getFranchiseShippingTypeView(a, function (res) {
            $("#shipping-type-container").html(res);
        });
    },

    ClearValidationMessage: function () {
        $("#valDisplayName").on("blur", function () {
            if ($("#valDisplayName").val() != "")
                $("#valDisplayNameErr").html("");
        });
        $("#valInternalCode").on("blur", function () {
            if ($("#valInternalCode").val() != "")
                $("#valInternalCodeErr").html("");
        });
    },

    ValidationAttributesForShippingOption: function () {
        if ($('#valDisplayName').is(':visible') && $('#valInternalCode').is(':visible')) {
            flag = false;

            var displayName = $("#valDisplayName").val();
            if (displayName.length < 1) {
                $("#valDisplayNameErr").html("Enter Shipping option name");
                flag = false;
            }
            else {
                $("#valDisplayNameErr").html("");
                flag = true;
            }

            var internalCode = $("#valInternalCode").val();
            if (internalCode.length < 1) {
                $("#valInternalCodeErr").html("Enter Shipping code");
                flag = false;
            }
            else {
                $("#valInternalCodeErr").html("");
                flag = true;

            }
            return flag;
        }
    },

    BindServiceList: function (ddlShippingServiceCodeList, serviceShippingTypeId) {

        var id = $("#ddlShippingTypeList").val();
        if (id !== undefined) {
            App.Api.getFranchiseServiceListByShippingTypeId(id, function (response) {
                $('#' + ddlShippingServiceCodeList).empty();
                for (var i = 0; i < response.length; i++) {
                    if (parseInt(response[i].Value) > 0) {
                        var defaultValue = parseInt(response[i].Value) - 1;

                        $('#' + ddlShippingServiceCodeList).append("<option value=" + response[i].Value + ">" + response[i].Text + "</option>");
                    }
                    else {

                        $('#' + ddlShippingServiceCodeList).append("<option value=" + response[i].Value + ">" + response[i].Text + "</option>");
                    }
                }
                var selectedValue = $("#ShippingServiceCodeId").val();
                if (parseInt(selectedValue) !== 0) {
                    $('#' + ddlShippingServiceCodeList).val(selectedValue);
                }

                if (response.length < 1) {
                    $('#' + ddlShippingServiceCodeList);
                }
            });
        }
    },


    checkvalues: function (e, control) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
            // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
        var pattern = new RegExp(/^\d{0,30}(\.\d{0,1})?$/);
        if (!pattern.test($(control).val())) {
            e.preventDefault();
            return false;
        }
    },

    GetShippingOption: function (shippingOption) {

        switch ($.trim(shippingOption)) {
            case "ZnodeShippingCustom":
                $("#profile-content").show();
                $("#displayname-content").show();
                $("#internalcode-content").show();
                $("#service-content").hide();
                $("#handlingcharge-content").show();
                $("#countries-content").show();

                break;
            case "ZnodeShippingFedEx":
                $("#profile-content").show();
                $("#displayname-content").hide();
                $("#internalcode-content").hide();
                $("#service-content").show();
                $("#handlingcharge-content").show();
                $("#countries-content").show();
                break;
            case "ZnodeShippingUps":
                $("#profile-content").show();
                $("#displayname-content").hide();
                $("#internalcode-content").hide();
                $("#service-content").show();
                $("#handlingcharge-content").show();
                $("#countries-content").hide();
                break;
            case "ZnodeShippingUsps":
                $("#profile-content").show();
                $("#displayname-content").show();
                $("#internalcode-content").show();
                $("#service-content").hide();
                $("#handlingcharge-content").show();
                $("#countries-content").show();
                break;
        }
    },
    bindShippingOption: function () {
    },

    CheckRangeValue: function () {
        var valueBaseCost = $("#valBaseCost").val();
        var valuePerItemCost = $("#valPerItemCost").val();
        var valueLowerLimit = $("#valLowerLimit").val();
        var valueUpperLimit = $("#valUpperLimit").val();
        if (valuePerItemCost != undefined) {
            if (parseFloat(valueBaseCost) > 214748 || parseFloat(valuePerItemCost) > 214748) {
                CommonHelper.DisplayNotificationMessagesHelper("Base cost and Per item cost should not be greater than 214748.", "error", false, 5000);
                return false;
            }
        }
        else {
            if (parseFloat(valueBaseCost) > 214748) {
                CommonHelper.DisplayNotificationMessagesHelper("Base cost should not be greater than 214748.", "error", false, 5000);
                return false;
            }
        }
        if (parseFloat(valueLowerLimit) > 999999 || parseFloat(valueUpperLimit) > 999999) {
            CommonHelper.DisplayNotificationMessagesHelper("Lower limit or Upper limit should not be greater than 999999.", "error", false, 5000);
            return false;
        }
    }
}
$("#ddlShippingTypeList").on("change", function () {

    FranchiseShipping.GetShippingOption($(this).val());
});

$(document).ready(function () {

    FranchiseShipping.Init();
    var a = $("#ddlShippingRuleList").val();
    FranchiseShipping.getShippingTypeView(a);
    FranchiseShipping.ClearValidationMessage();
    var shipId = $("#ddlShippingTypeList").val();
    FranchiseShipping.GetShippingOption(shipId);
    var shippingTypeId = $("#ddlShippingTypeList").val();
    FranchiseShipping.BindServiceList('ddlShippingServiceCodeList', shippingTypeId);
});


