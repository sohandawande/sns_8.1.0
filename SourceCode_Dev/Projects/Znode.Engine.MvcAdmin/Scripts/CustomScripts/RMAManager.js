﻿RMAManager = {
    Load: function (mode) {
        if (mode.toLowerCase() != "view") {
            $('#rmagrid').find('th')[1].innerHTML = "<label><input id='chkall' name='chkall' class='chkboxstyle' type='checkbox' onchange='RMAManager.SelectAll("+'""'+");'><span class='lbl padding-8'></span></label>";
            $("#ddlRmaQuantity").closest("tr").parent().find("tr").each(function () {
                quantityDropdown = $(this).find("#ddlRmaQuantity")
                RMAManager.GetRMAItemTotal(quantityDropdown, mode);
            });
        } else {
            var subTotal = 0.0;
            var tax = 0.0;
            var pricetotal = 0.0;
            $(".Quantity").each(function () {
                var quantity = $(this).html();
                var row = $(this).closest("tr");
                var price = $(row).find(".price").text();
                var total = Number(price.replace(/[^0-9\.]+/g, "")) * parseInt(quantity);
                subTotal = subTotal + total;
                lineItemTax = Number($(row).find(".tax").text().replace(/[^0-9\.]+/g, ""));
                tax = tax + lineItemTax;
                App.Api.getConvertedDecimalValues(total, function (response) {
                    $(row).find(".total").text(response._currencyValue);
                });
            });
            pricetotal = subTotal + tax;
            if (mode.toLowerCase() != "view") {
                App.Api.getConvertedDecimalValues(subTotal, function (response) {
                    $("#dynamic-sub-total").text(response._currencyValue);
                });
                App.Api.getConvertedDecimalValues(tax, function (response) {
                    $("#dynamic-tax-amount").text(response._currencyValue);
                });
                App.Api.getConvertedDecimalValues(pricetotal, function (response) {
                    $("#dynamic-total").text(response._currencyValue);
                });
            }
        }
    },

    GetGridData: function () {
        var items = new Array();
        var orderId = $("#OrderId").val();
        var subTotal = 0;
        $('#rmagrid tr').not('thead tr').each(function () {
            var checkbox = $(this).find('input:checkbox')[0];
            if (checkbox && checkbox.checked) {
                var orderLineItemId = $(this).find(".orderLineItem").text();
                var quantity = $(this).find("#ddlRmaQuantity :selected").val();
                var reasonForReturnId = $(this).find("#ddlReasonForReturn :selected").val();
                var price = $(this).find(".price").text();
                items.push({ OrderId: orderId, OrderLineItemId: orderLineItemId, Quantity: quantity, Price: Number(price.replace(/[^0-9\.]+/g, "")), ReasonForReturnId: reasonForReturnId });
                subTotal += Number(price.replace(/[^0-9\.]+/g, "")) * parseInt(quantity);
            }
        });
        $("#hdnSubtotal").val(subTotal);
        return items;
    },

    CreateRMARequest: function () {
        var type = "error";
        var checkedItems = $('#rmagrid tr').filter(':has(:checkbox:checked)').find('td');
        if (checkedItems.length < 1) {
            CommonHelper.DisplayNotificationMessagesHelper(errorRMAItem, type, "false", 5000);
            return false;
        }
        var items = RMAManager.GetGridData();

        var tax = $("#dynamic-tax-amount").html();
        tax = tax.substring(1, tax.length);

        var subTotal = $("#hdnSubtotal").val();

        var total = Number(subTotal.replace(/[^0-9\.]+/g, "")) + Number(tax.replace(/[^0-9\.]+/g, ""));

        var requestModel = {
            RMARequestId: 0,
            RequestNumber: $("#requestNumber").val(),
            TaxCost: tax,
            SubTotal: subTotal,
            Total: total,
            Comments: $("#Comments").val(),
            Flag: "",
            RMARequestItems: items
        };
        App.Api.createRMARequest(requestModel, function (response) {
            if (response.sucess) {
                type = "success";
                window.location.href = "/rmamanager/rmalist"
            }
            CommonHelper.DisplayNotificationMessagesHelper(response.message, type, response.isFadeOut, 5000);
        });
    },

    GetRMAItemTotal: function (obj, mode) {
        var row = $(obj).closest("tr");
        var qty = $(row).find("#ddlRmaQuantity :selected").val();
        var price = $(row).find(".price").text();
        var total = Number(price.replace(/[^0-9\.]+/g, "")) * parseInt(qty);
        App.Api.getConvertedDecimalValues(total, function (response) {
            $(row).find(".total").text(response._currencyValue);
        });
        RMAManager.CalculateSubTotal(mode);
    },

    CalculateSubTotal: function (mode) {
        var subTotal = 0;
        var subTax = 0;
        var total = 0;

        $('#rmagrid tr').not('thead tr').each(function () {
            var checkbox = $(this).find('input:checkbox')[0];
            if (checkbox && checkbox.checked) {
                var quantity = $(this).find("#ddlRmaQuantity :selected").val();
                var price = $(this).find(".price").text();
                subTotal += Number(price.replace(/[^-0-9\.]+/g, "")) * quantity;
                var taxPrice = $(this).find(".tax").val();
                subTax += Number(taxPrice.replace(/[^0-9\.]+/g, "")) * parseInt(quantity);
            }
        });
        total = subTax + subTotal;
        if (mode.toLowerCase() != "view") {
            App.Api.getConvertedDecimalValues(subTotal, function (response) {
                $("#dynamic-sub-total").html(response._currencyValue);
            });
            App.Api.getConvertedDecimalValues(subTax, function (response) {
                $("#dynamic-tax-amount").html(response._currencyValue);
            });
            App.Api.getConvertedDecimalValues(total, function (response) {
                $("#dynamic-total").html(response._currencyValue);
            });
        }
        RMAManager.CheckAllSelected();
    },

    SelectAll: function (mode) {
        var chkAll = $("#rmagrid #chkall")[0];
        if (chkAll && chkAll.checked) {
            var items = $('#rmagrid input:checkbox[name=IsReturnable]');
            for (var i = 0; i < items.length; i++) {
                items[i].checked = true;
            }
        }
        else {
            $('#rmagrid input:checkbox[name=IsReturnable]').attr('checked', false);
        }
        RMAManager.CalculateSubTotal(mode);
    },

    CheckAllSelected: function () {
        var items = $('#rmagrid input:checkbox[name=IsReturnable]');
        var checkedItems = $('#rmagrid input:checkbox[name=IsReturnable]:checked');
        if (checkedItems.length != items.length) {
            $("#rmagrid #chkall")[0].checked = false;
        }
        else {
            $("#rmagrid #chkall")[0].checked = true;
        }
    },

    GetGiftCardGridData: function () {
        var items = new Array();
        var orderId = $("#OrderId").val();
        var subTotal = 0;
        $('#rmagrid tr').not('thead tr').each(function () {
            var checkbox = $(this).find('input:checkbox')[0];
            if (checkbox && checkbox.checked) {
                var orderLineItemId = $(this).find(".orderLineItem").text();
                var quantity = $(this).find("#ddlRmaQuantity :selected").val();
                var reasonForReturnId = 0;
                var price = $(this).find(".price").text();
                items.push({ OrderId: orderId, OrderLineItemId: orderLineItemId, Quantity: quantity, Price: Number(price.replace(/[^0-9\.]+/g, "")), ReasonForReturnId: reasonForReturnId });
                subTotal += Number(price.replace(/[^0-9\.]+/g, "")) * parseInt(quantity);
            }
        });
        $("#hdnSubtotal").val(subTotal);
        return items;
    },

    IssueGiftCard: function () {
        var type = "error";
        var checkedItems = $('#rmagrid tr').not('thead tr').filter(':has(:checkbox:checked)').find('td');
        if (checkedItems.length < 1) {
            CommonHelper.DisplayNotificationMessagesHelper(errorRMAItem, type, "false", 5000);
            return false;
        }
        var items = RMAManager.GetGiftCardGridData();

        var orderLineItems = "";
        for (var i = 0; i < items.length; i++) {
            orderLineItems = orderLineItems + items[i].OrderLineItemId + ",";
        }
        orderLineItems = orderLineItems.substr(0, orderLineItems.length - 1);


        var quantities = "";
        for (var i = 0; i < items.length; i++) {
            quantities = quantities + items[i].Quantity + ",";
        }
        quantities = quantities.substr(0, quantities.length - 1);
        tax = Number($("#dynamic-tax-amount").text().replace(/[^0-9\.]+/g, "")).toFixed(2);
        var subTotal = Number($("#hdnSubtotal").val().replace(/[^0-9\.]+/g, "")).toFixed(2);

        var total = (parseFloat(subTotal) + parseFloat(tax)).toFixed(2);
        var orderId = $("#OrderId").val();
        var requestModel = {
            RMARequestId: $("#RMARequestId").val(),
            RequestNumber: $("#requestNumber").val(),
            TaxCost: tax,
            SubTotal: subTotal,
            Total: total,
            Comments: $("#Comments").val(),
            Flag: "",
            RMARequestItems: items,
            Mode: "rma",
            OrderLineItems: orderLineItems,
            Quantities: quantities,
            OrderId: orderId
        };
        App.Api.issueGiftCard(requestModel, function (response) {
            var type = "error";
            if (response.sucess) {
                type = "success";
                window.location.href = response.url;
            }
            CommonHelper.DisplayNotificationMessagesHelper(response.message, type, response.isFadeOut, 5000);
        });
    },

        PrintContent: function () {
        var divContents = $("#rmalist").html();
        var printWindow = window.open('', '', 'height=500,width=900');
        printWindow.document.write('<html><head><title>RMA Request Report</title><link rel="stylesheet" href="/Content/bootstrap-3.3.1/css/bootstrap.css"><link rel="stylesheet" href="/Content/site.css">');
        printWindow.document.write('</head><body>');
        printWindow.document.write(divContents);
        printWindow.document.write('</body></html>');
        printWindow.document.close();
    },

    ShowHideGridLinks: function () {
        $("#grid tbody tr").find('.z-manage').addClass('z-remaining-process');
        $("#grid tbody tr").find('.z-manage').removeClass('z-manage');
        $("#grid tbody tr").find('.z-remaining-process').attr("title", "Append RMA");

        $("#grid tbody tr").find('.z-remaining-process').each(function () {
            if (!($(this).attr('href').toLowerCase().indexOf('append') >= 0)) {
                $(this).removeClass('z-remaining-process');
            }
            else {
                $(this).parent().parent().find(".z-edit").remove();
            }
            if (($(this).attr('href').toLowerCase().indexOf('void') >= 0)) {
                $(this).parent().parent().find(".z-edit").remove();
            }
        });
    }
}



