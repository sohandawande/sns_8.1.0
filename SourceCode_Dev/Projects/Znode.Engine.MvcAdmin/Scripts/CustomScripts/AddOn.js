﻿AddOn = {
    ShowRecurringBillingOptions: function () {
        if ($('#EnableRecurringBilling').prop("checked") == true) {
            $('#ShowBillingOptions').removeClass("hidden");
        } else {
            $('#ShowBillingOptions').addClass("hidden");
            $("#BillingAmount").val('');
        }
    },
    CheckAddOnRecurringBillingCheckBox: function () {
        if ($("#EnableRecurringBilling").length > 0) {
            if ($($("#EnableRecurringBilling")[0]).is(":checked")) {
                $("#ShowBillingOptions").removeClass("hidden");
            } else {
                $("#ShowBillingOptions").addClass("hidden");
                $("#BillingAmount").val('');
            }
        }
    },
    ValidateAddOnValueAttributes: function () {
        var returnVariable = true;
        if ($("#ShippingRuleTypeId").val() == '2') {
            var weight = $("#Weight").val();
            if (weight == "") {
                $("#valWeight").text("You must enter weight for this Shipping Type").addClass("field-validation-error");
                $("#valWeight").show();
                returnVariable = AddOn.ValidateBillingAmount();
                returnVariable = false;
            }
            else if (weight < 0.1) {
                $("#valWeight").text(" This Shipping Type requires that Weight be greater than or equal to 0.1").addClass("field-validation-error");
                $("#valWeight").show();
                returnVariable = AddOn.ValidateBillingAmount();
                returnVariable = false;
            }
            else {
                returnVariable = AddOn.ValidateBillingAmount();
            }
        }
        else {
            $("#valWeight").hide();
            returnVariable = AddOn.ValidateBillingAmount();
        }
        return returnVariable;
    },
    ValidateBillingAmount: function () {
        $("#valBillingAmount").text('').removeClass("field-validation-error");
        if ($($("#EnableRecurringBilling")[0]).is(":checked")) {
            var billingAmount = $("#BillingAmount").val();
            if (billingAmount.length < 1) {
                $("#valBillingAmount").text("Enter a recurring billing amount").addClass("field-validation-error");
                $("#valBillingAmount").show();
                return false;
            }
            else {
                $("#valBillingAmount").hide();
                return true;
            }
        }
    },
}
$(document).ready(function () {
    AddOn.ShowRecurringBillingOptions();
});