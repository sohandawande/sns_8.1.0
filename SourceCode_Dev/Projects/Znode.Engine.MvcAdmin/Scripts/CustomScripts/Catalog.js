﻿Catalog = {
    CatalogDeleteRedirect: function () {
        $("#grid tbody tr").find('.z-delete').removeAttr("onclick");
        $("#grid tbody tr").find('.z-delete').click(function (e) {
            e.preventDefault();
            window.location.href = "/catalog/delete/" + (this.id).split('/')[(this.id).split('/').length - 1]
        })
    },

}