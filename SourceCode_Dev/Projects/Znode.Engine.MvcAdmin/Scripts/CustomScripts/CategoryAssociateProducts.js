﻿
CategoryAssociateProducts = {
    Load: function () {
        $("#Category #btnsearch").on('click', CategoryAssociateProducts.SetCurrentView);
        $("#FranchiseCategory #btnsearch").on('click', CategoryAssociateProducts.SetCurrentView);

        CategoryAssociateProducts.LoadTreeData();
        $(document).on('click', '.jstree-anchor', function (e) {
            var categoryId = $(this).parent().attr('id');
            var name = e.currentTarget.childNodes[1].data;
            sessionStorage.setItem('selectedcatalog', $("#CatalogId").val());
            sessionStorage.setItem('selectedname', $("input[name='Name']").val());
            if (name.indexOf("(") == -1 && name.indexOf(")") == -1) {
                window.location = CategoryAssociateProducts.GetProductUrl(categoryId);
                return false;
            }
            else {
                window.location = CategoryAssociateProducts.GetCategoryUrl(categoryId);
            }

        });
    },
    ShowImageButton: function () {
        if ($('#rdoKeepCurrent').prop("checked") == true) {
            $("#image-div").hide();
        }
        else if ($('#rdoUploadNew').prop("checked") == true) {
            $("#image-div").show();
        }
    },
    BindMasterPageList: function (cssList, cssThemeId) {
        CategoryAssociateProducts.DropDownSelect(cssThemeId.value);
        CategoryAssociateProducts.BindCssList(cssList, cssThemeId.value);
    },
    BindCssList: function (cssList, cssThemeId) {
        App.Api.getProductCssListByThemeId(cssThemeId, function (response) {
            $('#' + cssList.id).empty();
            $('#' + cssList.id).append("<option value=''>Same as store</option>");
            for (var i = 0; i < response.length; i++) {
                $('#' + cssList.id).append("<option value='" + response[i].Value + "'>" + response[i].Text + "</option>");
            }
        });
    },
    ShowHideCssMasterList: function () {
        var selectedIndex = $("#themeList").val()
        if (selectedIndex > 0) {
            $("#divCssList").show();
        }
        else {
            $("#divCssList").hide();
        }
    },
    DropDownSelect: function (id) {
        if (id > 0) {
            $("#divCssList").show();
        }
        else {
            $("#divCssList").hide();
        }
    },
    AssociatedProducts: function (categoryId) {
        if (categoryId > 0) {
            var productIds = "";
            $("#grid input[name=Checkbox]:checked").each(function () {
                productIds = productIds + $(this).attr('id').split('_')[1] + ",";
            });

            productIds = productIds.substr(0, productIds.length - 1);
            var type = "error";
            if (productIds.length > 0) {
                var url = "/Category/CategoryAssociatedProducts/" + categoryId;
                App.Api.categoryAssociatedProduct(url, productIds, function (response) {
                    if (response.sucess) {
                        type = "success";
                        CommonHelper.DisplayNotificationMessagesHelper(response.message, type, response.isFadeOut, 5000);
                        var url = "/Category/Manage/" + response.id;
                        window.location.href = url;
                    }
                    CommonHelper.DisplayNotificationMessagesHelper(response.message, type, response.isFadeOut, 5000);
                });
            }
            else {
                CommonHelper.DisplayNotificationMessagesHelper("At least one Product should be selected.", type, false, 5000);
            }
        }
    },

    AssociatedFranchiseProducts: function (categoryId) {
        if (categoryId > 0) {
            var productIds = "";
            $("#grid input[name=Checkbox]:checked").each(function () {
                productIds = productIds + $(this).attr('id').split('_')[1] + ",";
            });
            productIds = productIds.substr(0, productIds.length - 1);
            var type = "error";
            if (productIds.length > 0) {
                var url = "/FranchiseCategory/CategoryAssociatedProducts/" + categoryId;
                App.Api.categoryAssociatedProduct(url, productIds, function (response) {
                    if (response.sucess) {
                        type = "success";
                        CommonHelper.DisplayNotificationMessagesHelper(response.message, type, response.isFadeOut, 5000);
                        var url = "/FranchiseCategory/Manage/" + response.id;
                        window.location.href = url;
                    }
                    CommonHelper.DisplayNotificationMessagesHelper(response.message, type, response.isFadeOut, 5000);
                });
            }
            else {
                CommonHelper.DisplayNotificationMessagesHelper("At least one Product should be selected.", type, false, 5000);
            }
        }
    },
    BindCategoryTree: function () {
        $("#MessageBoxContainerId").hide();
        var catlogId = $('#CatalogId').val();
        if (catlogId == undefined) {
            catlogId = 0;
        }
        if (catlogId.length < 1) {
            CommonHelper.DisplayNotificationMessagesHelper("Please select catalog", "error", false, 5000);
        }
        else {
            var name = $("input[name=Name]").val();
            if ($(':jstree').length < 1) {
                this.ShowCategoryTree(null);
            }
            CategoryAssociateProducts.SetTreeData();
        }
    },
    ShowCategoryTree: function (response) {
        $('#category-tree')
             .bind("open_node.jstree", function (event, data) {
                 //To do node open event 
             })
             .bind("loaded.jstree", function (event, data) {
                 //To do node tree load complete event   
                 // var treedata = $('#category-tree').jstree(true).get_json();
             })
             .jstree({
                 'core': {
                     "animation": 0,
                     "check_callback": true,
                     "themes": { "stripes": true },
                     data: eval(response),
                 },
                 "types": {
                     "#": {
                         "max_children": 1,
                         "max_depth": 10,
                         "valid_children": ["root"]
                     },
                     "root": {
                         "icon": "/static/3.1.1/assets/images/tree_icon.png",
                         "valid_children": ["default"]
                     },
                     "default": {
                         "valid_children": ["default", "file"]
                     },
                     "file": {
                         "icon": "glyphicon glyphicon-file",
                         "valid_children": []
                     }
                 },
                 "plugins": [
                   //"dnd", //TODO : unable/disable draging
                   "search",
                   "state",
                   "types",
                   "wholerow"
                 ]
             });
    },
    SetCurrentView: function () {
        var view = sessionStorage.getItem('viewName');
        $("#MessageBoxContainerId").hide();
        if (view == "tree-view") {
            CategoryAssociateProducts.SetTreeView();
            return false;
        }
        else {
            CategoryAssociateProducts.SetGridView();
        }
    },
    ToggleView: function (obj) {
        var viewId = obj.id;
        if (viewId == "tree-view") {
            CategoryAssociateProducts.SetTreeView();
        }
        else {
            CategoryAssociateProducts.SetGridView();
            $("#btnsearch").closest("form").submit();
        }
    },
    AddRemoveCatalogValue: function (action) {
        if ($('#CatalogId option').eq(0).text().toLocaleLowerCase() == "all" && action == true) {
            $('#CatalogId option:eq(' + 0 + ')').remove();
        }
        else if ($('#CatalogId option').eq(0).text().toLocaleLowerCase() != "all" && action == false) {
            $("<option></option>").html("All").insertBefore($("#CatalogId").children().eq(0));
        }
    },
    SetTreeView: function () {
        CategoryAssociateProducts.AddRemoveCatalogValue(true);
        sessionStorage.setItem('viewName', "tree-view");
        $("#tree-view-content").show();
        $("#grid-view-content").hide();
        CategoryAssociateProducts.BindCategoryTree();
    },
    SetGridView: function () {
        CategoryAssociateProducts.AddRemoveCatalogValue(false);
        sessionStorage.setItem('viewName', "grid-view");
        $("#tree-view-content").hide();
        $("#grid-view-content").show();
       
    },
    LoadTreeData: function () {
        var querystring = window.location.search;
        if (querystring.length > 0) {
            var selectedcatalog = sessionStorage.getItem('selectedcatalog');
            var selectedname = sessionStorage.getItem('selectedname');
            if (selectedcatalog != undefined) {
                $("#CatalogId").val(selectedcatalog);
                $("input[name='Name']").val(selectedname);
                CategoryAssociateProducts.SetCurrentView();
            }
        }
        else {
            sessionStorage.removeItem("selectedcatalog");
            sessionStorage.removeItem("selectedname");
            sessionStorage.removeItem("treeData");
        }
    },
    GetProductUrl: function (productId) {
        var userType = $('#UserType').val();
        if (userType.toLocaleLowerCase() == "admin") {
            return "/product/manage?id=" + productId + "&tabMode=treeview";
        }
        else if (userType.toLocaleLowerCase() == "franchiseadmin") {
            return "/FranchiseAdmin/Franchiseproduct/manage?id=" + productId + "&tabMode=treeview";
        }
    },
    GetCategoryUrl: function (categoryId) {
        var userType = $('#UserType').val();
        if (userType.toLocaleLowerCase() == "admin") {
            return "/category/manage?id=" + categoryId + "&tabMode=treeview";
        }
        else if (userType.toLocaleLowerCase() == "franchiseadmin") {
            return "/FranchiseAdmin/Franchisecategory/manage?id=" + categoryId + "&tabMode=treeview";
        }
    },
    GetTreeUrl: function () {
        var userType = $('#UserType').val();
        if (userType.toLocaleLowerCase() == "admin") {
            return "/Category/GetCategoryTree";

        } else if (userType.toLocaleLowerCase() == "franchiseadmin") {
            return "/FranchiseAdmin/FranchiseCategory/GetCategoryTree";
        }
    },
    SetTreeData: function () {
        var selectedcatalog = sessionStorage.getItem('selectedcatalog');
        var selectedname = sessionStorage.getItem('selectedname');
        var currentcatalog = $("#CatalogId").val();
        var currentname = $("input[name='Name']").val();
        var data = null;
        if (selectedcatalog == currentcatalog && selectedname == currentname) {
            data = JSON.parse(sessionStorage.getItem('treeData'));
            if (data == null) {
                data = CategoryAssociateProducts.GetTreeData();
            }
        }
        else {
            data = CategoryAssociateProducts.GetTreeData();
        }
        $('#category-tree').jstree(true).settings.core.data = data;
        $('#category-tree').jstree(true).refresh();
    },
    GetTreeData: function () {
        var catlogId = $('#CatalogId').val();
        if (catlogId == undefined) {
            catlogId = 0;
        }
        var name = $("input[name=Name]").val();;
        var url = CategoryAssociateProducts.GetTreeUrl();
        App.Api.GetCategoryTree(url, catlogId, name, function (response) {
            sessionStorage.setItem('treeData', JSON.stringify(response));
            $('#category-tree').jstree(true).settings.core.data = response;
            $('#category-tree').jstree(true).refresh();
            return response;
        });
    }
}