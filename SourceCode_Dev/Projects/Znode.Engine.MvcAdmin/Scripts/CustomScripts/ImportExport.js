﻿
ImportExport = {
    BindAttributeTypes: function () {
        var catalogId = $('#ddlCatalog').val();
        if (catalogId != undefined) {
            App.Api.bindAttributeTypes(catalogId, function (data) {
                $("#ddlAttributeType").html('');
                var subjectList = "";
                if (data.length > 0) {
                    for (var i = 0; i < data.length; i++) {
                        subjectList += "<option value='" + data[i].Value + "' title='" + data[i].Text + "'>" + data[i].Text + "</option>";
                    }
                }
                else {
                    subjectList = "<option value='-1' title='No Record Found'>No Record Found</option>";
                }
                $("#ddlAttributeType").html(subjectList);
            });
        }
        try { $('#MessageBoxContainerId').html(''); } catch (err) { }
    },
    OnSubmit: function (filter) {
        var status = true;
        if (filter != undefined && filter != "") {
            if (filter == "Attribute") {
                var selectedAttributeType = $("#ddlAttributeType").val();
                if (selectedAttributeType == "-1") {
                    CommonHelper.DisplayNotificationMessagesHelper("No Attributes found.", "error", false, 5000);
                    status = false;
                }
            }
            if (filter == "Sku") {
                ImportExport.BindSelectedAttributes();
            }
        }
        return status;
    },
    BindCategories: function () {
        var catalogId = $('#ddlCatalog').val();
        if (catalogId != undefined) {
            App.Api.bindCategories(catalogId, function (data) {
                $("#ddlCategory").html('');
                var subjectList = "";
                if (data.length > 0) {
                    for (var i = 0; i < data.length; i++) {
                        subjectList += "<option value='" + data[i].Value + "' title='" + data[i].Text + "'>" + data[i].Text + "</option>";
                    }
                }
                $("#ddlCategory").html(subjectList);
            });
        }

    },
    BindAttributeList: function (data) {
        $("#productattributes").html('');
        $("#exportskuattribute").show();
        for (var i = 0; i < data.length; i++) {
            var drpId = "ddlAttribute_" + data[i].AttributeTypeId;
            var ddl = ' <div class="form-group" style="padding-left:0;"><select name="' + drpId + '" id="' + drpId + '" class="ddlAttributeList"><option value="0">' + data[i].AttributeName + '</option>';
            for (var j = 0; j < data[i].AttributeValueList.length; j++) {
                var attributId = data[i].AttributeValueList[j].Value;
                var attributName = data[i].AttributeValueList[j].Text;
                ddl += '<option value="' + attributId + '">' + attributName + '</option>';
            }
            ddl += '</select>'
            $("#productattributes").append(ddl);
        }
    },
    ClearSelection: function () {
        $('#txtProductName').val('');
        $("#productattributes").html('');
        $("#exportskuattribute").hide();
    },
    BindSelectedAttributes: function () {

        var attributeIds = '';
        $('select.ddlAttributeList').each(function () {
            var attributeId = $('#' + $(this).attr("id") + ' option:selected').val();

            if (attributeId > 0) {
                attributeIds += attributeId + ',';
            }
        });
        if (attributeIds.length > 0) {
            $("#ProductAttributes").val(attributeIds.slice(0, -1));
        } else { $("#ProductAttributes").val(""); }
    },
    PreviewImportData: function (filter) {

        var totalFiles = document.getElementById("ImportFile").files.length;
        if (totalFiles > 0) {
            var file = document.getElementById('ImportFile').files[0];
            var regularExpression = "";
            regularExpression = /.xlsx|.xls|.csv/;
            if (regularExpression.test(file.name)) {
                overlayforwaitblock();
                var xhr = new XMLHttpRequest();
                var fd = new FormData();
                fd.append("file", file);
                fd.append("type", filter);
                xhr.open("POST", "/ImportExport/ImportPreview/", true);
                xhr.send(fd);
                xhr.addEventListener("load", function (event) {
                    
                    if (event.target.response.length < 500)
                    {
                        var data = $.parseJSON(event.target.response);
                        if (data.HasError === true) {
                            CommonHelper.DisplayNotificationMessagesHelper(data.ErrorMessage, 'error', false, 5000);
                        }
                    }
                    else {
                        $("#importPreviewOrErrorDiv").html(event.target.response);
                    }                                    
                    overlayforwaitnone();
                   
                }, false);
            }
            else {
                var fileTypeValidationError = (uploadFileExtensionError != undefined && uploadFileExtensionError != null && uploadFileExtensionError != "") ? uploadFileExtensionError : "Please select a valid .csv, .xls or .xlsx file.";
                $("span[data-valmsg-for='ImportFile']").html("<span for='ImportFile'>" + fileTypeValidationError + "</span>");
                $("span[data-valmsg-for='ImportFile']").attr("class", "field-validation-error");
            }
        }
        else {
            $("span[data-valmsg-for='ImportFile']").html("<span for='ImportFile'>" + $("#ImportFile").attr("data-val-required") + "</span>");
            $("span[data-valmsg-for='ImportFile']").attr("class", "field-validation-error");
        }

    },
    OnImportSubmit: function () {
        var totalFiles = document.getElementById("ImportFile").files.length;
        if (totalFiles > 0) {
            var file = document.getElementById('ImportFile').files[0];
            var regularExpression = "";
            regularExpression = /.xlsx|.xls|.csv/;
            if (regularExpression.test(file.name)) {
                overlayforwaitblock();
            }
        }
        return true;
    }
}

$(document).ready(function () {

    $("#txtProductName").autocomplete({
        source: function (request, response) {
            var catalogId = $('#ddlCatalog').val();
            try {
                App.Api.autoCompleteSearchProduct(request.term, catalogId, function (data) {
                    response($.map(data, function (item) {
                        return { label: item.Name, value: item.ProductId, productTypeId: item.ProductTypeId }
                    }));
                });
            } catch (err) {
            }
        },
        select: function (event, ui) {
            $('#txtProductName').val(ui.item.label);
            $('#txtProductName').attr('data-ProductId', ui.item.value);
            $('#ProductId').val(ui.item.value);
            $("#productattributes").html('');
            if (ui.item.productTypeId > 0) {
                App.Api.getProductAttributesByProductTypeId(ui.item.productTypeId, function (response) {
                    if (response.length > 0) {
                        ImportExport.BindAttributeList(response);
                    }
                });
            }
            overlayforwaitnone();
            return false;
        },
        messages: {
            noResults: "", results: ""
        }
    });
});