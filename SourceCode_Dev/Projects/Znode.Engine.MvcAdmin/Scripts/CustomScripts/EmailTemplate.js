﻿EmailTemplate = {
    Load: function () {
        var deleteTemplateKeys = $("#ddlDeleteTemplateKeys").val();
        EmailTemplate.GetHtmlContent(function (res) { if (res) $("#ddlDeleteTemplateKeys").val(deleteTemplateKeys); deleteTemplateKeys = null; });
        $(document).on('click', '#btnsubmit', EmailTemplate.SetSelectTemplateValidation);
        EmailTemplate.EncodeTemplateContent();
        EmailTemplate.PreviewClick();

    },

    //Get template content in proper format.
    EncodeTemplateContent: function () {
        var content = tinymce.get('Html')
        if (content != null && content != '') {
            content = content.getContent();
            var response = content.replace(/&lt;!-- --&gt;/g, '<br/>');
            tinymce.activeEditor.setContent(response)
        }
    },

    //Set template content and replace new line tag with comment line to save in proper format.
    DecodeTemplateContent: function () {
        var content = tinymce.get('Html')
        content = content.getContent();
        var response = content.replace(/<br \/>/g, '<!-- -->');
        $('#HtmlStringForm').val(response);
    },

    SetSelectTemplateValidation: function (event) {
        var ddlDeleteTemplateKeys = $('#ddlDeleteTemplateKeys :selected').val();
        $('#valTemplateName').text('');
        var content = tinymce.get('Html')
        content = content.getContent();
        if (ddlDeleteTemplateKeys == "0") {
            $('#valTemplateName').text("Select template.");
            return false;
        }
        else if ($('#IsNewAdd').val() == 'true' || $('#IsNewAdd').val() == 'True') {
            if (content == "" || content == null) {
                if ($('#txtTemplateName').val() == "") {
                    $('#valTemplateName').text("Enter Name.");
                }

                $('#valTemplateContent').text("Enter template content.");
                event.preventDefault();
            }
            else {
                EmailTemplate.DecodeTemplateContent();
            }

        }
        else if ($('#IsNewAdd').val() == 'False' || $('#IsNewAdd').val() == 'false') {

            if (content == "" || content == null) {
                $('#valTemplateName').text("");
                $('#valTemplateContent').text("Enter template content.");
                event.preventDefault();
            }
            else {
                EmailTemplate.DecodeTemplateContent();
            }
        }
        else {
            EmailTemplate.DecodeTemplateContent();
        }
    },

    DisplayDeletedTemplateKeyTextBox: function () {

        if ($('#ddlDeleteTemplateKeys').val() == '+1') {
            $('#ddlDeleteTemplateKeys').hide();
            $('#btnDeleteTemplateKey').show();
            $('#txtTemplateName').show();
            $('#valTemplateName').text("");
            $('#valTemplateContent').text("");
        }
    },

    ClearSelection: function () {
        $('#ddlDeleteTemplateKeys').show();
        $('#btnDeleteTemplateKey').hide();
        $('#txtTemplateName').hide();
        var ddlTemplateKeys = $('#ddlDeleteTemplateKeys');
        ddlTemplateKeys[0].selectedIndex = 0;
        $('#txtTemplateName').val('');
        $('#IsNewAdd').val(false);
        tinymce.activeEditor.setContent('')
        $("span[data-valmsg-for='TemplateName']").html('');
        $('#valTemplateName').text("");
        $('#valTemplateContent').text("");

    },

    GetHtmlContent: function (fun) {

        $("#ddlDeleteTemplateKeys").unbind("change");
        $("#ddlDeleteTemplateKeys").change(function () {
            var ddlDeleteTemplateKeys = $('#ddlDeleteTemplateKeys :selected').val();

            $('#valTemplateName').text('');
            if (ddlDeleteTemplateKeys == "+1") {
                tinymce.activeEditor.setContent('');
                $('#IsNewAdd').val(true);
                return false;
            }
            if (ddlDeleteTemplateKeys == "0") {

                tinymce.activeEditor.setContent('')
                return false;
            }
            App.Api.getHtmlContent(ddlDeleteTemplateKeys, function (response) {
                response = response.replace(/&lt;!-- --&gt;/g, '<br/>');
                tinymce.activeEditor.setContent(response)
            });
        });
    },

    PreviewClick: function () {
        $("#grid-container #grid tbody tr td").find(".z-view").click(function (e) {
            e.preventDefault();
            EmailTemplate.PreviewPopup($(this).attr("href"));
        });
    },

    PreviewPopup: function (templatehref) {
        var templateName = templatehref.split("/")[3];
        if (templateName != "" && templateName != null)
            App.Api.previewTemplate(templateName, function (response) {
                EmailTemplate.PopupCenter(response, 500, 900)
            });
        return false;
    },

    PopupCenter: function (response, windowHeight, windowWidth) {
        // Fixes dual-screen position                         Most browsers      Firefox
        var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
        var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

        width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
        height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

        var left = ((width / 2) - (windowWidth / 2)) + dualScreenLeft;
        var top = ((height / 2) - (windowHeight / 2)) + dualScreenTop;
        var template = window.open('', '', 'scrollbars=yes, width=' + windowWidth + ', height=' + windowHeight + ', top=' + top + ', left=' + left);

        $(template.document.body).html(response.data.html);
    }
}
