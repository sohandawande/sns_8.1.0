﻿var Supplier = {
    NotificationType: function () {
        $("#ddlSupplierList").on("change", function () {
            Supplier.BindNotificationType($(this).val());
        });
    },
    BindNotificationType: function (shippingrule) {
        if ($.trim(shippingrule) === "ZnodeSupplierEmail") {
            $("#emailSetting").show();
        }
        else if ($.trim(shippingrule) === "ZnodeSupplierWebService") {
            $("#emailSetting").hide();
        }
    },
    ValidationAttributes: function () {
        var ddlValue = $("#ddlSupplierList").val();
        if ($.trim(ddlValue) === "ZnodeSupplierWebService") {
            $("#NotificationEmail").val(defaultNotificationEmail);
            $("#EmailNotificationTemplate").val(defaultNotifiactionTemplate);
        }
        return true;
    }
}

//Global variables for storing the default values.
var defaultNotificationEmail;
var defaultNotifiactionTemplate;

$(document).ready(function () {
    Supplier.NotificationType();
    var val = $("#ddlSupplierList").val();

    if ($.trim(val) === "ZnodeSupplierEmail") {
        defaultNotificationEmail = $("#NotificationEmail").val();
        defaultNotifiactionTemplate = $("#EmailNotificationTemplate").val();
        $("#emailSetting").show();
    }
    else if ($.trim(val) === "ZnodeSupplierWebService") {
        $("#emailSetting").hide();
    }
});
