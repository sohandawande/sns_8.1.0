﻿Product = {
    Load: function () {
        var selectedItems = $("#ProductAttributes").val();
        if (selectedItems && selectedItems.length > 0) {
            var productTypeId = $("#ProductTypeId").val();
            Product.BindAttributes(productTypeId);
        }
        Product.ToggleBilling();
        $("#FreeShipping").change();
        $("#ShippingRuleTypeId").change();
        Product.ShowHideGiftCard();
    },
    GetProductAttribute: function (obj) {
        $("#hdnProductName").val($('#ProductTypeId :selected').text());
        $("#ProductAttributes").val('');
        var productTypeId = $('#' + obj.id + ' option:selected').val();
        Product.BindAttributes(productTypeId);
    },

    BindProductCatagory: function (fun) {
        $("#CatalogId").unbind("change");
        $("#CatalogId").change(function () {
            var catalogid = $('#CatalogId :selected').val();
            if (catalogid === "" || catalogid === null) {
                catalogid = 0;
            }
            //start ajax request
            App.Api.getCategoryListById(catalogid, function (response) {

                var ddl = '<option value= "" selected="selected">All</option>';
                for (var i = 0; i < response.length; i++) {
                    var categoryId = response[i].CategoryId;
                    var categoryName = response[i].Name;
                    ddl += '<option value="' + categoryId + '">' + categoryName + '</option>';
                }

                $("#CategoryId").html(ddl);
                if (catalogid !== 0) fun(true);
            });
        });
    },

    PreventSearchButton: function (fun) {
        var catalogid = $('#CatalogId :selected').val();
        if (catalogid != "") {
            App.Api.getCategoryListById(catalogid, function (response) {
                var selectedCategoryId = $('#CategoryId :selected').val();
                var ddl = '<option value= "">All</option>';
                for (var i = 0; i < response.length; i++) {
                    var categoryId = response[i].CategoryId;
                    var categoryName = response[i].Name;
                    ddl += '<option value="' + categoryId + '"' +(selectedCategoryId == categoryId ? ' selected' : '') + '>' +categoryName + '</option>';
                }
                $("#CategoryId").html(ddl);
                fun(true);
            });
        }
    },
    BindAttributes: function (productTypeId) {
        $("#divAttributes").hide();
        $("#divGiftCards").hide();
        $("#divProductAttributes").html('');
        App.Api.getProductAttributesByProductTypeId(productTypeId, function (response) {
            if (response.length > 0) {
                var isGiftCard = response[0].IsGiftCard;
                if (isGiftCard == 1) {
                    $("#IsProductTypeGiftCards").val(isGiftCard);
                    $("#divGiftCards").show();
                }
                else {
                    Product.BindAttributeList(response);
                    isAttributeLoad = true;
                    $("#divAttributes").show();
                }
            }
            else {
                var productType = $('#ProductTypeId option:selected').text();
                if (productType.toLowerCase().indexOf("gift") != -1) {
                    $("#divGiftCards").show();
                }
            }
        });
    },
    BindAttributeList: function (data) {
        for (var attributeIndex = 0; attributeIndex < data.length; attributeIndex++) {
            var drpId = "ddlAttribute_" + data[attributeIndex].AttributeTypeId;
            var ddl = '<div class="col-sm-8"> <select name="' + drpId + '" id="' + drpId + '" class="ddlAttributeList"  onchange="Product.ClearValidation(this);"></option>';
            for (var valueIndex = 0; valueIndex < data[attributeIndex].AttributeValueList.length; valueIndex++) {
                var attributId = data[attributeIndex].AttributeValueList[valueIndex].Value;
                var attributName = data[attributeIndex].AttributeValueList[valueIndex].Text;
                if (attributId > 0 && attributName != "") {
                    var selected = Product.CheckSelectedAttribute(attributId);
                    if (selected) {
                        ddl += '<option value="' + attributId + '"selected="selected">' + attributName + '</option>';
                    }
                    else {
                        ddl += '<option value="' + attributId + '">' + attributName + '</option>';
                    }
                }
            }
            ddl += '</select></div>'
            var errId = "val" + drpId;
            ddl += '<span id="' + errId + '" class="field-validation-valid" data-valmsg-replace="true" data-valmsg-for="' + drpId + '"></span>'
            var attributeLabel = '<div class="col-sm-4 nopadding"><label>' + data[attributeIndex].AttributeName + '</label></div>';
            $("#divProductAttributes").append(attributeLabel);
            $("#divProductAttributes").append(ddl);
        }
    },
    CheckSelectedAttribute: function (attributId) {
        var selectedItems = $("#ProductAttributes").val();
        var arrSelectedItems = [];
        if (selectedItems.length > 0) {
            arrSelectedItems = selectedItems.split(',');
            for (var i = 0; i < arrSelectedItems.length; i++) {
                if (parseInt(arrSelectedItems[i]) == parseInt(attributId)) {
                    return true;
                }
            }
        }
        return false;
    },
    ValidateAttributes: function () {
        var attributeIds = '';
        var isValid = true;
        $('select.ddlAttributeList').each(function () {
            var attributeId = $('#' + $(this).attr("id") + ' option:selected').val();
            var errorId = "val" + $(this).attr("id");
            $("#" + errorId).text('').removeClass("field-validation-error");
            $("#" + errorId).text('').removeClass("field-validation-error").hide();
            if (attributeId.length > 0) {
                attributeIds += attributeId + ',';
            }
            else {
                isValid = false;
                var errmsg = $('#' + $(this).attr("id") + ' option:selected').text() + " is required.";
                $("#" + errorId).text('').text(errmsg).addClass("field-validation-error");
                $("#" + errorId).text('').text(errmsg).addClass("field-validation-error").show();
            }
        });
        if (attributeIds.length > 0) {
            $("#ProductAttributes").val(attributeIds.slice(0, -1));
        }
        if (isValid == false) {
            return false;
        }
        $("#valExpirationPeriod").text('').removeClass("field-validation-error");
        if ($("#IsProductTypeGiftCards").val() != null && ($("#IsProductTypeGiftCards").val() == 1 || $("#IsProductTypeGiftCards").val().toLowerCase() == "true")) {
            var expPeriod = $("#ExpirationPeriod").val();
            if (expPeriod.length < 1) {
                $("#valExpirationPeriod").text("Expiration Period is requird.").addClass("field-validation-error");
                $("#valExpirationPeriod").show();
                return false;
            }

            if (parseFloat(expPeriod) < 1) {
                $("#valExpirationPeriod").text("Expiration Period is requird.").addClass("field-validation-error");
                $("#valExpirationPeriod").show();
                return false;
            }
        }
        if ($("#FreeShipping").prop("checked") == false) {
            if ($("#ShippingRuleTypeId").val() == "2") {
                var weight = $("#Weight").val();
                if (weight == "") {
                    $("#valweight").text("You must enter weight for this Shipping Type").addClass("field-validation-error");
                    $("#valweight").show();
                    return false;
                }
                else if (weight < 0.1) {
                    $("#valweight").text(" This Shipping Type requires that Weight be greater than or equal to 0.1").addClass("field-validation-error");
                    $("#valweight").show();
                    return false;
                }
            }
        }
        return true;
    },
    ClearValidation: function (ctrl) {
        var errorId = "val" + ctrl.id;
        $("#" + errorId).text('').removeClass("field-validation-error");
    },
    ToggleShipping: function () {
        var checked = $('#FreeShipping').prop("checked");
        if (checked == true) {
            $("#divShipping").hide();
        }
        else {
            $("#divShipping").show();
        }
    },
    ToggleShippingRate: function (obj) {
        var shippingTypeId = $('#' + obj.id + ' option:selected').val();
        var shippingRate = $("#txtShippingRate").val();
        if (parseInt(shippingTypeId) == 3) {
            if (shippingRate === undefined || shippingRate == null || shippingRate == "") {
                $("#txtShippingRate").val("0.00");
            }
            $("#divShippingRate").show();
        }
        else {
            $("#txtShippingRate").val('');
            $("#divShippingRate").hide();
        }
    },
    ToggleBilling: function () {
        var checked = $('#RecurringBillingInd').prop("checked");
        if (checked == true) {
            $("#divRecurringBilling").show();
        }
        else {
            $("#divRecurringBilling").hide();
        }
    },
    CheckSucess: function (data) {
        var type = "error";
        if (data.sucess) {
            type = "success";
        }
        document.getElementById("panel7").innerHTML = data.Response;
        try { if (data.message == "") { $('#MessageBoxContainerId').html(''); } } catch (err) { }
        CommonHelper.DisplayNotificationMessagesHelper(data.message, type, data.isFadeOut, 5000);
    },
    BindFacetsList: function (data, productId) {
        var facetGroupId = $(data).val();
        if (productId > 0 && facetGroupId > 0) {
            App.Api.bindFacetListByFacetGroupId(productId, facetGroupId, function (response) {
                _debug(response);
                $("#chklistFacets").html(response);
            });
        }
    },
    BindImageControl: function (data) {
        if ($(data).attr("value") == "CurrentImage") {
            $("#imgUploader").hide();
        }
        else {
            $("#imgUploader").show();
        }
    },
    ShowSkuAttributes: function () {
        var productType = $("#ProductTypeName").val();
        if (productType.toLowerCase() != "default") {
            var productTypeId = $("#ProductTypeId").val();
            App.Api.getProductAttributesByProductTypeId(productTypeId, function (response) {
                if (response.length > 0) {
                    var isGiftCard = response[0].IsGiftCard;
                    if (isGiftCard != 1) {
                        Product.BindAttributeList(response);
                        $("#divAttributes").show();
                    }
                    else {
                        $("#divAttributes").hide();
                    }
                }
            });
        }
        var skuId = $("#Id").val();
        if (parseInt(skuId) > 0) {
            this.GetSKUFacets(skuId);
            this.GetSkuProfileList(skuId);
        }
    },
    SkuValidateAttributes: function () {
        var sku = $("#SKU").val();
        var qty = $("#QuantityOnHand").val();
        if (sku.length > 0 && qty.length > 0) {
            return Product.ValidateAttributes();
        }
    },
    ValidateProductSetting: function () {
        var checked = $('#RecurringBillingInd').prop("checked");
        $("#valBillingAmount").text('').removeClass("field-validation-error");
        if (checked == true) {
            var amount = $("#RecurringBillingInitialAmount").val();
            if (amount.length < 0) {
                $("#valBillingAmount").text('').text('Enter a recurring billing amount').addClass("field-validation-error");
                $("#valBillingAmount").show();
                return false;
            }
            else if (parseFloat(amount) <= 0) {
                $("#valBillingAmount").text('').text('Enter a recurring billing amount').addClass("field-validation-error");
                $("#valBillingAmount").show();
                return false;
            }
            else if (parseFloat(amount) > 999999.99) {
                $("#valBillingAmount").text('').text('You must enter a Billing Amount value between $1 and $999,999.99').addClass("field-validation-error");
                $("#valBillingAmount").show();
                return false;
            }
            else if (isNaN(amount)) {
                $("#valBillingAmount").text('').text('You must enter a valid billing amount (ex: 123.45)').addClass("field-validation-error");
                $("#valBillingAmount").show();
                return false;
            }
        } else {
            $("#valBillingAmount").hide();
        }

        if (!this.IsValidSeoUrl()) {
            return false;
        }
        $("#valBillingAmount").hide();
        return true;
    },
    ShowAssociateFacetPopUp: function (productId, skuId) {
        App.Api.getAssociateSkuFacetByProductId(productId, skuId, function (response) {
            $("#divAssociateFacets").html(response);
            $("#divAssociateFacets").dialog({
                title: "Select Facets To Associate",
                resizable: false,
                modal: true,
                create: function () { $(this).closest(".ui-dialog").addClass("ui-md-popup"); }
            });
        });
    },
    CloseSKUFacets: function () {
        $("#divAssociateFacets").dialog("close");
        return false;
    },
    AssociateSKUFacets: function (skuId) {
        if (skuId > 0) {
            var associatedIds = "";
            var unAssociatedIds = "";
            var chkBox = $('input[type="checkbox"]')
            for (var i = 0; i < chkBox.length; i++) {
                if ($.isNumeric(chkBox[i].value)) {
                    if (chkBox[i].checked) {
                        associatedIds = (associatedIds == "") ? chkBox[i].value : associatedIds + "," + chkBox[i].value;
                    }
                    else {
                        unAssociatedIds = (unAssociatedIds == "") ? chkBox[i].value : unAssociatedIds + "," + chkBox[i].value;
                    }
                }
            }
            var type = "error";
            if (associatedIds.length > 0) {
                var url = "/Product/AssociateSKUFacets/" + skuId;
                App.Api.associateSKUFacets(url, associatedIds, unAssociatedIds, function (response) {
                    if (response.sucess) {
                        type = "success";
                        CommonHelper.DisplayNotificationMessagesHelper(response.message, type, response.isFadeOut, 5000);
                    }
                    CommonHelper.DisplayNotificationMessagesHelper(response.message, type, response.isFadeOut, 5000);
                });
            }
            else {
                CommonHelper.DisplayNotificationMessagesHelper("At least one facet should be selected.", type, false, 5000);
            }
            Product.GetSKUFacets(skuId);
            $("#divAssociateFacets").dialog("close");
        }
    },
    GetSKUFacets: function (skuId) {
        $("#divSKUFacetsList").html("");
        $("#divSKUFacetsList").hide();
        var productId = $("#ProductId").val();
        App.Api.getSkuFacetBySkuId(skuId, productId, function (response) {
            $("#divSKUFacetsList").html(response);
            $("#divSKUFacetsList").show();
            $("#divSKUFacetsList #grid tbody tr").find('td:eq(3) a').click(function (e) { e.preventDefault(); Product.EditSKUFacetPopUp(skuId, $(this).attr("href")); })
            $('#divSKUFacetsList .pagination').hide();
        });
    },
    BindSkuFacetsList: function (data, productId, skuId) {
        var facetGroupId = $(data).val();
        if (productId > 0 && facetGroupId > 0) {
            App.Api.bindFacetListBySkuFacetGroupId(productId, skuId, facetGroupId, function (response) {
                _debug(response);
                $("#chklistFacets").html(response);
            });
        }
    },
    EditSKUFacetPopUp: function (skuId, facethref) {
        var facetgroupId = facethref.split("/")[3];
        var productId = $("#ProductId").val();
        if (parseInt(facetgroupId) > 0 && parseInt(facetgroupId) > 0)
            App.Api.getAssociateSkuFacetByGroupId(facetgroupId, productId, skuId, function (response) {
                $("#divAssociateFacets").html(response);
                $("#divAssociateFacets").dialog({
                    resizable: false,
                    height: 500,
                    width: 500,
                    modal: true
                });
            });
    },
    ShowSkuProfilePopUp: function (skuId, userType) {
        if (userType != null && userType.toLowerCase() == "admin") {
            url = "/Product/CreateSkuProfile";
        }
        else {
            url = "/" + userType + "/MallProduct/CreateSkuProfile";
        }
        App.Api.createSkuProfile(skuId, url, function (response) {
            $("#divAssociateProfile").html(response);
            $("#divAssociateProfile").dialog({
                title: "Profile Settings",
                resizable: false,
                modal: true,
                create: function () { $(this).closest(".ui-dialog").addClass("ui-md-popup"); }
            });
        });
    },
    GetSkuProfileList: function (skuId) {
        $("#divProfileList").html("");
        $("#divProfileList").hide();
        App.Api.getSkuProfileList(skuId, function (response) {
            $("#divProfileList").html(response);
            $("#divProfileList").show();
            $("#divProfileList #grid tbody tr").find('td:eq(3) a').click(function (e) { e.preventDefault(); Product.EditSkuProfilePopUp(skuId, $(this).attr("href")); })
            $('#divProfileList .pagination').hide();
        });
    },
    EditSkuProfilePopUp: function (skuId, facethref) {
        var profileId = facethref.split("/")[3];
        if (parseInt(skuId) > 0 && parseInt(profileId) > 0)
            App.Api.editSkuProfile(profileId, function (response) {
                $("#divAssociateProfile").html(response);
                var associatedProfileId = $("#divAssociateProfile #hdnProfileId").val();
                if (associatedProfileId > 0) {
                    $("#divAssociateProfile #ddlSkuProfileList").val(associatedProfileId)
                }
                $("#divAssociateProfile").dialog({
                    title: "Profile Settings",
                    resizable: false,
                    modal: true,
                    create: function () { $(this).closest(".ui-dialog").addClass("ui-sm-popup"); }
                });
            });
    },
    CloseSKUProfile: function () {
        $("#divAssociateProfile").dialog("close");
        return false;
    },
    AssociateSKUProfile: function (id, skuId, userType) {
        var profile = {
            SkuProfileEffectiveID: id,
            SkuId: skuId,
            ProfileId: $('#ddlSkuProfileList option:selected').val(),
            EffectiveDate: $("#EffectiveDate").val()
        };
        var url = "";
        if (userType.toLowerCase() == "admin") {
            url = "/Product/AssociateSKUProfile";
        } else {
            url = "/Malladmin/MallProduct/AssociateSKUProfile";
        }

        App.Api.associateSKUProfile(profile, url, function (response) {
            if (response.sucess) {
                type = "success";
                isSuccess = true;
                CommonHelper.DisplayNotificationMessagesHelper(response.message, type, response.isFadeOut, 5000);
            }
            CommonHelper.DisplayNotificationMessagesHelper(response.message, type, response.isFadeOut, 5000);
        });
        Product.GetSkuProfileList(skuId);
        $("#divAssociateProfile").dialog("close");
    },
    AssociateSelectedHandler: function (inputTypeId, formId) {
        var ids = DynamicGrid.GetSelectedCheckBoxValue().toString();
        $("#" + inputTypeId).attr('value', ids);
        $("#" + formId).submit();
    },
    BindVendorProductCategories: function () {
        try {
            var categoryIds = "";
            $("#vendorproductcategories input[type='checkbox']:checked").each(function () {
                categoryIds = (categoryIds == "") ? $(this).attr('data-categoryid') : categoryIds + "," + $(this).attr('data-categoryid');
            });
            $("#vendorproductCategoryIds").attr('value', categoryIds);
        }
        catch (err) { }
        return true;
    },
    IsValidSeoUrl: function () {
        var seoUrl = $('#txtSeoPageUrl').val();
        var regex = "^[A-Za-z0-9-_]+$";
        if (seoUrl.trim().length > 0) {
            $("#valSeoPageUrl").text('').removeClass("field-validation-error");
            $("#valSeoPageUrl").hide();
            if (CommonHelper.IsMatchRegularExpressionString(seoUrl, regex) == false) {
                $("#valSeoPageUrl").text("Enter valid SEO friendly URL").addClass("field-validation-error");
                $("#valSeoPageUrl").show();
                return false;
            }
        }
        return true;
    },

    AssociateHighlights: function (productId, userType) {
        if (productId > 0) {
            var selectedHighlights = "";
            $("#grid input[name=CheckBox]:checked").each(function () {
                selectedHighlights = selectedHighlights + $(this).attr('id').split('_')[1] + ",";
            });

            selectedHighlights = selectedHighlights.substr(0, selectedHighlights.length - 1);
            var type = "error";
            if (selectedHighlights.length > 0) {
                var url = "";
                if (userType == "FranchiseAdmin") {
                    url = "/FranchiseAdmin/FranchiseProduct/AssociateHighlight/" + productId;
                }
                else {
                    url = "/Product/AssociateHighlight/" + productId;
                }
                App.Api.productAssociatedHilights(url, selectedHighlights, function (response) {
                    if (response.sucess) {
                        type = "success";
                        CommonHelper.DisplayNotificationMessagesHelper(response.message, type, response.isFadeOut, 5000);
                        var url = "";
                        if (userType == "FranchiseAdmin") {
                            url = "/FranchiseAdmin/FranchiseProduct/Manage/" + response.id;
                        }
                        else {
                            url = "/Product/Manage/" + response.id;
                        }
                        window.location.href = url;
                    }
                    CommonHelper.DisplayNotificationMessagesHelper(response.message, type, response.isFadeOut, 5000);
                });
            }
            else {
                CommonHelper.DisplayNotificationMessagesHelper("At least one Highlight should be selected.", type, false, 5000);
            }
        }
    },
    ManageFranchiseProductGridColumns: function () {
        $("#Dynamic_Grid #grid thead tr").find('th:eq(0)').hide();
        $("#Dynamic_Grid #grid tbody tr").find('td:eq(0)').each(function () {
            $(this).hide();
            if ($(this).html() != undefined && $(this).html().trim() == "") {
                $(this).parent().find('.z-manage').hide();
                $(this).parent().find('.z-edit').hide();
                $(this).parent().find('.z-delete').hide();
                $(this).parent().find('.z-copy').hide();
            }

        });
    },
    ManageFranchiseProductTabsGridColumns: function (id) {
        $("#" + id + " #grid tbody tr").each(function () {
            $(this).find('.z-manage').parent().hide();
            $(this).find('.z-edit').parent().hide();
            $(this).find('.z-delete').parent().hide();
            $(this).find('.z-copy').parent().hide();
            $("#" + id + " #grid thead tr").find('th:eq(' + $(this).find('.z-manage').parent().index() + ')').hide();
            $("#" + id + " #grid thead tr").find('th:eq(' + $(this).find('.z-edit').parent().index() + ')').hide();
            $("#" + id + " #grid thead tr").find('th:eq(' + $(this).find('.z-delete').parent().index() + ')').hide();
            $("#" + id + " #grid thead tr").find('th:eq(' + $(this).find('.z-copy').parent().index() + ')').hide();
        });
    },
    ShowHideGiftCard: function () {
        var isGiftCard = $("#IsGiftCard").val();
        if (isGiftCard != null && isGiftCard.toLowerCase() == "true") {
            $("#divGiftCards").show();
        }
        else {
            $("#divGiftCards").hide();
        }
    }
}

