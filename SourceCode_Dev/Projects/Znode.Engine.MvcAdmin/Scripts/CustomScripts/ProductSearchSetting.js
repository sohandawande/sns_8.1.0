﻿var SearchSetting = {
    SaveBoostValues: function (name) {
        var ids = new Array();
        var values = new Array();
        var errorCount = 0;
        $("#grid tbody tr input[type=text]").each(function () {
            ids.push((this.id).split('_')[1])

            if (parseFloat($(this).val()).toFixed(2) > 1000) {

                errorCount = 1;
            }
            values.push($(this).val());
        });

        if (errorCount > 0) {
            CommonHelper.DisplayNotificationMessagesHelper("Please enter a valid boost value – Valid values are: 0.00 to 1000.00", "error");
            return false;
        }

        var jIds = JSON.stringify(ids);
        var jValues = JSON.stringify(values);

        $.ajax({
            url: "/ProductSearchSettings/SaveProductBoostValues/",
            type: "get",
            dataType: "json",
            data: { "ids": jIds, "values": jValues, "name": name },
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.responce !== "false") {
                    switch (data.responce) {
                        case "product":
                            $("#productLevelSettingTab").click();
                            break;
                        case "category":
                            $("#categoryLevelSettingTab").click();
                            break;
                        case "fields":
                            $("#fieldsLevelSettingTab").click();
                            break;
                    }

                    CommonHelper.DisplayNotificationMessagesHelper("Changes saved successfully.", "success");
                }
                else {
                    CommonHelper.DisplayNotificationMessagesHelper("Changes Not Saved or No Changes Made", "error");
                }
            },
            error: function (msg) {

            }
        });
    },

    BoostValueValidate: function () {
        $("#grid tbody tr input[type=text]").unbind("keydown");
        $("#grid tbody tr input[type=text]").keydown(function (e) {
           
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                // Allow: Ctrl+A
                (e.keyCode == 65 && e.ctrlKey === true) ||
                // Allow: home, end, left, right, down, up
                    (e.keyCode >= 35 && e.keyCode <= 40)) {
                // let it happen, don't do anything
               
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }

            var val = $(this).val();
            if (parseFloat(val).toFixed(2) > 1000) {
                CommonHelper.DisplayNotificationMessagesHelper("Please enter a valid boost value – Valid values are: 0.00 to 1000.00", "error");
                return false;
            }
        });
    }
}

