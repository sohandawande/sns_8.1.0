﻿ProviderEngine = {
    GetPromotionType: function () {
        var promotionTypeName = $('#ddlPromotionType').val()
        App.Api.getPromotionTypeValueByPromotionName(promotionTypeName, function (response) {
            $("#Name").val(response.Name);
            $("#ClassName").val(response.ClassName);
            $("#Description").val(response.Description);
            $("#ClassType").val(response.ClassType);
        });
    },

    GetTaxType: function () {
        var taxTypeName = $('#ddlTaxType').val()
        App.Api.getTaxTypeValueByName(taxTypeName, function (response) {
            $("#Name").val(response.Name);
            $("#ClassName").val(response.ClassName);
            $("#Description").val(response.Description);
        });
    },

    GetSupplierType: function () {
        var supplierTypeName = $('#ddlSupplierType').val()
        App.Api.GetSupplierTypeByName(supplierTypeName, function (response) {
            $("#Name").val(response.Name);
            $("#ClassName").val(response.ClassName);
            $("#Description").val(response.Description);
        });
    },

    GetShippingType: function () {
        var shippingTypeName = $('#ddlShippingType').val()
        App.Api.GetShippingTypeByName(shippingTypeName, function (response) {
            $("#Name").val(response.Name);
            $("#ClassName").val(response.ClassName);
            $("#Description").val(response.Description);
        });
    },
}
