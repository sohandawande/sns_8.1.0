﻿var Personalization = {
    SearchProducts: function (userType, tabMode) {
        var url = "";
        var productId = $("#ProductId").val();
        var portalId = $("#PortalId").val();
        var categoryId = $("#CategoryId option:selected").val();
        var productName = $("#ProductName").val();
        var sku = $("#SKU").val();

        var associatedProductHtml = $("#AssignedId").html();
        var actionName;

        //the values 4 and 5 are read from enum ProductRelationCode
        if (tabMode == '4') {
            actionName = 'ManageFrequentlyBought';
        }
        else if (tabMode == '5') {
            actionName = 'ManageYouMayLikeAlso'
        }
        if (userType == "FranchiseAdmin") {
            url = "/FranchiseAdmin/FranchisePersonalization/" + actionName;
        }
        else {
            url = "/Personalization/" + actionName;
        }
        App.Api.getProductSearchResult(url, productId, portalId, categoryId, productName, sku, function (response) {
            $("#ManageCrossSell").html(response);
            if (associatedProductHtml != null && associatedProductHtml.trim() != "") {
                $(associatedProductHtml + 'option').each(function () {
                    var id = $(this).val();
                    $(response).find('#UnAssignedId option').each(function () {
                        if ($(this).val() == id) {
                            $("#UnAssignedId option[value='" + $(this).val() + "']").remove();
                        }
                    });
                });
            }
            if (associatedProductHtml != null && associatedProductHtml.trim() != "" || associatedProductHtml != undefined) {
                $("#AssignedId").html(associatedProductHtml);
            }
        });
    },

    ClearProducts: function (userType, tabMode) {
        var url = "";
        var productId = $("#ProductId").val();
        var portalId = $("#PortalId").val();
        var categoryId = 0;
        var productName = "";
        var sku = "";

        var associatedProductHtml = $("#AssignedId").html();
        var actionName;

        //the values 4 and 5 are read from enum ProductRelationCode
        if (tabMode == '4') {
            actionName = 'ManageFrequentlyBought';
        }
        else if (tabMode == '5') {
            actionName = 'ManageYouMayLikeAlso'
        }
        if (userType == "FranchiseAdmin") {
            url = "/FranchiseAdmin/FranchisePersonalization/" + actionName;
        }
        else {
            url = "/Personalization/" + actionName;
        }
        App.Api.getProductSearchResult(url, productId, portalId, categoryId, productName, sku, function (response) {
            $("#ManageCrossSell").html(response);
            if (associatedProductHtml != null && associatedProductHtml.trim() != "") {
                $(associatedProductHtml + 'option').each(function () {
                    var id = $(this).val();
                    $(response).find('#UnAssignedId option').each(function () {
                        if ($(this).val() == id) {
                            $("#UnAssignedId option[value='" + $(this).val() + "']").remove();
                        }
                    });
                });
            }               
            if (associatedProductHtml != null && associatedProductHtml.trim() != "" || associatedProductHtml != undefined) {
                $("#AssignedId").html(associatedProductHtml);
            }  
        });
    },
}

$(document).on("change", "#ddlPortal", function () { $(this).closest("form").submit(); })

$("#btnCreateAssociatedProducts").on("click", function () {
    try {
        return Validate();
    }
    catch (err) {
        return true;
    }
});