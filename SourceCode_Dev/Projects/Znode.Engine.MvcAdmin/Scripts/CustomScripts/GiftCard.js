﻿GiftCard = {
    CheckGiftCardCreateCheckBox: function () {
        if ($("#EnableToCustomerAccount").length > 0) {
            if ($($("#EnableToCustomerAccount")[0]).is(":checked")) {
                $("#ShowAccountId").removeClass("hidden");
            } else {
                $("#ShowAccountId").addClass("hidden");
                $("#AccountId").val('');
            }
        }
    },

    ValidateGiftCardAttributes: function () {
        var returnVariable = true;
        if ($("#ExpirationDate").val() != "") {
            
            var todaysDate = new Date();
            var month = todaysDate.getMonth() + 1;
            var day = todaysDate.getDate();

            var todaysConvertedDate = todaysDate.getFullYear() + '/' +
                (month < 10 ? '0' : '') + month + '/' +
                (day < 10 ? '0' : '') + day;

            var expirationDate = $("#ExpirationDate").val();
            var _brouser = window.navigator.userAgent;
            var msie = _brouser.indexOf("MSIE");

            if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
                expirationDate = expirationDate.split('-')[2] + "/" + expirationDate.split('-')[1] + "/" + expirationDate.split('-')[0];
            }
            else if (_brouser.indexOf("Safari") > -1) {
                expirationDate = expirationDate;
            } else {
                if (expirationDate.indexOf('-') >= 0) {
                    expirationDate = expirationDate.split('-')[0] + "/" + expirationDate.split('-')[1] + "/" + expirationDate.split('-')[2];
                }
            }

            var curentDate = (isNaN(todaysConvertedDate) ? Date.parse(todaysConvertedDate) : eval(todaysConvertedDate));
            var enteredDate = (isNaN(expirationDate) ? Date.parse(expirationDate) : eval(expirationDate));
            
            if (enteredDate >= curentDate) {
                returnVariable = GiftCard.ValidateAccountId();
            }
            else {
                $("#valExpirationDate").text('').removeClass("field-validation-error");
                $("#valExpirationDate").text("Expiration date must be greater than current date.").addClass("field-validation-error");
                $("#valExpirationDate").show();
                returnVariable = GiftCard.ValidateAccountId();
                returnVariable = false;
            }
        }
        else if ($("#ExpirationDate").val() == "") {
            $("#valExpirationDate").text('').removeClass("field-validation-error");
            $("#valExpirationDate").text("Enter Expiration Date.").addClass("field-validation-error");
            $("#valExpirationDate").show();
            returnVariable = GiftCard.ValidateAccountId();
        }
        else {
            returnVariable = GiftCard.ValidateAccountId();
        }
        return returnVariable;
    },
    ValidateAccountId: function () {
        if ($($("#EnableToCustomerAccount")[0]).is(":checked")) {
            var accountId = $("#AccountId").val();
            if (accountId.length < 1) {
                $("#valAccountId").text('').removeClass("field-validation-error");
                $("#valAccountId").text("Enter valid Account ID").addClass("field-validation-error");
                $("#valAccountId").show();
                return false;
            }
            else if (isNaN(accountId) || accountId <= 0) {
                $("#valAccountId").text('').removeClass("field-validation-error");
                $("#valAccountId").text("Must be a whole number").addClass("field-validation-error");
            }
            else {
                $("#valAccountId").hide();
                return true;
            }
        }
    },
    ShowTextBox: function () {
        if ($('#EnableToCustomerAccount').prop("checked") == true) {
            $('#ShowAccountId').removeClass("hidden");
        } else {
            $('#ShowAccountId').addClass("hidden");
            $("#AccountId").val('');
        }
    },
}
$(document).ready(function () {
    GiftCard.CheckGiftCardCreateCheckBox();
});