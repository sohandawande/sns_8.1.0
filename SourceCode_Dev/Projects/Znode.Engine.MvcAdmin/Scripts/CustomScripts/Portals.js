﻿Portal = {
    loadCountryList: function () {
        $("#grid tbody tr").find("input[id='rowcheck_US']").prop("checked", true)
    },
    load: function () {

    },

    BindCssList: function (ddlCss, ddlThemes, defaultValue) {
        var themeId = $(ddlThemes + " option:selected").val();
        App.Api.getPortalCssListByThemeId(themeId, function (response) {
            $(ddlCss).empty();
            for (var i = 0; i < response.length; i++) {
                $(ddlCss).append("<option value='" + response[i].Value + "'>" + response[i].Text + "</option>");
            }
            if (response.length < 1) {
                $(ddlCss).append("<option value=''>" + defaultValue +"</option>");
            }
        });
    },
    BindCurrencyInfo: function (ddlCurrency, txtCurrencySuffix, lblCurrencyPreview) {
        var currencyId = $("#" + ddlCurrency.id + " option:selected").val();
        App.Api.getCurrencyInformation(currencyId, function (response) {
            $(txtCurrencySuffix).val(response.currencySuffix);
            $(lblCurrencyPreview).text(response.currencyPreview);
        });
    },
    AddCountryToPortal: function (portalId) {
        var billingCountryCodes = "";
        var shippingCountryCodes = "";
        var portalID = portalId;
        DynamicGrid.selectedRowByIndex(0, function (res) {
            billingList = [res.length];
            for (var i = 0; i < res.length; i++) {
                billingCountryCodes = billingCountryCodes + res[i].values + ",";
            }
        });
        DynamicGrid.selectedRowByIndex(1, function (res) {
            shippingList = [res.length];
            for (var i = 0; i < res.length; i++) {
                shippingCountryCodes = shippingCountryCodes + res[i].values + ",";
            }
        });

        billingCountryCodes = billingCountryCodes.substr(0, billingCountryCodes.length - 1);
        shippingCountryCodes = shippingCountryCodes.substr(0, shippingCountryCodes.length - 1);

        var type = "error";
        if ((billingCountryCodes.length + shippingCountryCodes.length) > 0) {
            var url = "/Stores/AddPortalCountry/" + portalId;

            if (billingCountryCodes.length < 1) {
                billingCountryCodes = "Empty";
            }
            else if (shippingCountryCodes.length < 1) {
                shippingCountryCodes = "Empty";
            }

            App.Api.addPortalCountries(url, billingCountryCodes, shippingCountryCodes, function (response) {
                if (response.sucess) {
                    type = "success";
                    var url = "/Stores/Manage/" + response.id + "?tabMode=" + response.selectedTab;
                    window.location.href = url;
                }
                CommonHelper.DisplayNotificationMessagesHelper(response.message, type, response.isFadeOut, 5000);
            });
        }
        else {
            CommonHelper.DisplayNotificationMessagesHelper("At least one country should be selected.", type, false, 5000);
        }
    },
    ShowHideLogo: function (actionName) {

        $("#radCurrentImage").prop('checked', true);
        if (actionName == "Create") {
            $("#uploadNewImage").show();
        }
        else {
            $("#uploadNewImage").hide();
        }
    },
    ShowHideMobileLogo: function (actionName) {
        $("#radCurrentMobileImage").prop('checked', true);
        if (actionName == "Create") {
            $("#uploadNewMobileImage").show();
        }
        else {
            $("#uploadNewMobileImage").hide();
        }
    },
    ShowHideLogoDisplay: function () {
        $("#radCurrentImageDisplay").prop("checked", true);
        if ($('#radUploadNewImageDisplay').is(':checked')) {
            $("#uploadNewImageDisplay").show();
        }
        else {
            $("#uploadNewImageDisplay").hide();
        }
    },
    SetDefaultOrderState: function (RequiresManualApproval, pendingApproval) {

        if ($(RequiresManualApproval).prop("checked") == true) {
            $('#ddlOrderState').append('<option value="50" selected=selected>' + pendingApproval + '</option>');
            $('#ddlOrderState').attr("disabled", true);
        } else {
            $('#ddlOrderState').attr("disabled", false);
            $('select#ddlOrderState').find("option[value='50']").remove();
        }
    },
    UploadNewImageDisplay: function () {
        if ($('#radUploadNewImageDisplay').is(':checked')) {
            $("#uploadNewImageDisplay").show();
        }
    },
    HideUploadNewImageDisplay: function () {
        if ($('#radCurrentImageDisplay').is(':checked')) {
            $("#uploadNewImageDisplay").hide();
        }
    },
    UploadNewImage: function () {
        if ($('#radUploadNewImage').is(':checked')) {
            $("#uploadNewImage").show();
            $("#logoImage").hide();
        }
    },
    HideUploadNewImage: function () {
        if ($('#radCurrentImage').is(':checked')) {
            $("#uploadNewImage").hide();
            $("#logoImage").show();
        }
    },
    UploadNewMobileImage: function () {
        if ($('#radUploadNewMobileImage').is(':checked')) {
            $("#uploadNewMobileImage").show();
            $("#mobileLogoImage").hide();
        }
    },
    HideUploadNewMobileImage: function () {
        if ($('#radCurrentMobileImage').is(':checked')) {
            $("#uploadNewMobileImage").hide();
            $("#mobileLogoImage").show();
        }
    },
    SetRequiresManualApproval: function (pendingApproval, pendingApprovalText) {

        if (pendingApproval == "50") {
            $("#RequiresManualApproval").prop("checked", "checked");
            this.SetDefaultOrderState($("#RequiresManualApproval"), pendingApprovalText);
        }
    },
    ResetUserNamePassword: function () {
        if ($("#hiddenUpsUserName").val() == null || $("#hiddenUpsUserName").val() == "") {
            $("#UpsUsername").val("");
        }
        if ($("#hiddenUpsUserName").val() == null || $("#hiddenUpsUserName").val() == "") {
            $("#UpsPassword").val("");
        }
    },
    ResetUserNamePasswordSMTP: function () {
        if ($("#smtpUsername").val() == null || $("#smtpUsername").val() == "") {
            $("#SmtpUsername").val("");
        }
        if ($("#smtpPassword").val() == null || $("#smtpPassword").val() == "") {
            $("#SmtpPassword").val("");
        }
    },
    AddPreviewUrl: function () {
        $("#grid tbody tr").each(function () {
            if ($(this).find('.z-view').attr("href") === "http://#") {
                $(this).find('.z-view').attr("href", "#");
            }
            else {
                $(this).find('.z-view').attr('target', '_blank');
            }
            var url = $(this).find('.z-view').attr("href");
            if (url.indexOf("http://http://") >= 0) {
                $(this).find('.z-view').attr("href", url.replace("http://http://", "http://"));
            }

        });
    },
    DisableValidateAddress: function () {
        if ($("#EnableAddressValidation").prop('checked') == false) {
            $('#RequireValidatedAddress').attr('checked', false);
            $('#RequireValidatedAddress').attr("disabled", true);
        }
        else {
            $('#RequireValidatedAddress').attr("disabled", false);
        }
    },
    ShowHideProductCompare: function () {
        if ($("#IsEnableCompare").prop("checked") == true) {
            $("#divProductCompare").show();
        }
        else {
            $("#divProductCompare").hide();
        }
    },
}
