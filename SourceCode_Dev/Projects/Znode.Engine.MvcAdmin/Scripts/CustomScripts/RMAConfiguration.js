﻿RMAConfiguration = {

    ShowReasonForReturnPopUp: function () {
        App.Api.createReasonForReturn(function (response) {
            $("#divAddReasonForReturn").html(response);
            $("#divAddReasonForReturn").dialog({
                title: "Create a Return Reason",
                resizable: false,
                modal: true,
                create: function () { $(this).closest(".ui-dialog").addClass("ui-sm-popup"); }
            });
        });
        return false;
    },

    EditReasonForReturnPopup: function (reasonforreturnhref) {

        var reasonForReturnId = reasonforreturnhref.split("/")[3];
        if (parseInt(reasonForReturnId) > 0)
            App.Api.editReasonForReturn(reasonForReturnId, function (response) {
                $("#divAddReasonForReturn").html(response);
                $("#divAddReasonForReturn").dialog({
                    title: "Create a Return Reason",
                    resizable: false,
                    modal: true,
                    create: function () { $(this).closest(".ui-dialog").addClass("ui-sm-popup"); }
                });
            });
        return false;
    },

    CloseReasonForReturn: function () {
        $(".ui-dialog-titlebar-close").click();
        $("#divAddReasonForReturn").dialog("close");
        return false;
    },

    CloseRequestStatus: function () {
        $(".ui-dialog-titlebar-close").click();
        $("#divAddRequestStatus").dialog("close");
        return false;
    },

    EditRequestStatusPopup: function (requeststatushref) {
        var requestStatusId = requeststatushref.split("/")[3];
        if (parseInt(requestStatusId) > 0)
            App.Api.editRequestStatus(requestStatusId, function (response) {
                $("#divAddRequestStatus").html(response);
                $("#divAddRequestStatus").dialog({
                    title: "Edit Status",
                    resizable: false,
                    modal: true,
                    create: function () { $(this).closest(".ui-dialog").addClass("ui-md-popup"); }
                });
            });
        return false;
    },

    RequestStatusGridPaging: function () {
        $("#grid-container #grid tbody tr td").find(".z-edit").click(function (e) {
            e.preventDefault();
            RMAConfiguration.EditRequestStatusPopup($(this).attr("href"));
        });
    },

    ReasonForReturnGridPaging: function () {
        $("#grid-container #grid tbody tr td").find(".z-edit").click(function (e) {
            e.preventDefault();
            RMAConfiguration.EditReasonForReturnPopup($(this).attr("href"));
        });
    },

    CreateEditGridView: function () {
        $("#grid-container #grid tbody tr td").find(".z-edit").click(function (e) {
            e.preventDefault();
        })
    },

    IssueGiftCardUpdate: function (data) {
        var type = "error";
        if (data != undefined) {
            type = "error";

            if (data.sucess) {
                type = "success";
            }
            document.getElementById("issuegiftcard").innerHTML = data.Response;
            try { if (data.message == "") { $('#MessageBoxContainerId').html(''); } } catch (err) { }
            CommonHelper.DisplayNotificationMessagesHelper(data.message, type, data.isFadeOut, 5000);
        }
    }

}

