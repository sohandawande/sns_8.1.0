﻿var Payment = {
    Init: function (userType) {
        $("#ddlPaymentTypes").unbind("change")
        $("#ddlPaymentTypes").change(function () {
            Payment.getPaymentTypesForm($(this).val(), userType);
        });

        $("#ddlPaymentGetways").unbind("change")
        $("#ddlPaymentGetways").change(function () {
            Payment.getPaymentGetwayForm($(this).val(), userType);
        });
    },

    getPaymentTypesForm: function (paymentTypeId, userType) {
        var url = ""
        if (userType == "FranchiseAdmin") {
            url = "/FranchiseAdmin/FranchisePayment/GetPaymentTypeForm";
        }
        else {
            url = "/Payment/GetPaymentTypeForm";
        }
        App.Api.getPaymentTypeForm(url, paymentTypeId, function (res) {
            $("#paymenttypeform-container").html(res);
            Payment.getPaymentGetwayForm($("#ddlPaymentGetways").val(), userType);
            Payment.Init(userType);
        });
    },

    enterCharactersOnly: function () {
        $("#AdditionalFee").keydown(function (e) { // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                // Allow: Ctrl+A
                (e.keyCode == 65 && e.ctrlKey === true) ||
                // Allow: home, end, left, right, down, up
                    (e.keyCode >= 35 && e.keyCode <= 40)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        })
    },

    IsValidFees: function () {

        var additionalFee = $('#AdditionalFee').val();

        if (additionalFee != undefined && additionalFee != "") {
            var regex = "[0-9 -()+]+$";

            if (additionalFee.trim().length > 0) {
                $("#valAdditionalAmount").text('').removeClass("field-validation-error");
                $("#valAdditionalAmount").hide();

                if (CommonHelper.IsMatchRegularExpressionString(additionalFee, regex) == false) {
                    $("#valAdditionalAmount").text("enter values between 0 to 999999999.00").addClass("field-validation-error");
                    $("#valAdditionalAmount").show();
                    return false;
                }
            }
            else {
                return false;
            }
        }
        return true;

    },

    getPaymentGetwayForm: function (getwayId, userType) {
        var url = ""
        if (userType == "FranchiseAdmin") {
            url = "/FranchiseAdmin/FranchisePayment/GetPaymentGetwayForm";
        }
        else {
            url = "/Payment/GetPaymentGetwayForm";
        }
        App.Api.getPaymentGetwayForm(url, getwayId, function (res) {
            $("#PaymentGetwayForm-container").html(res);
        });
    },

    showTestModeMessage: function () {
        if ($("#TestMode").prop('checked') == true) {
            $("#testModeHiddenMessage").dialog({
                title: "Payment Gateway Settings",
                resizable: false,
                modal: true,
                create: function () { $(this).closest(".ui-dialog").addClass("ui-lg-popup"); },
                buttons: {
                    Ok: function () {
                        $(this).dialog("close");
                    }
                }
            });
        }
    }
}



