﻿Banner = {
    DisplayMessageKeyTextBox: function () {
        if ($('#ddlBannerKeys').val() == '+1')
        {
            $('#ddlBannerKeys').hide();
            $('#btnBannerMessageKey').show();
            $('#txtMessagekey').show();           
        }
    },
    ClearSelection: function () {
        $('#ddlBannerKeys').show();
        $('#btnBannerMessageKey').hide();
        $('#txtMessagekey').hide();
        $('#ddlBannerKeys').val('0');
        $('#txtMessagekey').val('');
        $("span[data-valmsg-for='MessageKey']").html('');
    },
}
