﻿Diagnostics = {

    HideOrShowExceptionLog: function (log) {
        if (log != "") {
            $("#exception-log").show();
        }
    },

    SendDiagnosticsEmail: function () {
        if ($(".field-validation-error").length) {
            return false;
        }
        overlayforwaitblock();
        var caseNumber = $("#CaseNumber").val();
        App.Api.getEmailDiagnostics(caseNumber, function (response) {
            overlayforwaitnone();
            if (response.IsModelError) {
                $("#case-number-error").removeClass("field-validation-valid").addClass("field-validation-error");
                $("#case-number-error").html(response.Log)
            }
            else if (response.Result) {
                $("#smpt-account").html(response.Log);
            }
            else {
                $("#exception-log").show();
                $("#log").html(response.Log);
            }
        })
    },
    
    Showtrace: function()
    {
        var url = GlobalVariables.ZnodeSiteAdminUrl + "/Trace.axd?id=0";
        App.Api.showDiagnosticsTrace(url, function (response) {
            $("#trace").html(response);
        })
    }
}