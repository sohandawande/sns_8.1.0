﻿var ListViewPage = function () {
    _this = this;

    this.MoveFromUnassignedToAssigned = function () {
        try {
            if (!validateCondForAssign()) {
                return;
            }
        }
        catch (e) {
            console.log("ValidateCondForAssign() not found, performing normal operations.")
        }
        var options = $("[id*=UnAssignedId] option:selected");
        var unAssignListLength = $("#UnAssignedId option").length;

        _this.ErrorClass(options);

        if ($("#UnAssignedId option").length == 0) {
            $("#mvUnassignToAssignBtn").addClass('DisableNextBtn').removeClass('ActiveNextBtn').attr('disabled', 'disabled');
        }
        else {
            if (options.length != 0) {
                if (document.URL.toLowerCase().indexOf("managefrequentlybought") > -1) {

                    if ($("#AssignedId option").length + $("#UnAssignedId option:selected").length <= 2) {
                        var counter = 0;
                        for (var i = 0; i < options.length; i++) {
                            var opt = $(options[i]).clone();
                            $(options[i]).remove();
                            $("#AssignedId").append(opt);

                            $("#ValMessage").text("").removeClass("field-validation-error");
                            $("#errorMessage").text("").removeClass("field-validation-error");

                            $("#AssignedId option").attr("selected", "selected");
                            counter++;
                        }

                        if (unAssignListLength == counter) {
                            $("#mvUnassignToAssignBtn").addClass('DisableNextBtn').removeClass('ActiveNextBtn').attr('disabled', 'disabled');
                            $("#mvAssignToUnassignBtn").removeClass('DisablePrevBtn').addClass('ActivePrevBtn').removeAttr('disabled');
                        }
                        else { $("#mvAssignToUnassignBtn").removeClass('DisablePrevBtn').addClass('ActivePrevBtn').removeAttr('disabled'); }
                        var assignListLength = $("#AssignedId option").length;
                        if (assignListLength >= 2) {
                            $("#move-up").removeClass('DisableUpBtn').addClass('ActiveUpBtn').removeAttr('disabled');
                            $("#move-down").removeClass('DisableDownBtn').addClass('ActiveDownBtn').removeAttr('disabled');
                        }
                    }
                } else {
                    var counter = 0;
                    for (var i = 0; i < options.length; i++) {
                        var opt = $(options[i]).clone();
                        $(options[i]).remove();
                        $("#AssignedId").append(opt);

                        $("#ValMessage").text("").removeClass("field-validation-error");
                        $("#errorMessage").text("").removeClass("field-validation-error");

                        $("#AssignedId option").attr("selected", "selected");
                        counter++;
                    }

                    if (unAssignListLength == counter) {
                        $("#mvUnassignToAssignBtn").addClass('DisableNextBtn').removeClass('ActiveNextBtn').attr('disabled', 'disabled');
                        $("#mvAssignToUnassignBtn").removeClass('DisablePrevBtn').addClass('ActivePrevBtn').removeAttr('disabled');
                    }
                    else { $("#mvAssignToUnassignBtn").removeClass('DisablePrevBtn').addClass('ActivePrevBtn').removeAttr('disabled'); }
                    var assignListLength = $("#AssignedId option").length;
                    if (assignListLength >= 2) {
                        $("#move-up").removeClass('DisableUpBtn').addClass('ActiveUpBtn').removeAttr('disabled');
                        $("#move-down").removeClass('DisableDownBtn').addClass('ActiveDownBtn').removeAttr('disabled');
                    }
                }
            } else {
                _this.ErrorClass();

            }
        }
    }

    this.MoveFromAssignedToUnassigned = function () {
        try {
            if (!validateCondForUnAssign()) {
                return;
            }
        }
        catch (e) {
            console.log("ValidateCondForUnAssign() not found, performing normal operations.")
        }
        var options = $("[id*=AssignedId] option:selected");
        var assignListLength = $("#AssignedId option").length;

        if ($("#AssignedId option").length == 0) {
            $("#mvAssignToUnassignBtn").addClass('DisablePrevBtn').removeClass('ActivePrevBtn').attr('disabled', 'disabled');
        }
        else {
            if (options.length != 0) {
                var counter = 0;
                for (var i = 0; i < options.length; i++) {
                    var opt = $(options[i]).clone();
                    $(options[i]).remove();
                    $("[id*=UnAssignedId]").append(opt);
                    $("#UnAssignedId option").attr("selected", false);
                    counter++;
                }
                if (assignListLength == counter) {
                    $("#mvAssignToUnassignBtn").addClass('DisablePrevBtn').removeClass('ActivePrevBtn').attr('disabled', 'disabled');
                    $("#mvUnassignToAssignBtn").removeClass('DisableNextBtn').addClass('ActiveNextBtn').removeAttr('disabled');
                }
                else { $("#mvUnassignToAssignBtn").removeClass('DisableNextBtn').addClass('ActiveNextBtn').removeAttr('disabled'); }
                var remainingAssignItem = $("#AssignedId option").length;
                if (remainingAssignItem <= 1) {
                    $("#move-up").addClass('DisableUpBtn').removeClass('ActiveUpBtn').attr('disabled', 'disabled');
                    $("#move-down").addClass('DisableDownBtn').removeClass('ActiveDownBtn').attr('disabled', 'disabled');
                }
            } else {
                _this.ErrorClass(options);
            }
        }
    }

    this.MoveUp = function () {
        var options = $("[id*=AssignedId] option:selected");
        if (options.length != 0) {
            $("#AssignedId option:selected").each(function () {
                var listItem = $(this);
                var listItemPosition = $("#AssignedId option").index(listItem) + 1;

                if (listItemPosition == 1) return false;

                listItem.insertBefore(listItem.prev());
            });
        }
    }

    this.MoveDown = function () {
        var options = $("[id*=AssignedId] option:selected");
        var itemsCount = $("#AssignedId option").length;
        if (options.length != 0) {
            $($("#AssignedId option:selected").get().reverse()).each(function () {
                var listItem = $(this);
                var listItemPosition = $("#AssignedId option").index(listItem) + 1;

                if (listItemPosition == itemsCount) return false;

                listItem.insertAfter(listItem.next());
            });
        }
    }

    this.ErrorClass = function (options) {
        if (options.length < 1) {
            $("#errorMessage").html("Please select at least one item");
            if (!$("#errorMessage").hasClass('field-validation-error')) {
                $("#errorMessage").addClass('field-validation-error');
            }
        }
    }
}