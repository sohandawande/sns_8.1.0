﻿/*Script to draw graph*/
var chart = "";
var globalReportGraphData = "";
var globalTotalRecordCount = 0;
var globalReportGraphType = "Pie";
var globalGraphTitle = "";
var graphReport = {};
graphReport = {
    GraphReport: function (reportId, chartType, userType) {

        globalReportGraphType = chartType;
        $("#sliders").hide();
        var xAxis = $("#xAxisFields").val();
        var yAxis = $("#yAxisFields").val();
        if (userType == "FranchiseAdmin") {
            url = "/FranchiseAdmin/FranchiseReports/GetCharts";
        }
        else if (userType == "Admin") {
            url = "/MyReports/GetCharts";
        }

        App.Api.GetGraphData(url, reportId, xAxis, yAxis, function (data) {
            if (data.totalRecordCount != undefined && data.totalRecordCount != 0) {
                if (chartType != "" && chartType != null) {
                    $("#ReportGraph").show();
                    globalReportGraphData = data.reportGraphData;
                    globalTotalRecordCount = data.totalRecordCount;
                    globalGraphTitle = data.graphTitle;
                    if (chartType == "Pie") {
                        graphReport.ShowGraphContainer();
                        graphReport.ReportViewInPieChart(data.reportGraphData, data.totalRecordCount, data.graphTitle);
                    }
                    else if (chartType == "3DBar") {
                        graphReport.ShowGraphContainer();
                        graphReport.ReportViewIn3DBarChart(data.reportGraphData, data.totalRecordCount, data.graphTitle);
                    }
                    else {
                        $('#reportGraphcontainer').hide();
                    }

                } else {
                    $('#reportGraphcontainer').hide();
                }
            }
            else {
                $("#ReportGraph").hide();
                $("#graphCheckboxContainer").hide();
            }
        });

    },
    ReportViewInPieChart: function (reportGraphData, totalRecordCount, graphTitle, legend, options3d, allowPointSelect, dataLabels) {
        if (legend == undefined) {
            legend = $("#Legend").prop("checked") ? true : false;
            options3d = $("#options3d").prop("checked") ? true : false;
            allowPointSelect = $("#AllowSelection").prop("checked") ? true : false;
            dataLabels = $("#DataLabel").prop("checked") ? true : false;
        }
        var dynamicReportData = new Array();
        var totalPercentage = 0;
        for (var i = 0; i < reportGraphData.length; i++) {
            var curPercentage = ((reportGraphData[i].RecordCount / totalRecordCount) * 100).toFixed(2);
            if (parseFloat(curPercentage) != 0) {
                dynamicReportData.push(new Array(reportGraphData[i].GroupClause, parseFloat(curPercentage)));
            }
        }
        chart = new Highcharts.Chart({
            chart: {
                renderTo: 'reportGraphcontainer',
                type: 'pie',
                options3d:
                    {
                        enabled: options3d,
                        alpha: 45,
                        beta: 0
                    }
            },
            credits: {
                enabled: false
            },
            title: {
                text: graphTitle
            },
            tooltip: {
                pointFormat: '<b>{point.percentage:.1f}%</b>'
            },
            legend: {
                enabled: legend,
                itemWidth: 200,
                labelFormatter: function () { return this.name.slice(0, 25) + '...' }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: allowPointSelect,
                    cursor: 'pointer',
                    depth: 35,
                    showInLegend: legend,
                    dataLabels: {
                        enabled: dataLabels,
                        style: { width: 200 }
                    }
                }
            },
            series: [{
                type: 'pie',
                name: '',
                event: {
                    enabled: false
                },
                data: dynamicReportData
            }]
        });
        $("[text-anchor='end']").each(function () {
            if ($(this).text() == "Highcharts.com") {
                $(this).remove();
            }
        });
    },
    ReportViewIn3DBarChart: function (reportGraphData, totalRecordCount, graphTitle, legend, options3d, allowPointSelect, dataLabels) {
        if (legend == undefined) {
            legend = $("#Legend").prop("checked") ? true : false;
            options3d = $("#options3d").prop("checked") ? true : false;
            allowPointSelect = $("#AllowSelection").prop("checked") ? true : false;
            dataLabels = $("#DataLabel").prop("checked") ? true : false;
        }
        if ($("#options3d").prop("checked")) {
            $("#sliders").show();
        }
        else {
            $("#sliders").hide();
        }
        var dynamicReportData = new Array();
        for (var i = 0; i < reportGraphData.length; i++) {
            if (parseFloat(reportGraphData[i].RecordCount) != 0) {
                dynamicReportData.push(new Array(reportGraphData[i].GroupClause, reportGraphData[i].RecordCount));
            }
        }
        // Set up the chart
        chart = new Highcharts.Chart({
            chart: {
                renderTo: 'reportGraphcontainer',
                type: 'column',
                margin: 75,
                options3d: {
                    enabled: options3d,
                    alpha: 45,
                    beta: 0,
                    depth: 50,
                    viewDistance: 25
                }
            },
            credits: {
                enabled: false
            },
            title: {
                text: graphTitle
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                type: 'category',
                labels: {
                    enabled: dataLabels,
                    rotation: -40,
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif',
                        width: '200px !important'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: graphTitle
                }
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                column: {
                    depth: 25
                }
            },
            series: [{
                data: dynamicReportData,
                dataLabels: {
                    enabled: dataLabels,
                    rotation: -90,
                    color: '#FFFFFF',
                    align: 'right',
                    x: 4,
                    y: 10
                }
            }]
        });
        $("[text-anchor='end']").each(function () {
            if ($(this).text() == "Highcharts.com") {
                $(this).remove();
            }
        });
    },
    showValues: function () {
        $('#R0-value').html(chart.options.chart.options3d.alpha);
        $('#R1-value').html(chart.options.chart.options3d.beta);
    },
    ShowReportView: function (control, userType) {
        if (control.checked) {
            var reportNo = getParameterByName('reportno');
            graphReport.GraphReport(parseInt(reportNo), "Pie", userType);
            localStorage.setItem('showGraphReportflag', true);
        }
        else
            localStorage.setItem('showGraphReportflag', false);
        $("#ReportGraph").slideToggle("slow");
    },
    ShowGraphContainer: function () {
        if (document.getElementById("reportGraphCheckBox").checked == true) {
            $('#reportGraphcontainer').show();
            $('#ReportGraph').slideDown();


        } else {
            $('#ReportGraph').slideUp();
            $('#reportGraphcontainer').hide();
        }
    },
    ShowGraphDetails: function (control) {
        if (control.id == "Legend") {
            if (control.checked) {
                localStorage.setItem('showLegend', true);
            }
            else {
                localStorage.setItem('showLegend', false);
            }
        }
        else if (control.id == "DataLabel") {
            if (control.checked) {
                localStorage.setItem('showDataLabel', true);
            }
            else {
                localStorage.setItem('showDataLabel', false);
            }
        }
        else if (control.id == "AllowSelection") {
            if (control.checked) {
                localStorage.setItem('showAllowSelection', true);
            }
            else {
                localStorage.setItem('showAllowSelection', false);
            }
        }
        else if (control.id == "options3d") {
            if (control.checked) {
                localStorage.setItem('showoptions3d', true);
            }
            else {
                localStorage.setItem('showoptions3d', false);
            }
        }
    }
}
$(document).on("change", "#charttype", function () {
    var chartType = $(this).val();
    var selectedClass = "";
    globalReportGraphType = chartType;
    $("#sliders").hide();
    $("#charttype a").each(function () {
        var typeClass = $(this).attr("class");
        typeClass = typeClass.replace("-selected", "");
        $(this).attr("class", typeClass);
    });
    selectedClass = $(this).attr("class");
    $(this).attr("class", selectedClass + "-selected");

    if (globalReportGraphType == "Pie") {
        graphReport.ReportViewInPieChart(globalReportGraphData, globalTotalRecordCount, globalGraphTitle);
    }
    else if (globalReportGraphType == "3DBar") {
        graphReport.ReportViewIn3DBarChart(globalReportGraphData, globalTotalRecordCount, globalGraphTitle);
    }

});

$(document).on("click", "#ReportViewType a", function () {
    var chartType = $(this).attr("data-graphtype");
    var selectedClass = "";
    reportTypeTileGrid = chartType;
    $("#sliders").hide();
    $("#GraphSetting input:checkbox").attr("checked", "checked");

    $("#ReportViewType a").each(function () {
        var typeClass = $(this).attr("class");
        typeClass = typeClass.replace("-selected", "");
        $(this).attr("class", typeClass);
    });
    selectedClass = $(this).attr("class");
    $(this).attr("class", selectedClass + "-selected");
    if (reportTypeTileGrid == "Tile") {
        ReportStudentTestGrid(0, reportTypeTileGrid);
    }
    else if (reportTypeTileGrid == "Grid") {
        ReportStudentTestGrid(0, reportTypeTileGrid);
    }

});
$(document).on("click", "input:checkbox", function () {
    var legend = true;
    var options3d = true;
    var allowPointSelect = true;
    var dataLabels = true;
    $('#GraphSetting input:checkbox').each(function () {
        var checkBoxId = $(this).attr("id");
        if (document.getElementById(checkBoxId).checked) {
            if (checkBoxId == "Legend") {
                legend = true;
                // chart.options.plotOptions.pie.showInLegend = true;
            }
            else if (checkBoxId == "DataLabel") {
                options3d = true;
                //chart.options.plotOptions.pie.dataLabels.enabled = true;
            }
            else if (checkBoxId == "AllowSelection") {
                allowPointSelect = true;
                // chart.options.plotOptions.pie.allowPointSelect = true;
            }
            else if (checkBoxId == "options3d") {
                options3d = true;
                //chart.options.chart.options3d.enabled = true;
            }
        }
        else {
            if (checkBoxId == "Legend") {
                legend = false;
                // chart.options.plotOptions.pie.showInLegend = false;
            }
            else if (checkBoxId == "DataLabel") {
                dataLabels = false;
                // chart.options.plotOptions.pie.dataLabels.enabled = false;
            }
            else if (checkBoxId == "AllowSelection") {
                allowPointSelect = false;
                //chart.options.plotOptions.pie.allowPointSelect = false;
            }
            else if (checkBoxId == "options3d") {
                options3d = false;
                // chart.options.chart.options3d.enabled = false;
            }
        }
    });
    if (globalReportGraphType == "Pie") {
        graphReport.ReportViewInPieChart(globalReportGraphData, globalTotalRecordCount, globalGraphTitle, legend, options3d, allowPointSelect, dataLabels);
    }
    else if (globalReportGraphType == "3DBar") {
        graphReport.ReportViewIn3DBarChart(globalReportGraphData, globalTotalRecordCount, globalGraphTitle, legend, options3d, allowPointSelect, dataLabels);
    }


});

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

$(document).on("change", "#xAxisFields,#yAxisFields", function () {
    var reportNo = getParameterByName('reportno');
    var chartType = $("#charttype").val();
    graphReport.GraphReport(parseInt(reportNo), chartType);
    $("#ReportGraph").slideToggle("slow");
    return false;
});

$(document).on('change', '#R0', function () {
    chart.options.chart.options3d.alpha = this.value;
    chart.redraw(false);
});

$(document).on('change', '#R1', function () {
    chart.options.chart.options3d.beta = this.value;
    chart.redraw(false);
});

$(document).ready(function () {
    // Activate the sliders
    $('#R0').on('change', function () {
        chart.options.chart.options3d.alpha = this.value;
        // showValues();
        chart.redraw(false);
    });
    $('#R1').on('change', function () {
        chart.options.chart.options3d.beta = this.value;
        //showValues();
        chart.redraw(false);
    });
    localStorage.setItem('showGraphReportflag', true);
    localStorage.setItem('showLegend', false);
    localStorage.setItem('showDataLabel', false);
    localStorage.setItem('showAllowSelection', false);
    localStorage.setItem('showoptions3d', false);
});


