﻿var isReport = false;

var DynamicGrid = {
    SetSortOrder: function () {
        DynamicGrid.getActionLink();
        $("#grid .grid-header th a").each(function () {
            var ahref = $(this).attr('href');
            if (ahref.indexOf('_swhg') >= 0) {
                var cutStr = ahref.substr(ahref.indexOf('_swhg'), ahref.length);
                ahref = ahref.replace(cutStr.split('&')[0], '')
                ahref = ahref.replace('_&', '');
                $(this).attr('href', ahref);
                $(this).attr('data-swhglnk', false);
            }
        });
        WebgridPagerHelper.UpdateHandler();
        $("#btnClearSearch").unbind("click");
        $("#btnClearSearch").click(function () {
            $(this).closest("form").find("input[type=text]").val('');
            $(this).closest("form").find("select").val('');
            //PRFT Custom Code: Start
            var isCustomerCheckbox = $(this).closest("form").find("#isCustomer");
            var isSuperUserCheckbox = $(this).closest("form").find("#isSuperUser");
            var isERPOrderFailedCheckbox = $(this).closest("form").find("#isERPOrderFailed");

            if (isCustomerCheckbox != undefined && isCustomerCheckbox != null) {
                isCustomerCheckbox.checked = false;
                isCustomerCheckbox.val('false');
            }

            if (isSuperUserCheckbox != undefined && isSuperUserCheckbox != null) {
                isSuperUserCheckbox.checked = false;
                isSuperUserCheckbox.val('false');
            }

            if (isERPOrderFailedCheckbox != undefined && isERPOrderFailedCheckbox != null) {
                isERPOrderFailedCheckbox.checked = false;
                isERPOrderFailedCheckbox.val('false');
            }
            //PRFT Custom Code: End

            $(this).closest("form").submit();
        });

        var bootstrapjs = document.createElement('script');
        bootstrapjs.setAttribute('src', window.location.protocol + "//" + window.location.host + "/Content/bootstrap-3.3.1/js/tooltip.min.js");
        document.body.appendChild(bootstrapjs);

        if ($('.datepicker').length) {
            var s = document.createElement('script');
            s.setAttribute('src', window.location.protocol + "//" + window.location.host + "/Content/bootstrap-3.3.1/js/datepicker.js");
            document.body.appendChild(s);
        }
        DynamicGrid.CreateNestedGridNode();
        CommonHelper.BlockHtmlTagForTextBox();
        if (!navigator.userAgent.match(/Trident\/7\./)) {
            $('.table-responsive').addClass('scroll-default');
        }
        var showGraph = localStorage.getItem('showGraphReportflag');
        if (showGraph === "true" && isReport) {
            $("#reportGraphCheckBox").click();
        }
        var showLegend = localStorage.getItem('showLegend');
        if (showLegend === "true" && isReport) {
            $("#Legend").click();
        }
        var showDataLabel = localStorage.getItem('showDataLabel');
        if (showDataLabel === "true" && isReport) {
            $("#DataLabel").click();
        }
        var showAllowSelection = localStorage.getItem('showAllowSelection');
        if (showAllowSelection === "true" && isReport) {
            $("#AllowSelection").click();
        }
        var showoptions3d = localStorage.getItem('showoptions3d');
        if (showoptions3d === "true" && isReport) {
            $("#options3d").click();
        }
    },

    getActionLink: function () {
        var index = 0;
        $("#grid th").each(function () {
            var hdrText = $.trim($(this).text());
            hdrText = hdrText.replace(/\s/g, "");
            if (hdrText === "Checkbox") {
                DynamicGrid.setCheckboxHeader(this);
                DynamicGrid.checkAllChange();
            }
            else if (hdrText.toLocaleLowerCase() === "select") {
                DynamicGrid.rowCheckChange();
            }
            index++;
        });
    },
    setCheckboxHeader: function (header) {
        $(header).closest("th").html("<input type='Checkbox' name='check-all' class='header-check-all' id='check-all'/><span class='lbl padding-8'></span>");
        DynamicGrid.rowCheckChange();
    },
    checkAllChange: function () {
        $("#check-all").unbind("change");
        $("#check-all").change(function () {
            var index = $(this).closest('th').index();
            if (this.checked) {
                $("#grid tr").find('td:eq(' + index + ') input[type=checkbox]:enabled').prop('checked', true);
            }
            else {
                $("#grid tr").find('td:eq(' + index + ') input[type=checkbox]').prop('checked', false);
            }
            DynamicGrid.CheckUncheckAllSelectedCheckboxItems(this.checked);
        });
    },

    rowCheckChange: function () {
        $(".grid-row-checkbox").unbind("change");
        $(".grid-row-checkbox").change(function () {
            if (this.checked) {
                var checkBoxCount = $(".grid-row-checkbox").length;
                var checkBoxCheckedCount = $(".grid-row-checkbox:checked").length;

                if (checkBoxCount === checkBoxCheckedCount)
                    $("#check-all").prop('checked', true);
                else
                    $("#check-all").prop('checked', false);
            }
            else {
                $("#check-all").prop('checked', false);
            }
            DynamicGrid.SaveSelectedCheckboxItems(this.checked, $(this).attr('id'));
        });
    },

    SaveSelectedCheckboxItems: function (isSelected, selectedValue) {
        var selectedIds = new Array();

        if (localStorage.getItem("selectedchkboxItems") != "") {
            selectedIds = JSON.parse(localStorage.getItem("selectedchkboxItems"));
        }
        if (isSelected) {
            selectedIds.push(selectedValue);
        }
        else {
            selectedIds.pop(selectedValue);
        }

        DynamicGrid.SetDistinctItemsInArray(selectedIds);
    },

    CheckUncheckAllSelectedCheckboxItems: function (isSelected) {
        var selectedIds = new Array();

        if (localStorage.getItem("selectedchkboxItems") != "") {
            selectedIds = JSON.parse(localStorage.getItem("selectedchkboxItems"));
        }

        $(".grid-row-checkbox").each(function () {
            if (isSelected) {
                selectedIds.push($(this).attr('id'));
            }
            else {
                selectedIds.pop($(this).attr('id'));
            }
        });

        DynamicGrid.SetDistinctItemsInArray(selectedIds);
    },

    SetDistinctItemsInArray: function (arrayObj) {
        var selectedIds = [];
        arrayObj.forEach(function (value) {
            if (selectedIds.indexOf(value) == -1) {
                selectedIds.push(value);
            }
        });

        if (selectedIds.length > 0) {
            localStorage.setItem("selectedchkboxItems", JSON.stringify(selectedIds));
        }
    },

    selectedRow: function (fun_success) {
        var ids = new Array();
        $(".grid-row-checkbox:checked").each(function () {
            ids.push({
                values: $.trim($(this).attr('id').split('_')[1])
            });
        });
        fun_success(ids);
    },

    setEnabledImage: function (index) {
        $("#grid tr").find("td:eq(" + index + ")").each(function () {
            var orgText = $(this).text();
            orgText = $.trim(orgText)
            $(this).text('');

            if (orgText === "True") {
                $(this).html("<i class='glyphicon glyphicon-ok'></i>");
            }
            else {
                $(this).html("<i class='glyphicon glyphicon-remove'></i>");
            }
        });
    },

    setDeleteConfirm: function (index) {
        $("#grid tr").find("td:eq(" + index + ")").each(function () {

            var orgText = $(this).text();
            orgText = $.trim(orgText)
            $(this).text('');

            if (orgText.indexOf("isConfirm") >= 0) {
                if ((orgText.split('$')[1]).split('=')[1] === "true") {
                    $(this).html("<a class='z-" + orgText.split('$')[0].toLowerCase() + " actiov-icon' href='#' title='" + orgText.split('$')[0] + "' onclick=CommonHelper.BindDeleteConfirmDialog('Confirm&nbspDelete?','Are&nbspyou&nbspsure,&nbspyou&nbspwant&nbspto&nbspdelete&nbspthis&nbsprecord?','" + orgText.split('$')[2] + "') ></a>");
                }
                else {
                    $(this).html("<a class='z-" + orgText.split('$')[0].toLowerCase() + " actiov-icon' title='" + orgText.split('$')[0] + "' href='" + orgText.split('$')[2] + "'></a>");
                }
            }
            else {
                $(this).html("<a class='z-" + orgText.split('$')[0].toLowerCase() + " actiov-icon' href='#' title='" + orgText.split('$')[0] + "' onclick=CommonHelper.BindDeleteConfirmDialog('Confirm&nbspDelete?','Are&nbspyou&nbspsure,&nbspyou&nbspwant&nbspto&nbspdelete&nbspthis&nbsprecord?','" + orgText.split('$')[1] + "') ></a>");
            }
        });
    },

    selectedRowByIndex: function (index, fun_success) {
        var ids = new Array();
        $("#grid tbody tr").find("td:eq(" + index + ") input[type=checkbox]:checked").each(function () {
            ids.push({
                values: $.trim($(this).attr('id').split('_')[1])
            });
        });
        fun_success(ids);
    },


    loadOperatorList: function (control) {

        $.ajax({
            url: "/AdvanceSearch/GetOperatorList",
            contentType: 'application/html; charset=utf-8',
            data: {
                id: $(control).val()
            },
            type: 'GET',
            success: function (data) {
                $(control).parent().parent().next().find('select').html(data.OperatorString);
                if ($(control).parent().parent().next().find('select').attr("data-value") != "0") {
                    if (DynamicGrid.IsDataPresentInList(control, $(control).parent().parent().next().find('select').attr("data-value"))) {
                        $(control).parent().parent().next().find('select').val($(control).parent().parent().next().find('select').attr("data-value"));
                    }
                    else {
                        $(control).parent().parent().next().find('select option').eq(0).prop('selected', true);
                    }
                }
                else {
                    $(control).parent().parent().next().find('select option').eq(0).prop('selected', true);
                }
            },
            error: function () {
            }
        });
    },

    clearAdvanceSearch: function () {
        App.Api.clearAdvanceSearch(function (responce) {
            $("#advance-search-containt").html(responce);
        });
    },

    clearAdvanceSearchFranchise: function () {
        App.Api.clearAdvanceSearchFranchise(function (responce) {
            $("#advance-search-containt").html(responce);
        });
    },

    bindAdvanceSearchEvents: function () {
        $("#advance-search .fields-name").attr("disabled", true);
        $("#advance-search .fields-name:eq(0)").attr("disabled", false);
        $("#advance-search .fields-name").unbind("change");
        $("#advance-search .fields-name").change(function () {
            DynamicGrid.loadOperatorList(this);
            $(this).parent().parent().parent().find('input').val("");
            if ($(this).val() != "" && $(this).parent().parent().parent().prev().find('select:eq(2)').length > 0) {
                $(this).parent().parent().parent().next().find('select:eq(0)').attr("disabled", false);
                $(this).parent().parent().parent().prev().find('select:eq(2)').val("AND");
            }
            else if ($(this).val() != "") {
                $(this).parent().parent().parent().next().find('select:eq(0)').attr("disabled", false);
            }

        });
    },

    clickonGridClear: function (event, control) {
        event.preventDefault();
        var domain = $(control).attr('href');
        var location = window.location.href;
        location = location.replace((domain.split('?')[1]).split('=')[1], "");

        if (location.indexOf("FranchiseAdmin") >= 0 || location.indexOf("MallAdmin") >= 0) {
            window.location.href = (domain.split('?')[0]) + "?returnurl=../../" + location;
        }
        else {
            window.location.href = (domain.split('?')[0]) + "?returnurl=../" + location;
        }


    },
    DataValidattion: function (e, control) {
        var datatype = $(control).parent().parent().prev().find('select option:selected').attr('data-datype');
        switch (datatype) {
            case "Int32":
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                    // Allow: Ctrl+A
                    (e.keyCode == 65 && e.ctrlKey === true) ||
                    // Allow: home, end, left, right, down, up
                        (e.keyCode >= 35 && e.keyCode <= 40)) {
                    // let it happen, don't do anything
                    return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
                break;
            case "String":
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                    // Allow: Ctrl+A
                    (e.keyCode == 65 && e.ctrlKey === true) ||
                    // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                    // let it happen, don't do anything
                    return;
                }

                var str = String.fromCharCode(e.keyCode)

                if (!/^[a-zA-Z0-9\s]+$/.test(str)) {
                    e.preventDefault();
                }
                else {
                    return;
                }
                break;

        }
    },

    DataValidattionOnFilters: function (e, control) {
        var datatype = $(control).attr('data-datype');
        switch (datatype) {
            case "Int32":
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                    // Allow: Ctrl+A
                    (e.keyCode == 65 && e.ctrlKey === true) ||
                    // Allow: home, end, left, right, down, up
                        (e.keyCode >= 35 && e.keyCode <= 40)) {
                    // let it happen, don't do anything
                    return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
                break;
            case "Decimal":
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                    // Allow: Ctrl+A
                    (e.keyCode == 65 && e.ctrlKey === true) ||
                    // Allow: home, end, left, right, down, up
                        (e.keyCode >= 35 && e.keyCode <= 40)) {
                    // let it happen, don't do anything
                    return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
                break;
        }
    },

    CreateNestedGridNode: function () {
        if ($("#subT").length > 0) {
            var size = $("#grid > thead > tr >th").size(); // get total column
            $("#grid > thead > tr >th").last().remove(); // remove last column
            $("#grid > thead > tr").prepend("<th style='padding:0 10px;'></th>"); // add one column at first for collapsible column
            $("#grid > tbody > tr").each(function (i, el) {
                $(this).prepend(
                        $("<td></td>")
                        .addClass("expand-grid")
                        .addClass("hoverEff")
                        .attr('title', "click for show/hide")
                    );

                //Now get sub table from last column and add this to the next new added row
                var table = $("table", this).parent().html();
                //add new row with this subtable
                $(this).after("<tr><td style='padding-left:20px;' colspan='" + (size) + "'>" + table + "</td></tr>");
                $("table", this).parent().remove();
                // ADD CLICK EVENT FOR MAKE COLLAPSIBLE
                $(".hoverEff", this).on("click", function () {
                    if ($(this).hasClass("collapse-grid")) {
                        var id = $(this).parent().closest("tr").next().find('table:eq(0) tbody tr:eq(0) td').find("#recored-id").val();
                        var type = $(this).parent().closest("tr").next().find('table:eq(0) tbody tr:eq(0) td').find("#type-name").val();
                        var method = $(this).parent().closest("tr").next().find('table:eq(0) tbody tr:eq(0) td').find("#method-name").val();
                        var control = this;
                        DynamicGrid.GetSubGrid(id, type, method, function (responce) {

                            $(control).parent().closest("tr").next().find('table:eq(0) thead').remove();
                            $(control).parent().closest("tr").next().find('table:eq(0) tbody tr:eq(0)').css("display", "none");
                            if ($(control).parent().closest("tr").next().find('table:eq(0) tbody tr').length == 1) {
                                $(control).parent().closest("tr").next().find('table:eq(0) tbody').append('<tr><td></td></tr>')
                            }

                            $(control).parent().closest("tr").next().find('table:eq(0) tbody tr:eq(1) td').html(responce);

                            if ($("#report-title").text() === "Order Pick List") {
                                $("#subT table tbody tr").find('td:eq(3)').each(function () {
                                    var txt = $(this).text(); $(this).html(txt);
                                })
                            }

                            $(control).parent().closest("tr").next().find('table:eq(0) tbody tr:eq(1) td').find('th').each(function () {
                                var header = $(this).text();
                                $(this).text(header.replace('_', ' '));
                            });

                            $(control).parent().closest("tr").next().slideToggle(100);
                            $(control).toggleClass("expand-grid collapse-grid");

                        });
                    }
                    else {
                        $(this).parent().closest("tr").next().slideToggle(100);
                        $(this).toggleClass("expand-grid collapse-grid");
                    }
                });

            });

            //by default make all subgrid in collapse mode
            $("#grid > tbody > tr td.expand-grid").each(function (i, el) {
                $(this).toggleClass("collapse-grid expand-grid");
                $(this).parent().closest("tr").next().slideToggle(100);
            });
        }
    },
    GetSubGrid: function (id, type, method, callback_fun) {
        App.Api.GetSubGridPartial(id, type, method, function (responce) {
            callback_fun(responce);
        });
    },
    GetSelectedCheckBoxValue: function () {
        if (localStorage.getItem("selectedchkboxItems") != undefined && localStorage.getItem("selectedchkboxItems") != "") {
            selectedIds = JSON.parse(localStorage.getItem("selectedchkboxItems"));
            for (var item in selectedIds) {
                selectedIds[item] = selectedIds[item].replace("rowcheck_", "");
            }
            return selectedIds;
        }
        else {
            return "";
        }
    },
    ShowHideGrid: function () {
        $("#grid-list-content").animate({
            opacity: 'toggle'
        }, 'slow');
        var text = $('#hide-grid-link').text();
        $('#hide-grid-link').text(
            text == "Hide Grid" ? "Show Grid" : "Hide Grid");
    },

    IsDataPresentInList: function (control, value) {
        $(control).parent().parent().next().find('select option').each(function () {
            if (this.value == value) {
                return false;
            }
        });
    }
}

$(document).on("keydown", "#filter-componant-control-content input[type=text]", function (e) {
    DynamicGrid.DataValidattionOnFilters(e, this)
});

$(document).ready(function () {
    $("#btnClearSearch").unbind("click");
    $("#btnClearSearch").click(function () {
        $(this).closest("form").find("input[type=text]").val('');
        $(this).closest("form").find("select").val('');
        //PRFT Custom Code: Start
        var isCustomerCheckbox = $(this).closest("form").find("#isCustomer");
         var isSuperUserCheckbox = $(this).closest("form").find("#isSuperUser");
        var isERPOrderFailedCheckbox = $(this).closest("form").find("#isERPOrderFailed");

        if (isCustomerCheckbox != undefined && isCustomerCheckbox != null) {
            isCustomerCheckbox.checked = false;
            isCustomerCheckbox.val('false');
        }

        if (isSuperUserCheckbox != undefined && isSuperUserCheckbox != null) {
            isSuperUserCheckbox.checked = false;
            isSuperUserCheckbox.val('false');
        }

        if (isERPOrderFailedCheckbox != undefined && isERPOrderFailedCheckbox != null) {
            isERPOrderFailedCheckbox.checked = false;
            isERPOrderFailedCheckbox.val('false');
        }
        //PRFT Custom Code: End
        $(this).closest("form").submit();
    });

    DynamicGrid.getActionLink();
    localStorage.setItem("selectedchkboxItems", "");
});