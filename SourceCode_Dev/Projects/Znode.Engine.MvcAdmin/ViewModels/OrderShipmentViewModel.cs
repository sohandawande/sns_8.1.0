﻿using Resources;
using Znode.Libraries.Helpers;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class OrderShipmentViewModel : BaseViewModel
    {
        public int OrderShipmentId { get; set; }
        public string ShipName { get; set; }
        public int? ShippingOptionId { get; set; }
        public string ShipToCity { get; set; }
        public string ShipToCompanyName { get; set; }
        public string ShipToCountryCode { get; set; }
        public string ShipToEmail { get; set; }
        public string ShipToFirstName { get; set; }
        public string ShipToLastName { get; set; }
        public string ShipToPhoneNumber { get; set; }
        public string ShipToPostalCode { get; set; }
        public string ShipToStateCode { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelShippingAddress)]
        public string ShipToStreetAddress1 { get; set; }
        public string ShipToStreetAddress2 { get; set; }

        public int AddressId { get; set; }
        public int Quantity { get; set; }
        public decimal? TaxCost { get; set; }
        public decimal? ShippingCost { get; set; }
        public string ShippingName { get; set; }
    }
}