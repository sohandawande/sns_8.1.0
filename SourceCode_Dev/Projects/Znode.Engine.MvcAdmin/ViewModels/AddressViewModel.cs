﻿
using Resources;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Znode.Engine.Api.Models;
using Znode.Libraries.Helpers;
namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model for Address
    /// </summary>
    public class AddressViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for AddressViewModel
        /// </summary>
        public AddressViewModel()
        {
            CountryList = new List<SelectListItem>();
            Countries = new Collection<CountryModel>();
        }

        [LocalizedDisplayName(Name = RZnodeResources.LabelFirstName)]
        //[Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "ErrorRequired")]
        public string FirstName { get; set; }
        public string MiddleName { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelLastName)]
        //[Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "ErrorRequired")]
        public string LastName { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelCompanyName)]
        public string CompanyName { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelStreet1)]
        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "ErrorRequired")]
        public string StreetAddress1 { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelStreet2)]
        public string StreetAddress2 { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "ErrorRequired")]
        public string City { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelPostalCode)]
        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "ErrorRequired")]
        public string PostalCode { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.PlaceHolderPhoneNo)]
        //[Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "ErrorRequired")]
        public string PhoneNumber { get; set; }
        public string Email { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelAddressName)]
        //[Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "ErrorRequired")]
        public string Name { get; set; }

        [DisplayName("State/Province/Region")]
        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "ErrorRequired")]
        public string StateCode { get; set; }

        public string CountryCode { get; set; }

        public int AccountId { get; set; }
        public int AddressId { get; set; }

        public bool IsDefaultBilling { get; set; }
        public bool IsDefaultShipping { get; set; }
        public bool UseSameAsBillingAddress { get; set; }
        public bool UseSameAsShippingAddress { get; set; }

        public string FullAddress { get; set; }

        public List<SelectListItem> CountryList { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelCountryDropdown)]
        public Collection<CountryModel> Countries { get; set; }
    }
}