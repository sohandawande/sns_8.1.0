﻿using Resources;
using System.ComponentModel.DataAnnotations;
using Znode.Libraries.Helpers;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class ShippingBillingAddressViewModel : BaseViewModel
    {
        public AddressViewModel ShippingAddressModel { get; set; }
        public AddressViewModel BillingAddressModel { get; set; }

        //[Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "ErrorRequired")]
        [LocalizedDisplayName(Name = RZnodeResources.LabelEmailAddress)]
        public string EmailAddress { get; set; }

        public int ShippingAddressId { get; set; }
        public int BillingAddressId { get; set; }
        public int? PortalId { get; set; }
        public int AccountId { get; set; }

        public bool UseSameAsShippingAddress { get; set; }

        public ShippingBillingAddressViewModel()
        {
            ShippingAddressModel = new AddressViewModel();
            BillingAddressModel = new AddressViewModel();
        }
    }
}