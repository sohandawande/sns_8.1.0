﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.MvcAdmin.Models;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class ProductTypeListViewModel : BaseViewModel
    {
        public ProductTypeListViewModel()
        {
            ProductTypes = new List<ProductTypeViewModel>();
        }

        public string SearchProductType { get; set; }
        public string SearchDescription { get; set; }

        public List<ProductTypeViewModel> ProductTypes { get; set; }

        public FilterColumnListModel FilterColumn { get; set; }
    }
}