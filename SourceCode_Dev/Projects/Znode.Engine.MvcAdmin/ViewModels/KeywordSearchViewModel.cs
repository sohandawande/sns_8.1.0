﻿using System.Collections.ObjectModel;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class KeywordSearchViewModel : BaseViewModel
    {
        // Response
        public Collection<SearchCategoryViewModel> Categories { get; set; }
        public Collection<FacetViewModel> Facets { get; set; }
        public Collection<ProductViewModel> Products { get; set; }
        public Collection<FacetViewModel> SelectedFacets { get; set; }

        public int OldPageNumber { get; set; }
        public int TotalPages { get; set; }
        public int TotalProducts { get; set; }
        public string NextPageUrl { get; set; }
        public string PreviousPageurl { get; set; }
        public string Category { get; set; }
        public string SearchTerm { get; set; }
        public string SeoDescription { get; set; }
        public string SeoKeywords { get; set; }
        public string SeoTitle { get; set; }
        //Znode version 7.2.2 To get Category Banner
        public string CategoryBanner { get; set; }

        public KeywordSearchViewModel()
        {
            Facets = new Collection<FacetViewModel>();
            Products = new Collection<ProductViewModel>();
            SelectedFacets = new Collection<FacetViewModel>();
        }
    }
}