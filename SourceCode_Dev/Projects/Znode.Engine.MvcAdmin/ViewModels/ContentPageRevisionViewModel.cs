﻿using System;
namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View model for content page revision
    /// </summary>
    public class ContentPageRevisionViewModel : BaseViewModel
    {
         #region Constructor
        /// <summary>
        /// Constructor for Manage Content Page 
        /// </summary>
        public ContentPageRevisionViewModel()
        {

        }
        #endregion

        #region Public Properties
        public string Description { get; set; }
        public string UpdatedUser { get; set; }
        public string Html { get; set; }
        public string OldHtml { get; set; }
        public int ContentPageId { get; set; }
        public string ContentPageName { get; set; }
        public int RevisionId { get; set; }
        public string PageName { get; set; }
        public string UpdateDate { get; set; }
        #endregion
    }
}