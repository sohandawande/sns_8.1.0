﻿using Resources;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Znode.Libraries.Helpers;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class CategoryProfileViewModel : BaseViewModel
    {
        public int CategoryProfileID { get; set; }
        public int CategoryID { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelProfile)]
        public int ProfileID { get; set; }
        public string ProfileName { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelEffectiveDate)]
        public string EffectiveDate { get; set; }
        public List<SelectListItem> CategoryProfile { get; set; }
        public int CategoryNodeId { get; set; }
    }
}