﻿using Resources;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Models;
using Znode.Libraries.Helpers;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model for Add Product Add-On
    /// </summary>
    public class AddOnViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for AddonViewModel
        /// </summary>

        public AddOnViewModel()
        {
            ReportModel = new ReportModel();
        }
        public IEnumerable<AddOnValueViewModel> AddOnValues { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "rfvAddOnName")]
        public string Name { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelProductAlternateImageTitle)]
        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "rfvAddOnTitle")]
        public string Title { get; set; }

        [UIHint("RichTextBox")]
        [AllowHtml]
        [LocalizedDisplayName(Name = RZnodeResources.LabelEnterADescription)]
        public string Description { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelInStockMessages)]
        public string InStockMessage { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelOutofStockMessages)]
        public string OutOfStockMessage { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelBackOrderMessages)]
        public string BackOrderMessage { get; set; }

        [MaxLength(50)]
        [LocalizedDisplayName(Name = RZnodeResources.LabelDisplayType)]
        public string DisplayType { get; set; }
        public string PromptMessage { get; set; }
        public string SKU { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelDisplayOrder)]
        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "rfvDisplayOrder")]
        [Range(1, 999999999, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "ValidNumber")]
        [RegularExpression("^(1|[1-9][0-9]*)$", ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "InvalidDisplayOrder")]
        public int? DisplayOrder { get; set; }
        public int AddOnId { get; set; }
        public InventoryOptionsState? OutofStockOptions { get; set; }
        public int AddOnValueId { get; set; }
        public int ProductAddOnId { get; set; }
        public int ProductId { get; set; }
        public int? PortalId { get; set; }

        public bool AllowBackOrder { get; set; }
        public bool TrackInventory { get; set; }
        public bool InventoryEnabled { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelOptional)]
        public bool IsOptional { get; set; }
        public string SkuOrProductNum { get; set; }
        public ReportModel ReportModel { get; set; }
        public int[] SelectedAddOnValue { get; set; }
        public bool OptionalIndicator { get; set; }
        public string AddOnValueName { get; set; }
        public int? AccountId { get; set; }
        public string Locale { get; set; }
        public int LocaleId { get; set; }
        public bool IsOutOfStock { get; set; }
    }
}