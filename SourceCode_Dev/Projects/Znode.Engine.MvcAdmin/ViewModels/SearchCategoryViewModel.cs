﻿using System.Collections.ObjectModel;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class SearchCategoryViewModel : BaseViewModel
    {
        public int Count { get; set; }
        public Collection<SearchCategoryViewModel> Hierarchy { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public string Categoryurl { get; set; }
    }
}