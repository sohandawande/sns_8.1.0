﻿using System.Collections.Generic;
using Znode.Engine.MvcAdmin.Models;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class CustomerBasedPricingListViewModel : BaseViewModel
    {
        public CustomerBasedPricingListViewModel()
        {
            CustomerBasedPricing = new List<CustomerBasedPricingViewModel>();
            GridModel = new ReportModel();
        }
        public List<CustomerBasedPricingViewModel> CustomerBasedPricing { get; set; }
        public FilterColumnListModel FilterColumn { get; set; }
        public ReportModel GridModel { get; set; }
        public int ProductId { get; set; }
        public int? ExternalAccountNo { get; set; }
        public int AccountId { get; set; }
    }
}