﻿using System.Collections.Generic;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class ThemeListViewModel : BaseViewModel
    {
        public ThemeListViewModel()
        {
            ThemeList = new List<ThemeViewModel>();
        }

        public List<ThemeViewModel> ThemeList { get; set; }
    }
}