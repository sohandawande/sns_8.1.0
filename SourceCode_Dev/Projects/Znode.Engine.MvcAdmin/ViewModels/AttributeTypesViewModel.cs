﻿using Resources;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using Znode.Engine.MvcAdmin.Models;
using Znode.Libraries.Helpers;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model for Add Attribute Type
    /// </summary>
    public class AttributeTypesViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for AttributeTypeViewMode
        /// </summary>
        public AttributeTypesViewModel()
        {
            GetAttributesList = new List<SelectListItem>();
            ProductAttributes = new Collection<AttributesViewModel>();
            ReportModel = new ReportModel();
        }
        [LocalizedDisplayName(Name = RZnodeResources.LabelAttributeTypesName)]
        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "ErrorRequired")]
        public string Name { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelDisplayOrder)]
        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "ErrorRequired")]
        [Range(1, 999999999, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "ValidNumber")]
        [RegularExpression("^(1|[1-9][0-9]*)$", ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "InvalidDisplayOrder")]
        public int DisplayOrder { get; set; }

        public string Description { get; set; }

        public int AttributeTypeId { get; set; }
        public int LocaleId { get; set; }
        public int ProductTypeId { get; set; }

        public List<SelectListItem> GetAttributesList { get; set; }

        public Collection<AttributesViewModel> ProductAttributes { get; set; }
        public bool ToBeFiltered { get; set; }
        public bool IsSelectedOption { get; set; }

        public ReportModel ReportModel { get; set; }

        public Collection<AttributeSelectListItem> DropdownAttributes
        {
            get
            {
                if (ProductAttributes.Any())
                {
                    return new Collection<AttributeSelectListItem>(ProductAttributes.Select(
                        x =>
                        new AttributeSelectListItem()
                        {
                            Text = x.Name,
                            Value = x.AttributeId.ToString(),
                            Highlighted = x.Available,
                            Selected = x.Selected,
                        }).ToList());
                }

                return new Collection<AttributeSelectListItem>();
            }
        }
    }
}