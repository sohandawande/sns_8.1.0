﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class LuceneIndexViewModel:BaseViewModel
    {
        public long LuceneIndexMonitorId { get; set; }
        public int SourceId { get; set; }
        public string SourceType { get; set; }
        public string SourceTransationType { get; set; }
        public string TransationDateTime { get; set; }
        public bool IsDuplicate { get; set; }
        public string AffectedType { get; set; }
        public string IndexerStatusChangedBy { get; set; }
    }
}