﻿
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Znode.Libraries.Helpers.Attributes;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model for Add Pricing Tier for Product
    /// </summary>
    public class ProductTierPricingViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for ProductTierPricingViewModel
        /// </summary>
        public ProductTierPricingViewModel()
        {

        }

        public int ProductTierId { get; set; }
        public int ProductId { get; set; }

        [Required]
        [Display(Name="Tier Start")]
        [Range(1, int.MaxValue, ErrorMessage = "Please enter a whole number")]
        public int? TierStart { get; set; }

        [Required]
        [Display(Name="Tier End")]
        [Range(1, int.MaxValue, ErrorMessage = "Please enter a whole number")]
        [GenericCompare(CompareToPropertyName = "TierStart", OperatorName = GenericCompareOperator.GreaterThanOrEqual, ErrorMessage = "Tier end should be greater than tier start.")]
        public int? TierEnd { get; set; }

        public int? ProfileId { get; set; }

        [Required(ErrorMessage="Enter a discounted price for this tier.")]
        [RegularExpression("^\\d{0,6}(\\.\\d{0,2})?$", ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "ErrorPriceNumberRange")]
        [Range(0, 999999.99, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "ErrorPriceNumberRange")]
        public decimal? Price { get; set; }

        public string PriceDisplay { get; set; }

        public string ProfileName { get; set; }

        public List<SelectListItem> Profiles { get; set; }

        public bool IsEditMode { get; set; }
        public string ProductName { get; set; }
        public string UserType { get; set; }
    }
}