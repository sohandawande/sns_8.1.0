﻿using System.Collections.Generic;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class RoleMenuListViewModel : BaseViewModel
    {
        public List<RoleMenuViewModel> RoleMenuList { get; set; }
        /// <summary>
        /// Constructor for RoleMenuListViewModel
        /// </summary>
        public RoleMenuListViewModel()
        {
            RoleMenuList = new List<RoleMenuViewModel>();
        }
    }
}