﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using Resources;
using Znode.Libraries.Helpers;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// ViewModel for create and edit Product Category
    /// </summary>
    public class ProductCategoryViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for ProductCategoryViewModel
        /// </summary>
        public ProductCategoryViewModel()
        {

        }
        public int CategoryId { get; set; }
        public int ProductCategoryId { get; set; }
        public int ProductId { get; set; }

        public int? CssId { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.DisplayOrderText)]        
        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "RequiredDisplayOrder")]
        [Range(1, 999999999, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "InvalidDisplayOrder")]
        [RegularExpression("^(1|[1-9][0-9]*)$", ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "InvalidDisplayOrder")]
        public int? DisplayOrder { get; set; }
        public int? MasterPageId { get; set; }
        public int? ThemeId { get; set; }

        public bool IsActive { get; set; }

        public DateTime? BeginDate { get; set; }
        public DateTime? EndDate { get; set; }

        public List<SelectListItem> ThemeList { get; set; }
        public List<SelectListItem> CssList { get; set; }
        public List<SelectListItem> MasterPageList { get; set; }

        public CategoryViewModel Category { get; set; }
        public ProductViewModel Product { get; set; }     
    }
}