﻿using System.Collections.ObjectModel;
namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class WishListViewModel : BaseViewModel
    {
        public Collection<WishListItemViewModel> Items { get; set; }
    }
}