﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class BundleDisplayViewModel : BaseViewModel
    {
        public string Name { get; set; }
        public string ImageLargePath { get; set; }
        public string ImageMediumPath { get; set; }
        public string ImageSmallPath { get; set; }
        public string ImageAltTag { get; set; }
        public string ImageFile { get; set; }
    }
}