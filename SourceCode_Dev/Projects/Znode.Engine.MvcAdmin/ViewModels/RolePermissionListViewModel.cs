﻿using System.Collections.Generic;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class RolePermissionListViewModel : BaseViewModel
    {
        public RolePermissionListViewModel()
        {
            UserRolePermissionList = new List<RolePermissionViewModel>();
        }
        public List<RolePermissionViewModel> UserRolePermissionList { get; set; }
    }
}