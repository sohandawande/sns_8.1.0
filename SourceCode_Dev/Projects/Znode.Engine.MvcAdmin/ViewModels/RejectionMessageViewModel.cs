﻿using Resources;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Znode.Libraries.Helpers;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View model for rejection message.
    /// </summary>
    public class RejectionMessageViewModel : BaseViewModel
    {
        #region Constructor
        /// <summary>
        /// Default constructor for rejection message. 
        /// </summary>
        public RejectionMessageViewModel()
        {

        }
        #endregion

        [RegularExpression(@"^[-_0-9a-zA-Z]+$", ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.ValidMessageKey)]
        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.RequiredMessageKey)]
        [LocalizedDisplayName(Name = RZnodeResources.LabelMessageKey)]
        [MaxLength(200)]
        public string MessageKey { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelMessageValue)]
        [MaxLength(800)]
        public string MessageValue { get; set; }
        public string StoreName { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.DisplaySelectStore)]
        public int PortalId { get; set; }
        public int LocaleId { get; set; }
        public int RejectionMessagesId { get; set; }

        public List<PortalViewModel> Portal { get; set; }
    }
}