﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.MvcAdmin.Models;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class LuceneIndexListViewModel : BaseViewModel
    {
        public List<LuceneIndexViewModel> LuceneIndexList { get; set; }
        public int TriggerFlag { get; set; }
        public int ServiceFlag { get; set; }
        public ReportModel ReportModel { get; set; }
        public LuceneIndexListViewModel()
        {
            LuceneIndexList = new List<LuceneIndexViewModel>();
            ReportModel = new ReportModel();
        }
    }
}