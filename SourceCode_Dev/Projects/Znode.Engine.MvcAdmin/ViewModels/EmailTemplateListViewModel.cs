﻿using System.Collections.Generic;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class EmailTemplateListViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for Email template list
        /// </summary>
        public EmailTemplateListViewModel()
        {
            EmailTemplateList = new List<EmailTemplateViewModel>();
        }

        public List<EmailTemplateViewModel> EmailTemplateList { get; set; }
    }
}