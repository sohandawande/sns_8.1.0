﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.Api.Models;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class CartItemViewModel : BaseViewModel
    {
        public string BundleItemsIds { get; set; }
        public string Description { get; set; }
        public decimal ExtendedPrice { get; set; }
        public string ExternalId { get; set; }
        public int ProductId { get; set; }
        public int Quantity { get; set; }
        public decimal ShippingCost { get; set; }
        public int ShippingOptionId { get; set; }
        public string Sku { get; set; }
        public int SkuId { get; set; }
        public int[] Packaging { get; set; }
        public int[] AttributeIds { get; set; }
        public string Attributes { get; set; }
        public decimal UnitPrice { get; set; }
        public bool InsufficientQuantity { get; set; }
        public string AddOnValueIds { get; set; }
        public string ProductName { get; set; }
        public int? MaxQuantity { get; set; }
        public ProductViewModel Product { get; set; }
        public string ImagePath { get; set; }
        public string ImageMediumPath { get; set; }
        public Dictionary<string, List<SelectListItem>> dropDownsList { get; set; }

        public List<AddressViewModel> Addresses { get; set; }
        public int SelectedAddressId { get; set; }
        public string SelectedAttributeValues { get; set; }
        public string SelectedAttributeNames { get; set; }
        public int? PortalId { get; set; }
        public string AddOnValuesCustomText { get; set; }
        public Collection<BundleItemModel> BundleItems { get; set; }
        public CartItemViewModel()
        {
            ExternalId = Guid.NewGuid().ToString();
            Addresses = new List<AddressViewModel>();
        }

        #region PRFT Custom  
        public decimal ERPUnitPrice { get; set; }
        public decimal ERPExtendedPrice { get; set; }

        #endregion
    }
}