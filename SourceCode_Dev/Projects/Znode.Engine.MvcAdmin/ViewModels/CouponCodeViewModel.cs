﻿using Resources;
using System.ComponentModel.DataAnnotations;
using Znode.Libraries.Helpers;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class CouponCodeViewModel : BaseViewModel
    {
        [RegularExpression("^[a-zA-Z0-9-_]+$")]
        [LocalizedDisplayName(Name = RZnodeResources.LabelCouponCode)]
        public string CouponCode { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelPromotionMessage)]
        public string PromotionMessage { get; set; }        

        [LocalizedDisplayName(Name = RZnodeResources.LabelAvailableQuantity)]
        public int? CouponQuantityAvailable { get; set; }        
    }
}