﻿using System.Collections.Generic;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class ProductReviewHistoryListViewModel : BaseViewModel
    {
        public ProductReviewHistoryListViewModel()
        {
            ProductReviews = new List<ProductReviewHistoryViewModel>();
        }
        public List<ProductReviewHistoryViewModel> ProductReviews { get; set; }
    }
}