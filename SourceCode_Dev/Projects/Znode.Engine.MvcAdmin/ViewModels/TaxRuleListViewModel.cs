﻿using System.Collections.Generic;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// List View Model for Tax Rule 
    /// </summary>
    public class TaxRuleListViewModel :BaseViewModel
    {
        #region Constructor for TaxRuleListViewModel
        /// <summary>
        /// Constructor for TaxRuleListViewModel
        /// </summary>
        public TaxRuleListViewModel()
        {
            TaxRules = new List<TaxRulesViewModel>();
        } 
        #endregion

        public List<TaxRulesViewModel> TaxRules { get; set; }
    }
}
   