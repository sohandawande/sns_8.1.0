﻿using Resources;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Libraries.Helpers;
using Znode.Libraries.Helpers.Attributes;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class SkuViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for SkuViewModel
        /// </summary>
        public SkuViewModel()
        {

        }

        [Required]
        [LocalizedDisplayName(Name = RZnodeResources.LabelSkuOrPart)]
        public string SKU { get; set; }
        public string SKUPicturePath { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelProductImageALTText)]
        public string ImageAltTag { get; set; }
        public string RecurringBillingPeriod { get; set; }
        public string RecurringBillingFrequency { get; set; }
        public string ProductTypeName { get; set; }
        public string Name { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelSkuPart)]
        public int Id { get; set; }
        public int ProductId { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelSupplier)]
        public int? SupplierId { get; set; }
        public int? RecurringBillingTotalCycles { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelWeightAdditional)]
        [Range(0.0, 9999999.0, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.rnvWeight)]
        public decimal? WeightAdditional { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelRetailPrice)]
        [Range(0, 999999, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "rnvRetailPrice")]
        [RegularExpression("^-?\\d{1,9}(\\.\\d{1,2})?$", ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "revRetailPrice")]
        public decimal? RetailPriceOverride { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelSalePrice)]
        [RegularExpression("^-?\\d{1,9}(\\.\\d{1,2})?$", ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "revSalesPrice")]
        [Range(0, 999999, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "rnvSalesPrice")]
        public decimal? SalePriceOverride { get; set; }

        [RegularExpression("^-?\\d{1,9}(\\.\\d{1,2})?$", ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "revWholesalePrice")]
        [Range(0, 999999, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "rnvWholesalePrice")]
        [LocalizedDisplayName(Name = RZnodeResources.LabelWholesalePrice)]
        public decimal? WholesalePriceOverride { get; set; }
        public decimal? RecurringBillingInitialAmount { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelEnableInventory)]
        public bool IsActive { get; set; }
        public bool IsGiftCard { get; set; }
        public bool ParentChildProduct { get; set; }
        public int AttributeCount { get; set; }

        public int InventoryId { get; set; }
        [Required]
        [LocalizedDisplayName(Name = RZnodeResources.LabelQuantityOnHand)]
        [Range(0, 999999, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "ErrorAddOnNumberRange")]
        public int? QuantityOnHand { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelReOrderLevel)]
        [Range(0, 999999, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "ErrorAddOnNumberRange")]
        public int? ReorderLevel { get; set; }

        public int ProductTypeId { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelProductAttribute)]
        public string ProductAttributes { get; set; }
        public string ErrorMessage { get; set; }
        public string ImageMediumPath { get; set; }

        public List<SelectListItem> SupplierList { get; set; }

        [FileTypeValidation(MvcAdminConstants.ImageFileTypes, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "ImageFileTypeErrorMessage")]
        [FileMaxSizeValidation(MvcAdminConstants.ImageMaxFileSize, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "FileSizeExceededErrorMessage")]
        [UIHint("FileUploader")]
        public HttpPostedFileBase SkuImage { get; set; }

        public SelectImageStatus? SelectedImageStatus { get; set; }
        public bool IsEnableActive { get; set; }
        public bool NoImage { get; set; }
        public string UserType { get; set; }

        //PRFT Custom Code : Start
        public string Custom1 { get; set; }
        //PRFT Custom Code  : End
    }
}