﻿namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model for Shipping Rule Type 
    /// </summary>
    public class ShippingRuleTypeViewModel : BaseViewModel
    {
        #region Constructor
        /// <summary>
        /// Constructor for Shipping Rule Type View Model
        /// </summary> 
        public ShippingRuleTypeViewModel()
        {

        } 
        #endregion
        public string ClassName { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }       
        public int ShippingRuleTypeId { get; set; }   
    }
}