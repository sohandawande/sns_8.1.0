﻿using System.Collections.Generic;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class ReasonForReturnListViewModel :BaseViewModel
    {
        public List<ReasonForReturnViewModel> ReasonsForReturns { get; set; }

        /// <summary>
        /// Constructor for ReasonForReturnListViewModel
        /// </summary>
        public ReasonForReturnListViewModel()
        {
            ReasonsForReturns = new List<ReasonForReturnViewModel>();
        }        
    }
}