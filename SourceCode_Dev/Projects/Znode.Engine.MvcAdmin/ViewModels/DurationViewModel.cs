﻿
namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// Duration View Model
    /// </summary>
    public class DurationViewModel : BaseViewModel
    {
        #region Constructor
        /// <summary>
        /// Constructor for Duration View Model
        /// </summary>
        public DurationViewModel()
        {

        } 
        #endregion

        public int DurationId { get; set; }
        public string DurationName { get; set;}
    }
}