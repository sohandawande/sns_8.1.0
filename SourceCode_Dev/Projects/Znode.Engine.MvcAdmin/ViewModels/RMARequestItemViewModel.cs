﻿
using System.Collections.Generic;
using System.Web.Mvc;
namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class RMARequestItemViewModel : BaseViewModel
    {
        public int? OrderLineItemID { get; set; }
        public int OrderID { get; set; }
        public string ProductNum { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int? MaxQuantity { get; set; }
        public string Price { get; set; }
        public string SKU { get; set; }
        public string DiscountAmount { get; set; }
        public string ShippingCost { get; set; }
        public string PromoDescription { get; set; }
        public string SalesTax { get; set; }
        public int RMAMaxQuantity { get; set; }
        public int RMAQuantity { get; set; }
        public bool? IsReturnable { get; set; }
        public bool? IsReceived { get; set; }
        public int? ReasonForReturnId { get; set; }
        public decimal TaxCost { get; set; }
        public decimal SubTotal { get; set; }
        public decimal Total { get; set; }
        public string TaxCostString { get; set; }
        public string SubTotalString { get; set; }
        public string TotalString { get; set; }
        public string ResonForReturn { get; set; }
        public int RowCount { get; set; }
        public IEnumerable<SelectListItem> RmaQuantitySelectList { get; set; }
        public IEnumerable<SelectListItem> ReasonForReturnList { get; set; }
        public int RMARequestItemID { get; set; }
        public int AccountID { get; set; }
        public int GCExpirationPeriod { get; set; }
        public int? Quantity { get; set; }
        public int GiftCardId { get; set; }
        public decimal? PriceWithoutCurrencySymbol { get; set; }
        public int RMArequestId { get; set; }
        public string TransactionId { get; set; }
        public bool QuantityDropDownEnabled { get; set; }
        public bool IsReturnableCheckboxEnabled { get; set; }
        public decimal TotalPrice { get; set; }
        public string TotalPriceString { get; set; }
    }
}