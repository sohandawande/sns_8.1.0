﻿using System.Collections.Generic;
using System.Web.Mvc;
using Znode.Engine.MvcAdmin.Models;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// List view model for Vendor Account 
    /// </summary>
    public class VendorAccountListViewModel : BaseViewModel
    {
        #region Default constructor
        /// <summary>
        /// constructor Vendor Account
        /// </summary>
        public VendorAccountListViewModel()
        {
            VendorAccount = new List<VendorAccountViewModel>();
            VendorAccountDownloadList = new List<VendorAccountDownloadViewModel>();
            AccountModel = new List<AccountViewModel>();
            GridModel = new ReportModel();
            StoreList = new List<SelectListItem>();
            VendorContactList = new List<VendorContactViewModel>();
            VendorManage = new VendorAccountViewModel();
        } 
        #endregion

        public List<VendorAccountDownloadViewModel> VendorAccountDownloadList { get; set; }
        public List<VendorAccountViewModel> VendorAccount { get; set; }
        public List<VendorContactViewModel> VendorContactList { get; set; }
        public List<AccountViewModel> AccountModel { get; set; }
        public ReportModel GridModel { get; set; }
        public List<SelectListItem> StoreList { get; set; }
        public VendorAccountViewModel VendorManage { get; set; }
    }
}