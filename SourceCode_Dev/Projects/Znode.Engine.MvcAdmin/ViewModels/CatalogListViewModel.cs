﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class CatalogListViewModel : BaseViewModel
    {

        public CatalogListViewModel()
        {
            Catalogs = new List<CatalogViewModel>();
        }

        public List<CatalogViewModel> Catalogs { get; set; }
    }
}