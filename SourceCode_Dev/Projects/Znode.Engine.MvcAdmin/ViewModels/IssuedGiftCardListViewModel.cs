﻿using System.Collections.Generic;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class IssuedGiftCardListViewModel : BaseViewModel
    {
        public IssuedGiftCardListViewModel()
        {
            IssuedGiftCards = new List<IssuedGiftCardViewModel>();
        }

        public List<IssuedGiftCardViewModel> IssuedGiftCards { get; set; }
    }
}