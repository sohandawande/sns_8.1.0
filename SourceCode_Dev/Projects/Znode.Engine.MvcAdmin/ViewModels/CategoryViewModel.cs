﻿using Resources;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Libraries.Helpers;
using Znode.Libraries.Helpers.Attributes;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// ViewModel for create and edit Category
    /// </summary>
    public class CategoryViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for CategoryViewModel
        /// </summary>
        public CategoryViewModel()
        {
            ProductListViewModel ProductList = new ProductListViewModel();
            Collection<CategoryViewModel> Subcategories = new Collection<CategoryViewModel>();
            Collection<CategoryNodeViewModel> CategoryNodes = new Collection<CategoryNodeViewModel>();
            List<CategoryProfileViewModel> CategoryProfiles = new List<CategoryProfileViewModel>();
        }


        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "rfvCategoryName")]
        [RegularExpression(@"^[^/|$!^*()?{},\\]+$", ErrorMessage = "(Only special characters like #, @, %, &, -, +, _, ', . are allowed in category name.)")]
        [LocalizedDisplayName(Name = RZnodeResources.LabelCategoryName)] 
        public string Name { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelAdditionalDescription)]
        [UIHint("RichTextBox")]
        [AllowHtml]
        public string AlternateDescription { get; set; }

        public int CategoryId { get; set; }
        public Collection<CategoryNodeViewModel> CategoryNodes { get; set; }
        public List<CategoryProfileViewModel> CategoryProfiles { get; set; }

        public DateTime? CreateDate { get; set; }
        public string Custom1 { get; set; }
        public string Custom2 { get; set; }
        public string Custom3 { get; set; }
        public string ExternalId { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelCategoryLabelImageAltText)]
        public string ImageAltTag { get; set; }
        public string ImageFile { get; set; }
        public string ImageLargePath { get; set; }
        public string ImageSmallPath { get; set; }
        public string ImageThumbnailPath { get; set; }
        public string ProductIds { get; set; }
        public string CategoryIds { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelSEODescription)]
        public string SeoDescription { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelSEOKeywords)]
        public string SeoKeywords { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelSEOTitle)]
        public string SeoTitle { get; set; }

        [MaxLength(100)]
        [LocalizedDisplayName(Name = RZnodeResources.LabelSEOFriendlyPageName)]
        [RegularExpression("([A-Za-z0-9-_]+)", ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "revSeoUrl")]
        public string SeoUrl { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelShortDescription)]
        public string ShortDescription { get; set; }


        [LocalizedDisplayName(Name = RZnodeResources.LabelLongDescription)]
        [UIHint("RichTextBox")]
        [AllowHtml]
        public string Description { get; set; }


        [LocalizedDisplayName(Name = RZnodeResources.LabelCategoryTitle)]
        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "rfvCategoryTitle")]
        public string Title { get; set; }
        public DateTime? UpdateDate { get; set; }

        //Znode version 7.2.2 To get Category Banner

        [LocalizedDisplayName(Name = RZnodeResources.LabelCategoryBanner)]
        [UIHint("RichTextBox")]
        [AllowHtml]
        public string CategoryBanner { get; set; }

        public string ImageMediumPath { get; set; }
        public string ImageSmallThumbnailPath { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelDisplayOrder)]
        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "rfvDisplayOrder")]
        [Range(1, 999999999, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "InvalidDisplayOrder")]
        [RegularExpression("^(1|[1-9][0-9]*)$", ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "InvalidDisplayOrder")]
        public int? DisplayOrder { get; set; }

        public bool SubcategoryGridIsVisible { get; set; }
        public bool IsEditMode { get; set; }
        public bool IsVisible { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelChildCategories)]
        public bool ChildCategories { get; set; }
        public bool IsAssociate { get; set; }

        public ProductListViewModel ProductList { get; set; }

        public List<SelectListItem> CatalogList { get; set; }

        public Collection<CategoryViewModel> Subcategories { get; set; }

        [DisplayName("Select Image")]
        [FileTypeValidation(MvcAdminConstants.ImageFileTypes, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "ImageFileTypeErrorMessage")]
        [FileMaxSizeValidation(MvcAdminConstants.ImageMaxFileSize, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "FileSizeExceededErrorMessage")]
        [UIHint("FileUploader")]
        public HttpPostedFileBase SelectImage { get; set; }

        public int? ProductId { get; set; }
        public int? PortalID { get; set; }

        //PRFT Custom Code: Start
        //Property for indicating Category as Manufacturer.
        public bool IsManufacturer { get; set; }
        //PRFT Custom Code: End
    }
}