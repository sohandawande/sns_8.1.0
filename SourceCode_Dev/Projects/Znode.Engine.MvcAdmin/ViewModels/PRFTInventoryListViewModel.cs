﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class PRFTInventoryListViewModel:BaseViewModel
    {
        /// <summary>
        ///  Constructor for CaseRequestsListViewModel 
        /// </summary>
        public PRFTInventoryListViewModel()
        {
            InventoryList = new List<PRFTInventoryViewModel>();
        }

        public List<PRFTInventoryViewModel> InventoryList { get; set; }
    }
}