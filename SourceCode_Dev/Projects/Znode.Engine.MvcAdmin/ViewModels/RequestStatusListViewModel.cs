﻿using System.Collections.Generic;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class RequestStatusListViewModel:BaseViewModel
    {
        public RequestStatusListViewModel()
        {
            RequestStatusList = new List<RequestStatusViewModel>();
        }
        public List<RequestStatusViewModel> RequestStatusList { get; set; }
    }
}