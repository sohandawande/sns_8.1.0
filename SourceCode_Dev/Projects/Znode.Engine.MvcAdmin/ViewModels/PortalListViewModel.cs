﻿using System.Collections.Generic;
using Znode.Engine.MvcAdmin.Models;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// Model for Portal List
    /// </summary>
    public class PortalListViewModel : BaseViewModel
    {
        #region Constructor
        /// <summary>
        /// Constructor for PortalViewModel that initialises the Portals list.
        /// </summary>
        public PortalListViewModel()
        {
            Portals = new List<PortalViewModel>();
        }
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets all Portals list.
        /// </summary>
        public List<PortalViewModel> Portals { get; set; }
        public string SearchStore { get; set; }

        public FilterColumnListModel FilterColumn { get; set; }

        #endregion
    }
}