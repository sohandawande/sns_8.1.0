﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class ReferralCommissionListViewModel : BaseViewModel
    {
        public List<ReferralCommissionViewModel> ReferralCommissions { get; set; }

        /// <summary>
        /// Constructor for ReferralCommssionListViewModel
        /// </summary>
        public ReferralCommissionListViewModel()
        {
            ReferralCommissions = new List<ReferralCommissionViewModel>();
        }

        
    }
}