﻿using System;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// This view model will be used for showing the lists of stores and roles associated with relevant user
    /// </summary>
    public class PermissionViewModel : BaseViewModel
    {
        /// <summary>
        /// Default constructor
        /// </summary>
        public PermissionViewModel()
        { }

        public RolesViewModel Roles { get; set; }
        public ProfilePortalViewModel Stores { get; set; }
        public Guid UserId { get; set; }
        
        public int AccountId { get; set; }        
        public string UserName { get; set; }

        public bool IsRolesVisible { get; set; }
        public bool IsStoresVisible { get; set; }
    }
}