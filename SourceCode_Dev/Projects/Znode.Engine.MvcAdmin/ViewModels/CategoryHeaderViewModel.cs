﻿using System.Collections.Generic;
namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// This is the model for Category Header which is used to show the Main navigation menus
    /// </summary>
    public class CategoryHeaderViewModel 
    {
        public CategoryHeaderViewModel()
        {
            children = new List<CategorySubHeaderViewModel>();
        }

        public string text { set; get; }
        public int id { set; get; }
        public string icon { set; get; }
        public List<CategorySubHeaderViewModel> children { get; set; }
    }
}