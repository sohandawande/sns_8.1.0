﻿using System.Collections.Generic;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model for Shipping Rule
    /// </summary>
    public class ShippingRuleListViewModel : BaseViewModel
    {
        #region Constructor
        /// <summary>
        /// Constructor for Shipping Rule
        /// </summary>
        public ShippingRuleListViewModel()
        {
            ShippingRule = new List<ShippingRuleViewModel>();
        } 
        #endregion

        public List<ShippingRuleViewModel> ShippingRule { get; set; }      

        public int shippingOptionId { get; set; }
    }
}