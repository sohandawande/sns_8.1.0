﻿using System.Collections.Generic;
using Znode.Engine.MvcAdmin.Models;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class ProfileListViewModel : BaseViewModel
    {
        public ProfileListViewModel()
        {
            Profiles = new List<ProfileViewModel>();
            GridModel = new ReportModel();
        }

        public List<ProfileViewModel> Profiles { get; set; }
        public ReportModel GridModel { get; set; }
        public int AccountId { get; set; }
        public string FullName { get; set; }
    }
}