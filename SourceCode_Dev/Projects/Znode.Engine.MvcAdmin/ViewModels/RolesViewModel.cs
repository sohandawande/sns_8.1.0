﻿using System;
using System.Collections.Generic;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// This view model will be used for showing the list of roles associated with user
    /// </summary>
    public class RolesViewModel : BaseViewModel
    {
        /// <summary>
        /// Default constructor
        /// </summary>
        public RolesViewModel()
        { 
        }

        public List<Tuple<string, int, bool>> RolesList { get; set; }
    }
}