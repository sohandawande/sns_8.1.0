﻿using Resources;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Znode.Libraries.Helpers;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// Provide Engine View model
    /// </summary>
    public class ProviderEngineViewModel
    {
        /// <summary>
        /// constructor for Provider Engine view model
        /// </summary>
        public ProviderEngineViewModel()
        {

        }
        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "ErrorRequired")]
        public string Description { get; set; }

        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "ErrorRequired")]
        public string Name { get; set; }

        [MaxLength(50)]
        [LocalizedDisplayName(Name = RZnodeResources.LabelClassName)]
        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "ErrorRequired")]
        public string ClassName { get; set; }
        public string AvailablePromotionType { get; set; }

        public string ClassType { get; set; }

        public int Id { get; set; }

        public bool IsActive { get; set; }



        public List<SelectListItem> AvailablePromotionTypes { get; set; }
    }
}