﻿namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View model for highlight Type
    /// </summary>
    public class HighlightTypeViewModel
    {
        public string Name { get; set; }	
        public string Description { get; set; }

        public int HighlightTypeId { get; set; }       
    }
}