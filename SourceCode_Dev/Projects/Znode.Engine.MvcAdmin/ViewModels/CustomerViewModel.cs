﻿using Resources;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Web.Mvc;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Models;
using Znode.Libraries.Helpers;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class CustomerViewModel : BaseViewModel
    {
        public CustomerViewModel()
        {
            CustomerAccount = new AccountViewModel();
            GridModel = new ReportModel();
            AddressList = new AddressListViewModel();
            Address = new AddressViewModel();
            GetAffilateApprovalStatusList = new List<SelectListItem>();
        }
        [LocalizedDisplayName(Name = RZnodeResources.DisplayNameBilling)]
        public string FullName { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelCompanyName)]
        public string CompanyName { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelPhoneBilling)]
        public string PhoneNumber { get; set; }
        
        public int AccountId { get; set; }
        public AccountViewModel CustomerAccount { get; set; }
        public AddressListViewModel AddressList { get; set; }
        public ReportModel GridModel { get; set; }
        public AddressViewModel Address { get; set; }
        public bool? EnableCustomerPricing { get; set; }
        public string LoginName { get; set; }
        public bool IsConfirmed { get; set; }
        public UserTypes ? UserType { get; set; }
        public int? PortalId { get; set; }

        public bool IsUserAdmin { get; set; }
        
        public List<SelectListItem> GetAffilateApprovalStatusList { get; set; }

        //PRFT Custom Code : Start
        public Collection<PRFTSalesRepresentative> SalesReps { get; set; }
        public bool IsSalesRepFound { get; set; }
        public int SalesRepId { get; set; }
                
        public CustomerListViewModel customerList { get; set; }
        public string Custom1 { get; set; }
        public string Custom2 { get; set; }
        //public string Custom3 { get; set; }
        public int ParentAccountId { get; set; }
        public bool IsCustParentAccountFound { get; set; }
        public int? CustParentAccountId { get; set; }
        public int CustomerUserMappingId { get; set; }
        //PRFT Custom Code : End
    }
}