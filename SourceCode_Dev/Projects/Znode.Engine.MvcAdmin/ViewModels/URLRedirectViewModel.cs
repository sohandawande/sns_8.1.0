﻿using Resources;
using System.ComponentModel.DataAnnotations;
using Znode.Libraries.Helpers;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model for 301 Redirect
    /// </summary>
    public class URLRedirectViewModel : BaseViewModel
    {
        #region Constructor
        /// <summary>
        /// Constructor for URL301RedirectViewModel
        /// </summary>
        public URLRedirectViewModel()
        {

        }
        #endregion

        [LocalizedDisplayName(Name = RZnodeResources.LabelUrlToredirectFrom)]
        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.RequiredFromSeoUrl)]
        public string OldUrl { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelUrlToRedirectTo)]
        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.RequiredToSeoUrl)]
        public string NewUrl { get; set; }

        public int URLRedirectId { get; set; }

        public bool IsActive { get; set; }
    }
}