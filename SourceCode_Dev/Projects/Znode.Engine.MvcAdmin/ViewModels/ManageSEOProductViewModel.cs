﻿using Resources;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Znode.Libraries.Helpers;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model For ManageSEOProductViewModel
    /// </summary>
    public class ManageSEOProductViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for ManageSEOProductViewModel
        /// </summary>
        public ManageSEOProductViewModel()
        {

        }

        [LocalizedDisplayName(Name = RZnodeResources.LabelSEODescription)]
        public string SeoDescription { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelSEOKeywords)]
        public string SeoKeywords { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelSEOTitle)]
        public string SeoTitle { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelSeoPageUrl)]
        [RegularExpression("^[A-Za-z0-9-_]+$", ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "ValidSEOUrl")]
        public string SeoPageUrl { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelShortDescription)]
        public string ShortDescription { get; set; }

        public bool RedirectUrlInd { get; set; }

        public int ProductId { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.CustomerReviewProductName)]
        public string ProductName { get; set; }

        [UIHint("RichTextBox")]
        [AllowHtml]
        [LocalizedDisplayName(Name = RZnodeResources.LabelLongDescription)]
        public string LongDescription { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelProductFeatures)]
        [UIHint("RichTextBox")]
        [AllowHtml]
        public string FeaturesDescription { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelProductSpecification)]
        [UIHint("RichTextBox")]
        [AllowHtml]
        public string ProductSpecifications { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.ManageStoreAdditionalInformation)]
        [UIHint("RichTextBox")]
        [AllowHtml]
        public string ShippingInformation { get; set; }


    }
}