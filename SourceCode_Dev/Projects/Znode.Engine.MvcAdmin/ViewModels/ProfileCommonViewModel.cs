﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class ProfileCommonViewModel : BaseViewModel
    {
        public Guid UserId { get; set; }
        public string ProfileStoreAccess { get; set; }
    }
}