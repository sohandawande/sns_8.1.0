﻿
namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class ProductReviewHistoryViewModel : BaseViewModel
    {
        public int ProductReviewHistoryID { get; set; }
        public int ProductID { get; set; }
        public int VendorID { get; set; }

        public string Status { get; set; }
        public string Reason { get; set; }
        public string Description { get; set; }
        public string Username { get; set; }
        public string LogDate { get; set; }
        public string NotificationDate { get; set; }
        public string ProductName { get; set; }
        public string VendorName { get; set; }
    }
}