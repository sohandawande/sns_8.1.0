﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model For SkuList
    /// </summary>
    public class SkuListViewModel : BaseViewModel
    {
        /// <summary>
        ///  Constructor for SkuListViewModel
        /// </summary>
        public SkuListViewModel()
        {
            Skus = new List<SkuViewModel>();
        }

        public List<SkuViewModel> Skus { get; set; }
    }
}