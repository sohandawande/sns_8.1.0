﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class FilterCollectionDataModel : BaseViewModel
    {
        public Dictionary<string, string> Params { get; set; }

        public FilterCollectionDataModel()
        {
            Params = new Dictionary<string, string>();
        }
    }
}