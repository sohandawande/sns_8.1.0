﻿
namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// ViewModel for Payment
    /// </summary>
    public class PaymentViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for Payment
        /// </summary>
        public PaymentViewModel()
        {

        }

        public string MerchantLogin { get; set; }
        public string TransactionKey { get; set; }
        public string Password { get; set; }
        public string Partner { get; set; }
        public string Vendor { get; set; }

        public int DisplayOrder { get; set; }

        public bool PreAuthorizeTransactionsWithoutCapturing { get; set; }
        public bool? EnableRMA  { get; set; }
        public bool EnableTestModel { get; set; }
        public bool? Visa { get; set; }
        public bool? MasterCard { get; set; }
        public bool? AmericanExpress { get; set; }
        public bool? Discover { get; set; }
        


    }
}