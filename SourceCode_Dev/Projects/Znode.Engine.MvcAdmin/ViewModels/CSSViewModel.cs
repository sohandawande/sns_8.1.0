﻿using Resources;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class CSSViewModel : BaseViewModel
    {
        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "rfvCSSName")]
        public string Name { get; set; }
        public string ThemeName { get; set; }
        public int CSSId{ get; set; }
        public int ThemeId { get; set; }
        public List<SelectListItem> ThemeList { get; set; }
    }
}