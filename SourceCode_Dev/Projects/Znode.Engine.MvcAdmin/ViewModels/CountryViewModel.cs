﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model for Country
    /// </summary>
    public class CountryViewModel : BaseViewModel
    {
        #region Constructor for CountryViewModel
        /// <summary>
        /// Constructor for CountryViewModel
        /// </summary>
        public CountryViewModel()
        {
           
        } 
        #endregion

        public string Code { get; set; }
        public int DisplayOrder { get; set; }
        public bool IsActive { get; set; }
        public string Name { get; set; }

        
    }
}