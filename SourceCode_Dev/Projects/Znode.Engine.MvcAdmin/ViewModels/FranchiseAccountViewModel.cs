﻿using Resources;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Models;
using Znode.Libraries.Helpers;
using Znode.Libraries.Helpers.Attributes;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model for Create and Edit Franchise Account
    /// </summary>
    public class FranchiseAccountViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for Create and Edit Franchise Account View Model
        /// </summary>
        public FranchiseAccountViewModel()
        {
            ContactInfo = new VendorContactViewModel();
            accountViewModel = new AccountViewModel();
            GridModel = new ReportModel();
        }
        [MaxLength(256)]
        [LocalizedDisplayName(Name = RZnodeResources.DisplayUserName)]
        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "ErrorRequired")]
        public string UserName { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.DisplayNameBilling)]
        public string FullName { get; set; }

        [MaxLength(255)]
        [LocalizedDisplayName(Name = RZnodeResources.TextStoreName)]
        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "ErrorRequired")]
        public string StoreName { get; set; }

        [MaxLength(100)]
        [LocalizedDisplayName(Name = RZnodeResources.LabelSiteUrl)]
        [RegularExpression("^(http\\:\\/\\/[a-zA-Z0-9_\\-/]+(?:\\.[a-zA-Z0-9_\\-/]+)*)$", ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "ValidationUrl")]
        public string SiteURL { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelPassword)]
        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "RequiredNewPassword")]
        [RegularExpression("^(?=.*[0-9])(?=.*[a-zA-Z]).{8,}$", ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "ValidPassword")]
        [MaxLength(128, ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "InValidPasswordLength")]
        public string Password { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelConfirmPassword)]
        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "RequiredNewConfirmPassword")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "ErrorPasswordMatch")]
        public string ConformPassword { get; set; }

        [MaxLength(50)]
        [LocalizedDisplayName(Name = RZnodeResources.LabelFranchiseNumber)]
        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "ErrorRequired")]
        public string FranchiseNumber { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelAccountId)]
        public int AccountId { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelTheme)]
        public int ThemeId { get; set; }
        public int CatalogId { get; set; }
        public int ProfileId { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelIsEmailOptional)]
        public bool IsEmailOptional { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelSelectLogo)]
        [FileTypeValidation(MvcAdminConstants.ImageFileTypes, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "ImageFileTypeErrorMessage")]
        [FileMaxSizeValidation(MvcAdminConstants.ImageMaxFileSize, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "FileSizeExceededErrorMessage")]
        [UIHint("FileUploader")]
        public HttpPostedFileBase Logo { get; set; }

        public List<SelectListItem> Themes { get; set; }

        public VendorContactViewModel ContactInfo { get; set; }

        public AccountViewModel accountViewModel { get; set; }

        public ReportModel GridModel { get; set; }

        public string UserType { get; set; }

        public bool CheckRole { get; set; }

        public int PortalId { get; set; }

    }
}