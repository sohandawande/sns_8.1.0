﻿using System.Collections.Generic;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class CategoryTreeListViewModel 
    {
        public CategoryTreeListViewModel()
        {
            Categories = new List<CategoryHeaderViewModel>();
        }

        public List<CategoryHeaderViewModel> Categories { set; get; }
    }
}