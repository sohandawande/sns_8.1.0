﻿using Resources;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Models;
using Znode.Libraries.Helpers;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View model create order
    /// </summary>
    public class OrderViewModel : BaseViewModel
    {
        /// <summary>
        /// Consructor for OrderViewModel
        /// </summary>
        public OrderViewModel()
        {
            OrderShipmentViewModel OrderShipment = new OrderShipmentViewModel();
            OrderLineItemListViewModel OrderLineItemList = new OrderLineItemListViewModel();
            Collection<OrderLineItemModel> OrderLineItems = new Collection<OrderLineItemModel>();
            RMARequestListViewModel rmaRequestList = new RMARequestListViewModel();
            List<string> OrderLineItemShippingAddressList = new List<string>();
        }
        public int? AccountId { get; set; }
        public string AdditionalInstructions { get; set; }
        public string BillingCity { get; set; }
        public string BillingCompanyName { get; set; }
        public string BillingCountryCode { get; set; }
        public string BillingEmail { get; set; }
        public string BillingFirstName { get; set; }
        public string BillingLastName { get; set; }
        public string BillingPhoneNumber { get; set; }
        public string BillingPostalCode { get; set; }
        public string BillingStateCode { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelBillingAddress)]
        public string BillingStreetAddress1 { get; set; }
        public string BillingStreetAddress2 { get; set; }
        public string CardAuthorizationCode { get; set; }
        public string CardExpiration { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelTransactionID)]
        public string CardTransactionId { get; set; }
        public int? CardTypeId { get; set; }
        public string CouponCode { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CreatedBy { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelDiscountAmount)]
        public string DiscountAmount { get; set; }
        public string ExternalId { get; set; }
        public decimal? Gst { get; set; }
        public decimal? Hst { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelOrderId)]
        public int OrderId { get; set; }
        public Collection<OrderLineItemModel> OrderLineItems { get; set; }
        public int? OrderStateId { get; set; }
        public int? PaymentOptionId { get; set; }
        public int? PaymentStatusId { get; set; }
        public int? PaymentTypeId { get; set; }
        public int? PortalId { get; set; }
        public string PromotionDescription { get; set; }
        public decimal? Pst { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelPurchaseOrder)]
        public string PurchaseOrderNumber { get; set; }
        public string ReceiptHtml { get; set; }
        public bool IsOffsitePayment { get; set; }
        public int? ReferralAccountId { get; set; }
        public DateTime? ReturnDate { get; set; }
        public decimal? SalesTax { get; set; }
        public string ShipDate { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelShippingAmount)]
        public string ShippingCost { get; set; }
        public int? ShippingOptionId { get; set; }
        public decimal? SubTotal { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelTaxAmount)]
        public string TaxCost { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelOrderTotal)]
        public string Total { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelTrackingNumber)]
        public string TrackingNumber { get; set; }
        public decimal? Vat { get; set; }
        public DateTime? WebServiceDownloadDate { get; set; }
        public OrderShipmentViewModel OrderShipment { get; set; }
        public int? ShippingID { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelShippingMethod)]
        public string ShippingTypeName { get; set; }
        public int? PaymentSettingId { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelGiftCardAmount)]
        public string GiftAmount { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelPaymentMethod)]
        public string PaymentMethod { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelPromotionCodes)]
        public string PromoCode { get; set; }
        public OrderLineItemListViewModel OrderLineItemList { get; set; }
        public string StoreName { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelOrderStatus)]
        public string OrderStatus { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelPaymentStatus)]
        public string PaymentStatus { get; set; }
        public string PaymentType { get; set; }
        public string Name { get; set; }
        public decimal Amount { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelOrderDate)]
        public string OrderDate { get; set; }

        public ReportModel OrderLineItemsGridModel { get; set; }
        public ReportModel RmaRequetsGridModel { get; set; }

        public List<SelectListItem> OrderStatusList { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelCustomerName)]
        public string FullName { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelCreditCard)]
        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "rfvCreditCardNumber")]
        public string RefundCardNumber { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelExpirationDate)]
        public string ExpirationDate { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelSelectMonth)]
        public int Month { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelSelectYear)]
        public int Year { get; set; }
        public List<SelectListItem> MonthList { get; set; }
        public List<SelectListItem> YearList { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelSecurityCode)]
        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "rfvSecurityCode")]
        public string SecurityCode { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelAmountToRefund)]
        public string RefundAmount { get; set; }
        public DateTime? BeginDate { get; set; }
        public DateTime? EndDate { get; set; }
        public ReportModel DynamicGrid { get; set; }
        [Range(1, int.MaxValue, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "revValidDownloadOrderId")]
        [LocalizedDisplayName(Name = RZnodeResources.LabelDownloadOrderId)]
        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "rfvDownloadOrderId")]
        public int? StartingOrderId { get; set; }
        public string EmailId { get; set; }
        public RMARequestListViewModel rmaRequestList { get; set; }
        public OrderLineItemViewModel OrderLineItemViewModel { get; set; }
        public List<string> OrderLineItemShippingAddressList { get; set; }
        public bool isMultipleShipping { get; set; }
        public string PaymentTypeName { get; set; }

        public string Custom1 { get; set; } //store External Account No. of Customer
    }
}