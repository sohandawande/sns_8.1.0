﻿using Resources;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web.Mvc;
using Znode.Engine.MvcAdmin.Models;
using Znode.Libraries.Helpers;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// List View Model for MessageConfig
    /// </summary>
    public class ManageMessageListViewModel : BaseViewModel
    {
        #region Constructor

        /// <summary>
        /// Constructor for MessageConfigListViewModel 
        /// </summary>
        public ManageMessageListViewModel()
        {
            ManageMessage = new List<ManageMessageViewModel>();
            Portal = new List<PortalViewModel>();
            GridModel = new ReportModel();
        }
 
        #endregion

        public int PortalId { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelMessage)]
        public string Message { get; set; }   
        [LocalizedDisplayName(Name = RZnodeResources.LabelStoreName)]
        public string StoreName { get; set; }

        public List<PortalViewModel> Portal { get; set; }
        public List<SelectListItem> Portals { get; set; }
        public List<ManageMessageViewModel> ManageMessage { get; set; }
        public ReportModel GridModel { get; set; }
    }
}