﻿using System.Collections.Generic;
using Znode.Engine.MvcAdmin.Models;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class AddOnValueListViewModel : BaseViewModel
    {
        public AddOnValueListViewModel()
        {
            AddOnValues = new List<AddOnValueViewModel>();
        }
        public List<AddOnValueViewModel> AddOnValues { get; set; }
    }
}
