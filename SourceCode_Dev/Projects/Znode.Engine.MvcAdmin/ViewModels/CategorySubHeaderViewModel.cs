﻿using System.Collections.Generic;
namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// This is the model for CategorySubHeaderViewModel to show the Sub Menus
    /// </summary>
    public class CategorySubHeaderViewModel 
    {
        public CategorySubHeaderViewModel()
        {
            children = new List<CategorySubHeaderViewModel>();
        }
        public string text { set; get; }
        public int id { set; get; }      
        public string icon { set; get; }
        public List<CategorySubHeaderViewModel> children { get; set; }
    }
}