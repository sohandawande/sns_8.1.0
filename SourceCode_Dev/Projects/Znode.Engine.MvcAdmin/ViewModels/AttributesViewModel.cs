﻿using System.ComponentModel.DataAnnotations;
using Resources;
using Znode.Engine.MvcAdmin.Helpers;
using System.Collections.ObjectModel;
using Znode.Libraries.Helpers;


namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class AttributesViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for Attributes View Model
        /// </summary>
        public AttributesViewModel()
        {

        }
        [LocalizedDisplayName(Name = RZnodeResources.LabelAttributeValue)]
        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "ErrorRequired")]
        public string Name { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelDisplayOrder)]
        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "ErrorRequired")]
        [Range(1, 999999999, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "ValidNumber")]
        [RegularExpression("^(1|[1-9][0-9]*)$", ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "InvalidDisplayOrder")]
        public int? DisplayOrder { get; set; }

        public int AttributeId { get; set; }
        public int AttributeTypeId { get; set; }

        public bool IsActive { get; set; }

        public string AttributeTypeName { get; set; }

        public Collection<int> SkuIds { get; set; }
        public bool Available { get; set; }
        public bool Selected { get; set; }
    }
}