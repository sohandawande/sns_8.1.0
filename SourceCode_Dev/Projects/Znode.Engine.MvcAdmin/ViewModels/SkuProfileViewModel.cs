﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View model for Sku Profile.
    /// </summary>
    public class SkuProfileViewModel : BaseViewModel
    {
        #region Constructor
        /// <summary>
        /// Default Constructor
        /// </summary>
        public SkuProfileViewModel()
        {

        } 
        #endregion

        public int ProfileId { get; set; }

        public DateTime EffectiveDate { get; set; }

     
    }
}