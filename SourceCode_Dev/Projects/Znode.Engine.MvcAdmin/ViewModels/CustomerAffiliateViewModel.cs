﻿using Resources;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Znode.Engine.MvcAdmin.Models;
using Znode.Libraries.Helpers;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class CustomerAffiliateViewModel : BaseViewModel
    {
        public CustomerAffiliateViewModel()
        {
            AccountPaymentGridModel = new ReportModel();
        }

        [MaxLength(100)]
        [LocalizedDisplayName(Name = RZnodeResources.LabelTrackingLink)]
        public string TrackingLink { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelAmountOwed)]
        public string AmountOwed { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelCommissionType)]
        public string ReferralCommissionType { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelTaxId)]
        public string TaxID { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelPartnerApprovalStatus)]
        public string ReferalStatus { get; set; }
        public string Status { get; set; }
        public int AccountId { get; set; }
        public ReportModel AccountPaymentGridModel { get; set; }

        public ReportModel ReferralCommissionGridModel { get; set; }

        public int? ReferralCommisionTypeId { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelCommission)]
        public decimal? ReferralCommission { get; set; }

        public List<SelectListItem> ReferralCommissionTypeList { get; set; }
        public List<SelectListItem> PartnerApprovalStatusList { get; set; }
    }
}