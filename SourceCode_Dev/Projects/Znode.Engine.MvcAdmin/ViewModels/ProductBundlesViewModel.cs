﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.MvcAdmin.Models;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class ProductBundlesViewModel : BaseViewModel
    {
        public ProductBundlesViewModel()
        {
            GridModel = new ReportModel();
            Products = new ProductListViewModel();
            Catalogs = new CatalogListViewModel();
            ProductTypes = new ProductTypeListViewModel();
        }

        public int ParentChildProductID { get; set; }
        public int? ParentProductID { get; set; }
        public int? ChildProductID { get; set; }
        public string ProductName { get; set; }
        public bool? IsActive { get; set; }

        public int CatalogId { get; set; }
        public int ProductTypeId { get; set; }
        public string ProductNumber { get; set; }
        public string ProductSku { get; set; }
        public string ProdutCategory { get; set; }

        public int ProductId { get; set; }
        public string ProductIds { get; set; }

        public ProductTypeListViewModel ProductTypes { get; set; }
        public CatalogListViewModel Catalogs { get; set; }

        public string AssociatedProductIds { get; set; }

        public ReportModel GridModel { get; set; }
        public ProductListViewModel Products { get; set; }
    }
}
