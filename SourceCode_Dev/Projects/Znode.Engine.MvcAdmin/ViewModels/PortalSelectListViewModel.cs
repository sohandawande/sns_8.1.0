﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class PortalSelectListViewModel : BaseViewModel
    {
        public List<SelectListItem> PortalSelectList { get; set; }
        public int SelectedPortalId { get; set; }
    }
}