﻿namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model for Default SEO Setting 
    /// </summary>
    public class DefaultSEOSettingViewModel : BaseViewModel
    {
        /// <summary>
        /// Default Constructor for DefaultSEOSettingViewModel
        /// </summary>
        public DefaultSEOSettingViewModel()
        {

        }

        public string SEOProductTitle { get; set; }
        public string SEOProductDescription { get; set; }
        public string SEOProductKeyword { get; set; }
        public string SEOCategoryTitle { get; set; }
        public string SEOCategoryDescription { get; set; }
        public string SEOCategoryKeyword { get; set; }
        public string SEOContentTitle { get; set; }
        public string SEOContentDescription { get; set; }
        public string SEOContentKeyword { get; set; }

        public int StoreId { get; set; }
    }
}