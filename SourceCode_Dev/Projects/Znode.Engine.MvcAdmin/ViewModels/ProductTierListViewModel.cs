﻿using System.Collections.Generic;
using Znode.Engine.MvcAdmin.Models;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class ProductTierListViewModel : BaseViewModel
    {
        public ProductTierListViewModel()
        {
            Tiers = new List<ProductTierPricingViewModel>();
        }

        public List<ProductTierPricingViewModel> Tiers { get; set; }

        public ReportModel GridModel { get; set; }
        public int ProductId { get; set; }
        public int? PortalId { get; set; }
    }
}