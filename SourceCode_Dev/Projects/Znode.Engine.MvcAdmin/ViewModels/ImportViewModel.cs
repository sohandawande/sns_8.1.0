﻿using Resources;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Helpers;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Libraries.Helpers;
using Znode.Libraries.Helpers.Attributes;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class ImportViewModel : BaseViewModel
    {
        [LocalizedDisplayName(Name = RZnodeResources.LabelSelectFileToImport)]
        [FileTypeValidation(MvcAdminConstants.ImportFileType, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "ImportFileTypeErrorMessage")]
        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "ImportFileRequiredErrorMessage")]
        [UIHint("FileUploader")]
        public HttpPostedFileBase ImportFile { get; set; }
        public string Type { get; set; }
        public bool ImportStatus { get; set; }
        public List<WebGridColumn> WebGridColumn { get; set; }
        public List<dynamic> DataTableList { get; set; }
        public ImportConfigUploadHeadViewModel ImportConfigData { get; set; }
    }

    public class ImportConfigUploadHeadViewModel : BaseViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string TemplateUniqueName { get; set; }
        public string DownloadLink { get; set; }
        public int? Status { get; set; }
        public string Description { get; set; }
    }
}