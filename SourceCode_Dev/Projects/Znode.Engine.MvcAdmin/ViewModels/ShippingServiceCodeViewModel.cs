﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model For Shipping servide Code
    /// </summary>
    public class ShippingServiceCodeViewModel : BaseViewModel
    {
        #region Constructor
        /// <summary>
        /// Constructor for Shipping Service Code View Model
        /// </summary>
        public ShippingServiceCodeViewModel()
        {

        } 
        #endregion

        public string Code { get; set; }
        public string Description { get; set; }
        public int ShippingServiceCodeId { get; set; }
        public int? DisplayOrder { get; set; }
        public bool IsActive { get; set; }        
    }
}