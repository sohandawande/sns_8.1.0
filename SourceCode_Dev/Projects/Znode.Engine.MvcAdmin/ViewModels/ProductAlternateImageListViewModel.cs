﻿using System.Collections.Generic;
using Znode.Engine.MvcAdmin.Models;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class ProductAlternateImageListViewModel : BaseViewModel
    {
         /// <summary>
        /// Constructor for Product alternate image.
        /// </summary>
        public ProductAlternateImageListViewModel()
        {
            Images = new List<ProductAlternateImageViewModel>();
            GridModel = new ReportModel();
            ImageReviewStates = new ProductReviewStateListViewModel();
            SwatchGridModel = new ReportModel();
        }

        public List<ProductAlternateImageViewModel> Images { get; set; }
        public ReportModel GridModel { get; set; }
        public int ProductId { get; set; }
        public ProductReviewStateListViewModel ImageReviewStates { get; set; }
        public int? ProductImageTypeId { get; set; }
        public string SelectedProductIds { get; set; }
        public string ReviewState { get; set; }        
        public ReportModel SwatchGridModel { get; set; }
        public string ImageSmallThumbnailPath { get; set; }
        public int? PortalId { get; set; }
    }
}