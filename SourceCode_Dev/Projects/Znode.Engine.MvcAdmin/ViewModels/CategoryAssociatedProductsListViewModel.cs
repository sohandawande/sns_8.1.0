﻿using System.Collections.Generic;
using Znode.Engine.MvcAdmin.Models;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// List view models for Product Category
    /// </summary>
    public class CategoryAssociatedProductsListViewModel:BaseViewModel
    {
        /// <summary>
        /// Constructor for Product Category
        /// </summary>
        public CategoryAssociatedProductsListViewModel()
        {
            AssociatedProducts = new List<CategoryAssociatedProductsViewModel>();
            GridModel = new ReportModel();
        }       
        public string CategoryName { get; set; }

        public int? CategoryId { get; set; }

        public List<CategoryAssociatedProductsViewModel> AssociatedProducts { get; set; }
        public ReportModel GridModel { get; set; }
    }
}