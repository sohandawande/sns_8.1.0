﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Resources;
using System.Web.Mvc;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class ReviewItemViewModel : BaseViewModel
    {
        public int? AccountId { get; set; }

        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "RequiredComments")]
        public string Comments { get; set; }

        public string CreateDate { get; set; }

        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "RequiredReviewName")]
        public string CreateUser { get; set; }

        public string ProductName { get; set; }

        public int? ProductId { get; set; }

        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "RequiredRating")]
        public int Rating { get; set; }

        public int ReviewId { get; set; }

        public string Status { get; set; }

        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "RequiredReviewHeadline")]
        public string Subject { get; set; }

        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "RequiredReviewLocation")]
        public string UserLocation { get; set; }

        public string Duration { get; set; }

        public List<SelectListItem> GetReviewStatus { get; set; }

        public List<SelectListItem> GetReviewRatings { get; set; }
    }
}