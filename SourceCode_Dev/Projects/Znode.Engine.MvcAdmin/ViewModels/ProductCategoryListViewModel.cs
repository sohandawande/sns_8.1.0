﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.MvcAdmin.Models;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class ProductCategoryListViewModel : BaseViewModel
    {
        public ProductCategoryListViewModel()
        {
            GridModel = new ReportModel();
            CategoryList = new CategoryListViewModel();
        }
        public ReportModel GridModel { get; set; }
        public CategoryListViewModel CategoryList { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; } 
    }
}