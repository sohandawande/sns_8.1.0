﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Models;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class ReviewViewModel : BaseViewModel
    {
        public ReviewViewModel()
        {
            ReviewItems = new List<ReviewItemViewModel>();
            GridModel = new ReportModel();
        }
        public List<ReviewItemViewModel> ReviewItems { get; set; }
        public FilterColumnListModel FilterColumn { get; set; }
        public List<SelectListItem> GetReviewStatusList { get; set; }
        public ReportModel GridModel { get; set; }
    }
}