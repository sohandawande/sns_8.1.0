﻿using System.Collections.Generic;
using System.Web.Mvc;
using Znode.Engine.MvcAdmin.Models;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// List View Model for Store Locator.
    /// </summary>
    public class StoreLocatorListViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for Store Locator
        /// </summary>
        public StoreLocatorListViewModel()
        {
            StoreLocators = new List<StoreLocatorViewModel>();
            StoreList = new List<SelectListItem>();
        }

        public List<StoreLocatorViewModel> StoreLocators { get; set; }
        
        //List for Dropdown
        public List<SelectListItem> StoreList { get; set; }

        public FilterColumnListModel FilterColumn { get; set; }
    }
}