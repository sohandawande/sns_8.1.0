﻿
using Resources;
using System.ComponentModel.DataAnnotations;
using Znode.Libraries.Helpers;
namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model to display domains
    /// </summary>
    public class DomainViewModel:BaseViewModel
    {
        #region Public Properties

        public int DomainId { get; set; }
        public int PortalId { get; set; }

        [MaxLength(100)]
        [LocalizedDisplayName(Name = RZnodeResources.LabelURLName)]
        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "ErrorRequired")]
        [RegularExpression("^(http\\:\\/\\/[a-zA-Z0-9_\\-/]+(?:\\.[a-zA-Z0-9_\\-/]+)*)$", ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "ValidationUrl")]
        public string DomainName { get; set; }
        public string ApiKey { get; set; }

        public bool IsActive { get; set; }

        public string OldDomainName{ get; set; }
        #endregion
    }
}