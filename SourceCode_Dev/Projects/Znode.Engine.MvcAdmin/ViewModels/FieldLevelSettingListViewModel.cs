﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class FieldLevelSettingListViewModel :BaseViewModel
    {
        public FieldLevelSettingListViewModel()
        {
            Fields = new List<FieldLevelSettingViewModel>();
        }
         public List<FieldLevelSettingViewModel> Fields { get; set; }
    }
}
