﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class VendorAccountDownloadListViewModel :  BaseViewModel
    {
        public List<VendorAccountDownloadViewModel> VendorAccountDownloadList { get; set; }
        public VendorAccountDownloadListViewModel()
        {
            VendorAccountDownloadList = new List<VendorAccountDownloadViewModel>();
        }
    }
}