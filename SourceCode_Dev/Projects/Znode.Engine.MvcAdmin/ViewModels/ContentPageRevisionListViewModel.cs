﻿using System.Collections.Generic;
using Znode.Engine.MvcAdmin.Models;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// List view model of Content Page Revision
    /// </summary>
    public class ContentPageRevisionListViewModel : BaseViewModel
    {
        #region Constructor
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public ContentPageRevisionListViewModel()
        {
            ContentPageRevisions = new List<ContentPageRevisionViewModel>();
            GridModel = new ReportModel();
        } 
        #endregion

        public List<ContentPageRevisionViewModel> ContentPageRevisions { get; set; }

        public string PageName { get; set; }

        public ReportModel GridModel { get; set; }
        public ContentPageViewModel contentPageViewModel { get; set; }
       
    }
}