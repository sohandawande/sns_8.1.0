﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using Znode.Libraries.Helpers;
using Resources;


namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class ExportViewModel : BaseViewModel
    {
        public string Type { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelSelectFileType)]
        public string FileType { get; set; }
        public List<SelectListItem> FileTypes { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelSelectCatalog)]
        public int SelectedCatalogId { get; set; }
        public List<SelectListItem> Catalogs { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelSelectAttributeType)]
        public int SelectedAttributeTypeId { get; set; }
        public List<SelectListItem> AttributeTypes { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelSelectCategory)]
        public int SelectedCategoryId { get; set; }
        public List<SelectListItem> Categories { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelBeginDate)]
        public string StartDate { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelEndDate)]
        public string EndDate { get; set; }

        public List<dynamic> ExportDataSet { get; set; }

        public string UserName { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelProductName)]
        public string ProductName { get; set; }
        public int ProductId { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelSelectProductAttributes)]
        public string ProductAttributes { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelSelectCountry)]
        public string SelectedCountryCode { get; set; }
        public List<SelectListItem> Countries { get; set; }
    }
}