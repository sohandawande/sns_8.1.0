﻿using Resources;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Znode.Libraries.Helpers;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model for Account.
    /// </summary>
    public class AccountViewModel : BaseViewModel
    {
        /// <summary>
        /// Deafult Constructor.
        /// </summary>
        public AccountViewModel()
        {
            CountryList = new List<SelectListItem>();
        }

        [LocalizedDisplayName(Name = RZnodeResources.LabelAccountId)]
        public int AccountId { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelCompanyName)]
        public string CompanyName { get; set; }
        public string CommissionType { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelIsEmailOptional)]
        public bool EmailOptIn { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelCustomerBasedPricingEnabled)]
        public bool? EnableCustomerPricing { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelExternalId)]
        public string ExternalId { get; set; }
        public bool? IsActive { get; set; }		
        public int? ProfileId { get; set; }		
        public Guid? UserId { get; set; }
        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "RequiredUserName")]
        [LocalizedDisplayName(Name = RZnodeResources.LabelLoginName)]
        public string UserName { get; set; }

        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "RequiredEmailID")]
        [EmailAddress(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "ValidEmailAddress", ErrorMessage = "")]
        [LocalizedDisplayName(Name = RZnodeResources.LabelEmail)]
        public string EmailAddress { get; set; }
        public string PasswordQuestion { get; set; }
        
        public string PasswordAnswer { get; set; }
        public bool UseWholeSalePricing { get; set; }
        //public bool IsResetPassword { get; set; }
        public string BaseUrl { get; set; }

        //new added field

        [LocalizedDisplayName(Name = RZnodeResources.LabelCreateDate)]
        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "ErrorRequired")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Date)]
        public DateTime? CreateDte { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelUpdateDate)]
        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "ErrorRequired")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Date)]
        public DateTime? UpdateDte { get; set; }

        public string Description { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelWebsite)]
        public string Website { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.DisplayCreateUser)]
        public string CreateUser { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.DisplayUpdateUser)]
        public string UpdateUser { get; set; }
        public string Custom1 { get; set; }
        public string Custom2 { get; set; }
        public string Custom3 { get; set; }

        public int? PortalId { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelSource)]
        public string Source { get; set; }
        public bool IsUserExists { get; set; }
        public List<SelectListItem> CountryList { get; set; }

        public Collection<AddressViewModel> Addresses { get; set; }
        public WishListViewModel WishList { get; set; }
        public Collection<OrdersViewModel> Orders { get; set; }

        //PRFT Custom Code: Start
        public int? ParentAccountID { get; set; }        
        //PRFT Custom Code: End
    }
}