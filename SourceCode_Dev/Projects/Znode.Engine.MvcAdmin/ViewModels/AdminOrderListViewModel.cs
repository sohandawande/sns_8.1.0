﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class AdminOrderListViewModel : BaseViewModel
    {
        public AdminOrderListViewModel()
        {
            OrderList = new List<AdminOrderViewModel>();
        }

        public List<AdminOrderViewModel> OrderList { get; set; }
    }
}