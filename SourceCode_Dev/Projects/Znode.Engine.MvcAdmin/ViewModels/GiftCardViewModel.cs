﻿using Resources;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Znode.Libraries.Helpers;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model for Create and Edit Gift Cards
    /// </summary>
    public class GiftCardViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for GiftCardViewModel
        /// </summary>
        public GiftCardViewModel()
        {

        }
        [MaxLength(40)]
        [LocalizedDisplayName(Name = RZnodeResources.LabelgiftGridHeaderCardNumber)]
        public string CardNumber { get; set; }

        [MaxLength(100)]
        [LocalizedDisplayName(Name = RZnodeResources.LabelGiftCardName)]
        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "RequiredGiftCardName")]
        public string Name { get; set; }
        public string FormatCreateDate { get; set; }
        public string FormatExpirationDate { get; set; }

        public int GiftCardId { get; set; }
        public int CreatedBy { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.TextStoreName)]
        public int PortalId { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelAccountId)]
        [Range(1, 999999999, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "ValidNumber")]
        public int? AccountId { get; set; }

        public string CreateDate { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelgiftGridHeaderExpirationDate)]
        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "RequiredExpirationDate")]
        public string ExpirationDate { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelGiftCardAmount)]
        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "RequiredGiftCardAmount")]
        [Range(0.01, 99999.00, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "ValidAmountRange")]
        [RegularExpression("^\\d{0,9}(\\.\\d{0,2})?$", ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "ValidAmountRange")]
        public decimal? Amount { get; set; }

        public string DisplayAmount { get; set; }
        public bool EnableToCustomerAccount { get; set; }
        public List<SelectListItem> PortalList { get; set; }

        public string Mode { get; set; }
        public int RMARequestId { get; set; }

        public string OrderLineItems { get; set; }
        public string QuantityList { get; set; }
        public int OrderId { get; set; }

        public bool SendMail { get; set; }
    }
}
