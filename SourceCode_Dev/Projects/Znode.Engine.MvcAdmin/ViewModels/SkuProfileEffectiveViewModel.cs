﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.Api.Models;
using Znode.Libraries.Helpers;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View model for Sku Profile Effective.
    /// </summary>
    public class SkuProfileEffectiveViewModel : BaseViewModel
    {
        #region Constructor
        /// <summary>
        /// Constructor for HighlightViewModel
        /// </summary>
        public SkuProfileEffectiveViewModel()
        {

        } 
        #endregion

        public int SkuProfileEffectiveID { get; set; }
        public int SkuId { get; set; }
        
        [LocalizedDisplayName(Name = RZnodeResources.LabelProfile)]
        public int ProfileId { get; set; }

        public string ProfileName { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelEffectiveDate)]
        public string EffectiveDate { get; set; }
        
        public List<SelectListItem> ProfileList { get; set; }
        
        public bool IsEditMode { get; set; }
    }
}