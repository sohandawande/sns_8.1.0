﻿
namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// Group By View Model
    /// </summary>
    public class GroupByViewModel : BaseViewModel
    {
        #region Constructor
        /// <summary>
        /// Constructor of Group By View Model
        /// </summary>
        public GroupByViewModel()
        {

        }
        
        #endregion

        public int GroupById { get; set; }
        public string GroupByName { get; set; }
    }
}