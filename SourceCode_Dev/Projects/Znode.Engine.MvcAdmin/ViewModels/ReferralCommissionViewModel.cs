﻿
namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class ReferralCommissionViewModel : BaseViewModel
    {
        public string TransactionId { get; set; }
        public string Description { get; set; }
        public string ReferralCommissionType { get; set; }

        public int ReferralAccountId { get; set; }
        public int ReferralCommissionId { get; set; }
        public int ReferralCommissionTypeId { get; set; }

        public int? OrderId { get; set; }

        public decimal ReferralCommission { get; set; }
        public decimal? CommissionAmount { get; set; }

        public string DisplayCommissionAmount { get; set; }
    }
}