﻿using Resources;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Znode.Libraries.Helpers;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class EmailTemplateViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for EmailTemplateViewModel
        /// </summary>
        public EmailTemplateViewModel()
        { 
        
        }

        [RegularExpression("^[-_0-9a-zA-Z]+$", ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.revContentPageName)]
        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.RequiredName)]
        [LocalizedDisplayName(Name = RZnodeResources.LabelFileName)]
        [MaxLength(50)]
        public string TemplateName { get; set; }

        [UIHint("RichTextBox")]
        [AllowHtml]
        public string Html { get; set; }
        public string Name { get; set; }
        public string Extension { get; set; }
        public string Key { get; set; }
        public string HtmlStringForm { get; set; }

        public bool IsEditMode { get; set; }
        public bool IsNewAdd { get; set; }
        
        public List<SelectListItem> DeletedTemplateNames { get; set; }
    }
}