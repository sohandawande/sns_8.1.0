﻿using System.Collections.Generic;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// List View Model for Zip Code
    /// </summary>
    public class ZipCodeListViewModel : BaseViewModel
    {
        #region Constructor for ZipCodeListViewModel
        /// <summary>
        /// Constructor for ZipCodeListViewModel
        /// </summary>
        public ZipCodeListViewModel()
        {
            ZipCodes = new List<ZipCodeViewModel>();
        } 
        #endregion

        public List<ZipCodeViewModel> ZipCodes { get; set; }               
    }
}
   