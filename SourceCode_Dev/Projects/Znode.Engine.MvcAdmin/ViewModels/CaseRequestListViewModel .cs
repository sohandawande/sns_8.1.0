﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// List View Model for CaseRequests
    /// </summary>
    public class CaseRequestListViewModel  : BaseViewModel
    {
        /// <summary>
        ///  Constructor for CaseRequestsListViewModel 
        /// </summary>
        public CaseRequestListViewModel()
        {
            CaseRequestsList = new List<CaseRequestViewModel>();
        }

        public List<CaseRequestViewModel> CaseRequestsList{ get; set; }
    }
}