﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.MvcAdmin.Models;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class FacetGroupListViewModel : BaseViewModel
    {
        public FacetGroupListViewModel()
        {
            FacetGroups = new List<FacetGroupViewModel>();
            GridModel = new ReportModel();
            Catalogs = new CatalogListViewModel();
        }
        public List<FacetGroupViewModel> FacetGroups { get; set; }

       
        public ReportModel GridModel { get; set; }
        public CatalogListViewModel Catalogs { get; set; }
    }
}