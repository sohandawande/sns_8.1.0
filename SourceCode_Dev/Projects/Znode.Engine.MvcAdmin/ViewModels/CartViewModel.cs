﻿using Resources;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Web.Mvc;
using Znode.Engine.Api.Models;
using Znode.Libraries.Helpers;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class CartViewModel : BaseViewModel
    {        
        public bool GiftCardApplied { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelGiftCardAmount)]
        public decimal GiftCardAmount { get; set; }
        public string GiftCardMessage { get; set; }
        public string GiftCardNumber { get; set; }
        public bool GiftCardValid { get; set; }
        public decimal GiftCardBalance { get; set; }
        public int CookieId { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelSubTotal)]
        public decimal? SubTotal { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelTax)]
        public decimal TaxCost { get; set; }
        public decimal TaxRate { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelDiscount)]
        public decimal Discount { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelOrderTotal)]
        public decimal? Total { get; set; }
        public decimal? Vat { get; set; }
        public int Count { get; set; }
        public ShippingModel Shipping { get; set; }
        public Collection<CartItemViewModel> Items { get; set; }
        public Collection<AddressViewModel> Addresses { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.HeaderShipping)]
        public decimal ShippingCost { get; set; }
        public List<SelectListItem> ShippingServices { get; set; }
        public List<SelectListItem> PaymentTypeList { get; set; }
        public int ShippingServiceCodeId { get; set; }
        public int PaymentOptionId { get; set; }
        public string PurchaseOrderNumber { get; set; }

        public List<CouponViewModel> Coupons { get; set; }
        public CartViewModel()
        {
            Items = new Collection<CartItemViewModel>();
            Addresses = new Collection<AddressViewModel>();
            Coupons = new List<CouponViewModel>();
        }
    }
}