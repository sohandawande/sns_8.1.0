﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Znode.Libraries.Helpers;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class PortalCatalogViewModel : BaseViewModel
    {
        public CatalogViewModel Catalog { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelCatalog)]
        public int CatalogId { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelStylesheet)]
        public int? CssId { get; set; }
        public int LocaleId { get; set; }
        public int PortalCatalogId { get; set; }
        public PortalViewModel Portal { get; set; }
        public int PortalId { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelTheme)]
        public int? ThemeId { get; set; }
        public string CSSName { get; set; }
        public string ThemeName { get; set; }
        public string CatalogName { get; set; }
    }
}