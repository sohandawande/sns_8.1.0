﻿using System.Collections.Generic;
using Znode.Engine.MvcAdmin.Models;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class CatalogAssociatedCategoriesListViewModel : BaseViewModel
    {
        public CatalogAssociatedCategoriesListViewModel()
        {
            AssociatedCategories = new List<CatalogAssociatedCategoriesViewModel>();
        }

        public List<CatalogAssociatedCategoriesViewModel> AssociatedCategories { get; set; }

        public ReportModel Grid { get; set; }
        public int CatalogId { get; set; }
    }
}