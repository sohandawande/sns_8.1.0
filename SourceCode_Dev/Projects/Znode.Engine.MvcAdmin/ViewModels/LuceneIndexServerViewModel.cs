﻿using System;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class LuceneIndexServerViewModel:BaseViewModel
    {
        public string ServerName { get; set; }
        public string Status { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime? EndTime { get; set; }
    }
}