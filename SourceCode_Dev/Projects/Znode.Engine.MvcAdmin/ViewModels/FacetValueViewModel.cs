﻿
namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class FacetValueViewModel : BaseViewModel
    {
        public string AttributeValue { get; set; }
        public long FacetCount { set; get; }
        public string RefineByUrl { get; set; }
    }
}