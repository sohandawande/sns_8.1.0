﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model for Add AttributeTypeValue
    /// </summary>
    public class AttributeTypeValueViewModel : BaseViewModel
    {
         /// <summary>
        /// Constructor for AttributeTypeValueModel
        /// </summary>

        public AttributeTypeValueViewModel()
        { 
        
        }
        public int AttributeTypeId { get; set; }
        public string AttributeName { get; set; }
        public bool IsGiftCard { get; set; }
        public List<SelectListItem> AttributeValueList { get; set; }
    }
}