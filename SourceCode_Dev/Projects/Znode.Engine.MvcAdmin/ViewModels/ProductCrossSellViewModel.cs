﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class ProductCrossSellViewModel : BaseViewModel
    {
        public string ProductName { get; set; }
        public string CrossSellProducts { get; set; }
        public string SKU { get; set; }
        
        public int ProductCrossSellTypeId { get; set; }
        public int ProductId { get; set; }
        public int RelatedProductId { get; set; }
        public int RelationTypeId { get; set; }
        public string RelatedProductName { get; set; }
        public int CategoryId { get; set; }
        public int PortalId { get; set; }
        public string CategoryName  { get; set; }

        public int? DisplayOrder { get; set; }

        public string FrequentlyBoughtProduct1 { get; set; }
        public string FrequentlyBoughtProduct2 { get; set; }
    }
}