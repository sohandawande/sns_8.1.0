﻿
using Resources;
using System.ComponentModel.DataAnnotations;
namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model for Reason for Return order
    /// </summary>
    public class ReasonForReturnViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for ReasonReturnViewModel
        /// </summary>
        public ReasonForReturnViewModel()
        {
        }

        [MaxLength(250)]
        public string  Reason { get; set; }

        public int ReasonForReturnId { get; set; }

        public bool IsEnabled { get; set; }

    }
}