﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class AdminOrderViewModel:BaseViewModel
    {
        public int OrderID { get; set; }
        public int PortalId { get; set; }
        public int AccountID { get; set; }
        public int OrderStateID { get; set; }
        public int ShippingID { get; set; }
        public int PaymentTypeId { get; set; }
        public int PaymentStatusID { get; set; }
        public string OrderStatus { get; set; }
        public string ShippingTypeName { get; set; }
        public string PaymentTypeName { get; set; }
        public string PaymentStatusName { get; set; }
        public string BillingFirstName { get; set; }
        public string BillingLastName { get; set; }
        public string BillingCompanyName { get; set; }
        public string Total { get; set; }
        public string OrderDate { get; set; }
        public string ShipDate { get; set; }
        public string TrackingNumber { get; set; }
        public int PaymentSettingId { get; set; }
        public string FullName { get; set; }
        public string StoreName { get; set; }
        public string CardTransactionId { get; set; }
    }
}