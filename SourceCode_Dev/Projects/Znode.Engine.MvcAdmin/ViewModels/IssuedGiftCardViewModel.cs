﻿
namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class IssuedGiftCardViewModel : BaseViewModel
    {
        public string CardNumber { get; set; }
        public decimal Amount { get; set; }
        public string ExpirationDate { get; set; }
    }
}