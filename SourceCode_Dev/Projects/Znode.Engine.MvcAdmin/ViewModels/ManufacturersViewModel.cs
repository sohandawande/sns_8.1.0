﻿using Resources;
using System.ComponentModel.DataAnnotations;
using Znode.Libraries.Helpers;
namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model For add New Manufacturer 
    /// </summary>
    public class ManufacturersViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for ManufacturerViewModel
        /// </summary>
        public ManufacturersViewModel()
        { 
            
        }

        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "ErrorRequired")]
        public string Name { get; set; }
        public string Description { get; set; }
        
        [RegularExpression("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$", ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "ValidEmailAddress")]
        public string Email { get; set; }

        public int ManufacturerId { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.TitleManufacturerIsActive)]
        public bool IsActive { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelDisplayOrder)]
        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "rfvDisplayOrder")]
        [Range(1, 999999999, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "InvalidDisplayOrder")]
        [RegularExpression("^(1|[1-9][0-9]*)$", ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "InvalidDisplayOrder")]
        public int? DisplayOrder { get; set; }
    }
}