﻿using System.Collections.Generic;
using Znode.Engine.MvcAdmin.Models;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// list View MOdel for AddOn
    /// </summary>
    public class AddOnListViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for AddOnListViewModel
        /// </summary>
        public AddOnListViewModel()
        {
            AddOns = new List<AddOnViewModel>();
        }

        public List<AddOnViewModel> AddOns { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string SkuProduct { get; set; }
        public string SelectedAddOns { get; set; }
        public ReportModel GridModel { get; set; }
        public int? PortalId { get; set; }
    }
}
    