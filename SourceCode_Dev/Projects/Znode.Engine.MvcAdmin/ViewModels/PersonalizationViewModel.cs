﻿using Resources;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Models;
using Znode.Libraries.Helpers;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model for Personalization
    /// </summary>
    public class PersonalizationViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for Personalization view model
        /// </summary>
        public PersonalizationViewModel()
        {
            GridModel = new ReportModel();
        }

        public string ProductTitle { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LableProductName)]
        public string ProductName { get; set; }
        public string SKU { get; set; }
        public string CatalogName { get; set; }
        public string AssociatedItems { get; set; }

        public int CatalogId { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelCategory)]
        public int CategoryId { get; set; }
        public int PortalId { get; set; }
        public int ProductId { get; set; }

        public ReportModel GridModel { get; set; }
        public PortalCatalogListViewModel PortalCatalog { get; set; }

        [UIHint("TwoListView")]
        public ListViewModel CrossSellProductList { get; set; }

        public List<SelectListItem> PortalList { get; set; }
        public List<CategoryModel> CategoryList { get; set; }
        public List<SelectListItem> CategorySelectList { get; set; }
        public int CrossSellType { get; set; }
    }
}