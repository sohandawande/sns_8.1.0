﻿
using Resources;
using System.ComponentModel.DataAnnotations;
namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class ThemeViewModel : BaseViewModel
    {
        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "rfvThemeName")]
        public string Name{ get; set; }
        public int ThemeId { get; set; }
    }
}