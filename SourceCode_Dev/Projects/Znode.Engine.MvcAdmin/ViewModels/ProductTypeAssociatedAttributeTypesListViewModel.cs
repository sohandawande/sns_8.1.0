﻿using System.Collections.Generic;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// List View Model For Associated attributes with Product Type.
    /// </summary>
    public class ProductTypeAssociatedAttributeTypesListViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for ProductTypeAssociatedAttributeTypesListViewModel.
        /// </summary>
        public ProductTypeAssociatedAttributeTypesListViewModel()
        {
            AssociatedAttributes = new List<ProductTypeAssociatedAttributeTypesViewModel>();
        }

        public List<ProductTypeAssociatedAttributeTypesViewModel> AssociatedAttributes { get; set; }
    }
}