﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Web.Mvc;
using Znode.Engine.MvcAdmin.Models;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// List View Model for Highlights
    /// </summary>
    public class HighlightsListViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for HighlightsListViewModel
        /// </summary>
        public HighlightsListViewModel()
        {
            Highlights = new List<HighlightViewModel>();
            HighlightTypeList = new List<SelectListItem>();
        }

        [DisplayName("Name")]
        public string Name { get; set; }

        [DisplayName("Type")]
        public int TypeId { get; set; }

        public string SelectedHighlights { get; set; }

        public List<HighlightViewModel> Highlights { get; set; }
        
        public List<SelectListItem> HighlightTypeList { get; set; }

        public int HighlightTypeId { get; set; }

        public int ProductId { get; set; }

        public ReportModel GridModel { get; set; }

        public string ProductName { get; set; }
        public string UserType { get; set; }
        public int? PortalId { get; set; }

    }
}