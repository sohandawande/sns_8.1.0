﻿using System.Collections.Generic;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class OrderStateListViewModel : BaseViewModel
    {
        public OrderStateListViewModel()
        {
            OrderStateList = new List<OrderStateViewModel>();
        }

        public List<OrderStateViewModel> OrderStateList { get; set; }
    }
}