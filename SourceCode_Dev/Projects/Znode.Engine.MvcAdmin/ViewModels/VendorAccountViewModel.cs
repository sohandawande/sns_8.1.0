﻿
using Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Znode.Engine.MvcAdmin.Models;
using Znode.Libraries.Helpers;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model for Create and Edit Vendor Account
    /// </summary>
    public class  VendorAccountViewModel : BaseViewModel
    {
        #region Default Constructor
        /// <summary>
        /// Constructor for Vendor Account View Model
        /// </summary>
        public VendorAccountViewModel()
        {
            StoreList = new List<SelectListItem>();
            VendorContact = new VendorContactViewModel();
            accountViewModel = new AccountViewModel();
            GridModel = new ReportModel();
            CountryList = new List<SelectListItem>();
        } 
        #endregion
        public Guid? UserId { get; set; }

        [MaxLength(256)]
        [LocalizedDisplayName(Name = RZnodeResources.DisplayUserName)]
        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "ErrorRequired")]
        public string UserName { get; set; }
        public string Name { get; set; }

        [MaxLength(255)]
        [LocalizedDisplayName(Name = RZnodeResources.LabelCompanyName)]
        public string CompanyName { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelWebsite)]
        public string Website { get; set; }

        public int VendorId { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.DisplayAccountId)]
        public int AccountId { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.DisplayNameBilling)]
        public string FullName { get; set; }
        public string RoleName { get; set; }
        public bool? EnableCustomerPricing { get; set; }
        public bool IsConfirmed { get; set; }
        public string LoginName { get; set; }
        
        [LocalizedDisplayName(Name = RZnodeResources.LabelPassword)]
        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "RequiredNewPassword")]
        [RegularExpression("^(?=.*[0-9])(?=.*[a-zA-Z]).{8,}$", ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "ValidPassword")]
        [MaxLength(128, ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "InValidPasswordLength")]
        public string Password { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelConfirmPassword)]
        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "RequiredNewConfirmPassword")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "ErrorPasswordMatch")]
        public string ConformPassword { get; set; }

        public string UserType { get; set; }

        [MaxLength(50)]
        [LocalizedDisplayName(Name = RZnodeResources.DisplayAccountNo)]
        public string ExternalAccountNum { get; set; }
        public int? PortalId { get; set; }
        public int? ProfileId { get; set; }
        
        [LocalizedDisplayName(Name = RZnodeResources.DisplaySelectStore)]
        public List<SelectListItem> StoreList { get; set; }

        public VendorContactViewModel VendorContact { get; set; }
        public AccountViewModel accountViewModel { get; set; }
        public ReportModel GridModel { get; set; }
        public List<SelectListItem> CountryList { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelIsEmailOptional)]
        public bool IsEmailOptional { get; set; }

        public bool CheckRole { get; set; }
    }
}