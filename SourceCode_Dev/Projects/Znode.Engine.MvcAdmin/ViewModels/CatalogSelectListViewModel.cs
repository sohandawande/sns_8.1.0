﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class CatalogSelectListViewModel : BaseViewModel
    {
        public List<SelectListItem> CatalogSelectList { get; set; }
        public int SelectedCatalogId { get; set; }
    }
}