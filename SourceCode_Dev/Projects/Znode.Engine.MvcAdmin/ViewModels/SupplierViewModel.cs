﻿using Resources;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Znode.Engine.Api.Models;
using Znode.Libraries.Helpers;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model for Supplier
    /// </summary>
    public class SupplierViewModel : BaseViewModel
    {
        #region Constructor
        /// <summary>
        /// Consructor for SupplierViewModel
        /// </summary>
        public SupplierViewModel()
        {

        }
        #endregion

        [LocalizedDisplayName(Name = RZnodeResources.LabelSupplierName)]
        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.RequiredName)]
        [MaxLength(100)]
        public string Name { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelBannerDescription)]
        public string Description { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelContactFirstName)]
        public string ContactFirstName { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelContactLastName)]
        public string ContactLastName { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelContactPhone)]
        public string ContactPhoneNumber { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelContactEmail)]
        [RegularExpression("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$", ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.revErrorEmailFormat)]
        public string ContactEmail { get; set; }

        [RegularExpression(@"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*([,]\s*\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)*", ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.revErrorEmailFormat)]
        [LocalizedDisplayName(Name = RZnodeResources.LabelNotificationEmail)]
        public string NotificationEmail { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelEmailNotificationTemplate)]
        [AllowHtml]
        public string EmailNotificationTemplate { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelSupplierCode)]
        [MaxLength(100)]
        public string ExternalSupplierNumber { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelCustom1)]
        public string Custom1 { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelCustom2)]
        public string Custom2 { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelCustom3)]
        public string Custom3 { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelCustom4)]
        public string Custom4 { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelCustom5)]
        public string Custom5 { get; set; }

        public string SupplierTypeName { get; set; }

        public int SupplierId { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.DisplayOrderText)]
        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.RequiredDisplayOrder)]
        [RegularExpression("^(1|[1-9][0-9]*)$", ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = RZnodeResources.InvalidDisplayOrder)]
        public int DisplayOrder { get; set; }
        public int? PortalId { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelNotificationMethod)]
        public int? SupplierTypeId { get; set; }

        public bool EnableEmailNotification { get; set; }
        public bool IsActive { get; set; }

        public List<SupplierTypeModel> SupplierTypeList { get; set; }

        public string SupplierTypeClassName { get; set; }
    }
}