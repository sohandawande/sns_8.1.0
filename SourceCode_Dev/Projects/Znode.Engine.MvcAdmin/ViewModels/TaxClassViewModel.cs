﻿using Resources;
using System.ComponentModel.DataAnnotations;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Models;
using Znode.Libraries.Helpers;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model for Tax Class
    /// </summary>
    public class TaxClassViewModel:BaseViewModel
    {
        #region Constructor for TaxClassViewModel
        /// <summary>
        /// Constructor for Tax Class View Model
        /// </summary>
        public TaxClassViewModel()
        {
            ReportModel = new ReportModel();
            taxRuleList = new TaxRuleListViewModel();
        } 
        #endregion                

        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = RZnodeResources.rfvDisplayOrder)]
        [Range(1, 999999999, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.InvalidDisplayOrder)]
        [LocalizedDisplayName(Name = RZnodeResources.LabelPaymentDisplayOrder)]
        public int? DisplayOrder { get; set; }

        public string ExternalId { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelEnable)]
        public bool IsActive { get; set; }

        [MaxLength(100)]
        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName =RZnodeResources.rfvTaxClassName)]
        [LocalizedDisplayName(Name = RZnodeResources.LabelTaxClassName)]
        public string Name { get; set; }

        public int? PortalId { get; set; }
        public int TaxClassId { get; set; }
        public string StoreName { get; set; }
        public string CustomErrorMessage { get; set; }
        public PortalModel portalModel { get; set; }

        public TaxRuleListViewModel taxRuleList { get; set; }

        public ReportModel ReportModel { get; set; }
    }
}