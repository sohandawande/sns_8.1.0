﻿using Resources;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Libraries.Helpers;
using Znode.Libraries.Helpers.Attributes;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// view model for Create Highlight 
    /// </summary>
    public class HighlightViewModel : BaseViewModel
    {
        #region Constructor
        /// <summary>
        /// Constructor for HighlightViewModel
        /// </summary>
        public HighlightViewModel()
        {

        }
        #endregion

        [LocalizedDisplayName(Name = RZnodeResources.LabelImageAltText)]
        public string ImageAltTag { get; set; }
        public string ImageFile { get; set; }
        public string ImageLargePath { get; set; }
        public string ImageMediumPath { get; set; }
        public string ImageSmallPath { get; set; }
        public string ImageSmallThumbnailPath { get; set; }
        public string ImageThumbnailPath { get; set; }

        [RegularExpression("^[a-zA-Z][a-zA-Z0-9]*", ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.RegularExpressionFileName)]
        public string NewImageFileName { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.RequiredName)]
        [LocalizedDisplayName(Name = RZnodeResources.TitleManufacturerName)]
        public string Name { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelDisplayText)]
        [UIHint("RichTextBox")]
        [AllowHtml]
        public string Description { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelHyperlink)]
        [RegularExpression("^(http\\:\\/\\/[a-zA-Z0-9_\\-/]+(?:\\.[a-zA-Z0-9_\\-/]+)*)$", ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.ValidationUrl)]
        public string Hyperlink { get; set; }

        public int HighlightId { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelHighlightType)]
        public int? TypeId { get; set; }
        public int LocaleId { get; set; }
        public int ProductHighlightID { get; set; }
        public int ProductId { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.RequiredDisplayOrder)]
        [RegularExpression("^(1|[1-9][0-9]*)$", ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = RZnodeResources.InvalidDisplayOrder)]
        [LocalizedDisplayName(Name = RZnodeResources.LabelPaymentDisplayOrder)]
        public int? DisplayOrder { get; set; }
        public int? HighlightTypeId { get; set; }
        public string HighlightTypeName { get; set; }

        public bool ShowPopup { get; set; }
        public bool NoImage { get; set; }
        public bool DispalyText { get; set; }
        public bool DisplayHyperlink { get; set; }
        public bool IsEditMode { get; set; }

        public bool IsActive { get; set; }
        public bool IsAssociatedProduct { get; set; }
        public bool? HyperlinkNewWindow { get; set; }
        public int IsNewImageFileName { get; set; }

        public HighlightTypeViewModel HighlightType { get; set; }

        public List<HighlightTypeModel> HighlightTypeList { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelSelectProductImage)]
        [FileTypeValidation(MvcAdminConstants.ImageFileTypes, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.ImageFileTypeErrorMessage)]
        [FileMaxSizeValidation(MvcAdminConstants.ImageMaxFileSize, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.FileSizeExceededErrorMessage)]
        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = RZnodeResources.ImageFileTypeErrorMessage)]
        [UIHint("FileUploader")]
        public HttpPostedFileBase HighlightImage { get; set; }
    }
}