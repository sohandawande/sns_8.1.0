﻿using System.Collections.Generic;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// List View Model for Tax Class
    /// </summary>
    public class TaxClassListViewModel :BaseViewModel
    {
        #region Constructor for Tax Class List View Model
        /// <summary>
        /// Constructor for TaxClassListViewModel
        /// </summary>
        public TaxClassListViewModel()
        {
            TaxClasses = new List<TaxClassViewModel>();
        } 
        #endregion

        public List<TaxClassViewModel> TaxClasses { get; set; }
    }
}