﻿using System.Collections.Generic;
using Znode.Engine.MvcAdmin.Models;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// List View Model for Products
    /// </summary>
    public class ProductListViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for Products
        /// </summary>
        public ProductListViewModel()
        {
            Products = new List<ProductViewModel>();
            GridModel = new ReportModel();
        }

        public List<ProductViewModel> Products { get; set; }
        public ReportModel GridModel { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }

        public string Name { get; set; }
        public string ProductNumber { get; set; }
        public string SKU { get; set; }
        public int CatalogId { get; set; }
        public int ManufacturerId { get; set; }
        public int ProductTypeId { get; set; }
        public int CategoryId { get; set; }
        public FilterColumnListModel FilterColumn { get; set; }

        public string CategoryName { get; set; }
        public string ManufacturerName { get; set; } 
    }
}