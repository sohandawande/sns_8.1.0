﻿using System.Collections.Generic;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// List view model for Address
    /// </summary>
    public class AddressListViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for AddressListViewModel
        /// </summary>
        public AddressListViewModel()
        {
            AccountAddress = new List<AddressViewModel>();
        }
        public List<AddressViewModel> AccountAddress { get; set; }
    }
}