﻿
using System.Collections.Generic;
using System.ComponentModel;
using Resources;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Znode.Libraries.Helpers;
namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// ViewModel for Manage Message
    /// </summary>
    public class ManageMessageViewModel : BaseViewModel
    {
        #region Constructor
        /// <summary>
        /// Constructor for Manage Message
        /// </summary>
        public ManageMessageViewModel()
        {

        }
        #endregion

        #region Public Properties

        public int MessageConfigId { get; set; }
        public int PortalId { get; set; }
        public int MessageTypeId { get; set; }
        public int MessageId { get; set; }
        public int? LocaleId { get; set; }

        [MaxLength(100)]
        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.RequiredMessageKey)]
        [RegularExpression("^[0-9a-zA-Z]+$", ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = RZnodeResources.ValidMessageKey)]
        [LocalizedDisplayName(Name = RZnodeResources.LabelMessageKey)]
        public string MessageKey { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelDescription)]
        public string Description { get; set; }

        [UIHint("RichTextBox")]
        [AllowHtml]
        public string Value { get; set; }
        public string PortalName { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.DisplaySelectStore)]
        public PortalViewModel StoreName { get; set; }
        public List<PortalViewModel> Portal { get; set; }

        public string CustomErrorMessage { get; set; }

        #endregion
    }
}