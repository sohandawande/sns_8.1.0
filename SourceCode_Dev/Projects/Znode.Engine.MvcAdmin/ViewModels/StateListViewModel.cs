﻿using System.Collections.Generic;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// List View Model for State
    /// </summary>
    public class StateListViewModel : BaseViewModel
    {
        #region Constructor for State List View Model
        /// <summary>
        /// Constructor for State List View Model
        /// </summary>
        public StateListViewModel()
        {
            State = new List<StateViewModel>();
        } 
        #endregion

        public List<StateViewModel> State { get; set; }               
    }
}   
   