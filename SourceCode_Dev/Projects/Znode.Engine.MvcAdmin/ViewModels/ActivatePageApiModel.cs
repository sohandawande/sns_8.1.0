﻿using Resources;
using System;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.IO;
using System.Web;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Libraries.Helpers;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class ActivatePageApiModel : BaseViewModel
    {
        #region constructor
        public ActivatePageApiModel()
        {
            Eula = GetEULA();
        }
        #endregion

        #region properties

        public LicenseType LicenseType { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelFullName)]
        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.RequiredFullName)] 
        public string FullName { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelSerialNumber)]
        public string SerialNumber { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelEmailAddress)]
        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.RequiredEmailAddress)]
        [RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$", ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.RegularExpressionEmailAddress)]
        public string Email { get; set; }


        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.RequiredAcceptLicense)] 
        public bool CheckEula { get; set; }

        public string Eula { get; set; }

        public string Message { get; set; }

        public string ServerUrl { get; set; }

        #endregion

        #region Get Software License Agreement
        /// <summary>
        /// This method reads the Software License Agreement note from the file eula.txt
        /// And return the file text in string format
        /// </summary>
        /// <returns>Returns the Software License Agreement note</returns>
        public static string GetEULA()
        {
            string path = HttpContext.Current.Server.MapPath(Convert.ToString(ConfigurationManager.AppSettings["EULAFilePathAdmin"]));
            return File.ReadAllText(path);
        }
        #endregion
    }
}