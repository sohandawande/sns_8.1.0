﻿
namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// Category level setting view model
    /// </summary>
    public class CategoryLevelSettingViewModel:BaseViewModel
    {
        /// <summary>
        /// Constructor for ProductLevelSettingViewModel
        /// </summary>
        public CategoryLevelSettingViewModel()
        {

        }

        public string Name { get; set; }
        public string SKU { get; set; }
        public string ProductNumber { get; set; }
        public string CategoryName {get;set;}
        
        public int ProductId { get; set; }
        public int CatalogId { get; set; }
        public int categoryId { get; set; }
        public int ProductCategoryId { get; set; }
        public int ProductTypeId { get; set; }
        public int ManufacturerId { get; set; }

        public string Boost { get; set; }
    }
}
