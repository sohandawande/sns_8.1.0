﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class RMAConfigurationListViewModel
    {
        public List<RMAConfigurationViewModel> RMAConfigurations { get; set; }

        /// <summary>
        /// Constructor for RMAConfigurationListViewModel
        /// </summary>
        public RMAConfigurationListViewModel()
        {
            RMAConfigurations = new List<RMAConfigurationViewModel>();
        } 
    }
}