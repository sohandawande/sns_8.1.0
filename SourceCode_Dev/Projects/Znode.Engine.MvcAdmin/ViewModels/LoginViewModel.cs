﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class LoginViewModel : BaseViewModel
    {
        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "RequiredUserName")]
        public string Username { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "RequiredPassword")]
        public string Password { get; set; }
               
        public bool RememberMe { get; set; }

        public string PasswordQuestion { get; set; }

        public string PasswordAnswer { get; set; }

        public bool IsResetPassword { get; set; }

        public bool IsResetAdmin { get; set; }

        public string PasswordResetToken { get; set; }

        public int? ProfileId { get; set; }

        public bool IsResetAdminPassword { get; set; }
    }
}