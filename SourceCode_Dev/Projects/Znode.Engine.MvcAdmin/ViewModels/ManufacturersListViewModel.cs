﻿using System.Collections.Generic;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// List View Model for Vendor Account
    /// </summary>
    public class ManufacturersListViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for Vendor Account
        /// </summary>
        public ManufacturersListViewModel()
        {
            Manufacturers = new List<ManufacturersViewModel>();
        }

        public List<ManufacturersViewModel> Manufacturers { get; set; }
    }
}