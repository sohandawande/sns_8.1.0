﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class CategoryListViewModel:BaseViewModel
    {
        public CategoryListViewModel()
        {
            Categories = new List<CategoryViewModel>();
        }

        public List<CategoryViewModel> Categories { get; set; }
    }
}