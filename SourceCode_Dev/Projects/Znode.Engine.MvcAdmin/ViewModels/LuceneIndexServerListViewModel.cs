﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class LuceneIndexServerListViewModel:BaseViewModel
    {
        public List<LuceneIndexServerViewModel> IndexServerStatusList { get; set; }
        public LuceneIndexServerListViewModel()
        {
            IndexServerStatusList = new List<LuceneIndexServerViewModel>();
        }
    }
}