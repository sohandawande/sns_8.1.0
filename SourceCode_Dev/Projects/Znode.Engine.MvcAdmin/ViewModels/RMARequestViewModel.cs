﻿using System;
using System.Collections.Generic;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class RMARequestViewModel : BaseViewModel
    {
        public RMARequestViewModel()
        {
            RMARequestItems = new List<RMARequestItemViewModel>();
        }

        public int RMARequestID { get; set; }
        public int OrderId { get; set; }
        public string StoreName { get; set; }
        public int PortalId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CustomerName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string RequestDate { get; set; }
        public string RequestStatus { get; set; }
        public int? RequestStatusId { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string RequestNumber { get; set; }
        public string TaxCost { get; set; }
        public string Discount { get; set; }
        public string SubTotal { get; set; }
        public string Total { get; set; }
        public decimal? TaxCostAmount { get; set; }
        public decimal? DiscountAmount { get; set; }
        public decimal? SubTotalAmount { get; set; }
        public decimal? TotalAmount { get; set; }
        public string Comments { get; set; }
        public string GiftCardNumber { get; set; }
        public string Flag { get; set; }
        public List<RMARequestItemViewModel> RMARequestItems { get; set; }
    }
}