﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.MvcAdmin.Models;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// List view model for Product Sku
    /// </summary>
    public class ProductSkuListViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for Product Sku
        /// </summary>
        public ProductSkuListViewModel()
        {
            GridModel = new ReportModel();
            SkuList = new SkuListViewModel();
        }
        public ReportModel GridModel { get; set; }
        public SkuListViewModel SkuList { get; set; }
        public int ProductId { get; set; }       
        public int AttributeCount { get; set; }
        public bool ParentProduct { get; set; }
        public bool IsGiftCard { get; set; }
        public int? PortalId { get; set; }       
    }
}