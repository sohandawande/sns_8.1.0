﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class AttributeTypeValueListViewModel : BaseViewModel
    {
         /// <summary>
        /// Constructor for Vendor Account
        /// </summary>
        public AttributeTypeValueListViewModel()
        {
            AttributeType = new List<AttributeTypeValueViewModel>();
        }

        public List<AttributeTypeValueViewModel> AttributeType { get; set; }
    }
}