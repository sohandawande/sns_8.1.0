﻿
namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model for Tax Rule Type
    /// </summary>
    public class TaxRuleTypeViewModels : BaseViewModel
    {
        #region Constructor for TaxRuleTypeViewModels
        /// <summary>
        /// Constructor for CountryViewModel
        /// </summary>
        public TaxRuleTypeViewModels()
        {

        } 
        #endregion

        public string ClassName { get; set; }
		public string Description { get; set; }
		public bool IsActive { get; set; }
		public string Name { get; set; }
		public int? PortalId { get; set; }
		public int TaxRuleTypeId { get; set; }    
    }
}
 