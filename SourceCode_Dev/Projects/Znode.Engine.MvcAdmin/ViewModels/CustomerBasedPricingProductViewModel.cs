﻿
namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class CustomerBasedPricingProductViewModel : BaseViewModel
    {
        //Customer Pricing Product For Customer
        public int ProductId { get; set; }
        public int ExternalId { get; set; }
        public string SKU { get; set; }
        public string Name { get; set; }
        
        public int AccountId { get; set; }

        public string BasePrice { get; set; }
        public string Discount { get; set; }
        public string NegotiatedPrice { get; set; }
    }
}