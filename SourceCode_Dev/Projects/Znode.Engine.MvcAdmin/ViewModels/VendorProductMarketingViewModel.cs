﻿using System.ComponentModel.DataAnnotations;
using Resources;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class VendorProductMarketingViewModel:BaseViewModel
    {
        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "RequiredDisplayOrder")]
        [RegularExpression("^(1|[1-9][0-9]*)$", ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "InvalidDisplayOrder")]
        public int? DisplayOrder { get; set; }

        public bool HomepageSpecial { get; set; }
        public bool? NewProductInd { get; set; }
        public bool FeaturedInd { get; set; }
        public string SeoDescription { get; set; }
        public string SeoKeywords { get; set; }
        public string SeoTitle { get; set; }

        [RegularExpression("^[A-Za-z0-9-_]+$", ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "ValidSEOUrl")]
        public string SeoPageUrl { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }

    }
}