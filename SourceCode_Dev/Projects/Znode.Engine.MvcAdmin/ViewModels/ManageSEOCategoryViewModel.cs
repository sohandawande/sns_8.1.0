﻿using Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Znode.Libraries.Helpers;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model For ManageSEOCategoryViewModel
    /// </summary>
    public class ManageSEOCategoryViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for ManageSEOProductViewModel
        /// </summary>
        public ManageSEOCategoryViewModel()
        {

        }

        public string CategoryName { get; set; }

        public int CategoryId { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelCategoryTitle)]
        public string CategoryTitle { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelSEODescription)]
        public string SeoDescription { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelSEOKeywords)]
        public string SeoKeywords { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelSEOTitle)]
        public string SeoTitle { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelSeoPageName)]
        [RegularExpression("([A-Za-z0-9-_]+)", ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "revSeoUrl")]
        public string SeoUrl { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelShortDescription)]
        public string ShortDescription { get; set; }

        [UIHint("RichTextBox")]
        [AllowHtml]
        [LocalizedDisplayName(Name = RZnodeResources.LabelLongDescription)]
        public string Description { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.Label301Url)]
        public bool RedirectUrlInd { get; set; }
    }
}