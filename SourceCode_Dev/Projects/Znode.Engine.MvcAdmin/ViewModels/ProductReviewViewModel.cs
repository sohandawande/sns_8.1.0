﻿using System;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model for edit and create Product Review 
    /// </summary>
    public class ProductReviewViewModel : BaseViewModel
    {
        /// <summary>
        /// Default Constructor for Product Review View Model
        /// </summary>
        public ProductReviewViewModel()
        {

        }

        public string Status { get; set; }
        public string HeadLine { get; set; }
        public string UserLocation { get; set; }
        public string Duration { get; set; }
        public string Comments { get; set; }

        public string CreateUser { get; set; }
        public string ProductName { get; set; }

        public int Rating { get; set; }
        public int ReviewId { get; set; }

        public DateTime? CreateDate { get; set; }
    }
}