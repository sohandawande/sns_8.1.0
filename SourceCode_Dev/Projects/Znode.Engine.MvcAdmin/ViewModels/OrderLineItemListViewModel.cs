﻿using System.Collections.Generic;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class OrderLineItemListViewModel:BaseViewModel
    {
        public OrderLineItemListViewModel()
        {
            OrderLineItems = new List<OrderLineItemViewModel>();
        }

        public List<OrderLineItemViewModel> OrderLineItems { get; set; }
    }
}