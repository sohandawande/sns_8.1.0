﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.MvcAdmin.Models;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class FacetListViewModel : BaseViewModel
    {
        public FacetListViewModel()
        {
            Facets = new List<FacetViewModel>();
            GridModel = new ReportModel();
        }
        public List<FacetViewModel> Facets { get; set; }
        public int? FacetGroupId { get; set; }
        public ReportModel GridModel { get; set; }
    }
}