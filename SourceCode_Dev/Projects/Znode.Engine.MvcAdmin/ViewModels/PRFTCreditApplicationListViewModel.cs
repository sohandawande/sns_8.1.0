﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class PRFTCreditApplicationListViewModel:BaseViewModel
    {
        /// <summary>
        ///  Constructor for CaseRequestsListViewModel 
        /// </summary>
        public PRFTCreditApplicationListViewModel()
        {
            CreditApplicationsList = new List<PRFTCreditApplicationForListViewModel>();
        }

        public List<PRFTCreditApplicationForListViewModel> CreditApplicationsList { get; set; }
    }
}