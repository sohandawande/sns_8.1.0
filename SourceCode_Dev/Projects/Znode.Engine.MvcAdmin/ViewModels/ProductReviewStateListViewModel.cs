﻿using System.Collections.Generic;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class ProductReviewStateListViewModel : BaseViewModel
    {
         /// <summary>
        /// Constructor for Product Review State
        /// </summary>
        public ProductReviewStateListViewModel()
        {
            ProductReviewStates = new List<ProductReviewStateViewModel>();
        }
        public List<ProductReviewStateViewModel> ProductReviewStates { get; set; }       
    }
}