﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class PortalCountriesViewModel : BaseViewModel
    {
        public string CountryCode { get; set; }
        public bool IsBillingActive { get; set; }
        public bool IsShippingActive { get; set; }
        public int PortalCountryId { get; set; }
        public int PortalId { get; set; }
        public string CountryName { get; set; }
    }
}