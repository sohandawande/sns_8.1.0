﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class ProductFacetsViewModel : BaseViewModel
    {
       
        public ProductFacetsViewModel()
        {
        }
        public int ProductId { get; set; }
        public int SKUId { get; set; }

        public int FacetGroupId { get; set; }
        public List<SelectListItem> FacetGroups { get; set; }
        public List<Facets> AvailableFacets { get; set; }
        public List<Facets> SelectedFacets { get; set; }
        public PostedFacets PostedFacets { get; set; }

        public string FacetGroupName { get; set; }
        public string FacetNames { get; set; }
    }

    public class Facets:BaseViewModel
    {
        //Integer value of a checkbox
        public int Id { get; set; }

        //String name of a checkbox
        public string Name { get; set; }

        //Boolean value to select a checkbox
        //on the list
        public bool IsSelected { get; set; }

        //Object of html tags to be applied
        //to checkbox, e.g.:'new{tagName = "tagValue"}'
        public object Tags { get; set; }

    }

    public class PostedFacets
    {
        //this array will be used to POST values from the form to the controller
        public string[] FacetIds { get; set; }
    }
}