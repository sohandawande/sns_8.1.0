﻿using System.Collections.Generic;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// To Download Customer list view model
    /// </summary>
    public class CustomerDownloadListViewModel : BaseViewModel
    {
        #region public variables
        public List<CustomerDownloadViewModel> customerList { get; set; } 
        #endregion

        #region Constructor
        public CustomerDownloadListViewModel()
        {
            customerList = new List<CustomerDownloadViewModel>();
        } 
        #endregion
    }
}