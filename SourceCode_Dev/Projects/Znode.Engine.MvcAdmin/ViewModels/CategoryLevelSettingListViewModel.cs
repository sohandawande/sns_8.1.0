﻿using System.Collections.Generic;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class CategoryLevelSettingListViewModel: BaseViewModel
    {
        public CategoryLevelSettingListViewModel()
        {
            Category = new List<CategoryLevelSettingViewModel>();
        }
        public List<CategoryLevelSettingViewModel> Category { get; set; }
    }
}
