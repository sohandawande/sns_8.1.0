﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public partial class TaxRulesViewModel:BaseViewModel
    {
        public string AccountID { get; set; }
        
        public string License { get; set; }

        public string Company { get; set; }

        public string WebserviceURL{get; set; }
        public string ClientCode { get; set; }
        public string ClientName { get; set; }

        public string Custom1 { get; set; }// Security Credentials of Avalara Tax

        public int? TaxRuleTypeId { get; set; }

    }
}