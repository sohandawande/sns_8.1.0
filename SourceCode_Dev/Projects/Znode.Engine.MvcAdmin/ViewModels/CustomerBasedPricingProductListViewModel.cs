﻿using System.Collections.Generic;
using Znode.Engine.MvcAdmin.Models;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class CustomerBasedPricingProductListViewModel : BaseViewModel
    {
        public CustomerBasedPricingProductListViewModel()
        {
            CustomerBasedPricingProduct = new List<CustomerBasedPricingProductViewModel>();
            GridModel = new ReportModel();
        }
        public List<CustomerBasedPricingProductViewModel> CustomerBasedPricingProduct { get; set; }
        public FilterColumnListModel FilterColumn { get; set; }
        public ReportModel GridModel { get; set; }
        public int AccountId { get; set; }
    }
}