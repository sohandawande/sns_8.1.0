﻿
namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View model for Order State
    /// </summary>
    public class OrderStateViewModel : BaseViewModel
    {
        #region Constructor

        /// <summary>
        /// Consructor for OrderStateViewModel
        /// </summary>
        public OrderStateViewModel()
        {

        }  

        #endregion
       
        public string OrderStateName { get; set; }
        public string Description { get; set; }
        public int OrderStateId { get; set; }
    }
}