﻿using Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Libraries.Helpers;
using Znode.Libraries.Helpers.Attributes;
namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View model for create Service
    /// </summary>
    public class CaseRequestViewModel : BaseViewModel
    {
        /// <summary>
        /// Consructor for CreateServiceViewModel
        /// </summary>
        public CaseRequestViewModel()
        {

        }

        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "RequiredServiceFirstName")]
        [LocalizedDisplayName(Name = RZnodeResources.LabelServiceRequestFirstName)]
        public string FirstName { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "RequiredServiceLastName")]
        [LocalizedDisplayName(Name = RZnodeResources.LabelServiceRequestLastName)]
        public string LastName { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelServiceRequestCompanyName)]
        public string CompanyName { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelEmailID)]
        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "RequiredServiceEmailId")]
        [RegularExpression("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$", ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "ValidEmailAddress")]
        public string EmailId { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelServiceRequestPhoneNumber)]
        public string PhoneNumber { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelServiceRequestMessage)]
        public string Message { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelServiceRequestOrigin)]
        public string CaseOrigin { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "RequiredServiceTitle")]
        [LocalizedDisplayName(Name = RZnodeResources.LabelServiceRequestTitle)]
        public string Title { get; set; }

        [UIHint("RichTextBox")]
        [AllowHtml]
        [LocalizedDisplayName(Name = RZnodeResources.LabelMessage)]
        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "RequiredServiceDescription")]
        public string Description { get; set; }

        public string CreateUser { get; set; }

        public string StoreName { get; set; }
        public string CaseStatusName { get; set; }
        public string CasePriorityName { get; set; }


        public int CaseRequestId { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelPortalId)]
        public int PortalId { get; set; }
        public int? AccountId { get; set; }
        public int? OwnerAccountId { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelServiceRequestStatus)]
        public int CaseStatusId { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelServiceRequestPriority)]
        public int CasePriorityId { get; set; }
        public int CaseTypeId { get; set; }

        public string CaseOldStatusName { get; set; }
        public string CaseNewStatusName { get; set; }

        public string FormatedCreatedDate { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelServiceRequestCreateDate)]
        public DateTime? CreatedDate { get; set; }

        public NotesViewModel CaseNote { get; set; }

        public NotesListViewModel CaseNoteList { get; set; }

        public List<SelectListItem> PortalList { get; set; }
        public List<SelectListItem> CaseStatusList { get; set; }
        public List<SelectListItem> CasePriorityList { get; set; }

        //for Reply to Customer
        [LocalizedDisplayName(Name = RZnodeResources.LabelServiceRequestEmailSubject)]
        public string EmailSubject { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelServiceRequestEmailMessage)]
        [UIHint("RichTextBox")]
        [AllowHtml]
        public string EmailMessage { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelServiceRequestAttachments)]
        [FileTypeValidation(MvcAdminConstants.ImageFileTypes, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "ImageFileTypeErrorMessage")]
        [FileMaxSizeValidation(MvcAdminConstants.ImageMaxFileSize, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "FileSizeExceededErrorMessage")]
        [UIHint("FileUploader")]
        public HttpPostedFileBase Attachments { get; set; }
    }
}