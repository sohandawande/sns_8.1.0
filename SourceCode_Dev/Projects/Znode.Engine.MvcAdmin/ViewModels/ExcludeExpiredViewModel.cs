﻿
namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class ExcludeExpiredViewModel : BaseViewModel
    {
        public string ExpirationDate { get; set; }
        public string ExcludeText { get; set; }
    }
}