﻿using System.Collections.Generic;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View model for Shipping Type List
    /// </summary>
    public class ShippingTypeListViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for Shipping Type list view model
        /// </summary>
        public ShippingTypeListViewModel()
        {
            ShippingType = new List<ShippingTypeViewModel>();
        }
        public List<ShippingTypeViewModel> ShippingType { get; set; }
    }
}