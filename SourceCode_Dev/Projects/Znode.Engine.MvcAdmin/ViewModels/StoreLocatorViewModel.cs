﻿using Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Libraries.Helpers;
using Znode.Libraries.Helpers.Attributes;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class StoreLocatorViewModel : BaseViewModel
    {
        public int StoreID { get; set; }
        public int PortalID { get; set; }

        public int? AccountID { get; set; }


        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "rfvDisplayOrder")]
        [Range(1, 999999999, ErrorMessage = "Must be a whole number")]
        [RegularExpression("^(1|[1-9][0-9]*)$", ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "InvalidDisplayOrder")]
        [LocalizedDisplayName(Name = RZnodeResources.DisplayOrderText)]
        public int? DisplayOrder { get; set; }

        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "ErrorRequired")]
        [LocalizedDisplayName(Name = RZnodeResources.LabelStoreName)]        
        public string Name { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.PlaceHolderAddress1)]
        public string Address1 { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.PlaceHolderAddress2)]
        public string Address2 { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.PlaceHolderAddress3)]
        public string Address3 { get; set; }

        public string City { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.PlaceHolderState)]
        public string State { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelZipPostalCode)]
        public string Zip { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.PlaceHolderPhoneNo)]
        public string Phone { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelFaxNumber)]
        public string Fax { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.PlaceHolderContactName)]
        public string ContactName { get; set; }
        public string ImageFile { get; set; }
        public string Custom1 { get; set; }
        public string Custom2 { get; set; }
        public string Custom3 { get; set; }

        public string ImageLargePath { get; set; }
        public string ImageMediumPath { get; set; }
        public string ImageSmallPath { get; set; }
        public string ImageSmallThumbnailPath { get; set; }
        public string ImageThumbnailPath { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelDisplayStore)]
        public bool ActiveInd { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelSelectImage)]
        [FileTypeValidation(MvcAdminConstants.ImageFileTypes, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "ImageFileTypeErrorMessage")]
        [FileMaxSizeValidation(MvcAdminConstants.ImageMaxFileSize, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "FileSizeExceededErrorMessage")]
        [UIHint("FileUploader")]
        public HttpPostedFileBase StoreImage { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelSelectStore)]
        public List<SelectListItem> StoreList { get; set; }

        public List<PortalViewModel> PortalList { get; set; }
        public bool NoImage { get; set; }
        public bool IsEditMode { get; set; }
    }
}