﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class OrderCreateViewModel : BaseViewModel
    {
        public PortalSelectListViewModel Portals { get; set; }
        public CatalogSelectListViewModel Catalogs { get; set; }
        public int? PortalId { get; set; }
        public int? CatalogId { get; set; }
    }
}