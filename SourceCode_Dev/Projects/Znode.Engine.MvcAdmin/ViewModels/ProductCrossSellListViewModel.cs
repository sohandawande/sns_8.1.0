﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class ProductCrossSellListViewModel : BaseViewModel
    {
        public List<ProductCrossSellViewModel> CrossSellProducts { get; set; }

        /// <summary>
        /// Constructor for ProductCrossSellListViewModel
        /// </summary>
        public ProductCrossSellListViewModel()
        {
            CrossSellProducts = new List<ProductCrossSellViewModel>();
        }
    }
}