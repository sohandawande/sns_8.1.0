﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class ProductAttributesViewModel:BaseViewModel
    {
        public int ProductId { get; set; }
        public Collection<AttributeTypesViewModel> Attributes { get; set; }

        public ProductAttributesViewModel()
        {
            Attributes = new Collection<AttributeTypesViewModel>();
        }
    }
}