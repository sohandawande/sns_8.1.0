﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class PRFTInventoryViewModel : BaseViewModel
    {
        public int ProductId { get; set; }
        public string Name { get; set; }
        public string ProductNum { get; set; }
        public int Milwaukee { get; set; }
        public int Bloomington { get; set; }
        public int Nebraska { get; set; }
        public int DesMoines { get; set; }
        public int? PortalID { get; set; }

        public PRFTInventoryViewModel()
        {
            PortalID = 1;
            Milwaukee = 0;
            Bloomington = 0;
            Nebraska = 0;
            DesMoines = 0;
        }
    }
}