﻿using Resources;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Models;
using Znode.Libraries.Helpers;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class CatalogViewModel : BaseViewModel
    {
        int localeId = 0;
        public int CatalogId { get; set; }
        public string ExternalId { get; set; }
        public bool IsActive { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "rfvCatalogName")]
        [LocalizedDisplayName(Name = RZnodeResources.CatalogName)]
        [MaxLength(100)]
        public string Name { get; set; }
        public int? PortalId { get; set; }
        public int LocaleId
        {
            get
            {
                return localeId;
            }
            set
            {
                localeId = MvcAdminConstants.LocaleId;
            }
        }

        public bool PreserveCategories { get; set; }

        public List<CatalogAssociatedCategoriesViewModel> Categories { get; set; }
        public CatalogAssociatedCategoriesListViewModel CatalogAssociatedCategoryList { get; set; }

        public int CategoryProfileId { get; set; }
        public ReportModel ReportModel { get; set; }
        public bool IsCreated { get; set; }
        public bool IsDefault { get; set; }
    }
}