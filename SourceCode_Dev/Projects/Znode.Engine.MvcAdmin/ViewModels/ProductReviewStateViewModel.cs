﻿
namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class ProductReviewStateViewModel : BaseViewModel
    {
        public int ReviewStateID { get; set; }
        public string ReviewStateName { get; set; }
        public string Description { get; set; }
    }
}