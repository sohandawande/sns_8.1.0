﻿using System.Collections.Generic;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class AccountPaymentListViewModel : BaseViewModel
    {
        public List<AccountPaymentViewModel> AccountPaymentList { get; set; }
        /// <summary>
        /// Constructor for AccountPaymentListViewModel
        /// </summary>
        public AccountPaymentListViewModel()
        {
            AccountPaymentList = new List<AccountPaymentViewModel>();
        }

        
    }
}