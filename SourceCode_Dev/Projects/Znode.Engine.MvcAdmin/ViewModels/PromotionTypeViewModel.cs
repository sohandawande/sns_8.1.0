﻿
namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model for Promotions Types
    /// </summary>
    public class PromotionTypeViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for PromotionTypeViewModel
        /// </summary>
        public PromotionTypeViewModel()
        {
 
        }

        public string Description { get; set; }
        public string Name { get; set; }
        public string ClassName { get; set; }
        public string ClassType { get; set; }
        public string UserType { get; set; }

        public int PromotionTypeId { get; set; }

        public bool IsActive { get; set; }
    }
}