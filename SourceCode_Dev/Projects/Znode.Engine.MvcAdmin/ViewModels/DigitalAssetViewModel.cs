﻿using Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Znode.Libraries.Helpers;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class DigitalAssetViewModel : BaseViewModel
    {
        public int DigitalAssetID { get; set; }
        public int ProductID { get; set; }
        public int? OrderLineItemID { get; set; }
        public bool Assigned { get; set; }

        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "RfvDigitalAssets")] 
        [LocalizedDisplayName(Name = RZnodeResources.LabelDigitalAsset)]
        public string DigitalAsset { get; set; }
        public string ProductName { get; set; }
        public string UserType { get; set; }
    }
}