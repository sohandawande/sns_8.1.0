﻿using Resources;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model for create and edit Facet
    /// </summary>
    public class FacetViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for FacetViewModel
        /// </summary>
        public FacetViewModel()
        {

        }
        public int FacetID { get; set; }

        public int? FacetGroupID { get; set; }

        [AllowHtml]
        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "RequiredFacetName")]
        [RegularExpression("([A-Za-z0-9-_ < >]+)", ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "InvalidFacetName")]
        public string FacetName { get; set; }

        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "RequiredDisplayOrder")]
        [RegularExpression("^(1|[1-9][0-9]*)$", ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "InvalidDisplayOrder")]
        public int? FacetDisplayOrder { get; set; }

        public string IconPath { get; set; }

        public bool IsEditMode { get; set; }
    }
}