﻿using Resources;
using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Znode.Libraries.Helpers;
namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class NotesViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for NotesViewModel
        /// </summary>
        public NotesViewModel()
        {

        }

        public int NoteId { get; set; }
        public int? CaseId { get; set; }
        public int? AccountId { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "RequiredServiceTitle")]
        [LocalizedDisplayName(Name = RZnodeResources.LabelServiceRequestTitle)]
        public string NoteTitle { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelServiceRequestNote)]
        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "RequiredServiceNote")]
        [UIHint("RichTextBox")]
        [AllowHtml]
        public string NoteBody { get; set; }

        public DateTime CreateDate { get; set; }
        public string CreateUser { get; set; }
    }
}