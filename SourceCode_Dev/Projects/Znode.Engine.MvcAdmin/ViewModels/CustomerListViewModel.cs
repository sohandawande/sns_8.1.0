﻿using System.Collections.Generic;
using Znode.Engine.MvcAdmin.Models;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class CustomerListViewModel : BaseViewModel
    {
        public List<CustomerViewModel> Customers { get; set; }
        public ReportModel GridModel { get; set; }
        public bool IsEnableCustomerPricing { get; set; }

        public CustomerListViewModel()
        {
            Customers = new List<CustomerViewModel>();
            GridModel = new ReportModel();
        }

        public int AccountId { get; set; } //PRFT Custom Code
    }
}