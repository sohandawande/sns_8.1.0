﻿using Resources;
using System.ComponentModel.DataAnnotations;
using Znode.Libraries.Helpers;
namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View model for create RMAConfiguration
    /// </summary>
    public class RMAConfigurationViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for RMAConfigurationViewModel
        /// </summary>
        public RMAConfigurationViewModel()
        {

        }

        [MaxLength(100)]
        [LocalizedDisplayName(Name = RZnodeResources.LabelDepartment)]
        public string DisplayName { get; set; }

        [MaxLength(100)]
        [LocalizedDisplayName(Name = RZnodeResources.LabelCUstomerEmailId)]
        [EmailAddress(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "ValidEmailAddress", ErrorMessage = "")]
        public string EmailId { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelReturnAddress)]
        public string ReturnMailingAddress { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelShippingInstruction)]
        public string ShippingInstruction { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelCustomerNotification)]
        public string GiftCardNotification { get; set; }

        public int RMAConfigId { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelRMAMaxDays)]
        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "rfvMaxDays")]
        public int? MaxDays { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelGiftCardExpiration)]
        [Range(1, 99999, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "rnvRMAConfigIssueGiftCardRange")]
        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "rfvMaxDays")]
        public int GiftCardExpirationPeriod { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelEnableEmailNotification)]
        public bool? EnableEmailNotification { get; set; }
    }
}