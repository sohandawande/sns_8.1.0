﻿using System.Collections.Generic;
using System.Web.WebPages.Html;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// List View Model for Attributes
    /// </summary>
    public class AttributesListViewModel : BaseViewModel
    {
         /// <summary>
        /// Constructor for Attributes
        /// </summary>
        public AttributesListViewModel()
        {
            Attributes = new List<AttributesViewModel>();
        }

        public List<AttributesViewModel> Attributes { get; set; }       
    }
}