﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.MvcAdmin.Models;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class ApplicationSettingListViewModel : BaseViewModel
    {
        public List<ApplicationSettingModel> List { get; set; }

        public ApplicationSettingListViewModel()
        {
            List = new List<ApplicationSettingModel>();
        }
    }
}