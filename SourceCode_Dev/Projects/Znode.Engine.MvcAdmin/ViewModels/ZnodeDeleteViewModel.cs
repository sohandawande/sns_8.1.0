﻿
namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class ZnodeDeleteViewModel:BaseViewModel
    {
        public bool IsDeleted { get; set; }
        public string ErrorMessage { get; set; }
    }
}