﻿using System.Collections.Generic;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class RMARequestListViewModel : BaseViewModel
    {
        public RMARequestListViewModel()
        {
            RMARequestList = new List<RMARequestViewModel>();
        }
        public List<RMARequestViewModel> RMARequestList { get; set; }
        public List<RequestStatusViewModel> RequestStatusList { get; set; }
        public List<PortalViewModel> PortalList { get; set; }
    }
}