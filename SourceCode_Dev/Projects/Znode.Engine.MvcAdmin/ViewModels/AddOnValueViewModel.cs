﻿using Resources;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Libraries.Helpers;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// ViewModel for AddOn Value
    /// </summary>
    public class AddOnValueViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for AddOnValueViewModel
        /// </summary>
        public AddOnValueViewModel()
        {

        }
        [LocalizedDisplayName(Name = RZnodeResources.LabelLabel)]
        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "rfvLabel")]
        public string Name { get; set; }

        [MaxLength(100)]
        [LocalizedDisplayName(Name = RZnodeResources.LabelSkuPart)]
        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "rfvSku")]
        public string SKU { get; set; }

        [MaxLength(50)]
        [LocalizedDisplayName(Name = RZnodeResources.LabelBillingPeriod)]
        public string BillingPeriod { get; set; }
        public string Description { get; set; }
        public string RecurringBillingFrequency { get; set; }
        public string DisplayPrice { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelQuantityOnHand)]
        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "rfvQuantityOnHand")]
        [RegularExpression("^\\d{0,9}(\\.\\d{0,2})?$", ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "ErrorAddOnNumberRange")]
        [Range(0, 999999, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "ErrorAddOnNumberRange")]
        public int QuantityOnHand { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelReOrderLevel)]
        [RegularExpression("^\\d{0,9}(\\.\\d{0,2})?$", ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "ErrorAddOnNumberRange")]
        [Range(0, 999999, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "ErrorAddOnNumberRange")]
        public int? ReOrderLevel { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelDisplayOrder)]
        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "rfvDisplayOrder")]
        [Range(1, 999999999, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "ValidNumber")]
        [RegularExpression("^(1|[1-9][0-9]*)$", ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "InvalidDisplayOrder")]
        public int? DisplayOrder { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelSelectSupplier)]
        public int? SupplierId { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.HeaderTaxClassList)]
        public int TaxClassId { get; set; }
        public int AddOnId { get; set; }
        public int AddOnValueId { get; set; }
        public int RecurringBillingTotalCycles { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelSelectShippingType)]
        public int? ShippingRuleTypeId { get; set; }



        [LocalizedDisplayName(Name = RZnodeResources.LabelItemWeight)]
        [RegularExpression("^\\d{0,9}(\\.\\d{0,2})?$", ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "revWeight")]
        [Range(0.0, 9999999.0, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "rnvWeight")]
        public decimal? Weight { get; set; }

        [RegularExpression("^\\d{0,9}(\\.\\d{0,2})?$", ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "revHeight")]
        public decimal? Height { get; set; }


        [RegularExpression("^\\d{0,9}(\\.\\d{0,2})?$", ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "revWidth")]
        public decimal? Width { get; set; }

        [RegularExpression("^\\d{0,6}(\\.\\d{0,2})?$", ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "revLength")]
        public decimal? Length { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelRetailPrice)]
        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "rfvRetailPrice")]
        [Range(0, 999999, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "rnvRetailPrice")]
        [RegularExpression("^-?\\d{1,9}(\\.\\d{1,2})?$", ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "revRetailPrice")]
        public decimal RetailPrice { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelBillingAmount)]
        [RegularExpression("^-?\\d{1,9}(\\.\\d{1,2})?$", ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "revBillingAmount")]
        [Range(0, 999999.99, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "rnvBillingAmount")]
        public decimal? BillingAmount { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelSalePrice)]
        [RegularExpression("^-?\\d{1,9}(\\.\\d{1,2})?$", ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "revSalesPrice")]
        [Range(0, 999999, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "rnvSalesPrice")]
        public decimal? SalePrice { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelWholesalePrice)]
        [RegularExpression("^-?\\d{1,9}(\\.\\d{1,2})?$", ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "revWholesalePrice")]
        [Range(0, 999999, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "rnvWholesalePrice")]
        public decimal? WholesalePrice { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelFreeShipping)]
        public bool EnableFreeShipping { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelRecurringBilling)]
        public bool EnableRecurringBilling { get; set; }
        public bool IsDefault { get; set; }
        public bool RecurringBillingInd { get; set; }
        public bool RecurringBillingInstallmentInd { get; set; }

        public List<SelectListItem> SupplierList { get; set; }
        public List<SelectListItem> TaxClassList { get; set; }
        public List<SelectListItem> ShippingRuleTypeList { get; set; }
        public decimal AddOnFinalPrice { get; set; }
        public string CustomText { get; set; }
        public bool DefaultIndicator { get; set; }
        public int LocaleId { get; set; }
        public int ProductId { get; set; }
        public UserTypes? UserTypes { get; set; }

        public string AddOnValueText
        {
            get
            {
                if (AddOnFinalPrice > 0)
                    return string.Format("{0} + {1}", Name, MvcAdminUnits.GetCurrencyValue(AddOnFinalPrice));

                return Name;
            }
        }
    }
}