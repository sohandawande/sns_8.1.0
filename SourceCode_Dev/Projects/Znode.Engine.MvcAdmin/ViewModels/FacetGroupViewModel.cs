﻿using System.Collections.Generic;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using Resources;
using Znode.Engine.MvcAdmin.Models;
namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model for create and edit Facet Group
    /// </summary>
    public class FacetGroupViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for FacetGroupViewModel
        /// </summary>
        public FacetGroupViewModel()
        {
            GridModel = new ReportModel();
        }
        public int FacetGroupId { get; set; }
        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "RequiredFacetGroupName")]
        [RegularExpression("^[a-zA-Z0-9 ]+$", ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "InvalidFacetGroupName")]
        public string FacetGroupLabel { get; set; }

        public int? ControlTypeId { get; set; }
        public int? CatalogId { get; set; }

        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "RequiredDisplayOrder")]
        [RegularExpression("^(1|[1-9][0-9]*)$", ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "InvalidDisplayOrder")]
        public int? DisplayOrder { get; set; }

        public bool IsEditMode { get; set; }

        public List<SelectListItem> TagControlType { get; set; }

        public List<SelectListItem> Catalogs { get; set; }

        [UIHint("TwoListView")]
        public ListViewModel AssociateCatagoriesListViewModel { get; set; }

        public FacetListViewModel AssociatedFacets { get; set; }
        public ReportModel GridModel { get; set; }
    }
}