﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.MvcAdmin.Models;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class PortalProfileListViewModel:BaseViewModel
    {
        public PortalProfileListViewModel()
        {
            PortalProfiles = new List<PortalProfileViewModel>();
        }
        public List<PortalProfileViewModel> PortalProfiles { get; set; }

        public ReportModel Grid { get; set; }

        public int PortalId { get; set; }
    }
}