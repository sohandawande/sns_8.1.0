﻿using System.Collections.Generic;
using Znode.Engine.MvcAdmin.Models;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// List View Model for Promotions
    /// </summary>
    public class PromotionsListViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for PromotionsListViewModel
        /// </summary>
        public PromotionsListViewModel()
        {
            Promotions = new List<PromotionsViewModel>();
            GridModel = new ReportModel();
        }

        public List<PromotionsViewModel> Promotions { get; set; }
        public ReportModel GridModel { get; set; }
       
    }
}