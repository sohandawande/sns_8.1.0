﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Znode.Engine.MvcAdmin.Models;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class PortalCatalogListViewModel : BaseViewModel
    {
        public PortalCatalogListViewModel()
        {
            PortalCatalogs = new List<PortalCatalogViewModel>();
        }
        public List<PortalCatalogViewModel> PortalCatalogs { get; set; }
        public ReportModel Grid { get; set; }
        public int PortalId { get; set; }
    }
}
