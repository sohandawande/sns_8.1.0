﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// Product level setting list view model
    /// </summary>
    public class ProductLevelSettingListViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for ProductLevelSettingListViewModel
        /// </summary>
        public ProductLevelSettingListViewModel()
        {
            Products = new List<ProductLevelSettingViewModel>();
        }
        public List<ProductLevelSettingViewModel> Products { get; set; }
    }
}