﻿
namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class RolePermissionViewModel : BaseViewModel
    {
        public int RoleId { get; set; }
        public int PermissionId { get; set; }
        public string EntityAction { get; set; }
    }
}