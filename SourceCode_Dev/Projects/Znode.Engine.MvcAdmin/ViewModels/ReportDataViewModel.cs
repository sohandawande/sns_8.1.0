﻿using System.Collections.Generic;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// Report Data View Model
    /// </summary>
    public class ReportDataViewModel : BaseViewModel
    {
        public List<object> ReportDataSet { get; set; }
        public List<PortalViewModel> StoreList { get; set; }
        public SupplierListViewModel SupplierList { get; set; }
        public List<OrderStateViewModel> OrderStateList { get; set; }
        public List<DurationViewModel> DurationList { get; set; }
        public List<CategoryStateViewModel> CategoryList { get; set; }
        public List<DurationViewModel> GroupByList { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string ReportTitle { get; set; }
        public ReportDataViewModel()
        {
            ReportDataSet = new List<dynamic>();
            StoreList = new List<PortalViewModel>();
            SupplierList = new SupplierListViewModel();
            OrderStateList = new List<OrderStateViewModel>();
            DurationList = new List<DurationViewModel>();
            CategoryList = new List<CategoryStateViewModel>();
            GroupByList = new List<DurationViewModel>();
        }
    }
}