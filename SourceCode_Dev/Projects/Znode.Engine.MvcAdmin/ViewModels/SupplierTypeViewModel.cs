﻿
namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View model for Supplier Type
    /// </summary>
    public class SupplierTypeViewModel :BaseViewModel
    {
        /// <summary>
        /// Constructor for Supplier Type
        /// </summary>
        public SupplierTypeViewModel()
        {

        }
        public string ClassName { get; set; }
        public string Description { get; set; }
        
        public string Name { get; set; }

        public bool IsActive { get; set; }
        
        public int SupplierTypeId { get; set; }
    }
}