﻿using System.Collections.Generic;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class CustomerAffiliateListViewModel :BaseViewModel
    {
        public List<CustomerAffiliateViewModel> CustomerAffiliateList { get; set; }

        /// <summary>
        /// Constructor For CustomerAffiliateViewModel
        /// </summary>
        public CustomerAffiliateListViewModel()
        {
            CustomerAffiliateList = new List<CustomerAffiliateViewModel>();
        }
    }
}