﻿using System.Collections.Generic;
using Znode.Engine.MvcAdmin.Models;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class ProductBundlesListViewModel: BaseViewModel
    {
         /// <summary>
        /// Constructor for Product associate bundle list.
        /// </summary>
        public ProductBundlesListViewModel()
        {
            ProductBundles = new List<ProductBundlesViewModel>();
            GridModel = new ReportModel();
        }

        public List<ProductBundlesViewModel> ProductBundles { get; set; }
        public ReportModel GridModel { get; set; }
        public int ProductId { get; set; }
    }
}