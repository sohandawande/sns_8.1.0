﻿using Resources;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Znode.Libraries.Helpers;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model for Shipping Rule View Model
    /// </summary>
    public class ShippingRuleViewModel : BaseViewModel
    {
        #region Constructor
        /// <summary>
        /// Constructor for Shipping Rule View Model
        /// </summary>
        public ShippingRuleViewModel()
        {
            ShippingRuleType = new ShippingRuleTypeViewModel();
            ShippingOption = new ShippingOptionViewModel();
        } 
        #endregion

        public string RuleTypeName { get; set; }
        public string customErrorMessage { get; set; }

        public int ShippingRuleId { get; set; }
        public int ShippingRuleTypeId { get; set; }
        public int shippingId { get; set; }      
       
        [LocalizedDisplayName(Name = RZnodeResources.LabelBaseCost)]
        [DisplayFormat(DataFormatString = "{0:n2}", ApplyFormatInEditMode = true)]       
        public decimal BaseCost { get; set; }
        public string BaseCostWithDollar { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelPerUnitCost)]       
        public decimal PerItemCost { get; set; }
        public string PerItemCostWithDollar { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelLowerLimit)]       
        public decimal? LowerLimit { get; set; }
        public string LowerLimitWithDollar { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources. LabelUpperLimit)]
        public decimal? UpperLimit { get; set; }
        public string UpperLimitWithDollar { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelRuleType)]
        public ShippingRuleTypeViewModel ShippingRuleType { get; set; }

        public List<SelectListItem> ShippingRuleTypeList { get; set; }

        public ShippingOptionViewModel ShippingOption { get; set; }         
    }
}