﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model for Dashboard.
    /// </summary>
    public class DashboardViewModel : BaseViewModel
    {
        #region Constructor
        /// <summary>
        /// Constructor for Dashboard. 
        /// </summary>
        public DashboardViewModel()
        {

        }
        #endregion

        public string PaymentGateway { get; set; }
        public string StatusMessage { get; set; }
        public string StoreName { get; set; }

        public int TotalProducts { get; set; }
        public int TotalCategories { get; set; }
        public int TotalInventory { get; set; }
        public int TotalOutOfStock { get; set; }      
        public int ShippedToday { get; set; }
        public int ReturnedToday { get; set; }
        public int TotalOrders { get; set; }
        public int TotalOrdersMTD { get; set; }
        public int TotalNewOrders { get; set; }
        public int TotalPaymentPendingOrders { get; set; }
        public int TotalSubmittedOrders { get; set; }
        public int TotalShippedOrders { get; set; }
        public int TotalAccounts { get; set; }
        public int TotalAccountsMTD { get; set; }
        public int TotalPages { get; set; }
        public int TotalShippingOptions { get; set; }       
        public int TotalPendingServiceRequests { get; set; }
        public int TotalReviewsToApprove { get; set; }
        public int TotalAffiliatesToApprove { get; set; }
        public int TotalLowInventoryItems { get; set; }
        public int TotalFailedLoginsToday { get; set; }
        public int EmailOptInCustomers { get; set; }
        public int TotalDeclinedTransactions { get; set; }
        public int TotalLowInventoryThanReorder { get; set; }

        public decimal YTDRevenue { get; set; }
        public decimal MTDRevenue { get; set; }
        public decimal TodayRevenue { get; set; }

        public List<SelectListItem> PopularSearchKeywords { get; set; }
    }
}