﻿using Resources;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class PaymentOptionViewModel : BaseViewModel
    {
        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = RZnodeResources.rfvDisplayOrder)]
        [Range(1, 999999999, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "InvalidDisplayOrder")]
        public int DisplayOrder { get; set; }
        public bool? EnableAmericanExpress { get; set; }
        public bool? EnableDiscover { get; set; }
        public bool? EnableMasterCard { get; set; }
        public bool? EnableRecurringPayments { get; set; }
        public bool? EnableVault { get; set; }
        public bool? EnableVisa { get; set; }
        public bool IsActive { get; set; }
        public bool? IsRmaCompatible { get; set; }
        public string Partner { get; set; }
        public PaymentGatewayViewModel PaymentGateway { get; set; }
        public int? PaymentGatewayId { get; set; }
        public string PaymentGatewayPassword { get; set; }
        public string PaymentGatewayUsername { get; set; }
        public int PaymentOptionId { get; set; }
        public PaymentTypeViewModel PaymentType { get; set; }
        public int PaymentTypeId { get; set; }
        public bool PreAuthorize { get; set; }
        public int? ProfileId { get; set; }
        public string ProfileName { get; set; }
        public bool TestMode { get; set; }
        public string TransactionKey { get; set; }
        public string Vendor { get; set; }
        public string PaymentTypeName { get; set; }

        [Range(0.01, 99999999999.00, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "rnvDisplayOrder")]
        public string AdditionalFee { get; set; }

        public List<SelectListItem> Profiles { get; set; }
        public List<SelectListItem> PaymentTypes { get; set; }
        public List<SelectListItem> PaymentGateways { get; set; }

        public string PaymentName { get; set; }

        public PaymentOptionViewModel()
        {
            PaymentGateway = new PaymentGatewayViewModel();
            PaymentType = new PaymentTypeViewModel();
        }
    }
}