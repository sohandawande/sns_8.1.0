﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class ActionModel : BaseViewModel
    {
        private string _callback = "";        
        public string Action { get; set; }
        public string Controller { get; set; }
        public string FormId { get; set; }
        public string SaveHeader { get; set; }
        public string CancelHeader { get; set; }
        public string SaveId { get; set; }
        public string CancelId { get; set; }
        public string CancelUrl { get; set; }
        public string Callback
        {
            get
            {
                return _callback;
            }
            set
            {
                _callback = value;
            }
        }
    }
}