﻿using System.Collections.Generic;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View model for Url Redirect.
    /// </summary>
    public class URLRedirectListViewModel : BaseViewModel
    {
        #region Constructor
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public URLRedirectListViewModel()
        {
            UrlRedirects = new List<URLRedirectViewModel>();
        } 
        #endregion

        public List<URLRedirectViewModel> UrlRedirects { get; set; }
    }
}