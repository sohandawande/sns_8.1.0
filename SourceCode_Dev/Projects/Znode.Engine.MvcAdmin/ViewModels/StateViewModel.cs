﻿
namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model for State
    /// </summary>
    public class StateViewModel : BaseViewModel
    {
        #region Constructor for StateViewModel
        /// <summary>
        /// Constructor for CountryViewModel
        /// </summary>
        public StateViewModel()
        {

        } 
        #endregion

        public string Code { get; set; }
        public string CountryCode { get; set; }
        public string Name { get; set; }
    }
}
   