﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model for Shipping Type
    /// </summary>
    public class ShippingTypeViewModel : BaseViewModel
    {
        #region Constructor
        /// <summary>
        /// Constructor for Shipping Type View Model
        /// </summary>
        public ShippingTypeViewModel()
        {

        } 
        #endregion

        public string ClassName { get; set; }
        public string Description { get; set; }
        public int ShippingTypeId { get; set; }
        public bool IsActive { get; set; }
        public string Name { get; set; }        
    }
}