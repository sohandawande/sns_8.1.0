﻿
using Resources;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Znode.Engine.Api.Models;
using Znode.Libraries.Helpers;
namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// ViewModel for create and edit TaxRules
    /// </summary>
    public partial class TaxRulesViewModel : BaseViewModel
    {
        #region Constructor for TaxRuleViewModel
        /// <summary>
        /// Constructor for TaxRuleViewModel
        /// </summary>
        public TaxRulesViewModel()
        {

        } 
        #endregion
        
        public int TaxRuleId { get; set; }
        public int? TaxClassId { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelTaxType)]
        public int TaxTypeId { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelPrecedence)]
        [Range(1, 999999999, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.InvalidDisplayOrder)]
        [RegularExpression("^[0-9]*$", ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = RZnodeResources.ValidNumber)]
        public int Precedence { get; set; }
        public int PortalId { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelState)]
        public string StateCode { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelCountry)]
        public string CountryCode { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelCounty)]
        public string CountyFips { get; set; }
        public string CountyName { get; set; }
        public ZipCodeModel zipCodeModel { get; set; }
        public string CheckIsInclusive { get; set; }       
       
        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = RZnodeResources.ErrorRequired)]
        [Range(0, 100, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.rnvTax)]
        [RegularExpression("^[0-9]*(\\.[0-9]{1,2})?$", ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = RZnodeResources.VATTaxErrorMessage)]
        [LocalizedDisplayName(Name = RZnodeResources.LabelVATTax)]      
        public decimal? VATTax { get; set; }
        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = RZnodeResources.ErrorRequired)]
        [Range(0, 100, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.rnvTax)]
        [RegularExpression("^[0-9]*(\\.[0-9]{1,2})?$", ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = RZnodeResources.GSTTaxErrorMessage)]
        [LocalizedDisplayName(Name = RZnodeResources.LabelGSTTax)]
        public decimal? GSTTax { get; set; }
        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = RZnodeResources.ErrorRequired)]
        [Range(0, 100, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.rnvTax)]
        [RegularExpression("^[0-9]*(\\.[0-9]{1,2})?$", ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = RZnodeResources.PSTTaxErrorMessage)]
        [LocalizedDisplayName(Name = RZnodeResources.LabelPSTTax)]
        public decimal? PSTTax { get; set; }
        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = RZnodeResources.ErrorRequired)]
        [Range(0, 100, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.rnvTax)]
        [RegularExpression("^[0-9]*(\\.[0-9]{1,2})?$", ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = RZnodeResources.HSTTaxErrorMessage)]
        [LocalizedDisplayName(Name = RZnodeResources.LabelHSTTax)]
        public decimal? HSTTax { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelInclusiveTax)]
        public bool InclusiveTax { get; set; }

        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = RZnodeResources.ErrorRequired)]
        [RegularExpression("^[0-9]*(\\.[0-9]{1,2})?$", ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = RZnodeResources.SalesTaxErrorMessage)]
        [Range(0, 100, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.rnvTax)]
        [LocalizedDisplayName(Name = RZnodeResources.LabelSalesTax)]
        public string SalesTax { get; set; }
        public List<SelectListItem> TaxRuleTypeList { get; set; }
        public List<SelectListItem> CountryList { get; set; }
        public List<SelectListItem> StateList { get; set; }
        public List<SelectListItem> CountyList { get; set; }

        public Collection<TaxRuleTypeModel> TaxRuleTypeCollection { get; set; } //PRFT Custom Code 

    }
}