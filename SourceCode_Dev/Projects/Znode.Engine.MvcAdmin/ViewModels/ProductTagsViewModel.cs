﻿
namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class ProductTagsViewModel : BaseViewModel
    {
        public int ProductId { get; set; }
        public string ProductTags { get; set; }
        public int TagId { get; set; }
    }
}