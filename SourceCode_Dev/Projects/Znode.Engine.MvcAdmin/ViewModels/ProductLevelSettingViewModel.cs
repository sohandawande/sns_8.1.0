﻿
namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// Product level setting view model
    /// </summary>
    public class ProductLevelSettingViewModel : BaseViewModel
    {
        public string Name { get; set; }
        public string SKU { get; set; }
        public string ProductNumber { get; set; }
        
        public int ProductId { get; set; }
        public int CatalogId { get; set; }
        public int categoryId { get; set; }
        public int ProductTypeId { get; set; }
        public int ManufacturerId { get; set; }

        public string Boost { get; set; }
    }
}