﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Znode.Libraries.Helpers;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model For ManageSEO
    /// </summary>
    public class ManageSEOViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for ManufacturerViewModel
        /// </summary>
        public ManageSEOViewModel()
        {

        }

        [LocalizedDisplayName(Name = RZnodeResources.LabelSeoContentKeyword)]
        public string SeoDefaultContentKeyword { get; set; }

        [AllowHtml]
        [LocalizedDisplayName(Name = RZnodeResources.LabelSeoProductTitle)]
        public string SeoDefaultProductTitle { get; set; }

        [AllowHtml]
        [LocalizedDisplayName(Name = RZnodeResources.LabelSeoProductDescription)]
        public string SeoDefaultProductDescription { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelSeoProductKeyword)]
        public string SeoDefaultProductKeyword { get; set; }

        [AllowHtml]
        [LocalizedDisplayName(Name = RZnodeResources.LabelSeoCategoryTitle)]
        public string SeoDefaultCategoryTitle { get; set; }

        [AllowHtml]
        [LocalizedDisplayName(Name = RZnodeResources.LabelSeoCategoryDescription)]
        public string SeoDefaultCategoryDescription { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelSeoCategoryKeyword)]
        public string SeoDefaultCategoryKeyword { get; set; }

        [AllowHtml]
        [LocalizedDisplayName(Name = RZnodeResources.LabelSeoContentTitle)]
        public string SeoDefaultContentTitle { get; set; }

        [AllowHtml]
        [LocalizedDisplayName(Name = RZnodeResources.LabelSeoContentDescription)]
        public string SeoDefaultContentDescription { get; set; }

        public string PortalName { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelStoreName)]
        public int PortalId { get; set; }

        public List<SelectListItem> PortalList { get; set; }
    }
}