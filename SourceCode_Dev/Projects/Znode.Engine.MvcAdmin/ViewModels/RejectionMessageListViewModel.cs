﻿using System.Collections.Generic;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// List view model rejection message.
    /// </summary>
    public class RejectionMessageListViewModel : BaseViewModel
    {
        #region Constructor
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public RejectionMessageListViewModel()
        {
            RejectionMessages = new List<RejectionMessageViewModel>();
        } 
        #endregion

        public List<RejectionMessageViewModel> RejectionMessages { get; set; }
    }
}