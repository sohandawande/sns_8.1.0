﻿using System.Collections.Generic;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// List View Model for Shipping Options
    /// </summary>
    public class ShippingOptionListViewModel : BaseViewModel
    {
        #region Constructor
        /// <summary>
        /// Constructor for ShippingOptionListViewModel
        /// </summary>
        public ShippingOptionListViewModel()
        {
            ShippingOptions = new List<ShippingOptionViewModel>();
        } 
        #endregion

        public List<ShippingOptionViewModel> ShippingOptions { get; set; }

        public List<ShippingRuleViewModel> ShippingRule { get; set; }

        public List<ProfileViewModel> Profile { get; set; }       
    }
}