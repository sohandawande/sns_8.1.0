﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class AccountProfileViewModel : BaseViewModel
    {
        public int AccountProfileID { get; set; }
        public int? ProfileID { get; set; }
        public int AccountID { get; set; }
    }
}