﻿using Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Znode.Libraries.Helpers;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// view model for Vendor contact .
    /// </summary>
    public class VendorContactViewModel : BaseViewModel
    {
        #region Default constructor
        /// <summary>
        /// constructor for VendorContactViewModel
        /// </summary>
        public VendorContactViewModel()
        {

        }
        #endregion

        [LocalizedDisplayName(Name = RZnodeResources.LabelFirstName)]
        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "ErrorRequired")]
        public string FirstName { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelLastName)]
        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "ErrorRequired")]
        public string LastName { get; set; }

        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "ErrorRequired")]
        [RegularExpression("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$", ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "ValidEmailAddress")]
        [LocalizedDisplayName(Name = RZnodeResources.StoreAdminEmailAddress)]
        public string Email { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.PlaceHolderPhoneNo)]
        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "ErrorRequired")]
        public string PhoneNumber { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelCompanyName)]
        public string CompanyName { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelStreet1)]
        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "ErrorRequired")]
        public string Street1 { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelStreet2)]
        public string Street2 { get; set; }

        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "ErrorRequired")]
        public string City { get; set; }

        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "ErrorRequired")]
        public string State { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelPostalCode)]
        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "ErrorRequired")]
        public string PostalCode { get; set; }

        public string FullName { get; set; }
        public int AccountId { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelFranchiseAccountNumber)]
        public string ExternalAccountNum { get; set; }

        public string CountryCode { get; set; }
        public List<SelectListItem> CountryList { get; set; }
    }
}