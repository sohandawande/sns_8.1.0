﻿using System.Collections.Generic;
using Znode.Engine.MvcAdmin.Models;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class PortalCountriesListViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for PortalCountryListViewModel
        /// </summary>
        public PortalCountriesListViewModel()
        {
            PortalCountryList = new List<PortalCountriesViewModel>();
        }
        public List<PortalCountriesViewModel> PortalCountryList { get; set; }
        public ReportModel Grid { get; set; }
        public int PortalId { get; set; }
    }
}