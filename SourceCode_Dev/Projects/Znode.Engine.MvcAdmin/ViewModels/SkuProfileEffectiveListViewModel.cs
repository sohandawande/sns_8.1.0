﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.MvcAdmin.Models;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// List View model for Sku Profile Effective.
    /// </summary>
    public class SkuProfileEffectiveListViewModel : BaseViewModel
    {
        #region Consrtuctor
        /// <summary>
        /// Constructor for SkuProfileEffectiveListViewModel
        /// </summary>
        public SkuProfileEffectiveListViewModel()
        {
            SkuProfileEffectives = new List<SkuProfileEffectiveViewModel>();
            GridModel = new ReportModel();
        } 
        #endregion

        public List<SkuProfileEffectiveViewModel> SkuProfileEffectives { get; set; }
        public ReportModel GridModel { get; set; }
        public int? SKUId { get; set; }
    }
}