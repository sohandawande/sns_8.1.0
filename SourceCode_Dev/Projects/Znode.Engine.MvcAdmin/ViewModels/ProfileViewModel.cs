﻿using Resources;
using System.ComponentModel.DataAnnotations;
using Znode.Libraries.Helpers;
namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model for create Profile
    /// </summary>
    public class ProfileViewModel : BaseViewModel
    {
        /// <summary>
        /// Consructor for ProfileViewModel
        /// </summary>
        public ProfileViewModel()
        {

        }
        [LocalizedDisplayName(Name = RZnodeResources.LabelProfileName)]
        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "rfvProfileName")]
        [MaxLength(100)]
        public string Name { get; set; }
        public int ProfileId { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "rfvProfileWeight")]
        [LocalizedDisplayName(Name = RZnodeResources.LabelProfileWeight)]
        [Range(1, int.MaxValue, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "revProfileWeight")]
        public decimal? Weighting { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelProfileExternalId)]
        public string ExternalId { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelProfileProductPrice)]
        public bool ProductPrice { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelProfileAffiliate)]
        public bool Affiliate { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelProfileTaxExempt)]
        public bool TaxExempt { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelProfileWholesalePrice)]
        public bool? WholeSalePrice { get; set; }

        public bool ShowPricing { get; set; }
        public int AccountProfileID { get; set; }
    }
}