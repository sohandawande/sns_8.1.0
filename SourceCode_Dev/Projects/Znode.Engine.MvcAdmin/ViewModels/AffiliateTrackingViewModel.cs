﻿using Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Znode.Libraries.Helpers;
using Znode.Libraries.Helpers.Attributes;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class AffiliateTrackingViewModel : BaseViewModel
    {
        [LocalizedDisplayName(Name = RZnodeResources.LabelBeginDate)]
        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "rfvBeginDate")]
        public string StartDate { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelEndDate)]
        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "rfvEndDate")]
        [GenericCompare(CompareToPropertyName = "StartDate", OperatorName = GenericCompareOperator.GreaterThanOrEqual, ErrorMessageResourceName = "CPVStartDateEndDate",
        ErrorMessageResourceType = typeof(ZnodeResources))]
        public string EndDate { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelFileType)]
        public int FileTypeId { get; set; }
        public List<SelectListItem> FileTypes { get; set; }
    }
}