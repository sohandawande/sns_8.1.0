﻿using Resources;
using System;
using System.ComponentModel.DataAnnotations;
using Znode.Libraries.Helpers;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class AccountPaymentViewModel : BaseViewModel
    {
        public string TransactionId { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelPaymentDescription)]
        public string Description { get; set; }
        public int AccountPaymentId { get; set; }
        public int AccountId { get; set; }

        public int? OrderId { get; set; }
       
        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "rfvAccountPayment")]
        [LocalizedDisplayName(Name = RZnodeResources.LabelAmount)]
        public decimal? Amount { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelAmountOwed)]
        public decimal? AmountOwed { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelPaymentDate)]
        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "rfvAccountPaymentDate")]
        public string PaymentDate { get; set; }

        public string DisplayAmountOwned { get; set; }

        public string DisplayAmount { get; set; }
        public string DisplayPaymentDate { get; set; }
    }
}