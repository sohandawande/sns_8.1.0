﻿using System.Collections.ObjectModel;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model for Franchise Administrators
    /// </summary>
    public class FranchiseAdministratorsViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for Franchise Administrators
        /// </summary>
        public FranchiseAdministratorsViewModel()
        {

        }

        public string UserName { get; set; }
        public string StoreName { get; set; }
        public string SiteURL { get; set; }
        public string Email { get; set; }
        public string FranchiseNumber { get; set; }
        
        public Collection<AddressViewModel> Addresses { get; set; }
        
    }
}