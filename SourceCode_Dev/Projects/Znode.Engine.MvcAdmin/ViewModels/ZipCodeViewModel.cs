﻿
namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model for Zip Code
    /// </summary>
    public class ZipCodeViewModel : BaseViewModel
    {
        #region Constructor for ZipCodeViewModel
        /// <summary>
        /// Constructor for CountryViewModel
        /// </summary>
        public ZipCodeViewModel()
        {

        } 
        #endregion

        public int ZipCodeId { get; set; }        
       
        public string StateAbbr { get; set; }        
        public string CountyName { get; set; }
        public string CountyFips { get; set; }
        public string StateFips { get; set; }       
    }
}
   