﻿using Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Znode.Engine.MvcAdmin.Models;
using Znode.Libraries.Helpers;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class CategoryNodeViewModel : BaseViewModel
    {
        public CatalogViewModel Catalog { get; set; }
        public int? CatalogId { get; set; }
        public CategoryViewModel Category { get; set; }
        public int CategoryId { get; set; }
        public int CategoryNodeId { get; set; }
        public DateTime? BeginDate { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelStylesheet)]
        public int? CssId { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "rfvDisplayOrder")]
        [Range(1, int.MaxValue, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "ValidDisplayOrder")]
        public int? DisplayOrder { get; set; }
        public DateTime? EndDate { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelEnableCategory)]
        public bool IsActive { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelMasterPageTemplate)]
        public int? MasterPageId { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelParentCategory)]
        public int? ParentCategoryNodeId { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelCategoryPageTheme)]
        public int? ThemeId { get; set; }
        public List<CatalogAssociatedCategoriesViewModel> Categories { get; set; }
        public string CatalogName { get; set; }
        public List<SelectListItem> ParentCategoryNodes { get; set; }
        public List<SelectListItem> ThemeList { get; set; }
        public List<SelectListItem> CssList { get; set; }
        public List<SelectListItem> MasterPageList { get; set; }
        public List<SelectListItem> CategoryProfile { get; set; }
        public int CategoryProfileId { get; set; }
        public CategoryProfileViewModel CategoryProfiles { get; set; }
        public ReportModel AssociatedCategoryProfiles { get; set; }

        public string Name { get; set; }
        public string Title { get; set; }
        public bool? VisibleInd { get; set; }
        public bool IsSuccess { get; set; }
    }
}