﻿using System.Collections.Generic;
using Znode.Engine.MvcAdmin.Models;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class DomainListViewModel : BaseViewModel
    {
        #region Constructor
        /// <summary>
        /// Constructor for DomainViewModel that initialises the Domains list.
        /// </summary>
        public DomainListViewModel()
        {
            GridModel = new ReportModel();
            Domains = new List<DomainViewModel>();
        } 
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets all Domain list.
        /// </summary>
        public List<DomainViewModel> Domains { get; set; }
        public FilterColumnListModel FilterColumn { get; set; }
        public ReportModel GridModel { get; set; }
        public int PortalId { get; set; }
        #endregion
    }
}