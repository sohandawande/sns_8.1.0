﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.MvcAdmin.Models;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// List view model for ManageSEO Category List
    /// </summary>
    public class ManageSEOCategoryListViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for Product Sku
        /// </summary>
        public ManageSEOCategoryListViewModel()
        {
            GridModel = new ReportModel();
            CategoryList = new CatalogAssociatedCategoriesListViewModel();
        }
        public ReportModel GridModel { get; set; }
        public CatalogAssociatedCategoriesListViewModel CategoryList { get; set; }
    }
}