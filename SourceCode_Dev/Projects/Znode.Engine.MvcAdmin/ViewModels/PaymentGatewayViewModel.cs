﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class PaymentGatewayViewModel
    {
        public string Name { get; set; }
        public int PaymentGatewayId { get; set; }
        public string Url { get; set; }
    }
}