﻿using Resources;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Models;
using Znode.Libraries.Helpers;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model For Shipping Options
    /// </summary>
    public class ShippingOptionViewModel : BaseViewModel
    {
        #region Constructor
        /// <summary>
        /// Constructor for ShippingOptionViewModel
        /// </summary>
        public ShippingOptionViewModel()
        {
            ReportModel = new ReportModel();
            ShippingType = new ShippingTypeModel();
            //ShippingRule = new ShippingRuleViewModel();
            //ShippingRuleList = new ShippingRuleListViewModel();
            //ShippingTypes = new ShippingTypeViewModel();
            //ShippingTypeList = new List<SelectListItem>();
            //Profiles = new ProfileViewModel();
            //ProfileList = new List<SelectListItem>();
            //Countries = new CountryViewModel();
            //CountryList = new List<SelectListItem>();
            //ShippingServiceCode = new ShippingServiceCodeViewModel();
            //ShippingServiceCodeList = new List<SelectListItem>();
        }
        #endregion

        [LocalizedDisplayName(Name = RZnodeResources.LabelDestinationCountryOptional)]
        public string CountryCode { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelDisplayName)]
        public string Description { get; set; }
        public string ExternalId { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelInternalCode)]
        public string ShippingCode { get; set; }
        public string ShippingTypeName { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelProfileName)]
        public string ProfileName { get; set; }
        public string CustomErrorMessage { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.rfvDisplayOrder)]
        [Range(1, 999999999, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.InvalidDisplayOrder)]
        [LocalizedDisplayName(Name = RZnodeResources.LabelDisplayOrder)]
        public int? DisplayOrder { get; set; }
        public int ShippingOptionId { get; set; }
        public int ShippingTypeId { get; set; }
        public int ShippingServiceCodeId { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelProfile)]
        public int? ProfileId { get; set; }
        public int PortalId { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.rfvHandlingCharge)]
        [RegularExpression("^\\d{0,30}(\\.\\d{0,2})?$", ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.revHandlingCharge)]
        [LocalizedDisplayName(Name = RZnodeResources.LabelHandlingCharge)]
        [Range(0, 214748, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.InvalidDisplayOrder)]
        public decimal HandlingCharge { get; set; }
        public string HandlingChargeWithDollar { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelEnable)]
        public bool IsActive { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelShippingType)]
        public ShippingTypeModel ShippingType { get; set; }

        public ShippingRuleViewModel ShippingRule { get; set; }

        public ShippingRuleListViewModel ShippingRuleList { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelShippingType)]
        public ShippingTypeViewModel ShippingTypes { get; set; }

        public List<SelectListItem> ShippingTypeList { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelProfile)]
        public ProfileViewModel Profiles { get; set; }

        public List<SelectListItem> ProfileList { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelDestinationCountryOptional)]
        public CountryViewModel Countries { get; set; }

        public List<SelectListItem> CountryList { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelService)]
        public ShippingServiceCodeViewModel ShippingServiceCode { get; set; }

        public List<SelectListItem> ShippingServiceCodeList { get; set; }

        public ReportModel ReportModel { get; set; }

        public string UserType { get; set; }

        public string ClassName { get; set; }
    }
}