﻿using Znode.Engine.MvcAdmin.Models;
using System.Data;
using System.Collections.Generic;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class VendorProductViewModel : BaseViewModel
    {
        public VendorProductViewModel()
        {
            ProductModel = new ProductViewModel();
            ProductReviewHistory = new ProductReviewHistoryListViewModel();
            GridModel = new ReportModel();
            CategoryList = new CategoryListViewModel();
            CategoryNode = new CategoryNodeListViewModel();
        }
        public ProductViewModel ProductModel { get; set; }
        public ProductReviewHistoryListViewModel ProductReviewHistory { get; set; }
        public CategoryListViewModel CategoryList { get; set; }
        public ReportModel GridModel { get; set; }
        public int ProductId { get; set; }
        public string ReviewState { get; set; }

        public List<ProductCategoryViewModel> AssociateProductCategories { get; set; }
        public CategoryNodeListViewModel CategoryNode { get; set; }
    }
}