﻿using System.Collections.Generic;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// List view model for suppliers. 
    /// </summary>
    public class SupplierListViewModel : BaseViewModel
    {
        #region Constructor
        /// <summary>
        /// Default constructor for supplier.
        /// </summary>
        public SupplierListViewModel()
        {
            Suppliers = new List<SupplierViewModel>();
        } 
        #endregion

        public List<SupplierViewModel> Suppliers { get; set; }
    }
}