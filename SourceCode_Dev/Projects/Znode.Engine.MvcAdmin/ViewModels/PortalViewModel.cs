﻿using Resources;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Libraries.Helpers;
using Znode.Libraries.Helpers.Attributes;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// Model for PortalViewModel
    /// </summary>
    public class PortalViewModel : BaseViewModel
    {

        //public PortalViewModel()

        #region public constructor
        public PortalViewModel()
        {

        }
        #endregion
        #region Public Properties
        public int PortalId { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.RequiredStoreName)]
        [LocalizedDisplayName(Name = RZnodeResources.LabelStoreName)]
        public string StoreName { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.RequiredCompanyName)]
        [LocalizedDisplayName(Name = RZnodeResources.LabelBrandName)]
        public string CompanyName { get; set; }
        public bool IsCurrentPortal { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelEnablePersistentCart)]
        public bool PersistentCartEnabled { get; set; }


        public int DefaultAnonymousProfileId { get; set; }
        public int MaxRecentViewItemToDisplay { get; set; }
        public string DomainName { get; set; }
        public string ProfileName { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.RequiredLogoImage)]
        [LocalizedDisplayName(Name = RZnodeResources.LabelSelectLogo)]
        [FileTypeValidation(MvcAdminConstants.ImageFileTypes, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.ImageFileTypeErrorMessage)]
        [FileMaxSizeValidation(MvcAdminConstants.ImageMaxFileSize, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.FileSizeExceededErrorMessage)]
        [UIHint("FileUploader")]
        public HttpPostedFileBase LogoPath { get; set; }

        public string LogoPathImageName { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelUseSSL)]
        public bool UseSsl { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.RequiredAdminEmail)]
        [RegularExpression("^([\\w+-.%]+@[\\w-.]+\\.[A-Za-z]{2,4},?)+$", ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.RegularExpressionEmail)]
        [LocalizedDisplayName(Name = RZnodeResources.LabelAdministratorEmail)]
        public string AdminEmail { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.RequiredSalesEmail)]
        [RegularExpression("^([\\w+-.%]+@[\\w-.]+\\.[A-Za-z]{2,4},?)+$", ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.RegularExpressionEmail)]
        [LocalizedDisplayName(Name = RZnodeResources.LabelSalesDepartmentEmail)]
        public string SalesEmail { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.RequiredCustomerEmail)]
        [RegularExpression("^([\\w+-.%]+@[\\w-.]+\\.[A-Za-z]{2,4},?)+$", ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.RegularExpressionEmail)]
        [LocalizedDisplayName(Name = RZnodeResources.LabelCustomerServiceEmail)]
        public string CustomerServiceEmail { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.RequiredCustomerServicePhoneNumber)]
        [LocalizedDisplayName(Name = RZnodeResources.LabelCustomerServicePhoneNumber)]
        public string CustomerServicePhoneNumber { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.RequiredSalesPhoneNumber)]
        [LocalizedDisplayName(Name = RZnodeResources.LabelSalesDepartmentPhoneNumber)]
        public string SalesPhoneNumber { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelEnableAddressValidation)]
        public bool? EnableAddressValidation { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelRequireValidatedAddress)]
        public bool? RequireValidatedAddress { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelEnableCustomerBasedPricing)]
        public bool? EnableCustomerPricing { get; set; }

        public int? DefaultRegisteredProfileId { get; set; }
        public string DimensionUnit { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelFedexAccountNumber)]
        public string FedExAccountNumber { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelInsuranceEnabled)]
        public bool? FedExAddInsurance { get; set; }
        public string FedExClientProductId { get; set; }
        public string FedExClientProductVersion { get; set; }
        public string FedExCspKey { get; set; }
        public string FedExCspPassword { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelSelectFedexDropoffType)]
        public string FedExDropoffType { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelFedexMeterNumber)]
        public string FedExMeterNumber { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelSelectFedexPackagingType)]
        public string FedExPackagingType { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelFedexProductionKey)]
        public string FedExProductionKey { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelFedexSecurityCode)]
        public string FedExSecurityCode { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelFedExDiscountRateEnabled)]
        public bool? FedExUseDiscountRate { get; set; }
        public string GoogleAnalyticsCode { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelDefaultProductImage)]
        [FileTypeValidation(MvcAdminConstants.ImageFileTypes, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.ImageFileTypeErrorMessage)]
        [FileMaxSizeValidation(MvcAdminConstants.ImageMaxFileSize, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.FileSizeExceededErrorMessage)]
        [UIHint("FileUploader")]
        public HttpPostedFileBase ImageNotAvailablePath { get; set; }
        public string ImageNotAvailablePathUrl { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelIncludeTaxesInProduct)]
        public bool InclusiveTax { get; set; }
        public bool IsActive { get; set; }
        public bool IsShippingTaxable { get; set; }

        public string MasterPage { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.RequiredNumberOfThumbnailColumns)]
        [Range(1, 1000, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.RangeValidatorNumbers)]
        [LocalizedDisplayName(Name = RZnodeResources.LabelMaxCatalogCategoryDisplayThumbnails)]
        [RegularExpression("^[\\d]+", ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.RegularExpressionSMTPPort)]
        public int MaxCatalogCategoryDisplayThumbnails { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.RequiredNumberOfCatalogDisplayColumns)]
        [LocalizedDisplayName(Name = RZnodeResources.LabelMaxCatalogDisplayColumns)]
        [Range(1, 10, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.RangeValidatorNumberOfCatalogDisplayColumns)]
        [RegularExpression("^[\\d]+", ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.RegularExpressionSMTPPort)]
        public byte MaxCatalogDisplayColumns { get; set; }

        public int MaxCatalogDisplayItems { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.RequiredCrossSellImageWidth)]
        [Range(1, 1000, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.RangeValidatorNumbers)]
        [LocalizedDisplayName(Name = RZnodeResources.LabelMaxCatalogItemCrossSellWidth)]
        [RegularExpression("^[\\d]+", ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.RegularExpressionSMTPPort)]
        public int MaxCatalogItemCrossSellWidth { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.RequiredLargeImageWidth)]
        [Range(1, 1000, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.RangeValidatorNumbers)]
        [LocalizedDisplayName(Name = RZnodeResources.LabelMaxCatalogItemLargeWidth)]
        [RegularExpression("^[\\d]+", ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.RegularExpressionSMTPPort)]
        public int MaxCatalogItemLargeWidth { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.RequiredMediumImageWidth)]
        [Range(1, 1000, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.RangeValidatorNumbers)]
        [LocalizedDisplayName(Name = RZnodeResources.LabelMaxCatalogItemMediumWidth)]
        [RegularExpression("^[\\d]+", ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.RegularExpressionSMTPPort)]
        public int MaxCatalogItemMediumWidth { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.RequiredNumberOfSmallThumbnailColumns)]
        [Range(1, 1000, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.RangeValidatorNumbers)]
        [LocalizedDisplayName(Name = RZnodeResources.LabelMaxCatalogItemSmallThumbnailWidth)]
        [RegularExpression("^[\\d]+", ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.RegularExpressionSMTPPort)]
        public int MaxCatalogItemSmallThumbnailWidth { get; set; }


        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.RequiredSmallImageWidth)]
        [Range(1, 1000, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.RangeValidatorNumbers)]
        [LocalizedDisplayName(Name = RZnodeResources.LabelMaxCatalogItemSmallWidth)]
        [RegularExpression("^[\\d]+", ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.RegularExpressionSMTPPort)]
        public int MaxCatalogItemSmallWidth { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.RequiredThumbnailImageWidth)]
        [Range(1, 1000, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.RangeValidatorNumbers)]
        [LocalizedDisplayName(Name = RZnodeResources.LabelMaxCatalogItemThumbnailWidth)]
        [RegularExpression("^[\\d]+", ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.RegularExpressionSMTPPort)]
        public int MaxCatalogItemThumbnailWidth { get; set; }

        public string MobileTheme { get; set; }
        [AllowHtml]
        [LocalizedDisplayName(Name = RZnodeResources.LabelOrderReceiptJavascript)]
        public string OrderReceiptAffiliateJavascript { get; set; }
        public string SeoDefaultCategoryDescription { get; set; }
        public string SeoDefaultCategoryKeyword { get; set; }
        public string SeoDefaultCategoryTitle { get; set; }
        public string SeoDefaultContentDescription { get; set; }
        public string SeoDefaultContentKeyword { get; set; }
        public string SeoDefaultContentTitle { get; set; }
        public string SeoDefaultProductDescription { get; set; }
        public string SeoDefaultProductKeyword { get; set; }
        public string SeoDefaultProductTitle { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelShippingOriginAddress1)]
        public string ShippingOriginAddress1 { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelShippingOriginAddress2)]
        public string ShippingOriginAddress2 { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelShippingOriginCity)]
        public string ShippingOriginCity { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelShippingOriginCountyCode)]
        public string ShippingOriginCountryCode { get; set; }

        [RegularExpression("^[ 0-9a-zA-Z()-]{7,15}$", ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "RegularExpressionPhoneNumber")]
        [LocalizedDisplayName(Name = RZnodeResources.LabelShippingOriginPhone)]
        public string ShippingOriginPhoneNumber { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelShippingOriginStateCode)]
        public string ShippingOriginStateCode { get; set; }


        [LocalizedDisplayName(Name = RZnodeResources.LabelShippingOriginZipCode)]
        [RegularExpression("^\\d{5}$|^\\d{5}-\\d{4}$", ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "RegularExpressionZipCode")]
        public string ShippingOriginZipCode { get; set; }
        public int ShopByPriceIncrement { get; set; }
        public int ShopByPriceMax { get; set; }
        public int ShopByPriceMin { get; set; }
        public bool ShowAlternateImageInCategory { get; set; }
        public bool ShowSwatchInCategory { get; set; }
        [AllowHtml]
        [LocalizedDisplayName(Name = RZnodeResources.LabelSiteWideJavascriptExceptOrderReceiptPage)]
        public string SiteWideAnalyticsJavascript { get; set; }
        [AllowHtml]
        [LocalizedDisplayName(Name = RZnodeResources.LabelSiteWideJavascriptBottom)]
        public string SiteWideBottomJavascript { get; set; }
        [AllowHtml]
        [LocalizedDisplayName(Name = RZnodeResources.LabelSiteWideJavascriptTop)]
        public string SiteWideTopJavascript { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelSMTPServerPasswod)]
        public string SmtpPassword { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelSMTPPort)]
        [RegularExpression("^[\\d]+", ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.RegularExpressionSMTPPort)]
        public int? SmtpPort { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelSMTPServer)]
        public string SmtpServer { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelSMTPSrverUserName)]
        public string SmtpUsername { get; set; }
        public int? SplashCategoryId { get; set; }
        public string SplashImageFile { get; set; }
        public string TimeZoneOffset { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelUPSAccessKey)]
        public string UpsKey { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelUPSPassword)]
        public string UpsPassword { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelUPSUserName)]
        public string UpsUsername { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelUseDynamicDisplayOrder)]
        public bool? UseDynamicDisplayOrder { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelWeightUnit)]
        public string WeightUnit { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelCatalog)]
        public int? CatalogId { get; set; }
        public List<SelectListItem> Catalogs { get; set; }
        public string CatalogName { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelTheme)]
        public int? ThemeId { get; set; }
        public List<SelectListItem> Themes { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelStylesheet)]
        public int? CSSId { get; set; }
        public List<SelectListItem> CSS { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelDefaultOrderStatus)]
        public int? DefaultOrderStateId { get; set; }
        public List<SelectListItem> OrderState { get; set; }
        public string DefaultOrderStateName { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelDefaultProductReviewSatus)]
        public int? DefaultProductReviewStateId { get; set; }
        public List<SelectListItem> ProductReviewState { get; set; }
        public string DefaultProductReviewStateName { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelCustomerReviewStatus)]
        public string DefaultReviewStatus { get; set; }
        public List<SelectListItem> DefaultReviewStates { get; set; }
        public string DefaultReviewStatusName { get; set; }

        public List<SelectListItem> WeightUnits { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelDimensionsUnit)]
        public List<SelectListItem> DimensionUnits { get; set; }

        public int LocaleId { get; set; }
        public List<SelectListItem> Locales { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelActiveCurrency)]
        public int? CurrencyTypeId { get; set; }
        public List<SelectListItem> CurrencyTypes { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelCurrencySuffix)]
        public string CurrencySuffix { get; set; }
        public string CurrencyPreview { get; set; }
        public string CurrencyName { get; set; }
        public string CSSName { get; set; }
        public string ThemeName { get; set; }
        public string LocaleDescription { get; set; }

        public DomainListViewModel Domains { get; set; }


        public List<SelectListItem> Countries { get; set; }

        public List<SelectListItem> FedexDropOffTypes { get; set; }


        public string FedExDropoffTypeName { get; set; }

        public List<SelectListItem> PackagingTypes { get; set; }


        public string FedexPackagingTypeName { get; set; }

        public PortalProfileViewModel PortalProfileViewModel { get; set; }

        public bool IsDefaultAnonymousProfileId { get; set; }

        public bool IsDefaultRegisteredProfileId { get; set; }

        public bool RequiresManualApproval { get; set; }

        public string UserType { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LableIsEnableCompare)]
        public bool IsEnableCompare { get; set; }

        public string CompareType { get; set; }

        public int? TabMode { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelIsMultipleCouponAllowedAtCheckout)]
        public bool IsMultipleCouponCodeAllowed { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelIsEnableSinglePageCheckout)]
        public bool? IsEnableSinglePageCheckout { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelEnableSslForSmtp)]
        public bool EnableSslForSmtp { get; set; }
       
        public string LogoNewImage { get; set; }
        public string NoImageNewImage { get; set; }
    
        #endregion


    }
}