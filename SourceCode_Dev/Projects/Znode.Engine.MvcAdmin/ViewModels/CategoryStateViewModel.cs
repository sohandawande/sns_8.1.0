﻿
namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// Category State View Model
    /// </summary>
    public class CategoryStateViewModel : BaseViewModel
    {
        #region Constructor

        /// <summary>
        /// Constructor for Category State View Model
        /// </summary>
        public CategoryStateViewModel()
        {

        } 

        #endregion

        public int CategoryId { get; set; }
        public string Category { get; set; }
    }
}