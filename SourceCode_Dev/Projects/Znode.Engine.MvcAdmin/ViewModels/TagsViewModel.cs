﻿
namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// VIew Model for a list of tags to associate with your product. 
    /// </summary>
    public class TagsViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor For TagsViewModelcs
        /// </summary>
        /// 
        public TagsViewModel()
        { 
        
        }

        public string Tags { get; set; }

        public int TagId { get; set; }
        public int ProductId { get; set; }
        
    }
}