﻿using System.Collections.Generic;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class NotesListViewModel : BaseViewModel
    {
         /// <summary>
        /// Constructor for NotesList
        /// </summary>
        public NotesListViewModel()
        {
            Notes = new List<NotesViewModel>();
        }

        public List<NotesViewModel> Notes { get; set; }
        public int AccountId { get; set; }
    }
}