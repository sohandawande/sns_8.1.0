﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Libraries.Helpers.Attributes;
using Resources;
namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model For Add Alternate Image for Product
    /// </summary>
    public class ProductAlternateImageViewModel : BaseViewModel
    {
        /// <summary>
        /// Construtor for ProductAlternateImageViewModel
        /// </summary>
        /// 
        public ProductAlternateImageViewModel()
        {

        }
        public int ProductImageId { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "RequiredDisplayOrder")]
        [RegularExpression("^(1|[1-9][0-9]*)$", ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "InvalidDisplayOrder")]
        public int? DisplayOrder { get; set; }
        public string Name { get; set; }
        public string ImageAltText { get; set; }
        public string ImageFile { get; set; }
        public string AlternateThumbnailImageFile { get; set; }
        public bool ActiveInd { get; set; }
        public bool ShowOnCategoryPage { get; set; }
        public int? ProductImageTypeID { get; set; }
        public int? ReviewStateID { get; set; }
        public int? PortalId { get; set; }
        public string ReviewStateString { get; set; }
        public SelectImageStatus? SelectedImageStatus { get; set; }
        public bool IsEditMode { get; set; }
        public bool IsInvalidMode { get; set; }
        public List<SelectListItem> ImageTypes { get; set; }

        public string VendorName { get; set; }
        public int? VendorId { get; set; }
        public string ProductSku { get; set; }
        public string ImageTypeName { get; set; }

        public string ImagePath { get; set; }

        [DisplayName("Select Image")]
        [FileTypeValidation(MvcAdminConstants.ImageFileTypes, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "ImageFileTypeErrorMessage")]
        [FileMaxSizeValidation(MvcAdminConstants.ImageMaxFileSize, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "FileSizeExceededErrorMessage")]
        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "RequiredProductImage")]
        [UIHint("FileUploader")]
        public HttpPostedFileBase ProductImage { get; set; }

        public string ImageLargePath { get; set; }
        public string ImageMediumPath { get; set; }
        public string ImageSmallPath { get; set; }
        public string ImageSmallThumbnailPath { get; set; }
        public string ImageThumbnailPath { get; set; }
        public string UserType { get; set; }
    }

   
}

