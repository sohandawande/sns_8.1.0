﻿
using System.Collections.Generic;
namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class VendorProductPreviewViewModel:BaseViewModel
    {
        public VendorProductPreviewViewModel()
        {
            ProductModel = new ProductViewModel();
            YMALProductList = new ProductListViewModel();
            FBTProductList = new ProductListViewModel();
        }
        public ProductViewModel ProductModel { get; set; }
        public IEnumerable<VendorAddOnViewModel> AddOns { get; set; }
        public IEnumerable<ProductAlternateImageViewModel> Images { get; set; }
        public List<ProductAlternateImageViewModel> SwatchImages { get; set; }
        public string YMALProductsIds { get; set; }
        public string FBTProductsIds { get; set; }
        public ProductListViewModel YMALProductList { get; set; }
        public ProductListViewModel FBTProductList { get; set; }
    }

    public class VendorAddOnViewModel : BaseViewModel
    {

        public VendorAddOnViewModel()
        { 
        }
        public string Name { get; set; }
        public string AddOnTitle { get; set; }     
        public string Description { get; set; }   
        public string InStockMessage { get; set; }     
        public string OutOfStockMessage { get; set; }      
        public string BackOrderMessage { get; set; }      
        public string DisplayType { get; set; }
        public string PromptMessage { get; set; }
        public string SKU { get; set; }
        public int? DisplayOrder { get; set; }
        public int AddOnId { get; set; }
        public int OutofStockOptions { get; set; }
        public int AddOnValueId { get; set; }
        public int ProductAddOnId { get; set; }
        public bool AllowBackOrder { get; set; }
        public bool TrackInventory { get; set; }
        public bool InventoryEnabled { get; set; }
        public bool IsOptional { get; set; }
        public bool IsAssociatedAddOnValue { get; set; }
        public string SkuOrProductNum { get; set; }
        public IEnumerable<AddOnValueViewModel> AddOnValues { get; set; }
        public bool OptionalIndicator { get; set; }
        public int[] SelectedAddOnValue { get; set; }
    }
}