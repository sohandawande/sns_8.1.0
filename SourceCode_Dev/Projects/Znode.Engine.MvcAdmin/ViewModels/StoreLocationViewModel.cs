﻿namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model for Store Location 
    /// </summary>
    public class StoreLocationViewModel : BaseViewModel
    {
        /// <summary>
        /// Default Constructor for StoreLocationViewModel
        /// </summary>
        public StoreLocationViewModel()
        {

        }

        public string StoreName { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string PhoneNumber { get; set; }
        public string FaxNumber { get; set; }
        public string ContactName { get; set; }
        public int DisplayOrder { get; set; }

        public int StoreId { get; set; }

        public bool DisplayStore { get; set; }
    }
}