﻿using Resources;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Znode.Libraries.Helpers;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// ViewModel for Manage Content Page
    /// </summary>
    public class ContentPageViewModel : BaseViewModel
    {
        #region Constructor
        /// <summary>
        /// Constructor for Manage Content Page 
        /// </summary>
        public ContentPageViewModel()
        {

        }
        #endregion

        #region Public Properties
        [RegularExpression("^[-_0-9a-zA-Z]+$", ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.revContentPageName)]
        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.RequiredName)]
        [LocalizedDisplayName(Name = RZnodeResources.LabelPageName)]
        [MaxLength(50)]
        public string PageName { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.RequiredPageTitle)]
        [LocalizedDisplayName(Name = RZnodeResources.LabelPageTitle)]
        public string PageTitle { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelSEOTitle)]
        public string SEOTitle { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelSEOKeywords)]
        public string SEOKeywords { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelSEODescription)]
        public string SEODescription { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelAdditionalMetaInformation)]
        public string AdditionalMetaInformation { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelSEOFriendlyPageName)]
        [RegularExpression("([A-Za-z0-9-_]+)", ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = RZnodeResources.revSeoUrl)]
        [MaxLength(100)]
        public string SEOFriendlyPageName { get; set; }
        public string TemplateName { get; set; }
        public string OldHtml { get; set; }
        public string CustomErrorMessage { get; set; }
        public string StoreName { get; set; }
        public string MasterPageName { get; set; }

        [UIHint("RichTextBox")]
        [AllowHtml]
        public string Html { get; set; }
        public string UpdatedUser { get; set; }

        public string MappedSeoUrl { get; set; }
       
        public string PageContent { get; set; }

        public int ContentPageId { get; set; }
        public int LocaleId { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.DisplaySelectStore)]
        public int PortalID { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelSelectTheme)]
        public int? ThemeID { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelCSS)]
        public int? CSSID { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelMasterPageTemplate)]
        public int? MasterPageID { get; set; }

        public bool ActiveInd { get; set; }
        public bool IsUrlRedirectEnabled { get; set; }
        public bool AllowDelete { get; set; }
        public bool IsEditMode { get; set; }

        public List<PortalViewModel> PortalList { get; set; }
        public List<SelectListItem> ThemeList { get; set; }
        public List<SelectListItem> CSSList { get; set; }
        public List<SelectListItem> MasterPageList { get; set; }

        #endregion
    }
}