﻿
using Resources;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Znode.Engine.MvcAdmin.Models;
using Znode.Libraries.Helpers;
namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model For Add New Product Type
    /// </summary>
    public class ProductTypeViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for ProductTypeViewModel
        /// </summary>
        public ProductTypeViewModel()
        {
            reportModel = new ReportModel();
        }

        [MaxLength(100)]
        [LocalizedDisplayName(Name = RZnodeResources.LabelProductTypeName)]
        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "rfvProductTypeName")]
        public string Name { get; set; }
        public string Description { get; set; }
        public string OldProductTypeName { get; set; }
        public string Attributes
        {
            get { return "Attributes"; }
        }

        public int ProductTypeId { get; set; }

        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "rfvDisplayOrder")]
        [Range(1, 999999999, ErrorMessage = "Must be a whole number")]
        [RegularExpression("^(1|[1-9][0-9]*)$", ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "InvalidDisplayOrder")]
        public int DisplayOrder { get; set; }

        public bool IsFranchisable { get; set; }
        public bool IsSuccess { get; set; }

        public bool? IsGiftCard { get; set; }
        
        public ReportModel reportModel { get; set; }

        
    }
}