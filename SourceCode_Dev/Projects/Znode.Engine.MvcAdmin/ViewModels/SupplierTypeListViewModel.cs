﻿using System.Collections.Generic;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View model for Supplier Type List
    /// </summary>
    public class SupplierTypeListViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for Supplier Type List view model
        /// </summary>
        public SupplierTypeListViewModel()
        {
            SupplierType = new List<SupplierTypeViewModel>();
        }
        public List<SupplierTypeViewModel> SupplierType { get; set; }
    }
}