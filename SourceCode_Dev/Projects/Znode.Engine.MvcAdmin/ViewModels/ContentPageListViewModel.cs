﻿using System.Collections.Generic;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class ContentPageListViewModel : BaseViewModel
    {
        public ContentPageListViewModel()
        {
            ContentPages = new List<ContentPageViewModel>();
        }

        public List<ContentPageViewModel> ContentPages { get; set; }
    }
}