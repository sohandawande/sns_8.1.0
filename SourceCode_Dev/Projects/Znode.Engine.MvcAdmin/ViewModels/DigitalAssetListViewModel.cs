﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.MvcAdmin.Models;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class DigitalAssetListViewModel : BaseViewModel
    {
        public DigitalAssetListViewModel()
        {
            this.DigitalAssets = new List<DigitalAssetViewModel>();
        }

        public List<DigitalAssetViewModel> DigitalAssets { get; set; }

        public int ProductId { get; set; }

        public ReportModel GridModel { get; set; }
        public int? PortalId { get; set; }
    }
}