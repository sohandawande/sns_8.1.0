﻿
namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class RoleMenuViewModel : BaseViewModel
    {
        public int MenuId { get; set; }
        public string MenuName { get; set; }
        public int? ParentMenuId { get; set; }
        public string AreaName { get; set; }
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
        public string CSSClassName { get; set; }
        public int StatusId { get; set; }
        public int SequenceNumber { get; set; }
    }
}