﻿using Resources;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Znode.Libraries.Helpers;
using Znode.Libraries.Helpers.Attributes;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model for Promotions
    /// </summary>
    public class PromotionsViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for PromotionsViewModel
        /// </summary>
        public PromotionsViewModel()
        {
            Coupons = new CouponCodeViewModel();
            CouponInd = false;
        }

        [LocalizedDisplayName(Name = RZnodeResources.LabelPromotionName)]
        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "rfvPromotionName")]
        public string Name { get; set; }
        public string UserType { get; set; }
        public string StoreName { get; set; }
        public string Profile { get; set; }
        public string Description { get; set; }
        public bool? CouponInd { get; set; }
        public bool EnableCouponUrl { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelCallForPriceMessage)]
        public string CallForPriceMessage { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelProductSku)]
        public string SKU { get; set; }

        [MaxLength(25)]
        [LocalizedDisplayName(Name = RZnodeResources.LabelPromotionCode)]
        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "rfvPromotionCode")]
        public string PromoCode { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelDiscountProduct)]
        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "rfvProduct")]
        public string DiscountProduct { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelRequiredProduct)]
        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "rfvProduct")]
        public string RequireProduct { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelProduct)]
        public string CallForPriceProduct { get; set; }
        public int PromotionId { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "rfvPromotionDisplayOrder")]
        [Range(1, 999999999, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "rnvDisplayOrder")]
        [RegularExpression("^(1|[1-9][0-9]*)$", ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "InvalidDisplayOrder")]
        [LocalizedDisplayName(Name = RZnodeResources.LabelDisplayOrder)]
        public int? DisplayOrder { get; set; }

        public int PromotionTypeId { get; set; }
        
        [LocalizedDisplayName(Name = RZnodeResources.LabelStartDate)]
        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "rfvPromotionStartDate")]
        public string StartDate { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelEndDate)]
        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "rfvPromotionEndDate")]
        [GenericCompare(CompareToPropertyName = "StartDate", OperatorName = GenericCompareOperator.GreaterThanOrEqual, ErrorMessageResourceName = "CPVStartDateEndDate",
        ErrorMessageResourceType = typeof(ZnodeResources))]
        public string EndDate { get; set; }
        
        public ProductListViewModel ProductList { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelMinimumQuantity)]
        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "rfvMinimumQuantity")]
        [Range(1, 999, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "rnvMinimumQuantity")]
        public int? QuantityMinimum { get; set; }
        public int? PortalId { get; set; }
        public int? ProfileId { get; set; }
        public int? AccountId { get; set; }
        public int? CatalogId { get; set; }
        public int? CategoryId { get; set; }
        public int? SkuId { get; set; }
        public int? DiscountedProductId { get; set; }
        public int? RequiredProductId { get; set; }
        public int? ManufacturerId { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelProductQuantity)]
        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "rfvMinimumQuantity")]
        [Range(1, 999, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "rnvMinimumQuantity")]
        public int? DiscountedProductQuantity { get; set; }
        
        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "rfvDiscountPercent")]
        [Range(0.01, 100.00, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "rnvDiscountPercent")]
        [LocalizedDisplayName(Name = RZnodeResources.LabelDiscountPercent)]
        public decimal? DiscountPercent { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelDiscountAmount)]
        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "rfvDiscountAmount")]
        [Range(0.01, 9999999, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "rnvDiscountAmount")]
        public decimal? DiscountAmount { get; set; }        

        [LocalizedDisplayName(Name = RZnodeResources.LabelMinimumOrderAmount)]
        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "rfvMinimumOrder")]
        public decimal? OrderMinimum { get; set; }
        public string PromotionType { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelStore)]
        public List<SelectListItem> PortalList { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelRequireBrand)]
        public List<SelectListItem> ManufacturerList { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelDiscountType)]
        public List<SelectListItem> PromotionTypeList { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelRequireCatalog)]
        public List<SelectListItem> CatalogList { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelRequireCategory)]
        public List<SelectListItem> CategoryList { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelProfile)]
        public List<SelectListItem> ProfileList { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelPromotionSkuList)]
        public List<SelectListItem> SkuList { get; set; }

        public CouponCodeViewModel Coupons { get; set; }

        public bool IsCouponAllowedWithOtherCoupons { get; set; }
    }
}