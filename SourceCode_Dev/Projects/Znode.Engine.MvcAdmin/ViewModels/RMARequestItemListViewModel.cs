﻿using System.Collections.Generic;
using System.Web.Mvc;
namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class RMARequestItemListViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for RMARequestItemListViewModel
        /// </summary>
        public RMARequestItemListViewModel()
        {
            RMARequestItems = new List<RMARequestItemViewModel>();
            RMARequestItem = new RMARequestItemViewModel();
            ReasonForReturnItems = new List<SelectListItem>();
        }

        public List<RMARequestItemViewModel> RMARequestItems { get; set; }
        public List<SelectListItem> ReasonForReturnItems { get; set; }
        public RMARequestItemViewModel RMARequestItem { get; set; }
        public decimal SubTotal { get; set; }
        public decimal Tax { get; set; }
        public decimal Total { get; set; }
        public string Comments { get; set; }
        public int RMARequestId { get; set; }
        public string RequestNumber { get; set; }
        public string RequestDate { get; set; }
        public int OrderId { get; set; }
        public string flag { get; set; }
        public IssuedGiftCardListViewModel GiftCardsIssued { get; set; }
        public string StoreName { get; set; }
        public string SalesPhoneNumber { get; set; }
        public string CustomerName { get; set; }
        public int StatusId { get; set; }
    }
}