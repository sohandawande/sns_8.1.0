﻿using System.Collections.Generic;
using Znode.Engine.MvcAdmin.Models;
using System.Web.Mvc;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class VendorProductListViewModel : BaseViewModel
    {
        public VendorProductListViewModel()
        {
            Products = new List<ProductViewModel>();
            GridModel = new ReportModel();
            ReviewStates = new ProductReviewStateListViewModel();
        }
        public List<ProductViewModel> Products { get; set; }
        public ReportModel GridModel { get; set; }
        public ProductReviewStateListViewModel ReviewStates { get; set; }

        public string SelectedProductIds { get; set; }
        public string reviewState { get; set; }
        public string SelectedRejectionReason { get; set; }
        public bool IsSingleProduct { get; set; }
        public string VendorName { get; set; }
        public List<SelectListItem> RejectionResons { get; set; }
        public string ProductName { get; set; }
        public string DetailReason { get; set; }
        public string UserName { get; set; }
    }
}