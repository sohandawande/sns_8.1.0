﻿
namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// Field level setting view model
    /// </summary>
    public class FieldLevelSettingViewModel :BaseViewModel
    {
        /// <summary>
        /// Constructor for FieldLevelSettingViewModel
        /// </summary>
        public FieldLevelSettingViewModel()
        {
            
        }
        public int FieldId { get; set; }
        public string PropertyName { get; set; }
        public string Boost { get; set; }
    }
}