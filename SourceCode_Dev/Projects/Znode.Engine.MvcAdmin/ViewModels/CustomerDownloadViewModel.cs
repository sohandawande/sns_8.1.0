﻿using System;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class CustomerDownloadViewModel : BaseViewModel
    {
        public int AccountId { get; set; }
        public Guid? UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string RoleName { get; set; }
        public bool? EnableCustomerPricing { get; set; }
    }
}