﻿using System.Collections.Generic;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class PaymentOptionListViewModel:BaseViewModel
    {
        public List<PaymentOptionViewModel> PaymentOptionList { get; set; }

        public PaymentOptionListViewModel()
        {
            PaymentOptionList = new List<PaymentOptionViewModel>();
        }
    }
}