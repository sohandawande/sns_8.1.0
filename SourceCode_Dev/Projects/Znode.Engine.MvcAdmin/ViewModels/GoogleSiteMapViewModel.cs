﻿using Resources;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.WebPages.Html;
using Znode.Engine.Api.Models;
using Znode.Libraries.Helpers;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model for Google Site Map
    /// </summary>
    public class GoogleSiteMapViewModel : BaseViewModel
    {
        #region Constructor

        /// <summary>
        /// Constructor for Google Site Map View Model
        /// </summary>
        public GoogleSiteMapViewModel()
        {

        } 

        #endregion

        public GoogleSiteMapModel GoogleSiteMap { get; set; }

        public int GoogleSiteMapId { get; set; }
        
        [Required]
        public int[] StoreList { get; set; }

        public string Frequency { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelLastModificationDate)]
        public string LastModification { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelXMLSiteMapType)]
        public string XMLSiteMap { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelXMLSiteMap)]
        public string XMLSiteMapType { get; set; }       
        public string Date { get; set; }
        public string SuccessXMLGenerationMessage { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.RequiredXMLFileName)]
        
        [LocalizedDisplayName(Name = RZnodeResources.LabelXMLFileName)]
        [RegularExpression("([A-Za-z0-9-_]+)", ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "revXmlFileName")]
        public string XMLFileName { get; set; }
        public ProfilePortalViewModel Stores { get; set; }

        public decimal Priority { get; set; }      

        public List<SelectListItem> FrequencyList { get; set; }
        public List<SelectListItem> LastModificationList { get; set; }
        public List<SelectListItem> PriorityList { get; set; }
        public List<SelectListItem> XMLSiteMapList { get; set; }
        public List<SelectListItem> XMLSiteMapTypeList { get; set; }

        public string ErrorMessage { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.GoogleSiteMapFeedTitle)]
        public string GoogleSiteMapFeedTitle { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.GoogleSiteMapFeedLink)]
        public string GoogleSiteMapFeedLink { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.GoogleSiteMapFeedDescription)]
        public string GoogleSiteMapFeedDescription { get; set; }
    }
}