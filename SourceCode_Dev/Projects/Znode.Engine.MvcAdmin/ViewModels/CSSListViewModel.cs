﻿using System.Collections.Generic;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class CSSListViewModel : BaseViewModel
    {
        public CSSListViewModel()
        {
            CSSList = new List<CSSViewModel>();
        }

        public List<CSSViewModel> CSSList { get; set; }
    }
}