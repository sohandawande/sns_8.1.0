﻿using System.Collections.Generic;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// The List view model for Store Admins
    /// </summary>
    public class StoreAdminListViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for Vendor Account
        /// </summary>
        public StoreAdminListViewModel()
        {
            StoreAdmins = new List<StoreAdminViewModel>();
        }

        public List<StoreAdminViewModel> StoreAdmins { get; set; }
    }
}