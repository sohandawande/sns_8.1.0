﻿using System.Collections.Generic;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class CategoryProfileListViewModel:BaseViewModel
    {
        public CategoryProfileListViewModel()
        {
            CategoryProfiles = new List<CategoryProfileViewModel>();
        }

        public List<CategoryProfileViewModel> CategoryProfiles { get; set; }

    }
}