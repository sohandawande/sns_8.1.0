﻿
using Resources;
using Znode.Libraries.Helpers;
namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class CustomerBasedPricingViewModel : BaseViewModel
    {
        public int ProductId { get; set; }
        public int AccountId { get; set; }
        public string ExternalAccountNo { get; set; }
        public string CompanyName { get; set; }
        public string FullName { get; set; }

        public string BasePrice { get; set; }
        public string Discount { get; set; }
        public string NegotiatedPrice { get; set; }
    }
}