﻿namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class PaymentSettingViewModel
    {
        public bool? EnableAmericanExpress { get; set; }
        public bool? EnableDiscover { get; set; }
        public bool? EnableMasterCard { get; set; }
        public bool? EnableRecurringPayments { get; set; }
        public bool? EnableVault { get; set; }
        public bool? EnableVisa { get; set; }
        public string Partner { get; set; }
        public string PaymentGatewayPassword { get; set; }
        public string PaymentGatewayUsername { get; set; }
        public bool PreAuthorize { get; set; }
        public bool TestMode { get; set; }
        public string TransactionKey { get; set; }
        public string Vendor { get; set; }
    }
}