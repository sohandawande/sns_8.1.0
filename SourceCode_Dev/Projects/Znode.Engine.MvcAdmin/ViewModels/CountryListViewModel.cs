﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.MvcAdmin.Models;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class CountryListViewModel:BaseViewModel
    {
        public CountryListViewModel()
        {
            Countries = new List<CountryViewModel>();
        }
        public List<CountryViewModel> Countries { get; set; }

        public int PortalId { get; set; }

        public ReportModel Grid { get; set; }
    }
}