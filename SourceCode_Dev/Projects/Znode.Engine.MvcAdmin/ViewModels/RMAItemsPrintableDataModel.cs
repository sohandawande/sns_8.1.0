﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class RMAItemsPrintableDataModel : BaseViewModel
    {
        public string StoreName { get; set; }
        public string SalesPhoneNumber { get; set; }
        public string RequestNumber { get; set; }
        public int OrderId { get; set; }
    }
}