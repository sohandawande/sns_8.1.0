﻿using Resources;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Models;
using Znode.Libraries.Helpers;
using Znode.Libraries.Helpers.Attributes;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model For ProductViewModel
    /// </summary>
    public class ProductViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for ProductViewModel
        /// </summary>
        public ProductViewModel()
        {
            GridModel = new ReportModel();
        }

        public string BundleItemsIds { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelShortDescription)]
        public string ShortDescription { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelLongDescription)]
        [UIHint("RichTextBox")]
        [AllowHtml]
        public string LongDescription { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelFeatureDescription)]
        [UIHint("RichTextBox")]
        [AllowHtml]
        public string FeaturesDescription { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelProductSpecification)]
        [UIHint("RichTextBox")]
        [AllowHtml]
        public string ProductSpecifications { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelShippingInfo)]
        [UIHint("RichTextBox")]
        [AllowHtml]
        public string ShippingInformation { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelImageAltText)]
        public string ImageAltTag { get; set; }
        public string ImageFile { get; set; }
        public string ImageLargePath { get; set; }
        public string ImageMediumPath { get; set; }
        public string ImageSmallThumbnailPath { get; set; }
        public SelectImageStatus? SelectedImageStatus { get; set; }
        public bool IsEditMode { get; set; }
        public bool IsInvalidMode { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelBrandName)]
        public string BrandName { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelSupplierName)]
        public string SupplierName { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelTaxClassName)]
        public string TaxClassName { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelShippingRule)]
        public string ShippingRule { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.TextProductTypeName)]
        public string ProductTypeName { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelProductAttribute)]
        public string ProductAttributes { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LableProductName)]
        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "ErrorRequired")]
        [RegularExpression(@"^[^|]+$", ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = RZnodeResources.revUnallowedCharacters)]
        public string Name { get; set; }

        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "ErrorRequired")]
        [LocalizedDisplayName(Name = RZnodeResources.LabelSkuPart)]
        public string Sku { get; set; }
        public string InventoryMessage { get; set; }
        public string SeoDescription { get; set; }
        public string SeoKeywords { get; set; }
        public string SeoTitle { get; set; }

        [RegularExpression("^[A-Za-z0-9-_]+$", ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "ValidSEOUrl")]
        public string SeoPageUrl { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelDownloadLink)]
        public string DownloadLink { get; set; }
        public string FBTProductsIds { get; set; }
        public string YMALProductsIds { get; set; }
        public string VendorName { get; set; }
        public int? PortalID { get; set; }
        public int? VendorId { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelProductCode)]
        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "ErrorRequired")]
        public string ProductCode { get; set; }

        public int ProductId { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelProductType)]
        public int ProductTypeId { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.TitleBrand)]
        public int? BrandId { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelTaxClass)]
        public int? TaxClassId { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelSupplier)]
        public int? SupplierId { get; set; }
        public int SelectedQuantity { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelShippingCost)]
        public int? ShippingRuleTypeId { get; set; }
        public int Rating { get; set; }
        public int WishListId { get; set; }
        public int? ReviewStateId { get; set; }
        public string ReviewStateString { get; set; }
        public int AttributeCount { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelDisplayOrder)]
        [Range(1, 999999999, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "rnvDisplayOrder")]
        [RegularExpression("^(1|[1-9][0-9]*)$", ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "InvalidDisplayOrder")]
        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "ErrorRequired")]
        public int? DisplayOrder { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelQuantityOnHand)]
        [Range(0, 999999.00, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "ErrorAddOnNumberRange")]
        public int? QuantityOnHand { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelReOrderLevel)]
        [Range(0, 999999, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "ErrorAddOnNumberRange")]
        public int? ReorderLevel { get; set; }

        public int SkuId { get; set; }

        [Range(1, 9999, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "rnvExpirationPeriod")]
        [RegularExpression("^(1|[1-9][0-9]*)$", ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "rnvExpirationPeriod")]
        [LocalizedDisplayName(Name = RZnodeResources.LabelExpirationPeriod)]
        public int? ExpirationPeriod { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelExpirationFrequency)]
        public int? ExpirationFrequency { get; set; }
        public string ExpirationPeriodFrequency { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelMaxSelectableQuantity)]
        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "ErrorRequired")]
        [GenericCompare(CompareToPropertyName = "MinQuantity", OperatorName = GenericCompareOperator.GreaterThanOrEqual, ErrorMessageResourceName = "CPVMaxMinQuantity", ErrorMessageResourceType = typeof(ZnodeResources))]
        public int? MaxQuantity { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelMinSelectableQuantity)]
        [Range(1, 999999, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "rnvMinimumSelectedQuantity")]
        public int? MinQuantity { get; set; }

        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "ErrorRequired")]
        [LocalizedDisplayName(Name = RZnodeResources.LabelRetailPrice)]
        [RegularExpression(@"^\d{0,}(\.\d{1,2})?$", ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.revRetailPrice)]
        [Range(0, 999999, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.rnvRetailPrice)]
        public decimal? RetailPrice { get; set; }

        public string DisplayRetailPrice { get; set; }
        public string DisplaySalePrice { get; set; }
        public string DisplayWholeSalePrice { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelSalePrice)]
        [RegularExpression(@"^\d{0,}(\.\d{1,2})?$", ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.revSalesPrice)]
        [Range(0, 999999, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.rnvSalesPrice)]
        public decimal? SalePrice { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelWholesalePrice)]
        [RegularExpression(@"^\d{0,}(\.\d{1,2})?$", ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "revWholesalePrice")]
        [Range(0, 999999, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "rnvWholesalePrice")]
        public decimal? WholeSalePrice { get; set; }

        [DataType(DataType.Currency)]
        [Range(0.0, 9999999.0, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.rnvWeight)]
        public decimal? Weight { get; set; }

        [DataType(DataType.Currency)]
        [Range(0.0, 99999999.0, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.rnvHeight)]
        public decimal? Height { get; set; }

        [DataType(DataType.Currency)]
        [Range(0.0, 99999999.0, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.rnvWidth)]
        public decimal? Width { get; set; }

        [DataType(DataType.Currency)]
        [Range(0.0, 99999999.0, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.rnvLength)]
        public decimal? Length { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelShippingRate)]
        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = RZnodeResources.rfvShippingRate)]
        public decimal? ShippingRate { get; set; }
        public bool IsActive { get; set; }
        public bool ShowAddToCart { get; set; }
        public bool ShowWishlist { get; set; }
        public bool ShowQuantity { get; set; }
        public bool IsCallForPricing { get; set; }
        public bool IsGiftCard { get; set; }
        public bool? TrackInventory { get; set; }
        public bool? AllowBackOrder { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelFreeShipping)]
        public bool? FreeShipping { get; set; }

        public bool RecurringBillingInd { get; set; }
        public bool HomepageSpecial { get; set; }
        public bool? NewProductInd { get; set; }
        public bool FeaturedInd { get; set; }
        public bool Franchisable { get; set; }
        public string InStockMsg { get; set; }
        public string OutOfStockMsg { get; set; }
        public string BackOrderMsg { get; set; }

        [Range(0.00, 999999.99, ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = RZnodeResources.rnvRecurringBillingInitialAmount)]
        [LocalizedDisplayName(Name = RZnodeResources.LabelRecurringBillingInitialAmount)]
        public decimal? RecurringBillingInitialAmount { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelRecurringBillingFrequency)]
        public string RecurringBillingFrequency { get; set; }

        public bool DisablePurchasingForOutOfStockProducts { get; set; }
        public bool AllowBackOrderingOfProducts { get; set; }
        public bool DontTrackInventory { get; set; }

        public bool InventorySettingsType { get; set; }
        public bool RedirectUrlInd { get; set; }

        public byte InventoryDisplay { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelProductAffiliateURL)]
        [RegularExpression(@"^(http\:\/\/[a-zA-Z0-9_\-/]+(?:\.[a-zA-Z0-9_\-/]+)*)$", ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "RegularValidAffiliateUrl")]
        public string AffiliateUrl { get; set; }

        public bool? ParentProduct { get; set; }
        public bool? IsParentProduct { get; set; }
        public int? CategoryId { get; set; }
        public int? AccountID { get; set; }
        public string CategoryIds { get; set; }

        [FileTypeValidation(MvcAdminConstants.ImageFileTypes, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "ImageFileTypeErrorMessage")]
        [FileMaxSizeValidation(MvcAdminConstants.ImageMaxFileSize, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "FileSizeExceededErrorMessage")]
        [UIHint("FileUploader")]
        public HttpPostedFileBase ProductImage { get; set; }

        public List<SelectListItem> ProductTypeList { get; set; }
        public List<SelectListItem> ManufacturerList { get; set; }
        public List<SelectListItem> SupplierList { get; set; }
        public List<SelectListItem> TaxClassList { get; set; }
        public List<SelectListItem> ShippingRuleTypeList { get; set; }
        public List<SelectListItem> ExpirationFrequencyList { get; set; }
        public List<SelectListItem> CatalogList { get; set; }
        public InventoryOptionsState? SelectedInventoryOptionsState { get; set; }
        public decimal? NegotiatedPrice { get; set; }
        public string Price { get; set; }
        public decimal ProductPrice { get; set; }
        public Dictionary<string, Dictionary<string, string>> ProductAttributesCollection { get; set; }
        public Dictionary<string, Dictionary<string, string>> ProductAddOns { get; set; }
        public Dictionary<string, Dictionary<string, Dictionary<string, string>>> BundleProductDict { get; set; }
        public Dictionary<string, List<SelectListItem>> dropDownsList { get; set; }
        public string Total { get; set; }
        public int Quantity { get; set; }
        public string SelectedAttributeValues { get; set; }
        public string SelectedAttributeNames { get; set; }
        public IEnumerable<AddOnViewModel> AddOns { get; set; }
        public ProductAttributesViewModel ProductsAttributes { get; set; }
        public decimal OriginalPrice { get; set; }
        public string CategoryName { get; set; }
        public string OutOfStockMessage { get; set; }
        public string BackOrderMessage { get; set; }
        public string InStockMessage { get; set; }
        public string Description { get; set; }
        public string SeoPageName { get; set; }
        public IEnumerable<ImageViewModel> Images { get; set; }
        public string ProductSpecification { get; set; }
        public decimal UnitPrice { get; set; }
        public bool? EnableCustomerPricing { get; set; }
        public string UserType { get; set; }
        public ReportModel GridModel { get; set; }
        public string ReviewHistoryDescription { get; set; }

        
        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = RZnodeResources.rfvSalesPrice)]
        [RegularExpression(@"^\d{0,}(\.\d{1,2})?$", ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.revSalesPrice)]
        [Range(0, 999999, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = RZnodeResources.rnvSalesPrice)]
        public decimal? VendorRetailPrice { get; set; }

        public string ProductIds { get; set; }

        //PRFT Custom Code: Start
        public string Custom3 { get; set; }
        //PRFT Custom COde : End
    }
}