﻿using Resources;
using System;
using System.ComponentModel.DataAnnotations;
using Znode.Libraries.Helpers;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// The View Model for the Store Admin
    /// </summary>
    public class StoreAdminViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for StoreAdminViewModel
        /// </summary>
        public StoreAdminViewModel()
        { 
            
        }

        [LocalizedDisplayName(Name = RZnodeResources.StoreAdminUserID)]
        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "ErrorRequired")]
        public string UserID { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.StoreAdminEmailAddress)]
        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "ErrorRequired")]
        [EmailAddress(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "ValidEmailAddress", ErrorMessage = "")]
        public string Email { get; set; }

        public string Description { get { return ZnodeResources.SiteAdminAccountDescription; } }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        //PRFT Custom Code: Start
        public string MiddleName { get; set; } // Used For Sales Rep Location
        //PRFT Custom Code: End
        public string CompanyName { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelLoginName)]
        public string LoginName { get; set; }
        
        [LocalizedDisplayName(Name = RZnodeResources.DisplayAccountNo)]
        public string AccountNumber { get; set; }        
        
        [LocalizedDisplayName(Name = RZnodeResources.DisplayCreateUser)]
        public string CreateUser { get; set; }
        
        [LocalizedDisplayName(Name = RZnodeResources.DisplayUpdateUser)]
        public string UpdateUser { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelCreateDate)]
        public string StartDate { get; set; }
        
        [LocalizedDisplayName(Name = RZnodeResources.LabelUpdateDate)]
        public string EndDate { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.DisplayAccountNo)]
        public int? AccountId { get; set; }

        public Guid? UserGuid { get; set; }
        [LocalizedDisplayName(Name = RZnodeResources.LabelIsEmailOptional)]
        public bool EmailOptIn { get; set; }
        public bool IsConfirmed { get; set; }
    }
}