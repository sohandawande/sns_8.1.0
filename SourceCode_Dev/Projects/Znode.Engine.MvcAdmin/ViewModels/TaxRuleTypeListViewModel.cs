﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// List View Model for Tax Rule Type
    /// </summary>
    public class TaxRuleTypeListViewModel : BaseViewModel
    {
        #region Constructor for Tax Rule Type List View Model
        /// <summary>
        /// Constructor for Tax Rule Type List View Model
        /// </summary>
        public TaxRuleTypeListViewModel()
        {
            TaxRuleType = new List<TaxRuleTypeViewModels>();
        } 
        #endregion

        public List<TaxRuleTypeViewModels> TaxRuleType { get; set; }               
    }
}
   
 