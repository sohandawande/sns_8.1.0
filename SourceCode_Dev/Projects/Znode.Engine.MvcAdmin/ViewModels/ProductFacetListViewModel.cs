﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.MvcAdmin.Models;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class ProductFacetListViewModel : BaseViewModel
    {
        public ProductFacetListViewModel()
        {
            Facets = new List<ProductFacetGroupsViewModel>();
            GridModel = new ReportModel();
        }

        public List<ProductFacetGroupsViewModel> Facets { get; set; }
        public ReportModel GridModel { get; set; }
        public int ProductId { get; set; }
        public int? SKUId { get; set; }
    }

    public class ProductFacetGroupsViewModel : BaseViewModel
    {
        public int FacetGroupID { get; set; }
        public string FacetGroupLabel { get; set; }
        public int? CatalogID { get; set; }
        public string FacetName { get; set; }
        public int? SKUId { get; set; }
        public int? ProductId { get; set; }
    }
   
}