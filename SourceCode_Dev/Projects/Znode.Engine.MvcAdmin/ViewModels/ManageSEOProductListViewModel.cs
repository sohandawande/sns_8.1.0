﻿using Znode.Engine.MvcAdmin.Models;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// List view model for Manage SEO Product List
    /// </summary>
    public class ManageSEOProductListViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for Manage SEO Product List
        /// </summary>
        public ManageSEOProductListViewModel()
        {
            GridModel = new ReportModel();
            ProductList = new ProductListViewModel();
        }
        public ReportModel GridModel { get; set; }
        public ProductListViewModel ProductList { get; set; }
    }
}