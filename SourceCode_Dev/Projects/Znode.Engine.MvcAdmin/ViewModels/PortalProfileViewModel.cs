﻿using Resources;
using System.Collections.Generic;
using System.Web.Mvc;
using Znode.Libraries.Helpers;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class PortalProfileViewModel:BaseViewModel
    {
        public int PortalProfileID { get; set; }
        public int PortalID { get; set; }
         [LocalizedDisplayName(Name = RZnodeResources.LabelSelectProfile)]
        public int ProfileID { get; set; }
        public List<SelectListItem> Profiles { get; set; }
        public bool IsCurrentAnonymousProfile { get; set; }
        public bool IsCurrentRegisteredProfile { get; set; }
        public string Name { get; set; }
        public int SerialNumber { get; set; }
    }
}