﻿
using Resources;
using System.ComponentModel.DataAnnotations;
using Znode.Libraries.Helpers;
namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class RequestStatusViewModel:BaseViewModel
    {
        public int RequestStatusID { get; set; }

        [MaxLength(250)]
        public string Name { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelCustomerMessage)]
        public string CustomerNotification { get; set; }

        [LocalizedDisplayName(Name = RZnodeResources.LabelAdminNotification)]
        public string AdminNotification { get; set; }

        public bool? IsEnabled { get; set; }
    }
}