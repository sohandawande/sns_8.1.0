﻿using System;
using System.Collections.Generic;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// This view model will be used for showing the list of Stores
    /// </summary>
    public class ProfilePortalViewModel : BaseViewModel
    {
        /// <summary>
        /// Default constructor
        /// </summary>
        public ProfilePortalViewModel() 
        {
            
        }

        public List<Tuple<string, int, bool>> PortalList { get; set; }
    }
}