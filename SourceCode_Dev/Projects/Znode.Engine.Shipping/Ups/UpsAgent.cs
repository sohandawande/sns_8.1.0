using System;
using System.Configuration;
using System.Net;
using System.Text;
using System.Xml;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.DataAccess.Service;
using System.Collections.Generic;
using System.Linq;

namespace Znode.Engine.Shipping.Ups
{
    public class UpsAgent : ZNodeBusinessBase
    {
        public string ErrorCode { get; set; }
        public string ErrorDescription { get; set; }
        public decimal PackageHeight { get; set; }
        public decimal PackageLength { get; set; }
        public string PackageTypeCode { get; set; }
        public decimal PackageWeight { get; set; }
        public decimal PackageWidth { get; set; }
        public string PickupType { get; set; }
        public string ShipperCountryCode { get; set; }
        public string ShipperZipCode { get; set; }
        public string ShipToAddressType { get; set; }
        public string ShipToCountryCode { get; set; }
        public string ShipToZipCode { get; set; }
        public string ShipToCity { get; set; }
        public string ShipStateProvinceCode { get; set; }
        public string UpsGatewayUrl { get; set; }
        public string UpsAddressValidationUrl { get; set; }
        public string UpsKey { get; set; }
        public string UpsPassword { get; set; }
        public string UpsServiceCode { get; set; }
        public string UpsUserId { get; set; }
        public string WeightUnit { get; set; }

        //PRFT Custom Properties:Start
        public string ShipperStateProvinceCode { get; set; }
        public string ShipperAccountNumber { get; set; }
        public string UPSStreetAddressValidationURL { get; set; }
        public string ShipToAddressLine1 { get; set; }
        public string ShipToAddressLine2 { get; set; }
        public string CustomerExternalId { get; set; }

        //PRFT Custom Properties:End

        public UpsAgent()
        {
            // Get gateway URL and set other defaults
            UpsGatewayUrl = ConfigurationManager.AppSettings["UPSGatewayURL"];
            UpsAddressValidationUrl = ConfigurationManager.AppSettings["UPSAddressValidationURL"];
            UPSStreetAddressValidationURL = ConfigurationManager.AppSettings["UPSStreetAddressValidationURL"]; //PRFT Custom Code
            PickupType = "One Time Pickup";
            ShipToAddressType = "Residential";
            WeightUnit = "LBS";
        }

        public decimal GetShippingRate()
        {
            //PRFT Custom Code:Start
            //// The 0 is the commercial address type
            //var addressType = ShipToAddressType == "Residential" ? "1" : "0"; //Existing znode code

            // Build the payload for sending to UPS
            var payload = new StringBuilder();

            //string isAddressValid = GetAddressValidation(); //Znode existing code

            string isAddressValid = "0";
            if (ConfigurationManager.AppSettings["UseUPSStreetAddressValidation"].Equals("true"))
            {
                isAddressValid = GetStreetLevelAddressValidation(); 
            }
            else
            {
                isAddressValid = GetAddressValidation();
            }

            //Moved at bottom of address validation call as its value may be changed by UPS address validation method call.
            // The 0 is the commercial address type
            var addressType = ShipToAddressType == "Residential" ? "1" : "0";
            if (addressType.Equals("1"))
            {
                PickupType = "01"; //Daily Pickup
            }

                //PRFT Custom Code:End

                payload.Append("<?xml version='1.0'?>");
            payload.Append("<AccessRequest xml:lang='en-US'>");
            payload.Append("	<AccessLicenseNumber>" + UpsKey + "</AccessLicenseNumber>");
            payload.Append("	<UserId>" + UpsUserId + "</UserId>");
            payload.Append("	<Password>" + UpsPassword + "</Password>");
            payload.Append("</AccessRequest>");
            payload.Append("<?xml version='1.0'?>");
            payload.Append("<RatingServiceSelectionRequest xml:lang='en-US'>");
            payload.Append("	<Request>");
            payload.Append("		<TransactionReference>");
            payload.Append("			<CustomerContext>Rating and Service</CustomerContext>");
            payload.Append("			<XpciVersion>1.0001</XpciVersion>");
            payload.Append("		</TransactionReference>");
            payload.Append("		<RequestAction>Rate</RequestAction>");
            payload.Append("		<RequestOption>Rate</RequestOption>");
            payload.Append("	</Request>");            
            payload.Append("	<Shipment>");
            payload.Append("		<Shipper>");
            payload.Append("         <ShipperNumber>"+ShipperAccountNumber+"</ShipperNumber>"); //PRFT Custom code
            payload.Append("			<Address>");
            payload.Append("             <StateProvinceCode>"+ShipperStateProvinceCode+"</StateProvinceCode>"); //PRFT Custom code 
            payload.Append("				<PostalCode>" + ShipperZipCode + "</PostalCode>");
            payload.Append("				<CountryCode>" + ShipperCountryCode + "</CountryCode>");
            payload.Append("			</Address>");
            payload.Append("		</Shipper>");
            payload.Append("		<ShipTo>");
            payload.Append("			<Address>");
            payload.Append("             <StateProvinceCode>"+ ShipStateProvinceCode + "</StateProvinceCode>"); //PRFT Custom code 
            payload.Append("				<PostalCode>" + ShipToZipCode + "</PostalCode>");
            payload.Append("				<CountryCode>" + ShipToCountryCode + "</CountryCode>");
            //payload.Append("				<ResidentialAddress>" + addressType + "</ResidentialAddress>");
            if (addressType.Equals("1"))
            {
                payload.Append("				<ResidentialAddressIndicator/>");
                
            }
            payload.Append("			</Address>");
            payload.Append("		</ShipTo>");

            //PRFT Code:Start
            payload.AppendLine("<ShipFrom>");
            payload.AppendLine("    <Address>");
            payload.AppendLine("        <StateProvinceCode>"+ShipperStateProvinceCode+"</StateProvinceCode>");
            payload.AppendLine("        <PostalCode>" + ShipperZipCode + "</PostalCode>");
            payload.AppendLine("        <CountryCode>" +ShipperCountryCode + "</CountryCode>");
            payload.AppendLine("    </Address>");
            payload.AppendLine("</ShipFrom>");
            //PRFT Code:end
            payload.Append("		<Service>");
            payload.Append("			<Code>" + UpsServiceCode + "</Code>");
            payload.Append("		</Service>");
            payload.Append("		<Package>");
            payload.Append("			<PackagingType>");
            payload.Append("				<Code>" + PackageTypeCode + "</Code>");
            payload.Append("			</PackagingType>");
            payload.Append("			<Dimensions>");
            payload.Append("				<UnitOfMeasurement>");
            payload.Append("					<Code>IN</Code>");
            payload.Append("				</UnitOfMeasurement>");
            payload.Append("				<Length>" + PackageLength + "</Length>");
            payload.Append("				<Width>" + PackageWidth + "</Width>");
            payload.Append("				<Height>" + PackageHeight + "</Height>");
            payload.Append("			</Dimensions>");
            payload.Append("			<PackageWeight>");
            payload.Append("				<UnitOfMeasurement>");
            payload.Append("					<Code>" + WeightUnit + "</Code>");
            payload.Append("				</UnitOfMeasurement>");
            payload.Append("				<Weight>" + PackageWeight + "</Weight>");
            payload.Append("			</PackageWeight>");
            payload.Append("		</Package>");
            payload.AppendLine("<RateInformation><NegotiatedRatesIndicator/></RateInformation>"); //PRFT Custom code
            payload.Append("	</Shipment>");
            payload.Append("</RatingServiceSelectionRequest>");

            //Log the Request
            ZNodeLogging.LogMessage("RatingRequest: " + payload.ToString());

            try
            {
                decimal shippingRate = 0;

                if (Equals(isAddressValid, "0"))
                {
                    // Get response error code and description
                    ErrorCode = "1";
                    ErrorDescription = "Invalid Address";
                    return shippingRate;
                }
                else
                {

                    // Instantiate the request
                    var request = new WebClient();
                    request.Headers.Add("Content-Type", "application/x-www-form-urlencoded");

                    // Convert the payload into byte array and send the request to UPS
                    var requestBytes = Encoding.ASCII.GetBytes(payload.ToString());
                    var responseBytes = request.UploadData(UpsGatewayUrl, "POST", requestBytes);

                    // Decode the response bytes into an XML string and create an XML document from it
                    var xmlResponse = Encoding.ASCII.GetString(responseBytes);
                    var xmlDocument = new XmlDocument();

                    ZNodeLogging.LogMessage("RatingResponse: " + xmlResponse);
                    

                    xmlDocument.LoadXml(xmlResponse);

                    var responseCodesNodes = xmlDocument.SelectNodes("RatingServiceSelectionResponse/Response/ResponseStatusCode");
                    if (responseCodesNodes != null && responseCodesNodes.Count > 0 && responseCodesNodes[0].InnerText.Equals("1"))
                    {
                        // Get response charges

                        //PRFT Custom Code: Get the actual rate or negociated rate based on the web.config settings
                        bool calculateNegotiatedRate = false;
                        bool.TryParse(Convert.ToString(ConfigurationManager.AppSettings["IsUPSNegotiatedRate"]), out calculateNegotiatedRate);
                        if (calculateNegotiatedRate)
                        {
                            var negociatedChargesRate = xmlDocument.SelectNodes("RatingServiceSelectionResponse/RatedShipment/NegotiatedRates/NetSummaryCharges/GrandTotal/MonetaryValue");
                            if (negociatedChargesRate != null && negociatedChargesRate.Count > 0)
                            {
                                ErrorCode = "0";
                                shippingRate = Decimal.Parse(negociatedChargesRate[0].InnerText);
                            }
                        }
                        else
                        {
                            var chargesNodes = xmlDocument.SelectNodes("RatingServiceSelectionResponse/RatedShipment/TotalCharges/MonetaryValue");
                            if (chargesNodes != null && chargesNodes.Count > 0)
                            {
                                ErrorCode = "0";
                                shippingRate = Decimal.Parse(chargesNodes[0].InnerText);
                            }
                        }
                    }
                    else
                    {
                        // Get response error code and description
                        ErrorCode = xmlDocument.SelectNodes("RatingServiceSelectionResponse/Response/Error/ErrorCode")[0].InnerText;
                        ErrorDescription = xmlDocument.SelectNodes("RatingServiceSelectionResponse/Response/Error/ErrorDescription")[0].InnerText;
                    }

                    //PRFT Custom Code:Start
                    if (shippingRate > 0)
                    {
                        shippingRate += GetIncreasedShippingAmount(shippingRate, ShipStateProvinceCode);
                        shippingRate += GetUPSShippingOverchargeAmount(shippingRate);
                    }
                    else
                    {
                        //Log the response
                        ZNodeLogging.LogMessage("RatingResponse: " + xmlResponse);
                    }
                    
                    //PRFT Custom Code:End
                    return shippingRate;
                }
            }
            catch (Exception ex)
            {
                ErrorCode = "Connection failed.";
                ErrorDescription = "Error while trying to connect with host server. Please try again.";
                ZNodeLogging.LogMessage("UPSAgent-GetShippingRate(): General Connection Error " + ex.ToString());
                return 0;
            }
        }

        private decimal GetUPSShippingOverchargeAmount(decimal shippingRate)
        {
            decimal shippingOverchargeAmount = 0;
            bool isGenericCustomer = (string.IsNullOrEmpty(CustomerExternalId))?true : ConfigurationManager.AppSettings["GenericUserExternalID"].Trim().Equals(CustomerExternalId.Trim(), StringComparison.OrdinalIgnoreCase);
            MessageConfigService messageConfigService = new MessageConfigService();
            //Get the Comma Separated shipping overcharge methods.
            var messageConfigList = messageConfigService.GetByKeyPortalIDLocaleIDMessageTypeID("UPSShippingOverchargeMethods", ZNodeConfigManager.SiteConfig.PortalID, 43, 1);
            string commaSeparatedShippingMethodList = string.Empty;
            if (messageConfigList!=null && messageConfigList.Count>0 && !string.IsNullOrEmpty(messageConfigList[0].Value))
            {
                commaSeparatedShippingMethodList = messageConfigList[0].Value;
            }

            bool isShippingMethodBelongToOvercharge = false;
            List<string> upsShippingOverchargeMethodList = new List<string>(commaSeparatedShippingMethodList.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries));
            isShippingMethodBelongToOvercharge = upsShippingOverchargeMethodList.Any(x => x.Trim().Equals(UpsServiceCode.Trim(), StringComparison.OrdinalIgnoreCase));

            if (isGenericCustomer && isShippingMethodBelongToOvercharge)
            {
                messageConfigList = messageConfigService.GetByKeyPortalIDLocaleIDMessageTypeID("UPSShippingOverchargePercentage", ZNodeConfigManager.SiteConfig.PortalID, 43, 1);
                decimal upsShippingOverchargePercentage = 0;
                if (messageConfigList != null && messageConfigList.Count > 0 && !string.IsNullOrEmpty(messageConfigList[0].Value))
                {
                    decimal.TryParse(messageConfigList[0].Value, out upsShippingOverchargePercentage);
                }
                if(upsShippingOverchargePercentage > 0)
                {
                    shippingOverchargeAmount = (shippingRate * upsShippingOverchargePercentage) / 100;
                }
            }
            return shippingOverchargeAmount;
        }

        /// <summary>
        /// To perform UPS address validation service.
        /// </summary>
        /// <returns>Error 1 / 0 if address is invalid then return 0 otherwise 1.</returns>
        private string GetAddressValidation()
        {
            // Instantiate the request
            var addressRequest = new WebClient();
            addressRequest.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
             
            var addressResponse = new StringBuilder();

            addressResponse.Append("<?xml version='1.0'?>");
            addressResponse.Append("<AccessRequest>");
            addressResponse.Append("	<AccessLicenseNumber>" + UpsKey + "</AccessLicenseNumber>");
            addressResponse.Append("	<UserId>" + UpsUserId + "</UserId>");
            addressResponse.Append("	<Password>" + UpsPassword + "</Password>");           
            addressResponse.Append("</AccessRequest>");
            addressResponse.Append("<?xml version='1.0'?>");
            addressResponse.Append("<AddressValidationRequest xml:lang='en-US'>");
            addressResponse.Append("   <Request>");
            addressResponse.Append("        <TransactionReference>");
            addressResponse.Append("           <CustomerContext>Address Validation</CustomerContext>");
            addressResponse.Append("           <XpciVersion>1.0001</XpciVersion>");
            addressResponse.Append("        </TransactionReference>");
            addressResponse.Append("   <RequestAction>AV</RequestAction>");
            addressResponse.Append("   </Request>");
            addressResponse.Append("       <Address>");
            addressResponse.Append("           <StateProvinceCode>" + ShipStateProvinceCode + "</StateProvinceCode>");
            addressResponse.Append("           <City>" + ShipToCity + "</City>");
            addressResponse.Append("		    <PostalCode>" + ShipToZipCode + "</PostalCode>");
            addressResponse.Append("       </Address>");
            addressResponse.Append("</AddressValidationRequest>");            
            
            // Convert the addressResponse into byte array and send the request to UPS
            var requestBytesTest = Encoding.ASCII.GetBytes(addressResponse.ToString());

            //PRFT Custom Code for allowing TLS12 with the new FedEx updates
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            var requestAVBytes = addressRequest.UploadData(UpsAddressValidationUrl, "POST", requestBytesTest);

            // Decode the response bytes into an XML string and create an XML document from it
            var xmlAddressResponce = Encoding.ASCII.GetString(requestAVBytes);
            XmlDocument xDoc = new XmlDocument();
            xDoc.LoadXml(xmlAddressResponce);
            XmlNodeList addressValidationResponse = xDoc.SelectNodes("AddressValidationResponse/Response");
            string addressValidationErrorMessage = addressValidationResponse.Item(0).SelectSingleNode("ResponseStatusDescription").InnerText;
            XmlNodeList errorRank = xDoc.SelectNodes("AddressValidationResponse/AddressValidationResult/Rank");
            return Equals(addressValidationErrorMessage, "Failure") ? "0" : (errorRank.Count > 1) ? "0" : "1";
        }

        #region PRFT Custom Code
        /// <summary>
        /// To perform UPS street level address validation.
        /// </summary>
        /// <returns>Error 1 / 0 if address is invalid then return 0 otherwise 1. Also set address type</returns>
        private string GetStreetLevelAddressValidation()
        {
            // Instantiate the request
            var addressRequest = new WebClient();
            addressRequest.Headers.Add("Content-Type", "application/x-www-form-urlencoded");

            var addressResponse = new StringBuilder();

            addressResponse.Append("<?xml version='1.0'?>");
            addressResponse.Append("<AccessRequest xml:lang='en-US'>");
            addressResponse.Append("	<AccessLicenseNumber>" + UpsKey + "</AccessLicenseNumber>");
            addressResponse.Append("	<UserId>" + UpsUserId + "</UserId>");
            addressResponse.Append("	<Password>" + UpsPassword + "</Password>");
            addressResponse.Append("</AccessRequest>");
            addressResponse.Append("<?xml version='1.0'?>");
            addressResponse.Append("<AddressValidationRequest xml:lang='en-US'>");
            addressResponse.Append("   <Request>");
            addressResponse.Append("        <TransactionReference>");
            addressResponse.Append("           <CustomerContext>Address Validation</CustomerContext>");
            addressResponse.Append("           <XpciVersion>1.0001</XpciVersion>");
            addressResponse.Append("        </TransactionReference>");
            addressResponse.Append("        <RequestAction>XAV</RequestAction>");
            addressResponse.Append("        <RequestOption>3</RequestOption>");
            addressResponse.Append("   </Request>");
            addressResponse.Append("   <AddressKeyFormat>");
            addressResponse.Append("        <AddressLine>" + ShipToAddressLine1 +" "+ ShipToAddressLine2 + "</AddressLine>");
            addressResponse.Append("        <PoliticalDivision2>" + ShipToCity + "</PoliticalDivision2>");
            addressResponse.Append("		<PoliticalDivision1>" + ShipStateProvinceCode + "</PoliticalDivision1>");
            addressResponse.Append("		<PostcodePrimaryLow>" + ShipToZipCode + "</PostcodePrimaryLow>");
            addressResponse.Append("		<CountryCode>" + ShipperCountryCode + "</CountryCode>");
            addressResponse.Append("   </AddressKeyFormat>");
            addressResponse.Append("</AddressValidationRequest>");

            //Log the Request
            ZNodeLogging.LogMessage("AddressRequest: " + addressResponse); 
            
            // Convert the addressResponse into byte array and send the request to UPS
            var requestBytesTest = Encoding.ASCII.GetBytes(addressResponse.ToString());

            //PRFT Custom Code for allowing TLS12 with the new FedEx updates
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            var requestAVBytes = addressRequest.UploadData(UPSStreetAddressValidationURL, "POST", requestBytesTest);  //PRFT Custom Code

            // Decode the response bytes into an XML string and create an XML document from it
            var xmlAddressResponce = Encoding.ASCII.GetString(requestAVBytes);

            //Log the xml response
            ZNodeLogging.LogMessage("AddressResponse: " + xmlAddressResponce); 
                   
            XmlDocument xDoc = new XmlDocument();
            xDoc.LoadXml(xmlAddressResponce);
            XmlNodeList addressValidationResponse = xDoc.SelectNodes("AddressValidationResponse/Response");
            string addressValidationErrorMessage = string.Empty;
            if (addressValidationResponse != null && addressValidationResponse.Count > 0)
            {
                addressValidationErrorMessage = addressValidationResponse.Item(0).SelectSingleNode("ResponseStatusDescription").InnerText;
            }
            
            
            XmlNodeList errorRank = xDoc.SelectNodes("AddressValidationResponse/AddressKeyFormat");
            XmlNodeList addressClassificationResponse = xDoc.SelectNodes("AddressValidationResponse/AddressKeyFormat/AddressClassification");

            string addressClassificationDescription = addressClassificationResponse.Count > 0 ? addressClassificationResponse.Item(0).SelectSingleNode("Description").InnerText : "Commercial";

            if (!string.IsNullOrEmpty(addressClassificationDescription) && !Equals(addressClassificationDescription, "Unknown"))
                ShipToAddressType = addressClassificationDescription;

            return Equals(addressValidationErrorMessage, "Failure") ? "0" : (errorRank.Count > 1) ? "0" : "1";
        }

        /// <summary>
        /// Calculate the extra shipping amount based on message config setting
        /// </summary>
        /// <parameter>decimal shippingrate</parameter>
        /// <parameter>string ShipStateProvinceCode</parameter>
        /// <returns>decimal value of extra shipping amount</returns>
        private decimal GetIncreasedShippingAmount(decimal shippingRate, string ShipStateProvinceCode)
        {
            decimal extraShippingAmount = 0;
            MessageConfigService messageConfigService = new MessageConfigService();
            var messageConfigList = messageConfigService.GetByKeyPortalIDLocaleIDMessageTypeID("StateCodeAndShippingRatePercentage", ZNodeConfigManager.SiteConfig.PortalID, 43, 1);
            string commaSeparatedStateShippingPercentage = string.Empty;
            if (messageConfigList != null && messageConfigList.Count > 0 && !string.IsNullOrEmpty(messageConfigList[0].Value))
            {
                commaSeparatedStateShippingPercentage = messageConfigList[0].Value;
                decimal extraPercentage = Convert.ToDecimal(commaSeparatedStateShippingPercentage?.Split(',').ToList().Where(x => x.Contains(ShipStateProvinceCode)).FirstOrDefault()?.Split('-')[1]);
                extraShippingAmount = (shippingRate * extraPercentage) / 100;
                extraShippingAmount = Math.Round(extraShippingAmount, 2);

            }
            return extraShippingAmount;
        }

        #endregion
    }
}
