﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Net;
using System.Web;
using System.Xml;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Shipping.Usps
{
    public class UspsAgent : ZNodeBusinessBase
    {
        private const string AddressLookupQueryString = "API=Verify&XML=";
        private const string RateLookupQueryString = "API=RateV4&XML=";

        public string ErrorCode { get; set; }
        public string ErrorDescription { get; set; }
        public string OriginZipCode { get; set; }
        public string PostageDeliveryUnitZip5 { get; set; }
        public string ServiceType { get; set; }
        public Address ShippingAddress { get; set; }
        public decimal ShippingRate { get; set; }
        public string Sku { get; set; }
        public string UspsShippingApiUrl { get; set; }
        public string UspsWebToolsUserId { get; set; }
        public string WeightInOunces { get; set; }
        public string WeightInPounds { get; set; }

        public UspsAgent()
        {
            // Get config settings and set defaults
            UspsShippingApiUrl = ConfigurationManager.AppSettings["USPSShippingAPIURL"];
            UspsWebToolsUserId = ConfigurationManager.AppSettings["USPSWebToolsUserID"];
            ServiceType = "Priority";
            ErrorCode = "0";
            WeightInOunces = "0";
            WeightInPounds = "0";
        }

        public bool CalculateShippingRate()
        {
            var isSuccess = false;

            // Build the XML and URL for the API call
            var xml = BuildRateRequestXml();
            var url = BuildRequestUrl(UspsShippingApiUrl, RateLookupQueryString, HttpUtility.UrlEncode(xml));

            try
            {
                var request = (HttpWebRequest)WebRequest.Create(url);
                using (var response = request.GetResponse())
                {
                    using (var stream = response.GetResponseStream())
                    {
                        var ds = new DataSet();
                        ds.ReadXml(stream);

                        isSuccess = ProcessRateResult(ds);
                    }
                }
            }
            catch (Exception ex)
            {
                // Set error code and description
                ErrorCode = "-1";
                ErrorDescription = ex.Message;
            }

            return isSuccess;
        }

        public bool IsAddressValid()
        {
            return IsAddressValid(ShippingAddress);
        }

        public bool IsAddressValid(Address address)
        {
            var isValid = false;

            ErrorCode = "-1";

            if (address.Street.Length > 38)
            {
                ErrorDescription = "Address1 is limited to a maximum of 38 characters.";
                return false;
            }

            if (!Equals(address.Street1, null) && address.Street1.Length > 38)
            {
                ErrorDescription = "Address2 is limited to a maximum of 38 characters.";
                return false;
            }

            if (address.City.Length > 15)
            {
                ErrorDescription = "City is limited to a maximum of 15 characters.";
                return false;
            }

            if (address.StateCode.Length > 2)
            {
                ErrorDescription = "State code is limited to a maximum of 2 characters.";
                return false;
            }

            if (address.PostalCode.Length > 5)
            {
                ErrorDescription = "Postal code is limited to a maximum of 5 characters.";
                return false;
            }

            // Build the XML and URL for the API call
            var xml = BuildAddressRequestXml(address);
            var url = BuildRequestUrl(UspsShippingApiUrl, AddressLookupQueryString, HttpUtility.UrlEncode(xml));

            try
            {
                var request = (HttpWebRequest)WebRequest.Create(url);
                using (var response = request.GetResponse())
                {
                    using (var stream = response.GetResponseStream())
                    {
                        var ds = new DataSet();
                        ds.ReadXml(stream);

                        isValid = ProcessAddressResult(ds);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorCode = "-1";
                ErrorDescription = ex.Message;
            }

            return isValid;
        }

        private string BuildRequestUrl(string apiUrl, string apiQueryString, string xmlValue)
        {
            return String.Format("{0}?{1}{2}", apiUrl, apiQueryString, xmlValue);
        }

        private string BuildRateRequestXml()
        {
            var xml = String.Empty;

            var document = new XmlDocument();
            var requestNode = document.CreateElement("RateV4Request");
            requestNode.SetAttribute("USERID", UspsWebToolsUserId);

            var packageNode = document.CreateElement("Package");
            packageNode.SetAttribute("ID", Sku);

            var serviceNode = document.CreateElement("Service");
            serviceNode.InnerText = "PRIORITY";

            var zipOriginationNode = document.CreateElement("ZipOrigination");
            zipOriginationNode.InnerText = OriginZipCode;

            var zipDestinationNode = document.CreateElement("ZipDestination");
            zipDestinationNode.InnerText = PostageDeliveryUnitZip5;

            var poundsNode = document.CreateElement("Pounds");
            poundsNode.InnerText = ((int)Decimal.Parse(WeightInPounds)).ToString();

            var ouncesNode = document.CreateElement("Ounces");
            ouncesNode.InnerText = ((int)Decimal.Parse(WeightInOunces)).ToString();

            var sizeNode = document.CreateElement("Size");
            sizeNode.InnerText = "REGULAR";

            var containerNode = document.CreateElement("Container");
            containerNode.InnerText = "Variable";

            // Append nodes
            packageNode.AppendChild(serviceNode);
            packageNode.AppendChild(zipOriginationNode);
            packageNode.AppendChild(zipDestinationNode);
            packageNode.AppendChild(poundsNode);
            packageNode.AppendChild(ouncesNode);
            packageNode.AppendChild(containerNode);
            packageNode.AppendChild(sizeNode);
            requestNode.AppendChild(packageNode);


            using (var sw = new StringWriter())
            {
                using (var xw = new XmlTextWriter(sw))
                {
                    requestNode.WriteTo(xw);
                    xml = sw.ToString();
                }
            }

            return xml;
        }

        private string BuildAddressRequestXml(Address address)
        {
            var xml = String.Empty;

            var document = new XmlDocument();
            var requestNode = document.CreateElement("AddressValidateRequest");
            requestNode.SetAttribute("USERID", UspsWebToolsUserId);

            var addressNode = document.CreateElement("Address");
            addressNode.SetAttribute("ID", address.AddressID.ToString());

            var companyNode = document.CreateElement("FirmName");
            companyNode.InnerText = address.CompanyName;

            // Swap the address line
            var addressLine1Node = document.CreateElement("Address1");
            addressLine1Node.InnerText = address.Street1;

            var addressLine2Node = document.CreateElement("Address2");
            addressLine2Node.InnerText = address.Street;

            var cityNode = document.CreateElement("City");
            cityNode.InnerText = address.City;

            var stateNode = document.CreateElement("State");
            stateNode.InnerText = address.StateCode;

            var zip5Node = document.CreateElement("Zip5");
            zip5Node.InnerText = address.PostalCode;

            var zip4Node = document.CreateElement("Zip4");
            if (address.PostalCode.Length > 4)
            {
                zip4Node.InnerText = address.PostalCode.Substring(0, 4);
            }
            else
            {
                zip4Node.InnerText = address.PostalCode;
            }

            // Append nodes
            addressNode.AppendChild(companyNode);
            addressNode.AppendChild(addressLine1Node);
            addressNode.AppendChild(addressLine2Node);
            addressNode.AppendChild(cityNode);
            addressNode.AppendChild(stateNode);
            addressNode.AppendChild(zip5Node);
            addressNode.AppendChild(zip4Node);
            requestNode.AppendChild(addressNode);

            using (var sw = new StringWriter())
            {
                using (var xw = new XmlTextWriter(sw))
                {
                    requestNode.WriteTo(xw);
                    xml = sw.ToString();
                }
            }

            return xml;
        }

        private bool ProcessRateResult(DataSet responseDataSet)
        {
            var isSuccess = true;

            // Reset the error code and description
            ErrorCode = "0";
            ErrorDescription = String.Empty;

            // Determine if error response was returned
            if (responseDataSet.Tables["Error"] == null && responseDataSet.Tables["Package"] != null)
            {
                // Process response
                var tempTable = responseDataSet.Tables["Package"];
                var tempPackage = tempTable.Rows[0];

                if (tempPackage != null)
                {
                    var rows = tempPackage.GetChildRows(responseDataSet.Relations["Package_Postage"]);
                    var postageRow = rows[0];
                    ShippingRate = Decimal.Parse(postageRow["Rate"].ToString());
                }
            }
            else
            {
                isSuccess = false;
                ErrorCode = responseDataSet.Tables["Error"].Rows[0]["Number"].ToString();
                ErrorDescription = responseDataSet.Tables["Error"].Rows[0]["Description"].ToString();
            }

            return isSuccess;
        }

        private bool ProcessAddressResult(DataSet responseDataSet)
        {
            var isSuccess = true;

            // Reset the error code and description
            ErrorCode = "0";
            ErrorDescription = String.Empty;

            // Determine if error response was returned
            if (responseDataSet.Tables["Error"] == null)
            {
                var tempTable = responseDataSet.Tables["Address"];
            }
            else
            {
                isSuccess = false;
                ErrorCode = responseDataSet.Tables["Error"].Rows[0]["Number"].ToString();
                ErrorDescription = responseDataSet.Tables["Error"].Rows[0]["Description"].ToString();
            }

            return isSuccess;
        }


        #region Znode Version 7.2.2 - Split PackageWeight

        /// <summary>
        /// Znode Version 7.2.2
        /// This function split the PackageWeight in elements according to specified packageWeight limit
        /// </summary>
        /// <param name="packageWeight", name="packageWaightLimitLbs">Order Line Id </param>
        /// <param name="packageWaightLimitLbs">Order Line Id </param>
        /// <returns>List of splited packageWeight </returns>
        public List<decimal> SplitPackageWeight(decimal packageWeight, decimal packageWaightLimitLbs)
        {
            try
            {

                decimal actualWeight = packageWeight;
                int count = 0;
                List<decimal> weightList = new List<decimal>();

                while (actualWeight > packageWaightLimitLbs)
                {

                    decimal extraWeight = 0;
                    extraWeight = actualWeight - packageWaightLimitLbs;
                    actualWeight = actualWeight - extraWeight;
                    weightList.Add(actualWeight);
                    actualWeight = extraWeight;
                    count++;
                }
                if (actualWeight > 0)
                {
                    weightList.Add(actualWeight);
                }

                return weightList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

    }
}
