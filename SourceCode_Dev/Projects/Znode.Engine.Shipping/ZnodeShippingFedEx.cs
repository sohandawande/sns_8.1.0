using System;
using System.Collections.Generic;
using Znode.Engine.Shipping.FedEx;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Shipping
{
    public class ZnodeShippingFedEx : ZnodeShippingType
    {
        public ZnodeShippingFedEx()
        {
            Name = "FedEx";
            Description = "Calculates shipping rates when using FedEx.";

            Controls.Add(ZnodeShippingControl.Profile);
            Controls.Add(ZnodeShippingControl.ServiceCodes);
            Controls.Add(ZnodeShippingControl.HandlingCharge);
            Controls.Add(ZnodeShippingControl.Countries);
        }

        /// <summary>
        /// Calculates shipping rates when using FedEx.
        /// </summary>
        public override void Calculate()
        {
            decimal itemShippingRate = 0;
            decimal itemTotalValue = 0;

            var packageType = ZNodeConfigManager.SiteConfig.FedExPackagingType;
            var addInsurance = ZNodeConfigManager.SiteConfig.FedExAddInsurance;
            var weightUnit = ZNodeConfigManager.SiteConfig.WeightUnit;
            var dimensionUnit = ZNodeConfigManager.SiteConfig.DimensionUnit;

            // Instantiate FedEx agent
            var fedEx = new FedExAgent();

            try
            {
                // Try decrypting FedEx account info
                var encrypt = new ZNodeEncryption();
                fedEx.FedExAccessKey = encrypt.DecryptData(ZNodeConfigManager.SiteConfig.FedExProductionKey);
                fedEx.FedExAccountNumber = encrypt.DecryptData(ZNodeConfigManager.SiteConfig.FedExAccountNumber);
                fedEx.FedExMeterNumber = encrypt.DecryptData(ZNodeConfigManager.SiteConfig.FedExMeterNumber);
                fedEx.FedExSecurityCode = encrypt.DecryptData(ZNodeConfigManager.SiteConfig.FedExSecurityCode);
            }
            catch (Exception ex)
            {
                // Ignore decryption issues but log it (not sure why we ignore it)
                ZNodeLogging.LogMessage("There was an error decrypting FedEx account information.");
                ZNodeLogging.LogMessage(ex.Message);
            }

            // CSP account info
            fedEx.CspAccessKey = ZNodeConfigManager.SiteConfig.FedExCSPKey;
            fedEx.CspPassword = ZNodeConfigManager.SiteConfig.FedExCSPPassword;
            fedEx.ClientProductId = ZNodeConfigManager.SiteConfig.FedExClientProductId;
            fedEx.ClientProductVersion = ZNodeConfigManager.SiteConfig.FedExClientProductVersion;

            // Service type
            fedEx.FedExServiceType = ShippingBag.ShippingCode;
            fedEx.PackageTypeCode = ZNodeConfigManager.SiteConfig.FedExPackagingType;
            fedEx.DropOffType = ZNodeConfigManager.SiteConfig.FedExDropoffType;

            // Shipping origin
            fedEx.ShipperAddress1 = ZNodeConfigManager.SiteConfig.ShippingOriginAddress1;
            fedEx.ShipperAddress2 = ZNodeConfigManager.SiteConfig.ShippingOriginAddress2;
            fedEx.ShipperCity = ZNodeConfigManager.SiteConfig.ShippingOriginCity;
            fedEx.ShipperZipCode = ZNodeConfigManager.SiteConfig.ShippingOriginZipCode;
            fedEx.ShipperStateCode = ZNodeConfigManager.SiteConfig.ShippingOriginStateCode;
            fedEx.ShipperCountryCode = ZNodeConfigManager.SiteConfig.ShippingOriginCountryCode;

            // Destination properties
            fedEx.ShipToAddress1 = ShoppingCart.Payment.ShippingAddress.Street;
            fedEx.ShipToAddress2 = ShoppingCart.Payment.ShippingAddress.Street1;
            fedEx.ShipToCity = ShoppingCart.Payment.ShippingAddress.City;
            fedEx.ShipToZipCode = ShoppingCart.Payment.ShippingAddress.PostalCode;
            fedEx.ShipToStateCode = ShoppingCart.Payment.ShippingAddress.StateCode;
            fedEx.ShipToCountryCode = ShoppingCart.Payment.ShippingAddress.CountryCode;

            // Ship-to residence
            if (String.IsNullOrEmpty(ShoppingCart.Payment.ShippingAddress.CompanyName))
            {
                fedEx.ShipToAddressIsResidential = true;
            }
            else
            {
                fedEx.ShipToAddressIsResidential = false;
            }

            // Check to apply FedEx discount rates
            if (ZNodeConfigManager.SiteConfig.FedExUseDiscountRate.HasValue)
            {
                fedEx.UseDiscountRate = ZNodeConfigManager.SiteConfig.FedExUseDiscountRate.Value;
            }

            // Check weight unit
            if (weightUnit.Length > 0)
            {
                fedEx.WeightUnit = weightUnit.TrimEnd(new char[] { 'S' });
            }

            // Check dimension unit
            if (dimensionUnit.Length > 0)
            {
                fedEx.DimensionUnit = dimensionUnit;
            }

            // Currency code
            fedEx.CurrencyCode = ZNodeCurrencyManager.CurrencyCode();

            // Split the items that ship separately from the items that ship together
            SplitShipSeparatelyFromShipTogether();

            // Shipping estimate for ship-together package
            if (ShipTogetherItems.Count > 0)
            {
                var shipTogetherPackage = new ZnodeShippingPackage(ShipTogetherItems);

                if (packageType != null && packageType.ToUpper().Equals("YOUR_PACKAGING"))
                {
                    fedEx.PackageLength = Convert.ToInt32(shipTogetherPackage.Length).ToString();
                    fedEx.PackageHeight = Convert.ToInt32(shipTogetherPackage.Height).ToString();
                    fedEx.PackageWidth = Convert.ToInt32(shipTogetherPackage.Width).ToString();
                }

                // Set weight
                fedEx.PackageWeight = shipTogetherPackage.Weight;

                // Add insurance
                itemTotalValue = shipTogetherPackage.Value;

                if (addInsurance.GetValueOrDefault(false))
                {
                    fedEx.TotalInsuredValue = itemTotalValue;
                }

                // Add customs
                fedEx.TotalCustomsValue = itemTotalValue;


                #region Znode Version 7.2.2 - Split PackageWeight

                //Splite Weight if Weight is greter than 150 for fedEx
                List<decimal> splWeight = new List<decimal>();
                if (fedEx.WeightUnit.ToUpper().Equals(weightUnitKgs))
                {
                    WeightUnitBase = fedEx.WeightUnit;
                    fedEx.PackageWeight = ConvertWeightKgToLbs(fedEx.PackageWeight);
                    fedEx.WeightUnit = WeightUnitBase = weightUnitLbs;
                }
                if (fedEx.PackageWeight > GetWeightLimit(1)) //1 FexEx
                {
                    WeightUnitBase = fedEx.WeightUnit;
                    splWeight = SplitPackageWeight(fedEx.PackageWeight);
                }

                if (splWeight.Count > 0)
                {
                    for (int listItemCount = 0; listItemCount < splWeight.Count; listItemCount++)
                    {
                        fedEx.PackageWeight = splWeight[listItemCount];
                        // Get shipping rate for each item
                        itemShippingRate += fedEx.GetShippingRate();
                    }
                }
                else
                {
                    itemShippingRate += fedEx.GetShippingRate();
                }

                #endregion

                // Add handling charge for ship-together package
                itemShippingRate += ShippingBag.HandlingCharge;
            }

            // Shipping estimate for ship-separately packages
            if (ShipSeparatelyItems.Count > 0)
            {
                foreach (ZNodeShoppingCartItem separateItem in ShipSeparatelyItems)
                {
                    var singleItemlist = new ZNodeGenericCollection<ZNodeShoppingCartItem>();
                    singleItemlist.Add(separateItem);

                    var seperateItemPackage = new ZnodeShippingPackage(singleItemlist);

                    if (packageType.ToUpper().Equals("YOUR_PACKAGING"))
                    {
                        fedEx.PackageLength = Convert.ToInt32(seperateItemPackage.Length).ToString();
                        fedEx.PackageHeight = Convert.ToInt32(seperateItemPackage.Height).ToString();
                        fedEx.PackageWidth = Convert.ToInt32(seperateItemPackage.Width).ToString();
                    }

                    // Set weight
                    fedEx.PackageWeight = seperateItemPackage.Weight;

                    // Add insurance
                    itemTotalValue = seperateItemPackage.Value;

                    if (addInsurance.GetValueOrDefault(false))
                    {
                        fedEx.TotalInsuredValue = itemTotalValue;
                    }

                    // Add customs
                    fedEx.TotalCustomsValue = itemTotalValue;

                    #region Znode Version 7.2.2 - Split PackageWeight

                    //Splite Weight if Weight is greter than 150 for fedEx
                    List<decimal> spWeight = new List<decimal>();
                    if (fedEx.WeightUnit.ToUpper().Equals(weightUnitKgs))
                    {
                        WeightUnitBase = fedEx.WeightUnit;
                        fedEx.PackageWeight = ConvertWeightKgToLbs(fedEx.PackageWeight);
                        fedEx.WeightUnit = WeightUnitBase = weightUnitLbs;
                    }
                    if (fedEx.PackageWeight > GetWeightLimit(1))//1 FedEx
                    {
                        WeightUnitBase = fedEx.WeightUnit;
                        spWeight = SplitPackageWeight(fedEx.PackageWeight);
                    }

                    if (spWeight.Count > 0)
                    {

                        for (int listItemCount = 0; listItemCount < spWeight.Count; listItemCount++)
                        {
                            fedEx.PackageWeight = spWeight[listItemCount];
                            // Get shipping rate for each item
                            itemShippingRate += fedEx.GetShippingRate();
                        }


                    }
                    else
                    {
                        itemShippingRate += fedEx.GetShippingRate();
                    }

                    #endregion

                    // Add handling charge for ship seperately item
                    itemShippingRate += ShippingBag.HandlingCharge;
                }
            }

            if (fedEx.ErrorCode == null)
            {
                ShoppingCart.Shipping.ResponseCode = "-1";
                ShoppingCart.AddErrorMessage = "Shipping error: Invalid option selected.";
                ShoppingCart.Shipping.ResponseMessage = GenericShippingErrorMessage();
            }
            else if (!fedEx.ErrorCode.Equals("0") && (ShipSeparatelyItems.Count > 0 || ShipTogetherItems.Count > 0))
            {
                ShoppingCart.Shipping.ResponseCode = fedEx.ErrorCode;
                ShoppingCart.Shipping.ResponseMessage = GenericShippingErrorMessage();
                ShoppingCart.AddErrorMessage = GenericShippingErrorMessage();
                ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.GeneralError, "Shipping error: " + fedEx.ErrorCode + " " + fedEx.ErrorDescription);
            }
            else
            {
                ShoppingCart.OrderLevelShipping += itemShippingRate;
            }
        }



        /// <summary>
        /// Checks the response code before the order is submitted.
        /// </summary>
        /// <returns>True if the response code is 0; otherwise, false.</returns>
        public override bool PreSubmitOrderProcess()
        {
            if (ShoppingCart.Shipping.ResponseCode != "0")
            {
                ShoppingCart.AddErrorMessage = GenericShippingErrorMessage();
                ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.GeneralError, "Shipping error in PreSubmitOrderProcess: " + ShoppingCart.Shipping.ResponseCode + " " + ShoppingCart.Shipping.ResponseMessage);
                return false;
            }

            return true;
        }
    }
}