using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.Framework.Business;
using System.Configuration;
using ZNode.Libraries.DataAccess.Service;

namespace Znode.Engine.Shipping
{
    /// <summary>
    /// This is the base class for all shipping types.
    /// </summary>
    public class ZnodeShippingType : IZnodeShippingType
    {
        private string _className;
        private Collection<ZnodeShippingControl> _controls;
        private ZNodeGenericCollection<ZNodeShoppingCartItem> _shipSeparatelyItems;
        private ZNodeGenericCollection<ZNodeShoppingCartItem> _shipTogetherItems;
        private string _weightUnit;
        private decimal _weightLimit;
        public const string weightUnitLbs = "LB";
        public const string weightUnitKgs = "KG";
        public decimal WeightLimit
        {
            get { return _weightLimit; }
            set { _weightLimit = value; }
        }
        public string ClassName
        {
            get
            {
                if (String.IsNullOrEmpty(_className))
                {
                    _className = GetType().Name;
                }

                return _className;
            }

            set { _className = value; }
        }

        public string WeightUnitBase
        {
            get
            {
                if (String.IsNullOrEmpty(_weightUnit))
                {
                    _weightUnit = GetType().Name;
                }

                return _weightUnit;
            }

            set { _weightUnit = value; }
        }

        public Collection<ZnodeShippingControl> Controls
        {
            get { return _controls ?? (_controls = new Collection<ZnodeShippingControl>()); }
        }

        public ZNodeGenericCollection<ZNodeShoppingCartItem> ShipSeparatelyItems
        {
            get { return _shipSeparatelyItems ?? (_shipSeparatelyItems = new ZNodeGenericCollection<ZNodeShoppingCartItem>()); }
        }

        public ZNodeGenericCollection<ZNodeShoppingCartItem> ShipTogetherItems
        {
            get { return _shipTogetherItems ?? (_shipTogetherItems = new ZNodeGenericCollection<ZNodeShoppingCartItem>()); }
        }

        public string Name { get; set; }
        public string Description { get; set; }
        public int Precedence { get; set; }
        public ZNodeShoppingCart ShoppingCart { get; set; }
        public ZnodeShippingBag ShippingBag { get; set; }

        /// <summary>
        /// Returns a generic shipping error message.
        /// </summary>
        /// <returns>A generic error message used by any shipping type.</returns>
        public virtual string GenericShippingErrorMessage()
        {
            return "Unable to calculate shipping rates at this time, please try again later.";
        }

        /// <summary>
        /// Binds the shopping cart and shipping data to the shipping type.
        /// </summary>
        /// <param name="shoppingCart">The current shopping cart.</param>
        /// <param name="shippingBag">The shipping properties.</param>
        public virtual void Bind(ZNodeShoppingCart shoppingCart, ZnodeShippingBag shippingBag)
        {
            ShoppingCart = shoppingCart;
            ShippingBag = shippingBag;
        }

        /// <summary>
        /// Calculates the shipping rates and updates the shopping cart.
        /// </summary>
        public virtual void Calculate()
        {
        }

        /// <summary>
        /// Process anything that must be done before the order is submitted.
        /// </summary>
        /// <returns>True if everything is good for submitting the order; otherwise, false.</returns>
        public virtual bool PreSubmitOrderProcess()
        {
            // Most shipping types don't need any special verification
            return true;
        }

        /// <summary>
        /// Process anything that must be done after the order is submitted.
        /// </summary>
        public virtual void PostSubmitOrderProcess()
        {
            // Most shipping types don't need any further processing after the order is submitted
        }

        /// <summary>
        /// Helper method to split the ship-separately items from the ship-together items in the shopping cart.
        /// </summary>
        public virtual void SplitShipSeparatelyFromShipTogether()
        {
            foreach (ZNodeShoppingCartItem cartItem in ShippingBag.ShoppingCart.ShoppingCartItems)
            {
                var hasWeight = false;
                if (cartItem.Product.FreeShippingInd)
                {
                    foreach (ZNodeAddOnEntity addOn in cartItem.Product.SelectedAddOns.AddOnCollection)
                    {
                        foreach (ZNodeAddOnValueEntity addOnValue in addOn.AddOnValueCollection)
                        {
                            if (!addOnValue.FreeShippingInd && addOnValue.Weight > 0)
                            {
                                hasWeight = true;
                                break;
                            }
                        }
                    }
                }

                if (cartItem.Product.Weight > 0 && cartItem.Product.ShipSeparately)
                {
                    if (!cartItem.Product.FreeShippingInd || hasWeight)
                    {
                        ShipSeparatelyItems.Add(cartItem);
                    }
                }
                else if (cartItem.Product.Weight > 0 && !cartItem.Product.ShipSeparately)
                {
                    if (!cartItem.Product.FreeShippingInd || hasWeight)
                    {
                        ShipTogetherItems.Add(cartItem);
                    }
                }
            }
        }


        #region Znode Version 7.2.2 - Split PackageWeight

        /// <summary>
        /// Znode Version 7.2.2
        /// This function split the PackageWeight in elements according to specified packageWeight limit
        /// </summary>
        /// <returns>List of splited packageWeight </returns>
        public  List<decimal> SplitPackageWeight(decimal packageWeight)
        {
            try
            {
                decimal actualWeight = packageWeight;
                decimal weightLmt = WeightLimit;
                int count = 0;
                List<decimal> weightList = new List<decimal>();

                while (actualWeight > weightLmt)
                {
                    decimal extraWeight = 0;
                    extraWeight = actualWeight - weightLmt;
                    actualWeight = actualWeight - extraWeight;
                    weightList.Add(actualWeight);
                    actualWeight = extraWeight;
                    count++;
                }

                if (actualWeight > 0)
                {
                    weightList.Add(actualWeight);
                }

                return weightList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Znode Version 7.2.2
        /// This function will convert the Package Weight from KG to LBs.
        /// To convert from KG to LBs the formula will be
        /// Lbs = kg * 2.20462262
        /// </summary>
        /// <returns>calculated weight in lbs</returns>
        public  decimal ConvertWeightKgToLbs(decimal packageWeight)
        {
            try
            {
                return (WeightUnitBase.Equals(weightUnitKgs)) ? (packageWeight * (decimal)2.20462262) : packageWeight;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        //TODO: Replace code of reading key value from web config file with DB as dynamic. Notation DATE: 11/20/2014 (MSE547)
        /// <summary>
        /// Znode Version 7.2.2 
        /// This function identify the Shipping Weight limit according to shipping Key values which  currently specify in web.config file 
        /// </summary>
        /// <returns>Shipping package WeightLimit </returns>
        public  decimal GetWeightLimit(int shipingType)
        {
            try
            {
                switch (shipingType)
                {
                    case 1://FedEx
                        if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["FedExWeightLimitInLBS"].ToString()))
                        {
                            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("FedEx Weight limit in lbs is missing.");
                        }
                        else
                        {
                            WeightLimit = Convert.ToDecimal(ConfigurationManager.AppSettings["FedExWeightLimitInLBS"].ToString());
                        }
                        break;
                    case 2://UPS
                        MessageConfigService messageConfigService = new MessageConfigService();                        
                        if (string.IsNullOrEmpty(messageConfigService.GetByKeyPortalIDLocaleIDMessageTypeID("UPSWeightLimitInLBS", ZNodeConfigManager.SiteConfig.PortalID, 43, 1).ToString()))
                        {
                            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("UPS Weight limit in lbs is missing.");
                        }
                        else
                        {
                           //WeightLimit = Convert.ToDecimal(ConfigurationManager.AppSettings["UPSWeightLimitInLBS"].ToString());                            
                           var messageConfigList = messageConfigService.GetByKeyPortalIDLocaleIDMessageTypeID("UPSWeightLimitInLBS", ZNodeConfigManager.SiteConfig.PortalID, 43, 1);
                           decimal WeightLimitMsgConfig = 0;
                           decimal.TryParse(messageConfigList[0].Value, out WeightLimitMsgConfig);
                           WeightLimit = WeightLimitMsgConfig;
                        }

                        break;
                    case 3://USPS
                        if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["USPSWeightLimitInLBS"].ToString()))
                        {
                            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("USPS Weight limit in lbs is missing.");
                        }
                        else
                        {
                            WeightLimit = Convert.ToDecimal(ConfigurationManager.AppSettings["USPSWeightLimitInLBS"].ToString());
                        }

                        break;
                }
                return WeightLimit;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

    }
}
