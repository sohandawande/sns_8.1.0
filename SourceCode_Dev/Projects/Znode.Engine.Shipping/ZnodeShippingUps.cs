using System;
using System.Collections.Generic;
using System.Configuration;
using Znode.Engine.Shipping.Ups;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Shipping
{
    public class ZnodeShippingUps : ZnodeShippingType
    {
        public ZnodeShippingUps()
        {
            Name = "UPS";
            Description = "Calculates shipping rates when using UPS.";

            Controls.Add(ZnodeShippingControl.Profile);
            Controls.Add(ZnodeShippingControl.ServiceCodes);
            Controls.Add(ZnodeShippingControl.HandlingCharge);
        }

        /// <summary>
        /// Calculates shipping rates when using UPS.
        /// </summary>
        public override void Calculate()
        {
            decimal itemShippingRate = 0;

            // Instantiate UPS agent
            var ups = new UpsAgent();

            try
            {
                // Try decrypting UPS account info
                var encrypt = new ZNodeEncryption();
                ups.UpsUserId = encrypt.DecryptData(ZNodeConfigManager.SiteConfig.UPSUserName);
                ups.UpsPassword = encrypt.DecryptData(ZNodeConfigManager.SiteConfig.UPSPassword);
                ups.UpsKey = encrypt.DecryptData(ZNodeConfigManager.SiteConfig.UPSKey);
            }
            catch (Exception ex)
            {
                // Ignore decryption issues but log it (not sure why we ignore it)
                ZNodeLogging.LogMessage("There was an error decrypting UPS account information.");
                ZNodeLogging.LogMessage(ex.Message);
            }

            // General settings
            ups.ShipperZipCode = ZNodeConfigManager.SiteConfig.ShippingOriginZipCode;
            ups.ShipperCountryCode = ZNodeConfigManager.SiteConfig.ShippingOriginCountryCode;
            //PRFT Custom Code:start
            ups.ShipperStateProvinceCode = ZNodeConfigManager.SiteConfig.ShippingOriginStateCode;
            ups.ShipperAccountNumber = Convert.ToString(ConfigurationManager.AppSettings["ShipperAccountNumber"]);
            ups.ShipToAddressLine1 = ShoppingCart.Payment.ShippingAddress.Street;
            ups.ShipToAddressLine2 = ShoppingCart.Payment.ShippingAddress.Street1;
            //PRFT Custom Code:End
            ups.ShipToZipCode = ShoppingCart.Payment.ShippingAddress.PostalCode;
            ups.ShipToCountryCode = ShoppingCart.Payment.ShippingAddress.CountryCode;
            ups.ShipToCity = ShoppingCart.Payment.ShippingAddress.City;
            ups.ShipStateProvinceCode = ShoppingCart.Payment.ShippingAddress.StateCode;
            ups.PackageTypeCode = "02";
            ups.UpsServiceCode = ShippingBag.ShippingCode;
            ups.PickupType = "06"; // One time pickup
            ups.ShipToAddressType = "Commercial";
            ups.CustomerExternalId = ShoppingCart.CustomerExternalId;
            //Znode Version 7.2.2
            //Set shipping package weightUnit - Start
            var weightUnit = ZNodeConfigManager.SiteConfig.WeightUnit;
            if (weightUnit.Length > 0)
            {
                ups.WeightUnit = weightUnit.ToUpper().Equals("KGS") ? weightUnit.TrimEnd(new char[] { 'S' }) : weightUnit;

            }
            //Set shipping package weightUnit - End

            // Split the items that ship separately from the items that ship together
            SplitShipSeparatelyFromShipTogether();

            // Shipping estimate for ship-together package
            if (ShipTogetherItems.Count > 0)
            {
                var shipTogetherPackage = new ZnodeShippingPackage(ShipTogetherItems, false, true);

                // Package dimensions
                ups.PackageLength = shipTogetherPackage.Length;
                ups.PackageHeight = shipTogetherPackage.Height;
                ups.PackageWidth = shipTogetherPackage.Width;
                ups.PackageWeight = shipTogetherPackage.Weight;

                #region Znode Version 7.2.2 - Split PackageWeight

                //convert waight in LBS
                if (ups.WeightUnit.ToUpper().Equals(weightUnitKgs))
                {
                    WeightUnitBase = ups.WeightUnit;
                    ups.PackageWeight = ConvertWeightKgToLbs(ups.PackageWeight);
                    ups.WeightUnit = "LBS";
                    WeightUnitBase = weightUnitLbs;
                }

                //Splite Weight if Weight is greter than 150lbs 
                List<decimal> spWeight = new List<decimal>();
                if (ups.PackageWeight > GetWeightLimit(2))//2 UPS
                {
                    WeightUnitBase = ups.WeightUnit;
                    spWeight = SplitPackageWeight(ups.PackageWeight);
                }

                if (spWeight.Count > 0)
                {
                    for (int listItemCount = 0; listItemCount < spWeight.Count; listItemCount++)
                    {
                        ups.PackageWeight = spWeight[listItemCount];
                        // Get shipping rate for each item
                        itemShippingRate += ups.GetShippingRate();
                    }
                }
                else
                {
                    // Get shipping rate for each item
                    itemShippingRate += ups.GetShippingRate();
                }

                #endregion

                // Add handling charge for ship-together package
                itemShippingRate += ShippingBag.HandlingCharge;
                //PRFT custom code to show proper Invalid Address Message : Start
                if (ups != null && !string.IsNullOrEmpty(ups.ErrorCode))
                {
                    ZNodeLogging.LogMessage("ups.ErrorCode: " + ups.ErrorCode);
                    if (ups.ErrorCode.Equals(ConfigurationManager.AppSettings["InvalidAddressCode"]))
                    {
                        ShoppingCart.Shipping.ResponseCode = ups.ErrorCode;
                        ShoppingCart.Shipping.ResponseMessage = ConfigurationManager.AppSettings["InvalidAddressMessage"];
                        ShoppingCart.AddErrorMessage = ConfigurationManager.AppSettings["InvalidAddressMessage"];
                        ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.GeneralError, "Shipping error: " + ups.ErrorCode + " " + ups.ErrorDescription);
                    }
                    //PRFT custom code : End
                    else if (ups.ErrorCode != "0")
                    {
                        ShoppingCart.Shipping.ResponseCode = ups.ErrorCode;
                        ShoppingCart.Shipping.ResponseMessage = GenericShippingErrorMessage();
                        ShoppingCart.AddErrorMessage = GenericShippingErrorMessage();
                        ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.GeneralError, "Shipping error: " + ups.ErrorCode + " " + ups.ErrorDescription);
                    }
                }
            }

            // Shipping estimate for ship-separately packages
            if (ShipSeparatelyItems.Count > 0)
            {
                foreach (ZNodeShoppingCartItem separateItem in ShipSeparatelyItems)
                {
                    var singleItemlist = new ZNodeGenericCollection<ZNodeShoppingCartItem>();
                    singleItemlist.Add(separateItem);

                    var seperateItemPackage = new ZnodeShippingPackage(singleItemlist, false, false);

                    // Package dimensions
                    ups.PackageLength = seperateItemPackage.Length;
                    ups.PackageHeight = seperateItemPackage.Height;
                    ups.PackageWidth = seperateItemPackage.Width;
                    ups.PackageWeight = seperateItemPackage.Weight;

                    #region Znode Version 7.2.2 - Split PackageWeight

                    //convert waight in LBS
                    if (ups.WeightUnit.ToUpper().Equals(weightUnitKgs))
                    {
                        WeightUnitBase = ups.WeightUnit;
                        ups.PackageWeight = ConvertWeightKgToLbs(ups.PackageWeight);
                        ups.WeightUnit = "LBS";
                        WeightUnitBase = weightUnitLbs;
                    }

                    //Splite Weight if Weight is greter than 150lbs
                    List<decimal> spWeight = new List<decimal>();
                    if (ups.PackageWeight > GetWeightLimit(2))//2 UPS
                    {
                        WeightUnitBase = ups.WeightUnit;
                        spWeight = SplitPackageWeight(ups.PackageWeight);
                    }

                    if (spWeight.Count > 0)
                    {
                        for (int listItemCount = 0; listItemCount < spWeight.Count; listItemCount++)
                        {
                            ups.PackageWeight = spWeight[listItemCount];
                            // Get shipping rate for each single item and multiply with quantity to total charges of order line item
                            itemShippingRate += ups.GetShippingRate() * separateItem.Quantity;
                        }
                    }
                    else
                    {
                        // Get shipping rate for each single item and multiply with quantity to total charges of order line item
                        itemShippingRate += ups.GetShippingRate() * separateItem.Quantity;
                    }

                    #endregion

                    // Add handling charge for ship-seperately item and multiply with quantity to total charges of order line item
                    itemShippingRate += ShippingBag.HandlingCharge * separateItem.Quantity;

                    if (ups.ErrorCode != "0")
                    {
                        ShoppingCart.Shipping.ResponseCode = ups.ErrorCode;
                        ShoppingCart.AddErrorMessage = GenericShippingErrorMessage();
                        ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.GeneralError, String.Format("Shipping error for {0}: {1} {2}", separateItem.Product.Name, ups.ErrorCode, ups.ErrorDescription));
                    }
                }
            }

            ShoppingCart.OrderLevelShipping += itemShippingRate;
        }

        /// <summary>
        /// Checks the response code before the order is submitted.
        /// </summary>
        /// <returns>True if the response code is 0; otherwise, false.</returns>
        public override bool PreSubmitOrderProcess()
        {
            if (ShoppingCart.Shipping.ResponseCode != "0")
            {
                ShoppingCart.AddErrorMessage = GenericShippingErrorMessage();
                ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.GeneralError, "Shipping error in PreSubmitOrderProcess: " + ShoppingCart.Shipping.ResponseCode + " " + ShoppingCart.Shipping.ResponseMessage);
                return false;
            }

            return true;
        }
    }
}
