using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Shipping
{
	public class ZnodeShippingCustomWeight : ZNodeBusinessBase
	{
		/// <summary>
		/// Calculates the custom shipping rate based on weight.
		/// </summary>
		/// <param name="shoppingCart">The current shopping cart.</param>
		/// <param name="shippingBag">The shipping data for the custom shipping type.</param>
        public void Calculate(ZNodeShoppingCart shoppingCart, ZnodeShippingBag shippingBag)
        {
            // Filter for weight-based rate (ShippingRuleTypeID = 2 in the ZNodeShippingRuleType table)
            var shippingRules = shippingBag.ShippingRules.FindAll(ShippingRuleColumn.ShippingRuleTypeID, (int)ZnodeShippingRuleType.RateBasedOnWeight);

            // Determine weight-based shipping rate for each item
            foreach (ZNodeShoppingCartItem cartItem in shippingBag.ShoppingCart.ShoppingCartItems)
            {
                decimal itemShippingCost = 0;
                decimal totalItemWeightQuantity = 0;
                var isRuleApplied = false;

                var shippingRuleTypeId = cartItem.Product.ShippingRuleTypeID.GetValueOrDefault(-1);
                var getsFreeShipping = cartItem.Product.FreeShippingInd;
                var quantity = cartItem.Quantity;

                if (shippingRuleTypeId == (int)ZnodeShippingRuleType.RateBasedOnWeight && (!getsFreeShipping))
                {
                    totalItemWeightQuantity = cartItem.Product.Weight * quantity;
                }

                if (totalItemWeightQuantity > 0)
                {
                    isRuleApplied = ApplyRule(shippingRules, totalItemWeightQuantity, out itemShippingCost);

                    if (isRuleApplied && cartItem.Product.ShipSeparately)
					{
						
                        cartItem.Product.ShippingCost = itemShippingCost + shippingBag.HandlingCharge;
                    }
                    else if (isRuleApplied)
                    {
						
                        cartItem.Product.ShippingCost = itemShippingCost;
                    }
                }

                // Reset if rule is applied
                isRuleApplied = true;

                var applyHandlingCharge = false;

                // Get weight-based rate for addons
                foreach (ZNodeAddOnEntity addOn in cartItem.Product.SelectedAddOns.AddOnCollection)
                {
                    foreach (ZNodeAddOnValueEntity addOnValue in addOn.AddOnValueCollection)
                    {
                        getsFreeShipping = addOnValue.FreeShippingInd;
                        shippingRuleTypeId = addOnValue.ShippingRuleTypeID.GetValueOrDefault(-1);

						if (shippingRuleTypeId == (int)ZnodeShippingRuleType.RateBasedOnWeight && !getsFreeShipping)
                        {
                            var addOnItemWeightQuantity = addOnValue.Weight * quantity;

                            isRuleApplied &= ApplyRule(shippingRules, addOnItemWeightQuantity, out itemShippingCost);

                            applyHandlingCharge |= isRuleApplied;

                            if (isRuleApplied)
                            {
                                addOnValue.ShippingCost = itemShippingCost;
                            }
                        }
                    }
                }

                if (applyHandlingCharge && cartItem.Product.ShipSeparately)
                {
                    shoppingCart.OrderLevelShipping += shippingBag.HandlingCharge;
                }
            }
        }

		private bool ApplyRule(TList<ShippingRule> shippingRules, decimal itemWeightQuantity, out decimal itemShippingCost)
		{
			var isRuleApplied = false;
			itemShippingCost = 0;

			foreach (var rule in shippingRules)
			{
				if (itemWeightQuantity >= rule.LowerLimit && itemWeightQuantity <= rule.UpperLimit)
				{
					itemShippingCost += rule.BaseCost + (rule.PerItemCost * itemWeightQuantity);
					isRuleApplied = true;
				}
			}

			return isRuleApplied;
		}
	}
}
