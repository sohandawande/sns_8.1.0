﻿namespace Znode.Engine.Shipping
{
	/// <summary>
	/// Maps to the ShippingRuleTypeID fields found in the ZNodeShippingRuleType table.
	/// </summary>
	public enum ZnodeShippingRuleType
	{
		FlatRatePerItem = 0,
		RateBasedOnQuantity = 1,
		RateBasedOnWeight = 2,
		FixedRatePerItem = 3,
	}
}
