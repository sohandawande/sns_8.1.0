using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using StructureMap;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Shipping
{
	/// <summary>
	/// Helps manage shipping types.
	/// </summary>
	public class ZnodeShippingManager : ZNodeBusinessBase
	{
		private ZNodeShoppingCart _shoppingCart;
		private ZNodeGenericCollection<IZnodeShippingType> _shippingTypes;

		/// <summary>
		/// Throws a NotImplementedException because this class requires a shopping cart to work.
		/// </summary>
		public ZnodeShippingManager()
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Instantiates all the shipping types and rules that are required for the current shopping cart.
		/// </summary>
		/// <param name="shoppingCart">The current shopping cart.</param>
		public ZnodeShippingManager(ZNodeShoppingCart shoppingCart)
		{
			_shoppingCart = shoppingCart;
			_shippingTypes = new ZNodeGenericCollection<IZnodeShippingType>();

			var shippingId = shoppingCart.Shipping.ShippingID;

			if (shippingId > 0)
			{
				var shippingService = new ShippingService();
				var shippingOption = shippingService.DeepLoadByShippingID(shippingId, true, ZNode.Libraries.DataAccess.Data.DeepLoadType.IncludeChildren, typeof(ShippingType), typeof(TList<ShippingRule>));

				if (shippingOption != null)
				{
					var shippingBag = BuildShippingBag(shippingOption);
					AddShippingTypes(shippingOption, shippingBag);
				}
			}
		}

		/// <summary>
		/// Calculates the shipping cost and updates the shopping cart.
		/// </summary>
		public void Calculate()
		{
			_shoppingCart.OrderLevelShipping = 0;
			_shoppingCart.Shipping.ShippingHandlingCharge = 0;
			_shoppingCart.Shipping.ResponseCode = "0";
			_shoppingCart.Shipping.ResponseMessage = String.Empty;

			ResetShippingCostForCartItemsAndAddOns();
			CalculateEachShippingType();
		}

		/// <summary>
		/// Process anything that must be done before the order is submitted.
		/// </summary>
		/// <returns>True if everything is good for submitting the order; otherwise, false.</returns>
		public bool PreSubmitOrderProcess()
		{
			var allPreConditionsOk = true;

			foreach (ZnodeShippingType shippingType in _shippingTypes)
			{
				// Make sure all pre-conditions are good before letting the customer check out
				allPreConditionsOk &= shippingType.PreSubmitOrderProcess();
			}

			return allPreConditionsOk;
		}

		/// <summary>
		/// Process anything that must be done after the order is submitted.
		/// </summary>      
		public void PostSubmitOrderProcess()
		{
			foreach (ZnodeShippingType shippingType in _shippingTypes)
			{
				shippingType.PostSubmitOrderProcess();
			}
		}

		/// <summary>
		/// Caches all available shipping types in the application cache.
		/// </summary>
		public static void CacheAvailableShippingTypes()
		{
			if (HttpRuntime.Cache["ShippingTypesCache"] == null)
			{
				if (ObjectFactory.Container == null)
				{
					ObjectFactory.Initialize(scanner => scanner.Scan(x =>
					{
						x.AssembliesFromApplicationBaseDirectory();
						x.AddAllTypesOf<IZnodeShippingType>();
					}));
				}
				else
				{
					ObjectFactory.Configure(scanner => scanner.Scan(x =>
					{
						x.AssembliesFromApplicationBaseDirectory();
						x.AddAllTypesOf<IZnodeShippingType>();
					}));
				}

				// Only cache shipping types that have a ClassName and Name; this helps avoid showing base classes in some of the dropdown lists
				var shippingTypes = ObjectFactory.GetAllInstances<IZnodeShippingType>().Where(x => !String.IsNullOrEmpty(x.ClassName) && !String.IsNullOrEmpty(x.Name));
				HttpRuntime.Cache["ShippingTypesCache"] = shippingTypes.ToList();
			}
		}

		/// <summary>
		/// Gets all available shipping types from the application cache.
		/// </summary>
		/// <returns>A list of the available shipping types.</returns>
		public static List<IZnodeShippingType> GetAvailableShippingTypes()
		{
			var list = HttpRuntime.Cache["ShippingTypesCache"] as List<IZnodeShippingType>;
			if (list != null)
			{
				list.Sort((shippingTypeA, shippingTypeB) => String.CompareOrdinal(shippingTypeA.Name, shippingTypeB.Name));
			}
			else
			{
				list = new List<IZnodeShippingType>();
			}

			return list;
		}

		private void ResetShippingCostForCartItemsAndAddOns()
		{
			foreach (ZNodeShoppingCartItem cartItem in _shoppingCart.ShoppingCartItems)
			{
				// Reset each line item shipping cost
				cartItem.ShippingCost = 0;
				cartItem.Product.ShippingCost = 0;

				foreach (ZNodeAddOnEntity addOn in cartItem.Product.SelectedAddOns.AddOnCollection)
				{
					foreach (ZNodeAddOnValueEntity addOnValue in addOn.AddOnValueCollection)
					{
						addOnValue.ShippingCost = 0;
					}
				}
			}
		}

		private void CalculateEachShippingType()
		{
			foreach (ZnodeShippingType shippingType in _shippingTypes)
			{
				shippingType.Calculate();
			}
		}

		private ZnodeShippingBag BuildShippingBag(ZNode.Libraries.DataAccess.Entities.Shipping shippingOption)
		{
			var shippingBag = new ZnodeShippingBag();
			shippingBag.ShoppingCart = _shoppingCart;
			shippingBag.ShippingCode = shippingOption.ShippingCode;
			shippingBag.HandlingCharge = shippingOption.HandlingCharge;
			shippingBag.ShippingRules = shippingOption.ShippingRuleCollection;

			return shippingBag;
		}

		private void AddShippingTypes(ZNode.Libraries.DataAccess.Entities.Shipping shippingOption, ZnodeShippingBag shippingBag)
		{
			var availableShippingTypes = GetAvailableShippingTypes();
			foreach (var t in availableShippingTypes.Where(t => t.ClassName == shippingOption.ShippingTypeIDSource.ClassName))
			{
				try
				{
					var shippingTypeAssembly = Assembly.GetAssembly(t.GetType());
					var shippingType = (IZnodeShippingType)shippingTypeAssembly.CreateInstance(t.ToString());

					if (shippingType != null)
					{
						shippingType.Bind(_shoppingCart, shippingBag);
						_shippingTypes.Add(shippingType);
					}
				}
				catch (Exception ex)
				{
					ZNodeLogging.LogMessage("Error while instantiating shipping type: " + t);
					ZNodeLogging.LogMessage(ex.ToString());
				}
			}
		}
	}
}
