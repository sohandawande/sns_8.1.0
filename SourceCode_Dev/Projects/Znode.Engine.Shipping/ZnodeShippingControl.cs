﻿namespace Znode.Engine.Shipping
{
	public enum ZnodeShippingControl
	{
		Profile,
		ServiceCodes,
		DisplayName,
		InternalCode,
		HandlingCharge,
		Countries
	}
}
