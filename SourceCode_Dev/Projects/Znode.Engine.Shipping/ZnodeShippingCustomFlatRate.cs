using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Shipping
{
	public class ZnodeShippingCustomFlatRate : ZNodeBusinessBase
	{
		/// <summary>
		/// Calculates the custom fixed shipping rate.
		/// </summary>
		/// <param name="shoppingCart">The current shopping cart.</param>
		/// <param name="shippingBag">The shipping data for the custom shipping type.</param>
        public void Calculate(ZNodeShoppingCart shoppingCart, ZnodeShippingBag shippingBag)
        {
            // Filter for flat rate per item (ShippingRuleTypeID = 0 in the ZNodeShippingRuleType table)
            var shippingRules = shippingBag.ShippingRules.FindAll(ShippingRuleColumn.ShippingRuleTypeID, (int)ZnodeShippingRuleType.FlatRatePerItem);

            // Determine flat shipping rate for each item
            foreach (ZNodeShoppingCartItem cartItem in shippingBag.ShoppingCart.ShoppingCartItems)
            {
                decimal itemShippingCost = 0;
                var totalItemQuantity = 0;
                var isRuleApplied = false;

                var shippingRuleTypeId = cartItem.Product.ShippingRuleTypeID.GetValueOrDefault(-1);
                var getsFreeShipping = cartItem.Product.FreeShippingInd;
                var quantity = cartItem.Quantity;

                if (shippingRuleTypeId == (int)ZnodeShippingRuleType.FlatRatePerItem && (!getsFreeShipping))
                {
                    totalItemQuantity += quantity;
                }

                if (totalItemQuantity > 0)
                {
                    isRuleApplied = ApplyRule(shippingRules, totalItemQuantity, out itemShippingCost);

                    if (isRuleApplied && cartItem.Product.ShipSeparately)
                    {
						
                        cartItem.Product.ShippingCost = itemShippingCost + shippingBag.HandlingCharge;
                    }
                    else if (isRuleApplied)
                    {
						
                        cartItem.Product.ShippingCost = itemShippingCost;
                    }
                }

                // Reset if rule is applied
                isRuleApplied = true;

                var applyHandlingCharge = false;

                // Get flat rate for addons
                foreach (ZNodeAddOnEntity addOn in cartItem.Product.SelectedAddOns.AddOnCollection)
                {
                    foreach (ZNodeAddOnValueEntity addOnValue in addOn.AddOnValueCollection)
                    {
                        getsFreeShipping = addOnValue.FreeShippingInd;
                        shippingRuleTypeId = addOnValue.ShippingRuleTypeID.GetValueOrDefault(-1);

						if (shippingRuleTypeId == (int)ZnodeShippingRuleType.FlatRatePerItem && !getsFreeShipping)
                        {
                            isRuleApplied &= ApplyRule(shippingRules, quantity, out itemShippingCost);

                            applyHandlingCharge |= isRuleApplied;

                            if (isRuleApplied)
                            {
                                addOnValue.ShippingCost = itemShippingCost;
                            }
                        }
                    }
                }

                if (applyHandlingCharge && cartItem.Product.ShipSeparately)
                {
                    shoppingCart.OrderLevelShipping += shippingBag.HandlingCharge;
                }
            }
        }

		private bool ApplyRule(TList<ShippingRule> shippingRules, int itemQuantity, out decimal itemShippingCost)
		{
			var isRuleApplied = false;
			itemShippingCost = 0;

			foreach (var rule in shippingRules)
			{
				itemShippingCost += rule.BaseCost + (rule.PerItemCost * itemQuantity);
				isRuleApplied = true;
			}

			return isRuleApplied;
		}
	}
}
