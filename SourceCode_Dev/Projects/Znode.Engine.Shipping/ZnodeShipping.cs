using System;

namespace Znode.Engine.Shipping
{
    /// <summary>
    /// Property bag for settings used by the Shipping Rules.
    /// </summary>
    [Serializable()]
    public class ZnodeShipping : ZNode.Libraries.ECommerce.Entities.ZNodeShipping
    {
    }
}
