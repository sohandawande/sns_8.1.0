﻿using System;
using System.Collections.Generic;
using Znode.Engine.Shipping.Usps;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Shipping
{
    public class ZnodeShippingUsps : ZnodeShippingType
    {
        public ZnodeShippingUsps()
        {
            Name = "USPS";
            Description = "Calculates shipping rates when using the United States Postal Service.";

            Controls.Add(ZnodeShippingControl.Profile);
            Controls.Add(ZnodeShippingControl.DisplayName);
            Controls.Add(ZnodeShippingControl.InternalCode);
            Controls.Add(ZnodeShippingControl.HandlingCharge);
            Controls.Add(ZnodeShippingControl.Countries);
        }

        /// <summary>
        /// Calculates shipping rates when using the United States Postal Service.
        /// </summary>
        public override void Calculate()
        {
            decimal itemShippingRate = 0;

            // Instantiate the USPS agent
            var usps = new UspsAgent();
            var weightUnit = ZNodeConfigManager.SiteConfig.WeightUnit;
            // General settings
            usps.ShippingAddress = new ZNode.Libraries.DataAccess.Entities.Address();
            usps.OriginZipCode = ZNodeConfigManager.SiteConfig.ShippingOriginZipCode;
            usps.PostageDeliveryUnitZip5 = ShoppingCart.Payment.ShippingAddress.PostalCode;
            usps.ShippingAddress = ShoppingCart.Payment.ShippingAddress;

            // Split the items that ship separately from the items that ship together
            SplitShipSeparatelyFromShipTogether();

            // Check weight unit
            if (weightUnit.Length > 0)
            {
                weightUnit = weightUnit.TrimEnd(new char[] { 'S' });
            }

            //get package weight limit
            decimal packageWeightLimitlbs = GetWeightLimit(3);

            // Shipping estimate for ship-together package
            if (ShipTogetherItems.Count > 0)
            {
                var shipTogetherPackage = new ZnodeShippingPackage(ShipTogetherItems, false, true);

                usps.Sku = ShipTogetherItems[0].Product.SKU;

                //convert waight in LBS
                if (weightUnit.ToUpper().Equals(weightUnitKgs))
                {
                    WeightUnitBase = weightUnit;
                    shipTogetherPackage.Weight = ConvertWeightKgToLbs(shipTogetherPackage.Weight);
                    weightUnit = WeightUnitBase = weightUnitLbs;
                }

                var weightArray = shipTogetherPackage.Weight.ToString("N2").Split('.');
                usps.WeightInPounds = weightArray[0];

                var remainder = shipTogetherPackage.Weight - Convert.ToDecimal(weightArray[0]);
                var ounces = remainder * 16;

                usps.WeightInOunces = ounces.ToString();

                #region Znode Version 7.2.2 - Split PackageWeight

                //Splite Weight if Weight is greter than 70lbs
                List<decimal> spWeight = new List<decimal>();

                if (Convert.ToDecimal(usps.WeightInPounds) > packageWeightLimitlbs)
                {
                    spWeight = usps.SplitPackageWeight(Convert.ToDecimal(usps.WeightInPounds), packageWeightLimitlbs);
                }

                if (spWeight.Count > 0)
                {
                    for (int listItemCount = 0; listItemCount < spWeight.Count; listItemCount++)
                    {
                        usps.WeightInPounds = spWeight[listItemCount].ToString();

                        // Get shipping rate for each item
                        var isCalculated = usps.CalculateShippingRate();
                        if (isCalculated)
                        {
                            itemShippingRate += usps.ShippingRate;
                        }
                    }
                }
                else
                {
                    // Get shipping rate for each item
                    var isCalculated = usps.CalculateShippingRate();
                    if (isCalculated)
                    {
                        itemShippingRate += usps.ShippingRate;
                    }
                }

                #endregion

                // Add handling charge for ship-together package
                itemShippingRate += ShippingBag.HandlingCharge;

                if (usps.ErrorCode != "0")
                {
                    ShoppingCart.Shipping.ResponseCode = usps.ErrorCode;
                    ShoppingCart.Shipping.ResponseMessage = GenericShippingErrorMessage();
                    ShoppingCart.AddErrorMessage = GenericShippingErrorMessage();
                    ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.GeneralError, "Shipping error: " + usps.ErrorCode + " " + usps.ErrorDescription);
                }
            }

            // Shipping estimate for ship-separately packages
            if (ShipSeparatelyItems.Count > 0)
            {
                foreach (ZNodeShoppingCartItem separateItem in ShipSeparatelyItems)
                {
                    var singleItemlist = new ZNodeGenericCollection<ZNodeShoppingCartItem>();
                    singleItemlist.Add(separateItem);

                    var seperateItemPackage = new ZnodeShippingPackage(singleItemlist, false, false);

                    usps.Sku = separateItem.Product.SKU;


                    //convert waight in LBS
                    if (weightUnit.ToUpper().Equals(weightUnitKgs))
                    {
                        WeightUnitBase = weightUnit;
                        seperateItemPackage.Weight = ConvertWeightKgToLbs(seperateItemPackage.Weight);
                        weightUnit = WeightUnitBase = weightUnitLbs;
                    }

                    var weightArray = seperateItemPackage.Weight.ToString("N2").Split('.');
                    usps.WeightInPounds = weightArray[0];

                    var remainder = seperateItemPackage.Weight - Convert.ToDecimal(weightArray[0]);
                    var ounces = remainder * 16;
                    usps.WeightInOunces = ounces.ToString();

                    #region Znode Version 7.2.2 - Split PackageWeight



                    //Splite Weight if Weight is greter than 70 lbs
                    List<decimal> spWeight = new List<decimal>();
                    if (Convert.ToDecimal(usps.WeightInOunces) > packageWeightLimitlbs)
                    {
                        spWeight = usps.SplitPackageWeight(Convert.ToDecimal(usps.WeightInOunces), packageWeightLimitlbs);
                    }

                    if (spWeight.Count > 0)
                    {
                        for (int listItemCount = 0; listItemCount < spWeight.Count; listItemCount++)
                        {
                            usps.WeightInPounds = spWeight[listItemCount].ToString();
                            var isCalculated = usps.CalculateShippingRate();
                            if (isCalculated)
                            {
                                itemShippingRate = usps.ShippingRate;
                            }

                            // Get shipping rate for each single item and multiply with quantity to total charges of order line item
                            itemShippingRate += itemShippingRate * separateItem.Quantity;
                        }
                    }
                    else
                    {
                        var isCalculated = usps.CalculateShippingRate();
                        if (isCalculated)
                        {
                            itemShippingRate = usps.ShippingRate;
                        }

                        // Get shipping rate for each single item and multiply with quantity to total charges of order line item
                        itemShippingRate += itemShippingRate * separateItem.Quantity;
                    }

                    #endregion


                    // Add handling charge for ship-seperately item and multiply with quantity to total charges of order line item
                    itemShippingRate += ShippingBag.HandlingCharge * separateItem.Quantity;

                    if (usps.ErrorCode != "0")
                    {
                        ShoppingCart.Shipping.ResponseCode = usps.ErrorCode;
                        ShoppingCart.Shipping.ResponseMessage = GenericShippingErrorMessage();
                        ShoppingCart.AddErrorMessage = GenericShippingErrorMessage();
                        ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.GeneralError, String.Format("Shipping error for {0}: {1} {2}", separateItem.Product.Name, usps.ErrorCode, usps.ErrorDescription));
                    }
                }
            }

            ShoppingCart.OrderLevelShipping += itemShippingRate;
        }

        /// <summary>
        /// Checks the response code before the order is submitted.
        /// </summary>
        /// <returns>True if the response code is 0; otherwise, false.</returns>
        public override bool PreSubmitOrderProcess()
        {
            if (ShoppingCart.Shipping.ResponseCode != "0")
            {
                ShoppingCart.AddErrorMessage = GenericShippingErrorMessage();
                ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.GeneralError, "Shipping error in PreSubmitOrderProcess: " + ShoppingCart.Shipping.ResponseCode + " " + ShoppingCart.Shipping.ResponseMessage);
                return false;
            }

            return true;
        }
    }
}
