﻿using Znode.Plugin.Framework.Models;

namespace Znode.Plugin.Framework.Maps
{
    public class PluginVmMap
    {
        public PluginModel ToVm(IPlugin source)
        {
            return new PluginModel()
            {
                Name = source.Name,
                Title = source.Title,
                Version = source.Version.ToString(),
                IsInstalled = source.IsInstalled,
            };
        }

        public PluginModel ToVm(IAdminPlugin source)
        {
            var model = ToVm(source as IPlugin);

            model.ConfigurationEndPoint = source.EntryControllerName;

            return model;
        }

        public PluginModel ToVm(IApiPlugin source)
        {
            var model = ToVm(source as IPlugin);

            // currently IApiPlugin does not have more properties than IPlugin

            return model;
        }
    }
}