﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Hosting;
using Znode.Plugin.Framework.Domain.Models;

namespace Znode.Plugin.Framework.Domain.Repositories
{
    public class JsonRepository<T> : IRepository<T> where T : IEntity
    {
        private readonly string _fileName;

        private List<T> _list = new List<T>();

        public JsonRepository()
        {
            _fileName = string.Format("{0}/{1}.json", HostingEnvironment.MapPath("~/plugins"), typeof(T).Name);
        }

        public IQueryable<T> List()
        {
            if (!File.Exists(_fileName))
            {
                _list = new List<T>();
            }
            else
            {
                using (var r = new StreamReader(_fileName))
                {
                    var json = r.ReadToEnd();
                    _list = JsonConvert.DeserializeObject<List<T>>(json);
                }
            }

            return _list.AsQueryable();
        }

        public void Save(List<T> entities)
        {
            if (File.Exists(_fileName))
            {
                File.Delete(_fileName);
            }

            using (var fs = File.Open(_fileName, FileMode.OpenOrCreate))
            {
                using (var sw = new StreamWriter(fs))
                {
                    using (JsonWriter jw = new JsonTextWriter(sw))
                    {
                        jw.Formatting = Formatting.Indented;
                        var serializer = new JsonSerializer();
                        serializer.Serialize(jw, entities);
                        jw.Flush();
                    }
                }
            }
        }
    }
}