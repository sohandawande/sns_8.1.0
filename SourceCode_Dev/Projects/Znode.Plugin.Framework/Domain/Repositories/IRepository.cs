﻿using System.Collections.Generic;
using System.Linq;
using Znode.Plugin.Framework.Domain.Models;

namespace Znode.Plugin.Framework.Domain.Repositories
{
    public interface IRepository<T> where T : IEntity
    {
        IQueryable<T> List();
        void Save(List<T> entities);
    }
}