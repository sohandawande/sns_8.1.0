﻿using System.Linq;
using Znode.Plugin.Framework.Domain.Models;
using Znode.Plugin.Framework.Domain.Repositories;
using Znode.Plugin.Framework.Maps;
using Znode.Plugin.Framework.Models;

namespace Znode.Plugin.Framework.Domain.Services
{
    public class PluginService<T> where T : IPlugin
    {
        private readonly IRepository<PluginEntity> _repository;
        private readonly PluginVmMap _pluginMap;
        private readonly PluginEntityMap _entityMap;

        public PluginService()
        {
            _repository = new JsonRepository<PluginEntity>();
            _pluginMap = new PluginVmMap();
            _entityMap = new PluginEntityMap();
        }

        public ConfigurationModel GetPlugins()
        {
            var installedPlugins = _repository.List();

            foreach (var plugin in PluginManager<T>.Current.GetPlugins())
            {
                var persistedPlugin = installedPlugins.SingleOrDefault(x => x.Name == plugin.Name);
                plugin.IsInstalled = persistedPlugin != null && persistedPlugin.Installed;
            }

            var model = new ConfigurationModel()
            {
                Plugins = PluginManager<T>.Current
                            .GetPlugins()
                    //.Where(x => x.Name != Assembly.GetAssembly(GetType()).GetName().Name)
                            .Select(x => _pluginMap.ToVm((IAdminPlugin)x)).ToList()
            };

            return model;
        }

        public void Install(string name)
        {
            var persistedPlugin = _repository.List().SingleOrDefault(x => x.Name == name);
            var inMemoryPlugin = PluginManager<T>.Current.GetPlugins().SingleOrDefault(x => x.Name == name);

            if (inMemoryPlugin == null) return;

            if (persistedPlugin != null && persistedPlugin.Installed)
            {
                // already installed
                return;
            }

            // continue only if the plugin has not already been installed
            // and its currently loaded
            inMemoryPlugin.Install();
            inMemoryPlugin.IsInstalled = true;

            var pluginsToBePersisted = PluginManager<T>.Current
                .GetPlugins()
                .Select(x => _entityMap.ToEntity(x))
                .ToList();

            _repository.Save(pluginsToBePersisted);
        }

        public void Uninstall(string name)
        {
            var persistedPlugin = _repository.List().SingleOrDefault(x => x.Name == name);
            var inMemoryPlugin = PluginManager<T>.Current.GetPlugins().SingleOrDefault(x => x.Name == name);

            if (inMemoryPlugin == null)
            {
                // the plugin is not loaded, nothing to do
                return;
            }

            if (persistedPlugin != null && persistedPlugin.Installed)
            {
                // continue only if the plugin has not already been installed
                // and its currently loaded
                inMemoryPlugin.Uninstall();
                inMemoryPlugin.IsInstalled = false;

                var pluginsToBePersisted = PluginManager<T>.Current
                    .GetPlugins()
                    .Select(x => _entityMap.ToEntity(x))
                    .ToList();

                _repository.Save(pluginsToBePersisted);
            }
        }
    }
}