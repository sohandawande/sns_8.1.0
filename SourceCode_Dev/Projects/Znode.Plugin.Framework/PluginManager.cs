﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Znode.Plugin.Framework
{
    public class PluginManager<T> where T : IPlugin
    {
        public PluginManager()
        {
            Plugins = new Dictionary<T, Assembly>();
        }

        private static PluginManager<T> _current;
        public static PluginManager<T> Current
        {
            get { return _current ?? (_current = new PluginManager<T>()); }
        }

        /// <summary>
        /// This should be internal, but since we had to split
        /// PluginFramework into different assemblies because Api is on an older
        /// version of asp.net framework, this needs to be public.
        /// If Api and Web have the same version of Mvc, the Plugin Frameowrk assemblies
        /// should be collapsed.
        /// </summary>
        public Dictionary<T, Assembly> Plugins { get; set; }

        public IEnumerable<T> GetPlugins()
        {
            return Plugins.Select(m => m.Key).OrderBy(x => x.Order).ToList();
        }

        public T GetPlugin(string name)
        {
            return GetPlugins().FirstOrDefault(m => m.Name == name);
        }
    }
}