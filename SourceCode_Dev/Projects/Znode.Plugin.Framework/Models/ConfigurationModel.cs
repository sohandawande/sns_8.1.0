﻿using System.Collections.Generic;

namespace Znode.Plugin.Framework.Models
{
    public class ConfigurationModel
    {
        public List<PluginModel> Plugins { get; set; }
    }
}