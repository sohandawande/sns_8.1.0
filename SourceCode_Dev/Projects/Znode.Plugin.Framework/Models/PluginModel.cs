﻿using System.ComponentModel;

namespace Znode.Plugin.Framework.Models
{
    public class PluginModel
    {
        [DisplayName("Title")]
        public string Title { get; set; }
        [DisplayName("Plugin Name")]
        public string Name { get; set; }
        [DisplayName("Version")]
        public string Version { get; set; }
        [DisplayName("Installed?")]
        public bool IsInstalled { get; set; }
        [DisplayName("Configuration End Point")]
        public string ConfigurationEndPoint { get; set; }
    }
}