﻿using System;
using System.Collections;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web.Compilation;
using System.Web.Hosting;

namespace Znode.Plugin.Framework
{
    /// <summary>
    /// Currently, there is only one PreApplicationInit for both Admin and API. 
    /// A potential improvement would be to create IApiPlugin for api plugins 
    /// and create either separate PreApplicationInit for each hosting app, or 
    /// use a strategy to call the appropriate preinit depending on the host (Admin or Api).
    /// </summary>
    public class PluginConfig<T> where T : IPlugin
    {
        static PluginConfig()
        {
            string pluginsPath = HostingEnvironment.MapPath("~/plugins");
            string pluginsTempPath = HostingEnvironment.MapPath("~/plugins/temp");

            if (pluginsPath == null || pluginsTempPath == null)
                throw new DirectoryNotFoundException("plugins");

            PluginFolder = new DirectoryInfo(pluginsPath);
            TempPluginFolder = new DirectoryInfo(pluginsTempPath);
        }

        /// <summary>
        /// The source plugin folder from which to copy from
        /// </summary>
        /// <remarks>
        /// This folder can contain sub folders to organize plugin types
        /// </remarks>
        private static readonly DirectoryInfo PluginFolder;

        /// <summary>
        /// The folder to  copy the plugin DLLs to use for running the app
        /// </summary>
        private static readonly DirectoryInfo TempPluginFolder;

        /// <summary>
        /// Initialize method that registers all plugins
        /// </summary>
        public static void InitializePlugins()
        {
            Directory.CreateDirectory(TempPluginFolder.FullName);
            try
            {
                //clear out plugins
                FileInfo[] assemblyFiles = TempPluginFolder.GetFiles("*.dll", SearchOption.AllDirectories);
                foreach (FileInfo file in assemblyFiles)
                {
                    try
                    {
                        file.Delete();
                    }
                    catch (Exception)
                    {

                    }
                }

                //copy files
                foreach (FileInfo plug in PluginFolder.GetFiles("*.dll", SearchOption.AllDirectories))
                {
                    try
                    {
                        DirectoryInfo directory = Directory.CreateDirectory(TempPluginFolder.FullName);
                        File.Copy(plug.FullName, Path.Combine(directory.FullName, plug.Name), true);
                    }
                    catch (Exception)
                    {

                    }
                }

                // * This will put the plugin assemblies in the 'Load' context
                // This works but requires a 'probing' folder be defined in the web.config
                // eg: <probing privatePath="plugins/temp" />
                var assemblies = assemblyFiles.Where(file => file.Name != "System.Data.SQLite.dll")
                    .Select(x => AssemblyName.GetAssemblyName(x.FullName))
                    .Select(x => Assembly.Load(x.FullName));

                foreach (var assembly in assemblies)
                {
                    // DotNetOpenAuth related Dll Raised Error "Inheritance security rules violated by type: 'DotNetOpenAuth.Messaging.OutgoingWebResponseActionResult'.
                    // Derived types must either match the security accessibility of the base type or be less accessible."
                    // So added Check To Skip the Dll's
                    if (!assembly.FullName.Contains("DotNetOpenAuth"))
                    {
                            PluginScanStrategy(assembly);
                    }
                }
            }
            catch (ReflectionTypeLoadException ex)
            {
                StringBuilder sb = new StringBuilder();
                foreach (Exception exSub in ex.LoaderExceptions)
                {
                    sb.AppendLine(exSub.Message);
                    FileNotFoundException exFileNotFound = exSub as FileNotFoundException;
                    if (exFileNotFound != null)
                    {
                        if (!string.IsNullOrEmpty(exFileNotFound.FusionLog))
                        {
                            sb.AppendLine("Fusion Log:");
                            sb.AppendLine(exFileNotFound.FusionLog);
                        }
                    }
                    sb.AppendLine();
                }
                string errorMessage = sb.ToString();
                //Display or log the error based on your application.
            }
        }

        private static void PluginScanStrategy(Assembly assembly)
        {
            // find all instances of IPlugin
            Type type = assembly.GetTypes().FirstOrDefault(x => x.GetInterface(typeof(T).Name) != null);

            if (type != null)
            {
                //Add the plugin as a reference to the application
                BuildManager.AddReferencedAssembly(assembly);

                //Add the modules to the PluginManager to manage them later
                var module = (T)Activator.CreateInstance(type);
                PluginManager<T>.Current.Plugins.Add(module, assembly);
            }
        }
    }
}