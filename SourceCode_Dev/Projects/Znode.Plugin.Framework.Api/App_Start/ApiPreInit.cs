﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Compilation;
using System.Web.Hosting;
using Znode.Plugin.Framework.Api;

[assembly: PreApplicationStartMethod(typeof(ApiPreApplicationStart), "InitializePlugins")]

namespace Znode.Plugin.Framework.Api
{
    /// <summary>
    /// PreApplicationStartMethod for API applications.
    /// </summary>
    public class ApiPreApplicationStart
    {
        /// <summary>
        /// Initialize method that registers all plugins
        /// </summary>
        public static void InitializePlugins()
        {
            PluginConfig<IApiPlugin>.InitializePlugins();
        }
    }
}