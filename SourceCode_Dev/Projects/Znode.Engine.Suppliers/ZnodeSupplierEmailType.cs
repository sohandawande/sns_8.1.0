﻿using ZNode.Libraries.ECommerce.Fulfillment;
using ZNode.Libraries.ECommerce.ShoppingCart;

namespace Znode.Engine.Suppliers
{
	/// <summary>
	/// Provides the base implementation for all supplier email types.
	/// </summary>
	public abstract class ZnodeSupplierEmailType : ZnodeSupplierType, IZnodeSupplierEmailType
	{
		public virtual bool SendEmailReceipt(ZNodeOrderFulfillment order, ZNodeShoppingCart shoppingCart, ZnodeSupplier supplier)
		{
			// Nothing to see here, override this method in a derived class
			return true;
		}
	}
}
