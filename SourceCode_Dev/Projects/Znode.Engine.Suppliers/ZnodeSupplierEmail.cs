using System;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Fulfillment;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Suppliers
{
    public partial class ZnodeSupplierEmail : ZnodeSupplierEmailType
    {
        public ZnodeSupplierEmail()
        {
            Name = "Email";
            Description = "Sends an email receipt of the order to the supplier.";

            Controls.Add(ZnodeSupplierControl.NotificationEmail);
            Controls.Add(ZnodeSupplierControl.EmailNotificationTemplate);
            Controls.Add(ZnodeSupplierControl.DisplayOrder);
        }

        /// <summary>
        /// Sends an email receipt of the order to the supplier.
        /// </summary>
        /// <param name="order">The order being processed.</param>
        /// <param name="shoppingCart">The current shopping cart.</param>
        /// <param name="supplier">The supplier that will receive the email receipt.</param>
        /// <returns>True if sending email receipt was successful; otherwise, false.</returns>
        public override bool SendEmailReceipt(ZNodeOrderFulfillment order, ZNodeShoppingCart shoppingCart, ZnodeSupplier supplier)
        {
            var result = false;

            try
            {
                if (supplier.EnableEmailNotification)
                {
                    var to = supplier.NotificationEmailID;
                    var from = ZNodeConfigManager.SiteConfig.SalesEmail;
                    var bcc = ZNodeConfigManager.SiteConfig.SalesEmail;
                    var subject = ZNodeCatalogManager.MessageConfig.GetMessage("OrderReceiptEmailSubject");

                    var receipt = new ZnodeReceipt(order, shoppingCart);
                    var body = receipt.GetHtmlReceiptForEmail(supplier);

                    #region PRFT Custom Code
                    body = GetOrderReceiptTemplate(body);
                    #endregion

                    // Znode Version 7.2.2
                    // Change Description - Start 
                    // Remove print link from the body of email. 
                    int removeStartIndex = body.IndexOf("<!--~");
                    int removeLastIndex = body.IndexOf("~-->") + "~-->".Length; ;
                    body = Slice(body, removeStartIndex, removeLastIndex);
                    // Change Description - End

                    ZNodeEmail.SendEmail(to, from, bcc, subject, body, true);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                result = false;
                ZNodeLogging.LogMessage("There was a problem sending email to the supplier: " + ex.Message);
            }

            return result;
        }

        #region  Znode Version 7.2.2

        /// <summary>
        ///  Znode Version 7.2.2
        ///  Takes source string and removes substring for specified start index and end index.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="startIndex"></param>
        /// <param name="endIndex"></param>
        /// <returns>sliced string</returns>
        private string Slice(string source, int startIndex, int endIndex)
        {
            if (startIndex > 0)
            {
                if (endIndex < 0)
                {
                    endIndex = source.Length + endIndex;
                }
                int len = endIndex - startIndex;
                return source.Remove(startIndex, len);
            }
            return source;
        }

        #endregion
    }
}
