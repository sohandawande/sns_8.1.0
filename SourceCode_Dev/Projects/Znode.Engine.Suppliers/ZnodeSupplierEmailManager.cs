using System;
using System.Linq;
using System.Reflection;
using ZNode.Libraries.ECommerce.Fulfillment;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.Suppliers
{
	/// <summary>
	/// Helps manage supplier emails.
	/// </summary>
	public class ZnodeSupplierEmailManager : ZnodeSupplierManager
	{
		public ZnodeSupplierEmailManager(ZNodeOrderFulfillment order, ZNodeShoppingCart shoppingCart) : base(order, shoppingCart)
		{
		}

		/// <summary>
		/// Sends an email receipt of the order to each supplier and the customer.
		/// </summary>
		/// <returns>True if sending emails were successful; otherwise, false.</returns>
		public bool SendEmailReceipt()
		{
			var result = true;

			// Send email receipt to each supplier in the list
			foreach (ZnodeSupplier supplier in Suppliers)
			{
				result &= SendReceiptForEachSupplierEmailType(supplier);
			}

			// Send email receipt to customer
			result &= SendEmailReceiptToCustomer(Order, ShoppingCart);

			return result;
		}

		private bool SendReceiptForEachSupplierEmailType(ZnodeSupplier supplier)
		{
			var result = true;

			var availableSupplierTypes = GetAvailableSupplierTypes();
			foreach (var t in availableSupplierTypes.Where(t => t.ClassName == supplier.ClassName))
			{
				try
				{
					// Create instance of supplier type and call it
					var supplierTypeAssembly = Assembly.GetAssembly(t.GetType());
					var supplierType = (IZnodeSupplierEmailType)supplierTypeAssembly.CreateInstance(t.ToString());

					if (supplierType != null)
					{
						result &= supplierType.SendEmailReceipt(Order, ShoppingCart, supplier);
					}
				}
				catch (Exception ex)
				{
					ZNodeLogging.LogMessage("Error while instantiating supplier type: " + t);
					ZNodeLogging.LogMessage(ex.ToString());
				}
			}

			return result;
		}
	}
}
