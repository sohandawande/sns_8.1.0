﻿using ZNode.Libraries.ECommerce.Fulfillment;
using ZNode.Libraries.ECommerce.ShoppingCart;

namespace Znode.Engine.Suppliers
{
	/// <summary>
	/// This is the root interface for all supplier email types.
	/// </summary>
	public interface IZnodeSupplierEmailType : IZnodeSupplierType
	{
		bool SendEmailReceipt(ZNodeOrderFulfillment order, ZNodeShoppingCart shoppingCart, ZnodeSupplier supplier);
	}
}
