﻿namespace Znode.Engine.Suppliers
{
	public enum ZnodeSupplierControl
	{
		NotificationEmail,
		EmailNotificationTemplate,
		DisplayOrder
	}
}
