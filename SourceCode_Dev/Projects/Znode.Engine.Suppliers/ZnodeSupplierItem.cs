using System;

namespace Znode.Engine.Suppliers
{  
	[Serializable]
	public class ZnodeSupplierItem
	{
		public string ProductSupplierId { get; set; }
		public string Sku { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public int Quantity { get; set; }
		public decimal UnitPrice { get; set; }

		public decimal ExtendedPrice
		{
			get { return UnitPrice * Quantity; }
		}
	}
}
