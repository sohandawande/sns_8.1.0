﻿using System;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Fulfillment;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Suppliers
{
    public class ZnodeReceipt : ZNodeBusinessBase
    {
        public PortalService PortalService { get; set; }
        public ZNodeOrderFulfillment Order { get; set; }
        public ZNodeShoppingCart ShoppingCart { get; set; }
        public string FeedbackUrl { get; set; }
        public bool FromApi { get; set; }
        public ZNodeShoppingCart ApiShoppingCart { get; set; }

        public ZnodeReceipt(ZNodeOrderFulfillment order)
        {
            PortalService = new PortalService();
            Order = order;
            SetCurrentShoppingCart(order);
        }

        public ZnodeReceipt(ZNodeOrderFulfillment order, ZNodeShoppingCart shoppingCart)
        {
            PortalService = new PortalService();
            Order = order;
            ShoppingCart = shoppingCart;
            SetCurrentShoppingCart(order);
        }

        public ZnodeReceipt(ZNodeOrderFulfillment order, string feedbackUrl)
        {
            PortalService = new PortalService();
            Order = order;
            FeedbackUrl = feedbackUrl;
            SetCurrentShoppingCart(order);
        }

        private void SetCurrentShoppingCart(ZNodeOrderFulfillment order)
        {
            // Check to set cart based on portal ID
            if (order != null && ShoppingCart == null)
            {
                var cart = ZNodeShoppingCart.CurrentShoppingCart().PortalCarts.FirstOrDefault(x => x.PortalID == order.PortalId);
                if (cart != null)
                {
                    ShoppingCart = cart;
                }
            }
        }

        /// <summary>
        /// Generates the HTML used in email receipts.
        /// </summary>
        /// <param name="supplier">The supplier that will receive the email.</param>
        /// <param name="includeReceiptStylesheet">Flag that determines whether or not to include the receipt stylesheet.</param>
        /// <returns>The generated HTML used in email receipts.</returns>
        private string GenerateHtmlReceiptWithParser(ZnodeSupplier supplier, bool includeReceiptStylesheet)
        {
            var currentCulture = ZNodeCatalogManager.CultureInfo;
            var templatePathDefault = HttpContext.Current.Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath + "Receipt_en.htm");
            var templatePathMobile = HttpContext.Current.Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath + "Receipt_Mobile.htm");
            var templatePathCulture = HttpContext.Current.Server.MapPath("~/Themes/" + ZNodeCatalogManager.Theme + "/Receipt/" + "Receipt_" + currentCulture + ".htm");
            var templatePathNoCulture = HttpContext.Current.Server.MapPath("~/Themes/" + ZNodeCatalogManager.Theme + "/Receipt/" + "Receipt.htm");
            
            // Set initial path for template
            var templatePath = !String.IsNullOrEmpty(currentCulture) ? templatePathCulture : templatePathNoCulture;

            // Check to use mobile template
            if (ZNodeCatalogManager.Theme == "Mobile" || ZNodeCatalogManager.Theme == "MobileApp" || (HttpContext.Current.Session["cs"] != null && ZNodeConfigManager.SiteConfig.MobileTheme != null))
            {
                templatePath = templatePathMobile;
            }

            // If template doesn't exist, set to default
            var fileinfo = new FileInfo(templatePath);
            if (!fileinfo.Exists)
            {
                templatePath = templatePathDefault;
            }

            ZNodeLogging.LogMessage("initial Template Path after file exists or not - " + templatePath);

            // Create order table
            var orderTable = new DataTable();

            // Additional info
            orderTable.Columns.Add("SiteName");
            orderTable.Columns.Add("ReceiptText");
            orderTable.Columns.Add("CustomerServiceEmail");
            orderTable.Columns.Add("CustomerServicePhoneNumber");
            orderTable.Columns.Add("FeedBack");
            orderTable.Columns.Add("AdditionalInstructions");
            orderTable.Columns.Add("AdditionalInstructLabel");

            // Payment info
            orderTable.Columns.Add("CardTransactionID");
            orderTable.Columns.Add("CardTransactionLabel");
            orderTable.Columns.Add("PaymentName");

            orderTable.Columns.Add("PONumber");
            orderTable.Columns.Add("PurchaseNumberLabel");

            // Customer info
            orderTable.Columns.Add("OrderId");
            orderTable.Columns.Add("OrderDate");
            orderTable.Columns.Add("AccountId");
            orderTable.Columns.Add("BillingAddress");
            orderTable.Columns.Add("ShippingAddress");
            orderTable.Columns.Add("PromotionCode");
            orderTable.Columns.Add("TotalCost");
            orderTable.Columns.Add("StyleSheetPath");
            //PRFT Custom Code:Start
            orderTable.Columns.Add("ERPOrderNumber");
            //PRFT Custom Code:End

            // Create order amount table
            var orderAmountTable = new DataTable();
            orderAmountTable.Columns.Add("Title");
            orderAmountTable.Columns.Add("Amount");

            // Create new row
            var orderRow = orderTable.NewRow();

            if (includeReceiptStylesheet)
            {
                try
                {
                    var receiptCss = HttpContext.Current.Server.MapPath("~/Themes/" + ZNodeCatalogManager.Theme + "/Receipt/receipt.css");
                    var receiptCssMobile = HttpContext.Current.Server.MapPath("~/Themes/" + ZNodeConfigManager.SiteConfig.MobileTheme + "/Receipt/receipt.css");

                    // Default to normal CSS file
                    var cssPath = receiptCss;

                    // Check to use mobile CSS
                    if (HttpContext.Current.Session["cs"] != null && ZNodeConfigManager.SiteConfig.MobileTheme != null)
                    {
                        cssPath = receiptCssMobile;
                    }

                    using (var streamReader = new StreamReader(cssPath))
                    {
                        orderRow["StyleSheetPath"] = streamReader.ReadToEnd();
                    }
                }
                catch (Exception ex)
                {
                    ZNodeLogging.LogMessage("There was a problem including the stylesheet with the email receipt: " + ex.Message);
                }
            }

            var portal = PortalService.GetByPortalID(Order.PortalId);

            // Additional info
            orderRow["SiteName"] = (portal ?? ZNodeConfigManager.SiteConfig).StoreName;
            orderRow["ReceiptText"] = ZNodeCatalogManager.MessageConfig.GetMessage("OrderReceiptConfirmationIntroText", portal.PortalID, ZNodeCatalogManager.CatalogConfig.LocaleID);
            orderRow["CustomerServiceEmail"] = Equals(portal, null) ? ZNodeConfigManager.SiteConfig.CustomerServiceEmail.Replace(",", ", ") : string.IsNullOrEmpty(portal.CustomerServiceEmail) ? ZNodeConfigManager.SiteConfig.CustomerServiceEmail.Replace(",", ", ") : portal.CustomerServiceEmail.Replace(",", ", ").Trim();
            orderRow["CustomerServicePhoneNumber"] = Equals(portal, null) ? ZNodeConfigManager.SiteConfig.CustomerServicePhoneNumber : string.IsNullOrEmpty(portal.CustomerServicePhoneNumber) ? ZNodeConfigManager.SiteConfig.CustomerServicePhoneNumber : portal.CustomerServicePhoneNumber.Trim();
            orderRow["FeedBack"] = FeedbackUrl;

            // Payment info
            if (!String.IsNullOrEmpty(Order.CardTransactionID))
            {
                orderRow["CardTransactionID"] = Order.CardTransactionID;
                orderRow["CardTransactionLabel"] = "TransactionID:";
            }

            if (FromApi)
            {
                var paymentTypeService = new ZNode.Libraries.DataAccess.Service.PaymentTypeService();
                var paymentType = new PaymentType();
                if (ApiShoppingCart.Payment.PaymentSetting != null)
                {
                    paymentType = paymentTypeService.GetByPaymentTypeID(ApiShoppingCart.Payment.PaymentSetting.PaymentTypeID);
                }
                orderRow["PaymentName"] = paymentType != null ? paymentType.Name : String.Empty;
            }
            else
            {
                orderRow["PaymentName"] = ShoppingCart.Payment.PaymentName;
            }

            if (!String.IsNullOrEmpty(Order.PurchaseOrderNumber))
            {
                orderRow["PONumber"] = Order.PurchaseOrderNumber;
                orderRow["PurchaseNumberLabel"] = "Purchase Order Number:";
            }

            // Customer info
            orderRow["OrderId"] = Order.OrderID.ToString();
            orderRow["OrderDate"] = Order.OrderDate.ToString();
            orderRow["AccountId"] = Order.AccountID.ToString();

            orderRow["BillingAddress"] = Order.BillingAddress.ToString();
            orderRow["PromotionCode"] = Order.CouponCode;
            //PRFT Custom Code:Start
            orderRow["ERPOrderNumber"] = string.IsNullOrEmpty(Order.ExternalId) ? string.Empty : Order.ExternalId;           
            //PRFT Custom Code:End
            var addresses = ((ZNodePortalCart)ShoppingCart).AddressCarts;
            orderRow["ShippingAddress"] = addresses.Count() > 1 ? "You have selected multiple addresses" : GetOrderShipmentAddress(Order.OrderLineItems.FirstOrDefault().OrderShipmentIDSource);

            var multipleAddressTable = new DataTable();
            multipleAddressTable.Columns.Add("ShipTo");
            multipleAddressTable.Columns.Add("OrderShipmentID");
            multipleAddressTable.Columns.Add("ShipmentNo");

            var multipleTaxAddressTable = new DataTable();
            multipleTaxAddressTable.Columns.Add("OrderShipmentID");
            multipleTaxAddressTable.Columns.Add("Title");
            multipleTaxAddressTable.Columns.Add("Amount");

            // Create order line item table
            var orderlineItemTable = new DataTable();
            orderlineItemTable.Columns.Add("Name");
            orderlineItemTable.Columns.Add("SKU");
            orderlineItemTable.Columns.Add("Quantity");
            orderlineItemTable.Columns.Add("Description");
            orderlineItemTable.Columns.Add("Price");
            orderlineItemTable.Columns.Add("ExtendedPrice");
            orderlineItemTable.Columns.Add("OrderShipmentID");
            orderlineItemTable.Columns.Add("ShippingId");
            orderlineItemTable.Columns.Add("BackOrderMessage");//PRFT Custom Code

            if (supplier == null)
            {
                // Amount info
                BuildOrderAmountTable(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleSubTotal") == null ? "Sub Total" : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleSubTotal").ToString(), Order.SubTotal, orderAmountTable);
                BuildOrderAmountTable(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleTotalShippingCost") == null ? "Total Shipping Cost" : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleTotalShippingCost").ToString(), Order.ShippingCost, orderAmountTable);
                BuildOrderAmountTable(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleTotalTaxCost") == null ? "Tax Cost" : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleTotalTaxCost").ToString(), Order.TaxCost, orderAmountTable);
                BuildOrderAmountTable(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleSalesTax") == null ? "Sales Tax" : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleSalesTax").ToString(), Order.SalesTax, orderAmountTable);
                BuildOrderAmountTable(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnVAT") == null ? "VAT" : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnVAT").ToString(), Order.VAT, orderAmountTable);
                BuildOrderAmountTable(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnHST") == null ? "HST" : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnHST").ToString(), Order.HST, orderAmountTable);
                BuildOrderAmountTable(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnPST") == null ? "PST" : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnPST").ToString(), Order.PST, orderAmountTable);
                BuildOrderAmountTable(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnGST") == null ? "GST" : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnGST").ToString(), Order.GST, orderAmountTable);
                BuildOrderAmountTable(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleDiscountAmount") == null ? "Discount Amount" : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleDiscountAmount").ToString(), -Order.DiscountAmount, orderAmountTable);

                if (HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnGiftCardAmount") == null)
                {
                    BuildOrderAmountTable("Gift Card Amount", -Order.GiftCardAmount, orderAmountTable);
                }
                else
                {
                    BuildOrderAmountTable(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnGiftCardAmount").ToString(), -Order.GiftCardAmount, orderAmountTable);
                }

                orderRow["TotalCost"] = ZNodeCurrencyManager.GetCurrencyUnit(Order.Total) + ZNodeCurrencyManager.GetCurrencySuffix();

                if (Order.AdditionalInstructions != null)
                {
                    orderRow["AdditionalInstructions"] = Order.AdditionalInstructions;
                    orderRow["AdditionalInstructLabel"] = "Additional Instructions";
                }

                BuildOrderLineItem(multipleAddressTable, orderlineItemTable, multipleTaxAddressTable);
            }
            else
            {
                decimal orderTotal;

                BuildOrderLineItem(multipleAddressTable, orderlineItemTable, multipleTaxAddressTable, supplier, out orderTotal);

                // Amount info
                BuildOrderAmountTable(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleSubTotal") == null ? "Sub Total" : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleSubTotal").ToString(), orderTotal, orderAmountTable);

                orderRow["TotalCost"] = ZNodeCurrencyManager.GetCurrencyUnit(orderTotal) + ZNodeCurrencyManager.GetCurrencySuffix();

                if (Order.AdditionalInstructions != null)
                {
                    orderRow["AdditionalInstructions"] = String.Empty;
                    orderRow["AdditionalInstructLabel"] = "Additional Instructions";
                }
            }

            // Add rows to order table
            orderTable.Rows.Add(orderRow);

            // Create the HTML template
            var template = new ZNodeHtmlTemplate();

            // Set the HTML template path
            template.Path = templatePath;

            // Parse order table
            template.Parse(orderTable.CreateDataReader());

            // Parse order line items table			
            template.Parse("AddressItems", multipleAddressTable.CreateDataReader());
            foreach (DataRow address in multipleAddressTable.Rows)
            {
                // Parse OrderLineItem
                var filterData = orderlineItemTable.DefaultView;
                filterData.RowFilter = "OrderShipmentID=" + address["OrderShipmentID"];
                template.Parse("LineItems" + address["OrderShipmentID"], filterData.ToTable().CreateDataReader());

                //Parse Tax based on order shipment
                var amountFilterData = multipleTaxAddressTable.DefaultView;
                amountFilterData.RowFilter = "OrderShipmentID=" + address["OrderShipmentID"];
                template.Parse("AmountLineItems" + address["OrderShipmentID"], amountFilterData.ToTable().CreateDataReader());
            }
            // Parse order amount table
            template.Parse("GrandAmountLineItems", orderAmountTable.CreateDataReader());

            // Return the HTML output
            return template.Output;
        }

        /// <summary>
        /// Builds the order amount table.
        /// </summary>
        /// <param name="title">The title of the order.</param>
        /// <param name="amount">The amount of the order.</param>
        /// <param name="orderAmountTable">The order amount table.</param>
        private void BuildOrderAmountTable(string title, decimal amount, DataTable orderAmountTable)
        {
            if (amount != 0)
            {
                var row = orderAmountTable.NewRow();
                row["Title"] = title;
                row["Amount"] = ZNodeCurrencyManager.GetCurrencyUnit(amount) + ZNodeCurrencyManager.GetCurrencySuffix();

                orderAmountTable.Rows.Add(row);
            }
        }

        /// <summary>
        /// Builds the order line item table.
        /// </summary>
        /// <param name="multipleAddressTable">The multiple address table.</param>
        /// <param name="orderLineItemTable">The order line item table.</param>
        private void BuildOrderLineItem(DataTable multipleAddressTable, DataTable orderLineItemTable, DataTable multipleTaxAddressTable)
        {
            var orderShipments = Order.OrderLineItems.Select(x => x.OrderShipmentIDSource).Distinct();
            var shipmentCounter = 1;

            foreach (var orderShipment in orderShipments)
            {
                var addressRow = multipleAddressTable.NewRow();


                // If multiple shipping addresses then display the address for each group
                if (orderShipments.Count() > 1)
                {
                    addressRow["ShipmentNo"] = String.Format("Shipment #{0} - {1}", shipmentCounter++, orderShipment.ShipName);
                    addressRow["ShipTo"] = GetOrderShipmentAddress(orderShipment);
                }

                addressRow["OrderShipmentID"] = orderShipment.OrderShipmentID;
                var counter = 0;

                foreach (var lineitem in Order.OrderLineItems.Where(x => x.OrderShipmentID == orderShipment.OrderShipmentID))
                {
                    var shoppingCartItem = ((ZNodePortalCart)ShoppingCart).AddressCarts.Where(x => x.OrderShipmentID == orderShipment.OrderShipmentID).SelectMany(x => x.ShoppingCartItems.Cast<ZNodeShoppingCartItem>()).ElementAt(counter++);

                    var assetCollection = shoppingCartItem.Product.ZNodeDigitalAssetCollection;

                    var sb = new StringBuilder();
                    sb.Append(lineitem.Name + "<br />");

                    if (!String.IsNullOrEmpty(shoppingCartItem.Product.DownloadLink.Trim()))
                    {
                        sb.Append("<a href='" + shoppingCartItem.Product.DownloadLink + "' target='_blank'>Download</a><br />");
                    }

                    if (assetCollection.Count > 0)
                    {
                        foreach (ZNodeDigitalAsset digitalAsset in assetCollection)
                        {
                            sb.Append(digitalAsset.DigitalAsset + "<br />");
                            break;
                        }
                    }

                    if (orderLineItemTable != null)
                    {
                        var orderlineItemDbRow = orderLineItemTable.NewRow();
                        orderlineItemDbRow["Name"] = sb.ToString();
                        orderlineItemDbRow["SKU"] = lineitem.SKU;
                        orderlineItemDbRow["Description"] = lineitem.Description;
                        //orderlineItemDbRow["Quantity"] = lineitem.Quantity;
                        orderlineItemDbRow["Quantity"] = lineitem.Quantity;                    
                        orderlineItemDbRow["Price"] = ZNodeCurrencyManager.GetCurrencyUnit(shoppingCartItem.UnitPrice) + ZNodeCurrencyManager.GetCurrencySuffix();
                        orderlineItemDbRow["ExtendedPrice"] = ZNodeCurrencyManager.GetCurrencyUnit(shoppingCartItem.ExtendedPrice) + ZNodeCurrencyManager.GetCurrencySuffix();

                        //PRFT Custom Code : Start
                        //orderlineItemDbRow["Price"] = shoppingCartItem.UnitPrice.ToString("c") + ZNodeCurrencyManager.GetCurrencySuffix();
                        //orderlineItemDbRow["ExtendedPrice"] = shoppingCartItem.ExtendedPrice.ToString("c") + ZNodeCurrencyManager.GetCurrencySuffix();
                        orderlineItemDbRow["Price"] = shoppingCartItem.ERPUnitPrice.ToString("c") + ZNodeCurrencyManager.GetCurrencySuffix();
                        orderlineItemDbRow["ExtendedPrice"] = shoppingCartItem.ERPExtendedPrice.ToString("c") + ZNodeCurrencyManager.GetCurrencySuffix();
                        orderlineItemDbRow["BackOrderMessage"] = shoppingCartItem.BackOrderMessage;
                        //PRFT Custom Code: End

                        orderlineItemDbRow["OrderShipmentID"] = lineitem.OrderShipmentID;


                        orderLineItemTable.Rows.Add(orderlineItemDbRow);
                    }
                }

                var addressCart = ((ZNodePortalCart)ShoppingCart).AddressCarts.FirstOrDefault(y => y.OrderShipmentID == orderShipment.OrderShipmentID);

                if (addressCart != null)
                {
                    var globalResourceObject = HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleShipmentSubTotal");
                    if (globalResourceObject != null)
                        BuildOrderShipmentTotalLineItem(globalResourceObject.ToString(), addressCart.SubTotal, orderShipment.OrderShipmentID, multipleTaxAddressTable);

                    if (ApiShoppingCart != null)
                    {
                        var shippingService = new ZNode.Libraries.DataAccess.Service.ShippingService();
                        var shipping = shippingService.GetByShippingID(ApiShoppingCart.Shipping.ShippingID);
                        if (shipping != null)
                        {
                            addressCart.Shipping.ShippingName = shipping.Description;
                        }
                    }

                    BuildOrderShipmentTotalLineItem(String.Format("ShippingCost({0})", addressCart.Shipping.ShippingName), addressCart.ShippingCost, orderShipment.OrderShipmentID, multipleTaxAddressTable);

                    var resourceObject = HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnSalesTax");
                    if (resourceObject != null)
                        BuildOrderShipmentTotalLineItem(resourceObject.ToString(), addressCart.SalesTax, orderShipment.OrderShipmentID, multipleTaxAddressTable);

                    var o = HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnVAT");
                    if (o != null)
                        BuildOrderShipmentTotalLineItem(o.ToString(), addressCart.VAT, orderShipment.OrderShipmentID, multipleTaxAddressTable);

                    var globalResourceObject1 = HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnGST");
                    if (globalResourceObject1 != null)
                        BuildOrderShipmentTotalLineItem(globalResourceObject1.ToString(), addressCart.GST, orderShipment.OrderShipmentID, multipleTaxAddressTable);

                    var resourceObject1 = HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnHST");
                    if (resourceObject1 != null)
                        BuildOrderShipmentTotalLineItem(resourceObject1.ToString(), addressCart.HST, orderShipment.OrderShipmentID, multipleTaxAddressTable);

                    var o1 = HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnPST");
                    if (o1 != null)
                        BuildOrderShipmentTotalLineItem(o1.ToString(), addressCart.PST, orderShipment.OrderShipmentID, multipleTaxAddressTable);

                }

                multipleAddressTable.Rows.Add(addressRow);

            }
        }

        /// <summary>
        /// Build the Order line items for the specific Supplier and returns the Order Total which is used in Supplier Email
        /// </summary>
        /// <param name="multipleAddressTable"></param>
        /// <param name="orderLineItemTable"></param>
        /// <param name="multipleTaxAddressTable"></param>
        /// <param name="supplier"></param>
        /// <param name="orderTotal"></param>
        private void BuildOrderLineItem(DataTable multipleAddressTable, DataTable orderLineItemTable, DataTable multipleTaxAddressTable, ZnodeSupplier supplier, out decimal orderTotal)
        {
            var orderShipments = Order.OrderLineItems.Select(x => x.OrderShipmentIDSource).Distinct();
            var shipmentCounter = 1;
            orderTotal = 0;
            foreach (var orderShipment in orderShipments)
            {
                var addressRow = multipleAddressTable.NewRow();


                // If multiple shipping addresses then display the address for each group
                if (orderShipments.Count() > 1)
                {
                    addressRow["ShipmentNo"] = String.Format("Shipment #{0} - {1}", shipmentCounter++, orderShipment.ShipName);
                    addressRow["ShipTo"] = GetOrderShipmentAddress(orderShipment);
                }

                addressRow["OrderShipmentID"] = orderShipment.OrderShipmentID;
                var counter = 0;

                foreach (
                    var lineitem in Order.OrderLineItems.Where(x => x.OrderShipmentID == orderShipment.OrderShipmentID))
                {
                    if (supplier.SupplierItems.Cast<ZnodeSupplierItem>().Any(x => x.Sku == lineitem.SKU))
                    {
                        var shoppingCartItem =
                            ((ZNodePortalCart)ShoppingCart).AddressCarts.Where(
                                x => x.OrderShipmentID == orderShipment.OrderShipmentID)
                                                            .SelectMany(
                                                                x => x.ShoppingCartItems.Cast<ZNodeShoppingCartItem>())
                                                            .ElementAt(counter++);

                        var assetCollection = shoppingCartItem.Product.ZNodeDigitalAssetCollection;

                        var sb = new StringBuilder();
                        sb.Append(lineitem.Name + "<br />");

                        if (!String.IsNullOrEmpty(shoppingCartItem.Product.DownloadLink.Trim()))
                        {
                            sb.Append("<a href='" + shoppingCartItem.Product.DownloadLink +
                                      "' target='_blank'>Download</a><br />");
                        }

                        if (assetCollection.Count > 0)
                        {
                            foreach (ZNodeDigitalAsset digitalAsset in assetCollection)
                            {
                                sb.Append(digitalAsset.DigitalAsset + "<br />");
                            }
                        }
                        if (orderLineItemTable != null)
                        {
                            orderTotal += shoppingCartItem.ExtendedPrice;
                            var orderlineItemDbRow = orderLineItemTable.NewRow();
                            orderlineItemDbRow["Name"] = sb.ToString();
                            orderlineItemDbRow["SKU"] = lineitem.SKU;
                            orderlineItemDbRow["Description"] = lineitem.Description;
                            orderlineItemDbRow["Quantity"] = lineitem.Quantity;
                            orderlineItemDbRow["Price"] = shoppingCartItem.UnitPrice.ToString("c") +
                                                          ZNodeCurrencyManager.GetCurrencySuffix();
                            orderlineItemDbRow["ExtendedPrice"] = shoppingCartItem.ExtendedPrice.ToString("c") +
                                                                  ZNodeCurrencyManager.GetCurrencySuffix();
                            orderlineItemDbRow["OrderShipmentID"] = lineitem.OrderShipmentID;

                            orderLineItemTable.Rows.Add(orderlineItemDbRow);
                        }
                    }
                }

                multipleAddressTable.Rows.Add(addressRow);

            }
        }
        private void BuildOrderShipmentTotalLineItem(string title, decimal amount, int orderShipmentID, DataTable taxTable)
        {
            if (amount > 0)
            {
                var taxAddressRow = taxTable.NewRow();
                taxAddressRow["Title"] = title;
                taxAddressRow["Amount"] = ZNodeCurrencyManager.GetCurrencyUnit(Math.Round(amount, 2));
                taxAddressRow["OrderShipmentID"] = orderShipmentID;
                taxTable.Rows.Add(taxAddressRow);
            }
        }

        /// <summary>
        /// Builds the order line item table.
        /// </summary>
        /// <param name="orderlineItemTable">The order line item table.</param>
        /// <param name="supplier">The supplier for the order.</param>
        /// <param name="orderTotal">The order total.</param>
        private void BuildOrderLineItemTable(DataTable orderlineItemTable, ZnodeSupplier supplier, out decimal orderTotal)
        {
            orderTotal = 0;

            if (supplier != null && supplier.SupplierItems != null)
            {
                foreach (ZnodeSupplierItem supplierItem in supplier.SupplierItems)
                {
                    orderTotal += supplierItem.ExtendedPrice;

                    var row = orderlineItemTable.NewRow();
                    row["Name"] = supplierItem.Name;
                    row["SKU"] = supplierItem.Sku;
                    row["Description"] = supplierItem.Description;
                    row["Quantity"] = supplierItem.Quantity.ToString();
                    row["Price"] = supplierItem.UnitPrice.ToString("c") + ZNodeCurrencyManager.GetCurrencySuffix();
                    row["ExtendedPrice"] = supplierItem.ExtendedPrice.ToString("c") + ZNodeCurrencyManager.GetCurrencySuffix();

                    orderlineItemTable.Rows.Add(row);
                }
            }
        }

        private string GetOrderShipmentAddress(OrderShipment orderShipment)
        {
            return String.Format(CultureInfo.InvariantCulture, "{0} {1}{2}{3}{4}{5}, {6} {7} {8}<br />{9}",
                (String.IsNullOrEmpty(orderShipment.ShipToFirstName) ? String.Empty : orderShipment.ShipToFirstName),
                (String.IsNullOrEmpty(orderShipment.ShipToLastName) ? String.Empty : orderShipment.ShipToLastName),
                (String.IsNullOrEmpty(orderShipment.ShipToCompanyName) ? String.Empty : "<br />" + orderShipment.ShipToCompanyName),
                (String.IsNullOrEmpty(orderShipment.ShipToStreet) ? String.Empty : "<br />" + orderShipment.ShipToStreet),
                (String.IsNullOrEmpty(orderShipment.ShipToStreet1) ? String.Empty : "<br />" + orderShipment.ShipToStreet1),
                (String.IsNullOrEmpty(orderShipment.ShipToCity) ? String.Empty : "<br />" + orderShipment.ShipToCity),
                (String.IsNullOrEmpty(orderShipment.ShipToStateCode) ? String.Empty : orderShipment.ShipToStateCode), orderShipment.ShipToPostalCode,
                (String.IsNullOrEmpty(orderShipment.ShipToCountry) ? String.Empty : "<br />" + orderShipment.ShipToCountry),
                (String.IsNullOrEmpty(orderShipment.ShipToPhoneNumber) ? String.Empty : "Ph: " + orderShipment.ShipToPhoneNumber));
        }

        /// <summary>
        /// Gets the HTML used when showing receipts in the UI.
        /// </summary>
        /// <returns>The HTML for the receipt.</returns>
        public string GetHtmlReceiptForUI()
        {
            var receiptHtml = GenerateHtmlReceiptWithParser(null, false);

            var htmlParser = new HtmlParser(receiptHtml);
            htmlParser.CurIndex = receiptHtml.IndexOf("<body", StringComparison.CurrentCultureIgnoreCase) + 2;
            htmlParser.ParseHtml();

            return htmlParser.HtmlBlockContents;
        }

        /// <summary>
        /// Gets the HTML used when sending receipts in email.
        /// </summary>
        /// <param name="supplier">The supplier that will receive the email.</param>
        /// <returns>The HTML for the receipt.</returns>
        public string GetHtmlReceiptForEmail(ZnodeSupplier supplier)
        {
            var receiptHtml = GenerateHtmlReceiptWithParser(supplier, true);
            var htmlParser = new HtmlParser(receiptHtml);
            htmlParser.CurIndex = receiptHtml.IndexOf("<html", StringComparison.CurrentCultureIgnoreCase) + 2;
            htmlParser.ParseHtml();

            return htmlParser.HtmlBlockContents;
        }
    }
}
