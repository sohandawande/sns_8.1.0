﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using ZNode.Libraries.Framework.Business;
using System.Configuration;


namespace Znode.Engine.Suppliers
{
    public partial class ZnodeSupplierEmail
    {
        public string GetOrderReceiptTemplate(string messageText)
        {
            string apiUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            //string logoLink = (systemConfigurationManager.AppSettings["CookieExpiresValue"]);
            string logoLink = ConfigurationSettings.AppSettings["DemoWebsiteUrl"];

            var rx1 = new Regex("#SnSLogo#", RegexOptions.IgnoreCase);            
            string LogoPath = ZNodeConfigManager.SiteConfig.LogoPath.Trim().Replace("~/", "");
            messageText = rx1.Replace(messageText, apiUrl + "/" + LogoPath.TrimStart('~'));

            var rx3 = new Regex("#MailingInfo#", RegexOptions.IgnoreCase);
            messageText = rx3.Replace(messageText, ZNodeConfigManager.SiteConfig.CustomerServiceEmail);

            var rx4 = new Regex("#SupportPhone#", RegexOptions.IgnoreCase);
            messageText = rx4.Replace(messageText, ZNodeConfigManager.SiteConfig.CustomerServicePhoneNumber);

            var rx5 = new Regex("#SupportEmail#", RegexOptions.IgnoreCase);
            messageText = rx5.Replace(messageText, ZNodeConfigManager.SiteConfig.SalesEmail);

            var rx6 = new Regex("#LogoLink#", RegexOptions.IgnoreCase);
            messageText = rx6.Replace(messageText, logoLink);

            return messageText;
        }
    }
}
