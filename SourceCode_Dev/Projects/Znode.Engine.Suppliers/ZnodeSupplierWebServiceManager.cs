using System;
using System.Linq;
using System.Reflection;
using ZNode.Libraries.ECommerce.Fulfillment;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.Suppliers
{
	/// <summary>
	/// Helps manage supplier web services.
	/// </summary>
	public class ZnodeSupplierWebServiceManager : ZnodeSupplierManager
	{
		public ZnodeSupplierWebServiceManager(ZNodeOrderFulfillment order, ZNodeShoppingCart shoppingCart) : base(order, shoppingCart)
		{
		}

		/// <summary>
		/// Invokes web service calls for each supplier.
		/// </summary>
		/// <returns>True if all calls were successful; otherwise, false.</returns>
		public bool InvokeWebService()
		{
			var result = true;

			// Invoke web service call for each supplier in the list
			foreach (ZnodeSupplier supplier in Suppliers)
			{
				result &= InvokeServiceForEachSupplierWebServiceType(supplier);
			}

			return result;
		}

		private bool InvokeServiceForEachSupplierWebServiceType(ZnodeSupplier supplier)
		{
			var result = true;

			var availableSupplierTypes = GetAvailableSupplierTypes();
			foreach (var t in availableSupplierTypes.Where(t => t.ClassName == supplier.ClassName))
			{
				try
				{
					// Create instance of supplier type and call it
					var supplierTypeAssembly = Assembly.GetAssembly(t.GetType());
					var supplierType = (IZnodeSupplierWebServiceType)supplierTypeAssembly.CreateInstance(t.ToString());

					if (supplierType != null)
					{
						result &= supplierType.InvokeWebService(Order, ShoppingCart, supplier);
					}
				}
				catch (Exception ex)
				{
					ZNodeLogging.LogMessage("Error while instantiating supplier type: " + t);
					ZNodeLogging.LogMessage(ex.ToString());
				}
			}

			return result;
		}
	}
}
