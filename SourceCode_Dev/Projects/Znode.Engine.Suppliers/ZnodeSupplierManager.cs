using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StructureMap;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.ECommerce.Fulfillment;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;
using ZNodeShoppingCart = ZNode.Libraries.ECommerce.ShoppingCart.ZNodeShoppingCart;
using ZNodeShoppingCartItem = ZNode.Libraries.ECommerce.ShoppingCart.ZNodeShoppingCartItem;

namespace Znode.Engine.Suppliers
{
	/// <summary>
	/// Base class for ZnodeSupplierEmailManager and ZnodeSupplierWebServiceManager.
	/// </summary>
	public class ZnodeSupplierManager : ZNodeBusinessBase
	{
		public ZNodeOrderFulfillment Order { get; set; }
		public ZNodeShoppingCart ShoppingCart { get; set; }
		public ZNodeGenericCollection<ZnodeSupplier> Suppliers { get; set; }
	    public bool FromApi { get; set; }
        public string FeedbackUrl { get; set; }
	    /// <summary>
		/// Throws a NotImplementedException because the supplier options require an order and a shopping cart.
		/// </summary>
		protected ZnodeSupplierManager()
		{
			throw new NotImplementedException();
		}

		public ZnodeSupplierManager(ZNodeOrderFulfillment order, ZNodeShoppingCart shoppingCart)
		{
			Order = order;
			ShoppingCart = shoppingCart;
			Suppliers = new ZNodeGenericCollection<ZnodeSupplier>();

			foreach (ZNodeShoppingCartItem cartItem in ShoppingCart.ShoppingCartItems)
			{
				var supplierId = cartItem.Product.SupplierID;
				if (supplierId > 0)
				{
					var supplierItem = BuildSupplierItem(cartItem);
					AddSupplierToList(supplierId, supplierItem);
				}

				foreach (ZNodeAddOnEntity addOn in cartItem.Product.SelectedAddOnItems.AddOnCollection)
				{
					foreach (ZNodeAddOnValueEntity addOnValue in addOn.AddOnValueCollection)
					{
						supplierId = addOnValue.SupplierID;

						if (supplierId > 0)
						{
							var supplierItem = BuildSupplierItem(cartItem, addOn, addOnValue);
							AddSupplierToList(supplierId, supplierItem);
						}
					}
				}
			}
		}

		/// <summary>
		/// Sends an email receipt of the order to the customer.
		/// </summary>
		/// <param name="order">The order being processed.</param>
		/// <param name="shoppingCart">The current shopping cart.</param>
		/// <returns>True if sending email receipt was successful; otherwise, false.</returns>
		public bool SendEmailReceiptToCustomer(ZNodeOrderFulfillment order, ZNodeShoppingCart shoppingCart)
		{
			var result = false;

			try
			{
				var to = order.Email;
				var from = ZNodeConfigManager.SiteConfig.SalesEmail;
				var bcc = ZNodeConfigManager.SiteConfig.SalesEmail;
				var subject = ZNodeCatalogManager.MessageConfig.GetMessage("OrderReceiptEmailSubject");

                var receipt = new ZnodeReceipt(order, shoppingCart) { FromApi = FromApi, ApiShoppingCart = ShoppingCart, FeedbackUrl = FeedbackUrl };
                string body = receipt.GetHtmlReceiptForEmail(null);
                if(string.IsNullOrEmpty(body))
                { 
                    ZNodeLogging.LogMessage("GetHtmlReceiptForEmail(null) Return - "+ body);
                }
                    #region PRFT Custom Code
                ZnodeSupplierEmail znodeSupplierEmail = new ZnodeSupplierEmail();
                body = znodeSupplierEmail.GetOrderReceiptTemplate(body);
                #endregion

                //Znode Version 7.2.2
                //Change Description - Start 
                // Remove print link from the body of email. 
                int removeStartIndex = body.IndexOf("<!--~");
                int removeLastIndex = body.IndexOf("~-->") + "~-->".Length;
                //Change Description - End
               body = Slice(body, removeStartIndex, removeLastIndex);


				ZNodeEmail.SendEmail(to, from, bcc, subject, body, true);
				result = true;
			}
			catch (Exception ex)
			{
				result = false;
				ZNodeLogging.LogMessage("There was a problem sending email to the customer: " + ex.Message);
			}

			return result;
		}

		/// <summary>
		/// Caches all available supplier types in the application cache.
		/// </summary>
		public static void CacheAvailableSupplierTypes()
		{
			if (HttpRuntime.Cache["SupplierTypesCache"] == null)
			{
				if (ObjectFactory.Container == null)
				{
					ObjectFactory.Initialize(scanner => scanner.Scan(x =>
					{
						x.AssembliesFromApplicationBaseDirectory();
						x.AddAllTypesOf<IZnodeSupplierType>();
					}));
				}
				else
				{
					ObjectFactory.Configure(scanner => scanner.Scan(x =>
					{
						x.AssembliesFromApplicationBaseDirectory();
						x.AddAllTypesOf<IZnodeSupplierType>();
					}));	
				}

				// Only cache supplier types that have a ClassName and Name; this helps avoid showing base classes in some of the dropdown lists
				var supplierTypes = ObjectFactory.GetAllInstances<IZnodeSupplierType>().Where(x => !String.IsNullOrEmpty(x.ClassName) && !String.IsNullOrEmpty(x.Name));
				HttpRuntime.Cache["SupplierTypesCache"] = supplierTypes.ToList();
			}
		}

		/// <summary>
		/// Gets all available supplier types from the application cache.
		/// </summary>
		/// <returns>A list of the available supplier types.</returns>
		public static List<IZnodeSupplierType> GetAvailableSupplierTypes()
		{
			var list = HttpRuntime.Cache["SupplierTypesCache"] as List<IZnodeSupplierType>;
			if (list != null)
			{
				list.Sort((supplierTypeA, supplierTypeB) => String.CompareOrdinal(supplierTypeA.Name, supplierTypeB.Name));
			}
			else
			{
				list = new List<IZnodeSupplierType>();
			}

			return list;
		}

		/// <summary>
		/// Gets a supplier from the suppliers list.
		/// </summary>
		/// <param name="supplierId">The ID of the supplier.</param>
		/// <returns>The supplier.</returns>
		private ZnodeSupplier GetSupplierFromList(int supplierId)
		{
			foreach (ZnodeSupplier supplier in Suppliers)
			{
				if (supplier.SupplierID == supplierId)
				{
					return supplier;
				}
			}

			return null;
		}

		/// <summary>
		/// Adds a supplier to the list.
		/// </summary>
		/// <param name="supplierId">The ID of the supplier.</param>
		/// <param name="supplierItem">The supplier item.</param>
		private void AddSupplierToList(int supplierId, ZnodeSupplierItem supplierItem)
		{
			if (supplierId > 0)
			{
				var supplier = GetSupplierFromList(supplierId);
				if (supplier == null)
				{
					supplier = ZnodeSupplier.Create(supplierId);
					Suppliers.Add(supplier);
				}

				AddSupplierItemToList(supplierItem, supplier.SupplierItems);
			}
		}

		/// <summary>
		/// Adds the supplier item to the list, or updates its quantity if already in the list.
		/// </summary>
		/// <param name="supplierItem">The supplier item.</param>
		/// <param name="supplierItems">The list of supplier items.</param>
		private void AddSupplierItemToList(ZnodeSupplierItem supplierItem, ZNodeGenericCollection<ZnodeSupplierItem> supplierItems)
		{
			var index = IndexOf(supplierItem.ProductSupplierId, supplierItems);

			if (index >= 0)
			{
				supplierItems[index].Quantity += supplierItem.Quantity;
			}
			else
			{
				supplierItems.Add(supplierItem);
			}
		}

		/// <summary>
		/// Gets the index of the supplier item in the list.
		/// </summary>
		/// <param name="productSupplierId">The supplier ID of the product.</param>
		/// <param name="supplierItems">The list of supplier items.</param>
		/// <returns>The index of the supplier item, or -1 if not found in the list.</returns>
		private int IndexOf(string productSupplierId, ZNodeGenericCollection<ZnodeSupplierItem> supplierItems)
		{
			var index = -1;

			if (productSupplierId.Length > 0)
			{
				var counter = 0;

				foreach (ZnodeSupplierItem item in supplierItems)
				{
					if (item.ProductSupplierId.Equals(productSupplierId, StringComparison.OrdinalIgnoreCase))
					{
						index = counter;
						break;
					}

					counter++;
				}
			}

			return index;
		}

		private ZnodeSupplierItem BuildSupplierItem(ZNodeShoppingCartItem cartItem)
		{
			var supplierItem = new ZnodeSupplierItem();
			supplierItem.ProductSupplierId = "Product" + cartItem.Product.ProductID;
			supplierItem.Name = cartItem.Product.Name;
			supplierItem.Sku = cartItem.Product.SKU;
			supplierItem.Description = cartItem.Product.ShoppingCartDescription;

			if (cartItem.Product.SelectedSKU.SKUID > 0)
			{
				supplierItem.ProductSupplierId = "Sku" + cartItem.Product.SelectedSKU.SKUID;
				supplierItem.Sku = cartItem.Product.SelectedSKU.SKU;
			}

			supplierItem.Quantity = cartItem.Quantity;
			supplierItem.UnitPrice = cartItem.TieredPricing;

			return supplierItem;
		}

		private ZnodeSupplierItem BuildSupplierItem(ZNodeShoppingCartItem cartItem, ZNodeAddOnEntity addOn, ZNodeAddOnValueEntity addOnValue)
		{
			var supplierItem = new ZnodeSupplierItem();
			supplierItem.ProductSupplierId = "AddOnValue" + addOnValue.AddOnValueID;
			supplierItem.Name = addOnValue.Name;
			supplierItem.Description = addOn.Name + " - " + addOnValue.Name;
			supplierItem.Sku = addOnValue.SKU;
			supplierItem.Quantity = cartItem.Quantity;
			supplierItem.UnitPrice = addOnValue.FinalPrice;

			return supplierItem;
		}


        #region  Znode Version 7.2.2

        /// <summary>
        ///  Znode Version 7.2.2
        ///  Takes source string and removes substring for specified start index and end index.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="startIndex"></param>
        /// <param name="endIndex"></param>
        /// <returns>sliced string</returns>
        private string Slice(string source, int startIndex, int endIndex)
        {
            if (startIndex > 0)
            {
                if (endIndex < 0)
                {
                    endIndex = source.Length + endIndex;
                }
                int len = endIndex - startIndex;
                return source.Remove(startIndex, len);
            }
            return source;
        }

        #endregion
    }
}
