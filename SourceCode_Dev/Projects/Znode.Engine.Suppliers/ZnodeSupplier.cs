using System;
using System.Xml.Serialization;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Suppliers
{
	/// <summary>
	/// This is the supplier object.
	/// </summary>
	[Serializable]
	[XmlRoot("ZnodeSupplier")]
	public class ZnodeSupplier : ZNodeBusinessBase
	{
		private string _className;
		private ZNodeGenericCollection<ZnodeSupplierItem> _supplierItems;

		[XmlElement]
		public int SupplierID { get; set; }

		[XmlElement]
		public string ExternalSupplierNo { get; set; }

		[XmlElement]
		public string Name { get; set; }

		[XmlElement]
		public string Description { get; set; }

		[XmlElement]
		public string ContactFirstName { get; set; }

		[XmlElement]
		public string ContactLastName { get; set; }

		[XmlElement]
		public string ContactPhone { get; set; }

		[XmlElement]
		public string ContactEmail { get; set; }

		[XmlElement]
		public string NotificationEmailID { get; set; }

		[XmlElement]
		public string EmailNotificationTemplate { get; set; }

		[XmlElement]
		public bool EnableEmailNotification { get; set; }

		[XmlElement]
		public int DisplayOrder { get; set; }

		[XmlElement]
		public bool ActiveInd { get; set; }

		[XmlElement]
		public string ClassName
		{
			get
			{
				if (String.IsNullOrEmpty(_className))
				{
					_className = GetType().Name;
				}

				return _className;
			}

			set { _className = value; }
		}

		[XmlIgnore]
		public ZNodeGenericCollection<ZnodeSupplierItem> SupplierItems
		{
			get { return _supplierItems ?? (_supplierItems = new ZNodeGenericCollection<ZnodeSupplierItem>()); }
		}

		/// <summary>
		/// Creates a ZnodeSupplier object.
		/// </summary>
		/// <param name="supplierId">The ID of the supplier.</param>
		/// <returns>A ZnodeSupplier object.</returns>
		public static ZnodeSupplier Create(int supplierId)
		{
			var supplierHelper = new SupplierHelper();
			var supplierXml = supplierHelper.GetSupplierXMLById(supplierId);

			// Serialize the object
			var serializer = new ZNodeSerializer();
			return (ZnodeSupplier)serializer.GetContentFromString(supplierXml, typeof(ZnodeSupplier));
		}
	}
}
