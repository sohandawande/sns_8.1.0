﻿using ZNode.Libraries.ECommerce.Fulfillment;
using ZNode.Libraries.ECommerce.ShoppingCart;

namespace Znode.Engine.Suppliers
{
	/// <summary>
	/// This is the root interface for all supplier web service types.
	/// </summary>
	public interface IZnodeSupplierWebServiceType : IZnodeSupplierType
	{
		bool InvokeWebService(ZNodeOrderFulfillment order, ZNodeShoppingCart shoppingCart, ZnodeSupplier supplier);
	}
}
