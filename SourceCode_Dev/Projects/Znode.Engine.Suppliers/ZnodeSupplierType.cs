﻿using System;
using System.Collections.ObjectModel;

namespace Znode.Engine.Suppliers
{
	/// <summary>
	/// This is the base class for all supplier types.
	/// </summary>
	public class ZnodeSupplierType : IZnodeSupplierType
	{
		private string _className;
		private Collection<ZnodeSupplierControl> _controls;

		public string ClassName
		{
			get
			{
				if (String.IsNullOrEmpty(_className))
				{
					_className = GetType().Name;
				}

				return _className;
			}

			set { _className = value; }
		}

		public string Name { get; set; }
		public string Description { get; set; }

		public Collection<ZnodeSupplierControl> Controls
		{
			get { return _controls ?? (_controls = new Collection<ZnodeSupplierControl>()); }
		}
	}
}
