IF EXISTS (SELECT TOP 1 1 FROM SYS.PROCEDURES WHERE name = 'ZNode_GetProductListXMLSiteMap' )
BEGIN 
DROP PROCEDURE [dbo].[ZNode_GetProductListXMLSiteMap]
END 
GO

/****** Object:  StoredProcedure [dbo].[ZNode_GetProductListXMLSiteMap]    Script Date: 8/4/2016 8:18:25 PM ******/
CREATE  PROCEDURE [dbo].[ZNode_GetProductListXMLSiteMap] 
@PortalID NVarchar(Max) = NULL,
@FeedType NVarchar(Max) = NULL         
AS      
BEGIN      
    SET NOCOUNT ON;      
      DECLARE @Query NVARCHAR(3000)
	  DECLARE @WhereClause NVARCHAR(1000)
	
    SET @Query='      
    SELECT DISTINCT
		P.SEOURL as loc,
		P.UpdateDte As lastmod,
		P.Name As title,
		Isnull((Select ZM.Name from ZNodeManufacturer ZM Where P.ManufacturerID = ZM.ManufacturerID),'''')  As [g:brand],
		Isnull((Select ZM.Name from ZNodeManufacturer ZM Where P.ManufacturerID = ZM.ManufacturerID),'''')  As [g:manufacturer],
		''new'' As [g:condition],
		p.Description as [description],
		P.ProductID As [g:id],
        P.ImageFile As [g:image_link],
        '''' as link,      
        FORMAT(Isnull(P.RetailPrice,0.0),''N2'') as [g:price],
        ''false'' As [g:identifier_exists],   
        ''g:availability'' =
			CASE 
				WHEN CONVERT(INT, ISNULL(P.Custom3,''0'')) > 0 Then ''In Stock''
				ELSE   '''+CASE WHEN @FeedType = 'Google' THEN 'Out Of Stock' ELSE 'Not In Stock' END+''' 
			END,
		ISNULL((select top 1 DomainName from ZNodeDomain where PortalID=ZP.Portalid),'''')  DomainName,
		ZP.PortalId
			
    FROM
	ZNodeProduct P
	INNER JOIN ZNodeProductCategory ZPC ON ZPC.ProductID = P.ProductID 
	INNER JOIN ZNodeCategoryNode ZCN ON ZCN.CategoryID = ZPC.CategoryID
	INNER JOIN ZNodePortalCatalog ZP ON ZP.CatalogID = zcn.CatalogID
	INNER JOIN ZNodeSKU ZS ON ZS.ProductID = P.ProductID
	INNER JOIN ZNodeSKUInventory ZSI ON ZSI.SKU = ZS.SKU'
    
     SET @WhereClause=' Where P.ActiveInd = 1';
   
   If(CHARINDEX(',',@PortalID,0)>0)
   BEGIN	 	  
			SET @WhereClause=@WhereClause+' AND ZP.PortalID in (SELECT cast([value] as int) from sc_splitter('''+@PortalID+''','',''))'
		END
   Else IF(Cast(@PortalID as Int) > 0)
	BEGIN	 	  
			SET @WhereClause=@WhereClause+' AND (ZP.PortalID = '+@PortalID+')'
	END
		
		Set @Query = @Query+@WhereClause
		
		EXEC SP_EXECUTESQL @Query  
    
END