USE [SnS_DataImport_2016-02-09]
GO
/****** Object:  Table [dbo].[PRFTShipviaCode]    Script Date: 04/07/2016 12:29:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PRFTShipviaCode](
	[ShipviaId] [int] IDENTITY(1,1) NOT NULL,
	[ShipviaCode] [nvarchar](255) NULL,
	[Description] [nvarchar](255) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PRFTPaymentTerms]    Script Date: 04/07/2016 12:29:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PRFTPaymentTerms](
	[PaymentTermsId] [int] IDENTITY(1,1) NOT NULL,
	[TermCode] [nvarchar](255) NULL,
	[Description] [nvarchar](255) NULL
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[PRFT_GetShipviaCode]    Script Date: 04/07/2016 12:29:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================

-- Author:		<Author,Farhat Khan>

-- Create date: <Create Date,04/07/16>

-- Description:	<Description,To get the Shipvia Description based on ShipviaCode>

-- =============================================

CREATE PROCEDURE [dbo].[PRFT_GetShipviaCode]

@ShipviaCode nvarchar(Max)

AS

BEGIN
		
	SELECT [Description] 
	FROM PRFTShipviaCode
	where ShipviaCode=@ShipviaCode
END
GO
/****** Object:  StoredProcedure [dbo].[PRFT_GetPaymentTerms]    Script Date: 04/07/2016 12:29:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================

-- Author:		<Author,Farhat Khan>

-- Create date: <Create Date,04/07/16>

-- Description:	<Description,To get the PaymentTerms Description based on terms code>

-- =============================================

CREATE PROCEDURE [dbo].[PRFT_GetPaymentTerms]

@TermCode nvarchar(Max)

AS

BEGIN
		
	SELECT [Description] 
	FROM PRFTPaymentTerms
	where TermCode=@TermCode
END
GO
