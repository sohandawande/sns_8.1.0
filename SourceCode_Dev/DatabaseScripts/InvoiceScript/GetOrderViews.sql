
GO

/****** Object:  View [dbo].[vw_ZNodeGetOrderDetails]    Script Date: 4/6/2016 5:03:24 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[vw_ZNodeGetOrderDetails]
AS
SELECT DISTINCT 
                         a.OrderID, a.PortalId, a.AccountID, a.OrderStateID, a.ShippingID, a.PaymentTypeId, a.PaymentStatusID, b.OrderStateName AS OrderStatus, 
                         c.Description AS ShippingTypeName, ISNULL(d.Name, '') AS PaymentTypeName, ISNULL(e.PaymentStatusName, '') AS PaymentStatusName, a.BillingFirstName, 
                         a.BillingLastName, a.Total, a.OrderDate, a.ShipDate, a.TrackingNumber, ISNULL(a.PaymentSettingID, 0) AS PaymentSettingId, ISNULL(f.StoreName, '') AS StoreName,
                          a.BillingCompanyName, ISNULL(a.BillingFirstName, '') + ' ' + ISNULL(a.BillingLastName, '') AS FullName, ISNULL(a.CardTransactionID, 0) AS CardTransactionId, 
                         a.ExternalID
FROM            dbo.ZNodeOrder AS a INNER JOIN
                         dbo.ZNodeOrderState AS b ON a.OrderStateID = b.OrderStateID LEFT OUTER JOIN
                         dbo.ZNodeShipping AS c ON a.ShippingID = c.ShippingID LEFT OUTER JOIN
                         dbo.ZNodePaymentType AS d ON a.PaymentTypeId = d.PaymentTypeID LEFT OUTER JOIN
                         dbo.ZNodePaymentStatus AS e ON a.PaymentStatusID = e.PaymentStatusID LEFT OUTER JOIN
                         dbo.ZNodePortal AS f ON a.PortalId = f.PortalID

GO


