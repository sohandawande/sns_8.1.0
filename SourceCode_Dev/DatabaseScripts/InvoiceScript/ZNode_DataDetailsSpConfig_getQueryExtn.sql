
GO
/****** Object:  StoredProcedure [dbo].[ZNode_DataDetailsSpConfig_getQueryExtn]    Script Date: 4/6/2016 4:34:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER     PROCEDURE [dbo].[ZNode_DataDetailsSpConfig_getQueryExtn] 
    (
      @SPName NVARCHAR(MAX),
      @WhereClause NVARCHAR(MAX) = NULL,
	  @HavingClause NVARCHAR(MAX) = NULL,
	  @OrderBy NVARCHAR(MAX) = NULL,
      @PreFix NVARCHAR(MAX) = NULL,
      @Flag INT = 0,
      @WhereClause1 NVARCHAR(MAX) = NULL,
      @Select_Clause NVARCHAR(MAX) OUTPUT,
      @Sql NVARCHAR(MAX) OUTPUT,
      @OrderBy_Output NVARCHAR(MAX) OUTPUT
    )
AS 
    BEGIN

        DECLARE @Select_FROM_CLAUSE NVARCHAR(MAX)   
        DECLARE @FROM_CLAUSE NVARCHAR(MAX)          
        DECLARE @OrderByDefault AS NVARCHAR(MAX)    
        DECLARE @L_OrderBy AS NVARCHAR(MAX)
        DECLARE @L_WhereClause AS NVARCHAR(MAX)
        DECLARE @L_GroupByClause AS NVARCHAR(MAX)
        DECLARE @L_SPfilterClause AS NVARCHAR(MAX)= ' ' ;
        DECLARE @L_HavingClause AS NVARCHAR(MAX)= ' ' ;
        DECLARE @L_innerWhereClause AS NVARCHAR(MAX)= LOWER(@WhereClause) ;
        DECLARE @L_inLineWhereClause NVARCHAR(MAX) = LOWER(@WhereClause1) ;
		Declare @L_inLine nvarchar(3000)
		Declare @AlliesName nvarchar(100)
		Declare @Specialcondition nvarchar(100)
		Declare @AccountId nvarchar(100) --PRFT Custom Code
		-- Znode Category and Znode category Node details

		IF @SPName in ('ZNode_SearchCategories','ZNodeSearchAddons','ZNodeSearchCase','ZNodeSearchRMA' ,'ZNodeGetVendorProduct','ZNodeGetDistinctProductsByCriteria','ZNodeGetRejectionMessages','Znode_TaxClass','ZNode_SearchCategoriesByPortalCatalog','ZNodeAdminAccounts','ZnodeGetWebpagesProfile','PRFT_CreditApplicationList','PRFT_InventoryList','PRFT_GetCustomerUserMappingList','PRFT_GetNotAssociatedCustomerList','PRFT_GetSubUserList')
        BEGIN
			SEt @Specialcondition = ''
			IF @SPName = 'ZNode_SearchCategories' 
				SEt @AlliesName = 'ZNodeCategory'

			else IF @SPName = 'ZNodeSearchAddons'
				Begin
					SEt @AlliesName = 'a'
					SEt @Specialcondition = ' OR a.Portalid is null'
				End

			else IF @SPName = 'ZNodeSearchCase'
				SEt @AlliesName = 'R'

			else IF @SPName = 'ZNodeSearchRMA'
				SEt @AlliesName = 'ZnodeOrder'

			else IF @SPName = 'ZNodeGetVendorProduct'
				SEt @AlliesName = 'P'

			else IF @SPName = 'ZNodeGetDistinctProductsByCriteria'
				SEt @AlliesName = 'g'

			else IF @SPName = 'ZNodeGetRejectionMessages'
				SEt @AlliesName = 'a'

			else IF @SPName = 'Znode_TaxClass'
				Begin
					SEt @AlliesName = 'a'
					SEt @Specialcondition = ' OR a.Portalid is null'
				End

			else IF @SPName = 'ZNode_SearchCategoriesByPortalCatalog'
					SEt @AlliesName = 'Znodeportalcatalog'

			else IF @SPName = 'ZNodeAdminAccounts'
					SEt @AlliesName = 'wp'

			else If @SPName = 'ZnodeGetWebpagesProfile'
					SEt @AlliesName = 'a'

			else if @SPName = 'PRFT_CreditApplicationList'
					SET @AlliesName = 'prftc'	
			
			else if @SPName = 'PRFT_InventoryList'
					SET @AlliesName = 'prftinv'
			
			else if @SPName = 'PRFT_GetCustomerUserMappingList'
					SET @AlliesName = 'prftcum'	
					
			else if @SPName = 'PRFT_GetNotAssociatedCustomerList'
					SET @AlliesName = 'prftnac'
						
			else if @SPName = 'PRFT_GetSubUserList'
					SET @AlliesName = 'prftsu'

			IF (@L_inLineWhereClause IS NOT NULL OR LEN(@L_inLineWhereClause) >= 1) 
			BEGIN
				SET @L_inLineWhereClause = REPLACE(@L_inLineWhereClause,'~~' + 'UserName'+ '~~','Isnull(UserName,'''')')

				if(@L_inLineWhereClause != '')
				Begin
					SET @L_inLine  = ' and ((  isnull(' + @AlliesName + '.PortalID  ,0 ) in '
					SET @L_inLine = @L_inLine + ' (select _wp.PortalId from webpages_profile _wp inner join WebPages_users _wu  on _wp.UserId  = _wu.UserId where  '+ @L_inLineWhereClause +' ) '
					SET @L_inLine = @L_inLine + ' OR ('+ @AlliesName + '.PortalID  =' + @AlliesName + '.PortalID  and '
					SET @L_inLine = @L_inLine + ' (select top 1  _wp1.PortalId from webpages_profile _wp1 inner join WebPages_users _wu1  on _wp1.UserId  = _wu1.UserId where  '+ @L_inLineWhereClause+' ) <= 0'
					SET @L_inLine = @L_inLine + ' ) ' + @Specialcondition+ ' ))'
				END
			END ;
		END	


        IF @SPName = 'ZNodeCategory' 
            BEGIN
                SET @Select_Clause = 'CategoryNodeId,CatalogID,CategoryId,PortalID, Name,ParentCategoryNodeId,SeoUrl,ActiveInd,DisplayOrder'
                SET @Select_FROM_CLAUSE = 'ZCn.CategoryNodeId, ZCn.CatalogID, ZCn.CategoryId, ZC.PortalID,  dbo.FN_GetCategoryPath(CategoryNodeId) as Name, Zcn.ParentCategoryNodeId, ZC.SeoUrl, ZCn.ActiveInd, ZCn.Displayorder  '
                SET @FROM_CLAUSE = 'from ZNodeCategoryNode ZCn inner join ZNodeCategory ZC On ZCn.CategoryId = ZC.CategoryId  '
                SET @OrderByDefault = 'DisplayOrder'
                SET @L_GroupByClause = ''
                SET @L_SPfilterClause = ''

                IF ( @L_innerWhereClause IS NOT NULL OR LEN(@L_innerWhereClause) >= 1) 
                    BEGIN
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'PortalId'+ '~~','ZC.PortalId')
                        SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'CatalogID'+ '~~','ZCn.CatalogID')
                        SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'Name' + '~~','dbo.FN_GetCategoryPath(CategoryNodeId)')
                    END ;
            END ;

-- ZNodeAttributeType and  ZNodeProductTypeAttribute 
        IF @SPName = 'GetAttributeTypesBy' 
            BEGIN
                SET @Select_Clause = 'ProductAttributeTypeID,ProductTypeId,AttributeTypeId,Name,Description,DisplayOrder,IsPrivate,LocaleId,PortalID'
                SET @Select_FROM_CLAUSE = 'PT.ProductAttributeTypeID,PT.ProductTypeId,PT.AttributeTypeId,AT.Name,AT.Description,AT.DisplayOrder,AT.IsPrivate,AT.LocaleId,AT.PortalID '
                SET @FROM_CLAUSE = ' from [ZNodeProductTypeAttribute] PT INNER JOIN [ZNodeAttributeType] AT  on  PT.AttributeTypeID = AT.AttributeTypeID '
                SET @OrderByDefault = 'Name'
                SET @L_GroupByClause = ''
                SET @L_SPfilterClause = ''

                IF ( @L_innerWhereClause IS NOT NULL OR LEN(@L_innerWhereClause) >= 1) 
                    BEGIN
                        SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'ProductTypeId' + '~~', 'PT.ProductTypeId')
                    END ;
            END ;

        IF @SPName = 'ZNode_SearchCategories' 
			BEGIN
				SET @Select_Clause = '  CategoryID,Name , Title ,ShortDescription ,  [Description] ,  ImageFile ,  ImageAltTag ,  VisibleInd ,  SubCategoryGridVisibleInd ,  SEOTitle ,'        
                SET @Select_Clause = @Select_Clause + 'SEOKeywords ,  SEODescription ,  AlternateDescription ,  DisplayOrder ,  Custom1 ,  Custom2 ,  Custom3 ,  SEOURL ,  CatalogName,    PortalID  '
                SET @Select_FROM_CLAUSE = '  ZNodeCategory.CategoryID ,     ZNodeCategory.Name ,  ZNodeCategory.Title ,  ZNodeCategory.ShortDescription ,  ZNodeCategory.[Description] ,  ZNodeCategory.ImageFile ,'
                SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE + 'ZNodeCategory.ImageAltTag ,  ZNodeCategory.VisibleInd ,  ZNodeCategory.SubCategoryGridVisibleInd ,  ZNodeCategory.SEOTitle ,  ZNodeCategory.SEOKeywords ,  '
                SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE + 'ZNodeCategory.SEODescription ,  ZNodeCategory.AlternateDescription ,  ZNodeCategory.DisplayOrder ,  ZNodeCategory.Custom1 ,  '
                SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE + 'ZNodeCategory.Custom2 ,  ZNodeCategory.Custom3 ,'
                SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE + 'ZNodeCategory.SEOURL ,  ZNodeCatalog.Name CatalogName,  ZNodeCatalog.CatalogID,  ZNodeCategory.PortalID   '
                SET @FROM_CLAUSE = 'FROM  ZNodeCategory  LEFT OUTER JOIN ZNodeCategoryNode  ON  ZNodeCategory.CategoryID = ZNodeCategoryNode.CategoryID  '
                SET @FROM_CLAUSE = @FROM_CLAUSE + 'LEFT OUTER JOIN ZNodeCatalog  ON  ZNodeCatalog.CatalogID = ZNodeCategoryNode.CatalogID '
                --SET @FROM_CLAUSE = @FROM_CLAUSE + 'LEFT OUTER JOIN webpages_profile  ON  ZNodeCategory.PortalID  =webpages_profile.PortalID '
                --SET @FROM_CLAUSE = @FROM_CLAUSE + 'LEFT OUTER JOIN WebPages_users  ON   WebPages_users.UserId  = webpages_profile.UserId '
                SET @OrderByDefault = 'DisplayOrder'
                SET @L_GroupByClause = ''

                if(@L_inLineWhereClause != '')
					SET @L_SPfilterClause =  @L_inLine
				else 
					SET @L_SPfilterClause = ''

                IF ( @L_innerWhereClause IS NOT NULL OR LEN(@L_innerWhereClause) >= 1) 
                    BEGIN
                        SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'Name' + '~~','ZNodeCategory.Name')
                        SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'CatalogID'+ '~~','ZNodeCatalog.CatalogID')
                        SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'PortalID'+ '~~','ZNodeCategory.PortalID')
                        SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'CategoryID'+ '~~','ZNodeCategory.CategoryID')
                       -- SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'UserName'+ '~~','WebPages_users.UserName')
                    END 
            END ;

		IF @SPName = 'ZNode_SearchCategoriesByPortalCatalog' 
            BEGIN
				SET @Select_Clause = '   CategoryID,Name , Title ,ShortDescription ,  [Description] ,  ImageFile ,  ImageAltTag ,  VisibleInd ,  SubCategoryGridVisibleInd ,  SEOTitle ,'        
                SET @Select_Clause = @Select_Clause + 'SEOKeywords ,  SEODescription ,  AlternateDescription ,  DisplayOrder ,  Custom1 ,  Custom2 ,  Custom3 ,  SEOURL ,  CatalogName,    PortalID  '
                SET @Select_FROM_CLAUSE = ' Distinct ZNodeCategory.CategoryID ,     ZNodeCategory.Name ,  ZNodeCategory.Title ,  ZNodeCategory.ShortDescription ,  ZNodeCategory.[Description] ,  ZNodeCategory.ImageFile ,'
                SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE + 'ZNodeCategory.ImageAltTag ,  ZNodeCategory.VisibleInd ,  ZNodeCategory.SubCategoryGridVisibleInd ,  ZNodeCategory.SEOTitle ,  ZNodeCategory.SEOKeywords ,  '
                SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE + 'ZNodeCategory.SEODescription ,  ZNodeCategory.AlternateDescription ,  ZNodeCategory.DisplayOrder ,  ZNodeCategory.Custom1 ,  '
                SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE + 'ZNodeCategory.Custom2 ,  ZNodeCategory.Custom3 ,'
                SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE + 'ZNodeCategory.SEOURL ,  ZNodeCatalog.Name CatalogName,  ZNodeCatalog.CatalogID,  ZNodeCategory.PortalID   '
                SET @FROM_CLAUSE = 'FROM  ZNodeCategory  LEFT OUTER JOIN ZNodeCategoryNode  ON  ZNodeCategory.CategoryID = ZNodeCategoryNode.CategoryID  '
                SET @FROM_CLAUSE = @FROM_CLAUSE + 'LEFT OUTER JOIN ZNodeCatalog  ON  ZNodeCatalog.CatalogID = ZNodeCategoryNode.CatalogID '
                SET @FROM_CLAUSE = @FROM_CLAUSE + 'LEFT OUTER JOIN Znodeportalcatalog on ZNodeCatalog.CatalogID =Znodeportalcatalog.CatalogID   '
                --SET @FROM_CLAUSE = @FROM_CLAUSE + 'LEFT OUTER JOIN webpages_profile  ON  ZNodeCategory.PortalID  =webpages_profile.PortalID '
                --SET @FROM_CLAUSE = @FROM_CLAUSE + 'LEFT OUTER JOIN WebPages_users  ON   WebPages_users.UserId  = webpages_profile.UserId '
                SET @OrderByDefault = 'DisplayOrder'
                SET @L_GroupByClause = ''

                if(@L_inLineWhereClause != '')
					SET @L_SPfilterClause =  @L_inLine
				else 
					SET @L_SPfilterClause = ''

                IF ( @L_innerWhereClause IS NOT NULL OR LEN(@L_innerWhereClause) >= 1) 
                    BEGIN
                        SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'Name' + '~~','ZNodeCategory.Name')
                        SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'CatalogID'+ '~~','ZNodeCatalog.CatalogID')
                        SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'PortalID'+ '~~','ZNodeCategory.PortalID')
                        SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'CategoryID'+ '~~','ZNodeCategory.CategoryID')
                       -- SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'UserName'+ '~~','WebPages_users.UserName')
                    END 
            END ;

        IF @SPName = 'ProductTypeWiseAttributes' 
            BEGIN
                SET @Select_Clause = 'ProductTypeId,AttributeTypeId,AttributeId,AttributeType, AttributeValue ,DisplayOrder  ,IsGiftCard'
                SET @Select_FROM_CLAUSE = ' ISNULL(pta.ProductTypeId,0) ProductTypeId ,'
				SET @Select_FROM_CLAUSE =  @Select_FROM_CLAUSE + 'ISNULL(at.AttributeTypeId,0) AttributeTypeId,ISNULL(pa.AttributeId,0) AttributeId, IsNull(at.NAME,'''') AttributeType '
				SET @Select_FROM_CLAUSE =  @Select_FROM_CLAUSE + ', isnull(pa.Name ,'''') AttributeValue,ISNULL(pa.DisplayOrder ,0) DisplayOrder , isnull(pt.IsGiftCard,0) IsGiftCard '
                SET @FROM_CLAUSE = ' from ZNodeAttributeType at left outer join ZNodeProductAttribute pa on at.AttributeTypeId = pa.AttributeTypeId '
				SET @FROM_CLAUSE = @FROM_CLAUSE + 'left outer join  ZNodeProductTypeAttribute pta on pta.AttributeTypeId = at.AttributeTypeId Left outer join ZNodeProductType pt on pta.ProductTypeId= pt.ProductTypeId'
                SET @OrderByDefault = ' AttributeTypeId,DisplayOrder'
                SET @L_GroupByClause = ''
                SET @L_SPfilterClause = ''

                IF ( @L_innerWhereClause IS NOT NULL OR LEN(@L_innerWhereClause) >= 1 ) 
                    BEGIN
                        SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'ProductTypeId' + '~~', 'pta.ProductTypeId')
                    END ;
            END ;

        IF @SPName = 'AttributeTypeWiseAttributes' 
            BEGIN
                SET @Select_Clause = 'AttributeTypeId,Name,Description,DisplayOrder,IsPrivate,LocaleId,PortalID,AttributeId,'
                SET @Select_Clause =@Select_Clause  + 'AttributeName,AttributeExternalId,AttributeDisplayOrder,AttributeIsActive,AttributeOldAttributeId'
                SET @Select_FROM_CLAUSE = 'at.AttributeTypeId,at.Name,at.Description,pa.DisplayOrder,at.IsPrivate,at.LocaleId,at.PortalID,pa.AttributeId,pa.Name AttributeName ,'
                SET @Select_FROM_CLAUSE =  @Select_FROM_CLAUSE   + 'pa.ExternalId AttributeExternalId ,pa.DisplayOrder AttributeDisplayOrder,pa.IsActive AttributeIsActive,pa.OldAttributeId AttributeOldAttributeId'
                SET @FROM_CLAUSE = 'from ZNodeAttributeType at inner join ZNodeProductAttribute pa on at.AttributeTypeId = pa.AttributeTypeId  '
                SET @OrderByDefault = 'DisplayOrder'
                SET @L_GroupByClause = ''
                SET @L_SPfilterClause = ''

                IF ( @L_innerWhereClause IS NOT NULL OR LEN(@L_innerWhereClause) >= 1) 
                    BEGIN
                        SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'AttributeTypeId'+ '~~','at.AttributeTypeId')
                    END ;
            END ;

        IF @SPName = 'ZNode_GetAvailableAddOn' 
         BEGIN
				IF (@L_inLineWhereClause IS NOT NULL OR LEN(@L_inLineWhereClause) >= 1) 
				BEGIN
					SET @L_inLineWhereClause = REPLACE(@L_inLineWhereClause,'~~' + 'ProductID'+ '~~','Isnull(ProductID,0)')
					SET @L_inLineWhereClause = ' and a.AddOnID NOT IN ( SELECT AddOnID FROM dbo.ZNodeProductAddOn WHERE  '+ @L_inLineWhereClause  +')'
				END ;

                SET @Select_Clause		= 'AddOnID,ProductID,Title,NAME,Description,DisplayOrder,DisplayType,OptionalInd,AllowBackOrder,InStockMsg,OutOfStockMsg,'
                SET @Select_Clause		=@Select_Clause +'BackOrderMsg,PromptMsg,TrackInventoryInd,LocaleId,ExternalID,ExternalAPIID,AccountID,PortalID ,AddOnValueName ' 
                
				SET @Select_FROM_CLAUSE = 'Distinct a.AddOnID,a.ProductID,a.Title,a.NAME,a.Description,a.DisplayOrder,a.DisplayType,a.OptionalInd,a.AllowBackOrder,a.InStockMsg,a.OutOfStockMsg,a.BackOrderMsg,'
                SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE + 'a.PromptMsg,a.TrackInventoryInd,a.LocaleId,a.ExternalID,a.ExternalAPIID,a.AccountID,a.PortalID '
                SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE + ', ( SELECT DISTINCT STUFF(( SELECT Convert(nvarchar,Isnull(X.Name,'''')) + '' ( ''+ Convert(nvarchar, Isnull(dbo.ZnodeConvertCurrency(RetailPrice,Isnull(a.PortalID,0)),'''')) + '') <br /> '' FROM ZNodeAddOnValue x WHERE x.AddOnID = a.AddOnID FOR XML PATH('''')), 1, 0, '''') ) AddOnValueName '
                
				SET @FROM_CLAUSE =		  'FROM dbo.ZNodeAddOn a'
                SET @FROM_CLAUSE = @FROM_CLAUSE + '  Left Outer JOIN ZNODEPRODUCTADDON b on a.AddOnID = b.AddOnID '
				SET @FROM_CLAUSE = @FROM_CLAUSE + ' Left Outer JOIN  znodeProduct c on b.ProductId = c.ProductId '
				SET @FROM_CLAUSE = @FROM_CLAUSE + ' Left Outer Join [ZNodeAddOnValue] d on a.AddOnID = d.AddOnID '

                SET @OrderByDefault   =   'NAME'
				SET @L_GroupByClause  =   ''
				SET @L_SPfilterClause =   @L_inLineWhereClause 

               IF ( @L_innerWhereClause IS NOT NULL OR LEN(@L_innerWhereClause) >= 1 ) 
                    BEGIN
                        SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'PortalID'+ '~~','Isnull(a.PortalID,0)')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'ProductNum'+ '~~','c.ProductNum ')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'SKU'+ '~~','d.[SKU]')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'Title'+ '~~','a.[Title]')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'NAME'+ '~~','a.[NAME]')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'AccountID'+ '~~','a.[AccountID]')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'NAME'+ '~~','a.[NAME]')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'LocaleId'+ '~~','a.[LocaleId]')
                    END ;
                    --@NAME VARCHAR (MAX), @TITLE VARCHAR (MAX), @SKUPRODUCT VARCHAR (MAX), @LocaleId INT=NULL, @AccountId INT=NULL, @PortalID int=NULL  
            END ;

	IF @SPName = 'ZNode_GetAssociatedAddOn' 
            BEGIN
		        SET @Select_Clause		= 'AddOnID,ProductID,Title,NAME,Description,DisplayOrder,DisplayType,OptionalInd,AllowBackOrder,InStockMsg,OutOfStockMsg,BackOrderMsg,'
		        SET @Select_Clause		= @Select_Clause + 'PromptMsg,TrackInventoryInd,LocaleId,ExternalID,ExternalAPIID,AccountID,PortalID ,ProductAddOnID'
                
				SET @Select_FROM_CLAUSE = 'ZAO.AddOnID,ZAO.ProductID,ZAO.Title,ZAO.NAME,ZAO.Description,ZAO.DisplayOrder,ZAO.DisplayType,ZAO.OptionalInd,ZAO.AllowBackOrder,'
                SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE + 'ZAO.InStockMsg,ZAO.OutOfStockMsg,ZAO.BackOrderMsg,ZAO.PromptMsg,ZAO.TrackInventoryInd,'
                SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE + 'ZAO.LocaleId,ZAO.ExternalID,ZAO.ExternalAPIID,ZAO.AccountID,ZAO.PortalID ,ZPA.ProductAddOnID                '

                SET @FROM_CLAUSE =		  ' FROM ZNodeProductAddOn ZPA INNER JOIN ZNodeAddOn ZAO ON ZPA.AddOnID = ZAO.AddOnID '
                SET @OrderByDefault   =   'NAME'
                SET @L_GroupByClause  =   ''
                SET @L_SPfilterClause =   ''

                IF ( @L_innerWhereClause IS NOT NULL OR LEN(@L_innerWhereClause) >= 1) 
                    BEGIN
                        SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'PortalID'+ '~~','PortalID')
                        SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'ProductID'+ '~~','ZPA.ProductID')
                    END ;
            END ;

        IF @SPName = 'ZNode_GetAvailableHighLights' 
            BEGIN
				IF (@L_inLineWhereClause IS NOT NULL OR LEN(@L_inLineWhereClause) >= 1) 
				BEGIN
					SET @L_inLineWhereClause = REPLACE(@L_inLineWhereClause,'~~' + 'ProductID'+ '~~','Isnull(ProductID,0)')
					SET @L_inLineWhereClause = 'AND  HighlightID NOT IN (SELECT  HighlightID FROM dbo.ZNodeProductHighlight WHERE   '+ @L_inLineWhereClause  +')'
				END ;

                SET @Select_Clause		= 'HighlightID,ImageFile,ImageAltTag,NAME,Description,DisplayPopup,'
                SET @Select_Clause		= @Select_Clause +  'Hyperlink,HyperlinkNewWinInd,HighlightTypeID,ActiveInd,DisplayOrder,ShortDescription,LocaleId,PortalID,HighlightTypeName'		 
                SET @Select_FROM_CLAUSE = 'a.HighlightID,a.ImageFile,a.ImageAltTag,a.NAME,a.Description,a.DisplayPopup,a.Hyperlink,a.HyperlinkNewWinInd,'
                SET @Select_FROM_CLAUSE =  @Select_FROM_CLAUSE  + 'a.HighlightTypeID,a.ActiveInd,a.DisplayOrder,a.ShortDescription,a.LocaleId,a.PortalID,b.NAME HighlightTypeName'
				SET @FROM_CLAUSE =		  'FROM dbo.ZNodeHighlight a LEFT OUTER JOIN ZNodeHighlightType b ON a.HighlightTypeID = b.HighlightTypeID'
                SET @OrderByDefault   =   'NAME'
                SET @L_GroupByClause  =   ''

				SET @L_SPfilterClause =   @L_inLineWhereClause 

                IF ( @L_innerWhereClause IS NOT NULL OR LEN(@L_innerWhereClause) >= 1) 
                    BEGIN
                        SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'PortalID'+ '~~','PortalID')
                        SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'NAME'+ '~~','a.NAME')
                        SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'HighlightTypeName'+ '~~','b.NAME')
                        SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'HighlightTypeID'+ '~~','a.HighlightTypeID')
                        SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'LocaleId'+ '~~','a.LocaleId')
                    END ;
            END ;

         IF @SPName = 'ZNode_GetAssocitedHighLights' 
            BEGIN
				SET @Select_Clause		= 'HighlightID,ImageFile,ImageAltTag,NAME,Description,DisplayPopup,Hyperlink,'
				SET @Select_Clause		=  @Select_Clause		 + 'HyperlinkNewWinInd,HighlightTypeID,ActiveInd,DisplayOrder,ShortDescription,LocaleId,PortalID,ProductHighlightID,HighlightTypeName '
                SET @Select_FROM_CLAUSE = 'ZHL.HighlightID,ZHL.ImageFile,ZHL.ImageAltTag,ZHL.NAME,ZHL.Description,ZHL.DisplayPopup,ZHL.Hyperlink,ZHL.HyperlinkNewWinInd,ZHL.HighlightTypeID,'
				SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE  + 'ZHL.ActiveInd,ZHL.DisplayOrder,ZHL.ShortDescription,ZHL.LocaleId,ZHL.PortalID ,ZPH.ProductHighlightID ,b.NAME HighlightTypeName'
                SET @FROM_CLAUSE =		  'FROM ZNodeProductHighlight ZPH INNER JOIN ZNodeHighlight ZHL ON ZPH.HighlightID = ZHL.HighlightID '
                SET @FROM_CLAUSE =		  @FROM_CLAUSE + ' LEFT OUTER JOIN ZNodeHighlightType b ON ZHL .HighlightTypeID = b.HighlightTypeID '
                SET @OrderByDefault   =   'NAME'
                SET @L_GroupByClause  =   ''
                SET @L_SPfilterClause =   ' and ZHL.ActiveInd =1 '

                IF ( @L_innerWhereClause IS NOT NULL OR LEN(@L_innerWhereClause) >= 1) 
				Begin
					SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'PortalID'+ '~~','ZHL.PortalID')
                    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'ProductID'+ '~~','ZPH.ProductID')
                END ;
            END ;

        IF @SPName = 'ZNodeGetProductCategoryByProductID' 
            BEGIN
                SET @Select_Clause = 'ProductCategoryID,ProductID,CategoryID,Name,Title'
                SET @Select_FROM_CLAUSE = 'ProductCategoryID,PC.ProductID,PC.CategoryID,C.Name,C.Title'
                SET @FROM_CLAUSE = ' FROM ZNodeProductCategory PC INNER JOIN ZNodeProduct P ON PC.ProductID = P.ProductID INNER JOIN ZNodeCategory C ON PC.CategoryID = C.CategoryID  LEFT JOIN ZNodePortal ZP ON ZP.PortalId = c.PortalId '
                SET @OrderByDefault = 'Name'
                SET @L_GroupByClause = ''
                SET @L_SPfilterClause = ''
				--PC.ProductID = @PRODUCTID AND ( ZP.PortalId = @PORTALID OR @PORTALID IS NULL)  
                IF ( @L_innerWhereClause IS NOT NULL OR LEN(@L_innerWhereClause) >= 1) 
					BEGIN
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ProductID'+ '~~','PC.ProductID')
                        SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'PortalId'+ '~~','ZP.PortalId')
					END ;
			END ;

        IF @SPName = 'ZNodeGetProductCategoryNonAssociated' 
            BEGIN
				IF (@L_inLineWhereClause IS NOT NULL OR LEN(@L_inLineWhereClause) >= 1) 
				BEGIN
					SET @L_inLineWhereClause = REPLACE(@L_inLineWhereClause,'~~' + 'ProductID'+ '~~','Isnull(ProductID,0)')
					SET @L_inLineWhereClause = ' and CategoryID NOT IN (SELECT CategoryID FROM ZNodeProductCategory WHERE '+ @L_inLineWhereClause  +')'
				END ;

                SET @Select_Clause		= 'CategoryID,Name,Title'
                SET @Select_FROM_CLAUSE = 'CategoryID,Name,Title'
                SET @FROM_CLAUSE =		  'FROM ZNodeCategory'
                SET @OrderByDefault   =   'NAME'
                SET @L_GroupByClause  =   ''
                SET @L_SPfilterClause =   @L_inLineWhereClause 

                IF ( @L_innerWhereClause IS NOT NULL OR LEN(@L_innerWhereClause) >= 1) 

                        SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'Name'+ '~~',' Name ')

                        SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'PortalID'+ '~~',' PortalID ')
            END ;

        IF @SPName = 'ZNodeGetProductFacetGroup' 

            BEGIN
				SET @Select_Clause = 'CatalogID,FacetGroupID,FacetGroupLabel,FacetName'
                SET @Select_FROM_CLAUSE = '  c.CatalogID,c.FacetGroupID,c.FacetGroupLabel, ( SELECT SUBSTRING(FacetName, 1, LEN(FacetName) - 1) FacetName '
				SET @FROM_CLAUSE =  ' FROM ( SELECT DISTINCT STUFF(( SELECT CAST(x.FacetName AS NVARCHAR) + '', '' FROM ZNodeFacet X '
				SET @FROM_CLAUSE = @FROM_CLAUSE  + ' INNER JOIN ZNodeFacetProductSKU ASKU ON a.ProductID = ASKU.ProductID AND x.FacetID = ASKU.FacetID ' 
				SET @FROM_CLAUSE = @FROM_CLAUSE  + '  WHERE x.FacetGroupID = c.FacetGroupID FOR XML PATH('''')), 1, 0, '''') FacetName ) Tbl) FacetName'
				SET @FROM_CLAUSE = @FROM_CLAUSE  + ' FROM ZNodeProductCategory a INNER JOIN ZNodeFacetGroupCategory b ON a.CategoryID = b.CategoryID '
				SET @FROM_CLAUSE = @FROM_CLAUSE  + ' INNER JOIN ZNodeFacetGroup c ON b.FacetGroupID = c.FacetGroupID '
				SET @FROM_CLAUSE = @FROM_CLAUSE  + ' INNER JOIN ZNodeFacet d ON b.FacetGroupID = D.FacetGroupID '
				--	SET @FROM_CLAUSE = @FROM_CLAUSE  + ' INNER JOIN ZNodeFacetProductSKU ASKU1 on  d.FacetID = ASKU1.FacetID '
                SET @OrderByDefault = 'FacetGroupLabel'
                SET @L_GroupByClause = 'GROUP BY  a.ProductID ,c.CatalogID ,c.FacetGroupID ,c.FacetGroupLabel'
				SET @L_SPfilterClause =   '  AND ( SELECT SUBSTRING(FacetName, 1, LEN(FacetName) - 1) FacetName '
				SET @L_SPfilterClause =  @L_SPfilterClause + '  FROM ( SELECT DISTINCT STUFF(( SELECT CAST(x.FacetName AS NVARCHAR) + '', '' FROM ZNodeFacet X'
				SET @L_SPfilterClause =  @L_SPfilterClause + '  INNER JOIN ZNodeFacetProductSKU ASKU ON a.ProductID = ASKU.ProductID AND x.FacetID = ASKU.FacetID'
				SET @L_SPfilterClause =  @L_SPfilterClause + '  WHERE x.FacetGroupID = c.FacetGroupID '
				SET @L_SPfilterClause =  @L_SPfilterClause + '  FOR XML PATH('''')), 1, 0, '''') FacetName ) Tbl)  IS NOT NULL '
				--PC.ProductID = @PRODUCTID AND ( ZP.PortalId = @PORTALID OR @PORTALID IS NULL)  
                IF ( @L_innerWhereClause IS NOT NULL OR LEN(@L_innerWhereClause) >= 1) 
                    BEGIN
                        SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ProductID'+ '~~','a.ProductID')
						--SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'SKUID'+ '~~','ASKU1.SKUID')
					END ;
			END ;

			IF @SPName = 'ZNodeGetQtyReorderLverl' 
	        BEGIN
				SET @Select_Clause = 'AddOnValueID,NAME,SKU,IsDefault,DisplayOrder,RetailPrice,AddOnId, QuantityOnHand,ReOrderLevel'
                SET @Select_FROM_CLAUSE = ' ZAV.AddOnValueID,ZAV.NAME,ZAV.SKU,ZAV.DefaultInd IsDefault,ZAV.DisplayOrder,ZAV.RetailPrice,ZAV.AddOnId,Isnull(ZSI.QuantityOnHand,0)QuantityOnHand,isnull( ZSI.ReOrderLevel,0) ReOrderLevel'
				SET @FROM_CLAUSE =  ' FROM dbo.ZNodeAddOnValue  ZAV INNER JOIN  ZNodeSKUInventory ZSI ON ZAV.SKU = ZSI.SKU '
				SET @OrderByDefault = 'DisplayOrder'
                SET @L_GroupByClause = ''
				SET @L_SPfilterClause =   ''
				--PC.ProductID = @PRODUCTID AND ( ZP.PortalId = @PORTALID OR @PORTALID IS NULL)  
                IF ( @L_innerWhereClause IS NOT NULL OR LEN(@L_innerWhereClause) >= 1) 
                    BEGIN
                        SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'AddOnId'+ '~~','ZAV.AddOnId')
                    END ;
            END ;

			IF @SPName = 'ZNodeSearchAddons' 
            BEGIN
            	SET @Select_Clause = 'AddOnID,Title,NAME,Description,DisplayOrder,DisplayType,IsOptional,AllowBackOrder,InStockMsg,OutOfStockMsg,BackOrderMsg,'
				SET @Select_Clause =  @Select_Clause  + ' PromptMsg,TrackInventoryInd,LocaleId,ExternalID,ExternalAPIID,AccountID,PortalID'
                SET @Select_FROM_CLAUSE = 'DISTINCT a.AddOnID,a.Title ,a.NAME,a.Description,a.DisplayOrder,a.DisplayType,a.OptionalInd IsOptional,a.AllowBackOrder,'
				SET @Select_FROM_CLAUSE =@Select_FROM_CLAUSE  + 'a.InStockMsg,a.OutOfStockMsg,a.BackOrderMsg,a.PromptMsg,a.TrackInventoryInd,a.LocaleId,a.ExternalID,a.ExternalAPIID,a.AccountID,a.PortalID'
				SET @FROM_CLAUSE =  ' FROM dbo.ZNodeAddOn a left outer JOIN dbo.ZNodeAddOnValue b ON a.AddOnID= b.AddOnID  '
				SET @OrderByDefault = 'DisplayOrder'
                SET @L_GroupByClause = ''

                IF (@L_inLineWhereClause IS NOT NULL OR LEN(@L_inLineWhereClause) >= 1) 
					SET @L_SPfilterClause = @L_inLine  
				ELSE 
					SET @L_SPfilterClause = ''

				--PC.ProductID = @PRODUCTID AND ( ZP.PortalId = @PORTALID OR @PORTALID IS NULL)  
                IF ( @L_innerWhereClause IS NOT NULL OR LEN(@L_innerWhereClause) >= 1) 
					BEGIN 
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'SKU'+ '~~','b.SKU')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'NAME'+ '~~','a.NAME')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'Title'+ '~~','a.Title')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'PortalID'+ '~~','a.PortalID')
					END 
            END ;

			IF @SPName = 'ZNodeAccountDetails' 
            BEGIN
            --ZPP.PortalID, 
				SET @L_inLine = ''
				SET @L_inLineWhereClause = REPLACE(@L_inLineWhereClause,'~~' + 'UserName'+ '~~','Isnull(UserName,'''')')

				 if(@L_inLineWhereClause != '')
					Begin
						SET @L_inLine  = ' and (isnull(wp.PortalId  ,0) in '
						SET @L_inLine = @L_inLine + ' (select a.PortalId from webpages_profile a inner join WebPages_users b  on a.UserId  = b.UserId where  '+ @L_inLineWhereClause +' ) '
						SET @L_inLine = @L_inLine + ' OR (wp.PortalId  =wp.PortalId  and '
						SET @L_inLine = @L_inLine + ' (select top 1  a.PortalId from webpages_profile a inner join WebPages_users b  on a.UserId  = b.UserId where  '+ @L_inLineWhereClause+' ) <= 0'
						SET @L_inLine = @L_inLine + ' ) )'
					END

				SET @Select_Clause = '   RoleName, AccountID,ParentAccountID,UserID,ExternalAccountNo,CompanyName,AddressCompanyName,AccountTypeID,ProfileID,AccountProfileCode'
				SET @Select_Clause  = @Select_Clause  + ' ,SubAccountLimit,Description,CreateUser,CreateDte,UpdateUser,UpdateDte,ActiveInd,Website,Source ' 
				SET @Select_Clause  = @Select_Clause  + ' ,ReferralAccountID,ReferralStatus,Custom1,Custom2,Custom3,EmailOptIn,WebServiceDownloadDte,ReferralCommission'
				SET @Select_Clause  = @Select_Clause  + ' ,ReferralCommissionTypeID,TaxID,Email,EnableCustomerPricing'
				SET @Select_Clause  = @Select_Clause  + ' ,AddressID,FirstName,MiddleName,LastName,Street,Street1'
				SET @Select_Clause  = @Select_Clause  + ' ,City,StateCode,PostalCode,CountryCode,PhoneNumber,IsDefaultBilling,IsDefaultShipping,NAME,UserName,FullName,IsConfirmed'
				SET @Select_FROM_CLAUSE = ' distinct  wr.RoleName,za.AccountID,za.ParentAccountID,za.UserID,za.ExternalAccountNo,za.CompanyName,za2.CompanyName as AddressCompanyName,za.AccountTypeID,za.ProfileID,za.AccountProfileCode'
				SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE  + ' ,za.SubAccountLimit, '''' Description ,za.CreateUser,za.CreateDte,za.UpdateUser,za.UpdateDte,za.ActiveInd,za.Website,za.Source ' 
				SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE  + ' ,za.ReferralAccountID,za.ReferralStatus,za.Custom1,za.Custom2,za.Custom3,za.EmailOptIn,za.WebServiceDownloadDte,za.ReferralCommission'
				SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE + ' ,za.ReferralCommissionTypeID,za.TaxID,za.Email,za.EnableCustomerPricing'
				SET @Select_FROM_CLAUSE  = @Select_FROM_CLAUSE  + ' ,za2.AddressID,za2.FirstName,za2.MiddleName,za2.LastName,za2.Street,za2.Street1'
				SET @Select_FROM_CLAUSE  = @Select_FROM_CLAUSE + ' ,za2.City,za2.StateCode,za2.PostalCode,za2.CountryCode,za2.PhoneNumber,za2.IsDefaultBilling,'
				SET @Select_FROM_CLAUSE  = @Select_FROM_CLAUSE + ' za2.IsDefaultShipping,za2.NAME,WUS.UserName,ISNULL(za2.FirstName,'''')  + '' '' + Isnull( za2.MiddleName,'''') + '' '' + Isnull(za2.LastName,'''') FullName,wpm.IsConfirmed'
				
				--left outer JOIN  dbo.ZNodeAccountProfile ZAP ON za.AccountID = Zap.AccountID  
				-- left outer JOIN   dbo.ZNodePortalProfile ZPP ON ZAP.ProfileID = ZPP.ProfileID  
				SET @FROM_CLAUSE =  ' FROM ZNodeAccount za INNER JOIN webpages_UsersInRoles wuir ON za.UserID = wuir.UserId INNER JOIN webpages_Roles wr ON wuir.RoleId = wr.RoleId '
				SET @FROM_CLAUSE =  @FROM_CLAUSE + ' LEFT OUTER JOIN ZNodeAddress za2 ON za.AccountID = za2.AccountID  ' 
				SET @FROM_CLAUSE =  @FROM_CLAUSE + ' LEFT OUTER JOIN webpages_users WUS ON za.UserID = WUS.UserID  ' 
				
				--SET @FROM_CLAUSE =  @FROM_CLAUSE + ' LEFT OUTER JOIN dbo.ZNodeAccountProfile ZAP ON za.AccountID = Zap.AccountID '
				--SET @FROM_CLAUSE =  @FROM_CLAUSE + ' LEFT OUTER JOIN dbo.ZNodePortalProfile ZPP ON ZAP.ProfileID = ZPP.ProfileID '
				--SET @FROM_CLAUSE =  @FROM_CLAUSE + ' LEFT OUTER JOIN dbo.webpages_Membership wpm ON za.UserID = wpm.userid'
				SET @FROM_CLAUSE =  @FROM_CLAUSE + ' LEFT OUTER JOIN  webpages_Profile WP ON za.UserID = WP.UserID  '
				SET @FROM_CLAUSE =  @FROM_CLAUSE + ' LEFT OUTER JOIN dbo.webpages_Membership wpm ON za.UserID = wpm.userid'
				SET @FROM_CLAUSE =  @FROM_CLAUSE + ' LEFT OUTER JOIN dbo.ZNodePortalProfile ZPP ON wp.PortalId = ZPP.PortalId '
				SET @L_GroupByClause= ''
				SET @OrderByDefault = 'AccountID DESC '
                --SET @L_GroupByClause = ''
				if @L_inLineWhereClause IS NOT NULL OR LEN(@L_inLineWhereClause) >= 1
					SET @L_SPfilterClause =   ' and za2.IsDefaultBilling=1 ' + @L_inLine 
                else 
					SET @L_SPfilterClause =   ' and za2.IsDefaultBilling=1 ' 
				
				--SET @L_SPfilterClause = 
				--PC.ProductID = @PRODUCTID AND ( ZP.PortalId = @PORTALID OR @PORTALID IS NULL)    
                IF ( @L_innerWhereClause IS NOT NULL OR LEN(@L_innerWhereClause) >= 1 ) 
				Begin
                    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'RoleName'+ '~~','wr.RoleName')
                    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'FirstName'+ '~~','za2.FirstName')
                    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'LastName'+ '~~','za2.LastName')
                    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ProfileID'+ '~~','ZPP.ProfileID')
                    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'RoleName'+ '~~','wr.RoleName')
                    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'LoginName'+ '~~','WUS.UserName')
				    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'AccountID'+ '~~','za.AccountID')
                    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ExternalAccountNum'+ '~~','za.ExternalAccountNo')
				    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'PhoneNumber'+ '~~','za2.PhoneNumber')
                    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'Email'+ '~~','za.Email')
                    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ReferralStatus'+ '~~','za.ReferralStatus')
                    --SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'PortalID'+ '~~','ZPP.PortalID')
                    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'PortalID'+ '~~','wP.PortalID')
                    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'CreateDate'+ '~~','Convert(date,ZA.CreateDte)')
                    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'CompanyName'+ '~~','za.CompanyName')
                 END
            END ;

		IF @SPName = 'ZNodeFullAccountDetails' 
        BEGIN
            --ZPP.PortalID, 
				SET @L_inLine = ''
				SET @L_inLineWhereClause = REPLACE(@L_inLineWhereClause,'~~' + 'UserName'+ '~~','Isnull(UserName,'''')')

				if(@L_inLineWhereClause != '')
					Begin
						SET @L_inLine  = ' and (isnull(ZPP.PortalId  ,0) in '
						SET @L_inLine = @L_inLine + ' (select a.PortalId from webpages_profile a inner join WebPages_users b  on a.UserId  = b.UserId where  '+ @L_inLineWhereClause +' ) '
						SET @L_inLine = @L_inLine + ' OR (ZPP.PortalId  =ZPP.PortalId  and '
						SET @L_inLine = @L_inLine + ' (select top 1  a.PortalId from webpages_profile a inner join WebPages_users b  on a.UserId  = b.UserId where  '+ @L_inLineWhereClause+' ) <= 0'
						SET @L_inLine = @L_inLine + ' ) )'
					END

				SET @Select_Clause = '   RoleName, AccountID,ParentAccountID,UserID,ExternalAccountNo,CompanyName,AccountTypeID,ProfileID,AccountProfileCode'
				SET @Select_Clause  = @Select_Clause  + ' ,SubAccountLimit,Description,CreateUser,CreateDte,UpdateUser,UpdateDte,ActiveInd,Website,Source ' 
				SET @Select_Clause  = @Select_Clause  + ' ,ReferralAccountID,ReferralStatus,Custom1,Custom2,Custom3,EmailOptIn,WebServiceDownloadDte,ReferralCommission'
				SET @Select_Clause  = @Select_Clause  + ' ,ReferralCommissionTypeID,TaxID,Email,EnableCustomerPricing'
				SET @Select_Clause  = @Select_Clause  + ' ,AddressID,FirstName,MiddleName,LastName,Street,Street1'
				SET @Select_Clause  = @Select_Clause  + ' ,City,StateCode,PostalCode,CountryCode,PhoneNumber,IsDefaultBilling,IsDefaultShipping,NAME,UserName,FullName,IsConfirmed'
				SET @Select_FROM_CLAUSE = ' distinct  wr.RoleName,za.AccountID,za.ParentAccountID,za.UserID,za.ExternalAccountNo,za.CompanyName,za.AccountTypeID,za.ProfileID,za.AccountProfileCode'
				SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE  + ' ,za.SubAccountLimit, '''' Description ,za.CreateUser,za.CreateDte,za.UpdateUser,za.UpdateDte,za.ActiveInd,za.Website,za.Source ' 
				SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE  + ' ,za.ReferralAccountID,za.ReferralStatus,za.Custom1,za.Custom2,za.Custom3,za.EmailOptIn,za.WebServiceDownloadDte,za.ReferralCommission'
				--SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE + ' ,za.ReferralCommissionTypeID,za.TaxID,Case when za.Email is null then WUS.Email  else za.Email  end Email ,za.EnableCustomerPricing'
				SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE + ' ,za.ReferralCommissionTypeID,za.TaxID,za.Email ,za.EnableCustomerPricing'
				SET @Select_FROM_CLAUSE  = @Select_FROM_CLAUSE  + ' ,za2.AddressID,za2.FirstName,za2.MiddleName,za2.LastName,za2.Street,za2.Street1'
				SET @Select_FROM_CLAUSE  = @Select_FROM_CLAUSE + ' ,za2.City,za2.StateCode,za2.PostalCode,za2.CountryCode,za2.PhoneNumber,za2.IsDefaultBilling,'
				SET @Select_FROM_CLAUSE  = @Select_FROM_CLAUSE + ' za2.IsDefaultShipping,za2.NAME,WUS.UserName,ISNULL(za2.FirstName,'''')  + '' '' + Isnull( za2.MiddleName,'''') + '' '' + Isnull(za2.LastName,'''') FullName,wpm.IsConfirmed'
				SET @FROM_CLAUSE =  ' FROM ZNodeAccount za Left Outer JOIN  webpages_UsersInRoles wuir ON za.UserID = wuir.UserId Left Outer JOIN webpages_Roles wr ON wuir.RoleId = wr.RoleId '
				SET @FROM_CLAUSE =  @FROM_CLAUSE + ' LEFT OUTER JOIN ZNodeAddress za2 ON za.AccountID = za2.AccountID  ' 
				SET @FROM_CLAUSE =  @FROM_CLAUSE + ' LEFT OUTER JOIN webpages_users WUS ON za.UserID = WUS.UserID  ' 
				SET @FROM_CLAUSE =  @FROM_CLAUSE + ' LEFT OUTER JOIN dbo.ZNodeAccountProfile ZAP ON za.AccountID = Zap.AccountID '
				SET @FROM_CLAUSE =  @FROM_CLAUSE + ' LEFT OUTER JOIN dbo.ZNodePortalProfile ZPP ON ZAP.ProfileID = ZPP.ProfileID '
				SET @FROM_CLAUSE =  @FROM_CLAUSE + ' LEFT OUTER JOIN dbo.webpages_Membership wpm ON za.UserID = wpm.userid'
				--SET @FROM_CLAUSE =  ' FROM ZNodeAccount za INNER JOIN webpages_UsersInRoles wuir ON za.UserID = wuir.UserId INNER JOIN webpages_Roles wr ON wuir.RoleId = wr.RoleId '
				--SET @FROM_CLAUSE =  @FROM_CLAUSE + ' LEFT OUTER JOIN ZNodeAddress za2 ON za.AccountID = za2.AccountID '
				SET @L_GroupByClause= ''
				--SET @L_GroupByClause= ' GROUP BY  wr.RoleName,za.AccountID,za.ParentAccountID,za.UserID,za.ExternalAccountNo,za.CompanyName,za.AccountTypeID,za.ProfileID,za.AccountProfileCode'
				--SET @L_GroupByClause = @L_GroupByClause  + ' ,za.SubAccountLimit,za.Description,za.CreateUser,za.CreateDte,za.UpdateUser,za.UpdateDte,za.ActiveInd,za.Website,za.Source ' 
				--SET @L_GroupByClause = @L_GroupByClause  + ' ,za.ReferralAccountID,za.ReferralStatus,za.Custom1,za.Custom2,za.Custom3,za.EmailOptIn,za.WebServiceDownloadDte,za.ReferralCommission'
				--SET @L_GroupByClause = @L_GroupByClause  + ' ,za.ReferralCommissionTypeID,za.TaxID,za.Email,za.EnableCustomerPricing'
				--SET @L_GroupByClause  = @L_GroupByClause + ' ,za2.AddressID,za2.FirstName,za2.MiddleName,za2.LastName,za2.Street,za2.Street1'
				--SET @L_GroupByClause  = @L_GroupByClause + ' ,za2.City,za2.StateCode,za2.PostalCode,za2.CountryCode,za2.PhoneNumber,za2.IsDefaultBilling,za2.IsDefaultShipping,za2.NAME,WUS.UserName'
				SET @OrderByDefault = 'AccountID Desc'
                --SET @L_GroupByClause = ''
                
				if @L_inLineWhereClause IS NOT NULL OR LEN(@L_inLineWhereClause) >= 1
					SET @L_SPfilterClause =   ' and za2.IsDefaultBilling=1 ' + @L_inLine 
                else 
					SET @L_SPfilterClause =   ' and za2.IsDefaultBilling=1 ' 

				--PC.ProductID = @PRODUCTID AND ( ZP.PortalId = @PORTALID OR @PORTALID IS NULL)    
                IF ( @L_innerWhereClause IS NOT NULL OR LEN(@L_innerWhereClause) >= 1 ) 
				Begin
                    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'RoleName'+ '~~','wr.RoleName')
                    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'FirstName'+ '~~','za2.FirstName')
                    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'LastName'+ '~~','za2.LastName')
                    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ProfileID'+ '~~','ZPP.ProfileID')
                    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'RoleName'+ '~~','wr.RoleName')
                    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'LoginName'+ '~~','WUS.UserName')
				    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'AccountID'+ '~~','za.AccountID')
                    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ExternalAccountNum'+ '~~','za.ExternalAccountNo')
                    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ExternalAccountNo'+ '~~','za.ExternalAccountNo')
				    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'PhoneNumber'+ '~~','za2.PhoneNumber')
                    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'Email'+ '~~','za.Email')
                    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ReferralStatus'+ '~~','za.ReferralStatus')
                    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'PortalID'+ '~~','ZPP.PortalID')
                    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'CreateDate'+ '~~','Convert(date,ZA.CreateDte)')
                    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'CompanyName'+ '~~','za.CompanyName')
                    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'PostalCode'+ '~~','za2.PostalCode')
					--PRFT Custom Code : Start    
					SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'custom2'+ '~~','IsNull(za.custom2,0)')
                    --PRFT Custom Code : End    
                 END
           --print @L_innerWhereClause
            END ;

		IF @SPName = 'ZNodeAdminAccounts' 
            BEGIN
            --ZPP.PortalID, 
				SET @Select_Clause = '   AccountID,ParentAccountID,UserID,ExternalAccountNo,CompanyName,AddressCompanyName,AccountTypeID,ProfileID,AccountProfileCode'
				SET @Select_Clause  = @Select_Clause  + ' ,SubAccountLimit,Description,CreateUser,CreateDte,UpdateUser,UpdateDte,ActiveInd,Website,Source ' 
				SET @Select_Clause  = @Select_Clause  + ' ,ReferralAccountID,ReferralStatus,Custom1,Custom2,Custom3,EmailOptIn,WebServiceDownloadDte,ReferralCommission'
				SET @Select_Clause  = @Select_Clause  + ' ,ReferralCommissionTypeID,TaxID,Email,EnableCustomerPricing'
				SET @Select_Clause  = @Select_Clause  + ' ,AddressID,FirstName,MiddleName,LastName,Street,Street1'
				SET @Select_Clause  = @Select_Clause  + ' ,City,StateCode,PostalCode,CountryCode,PhoneNumber,IsDefaultBilling,IsDefaultShipping,NAME,UserName,FullName,IsConfirmed'
				SET @Select_FROM_CLAUSE = ' distinct  za.AccountID,za.ParentAccountID,za.UserID,za.ExternalAccountNo,za.CompanyName,za2.CompanyName as AddressCompanyName,za.AccountTypeID,za.ProfileID,za.AccountProfileCode'
				SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE  + ' ,za.SubAccountLimit, '''' Description ,za.CreateUser,za.CreateDte,za.UpdateUser,za.UpdateDte,za.ActiveInd,za.Website,za.Source ' 
				SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE  + ' ,za.ReferralAccountID,za.ReferralStatus,za.Custom1,za.Custom2,za.Custom3,za.EmailOptIn,za.WebServiceDownloadDte,za.ReferralCommission'
				SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE + ' ,za.ReferralCommissionTypeID,za.TaxID,za.Email,za.EnableCustomerPricing'
				SET @Select_FROM_CLAUSE  = @Select_FROM_CLAUSE  + ' ,za2.AddressID,za2.FirstName,za2.MiddleName,za2.LastName,za2.Street,za2.Street1'
				SET @Select_FROM_CLAUSE  = @Select_FROM_CLAUSE + ' ,za2.City,za2.StateCode,za2.PostalCode,za2.CountryCode,za2.PhoneNumber,za2.IsDefaultBilling,'
				SET @Select_FROM_CLAUSE  = @Select_FROM_CLAUSE + ' za2.IsDefaultShipping,za2.NAME,WUS.UserName,ISNULL(za2.FirstName,'''')  + '' '' + Isnull( za2.MiddleName,'''') + '' '' + Isnull(za2.LastName,'''') FullName,wpm.IsConfirmed'
				SET @FROM_CLAUSE =  ' FROM ZNodeAccount za INNER JOIN webpages_UsersInRoles wuir ON za.UserID = wuir.UserId INNER JOIN webpages_Roles wr ON wuir.RoleId = wr.RoleId '
				SET @FROM_CLAUSE =  @FROM_CLAUSE + ' LEFT OUTER JOIN ZNodeAddress za2 ON za.AccountID = za2.AccountID  ' 
				SET @FROM_CLAUSE =  @FROM_CLAUSE + ' LEFT OUTER JOIN webpages_users WUS ON za.UserID = WUS.UserID  ' 
				SET @FROM_CLAUSE =  @FROM_CLAUSE + ' LEFT OUTER JOIN  webpages_Profile WP ON za.UserID = WP.UserID  '
				SET @FROM_CLAUSE =  @FROM_CLAUSE + ' LEFT OUTER JOIN dbo.webpages_Membership wpm ON za.UserID = wpm.userid'
				SET @FROM_CLAUSE =  @FROM_CLAUSE + ' LEFT OUTER JOIN dbo.ZNodePortalProfile ZPP ON wp.PortalId = ZPP.PortalId '
				SET @L_GroupByClause= ''
				SET @OrderByDefault = 'AccountID Desc'
            	
				if @L_inLineWhereClause IS NOT NULL OR LEN(@L_inLineWhereClause) >= 1
					SET @L_SPfilterClause =   ' and za2.IsDefaultBilling=1 and (wr.RoleName not in (''FRANCHISE'', ''VENDOR'') )  ' + @L_inLine 
                else 
					SET @L_SPfilterClause =   ' and za2.IsDefaultBilling=1 ' 

			    IF ( @L_innerWhereClause IS NOT NULL OR LEN(@L_innerWhereClause) >= 1 ) 
				Begin
                    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'RoleName'+ '~~','wr.RoleName')
                    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'FirstName'+ '~~','za2.FirstName')
                    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'LastName'+ '~~','za2.LastName')
                    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ProfileID'+ '~~','ZPP.ProfileID')
                    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'RoleName'+ '~~','wr.RoleName')
                    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'LoginName'+ '~~','WUS.UserName')
				    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'AccountID'+ '~~','za.AccountID')
                    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ExternalAccountNum'+ '~~','za.ExternalAccountNo')
				    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'PhoneNumber'+ '~~','za2.PhoneNumber')
                    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'Email'+ '~~','za.Email')
					SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ReferralStatus'+ '~~','za.ReferralStatus')
                    --SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'PortalID'+ '~~','ZPP.PortalID')
                    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'PortalID'+ '~~','wP.PortalID')
                    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'CreateDate'+ '~~','Convert(date,ZA.CreateDte)')
                    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'CompanyName'+ '~~','za.CompanyName')
                 END
            END ;

		--SELECT za.*, wr.RoleName FROM ZNodeAccount za INNER JOIN webpages_UsersInRoles wuir ON za.UserID = wuir.UserId INNER JOIN webpages_Roles wr ON wuir.RoleId = wr.RoleId
		--WHERE wr.RoleName = 'ADMIN'

		IF @SPName = 'ZNodeProductSKUDetails' 
            BEGIN
				SET @Select_Clause = 'ProductID,Name,SKUID,SKU,Custom1,Custom2,Custom3,SupplierID,Note,WeightAdditional,SKUPicturePath,ImageAltTag,DisplayOrder,'
				SET @Select_Clause  = @Select_Clause  + ' RetailPriceOverride,SalePriceOverride,WholesalePriceOverride,RecurringBillingPeriod,RecurringBillingFrequency, ' 
				SET @Select_Clause  = @Select_Clause  + ' RecurringBillingTotalCycles,RecurringBillingInitialAmount,IsActive,ExternalID ,QuantityOnHand,ReOrderLevel,'
				SET @Select_Clause  = @Select_Clause  + ' SKUInventoryID,PRODUCTTYPENAME , AttributeIds, ProductTypeId ,IsGiftCard,ParentChildProduct,AttributeCount'
				
				SET @Select_FROM_CLAUSE = 'zp.ProductID,zp.Name,zs.SKUID,zs.SKU,zs.Custom1,zs.Custom2,zs.Custom3,zs.SupplierID,zs.Note,zs.WeightAdditional,zs.SKUPicturePath,zs.ImageAltTag,zs.DisplayOrder,'
				SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE  + ' zs.RetailPriceOverride,zs.SalePriceOverride,zs.WholesalePriceOverride,zs.RecurringBillingPeriod,zs.RecurringBillingFrequency, ' 
				SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE  + ' zs.RecurringBillingTotalCycles,zs.RecurringBillingInitialAmount,zs.ActiveInd IsActive,zs.ExternalID ,zsi.QuantityOnHand,zsi.ReOrderLevel,'
				SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE  + ' zsi.SKUInventoryID,ISNULL(zt.NAME ,'''') PRODUCTTYPENAME,'
				SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE  + ' (select Substring(AttributeId,1,LEN(AttributeId)-1)AttributeId FROM ( SELECT DISTINCT Stuff((SELECT Cast(AttributeId AS NVARCHAR)+ '','' '
				SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE + ' FROM ZNodeSKUAttribute  WHERE SKUID  =zs.Skuid  FOR XML PATH('''')), 1, 0, '''')AttributeId )Tbl )AttributeIds, zt.ProductTypeId , '
				SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE  + ' isnull(zt.IsGiftCard,0) IsGiftCard, '
				SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE  + ' convert(Bit,( select isnull((SELECT  Top 1 1   FROM   [dbo].[ZNodeParentChildProduct]   WHERE     [ChildProductID] = zp.ProductID ),0)) )ParentChildProduct ,'
				SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE  + '(SELECT COUNT(*)FROM ZNodeAttributeType A inner join ZNodeProductTypeAttribute B on  A.[AttributeTypeId]  =B.AttributeTypeId  and  B.ProductTypeId =zp.ProductTypeID) AttributeCount' 

				SET @FROM_CLAUSE =  ' from ZNodeProduct zp inner join ZNodeSKU zs on zp.ProductID = zs.ProductID inner join ZNodeSKUInventory zsi on zs.SKU = zsi.SKU  '
				SET @FROM_CLAUSE =  @FROM_CLAUSE + ' INNER JOIN ZNODEPRODUCTTYPE zt ON zp.ProductTypeID = zt.ProductTypeId '

				SET @OrderByDefault = 'SKUID'
                SET @L_GroupByClause = ''
				SET @L_SPfilterClause =   ''
		    --PC.ProductID = @PRODUCTID AND ( ZP.PortalId = @PORTALID OR @PORTALID IS NULL)    
            
			    IF ( @L_innerWhereClause IS NOT NULL OR LEN(@L_innerWhereClause) >= 1) 
                    BEGIN
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'SKU'+ '~~','zs.SKU')
                        SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'SKUID'+ '~~','zs.SKUID')
                        SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ProductID'+ '~~','zp.ProductID')
                    END ;
            END ;

        IF @SPName = 'ZNodeGetProductByCriteria' 
            BEGIN
				SET @Select_Clause = 'ProductID,Name,ShortDescription,ProductNum,ProductTypeID, RetailPrice,SalePrice,WholesalePrice,ImageFile,ImageAltTag,CallForPricing,'
                SET @Select_Clause = @Select_Clause + 'HomepageSpecial,CategorySpecial,TaxClassID,PortalID,ActiveInd,Franchisable,CategoryName,ParentProduct'
                SET @Select_FROM_CLAUSE = 'a.[ProductID],a.[Name],a.[ShortDescription],a.[ProductNum],a.[ProductTypeID], a.[RetailPrice],a.[SalePrice]'
				SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE  + ',a.[WholesalePrice],a.[ImageFile],a.[ImageAltTag],a.[CallForPricing],a.[HomepageSpecial],'
                SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE  + ' a.[CategorySpecial],a.[TaxClassID],a.[PortalID],a.[ActiveInd],a.[Franchisable],e.[Name] CategoryName , '
                SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE  + ' CONVERT(BIT, ISNULL(( SELECT TOP 1 1 FROM   ZNodeParentChildProduct WHERE  ParentProductID = a.ProductID), 0) ) ParentProduct '
                SET @FROM_CLAUSE = ' FROM dbo.ZNodeProduct a LEFT OUTER JOIN dbo.ZNodeSKU b ON a.ProductID = b.ProductID LEFT OUTER JOIN   dbo.ZNodeManufacturer c  ON a.ManufacturerID = c.ManufacturerID'
				SET @FROM_CLAUSE = @FROM_CLAUSE  + ' LEFT OUTER JOIN  dbo.ZNodeProductCategory d ON a.ProductID = d.ProductID  LEFT OUTER JOIN   dbo.ZNodeCategory e ON d.CategoryID = e.CategoryID '
				SET @FROM_CLAUSE = @FROM_CLAUSE  + ' LEFT OUTER JOIN  dbo.ZNodeCategoryNode f ON d.CategoryID = f.CategoryID '
				SET @FROM_CLAUSE = @FROM_CLAUSE  + ' LEFT OUTER JOIN  dbo.ZnodePortalCatalog g ON f.CatalogID = g.CatalogID '
				SET @FROM_CLAUSE = @FROM_CLAUSE  + ' INNER JOIN  dbo.ZNodeProductType h ON a.ProductTypeID =  h.ProductTypeID'
				
				--SET @FROM_CLAUSE = @FROM_CLAUSE  + ' LEFT OUTER Join  ZNodeLuceneGlobalProductBoost i ON a.ProductID =  i.ProductID'
				SET @OrderByDefault = 'Name'
                SET @L_GroupByClause = 'GROUP BY a.[ProductID],a.[Name],a.[ShortDescription],a.[ProductNum],a.[ProductTypeID],'
				SET @L_GroupByClause = @L_GroupByClause + ' a.[RetailPrice],a.[SalePrice],a.[WholesalePrice],a.[ImageFile],a.[ImageAltTag],a.[CallForPricing],'
				SET @L_GroupByClause = @L_GroupByClause + 'a.[HomepageSpecial],a.[CategorySpecial],a.[TaxClassID],a.[PortalID],a.[ActiveInd],a.[Franchisable],e.[Name],h.NAME'
                SET @L_SPfilterClause = ''

                IF ( @L_innerWhereClause IS NOT NULL OR LEN(@L_innerWhereClause) >= 1) 
					BEGIN 
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ProductID'+ '~~','a.[ProductID]')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ProductName'+ '~~','a.[Name]')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ProductNumber'+ '~~','a.[ProductNum]')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'SKU'+ '~~','b.[SKU]')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ManufactureName'+ '~~','c.[Name]')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ManufacturerID'+ '~~','a.ManufacturerID')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ProductTypeID'+ '~~','a.[ProductTypeID]')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ProductType'+ '~~','h.[Name]')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'CategoryName'+ '~~','e.[Name]')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'PortalID'+ '~~','e.[PortalID]')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ManufacturerID'+ '~~','a.ManufacturerID')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'CatalogId'+ '~~','f.CatalogId')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'CategoryID'+ '~~','d.CategoryID')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'PortalID'+ '~~','g.PortalID')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'AccountId'+ '~~','a.AccountId')
				    END
            END ;

        IF @SPName = 'ZNodeGetDistinctProductsByCriteria' 
            BEGIN
				SET @Select_Clause = 'ProductID,Name,ShortDescription,ProductNum,ProductTypeID, RetailPrice,SalePrice,WholesalePrice,ImageFile,ImageAltTag,CallForPricing,'
                SET @Select_Clause = @Select_Clause + 'HomepageSpecial,CategorySpecial,TaxClassID,PortalID, IsActive ,Franchisable,QuantityOnHand,DisplayOrder ,ParentProduct,ReviewStateString'
                SET @Select_Clause = @Select_Clause + ',SEOURL,SEOTitle,SEOKeywords,SEODescription'
                
				SET @Select_FROM_CLAUSE = 'distinct a.[ProductID],a.[Name],a.[ShortDescription],a.[ProductNum],a.[ProductTypeID], a.[RetailPrice],a.[SalePrice]'
				SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE  + ',a.[WholesalePrice],a.[ImageFile],a.[ImageAltTag],a.[CallForPricing],a.[HomepageSpecial],'
                SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE  + ' a.[CategorySpecial],a.[TaxClassID],a.[PortalID],a.[ActiveInd] as IsActive,a.[Franchisable],'
				SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE  + ' (SELECT Top 1 SkuInventory.QuantityOnHand FROM ZNodeSKUInventory AS SkuInventory WHERE SkuInventory.SKU = b.SKU) QuantityOnHand ,a.DisplayOrder, '
				SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE  + ' CONVERT(BIT, ISNULL(( SELECT TOP 1 1 FROM   ZNodeParentChildProduct WHERE  ParentProductID = a.ProductID), 0) ) ParentProduct, '
                SET @Select_FROM_CLAUSE  = @Select_FROM_CLAUSE + ' Case when ISNULL(a.ReviewStateId,0) = 10  then ''<div class="img-circle symbol-circle yellow"></div>'''
				SET @Select_FROM_CLAUSE  = @Select_FROM_CLAUSE + ' when ISNULL(a.ReviewStateId,0) = 20  then ''<div class="img-circle symbol-circle green"></div>'''
				SET @Select_FROM_CLAUSE  = @Select_FROM_CLAUSE + ' when ISNULL(a.ReviewStateId,0) = 30  then ''<div class="img-circle symbol-circle red"></div>'''
				SET @Select_FROM_CLAUSE  = @Select_FROM_CLAUSE + ' when ISNULL(a.ReviewStateId,0) = 40  then ''<div class="img-circle symbol-circle grey"></div>'' else ''<div class="img-circle symbol-circle grey"></div>'' end ReviewStateString'
                SET @Select_FROM_CLAUSE  = @Select_FROM_CLAUSE + ' ,a.SEOURL,a.SEOTitle,a.SEOKeywords,a.SEODescription'
                SET @FROM_CLAUSE = ' FROM dbo.ZNodeProduct a LEFT OUTER JOIN dbo.ZNodeSKU b ON a.ProductID = b.ProductID and b.SKU in (select Top 1 X.SKU from dbo.ZNodeSKU  X where X.ProductID = b.ProductID  ) '
                SET @FROM_CLAUSE = @FROM_CLAUSE  + ' LEFT OUTER JOIN   dbo.ZNodeManufacturer c ON a.ManufacturerID = c.ManufacturerID'
				SET @FROM_CLAUSE = @FROM_CLAUSE + ' LEFT OUTER JOIN  dbo.ZNodeProductCategory d ON a.ProductID = d.ProductID  LEFT OUTER JOIN   dbo.ZNodeCategory e ON d.CategoryID = e.CategoryID '
				SET @FROM_CLAUSE = @FROM_CLAUSE  + ' LEFT OUTER JOIN  dbo.ZNodeCategoryNode f ON d.CategoryID = f.CategoryID '
				SET @FROM_CLAUSE = @FROM_CLAUSE  + ' LEFT OUTER JOIN  dbo.ZnodePortalCatalog g ON f.CatalogID = g.CatalogID '
				SET @FROM_CLAUSE = @FROM_CLAUSE  + ' INNER JOIN  dbo.ZNodeProductType h ON a.ProductTypeID =  h.ProductTypeID'
				
				SET @OrderByDefault = 'Name'
				SET @L_GroupByClause = ''
				SET @L_SPfilterClause = ''

                if(@L_inLineWhereClause != '')
					SET @L_SPfilterClause =  @L_inLine
				else 
					SET @L_SPfilterClause = ''

                IF ( @L_innerWhereClause IS NOT NULL OR LEN(@L_innerWhereClause) >= 1) 
					BEGIN 
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ProductID'+ '~~','a.[ProductID]')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ProductName'+ '~~','a.[Name]')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'Name'+ '~~','a.[Name]')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ProductNumber'+ '~~','a.[ProductNum]')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'SKU'+ '~~','b.[SKU]')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ManufactureName'+ '~~','c.[Name]')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ManufacturerID'+ '~~','a.ManufacturerID')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ProductTypeID'+ '~~','a.[ProductTypeID]')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ProductType'+ '~~','h.[Name]')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'CategoryName'+ '~~','e.[Name]')
						--SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'PortalID'+ '~~','e.[PortalID]')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'CatalogId'+ '~~','f.CatalogId')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'CategoryID'+ '~~','d.CategoryID')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'PortalID'+ '~~','a.PortalID')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'AccountId'+ '~~','a.AccountId')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ReviewStateID'+ '~~','a.ReviewStateID')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'Franchisable'+ '~~','a.Franchisable')
				    END
            END ;

            -- changes : display order / active ind  retrive from Znodeproductcatety 
            IF @SPName = 'ZNodeGetDistinctProductsByCategory' 
            BEGIN
				SET @Select_Clause = 'ProductID,Name,ShortDescription,ProductNum,ProductTypeID, RetailPrice,SalePrice,WholesalePrice,ImageFile,ImageAltTag,CallForPricing,'
                SET @Select_Clause = @Select_Clause + 'HomepageSpecial,CategorySpecial,TaxClassID,PortalID,IsActive ,Franchisable,QuantityOnHand,DisplayOrder '
                
				SET @Select_FROM_CLAUSE = 'distinct a.[ProductID],a.[Name],a.[ShortDescription],a.[ProductNum],a.[ProductTypeID], a.[RetailPrice],a.[SalePrice]'
				SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE  + ',a.[WholesalePrice],a.[ImageFile],a.[ImageAltTag],a.[CallForPricing],a.[HomepageSpecial],'
                SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE  + ' a.[CategorySpecial],a.[TaxClassID],a.[PortalID],d.[ActiveInd] IsActive,a.[Franchisable],'
				SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE  + ' (SELECT Top 1 SkuInventory.QuantityOnHand FROM ZNodeSKUInventory AS SkuInventory WHERE SkuInventory.SKU = b.SKU) QuantityOnHand ,d.DisplayOrder'
                
				SET @FROM_CLAUSE = ' FROM dbo.ZNodeProduct a LEFT OUTER JOIN dbo.ZNodeSKU b ON a.ProductID = b.ProductID and b.SKU in (select Top 1 X.SKU from dbo.ZNodeSKU  X where X.ProductID = b.ProductID  ) '
                SET @FROM_CLAUSE = @FROM_CLAUSE  + ' LEFT OUTER JOIN   dbo.ZNodeManufacturer c ON a.ManufacturerID = c.ManufacturerID'
				SET @FROM_CLAUSE = @FROM_CLAUSE + ' LEFT OUTER JOIN  dbo.ZNodeProductCategory d ON a.ProductID = d.ProductID  LEFT OUTER JOIN   dbo.ZNodeCategory e ON d.CategoryID = e.CategoryID '
				SET @FROM_CLAUSE = @FROM_CLAUSE  + ' LEFT OUTER JOIN  dbo.ZNodeCategoryNode f ON d.CategoryID = f.CategoryID '
				SET @FROM_CLAUSE = @FROM_CLAUSE  + ' LEFT OUTER JOIN  dbo.ZnodePortalCatalog g ON f.CatalogID = g.CatalogID '
				SET @FROM_CLAUSE = @FROM_CLAUSE  + ' INNER JOIN  dbo.ZNodeProductType h ON a.ProductTypeID =  h.ProductTypeID'
				SET @OrderByDefault = 'Name'
				SET @L_GroupByClause = ''

				--SET @L_GroupByClause = 'GROUP BY  b.SKU'
				--SET @L_GroupByClause = @L_GroupByClause + ' a.[RetailPrice],a.[SalePrice],a.[WholesalePrice],a.[ImageFile],a.[ImageAltTag],a.[CallForPricing],'
    --            SET @L_GroupByClause = @L_GroupByClause + 'a.[HomepageSpecial],a.[CategorySpecial],a.[TaxClassID],a.[PortalID],a.[ActiveInd],a.[Franchisable]'
                SET @L_SPfilterClause = ''

                IF ( @L_innerWhereClause IS NOT NULL OR LEN(@L_innerWhereClause) >= 1) 
					BEGIN 
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ProductID'+ '~~','a.[ProductID]')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ProductName'+ '~~','a.[Name]')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ProductNumber'+ '~~','a.[ProductNum]')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'SKU'+ '~~','b.[SKU]')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ManufactureName'+ '~~','c.[Name]')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ManufacturerID'+ '~~','a.ManufacturerID')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ProductTypeID'+ '~~','a.[ProductTypeID]')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ProductType'+ '~~','h.[Name]')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'CategoryName'+ '~~','e.[Name]')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'PortalID'+ '~~','e.[PortalID]')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'CatalogId'+ '~~','f.CatalogId')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'CategoryID'+ '~~','d.CategoryID')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'PortalID'+ '~~','g.PortalID')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'AccountId'+ '~~','a.AccountId')
				    END
            END ;

            IF @SPName = 'ZNodeGetProductByCriteriaWithBoost' 
            BEGIN
                SET @Select_Clause = 'ProductID,Name,ShortDescription,ProductNum,ProductTypeID, RetailPrice,SalePrice,WholesalePrice,ImageFile,ImageAltTag,CallForPricing,'
                SET @Select_Clause = @Select_Clause + 'HomepageSpecial,CategorySpecial,TaxClassID,PortalID,ActiveInd,Franchisable,Boost,CategoryName,BoostCategoryName,ParentProduct,ProductCategoryID'
                SET @Select_FROM_CLAUSE = 'a.[ProductID],a.[Name],a.[ShortDescription],a.[ProductNum],a.[ProductTypeID], a.[RetailPrice],a.[SalePrice]'
                SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE  + ',a.[WholesalePrice],a.[ImageFile],a.[ImageAltTag],a.[CallForPricing],a.[HomepageSpecial],'
                SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE  + ' a.[CategorySpecial],a.[TaxClassID],a.[PortalID],a.[ActiveInd],a.[Franchisable],j.[Boost] ,e.[Name] CategoryName ,e.NAME BoostCategoryName, '
                SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE  + ' CONVERT(BIT, ISNULL(( SELECT TOP 1 1 FROM   ZNodeParentChildProduct WHERE  ParentProductID = a.ProductID), 0) ) ParentProduct,d.ProductCategoryID '
				SET @FROM_CLAUSE = ' FROM dbo.ZNodeProduct a LEFT OUTER JOIN dbo.ZNodeSKU b ON a.ProductID = b.ProductID LEFT OUTER JOIN   dbo.ZNodeManufacturer c  ON a.ManufacturerID = c.ManufacturerID'
				SET @FROM_CLAUSE = @FROM_CLAUSE  + ' LEFT OUTER JOIN  dbo.ZNodeProductCategory d ON a.ProductID = d.ProductID  Inner JOIN   dbo.ZNodeCategory e ON d.CategoryID = e.CategoryID '
				SET @FROM_CLAUSE = @FROM_CLAUSE  + ' LEFT OUTER JOIN  dbo.ZNodeCategoryNode f ON d.CategoryID = f.CategoryID '
				SET @FROM_CLAUSE = @FROM_CLAUSE  + ' LEFT OUTER JOIN  dbo.ZnodePortalCatalog g ON f.CatalogID = g.CatalogID '
				SET @FROM_CLAUSE = @FROM_CLAUSE  + ' INNER JOIN  dbo.ZNodeProductType h ON a.ProductTypeID = h.ProductTypeID'
				
				--SET @FROM_CLAUSE = @FROM_CLAUSE + ' LEFT OUTER JOIN  ZNodeLuceneGlobalProductBoost i ON a.ProductID =  i.ProductID'
				--SET @FROM_CLAUSE = @FROM_CLAUSE  + ' LEFT OUTER JOIN  ZNodeProductCategory k ON a.ProductId = k.ProductId'
				SET @FROM_CLAUSE = @FROM_CLAUSE  + ' LEFT OUTER JOIN  ZNodeLuceneGlobalProductCategoryBoost j ON  d.ProductCategoryID = j.ProductCategoryID' --  i.Boost = j.Boost'
				--SET @FROM_CLAUSE = @FROM_CLAUSE  + ' LEFT OUTER JOIN  ZNodeCategory  l ON k.CategoryID = k.CategoryID'
				SET @OrderByDefault = '[Boost] desc'
                SET @L_GroupByClause = 'GROUP BY a.[ProductID],a.[Name],a.[ShortDescription],a.[ProductNum],a.[ProductTypeID],'
                SET @L_GroupByClause = @L_GroupByClause + ' a.[RetailPrice],a.[SalePrice],a.[WholesalePrice],a.[ImageFile],a.[ImageAltTag],a.[CallForPricing],'
                SET @L_GroupByClause = @L_GroupByClause + 'a.[HomepageSpecial],a.[CategorySpecial],a.[TaxClassID],a.[PortalID],a.[ActiveInd],a.[Franchisable],j.Boost,e.[Name],h.NAME,e.NAME ,d.ProductCategoryID'
                SET @L_SPfilterClause = ''

                IF ( @L_innerWhereClause IS NOT NULL OR LEN(@L_innerWhereClause) >= 1) 
					BEGIN 
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ProductID'+ '~~','a.[ProductID]')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'Name'+ '~~','a.[Name]')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ProductNumber'+ '~~','a.[ProductNum]')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'SKU'+ '~~','b.[SKU]')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ManufactureName'+ '~~','c.[Name]')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ManufacturerID'+ '~~','a.ManufacturerID')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ProductTypeID'+ '~~','a.[ProductTypeID]')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ProductType'+ '~~','h.[Name]')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'CategoryName'+ '~~','e.[Name]')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'PortalID'+ '~~','e.[PortalID]')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ManufacturerID'+ '~~','a.ManufacturerID')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'CatalogId'+ '~~','f.CatalogId')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'CategoryID'+ '~~','d.CategoryID')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'PortalID'+ '~~','g.PortalID')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'AccountId'+ '~~','a.AccountId')
				    END
            END ;

           IF @SPName = 'ZNodeGetDigitalAsset' 
            BEGIN
				SET @Select_Clause = 'DigitalAssetID,ProductID,DigitalAsset,OrderLineItemID'
                SET @Select_FROM_CLAUSE = 'ISNULL(DigitalAssetID,0) DigitalAssetID,ISNULL(ProductID,0) ProductID,isnull(DigitalAsset,0) DigitalAsset ,ISNULL(OrderLineItemID,0) OrderLineItemID'
				SET @FROM_CLAUSE =  ' FROM  dbo.ZNodeDigitalAsset '
				SET @OrderByDefault = 'DigitalAsset'
				SET @L_GroupByClause = ''
				SET @L_SPfilterClause =   ''
		    --PC.ProductID = @PRODUCTID AND ( ZP.PortalId = @PORTALID OR @PORTALID IS NULL)  
			IF ( @L_innerWhereClause IS NOT NULL OR LEN(@L_innerWhereClause) >= 1) 
                    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'DigitalAssetID'+ '~~','DigitalAssetID')
                    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ProductID'+ '~~','ProductID')
            END ;
			IF @SPName = 'ZnodeProductTier' 
            BEGIN
				SET @Select_Clause = 'ProductTierID,ProductID,ProfileID,TierStart,TierEnd,Price,ProfileName'
                SET @Select_FROM_CLAUSE = ' a.ProductTierID,ISNULL(a.ProductID,0) ProductID ,ISNULL(a.ProfileID,0) ProfileID ,ISNULL(a.TierStart,0) TierStart,ISNULL(a.TierEnd,0) TierEnd ,ISNULL(a.Price,0) Price, b.Name ProfileName'
				SET @FROM_CLAUSE =  ' FROM  dbo.ZnodeProductTier a Left Outer join dbo.ZNodeProfile b on a.ProfileID = b.ProfileID '
				SET @OrderByDefault = 'ProductTierID'
                SET @L_GroupByClause = ''
				SET @L_SPfilterClause =   ''
				--PC.ProductID = @PRODUCTID AND ( ZP.PortalId = @PORTALID OR @PORTALID IS NULL)  
                IF ( @L_innerWhereClause IS NOT NULL OR LEN(@L_innerWhereClause) >= 1) 
                    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ProductID'+ '~~','ProductID')
            END ;
			
			IF @SPName = 'ZNodeGetCustomerBasedPricing' 
            BEGIN
				SET @Select_Clause = 'AccountID,ExternalAccountNo,FullName,CompanyName,BasePrice,NegotiatedPrice ,Discount'
				SET @Select_FROM_CLAUSE = 'DISTINCT ZA.AccountID,ZA.ExternalAccountNo,ISNULL(A.FirstName ,'') + '''' + ISNULL(A.LastName ,'') AS FullName,ZA.CompanyName '
				SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE  + ',CASE WHEN ZPE.UseWholesalePricing = 1 THEN ISNULL(ZP.WholeSalePrice,ZP.RetailPrice) ELSE ZP.RetailPrice END	BasePrice'
				SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE  + ',ZCP.NegotiatedPrice ,CASE WHEN ZPE.UseWholesalePricing = 1 THEN ISNULL(ZP.WholeSalePrice,ZP.RetailPrice) '
				SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE  + ' ELSE  ZP.RetailPrice   END	- ISNULL(ZCP.NegotiatedPrice,0) AS Discount '
				SET @FROM_CLAUSE = ' FROM 	ZNodeAccount ZA LEFT JOIN ZNodeAccountProfile ZAP ON ZA.AccountID = ZAP.AccountID INNER JOIN ZNodeProfile ZPE ON ZAP.ProfileID = ZPE.ProfileID '
				SET @FROM_CLAUSE = @FROM_CLAUSE  + ' INNER JOIN ZNodeAddress A ON ZA.AccountID = A.AccountID  INNER JOIN ZNodeCustomerPricing ZCP ON ZA.ExternalAccountNo = ZCP.ExternalAccountNo ' 
				SET @FROM_CLAUSE = @FROM_CLAUSE  + ' INNER JOIN ZNodeSKU ZS ON ZCP.SKUExternalId = ZS.ExternalId INNER JOIN ZNodeProduct ZP ON ZP.ProductID = ZS.ProductID ' 
				SET @FROM_CLAUSE = @FROM_CLAUSE  + ' LEFT OUTER JOIN  ZNodePortalProfile zpp ON ZA.ProfileID = zPP.ProfileID'
				SET @OrderByDefault = 'AccountID'
                SET @L_GroupByClause = ''
				SET @L_SPfilterClause =   'and A.IsDefaultBilling=1 '
				 
				 --PC.ProductID = @PRODUCTID AND ( ZP.PortalId = @PORTALID OR @PORTALID IS NULL)  
                IF ( @L_innerWhereClause IS NOT NULL OR LEN(@L_innerWhereClause) >= 1) 
					BEGIN
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'FullName'+ '~~','ISNULL(A.FirstName ,'') + '''' + ISNULL(A.LastName ,'')')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'CompanyName'+ '~~','ZA.CompanyName')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ExternalAccountNo'+ '~~','ZA.ExternalAccountNo')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ExternalAccountNum'+ '~~','ZA.ExternalAccountNo')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ProductID'+ '~~','ZP.ProductID')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'PortalID'+ '~~','ZPP.PortalID')
                    END
            END ;

			IF @SPName = 'ZNodeGetPortalCountry' 
			BEGIN
				SET @Select_Clause = 'PortalCountryID,PortalID,CountryCode,IsBillingActive,IsShippingActive ,CountryName'
				SET @Select_FROM_CLAUSE = 'a.PortalCountryID,isnull(a.PortalID,0)PortalID,isnull(a.CountryCode,0) CountryCode,'
				SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE  + 'isnull(a.BillingActive,0) IsBillingActive,isnull(a.ShippingActive ,0) IsShippingActive,b.Name CountryName'
				SET @FROM_CLAUSE = ' FROM dbo.ZNodePortalCountry  a INNER JOIN ZNodeCountry b ON a.CountryCode =b.Code '
				SET @OrderByDefault = 'PortalCountryID'
                SET @L_GroupByClause = ''
				SET @L_SPfilterClause =   ''

				IF ( @L_innerWhereClause IS NOT NULL OR LEN(@L_innerWhereClause) >= 1) 
				BEGIN
					SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'PortalID'+ '~~','a.PortalID')
					SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'CountryCode'+ '~~','a.CountryCode')
					SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'CountryName'+ '~~','b.Name')
				END
            END ;


         	IF @SPName = 'ZNodeGetSKUProfileEffective' 
            BEGIN
				SET @Select_Clause = 'Name,SkuProfileEffectiveID,SkuId,ProfileId,EffectiveDate'
				SET @Select_FROM_CLAUSE = 'a.Name,ISNULL(b.SkuProfileEffectiveID,0) SkuProfileEffectiveID,ISNULL(b.SkuId,0) SkuId,ISNULL(b.ProfileId,0) ProfileId,ISNULL(b.EffectiveDate,0)EffectiveDate'
				SET @FROM_CLAUSE = '  FROM ZnodeProfile  a inner JOIN  ZNodeSKUProfileEffective  b ON a.ProfileID = b.ProfileId '
				SET @OrderByDefault = 'Name'
                SET @L_GroupByClause = ''
				SET @L_SPfilterClause =   ''

				IF ( @L_innerWhereClause IS NOT NULL OR LEN(@L_innerWhereClause) >= 1) 
				BEGIN
					SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'SkuId'+ '~~','b.SkuId')
				END
            END ;

            IF @SPName = 'ZNodeGetVendorProduct' 
            BEGIN
    --        	IF (@L_inLineWhereClause IS NOT NULL OR LEN(@L_inLineWhereClause) >= 1) 
				--BEGIN
				--	SET @L_inLineWhereClause = REPLACE(@L_inLineWhereClause,'~~' + 'SKU'+ '~~','Isnull(SKU,'')')
				--	SET @L_inLineWhereClause = 'AND P.ProductID IN (SELECT ProductID FROM ZNodeSKU WHERE '+ @L_inLineWhereClause  +')'
				--END ;
                SET @Select_Clause = ' ProductID,ImageFile,Name,ProductNum,RetailPrice,SalePrice,DisplayOrder,IsActive,PortalID,Vendor, '
                SET @Select_Clause = @Select_Clause + ' QuantityOnHand,ReviewStateId,ProductStatus ,ReviewStateString,ReviewHistoryDescription'
				SET @Select_FROM_CLAUSE  =  ' Distinct P.ProductID,ImageFile,P.Name,P.ProductNum,RetailPrice,SalePrice,P.DisplayOrder,P.ActiveInd  IsActive, '
				--SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE  + ' CASE WHEN P.AccountId > 0 THEN G.PortalID ELSE  P.PortalID END PortalID, '
				--SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE  + ' P.PortalID PortalID, '
				SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE  + 'CASE WHEN P.AccountId > 0 THEN   ( Select  top 1 ZPT.PortalId from ZnodePortalCatalog ZPT,znodecategorynode ZCN, ZNODEPRODUCTCATEGORY ZPC  '
				SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE  + ' Where ZPT.CatalogId=ZCN.CatalogId and ' 
				SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE  + ' ZCN.categoryId=ZPC.CAtegoryId and ZPC.ProductID=P.ProductID) ELSE  P.PortalID END  PortalID ,'
				SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE  + ' ISNULL(A.FirstName ,'''')+ '' '' + ISNULL(A.LastName ,'''') + '' '' + ISNULL(A.CompanyName,'''')  Vendor, '
				SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE  + ' D.QuantityOnHand  ,ISNULL(P.ReviewStateId,0) ReviewStateId  ,R.ReviewStateName ProductStatus, '
                SET @Select_FROM_CLAUSE  = @Select_FROM_CLAUSE + ' Case when ISNULL(P.ReviewStateId,0) = 10  then ''<div class="img-circle symbol-circle yellow"></div>'''
				SET @Select_FROM_CLAUSE  = @Select_FROM_CLAUSE + ' when ISNULL(P.ReviewStateId,0) = 20  then ''<div class="img-circle symbol-circle green"></div>'''
				SET @Select_FROM_CLAUSE  = @Select_FROM_CLAUSE + ' when ISNULL(P.ReviewStateId,0) = 30  then ''<div class="img-circle symbol-circle red"></div>'''
				SET @Select_FROM_CLAUSE  = @Select_FROM_CLAUSE + ' when ISNULL(P.ReviewStateId,0) = 40  then ''<div class="img-circle symbol-circle grey"></div>'' else ''<div class="img-circle symbol-circle grey"></div>'' end ReviewStateString'
                SET @Select_FROM_CLAUSE  = @Select_FROM_CLAUSE + ' ,rh.Description ReviewHistoryDescription'
				SET @FROM_CLAUSE = ' FROM ZNodeProduct  P ' 
				SET @FROM_CLAUSE = @FROM_CLAUSE  + ' LEFT OUTER JOIN ZNodeProductReviewState R ON P.ReviewStateID = R.ReviewStateID '
				SET @FROM_CLAUSE = @FROM_CLAUSE  + ' LEFT OUTER JOIN ZNodeAddress A ON P.AccountID = A.AccountID  AND ISNULL(A.IsDefaultBilling ,0) = 1  '
				SET @FROM_CLAUSE = @FROM_CLAUSE  + ' LEFT OUTER JOIN ZNodeSKU C ON C.ProductID = P.ProductID '
				SET @FROM_CLAUSE = @FROM_CLAUSE  + ' and c.SKUID in (Select Top 1 X.SKUID  from ZNodeSKU X  where X.ProductID = P.ProductID )'
				SET @FROM_CLAUSE = @FROM_CLAUSE  + ' LEFT OUTER JOIN ZNodeSKUInventory  D ON C.SKU = D.SKU'
				SET @FROM_CLAUSE = @FROM_CLAUSE  + ' LEFT OUTER JOIN znodeProductReviewHistory  rh ON rh.ProductID = P.ProductID AND rh.status=''PendingApproval''  '
				SET @FROM_CLAUSE = @FROM_CLAUSE  + ' and rh.ProductReviewHistoryID in ( SELECT MAX(irh.ProductReviewHistoryID) FROM znodeProductReviewHistory irh WHERE irh.ProductID=P.ProductID ) '
				
				SET @OrderByDefault = 'ProductID'
				
				SET @L_GroupByClause = ''

                if(@L_inLineWhereClause != '')
				Begin 
					--SET @L_SPfilterClause = ' and ISNULL(A.FirstName ,'''')+ '' '' + ISNULL(A.LastName ,'''') + '' '' + ISNULL(A.CompanyName,'''') is NOT null '   + @L_inLine 
					SET @L_SPfilterClause = ' and (ISNULL(A.FirstName ,'''')+ '' '' + ISNULL(A.LastName ,'''') + '' '' + ISNULL(A.CompanyName,'''') is NOT null and  ' 
					SET @L_SPfilterClause = @L_SPfilterClause + 'CASE WHEN P.AccountId > 0 THEN   ( Select  top 1 ZPT.PortalId from ZnodePortalCatalog ZPT,znodecategorynode ZCN, ZNODEPRODUCTCATEGORY ZPC  '
					SET @L_SPfilterClause = @L_SPfilterClause + ' Where ZPT.CatalogId=ZCN.CatalogId and   ' 
					SET @L_SPfilterClause = @L_SPfilterClause + ' ZCN.categoryId=ZPC.CAtegoryId and ZPC.ProductID=P.ProductID) ELSE  P.PortalID END Is not null )' + @L_inLine 
					 --OR P.SupplierID is not null
				END 
				else 
					Begin 
					--SET @L_SPfilterClause = ' and ISNULL(A.FirstName ,'''')+ '' '' + ISNULL(A.LastName ,'''') + '' '' + ISNULL(A.CompanyName,'''') is NOT null '   
					SET @L_SPfilterClause = ' and (ISNULL(A.FirstName ,'''')+ '' '' + ISNULL(A.LastName ,'''') + '' '' + ISNULL(A.CompanyName,'''') is NOT null and  ' 
					SET @L_SPfilterClause = @L_SPfilterClause + 'CASE WHEN P.AccountId > 0 THEN   ( Select  top 1 ZPT.PortalId from ZnodePortalCatalog ZPT,znodecategorynode ZCN, ZNODEPRODUCTCATEGORY ZPC  '
					SET @L_SPfilterClause = @L_SPfilterClause + ' Where ZPT.CatalogId=ZCN.CatalogId and   ' 
					SET @L_SPfilterClause = @L_SPfilterClause + ' ZCN.categoryId=ZPC.CAtegoryId and ZPC.ProductID=P.ProductID) ELSE  P.PortalID END Is not null )' 
					--OR  P.SupplierID is not null 
				END 
                
				IF ( @L_innerWhereClause IS NOT NULL OR LEN(@L_innerWhereClause) >= 1) 
					BEGIN 
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ProductID'+ '~~','P.[ProductID]')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ProductName'+ '~~','P.[Name]')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ProductNumber'+ '~~','P.[ProductNum]')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'SKU'+ '~~','C.[SKU]')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'VendorId'+ '~~','P.AccountId')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'Vendor'+ '~~','ISNULL(A.FirstName ,'''')+ '' '' + ISNULL(A.LastName ,'''') + '' '' + ISNULL(A.CompanyName,'''')  ')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ProductStatus'+ '~~','isnull(R.ReviewStateName,'''')')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ReviewStateId'+ '~~','ISNULL(P.ReviewStateId,0)')
						--SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'PortalID'+ '~~','P.PortalID')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'PortalID'+ '~~','(CASE WHEN P.AccountId > 0 THEN   ( Select  top 1 ZPT.PortalId from ZnodePortalCatalog ZPT,znodecategorynode ZCN, ZNODEPRODUCTCATEGORY ZPC  Where ZPT.CatalogId=ZCN.CatalogId and ZCN.categoryId=ZPC.CAtegoryId and ZPC.ProductID=P.ProductID) ELSE  P.PortalID END )')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'SupplierID'+ '~~','P.SupplierID')
				    END
            END ;

           IF @SPName = 'ZNodeGetVendorProductforMallAdmin' 
            BEGIN
                SET @Select_Clause = ' ProductID,ImageFile,Name,ProductNum,RetailPrice,SalePrice,DisplayOrder,ActiveInd,PortalID,Vendor, '
                SET @Select_Clause = @Select_Clause + ' QuantityOnHand,ReviewStateId,ProductStatus ,ReviewStateString'
				SET @Select_FROM_CLAUSE  =  ' Distinct P.ProductID,ImageFile,P.Name,P.ProductNum,RetailPrice,SalePrice,P.DisplayOrder,P.ActiveInd, '
				--SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE  + ' CASE WHEN P.AccountId > 0 THEN G.PortalID ELSE  P.PortalID END PortalID, '
				--SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE  + ' P.PortalID PortalID, '
				SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE  + 'CASE WHEN P.AccountId > 0 THEN  ( Select  top 1 ZPT.PortalId from ZnodePortalCatalog ZPT,znodecategorynode ZCN, ZNODEPRODUCTCATEGORY ZPC  '
				SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE  + ' Where ZPT.CatalogId=ZCN.CatalogId and ' 
				SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE  + ' ZCN.categoryId=ZPC.CAtegoryId and ZPC.ProductID=P.ProductID) ELSE  P.PortalID END  PortalID ,'
				SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE  + ' ISNULL(A.FirstName ,'''')+ '' '' + ISNULL(A.LastName ,'''') + '' '' + ISNULL(A.CompanyName,'''')  Vendor, '
				SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE  + ' (select top 1 D.QuantityOnHand from ZNodeSKUInventory D  inner join ZNodeSKU C on D.SKU = C.SKU  where  C.ProductID = P.ProductID ) QuantityOnHand,'
				SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE  + ' ISNULL(P.ReviewStateId,0) ReviewStateId  ,R.ReviewStateName ProductStatus, '
                SET @Select_FROM_CLAUSE  = @Select_FROM_CLAUSE + ' Case when ISNULL(P.ReviewStateId,0) = 10  then ''<div class="img-circle symbol-circle yellow"></div>'''
				SET @Select_FROM_CLAUSE  = @Select_FROM_CLAUSE + ' when ISNULL(P.ReviewStateId,0) = 20  then ''<div class="img-circle symbol-circle green"></div>'''
				SET @Select_FROM_CLAUSE  = @Select_FROM_CLAUSE + ' when ISNULL(P.ReviewStateId,0) = 30  then ''<div class="img-circle symbol-circle red"></div>'''
				SET @Select_FROM_CLAUSE  = @Select_FROM_CLAUSE + ' when ISNULL(P.ReviewStateId,0) = 40  then ''<div class="img-circle symbol-circle grey"></div>'' else ''<div class="img-circle symbol-circle grey"></div>'' end ReviewStateString'
				SET @FROM_CLAUSE = ' FROM ZNodeProduct  P ' 
				SET @FROM_CLAUSE = @FROM_CLAUSE  + ' LEFT OUTER JOIN ZNodeProductReviewState R ON P.ReviewStateID = R.ReviewStateID '
				SET @FROM_CLAUSE = @FROM_CLAUSE  + ' LEFT OUTER JOIN ZNodeAddress A ON P.AccountID = A.AccountID  AND ISNULL(A.IsDefaultBilling ,0) = 1  '
			--	SET @FROM_CLAUSE = @FROM_CLAUSE  + ' LEFT OUTER JOIN ZNodeSKU C ON C.ProductID = P.ProductID '
			--	SET @FROM_CLAUSE = @FROM_CLAUSE  + ' LEFT OUTER JOIN ZNodeSKUInventory  D ON C.SKU = D.SKU'
			--	SET @FROM_CLAUSE = @FROM_CLAUSE  + ' LEFT OUTER JOIN ZNODEPRODUCTCATEGORY E ON P.ProductID = E.ProductID'
			--	SET @FROM_CLAUSE = @FROM_CLAUSE  + ' LEFT OUTER JOIN znodecategorynode F ON E.CategoryID = F.CategoryID'
			--	SET @FROM_CLAUSE = @FROM_CLAUSE  + ' LEFT OUTER JOIN ZnodePortalCatalog G ON F.CatalogID = G.CatalogID  ' 
				SET @OrderByDefault = 'ProductID'
				SET @L_GroupByClause = ''
                --SET @L_GroupByClause = 'GROUP BY a.[ProductID],a.[Name],a.[ShortDescription],a.[ProductNum],a.[ProductTypeID],'
                --SET @L_GroupByClause = @L_GroupByClause + ' a.[RetailPrice],a.[SalePrice],a.[WholesalePrice],a.[ImageFile],a.[ImageAltTag],a.[CallForPricing],'
                --SET @L_GroupByClause = @L_GroupByClause + 'a.[HomepageSpecial],a.[CategorySpecial],a.[TaxClassID],a.[PortalID],a.[ActiveInd],a.[Franchisable],i.Boost,h.NAME'
                --SET @L_SPfilterClause = '' 
                SET @L_SPfilterClause = ' and ISNULL(A.FirstName ,'''')+ '' '' + ISNULL(A.LastName ,'''') + '' '' + ISNULL(A.CompanyName,'''') is NOT null ' 
                --SET @L_SPfilterClause = @L_SPfilterClause + 'CASE WHEN P.AccountId > 0 THEN   ( Select  top 1 ZPT.PortalId from ZnodePortalCatalog ZPT,znodecategorynode ZCN, ZNODEPRODUCTCATEGORY ZPC  '
				--SET @L_SPfilterClause = @L_SPfilterClause + ' Where ZPT.CatalogId=ZCN.CatalogId and   ' 
				--SET @L_SPfilterClause = @L_SPfilterClause + ' ZCN.categoryId=ZPC.CAtegoryId and ZPC.ProductID=P.ProductID) ELSE  P.PortalID END Is not null '
                IF ( @L_innerWhereClause IS NOT NULL OR LEN(@L_innerWhereClause) >= 1) 
					BEGIN 
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ProductID'+ '~~','P.[ProductID]')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ProductName'+ '~~','P.[Name]')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ProductNumber'+ '~~','P.[ProductNum]')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'SKU'+ '~~','C.[SKU]')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'VendorId'+ '~~','P.AccountId')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'Vendor'+ '~~','ISNULL(A.FirstName ,'''')+ '' '' + ISNULL(A.LastName ,'''') + '' '' + ISNULL(A.CompanyName,'''')  ')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ProductStatus'+ '~~','isnull(R.ReviewStateName,'''')')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ReviewStateId'+ '~~','ISNULL(P.ReviewStateId,0)')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'PortalID'+ '~~','P.PortalID')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'quantityonhand'+ '~~','(select top 1 D.QuantityOnHand from ZNodeSKUInventory D  inner join ZNodeSKU C on D.SKU = C.SKU  where  C.ProductID = P.ProductID )')
				    END
            END ;

		 IF @SPName = 'ZNodeGetOrderDetails' 
            BEGIN
            	IF (@L_inLineWhereClause IS NOT NULL OR LEN(@L_inLineWhereClause) >= 1) 
				BEGIN
					SET @L_inLineWhereClause = REPLACE(@L_inLineWhereClause,'~~' + 'UserName'+ '~~','Isnull(UserName,'''')')
					if(@L_inLineWhereClause != '')
					Begin
						SET @L_inLine  = ' and (isnull(a.PortalID  ,0) in '
						SET @L_inLine = @L_inLine + ' (select a.PortalId from webpages_profile a inner join WebPages_users b  on a.UserId  = b.UserId where  '+ @L_inLineWhereClause +' ) '
						SET @L_inLine = @L_inLine + ' OR (a.PortalID  =a.PortalID  and '
						SET @L_inLine = @L_inLine + ' (select top 1  a.PortalId from webpages_profile a inner join WebPages_users b  on a.UserId  = b.UserId where  '+ @L_inLineWhereClause+' ) <= 0'
						SET @L_inLine = @L_inLine + ' ) )'
					END
				END 

                SET @Select_Clause = ' OrderID,PortalId,AccountID,OrderStateID,ShippingID,PaymentTypeId,PaymentStatusID,OrderStatus , '
                SET @Select_Clause = @Select_Clause + ' ShippingTypeName,PaymentTypeName,PaymentStatusName,BillingFirstName ,BillingLastName,'
                SET @Select_Clause = @Select_Clause + ' Total,OrderDate,ShipDate,TrackingNumber,PaymentSettingId ,StoreName ,BillingCompanyName,FullName,CardTransactionId'
				SET @Select_FROM_CLAUSE  = ' Distinct a.[OrderID],a.[PortalId],a.[AccountID],a.[OrderStateID],a.[ShippingID],a.[PaymentTypeId],a.[PaymentStatusID],b.OrderStateName OrderStatus '
				SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE  + ' ,c.Description ShippingTypeName,isnull(d.Name ,'''') PaymentTypeName,isnull(e.PaymentStatusName ,'''') PaymentStatusName,[BillingFirstName] ,[BillingLastName]'
		        SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE  + ' ,[Total],[OrderDate],[ShipDate],[TrackingNumber],Isnull([PaymentSettingId],0)  PaymentSettingId, isnull(f.StoreName,'''') StoreName,a.BillingCompanyName,'
				SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE  + ' Isnull(BillingFirstName ,'''') + '' '' + Isnull(BillingLastName,'''') FullName,Isnull(CardTransactionId,0) CardTransactionId '
				SET @FROM_CLAUSE =  ' FROM ZNODEORDER a INNER JOIN ZNodeOrderState  b ON a.[OrderStateID] =  b.OrderStateID'
				SET @FROM_CLAUSE = @FROM_CLAUSE  + ' Left OUTER JOIN ZNodeShipping c ON a.ShippingID = c.ShippingID '
				SET @FROM_CLAUSE = @FROM_CLAUSE  + ' Left OUTER JOIN ZNodePaymentType d ON a.PaymentTypeId = d.PaymentTypeID'
				SET @FROM_CLAUSE = @FROM_CLAUSE  + ' Left OUTER JOIN ZNodePaymentStatus e ON a.PaymentStatusID = e.PaymentStatusID'
				SET @FROM_CLAUSE = @FROM_CLAUSE  + ' Left OUTER JOIN ZNodePortal f ON a.PortalID = f.PortalID'
				SET @OrderByDefault = ' OrderId desc '
				SET @L_GroupByClause = ''
                
				if @L_inLineWhereClause != '' 
					SET @L_SPfilterClause = @L_inLine  
				Else 
					SET @L_SPfilterClause = ''

                IF ( @L_innerWhereClause IS NOT NULL OR LEN(@L_innerWhereClause) >= 1) 
					BEGIN 
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'BillingFirstName'+ '~~','a.[BillingFirstName]')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'BillingLastName'+ '~~','a.[BillingLastName]')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'BillingCompanyName'+ '~~','a.[BillingCompanyName]')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'AccountID'+ '~~','a.[AccountID]')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'OrderId'+ '~~','a.OrderId ')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'PortalID'+ '~~','a.PortalID')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'OrderStateID'+ '~~','a.OrderStateID')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'OrderDate'+ '~~','convert(date,a.OrderDate)')
						--PRFT Custom Code : Start
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ExternalID'+ '~~','a.ExternalID')        
						--PRFT Custom Code : End
				    END
            END ;

			IF @SPName = 'ZNodeGetcustReview' 
            BEGIN
                SET @Select_Clause = ' ReviewID,ProductID,AccountID,Subject,Pros,Cons,Comments,CreateUser,UserLocation,Rating,Status,CreateDate,Custom1,Custom2,Custom3 ,ProductName '
           		SET @Select_FROM_CLAUSE  = 'a.ReviewID,a.ProductID,ISNULL(a.AccountID,0)AccountID,ISNULL(a.Subject,'''') Subject,ISNULL(a.Pros,'''')Pros, isnull(a.Cons,'''') Cons '
            	SET @Select_FROM_CLAUSE  = @Select_FROM_CLAUSE + ',a.Comments,a.CreateUser,a.UserLocation,a.Rating,a.Status,a.CreateDate,a.Custom1,a.Custom2,a.Custom3, b.NAME ProductName'
				SET @FROM_CLAUSE =  ' FROM dbo.ZNodeReview a INNER JOIN dbo.ZNodeProduct b ON a.productid = b.productid'
				SET @OrderByDefault = 'ProductName'

				SET @L_GroupByClause = ''
                SET @L_SPfilterClause = ''
                IF ( @L_innerWhereClause IS NOT NULL OR LEN(@L_innerWhereClause) >= 1) 
					BEGIN 
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ProductID'+ '~~','a.ProductID')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ProductName'+ '~~','b.[Name]')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'Status'+ '~~','a.[Status]')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'Subject'+ '~~','a.[Subject]')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'CreateUser'+ '~~','a.CreateUser ')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'CreateUser'+ '~~','a.CreateUser ')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'PortalID'+ '~~','b.PortalID ')
						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'AccountID'+ '~~','a.AccountID ')
				    END
            END ;


			IF @SPName = 'ZNodeGetImageType' 
			BEGIN



                SET @Select_Clause = ' ProductImageID,ProductID,Name,ImageFile,ImageAltTag,AlternateThumbnailImageFile,ActiveInd,ShowOnCategoryPage, '



            	SET @Select_Clause = @Select_Clause +  'ProductImageTypeID,DisplayOrder,ReviewStateID, ImageTypeName,AccountID,ReviewStateString'



   	



        	SET @Select_FROM_CLAUSE  = ' DISTINCT I.ProductImageID,I.ProductID,I.Name,I.ImageFile,I.ImageAltTag,I.AlternateThumbnailImageFile,I.ActiveInd,I.ShowOnCategoryPage, '



            	SET @Select_FROM_CLAUSE  = @Select_FROM_CLAUSE + 'I.ProductImageTypeID,I.DisplayOrder,I.ReviewStateID,T.Name ImageTypeName,ISNULL(P.AccountID, 0) AccountID,  '



            	SET @Select_FROM_CLAUSE  = @Select_FROM_CLAUSE + 'Case when I.ReviewStateID = 10  then ''<div class="img-circle symbol-circle yellow"></div>'''



				SET @Select_FROM_CLAUSE  = @Select_FROM_CLAUSE + ' when I.ReviewStateID = 20  then ''<div class="img-circle symbol-circle green"></div>'''



				SET @Select_FROM_CLAUSE  = @Select_FROM_CLAUSE + ' when I.ReviewStateID = 30  then ''<div class="img-circle symbol-circle red"></div>'''



				SET @Select_FROM_CLAUSE  = @Select_FROM_CLAUSE + ' when I.ReviewStateID = 40  then ''<div class="img-circle symbol-circle grey"></div>'' else ''<div class="img-circle symbol-circle grey"></div>'' end ReviewStateString'







				



				SET @FROM_CLAUSE =  ' FROM ZNodeProductImage I LEFT OUTER JOIN  ZNodeProductImageType T ON I.ProductImageTypeID=T.ProductImageTypeID '



				SET @FROM_CLAUSE =  @FROM_CLAUSE  + '  LEFT OUTER JOIN ZNodeProduct P ON P.ProductID=I.ProductID  LEFT OUTER JOIN ZNodeProductReviewState R ON I.ReviewStateID=R.ReviewStateID'



				



				SET @FROM_CLAUSE =  @FROM_CLAUSE  + ' LEFT OUTER JOIN ZNodeAddress Q ON  P.AccountId  = Q.Accountid  '



				SET @FROM_CLAUSE =  @FROM_CLAUSE  + ' Left OUTER  JOIN ZNodeSKU S ON  I.ProductID = S.ProductID '



				SET @FROM_CLAUSE =  @FROM_CLAUSE  + ' Left OUTER  JOIN ZNodeAccount M ON  P.AccountId  = M.AccountId '



				



				



				SET @OrderByDefault = 'Name'



				SET @L_GroupByClause = ''



	            



                SET @L_SPfilterClause =   ' And I.ReviewStateId IS NOT NULL'



                



                IF ( @L_innerWhereClause IS NOT NULL OR LEN(@L_innerWhereClause) >= 1) 



					BEGIN 



					



						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ProductID'+ '~~','p.ProductID')



						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ProductName'+ '~~','P.Name')



						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ProductNum'+ '~~','P.ProductNum')



						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ProductStatus'+ '~~','I.ReviewStateId')



						--SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'VendorId'+ '~~','P.AccountId')



						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'SKU'+ '~~','S.SKU')



						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'Vendor'+ '~~',' isnull(Q.FirstName,'''') + '' ''+  isnull(Q.LastName,'''') ') 



						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'VendorId'+ '~~','M.ExternalAccountNo')



				    END



  



            END ;



    



		--IF @SPName = 'ZNodeGetImageType' 



  --          BEGIN



  --              SET @Select_Clause = ' ProductImageID,ProductID,Name,ImageFile,ImageAltTag,AlternateThumbnailImageFile,ActiveInd,ShowOnCategoryPage, '



  --          	SET @Select_Clause = @Select_Clause +  'ProductImageTypeID,DisplayOrder,ReviewStateID,   ImageTypeName,AccountID'



            	



  --          	SET @Select_FROM_CLAUSE  = ' DISTINCT I.ProductImageID,I.ProductID,I.Name,I.ImageFile,I.ImageAltTag,I.AlternateThumbnailImageFile,I.ActiveInd,I.ShowOnCategoryPage, '



  --          	SET @Select_FROM_CLAUSE  = @Select_FROM_CLAUSE + 'I.ProductImageTypeID,I.DisplayOrder,I.ReviewStateID,T.Name ImageTypeName,ISNULL(P.AccountID, 0) AccountID'



				



		--		SET @FROM_CLAUSE =  ' FROM ZNodeProductImage I LEFT OUTER JOIN  ZNodeProductImageType T ON I.ProductImageTypeID=T.ProductImageTypeID '



		--		SET @FROM_CLAUSE =  @FROM_CLAUSE + '  LEFT OUTER JOIN ZNodeProduct P ON P.ProductID=I.ProductID  LEFT OUTER JOIN ZNodeProductReviewState R ON I.ReviewStateID=R.ReviewStateID'



				



		--		SET @FROM_CLAUSE =  @FROM_CLAUSE  + ' LEFT OUTER JOIN ZNodeAddress Q ON  P.AccountId  = Q.Accountid  '



		--		SET @FROM_CLAUSE =  @FROM_CLAUSE  + ' Left OUTER  JOIN ZNodeSKU S ON  I.ProductID = S.ProductID '



		--		SET @FROM_CLAUSE =  @FROM_CLAUSE  + ' Left OUTER  JOIN ZNodeAccount M ON  P.AccountId  = M.AccountId '



				



				



		--		SET @OrderByDefault = 'Name'



		--		SET @L_GroupByClause = ''



	            



  --              SET @L_SPfilterClause =   ' And I.ReviewStateId IS NOT NULL'



                



  --              IF ( @L_innerWhereClause IS NOT NULL OR LEN(@L_innerWhereClause) >= 1) 



		--			BEGIN 



					



		--				SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ProductID'+ '~~','p.ProductID')



		--				SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ProductName'+ '~~','P.Name')



		--				SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ProductNum'+ '~~','P.ProductNum')



		--				SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ProductStatus'+ '~~','I.ReviewStateId')



		--				--SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'VendorId'+ '~~','P.AccountId')



		--				SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'SKU'+ '~~','S.SKU')



		--				SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ ' Vendor'+ '~~','inull(Q.FirstName,'''') + '' ''+  inull(Q.LastName,'''') ') 



		--				SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'VendorId'+ '~~','M.ExternalAccountNo')



		--		    END



  



  --          END ;



    



		IF @SPName = 'ZNodeGetCrossSellProductType_5' 



            BEGIN



            



				IF (@L_inLineWhereClause IS NOT NULL OR LEN(@L_inLineWhereClause) >= 1) 



				BEGIN



				



					DECLARE @InlineQuery VARCHAR(max) 



					



					SET @L_inLineWhereClause = REPLACE(@L_inLineWhereClause,'~~' + 'PortalId'+ '~~','Isnull(d.PortalId,0)')



					SET @L_inLineWhereClause = REPLACE(@L_inLineWhereClause,'~~' + 'CategoryID'+ '~~','Isnull(b.CategoryID,0)')



					



					SET @InlineQuery = ' and A.ProductID  IN (  SELECT DISTINCT ProductID FROM ZNodeProductCategory  a INNER JOIN ZNodeCategory b ON a.CategoryID = b.CategoryID  '



					SET @InlineQuery  = @InlineQuery + ' INNER JOIN ZNodeCategoryNode  c ON b.CategoryID = c.CategoryID '



					SET @InlineQuery = @InlineQuery + '  INNER join ZNodePortalCatalog d ON c.CatalogID = d.CatalogID  WHERE ' + @L_inLineWhereClause  +')'



					SET @L_inLineWhereClause   =@InlineQuery 



				END ;



				



   				SET @Select_Clause = 'ProductID,NAME, CrossSellProdName'



   				



   				



				--SET @Select_FROM_CLAUSE  = 'Distinct A.ProductID,A.NAME,(SELECT  CAST(U.Name AS NVARCHAR) + '','' FROM    ZNodeProduct U WHERE   U.ProductID  IN ( SELECT RelatedProductId   '



				--SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE  + ' FROM ZNodeProductCrossSell WHERE ProductID= A.ProductId  AND RelationTypeId = 5   ) Order by DisplayOrder  FOR XML PATH('''')) CrossSellProdName'



				--SET @FROM_CLAUSE =   '  FROM    dbo.ZNodeProduct A '



				--SET @FROM_CLAUSE =  @FROM_CLAUSE  + '  LEFT OUTER JOIN ZNodeSKU D ON A.ProductId= D.ProductId'



				



				



				SET @Select_FROM_CLAUSE  = 'Distinct A.ProductID,A.NAME,(SELECT  CAST(U.Name AS NVARCHAR) + '','' FROM    ZNodeProduct U  inner join'



				SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE  + ' ZNodeProductCrossSell ZPC on U.ProductID  = ZPC.RelatedProductId  and ZPC.ProductID= A.ProductId  '



				SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE  + 'AND ZPC.RelationTypeId = 5  Order by ZPC.DisplayOrder    FOR XML PATH('''')) CrossSellProdName '



				SET @FROM_CLAUSE =   '  FROM    dbo.ZNodeProduct A '



				SET @FROM_CLAUSE =  @FROM_CLAUSE  + '  LEFT OUTER JOIN ZNodeSKU D ON A.ProductId= D.ProductId'



				



				SET @OrderByDefault = 'Name'



				SET @L_GroupByClause = ''



	            



                SET @L_SPfilterClause =  @L_inLineWhereClause



                



                IF ( @L_innerWhereClause IS NOT NULL OR LEN(@L_innerWhereClause) >= 1) 



					BEGIN 



						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ProductID'+ '~~','A.ProductID')



						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ProductName'+ '~~','A.Name')



						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ProductNum'+ '~~','A.ProductNum')



						--SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'CategoryName'+ '~~','C.Name')



						--SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'PortalID'+ '~~','C.PortalID')



						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'SKU'+ '~~','D.SKU')



						



				    END



            END ;



			IF @SPName = 'ZNodeGetFrequentlyBoughtProduct' 



            BEGIN



            



            	IF (@L_inLineWhereClause IS NOT NULL OR LEN(@L_inLineWhereClause) >= 1) 



				BEGIN



				



					DECLARE @InlineQuery1 VARCHAR(max) 



					



					SET @L_inLineWhereClause = REPLACE(@L_inLineWhereClause,'~~' + 'PortalId'+ '~~','Isnull(d.PortalId,0)')



					SET @L_inLineWhereClause = REPLACE(@L_inLineWhereClause,'~~' + 'CategoryID'+ '~~','Isnull(b.CategoryID,0)')



					



					SET @InlineQuery1 = ' and A.ProductID  IN (  SELECT DISTINCT ProductID FROM ZNodeProductCategory  a INNER JOIN ZNodeCategory b ON a.CategoryID = b.CategoryID  '



					SET @InlineQuery1  = @InlineQuery1 + ' INNER JOIN ZNodeCategoryNode  c ON b.CategoryID = c.CategoryID '



					SET @InlineQuery1 = @InlineQuery1 + '  INNER join ZNodePortalCatalog d ON c.CatalogID = d.CatalogID  WHERE ' + @L_inLineWhereClause  +')'



					SET @L_inLineWhereClause   =@InlineQuery1 



				END ;



				



   				



   				SET @Select_Clause = 'ProductID,NAME, CrossSellProdName1,CrossSellProdName2'



				SET @Select_FROM_CLAUSE  = 'DISTINCT  A.ProductID,A.NAME,



				(SELECT   U.NAME FROM    ZNodeProduct U WHERE   U.ProductID  = ( SELECT TOP 1 Sell.RelatedProductId   '



				SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE  + ' FROM ZNodeProductCrossSell Sell



				WHERE Sell.ProductID= A.ProductId  AND Sell.RelationTypeId = 4  ORDER BY Sell.DisplayOrder , Sell.RelatedProductId ASC ) ) CrossSellProdName1,'



				SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE  + ' case WHEN count(b.ProductCrossSellTypeId) >1 then 



				(SELECT   U.NAME FROM    ZNodeProduct U WHERE   U.ProductID  = ( SELECT TOP 1 Sell1.RelatedProductId    ' 



				SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE  + ' FROM ZNodeProductCrossSell Sell1



				                                                   WHERE Sell1.ProductID= A.ProductId  AND Sell1.RelationTypeId = 4  



				                                                   ORDER BY Sell1.DisplayOrder , Sell1.RelatedProductId desc) ) else NULL END as CrossSellProdName2 '



				SET @FROM_CLAUSE  =  ' FROM    dbo.ZNodeProduct A '



				SET @FROM_CLAUSE =  @FROM_CLAUSE  + '  LEFT OUTER JOIN dbo.ZNodeProductCrossSell B ON b.ProductID= A.ProductId  AND b.RelationTypeId = 4  ' 



				SET @FROM_CLAUSE =  @FROM_CLAUSE  + '  LEFT OUTER JOIN ZNodeSKU D ON A.ProductId= D.ProductId '







				



				SET @OrderByDefault = ' Name '



				SET @L_GroupByClause = ' Group by  A.ProductID,A.NAME'



	            



               SET @L_SPfilterClause =    @L_inLineWhereClause 



                



                IF ( @L_innerWhereClause IS NOT NULL OR LEN(@L_innerWhereClause) >= 1) 



					BEGIN 



						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ProductID'+ '~~','A.ProductID')



						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ProductName'+ '~~','A.Name')



						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ProductNum'+ '~~','A.ProductNum')



						--SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'CategoryName'+ '~~','C.Name')



						--SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'PortalID'+ '~~','F.PortalID')



						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'SKU'+ '~~','D.SKU')



						



				    END



    END ;



            



          	IF @SPName = 'ZNodeGetRejectionMessages' 



            BEGIN



					



				SET @Select_Clause = 'RejectionMessagesId,MessageKey,MessageValue,PortalId,LocaleId,StoreName'



				SET @Select_FROM_CLAUSE = 'a.RejectionMessagesId,a.MessageKey,a.MessageValue,a.PortalId,a.LocaleId,b.StoreName'



				SET @FROM_CLAUSE = '  FROM [ZNodeRejectionMessages] a '



				SET @FROM_CLAUSE = @FROM_CLAUSE  + ' Left Outer Join ZNodePortal b on a.PortalId=b.PortalId'



				



				SET @OrderByDefault = 'RejectionMessagesId'



                SET @L_GroupByClause = ''



				SET @L_SPfilterClause =   ''



				if(@L_inLineWhereClause != '')



					SET @L_SPfilterClause =  @L_inLine



				else 



					SET @L_SPfilterClause =  ''



				IF ( @L_innerWhereClause IS NOT NULL OR LEN(@L_innerWhereClause) >= 1) 



				BEGIN



					SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'PortalId'+ '~~','a.PortalId')



					SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'LocaleId'+ '~~','a.LocaleId')



					SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'MessageKey'+ '~~','a.MessageKey')



					SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'MessageValue'+ '~~','a.MessageValue')



					SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'RejectionMessagesId'+ '~~','a.RejectionMessagesId')



					



				END



            END ;



        



            



          	IF @SPName = 'ZNodeSearchCase' 



            BEGIN

            	SET @Select_Clause = 'CaseRequestId,StoreName,AccountID,OwnerAccountID,CaseStatusName,CasePriorityName,CaseTypeID,CaseOrigin,Title,'



            	SET @Select_Clause = @Select_Clause  + 'Description,FirstName,LastName,CompanyName,EmailID,PhoneNumber,CreateDate,CreateUser,PortalID,CaseStatusID'

				SET @Select_FROM_CLAUSE = ' R.[CaseID] CaseRequestId ,P.[StoreName],R.[AccountID],R.[OwnerAccountID],S.[CaseStatusNme] CaseStatusName ,Py.[CasePriorityNme] CasePriorityName ,R.[CaseTypeID],R.[CaseOrigin],R.[Title],' 



				SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE + ' R.[Description],R.[FirstName],R.[LastName],R.[CompanyName],R.[EmailID],R.[PhoneNumber],R.[CreateDte] CreateDate,R.[CreateUser],R.PortalID,R.CaseStatusID' 



				SET @FROM_CLAUSE = ' FROM ZNodeCaseRequest  R  INNER JOIN  ZNodePortal P  ON R.PortalID=P.PortalID  ' 



				SET @FROM_CLAUSE = @FROM_CLAUSE + ' INNER JOIN ZNodeCasePriority Py  on R.CasePriorityID=Py.CasePriorityID   '



				SET @FROM_CLAUSE = @FROM_CLAUSE + ' INNER JOIN  ZNodeCaseStatus S  on R.CaseStatusID=S.CaseStatusID  '

        		SET @OrderByDefault = 'StoreName'



				SET @L_GroupByClause = ''



				SET @L_SPfilterClause =   ''



				if(@L_inLineWhereClause != '')



					SET @L_SPfilterClause =  @L_inLine



				else 



					SET @L_SPfilterClause =  ''



				IF ( @L_innerWhereClause IS NOT NULL OR LEN(@L_innerWhereClause) >= 1) 



				BEGIN



					SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'PortalID'+ '~~','R.PortalID')



					SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'TITLE'+ '~~','R.[Title]')



					SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'FIRSTNAME'+ '~~','R.[FirstName]')



					SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'LASTNAME'+ '~~','R.[LastName]')



					SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'CASEID'+ '~~','R.[CaseID]')



					SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'CompanyName'+ '~~','R.CompanyName')



					SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'CaseStatusID'+ '~~','R.CaseStatusID')

				END



            END ;


            IF @SPName = 'ZNodeSearchRMA' 



            BEGIN



            	SET @Select_Clause = 'RMARequestID,RequestNumber,OrderID,PortalId,RequestStatusID,RequestStatus,BillingFirstName,BillingLastName,RequestDate,'



            	SET @Select_Clause = @Select_Clause  + 'Total,TaxCost,Subtotal,Discount,StoreName,CustomerName'

				SET @Select_FROM_CLAUSE = ' Distinct RMARequest.RMARequestID,RequestNumber,ZnodeOrder.OrderID,ZnodeOrder.[PortalId] ,[RequestStatusID] '



				SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE + ' ,(SELECT Name FROM ZNodeRequestStatus WHERE RequestStatusID = RMARequest.RequestStatusID) AS RequestStatus '



				SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE + ' ,ZnodeOrder.[BillingFirstName] ,ZnodeOrder.BillingLastName,[RequestDate],ISNULL(RMARequest.Total ,0) Total ' 



				SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE + ' ,ISNULL(RMARequest.TaxCost ,0) TaxCost,ISNULL(RMARequest.Subtotal ,0) Subtotal,ISNULL(RMARequest.Discount ,0) Discount,ZNodePortal.StoreName ,' 



				SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE + ' Isnull(ZnodeOrder.[BillingFirstName],'''') + '' '' + Isnull(ZnodeOrder.BillingLastName,'''') CustomerName  ' 

				SET @FROM_CLAUSE = ' FROM ZNodeRMARequest  RMARequest Inner join ZNodeRMARequestItem RMARequestItem ON RMARequest.RMARequestID = RMARequestItem.RMARequestID  '



				SET @FROM_CLAUSE = @FROM_CLAUSE + ' inner join ZNodeOrderLineItem    OrderLineItem    ON RMARequestItem.OrderLineItemId = OrderLineItem.OrderLineItemId  '



				SET @FROM_CLAUSE = @FROM_CLAUSE + ' Inner join ZNodeOrder ZnodeOrder    ON OrderLineItem.OrderID = ZnodeOrder.OrderID  '



				SET @FROM_CLAUSE = @FROM_CLAUSE + ' Inner join ZNodePortal ZNodePortal  ON ZnodeOrder.[PortalId] = ZNodePortal.PortalId  '

				--SET @FROM_CLAUSE = @FROM_CLAUSE + ' Inner JOIN WEBPAGES_Profile WP on ZNodePortal.PortalId  = WP.PortalId ' 



				--SET @FROM_CLAUSE = @FROM_CLAUSE + ' Inner JOIN WEBPAGES_Users  WU on WP.UserId = WU.UserId '

        		SET @OrderByDefault = ' RMARequestID desc'



				SET @L_GroupByClause = ''



				SET @L_SPfilterClause =   ''



				if @L_inLineWhereClause != '' 



					SET @L_SPfilterClause = @L_inLine  



				Else 



					SET @L_SPfilterClause = ''

				IF ( @L_innerWhereClause IS NOT NULL OR LEN(@L_innerWhereClause) >= 1) 



				BEGIN



					SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'PortalID'+ '~~','ZnodeOrder.PortalID')



					SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'TITLE'+ '~~','R.[Title]')



					SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'BillingFirstName'+ '~~','ZnodeOrder.[BillingFirstName]')



					SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'BillingLastName'+ '~~','ZnodeOrder.[BillingLastName]')



					SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ORDERID'+ '~~','ZnodeOrder.[ORDERID]')



					SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'RequestStatusID'+ '~~','RMARequest.[RequestStatusID]')



					SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'RMARequestID'+ '~~','RMARequest.[RMARequestID]')



					SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'RequestDate'+ '~~','convert(date,RequestDate)')



					SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'StoreName'+ '~~',' ZNodePortal.StoreName ')



					--SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'UserName'+ '~~',' WU.UserName ')

				END


		--		IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID('ZNodeSearchRMA'))



		--		DROP TABLE ZNodeSearchRMA



		--		Create table ZNodeSearchRMA (RMARequestID int ,RequestNumber nVarchar(200),OrderID int ,PortalId int ,RequestStatusID int ,RequestStatus nVarchar(200) ,BillingFirstName nVarchar(200) ,BillingLastName nVarchar(200),RequestDate Datetime,



		--								    Total money ,TaxCost money  ,Subtotal money  ,Discount money  ,StoreName nVarchar(200)  ,CustomerName nVarchar(200) ) 



		--		IF ( @L_innerWhereClause IS NOT NULL OR LEN(@L_innerWhereClause) >= 1) 

		--		BEGIN

		--			SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'RMARequestID' + '~~',' @RMARequestID')



		--			SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'ORDERID' + '~~',' @@ORDERID')



		--			SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'BILLINGFIRSTNAME' + '~~',' @BILLINGFIRSTNAME')



		--			SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'BILLINGLASTNAME' + '~~',' @@BILLINGLASTNAME')



		--			SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'STARTDATE ' + '~~',' @STARTDATE ')



		--			SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'ENDDATE' + '~~',' @ENDDATE')



		--			SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'REQUESTSTATUSID' + '~~',' @REQUESTSTATUSID')



		--			SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'ORDERSTATEID' + '~~','@ORDERSTATEID')



		--			SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + '@PORTALID' + '~~',' @PORTALID')



		--			SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + '@portalIds' + '~~',' @portalIds')



		--			--SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'LoggedAccountId' + '~~',' @LoggedAccountId')
		--			SET @L_innerWhereClause = REPLACE(REPLACE(REPLACE(@L_innerWhereClause,'and',','),'>',''),'<','')



		--		END ;
		--		DEclare @SSqlRMA Varchar(2000)



		--		set @SSqlRMA = ' [ZNode_SearchRMA] '+ @L_innerWhereClause  
		--		insert into ZNodeSearchRMA(	RMARequestID  ,RequestNumber ,OrderID  ,PortalId  ,RequestStatusID  ,RequestStatus  ,BillingFirstName  ,BillingLastName ,RequestDate ,



		--								    Total ,TaxCost ,Subtotal ,Discount ,StoreName   ,CustomerName  ) 



		--		 Exec (@SSqlRMA)


		--	SET @Select_Clause = ' RMARequestID  ,RequestNumber ,OrderID  ,PortalId  ,RequestStatusID  ,RequestStatus  ,BillingFirstName  ,BillingLastName ,RequestDate ,'



		--	SET @Select_Clause = @Select_Clause  + ' Total ,TaxCost ,Subtotal ,Discount ,StoreName   ,CustomerName '

		--	SET @Select_FROM_CLAUSE = ' RMARequestID  ,RequestNumber ,OrderID  ,PortalId  ,RequestStatusID  ,RequestStatus  ,BillingFirstName  ,BillingLastName ,RequestDate ,'



		--	SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE + ' Total ,TaxCost ,Subtotal ,Discount ,StoreName   ,CustomerName '										

		--	SET @FROM_CLAUSE = ' FROM  ZNodeSearchRMA ' 



		--	SET @OrderByDefault =  ' RMARequestID '



		--	SET @L_GroupByClause = ''



		--	SET @L_SPfilterClause = ''



                



  --          SET @L_innerWhereClause = ''



  END ;


      IF @SPName = 'ZNodeGetSKUFacetGroup' 



            BEGIN



				SET @Select_Clause = 'CatalogID,FacetGroupID,FacetGroupLabel,FacetName'



                SET @Select_FROM_CLAUSE = '  c.CatalogID,c.FacetGroupID,c.FacetGroupLabel, ( SELECT SUBSTRING(FacetName, 1, LEN(FacetName) - 1) FacetName '

				SET @FROM_CLAUSE =  ' FROM ( SELECT DISTINCT STUFF(( SELECT CAST(x.FacetName AS NVARCHAR) + '','' FROM ZNodeFacet X '



				SET @FROM_CLAUSE = @FROM_CLAUSE  + ' INNER JOIN ZNodeFacetProductSKU ASKU ON ASKU1.SKUID = ASKU.SKUID AND x.FacetID = ASKU.FacetID ' 



				SET @FROM_CLAUSE = @FROM_CLAUSE  + '  WHERE x.FacetGroupID = c.FacetGroupID FOR XML PATH('''')), 1, 0, '''') FacetName ) Tbl) FacetName'



				SET @FROM_CLAUSE = @FROM_CLAUSE  + ' FROM ZNodeProductCategory a INNER JOIN ZNodeFacetGroupCategory b ON a.CategoryID = b.CategoryID '



				SET @FROM_CLAUSE = @FROM_CLAUSE  + ' INNER JOIN ZNodeFacetGroup c ON b.FacetGroupID = c.FacetGroupID '



				SET @FROM_CLAUSE = @FROM_CLAUSE  + ' INNER JOIN ZNodeFacet d ON b.FacetGroupID = D.FacetGroupID '



				SET @FROM_CLAUSE = @FROM_CLAUSE  + ' INNER JOIN ZNodeFacetProductSKU ASKU1 on  d.FacetID = ASKU1.FacetID '

                SET @OrderByDefault = 'FacetGroupLabel'


                SET @L_GroupByClause = 'GROUP BY   ASKU1.SKUID ,c.CatalogID ,c.FacetGroupID ,c.FacetGroupLabel'



				SET @L_SPfilterClause =   '  AND ( SELECT SUBSTRING(FacetName, 1, LEN(FacetName) - 1) FacetName '



				SET @L_SPfilterClause =  @L_SPfilterClause + '  FROM ( SELECT DISTINCT STUFF(( SELECT CAST(x.FacetName AS NVARCHAR) + '','' FROM ZNodeFacet X'



				SET @L_SPfilterClause =  @L_SPfilterClause + '  INNER JOIN ZNodeFacetProductSKU ASKU ON ASKU1.SKUID = ASKU.SKUID  AND x.FacetID = ASKU.FacetID'



				SET @L_SPfilterClause =  @L_SPfilterClause + '  WHERE x.FacetGroupID = c.FacetGroupID '



				SET @L_SPfilterClause =  @L_SPfilterClause + '  FOR XML PATH('''')), 1, 0, '''') FacetName ) Tbl)  IS NOT NULL '


		    --PC.ProductID = @PRODUCTID AND ( ZP.PortalId = @PORTALID OR @PORTALID IS NULL)  



			IF ( @L_innerWhereClause IS NOT NULL  OR LEN(@L_innerWhereClause) >= 1) 



                   BEGIN



                        SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ProductID'+ '~~','a.ProductID')



						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'SKUID'+ '~~','ASKU1.SKUID')



                    END ;



            END ;

            IF @SPName = 'ZNodeGetProductWithProductBoost' 



            BEGIN



                SET @Select_Clause = 'ProductID,Name,ShortDescription,ProductNum,ProductTypeID, RetailPrice,SalePrice,WholesalePrice,ImageFile,ImageAltTag,CallForPricing,'



                SET @Select_Clause = @Select_Clause + 'HomepageSpecial,CategorySpecial,TaxClassID,PortalID,ActiveInd,Franchisable,Boost,ParentProduct'



                SET @Select_FROM_CLAUSE = ' Distinct a.[ProductID],a.[Name],a.[ShortDescription],a.[ProductNum],a.[ProductTypeID], a.[RetailPrice],a.[SalePrice]'



                SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE  + ',a.[WholesalePrice],a.[ImageFile],a.[ImageAltTag],a.[CallForPricing],a.[HomepageSpecial],'



                SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE  + ' a.[CategorySpecial],a.[TaxClassID],a.[PortalID],a.[ActiveInd],a.[Franchisable],i.Boost, '



				SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE  + ' CONVERT(BIT, ISNULL(( SELECT TOP 1 1 FROM   ZNodeParentChildProduct WHERE  ParentProductID = a.ProductID), 0) ) ParentProduct '

                SET @FROM_CLAUSE = ' FROM dbo.ZNodeProduct a LEFT OUTER JOIN dbo.ZNodeSKU b ON a.ProductID = b.ProductID LEFT OUTER JOIN   dbo.ZNodeManufacturer c  ON a.ManufacturerID = c.ManufacturerID'



				SET @FROM_CLAUSE = @FROM_CLAUSE + ' LEFT OUTER JOIN  dbo.ZNodeProductCategory d ON a.ProductID = d.ProductID  LEFT OUTER JOIN   dbo.ZNodeCategory e ON d.CategoryID = e.CategoryID '



				SET @FROM_CLAUSE = @FROM_CLAUSE  + ' LEFT OUTER JOIN  dbo.ZNodeCategoryNode f ON d.CategoryID = f.CategoryID '



				SET @FROM_CLAUSE = @FROM_CLAUSE  + ' LEFT OUTER JOIN  dbo.ZnodePortalCatalog g ON f.CatalogID = g.CatalogID '



				SET @FROM_CLAUSE = @FROM_CLAUSE  + ' INNER JOIN  dbo.ZNodeProductType h ON a.ProductTypeID =  h.ProductTypeID'



				SET @FROM_CLAUSE = @FROM_CLAUSE  + ' LEFT OUTER Join  ZNodeLuceneGlobalProductBoost i ON a.ProductID =  i.ProductID'

				SET @OrderByDefault = 'Boost desc '



				SET @L_GroupByClause = ''



                --SET @L_GroupByClause = 'GROUP BY a.[ProductID],a.[Name],a.[ShortDescription],a.[ProductNum],a.[ProductTypeID],'



                --SET @L_GroupByClause = @L_GroupByClause + ' a.[RetailPrice],a.[SalePrice],a.[WholesalePrice],a.[ImageFile],a.[ImageAltTag],a.[CallForPricing],'



                --SET @L_GroupByClause = @L_GroupByClause + 'a.[HomepageSpecial],a.[CategorySpecial],a.[TaxClassID],a.[PortalID],a.[ActiveInd],a.[Franchisable],e.[Name],i.Boost,h.NAME'



                SET @L_SPfilterClause = ''

                IF ( @L_innerWhereClause IS NOT NULL OR LEN(@L_innerWhereClause) >= 1) 



					BEGIN 



						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ProductID'+ '~~','a.[ProductID]')



						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'Name'+ '~~','a.[Name]')



						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ProductNumber'+ '~~','a.[ProductNum]')



						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'SKU'+ '~~','b.[SKU]')



						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ManufactureName'+ '~~','c.[Name]')



						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ManufacturerID'+ '~~','a.ManufacturerID')



						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ProductTypeID'+ '~~','a.[ProductTypeID]')



						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ProductType'+ '~~','h.[Name]')



						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'CategoryName'+ '~~','e.[Name]')



						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'PortalID'+ '~~','e.[PortalID]')



						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ManufacturerID'+ '~~','a.ManufacturerID')



						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'CatalogId'+ '~~','f.CatalogId')



						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'CategoryID'+ '~~','d.CategoryID')



						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'PortalID'+ '~~','g.PortalID')



						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'AccountId'+ '~~','a.AccountId')



				    END

            END ; 



    IF @SPName = 'ZNode_SearchCustomerPricingProduct' 



      BEGIN

				IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID('ZnodeSearchCustomerPricingProduct'))



					DROP TABLE ZnodeSearchCustomerPricingProduct



				create table   ZnodeSearchCustomerPricingProduct (ProductID int ,ExternalID int ,SKU Varchar(1000),Name Varchar(1000) ,BasePrice money ,NegotiatedPrice money ,Discount money ,AccountID  int )



				Declare @SSql1 Varchar(1000)



				IF ( LEN(@WhereClause) >= 1) 



				BEGIN



					SET @WhereClause = REPLACE(@WhereClause,'~~' + 'Name' + '~~',' @Name')



					SET @WhereClause = REPLACE(@WhereClause,'~~' + 'ExternalID' + '~~',' @ExternalID')



					SET @WhereClause = REPLACE(@WhereClause,'~~' + 'SKU' + '~~','@SKU')



					SET @WhereClause = REPLACE(@WhereClause,'~~' + 'ManufacturerId' + '~~','@ManufacturerId')



					SET @WhereClause = REPLACE(@WhereClause,'~~' + 'CategoryId' + '~~','@CategoryId')



					SET @WhereClause = REPLACE(@WhereClause,'~~' + 'PortalID' + '~~','@PortalID')



					SET @WhereClause = REPLACE(@WhereClause,'~~' + 'AccountID' + '~~','@AccountID')



					SET @WhereClause = Replace(Replace(Replace(REPLACE(REPLACE(REPLACE(REPLACE(@WhereClause,' and ',','),'>','='),'<','='),'like','='),'==','='),' or ',''),'%','')



					--SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'and',',')



					--SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'like','=')



					--SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'%','')

					set @SSql1 = ' ZNode_SearchCustomerPricingProduct '+ @WhereClause  



					insert into ZnodeSearchCustomerPricingProduct (ProductID ,ExternalID ,SKU ,Name ,BasePrice ,NegotiatedPrice ,Discount ,AccountID ) 



					Exec (@SSql1 )



				END 



				SET @Select_Clause = ' ProductID ,ExternalID ,SKU ,Name ,BasePrice ,NegotiatedPrice ,Discount ,AccountID '



				SET @Select_FROM_CLAUSE = ' ProductID ,ExternalID ,SKU ,Name ,BasePrice ,NegotiatedPrice ,Discount ,AccountID '



                SET @FROM_CLAUSE = ' FROM  ZnodeSearchCustomerPricingProduct  ' 



				SET @OrderByDefault =  'ProductID '



				SET @L_GroupByClause = ''



				SET @L_SPfilterClause = ''



                SET @L_innerWhereClause = ''



            END ;

        IF @SPName = 'ZNode_GetAvailableProfiles' 



        BEGIN

				IF (@L_inLineWhereClause IS NOT NULL OR LEN(@L_inLineWhereClause) >= 1) 



				BEGIN



					SET @L_inLineWhereClause = REPLACE(@L_inLineWhereClause,'~~' + 'AccountID'+ '~~','Isnull(AccountID,0)')



					SET @L_inLineWhereClause = 'AND ProfileId  not  in (select ProfileId from ZNodeAccountProfile where  '+ @L_inLineWhereClause  +')'



				END ;



			    SET @Select_Clause		= 'ProfileID,DefaultExternalAccountNo,Name,UseWholesalePricing,EmailList,TaxExempt,TaxClassID,ShowPricing,ShowOnPartnerSignup,Weighting'



                SET @Select_FROM_CLAUSE = 'ProfileID,DefaultExternalAccountNo,Name,UseWholesalePricing,EmailList,TaxExempt,TaxClassID,ShowPricing,ShowOnPartnerSignup,Weighting'



                SET @FROM_CLAUSE =		  'from ZNodeProfile '



                SET @OrderByDefault   =   'NAME'



                SET @L_GroupByClause  =   ''



                SET @L_SPfilterClause =   @L_inLineWhereClause 

                IF ( @L_innerWhereClause IS NOT NULL



                     OR LEN(@L_innerWhereClause) >= 1



        ) 

                    BEGIN



                SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'NAME'+ '~~','NAME')



            SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'ProfileID'+ '~~','ProfileID')



                    END ;



        END ;


           IF @SPName = 'ZNode_GetAssociatedProfile' 



          BEGIN



	            SET @Select_Clause		= 'AccountProfileID,ProfileID,DefaultExternalAccountNo,Name,UseWholesalePricing,EmailList,TaxExempt,TaxClassID,ShowPricing,ShowOnPartnerSignup,Weighting'



                SET @Select_FROM_CLAUSE = 'a.AccountProfileID,b.ProfileID,b.DefaultExternalAccountNo,b.Name,b.UseWholesalePricing,b.EmailList,b.TaxExempt,b.TaxClassID,b.ShowPricing,b.ShowOnPartnerSignup,b.Weighting'



                SET @FROM_CLAUSE =		 ' from ZNodeAccountProfile a inner join ZNodeProfile b on a.profileid = b.Profileid   '



                SET @OrderByDefault   =   'NAME'



                SET @L_GroupByClause  =   ''



                SET @L_SPfilterClause =   ''

                IF ( @L_innerWhereClause IS NOT NULL



                     OR LEN(@L_innerWhereClause) >= 1



        ) 



                    BEGIN



                        SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'AccountId'+ '~~','a.AccountId')



                    END ;



            END ;



            IF @SPName = 'ZNode_GetProductPriceListByPortalID' 



            BEGIN



				IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID('ZNodeGetProductPriceListByPortalID'))



				DROP TABLE ZNodeGetProductPriceListByPortalID



				Create table ZNodeGetProductPriceListByPortalID (Name NVARCHAR(MAX),ProductID INT,ProductNum NVARCHAR(MAX),ShortDescription NVARCHAR(MAX),RetailPrice money,PortalID INT,SalePrice money,WholeSalePrice money,NegotiatedPrice money)



				Declare @SSql Varchar(1000)



				IF ( LEN(@L_innerWhereClause) >= 1) 



				BEGIN

					SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'PortalId' + '~~',' @PortalId')



					SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'Name' + '~~',' @Name')



					SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'ProductNum' + '~~',' @ProductNum')



					SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'Sku' + '~~',' @Sku')



					SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'Brand' + '~~',' @Brand')



					SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'Category' + '~~',' @Category')



					SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'CatalogId' + '~~',' @CatalogId')



					SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'FilterPortalID' + '~~',' @FilterPortalID')



					SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'ExternalAccountNo' + '~~',' @ExternalAccountNo')

					--SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,' and ',' , ')



					--SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'like',' = ')



					--SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'%','')


					SET @L_innerWhereClause = Replace(Replace(Replace(REPLACE(REPLACE(REPLACE(REPLACE(@L_innerWhereClause,' and ',','),'>','='),'<','='),'like','='),'==','='),' or ',''),'%','')

					set @SSql = ' ZNode_GetProductPriceListByPortalID '+ @L_innerWhereClause  



					insert into ZNodeGetProductPriceListByPortalID(Name ,ProductID ,ProductNum ,ShortDescription ,RetailPrice ,PortalID,SalePrice ,WholeSalePrice ,NegotiatedPrice) 



					Exec (@SSql )



				END 



				Else 



				Begin



				insert into ZNodeGetProductPriceListByPortalID(Name ,ProductID ,ProductNum ,ShortDescription ,RetailPrice ,PortalID,SalePrice ,WholeSalePrice ,NegotiatedPrice ) 



					Exec ZNode_GetProductPriceListByPortalID '','','',''



				END 



				SET @Select_Clause = ' Name ,ProductID ,ProductNum ,ShortDescription ,RetailPrice ,PortalID,SalePrice ,WholeSalePrice ,NegotiatedPrice'



				SET @Select_FROM_CLAUSE = ' Name ,ProductID ,ProductNum ,ShortDescription ,RetailPrice ,PortalID,SalePrice ,WholeSalePrice ,NegotiatedPrice '



                SET @FROM_CLAUSE = ' FROM  ZNodeGetProductPriceListByPortalID ' 



				SET @OrderByDefault =  'ProductID '



				SET @L_GroupByClause = ''



				SET @L_SPfilterClause = ''

             SET @L_innerWhereClause = ''



            END ;


			IF @SPName = 'ZNodeSearchProductsByKeyword' 



            BEGIN



				IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID('ZNodeSearchProductsByKeyword1'))



				DROP TABLE ZNodeSearchProductsByKeyword1



				Create table ZNodeSearchProductsByKeyword1 (ProductID int ,SKU Varchar(300), ImageFile Varchar(300),Name Varchar(300) ,ProductNum Varchar(300) ,DisplayOrder int , ActiveInd bit ,PortalID   int )



				--Declare @SSql Varchar(1000)



				IF ( LEN(@L_innerWhereClause) >= 1) 



				BEGIN


					SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'sku' + '~~',' @Keyword')



					SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'MANUFACTURERID' + '~~',' @MANUFACTURERID')



					SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'PRODUCTTYPEID' + '~~',' @PRODUCTTYPEID')



					SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'CategoryID' + '~~',' @CategoryID')



					SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'PortalId' + '~~',' @PortalId')

					SET @L_innerWhereClause = Replace(Replace(Replace(REPLACE(REPLACE(REPLACE(REPLACE(@L_innerWhereClause,' and ',','),'>','='),'<','='),'like','='),'==','='),' or ',''),'%','')


					set @SSql = ' ZNode_SearchProductsByKeyword '+ @L_innerWhereClause  



					insert into ZNodeSearchProductsByKeyword1(ProductID,SKU,ImageFile,Name,ProductNum,DisplayOrder,ActiveInd,PortalID ) 



					Exec (@SSql )



				END 



				Else 



				Begin



				insert into ZNodeSearchProductsByKeyword1(ProductID,SKU,ImageFile,Name,ProductNum,DisplayOrder,ActiveInd,PortalID ) 



					Exec ZNode_SearchProductsByKeyword '','','',''



				END 



				SET @Select_Clause = ' ProductID,SKU,ImageFile,Name,ProductNum,DisplayOrder,ActiveInd,PortalID ,IsActive '



				SET @Select_FROM_CLAUSE = ' ProductID,SKU,ImageFile,Name,ProductNum,DisplayOrder,ActiveInd ,PortalID ,ActiveInd IsActive'



                SET @FROM_CLAUSE = ' FROM  ZNodeSearchProductsByKeyword1 ' 



				SET @OrderByDefault =  'ProductID '



				SET @L_GroupByClause = ''



				SET @L_SPfilterClause = ''

             SET @L_innerWhereClause = ''



            END ;

			IF @SPName = 'Znode_TaxClass' 



            BEGIN



			    SET @Select_Clause = 'TaxClassID,Name,DisplayOrder,ActiveInd,PortalID,ExternalID, StoreName '



                SET @Select_FROM_CLAUSE = 'a.TaxClassID,a.Name,a.DisplayOrder,a.ActiveInd,a.PortalID,a.ExternalID, ISNULL(b.StoreName ,''All Stores'') StoreName '



                SET @FROM_CLAUSE = ' FROM ZnodeTaxClass a Left outer join ZnodePortal b on a.PortalId = b.PortalId '



                SET @OrderByDefault = 'Name'



                SET @L_GroupByClause = ''



                SET @L_SPfilterClause = ''



				if(@L_inLineWhereClause != '')



 					SET @L_SPfilterClause =  @L_inLine



 				else 



 					SET @L_SPfilterClause =  ''



                IF ( @L_innerWhereClause IS NOT NULL OR LEN(@L_innerWhereClause) >= 1) 



				BEGIN



					SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'PortalId'+ '~~','a.PortalId')



					SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'TaxClassID'+ '~~','a.TaxClassID')



					SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'Name'+ '~~','a.Name')



					SET @L_innerWhereClause = REPLACE(REPLACE(REPLACE(@L_innerWhereClause,'~~'+ 'IsActive'+ '~~',' a.ActiveInd '),'true',1),'false',0)



                END ;



		END ;

        IF @SPName = 'ZnodeGetProfilewithSerialNumber' 



            BEGIN

                SET @Select_Clause = 'SerialNumber , Name,PortalProfileID,PortalID,ProfileID'



                SET @Select_FROM_CLAUSE = ' Cast(ROW_NUMBER() OVER (ORDER BY a.name) as int) AS SerialNumber , a.Name,b.PortalProfileID,b.PortalID,b.ProfileID '



				SET @FROM_CLAUSE = ' from znodeprofile a inner join znodeportalprofile b on a.profileid = b.profileid '



			    SET @OrderByDefault = ' name'



                SET @L_GroupByClause = ''



                SET @L_SPfilterClause = ''

                IF ( @L_innerWhereClause IS NOT NULL OR LEN(@L_innerWhereClause) >= 1) 



                    BEGIN



                        SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'PortalID'+ '~~','b.PortalID')



                        SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ProfileID'+ '~~','b.ProfileID')



                        SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'Name'+ '~~','a.name')



                    END ;



            END ;

        IF @SPName = 'ZnodeGetShippingList' 



            BEGIN



                SET @Select_Clause = 'ShippingID,ShippingTypeID,ProfileID,ShippingCode,HandlingCharge,DestinationCountryCode,Description,ActiveInd,DisplayOrder,ExternalID,ShippingTypeName,ProfileName'



                SET @Select_FROM_CLAUSE = ' ZS.ShippingID,ZS.ShippingTypeID,ZS.ProfileID,ZS.ShippingCode,ZS.HandlingCharge,ZS.DestinationCountryCode,ZS.Description,ZS.ActiveInd,ZS.DisplayOrder,ZS.ExternalID,ZST.Name ShippingTypeName,ZPF.Name ProfileName'

				SET @FROM_CLAUSE = ' from znodeShipping zs inner join  znodePortalProfile zpp on zs.ProfileId = zpp.ProfileId '



				SET @FROM_CLAUSE = @FROM_CLAUSE  + ' Inner join ZnodeShippingType ZST on ZS.ShippingTypeID = ZST.ShippingTypeID ' 



				SET @FROM_CLAUSE = @FROM_CLAUSE  + ' Inner join ZnodeProfile ZPF on ZS.ProfileID = ZPF.ProfileID ' 

			    SET @OrderByDefault = ' ShippingID'



                SET @L_GroupByClause = ''



                SET @L_SPfilterClause = ''

                IF ( @L_innerWhereClause IS NOT NULL OR LEN(@L_innerWhereClause) >= 1) 



                    BEGIN



                        SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'PortalID'+ '~~','ZPP.PortalID')



                        SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ProfileID'+ '~~','zpp.ProfileID')



                        SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ShippingTypeID'+ '~~','zs.ShippingTypeID')



                    END ;



            END ;

			IF @SPName = 'ZnodeGetChildProduct' 



            BEGIN

                SET @Select_Clause = 'ParentChildProductID,ParentProductID,ChildProductID,ProductName,IsActive'



                SET @Select_FROM_CLAUSE = ' a.ParentChildProductID,a.ParentProductID,a.ChildProductID,b.Name as ProductName ,b.ActiveInd IsActive '



				SET @FROM_CLAUSE = ' from ZNodeParentChildProduct a inner join ZnodeProduct b on a.ChildProductId = b.ProductId  '

			    SET @OrderByDefault = 'ProductName'



                SET @L_GroupByClause = ''



                SET @L_SPfilterClause = ''

                IF ( @L_innerWhereClause IS NOT NULL OR LEN(@L_innerWhereClause) >= 1) 



                    BEGIN



                        SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ParentProductID'+ '~~','a.ParentProductID')



                        SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ChildProductID'+ '~~','a.ChildProductID')



                        SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ProductName'+ '~~','b.Name')



                    END ;



            END ;



			IF @SPName = 'ZnodeGetWebpagesProfile' 



            BEGIN



                SET @Select_Clause = 'PortalId,StoreName,CompanyName'



                SET @Select_FROM_CLAUSE = ' Distinct a.PortalId,a.StoreName,a.CompanyName '



				SET @FROM_CLAUSE = ' from ZNODEPORTAL   a inner join webpages_profile   b on a.portalid = b.portalid  OR b.Portalid =0  inner join  Webpages_Users c on  b.UserId = c.UserId  '



			    SET @OrderByDefault = 'PortalId'



                SET @L_GroupByClause = ''



                SET @L_SPfilterClause = ''

               if(@L_inLineWhereClause != '')



 					SET @L_SPfilterClause =  @L_inLine



 				else 



 					SET @L_SPfilterClause =  ''



                IF ( @L_innerWhereClause IS NOT NULL OR LEN(@L_innerWhereClause) >= 1) 

                IF ( @L_innerWhereClause IS NOT NULL OR LEN(@L_innerWhereClause) >= 1) 



                    BEGIN



                        SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'UserName'+ '~~','c.UserName')



                        SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'StoreName'+ '~~','a.StoreName')



                        SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'CompanyName'+ '~~','a.CompanyName')



                        SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'PortalId'+ '~~','a.PortalId')



                    END ;

            END ;

		IF @SPName = 'ZNode_SearchVendorOrder' 

            BEGIN

				IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID('ZNodeSearchVendorOrder'))



				DROP TABLE ZNodeSearchVendorOrder



				Create table ZNodeSearchVendorOrder(OrderID INT,PortalId  int,AccountID int,OrderStateID int,ShippingID   int,PaymentTypeId int,PaymentStatusID  int                              



													,OrderStatus Varchar(50),ShippingTypeName Varchar(50),PaymentTypeName Varchar(50),PaymentStatusName Varchar(50)



													,BillingFirstName Varchar(50),BillingLastName Varchar(50),Total money,OrderDate  datetime,ShipDate  datetime,TrackingNumber Varchar(100),



													ActualOrderStatus Varchar(50)



											       )

				IF ( @L_innerWhereClause IS NOT NULL OR LEN(@L_innerWhereClause) >= 1) 



				BEGIN
					SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'ORDERID' + '~~',' @ORDERID')



					SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'BILLINGFIRSTNAME' + '~~',' @BILLINGFIRSTNAME')



					SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'BILLINGLASTNAME' + '~~',' @BILLINGLASTNAME')



					SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'BILLINGCOMPANYNAME' + '~~',' @BILLINGCOMPANYNAME')



					SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'ACCOUNTID' + '~~',' @ACCOUNTID ')



					SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'STARTDATE' + '~~',' @STARTDATE')



					SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'ENDDATE' + '~~',' @ENDDATE')



					SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'ORDERSTATEID' + '~~','@ORDERSTATEID')



					SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'PORTALID' + '~~',' @PORTALID')



					SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'portalIds' + '~~',' @portalIds')



					SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'LoggedAccountId' + '~~',' @LoggedAccountId')

					SET @L_innerWhereClause = Replace(Replace(Replace(REPLACE(REPLACE(REPLACE(REPLACE(@L_innerWhereClause,' and ',','),'>','='),'<','='),'like','='),'==','='),' or ',''),'%','')

				END ;



				Set @SSql = ''



				if @L_innerWhereClause  = ''



					set @SSql = ' [ZNode_SearchVendorOrder] @source=1 '



				else 

					set @SSql = ' [ZNode_SearchVendorOrder] @source=1, '+ @L_innerWhereClause  

				insert into ZNodeSearchVendorOrder(	OrderID ,PortalId  ,AccountID ,OrderStateID ,ShippingID   ,PaymentTypeId ,PaymentStatusID                                



													,OrderStatus ,ShippingTypeName ,PaymentTypeName ,PaymentStatusName 



													,BillingFirstName ,BillingLastName ,Total ,OrderDate  ,ShipDate  ,TrackingNumber,ActualOrderStatus ) 



				 Exec (@SSql )


			SET @Select_Clause = ' OrderID ,PortalId  ,AccountID ,OrderStateID ,ShippingID   ,PaymentTypeId ,PaymentStatusID'



			SET @Select_Clause = @Select_Clause  + ' ,OrderStatus ,ShippingTypeName ,PaymentTypeName ,PaymentStatusName '



			SET @Select_Clause = @Select_Clause  + ' ,BillingFirstName ,BillingLastName ,Total ,OrderDate  ,ShipDate  ,TrackingNumber ,ActualOrderStatus'

			SET @Select_FROM_CLAUSE = ' OrderID ,PortalId  ,AccountID ,OrderStateID ,ShippingID   ,PaymentTypeId ,PaymentStatusID'



			SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE  + ' ,OrderStatus ,ShippingTypeName ,PaymentTypeName ,PaymentStatusName '



			SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE  + ' ,BillingFirstName ,BillingLastName ,Total ,OrderDate  ,ShipDate  ,TrackingNumber ,ActualOrderStatus'

			SET @FROM_CLAUSE = ' FROM  ZNodeSearchVendorOrder ' 



			SET @OrderByDefault =  ' OrderID Desc '



			SET @L_GroupByClause = ''



			SET @L_SPfilterClause = ''

            SET @L_innerWhereClause = ''



            END ;


         IF @SPName = 'ZNodeApplicationSetting' 



            BEGIN


                SET @Select_Clause = 'Id,GroupName,ItemName,Setting,CreatedDate,ModifiedDate,CreatedBy,ModifiedBy,CreatedByName,ModifiedByName,ViewOptions,FrontPageName,FrontObjectName,IsCompressed,OrderByFields'



                SET @Select_FROM_CLAUSE = 'Id,GroupName,ItemName,Setting,CreatedDate,ModifiedDate,CreatedBy,ModifiedBy,CreatedByName,ModifiedByName,ViewOptions,FrontPageName,FrontObjectName,IsCompressed,OrderByFields'



                SET @FROM_CLAUSE = 'from ZNodeApplicationSetting'



                SET @OrderByDefault = 'Id'



                SET @L_GroupByClause = ''



                SET @L_SPfilterClause = ''



                IF ( @L_innerWhereClause IS NOT NULL OR LEN(@L_innerWhereClause) >= 1) 



                    BEGIN



                        SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'Id'+ '~~','Id')



						SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'GroupName'+ '~~','GroupName')



                        SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'ItemName'+ '~~','ItemName')



                        SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'Setting' + '~~','Setting')



                        SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~' + 'ViewOptions' + '~~','ViewOptions')



                    END ;



            END ;


		--IF @SPName = 'ZNodeExportFullAccountDetails' 



  --      BEGIN



  --          --ZPP.PortalID, 



  --          	seT @L_inLine = ''



		--		 SET @L_inLineWhereClause = REPLACE(@L_inLineWhereClause,'~~' + 'UserName'+ '~~','Isnull(UserName,'''')')



		--		 if(@L_inLineWhereClause != '')



		--			Begin



		--				SET @L_inLine  = ' and (isnull(ZPP.PortalId  ,0) in '



		--				SET @L_inLine = @L_inLine + ' (select a.PortalId from webpages_profile a inner join WebPages_users b  on a.UserId  = b.UserId where  '+ @L_inLineWhereClause +' ) '



		--				SET @L_inLine = @L_inLine + ' OR (ZPP.PortalId  =ZPP.PortalId  and '



		--				SET @L_inLine = @L_inLine + ' (select top 1  a.PortalId from webpages_profile a inner join WebPages_users b  on a.UserId  = b.UserId where  '+ @L_inLineWhereClause+' ) <= 0'



		--				SET @L_inLine = @L_inLine + ' ) )'



		--			END


		--			--, ParentAccountID



		--		SET @Select_Clause = '   AccountID,UserID,FirstName, LastName,PhoneNumber,Email,EnableCustomerPricing ,ProfileName,EmailOptIn'



		--		SET @Select_FROM_CLAUSE = ' distinct  za.AccountID,WUS.UserName UserID,ISNULL(za2.FirstName,'''') FirstName , Isnull(za2.LastName,'''') LastName ,za2.PhoneNumber,za.Email,'



		--		SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE  + ' za.EnableCustomerPricing,ZP.Name ProfileName,Za.EmailOptIn '



		--		SET @FROM_CLAUSE =  ' FROM ZNodeAccount za Left Outer JOIN  webpages_UsersInRoles wuir ON za.UserID = wuir.UserId Left Outer JOIN webpages_Roles wr ON wuir.RoleId = wr.RoleId '



		--		SET @FROM_CLAUSE =  @FROM_CLAUSE + ' LEFT OUTER JOIN ZNodeAddress za2 ON za.AccountID = za2.AccountID  ' 



		--		SET @FROM_CLAUSE =  @FROM_CLAUSE + ' LEFT OUTER JOIN webpages_users WUS ON za.UserID = WUS.UserID  ' 



		--		SET @FROM_CLAUSE =  @FROM_CLAUSE + ' LEFT OUTER JOIN dbo.ZNodeAccountProfile ZAP ON za.AccountID = Zap.AccountID '



		--		SET @FROM_CLAUSE =  @FROM_CLAUSE + ' LEFT OUTER JOIN dbo.ZNodePortalProfile ZPP ON ZAP.ProfileID = ZPP.ProfileID '



		--		SET @FROM_CLAUSE =  @FROM_CLAUSE + ' LEFT OUTER JOIN dbo.webpages_Membership wpm ON za.UserID = wpm.userid'



		--		SET @FROM_CLAUSE =  @FROM_CLAUSE + ' LEFT OUTER JOIN dbo.ZNodeProfile ZP ON ZP.ProfileID = ZPP.ProfileID '

		--		SET @L_GroupByClause= ''

		--		SET @OrderByDefault = 'AccountID Desc'


  --              --SET @L_GroupByClause = ''



  --              if @L_inLineWhereClause IS NOT NULL OR LEN(@L_inLineWhereClause) >= 1



		--			SET @L_SPfilterClause =   ' and za2.IsDefaultBilling=1 ' + @L_inLine 



  --              else 



		--			SET @L_SPfilterClause =   ' and za2.IsDefaultBilling=1 ' 


		--		--PC.ProductID = @PRODUCTID AND ( ZP.PortalId = @PORTALID OR @PORTALID IS NULL)    



  --              IF ( @L_innerWhereClause IS NOT NULL OR LEN(@L_innerWhereClause) >= 1 ) 



		--		Begin



  --                  SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'RoleName'+ '~~','wr.RoleName')



  --                  SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'FirstName'+ '~~','za2.FirstName')



  --                  SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'LastName'+ '~~','za2.LastName')



  --                  SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ProfileID'+ '~~','ZPP.ProfileID')



  --                  SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'RoleName'+ '~~','wr.RoleName')



  --                  SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'LoginName'+ '~~','WUS.UserName')



		--		    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'AccountID'+ '~~','za.AccountID')



  --                  SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ExternalAccountNum'+ '~~','za.ExternalAccountNo')



		--		    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'PhoneNumber'+ '~~','za2.PhoneNumber')



  --                  SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'Email'+ '~~','za.Email')



  --                  SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ReferralStatus'+ '~~','za.ReferralStatus')



  --                  SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'PortalID'+ '~~','ZPP.PortalID')



  --                  SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'CreateDate'+ '~~','Convert(date,ZA.CreateDte)')



  --                  SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'CompanyName'+ '~~','za.CompanyName')



  --                  SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'PostalCode'+ '~~','za2.PostalCode')


  --               END

  --          END ;


          	IF @SPName = 'ZNodeExportFullAccountDetails' 



        BEGIN



            --ZPP.PortalID, 



            	seT @L_inLine = ''



				 SET @L_inLineWhereClause = REPLACE(@L_inLineWhereClause,'~~' + 'UserName'+ '~~','Isnull(UserName,'''')')



				 if(@L_inLineWhereClause != '')



					Begin



						SET @L_inLine  = ' and (isnull(ZPP.PortalId  ,0) in '



						SET @L_inLine = @L_inLine + ' (select a.PortalId from webpages_profile a inner join WebPages_users b  on a.UserId  = b.UserId where  '+ @L_inLineWhereClause +' ) '



						SET @L_inLine = @L_inLine + ' OR (ZPP.PortalId  =ZPP.PortalId  and '



						SET @L_inLine = @L_inLine + ' (select top 1  a.PortalId from webpages_profile a inner join WebPages_users b  on a.UserId  = b.UserId where  '+ @L_inLineWhereClause+' ) <= 0'



						SET @L_inLine = @L_inLine + ' ) )'



					END

					--, ParentAccountID



				SET @Select_Clause = '   AccountID,UserID,FirstName, LastName,PhoneNumber,Email,EnableCustomerPricing ,ProfileName,EmailOptIn'



				SET @Select_FROM_CLAUSE = ' distinct  za.AccountID,WUS.UserName UserID,ISNULL(za2.FirstName,'''') FirstName , Isnull(za2.LastName,'''') LastName ,za2.PhoneNumber,za.Email,'



				SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE  + ' za.EnableCustomerPricing,ZP.Name ProfileName,Za.EmailOptIn'

				--SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE  + ' Substring((SELECT DISTINCT Stuff((SELECT Cast( p.Name AS NVARCHAR)+ '',''FROM'   



				--SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE  + ' ZNodeProfile p   inner join ZNodeAccountProfile ap on(p.ProfileId = ap.ProfileId) Where za.AccountID =  ap.AccountID ORDER  BY p.Name'



				--SET @Select_FROM_CLAUSE = @Select_FROM_CLAUSE  + ' FOR XML PATH('''')), 1, 0, '''')), 1, Len((SELECT DISTINCT Stuff((SELECT Cast( p.Name AS NVARCHAR) + '','' FROM   ZNodeProfile p  inner join ZNodeAccountProfile ap on(p.ProfileId = ap.ProfileId) Where za.AccountID =  ap.AccountID ORDER  BY p.Name FOR XML PATH('''')), 1, 0, ''''))) - 1)   ProfileName'

				SET @FROM_CLAUSE =  ' FROM ZNodeAccount za Left Outer JOIN  webpages_UsersInRoles wuir ON za.UserID = wuir.UserId Left Outer JOIN webpages_Roles wr ON wuir.RoleId = wr.RoleId '



				SET @FROM_CLAUSE =  @FROM_CLAUSE + ' LEFT OUTER JOIN ZNodeAddress za2 ON za.AccountID = za2.AccountID  ' 



				SET @FROM_CLAUSE =  @FROM_CLAUSE + ' LEFT OUTER JOIN webpages_users WUS ON za.UserID = WUS.UserID  ' 



				SET @FROM_CLAUSE =  @FROM_CLAUSE + ' LEFT OUTER JOIN dbo.ZNodeAccountProfile ZAP ON za.AccountID = Zap.AccountID '



				SET @FROM_CLAUSE =  @FROM_CLAUSE + ' LEFT OUTER JOIN dbo.ZNodePortalProfile ZPP ON ZAP.ProfileID = ZPP.ProfileID '



				SET @FROM_CLAUSE =  @FROM_CLAUSE + ' LEFT OUTER JOIN dbo.webpages_Membership wpm ON za.UserID = wpm.userid'

				SET @FROM_CLAUSE =  @FROM_CLAUSE + ' LEFT OUTER JOIN dbo.ZNodeProfile ZP ON ZP.ProfileID = ZPP.ProfileID '

				SET @L_GroupByClause= ''

				SET @OrderByDefault = 'AccountID Desc'

                --SET @L_GroupByClause = ''



                if @L_inLineWhereClause IS NOT NULL OR LEN(@L_inLineWhereClause) >= 1



					SET @L_SPfilterClause =   ' and WUS.UserName  is not null  and za2.IsDefaultBilling=1 ' + @L_inLine 



                else 

					SET @L_SPfilterClause =   ' and WUS.UserName  is not null   and za2.IsDefaultBilling=1 ' 

				--PC.ProductID = @PRODUCTID AND ( ZP.PortalId = @PORTALID OR @PORTALID IS NULL)    



                IF ( @L_innerWhereClause IS NOT NULL OR LEN(@L_innerWhereClause) >= 1 ) 



				Begin



                    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'RoleName'+ '~~','wr.RoleName')



                    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'FirstName'+ '~~','za2.FirstName')



                    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'LastName'+ '~~','za2.LastName')



                    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ProfileID'+ '~~','ZPP.ProfileID')



                    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'RoleName'+ '~~','wr.RoleName')



                    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'LoginName'+ '~~','WUS.UserName')



				    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'AccountID'+ '~~','za.AccountID')



                    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ExternalAccountNum'+ '~~','za.ExternalAccountNo')



				    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'PhoneNumber'+ '~~','za2.PhoneNumber')



                    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'Email'+ '~~','za.Email')



                    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ReferralStatus'+ '~~','za.ReferralStatus')



                    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'PortalID'+ '~~','ZPP.PortalID')



                    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'CreateDate'+ '~~','Convert(date,ZA.CreateDte)')



                    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'CompanyName'+ '~~','za.CompanyName')



                    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'PostalCode'+ '~~','za2.PostalCode')

                 END

            END ;  


            IF @SPName = 'ZNode_GetFrequentlyBoughtProduct' 
			Begin	
				IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID('ZNodeGetFrequentlyBoughtProduct'))
					DROP TABLE ZNodeGetFrequentlyBoughtProduct

				create table   ZNodeGetFrequentlyBoughtProduct (id int, ProductID int  ,NAME varchar(200), CrossSellProdName1 varchar(500) , CrossSellProdName2 varchar(500))

            	--Declare @SSql1 Varchar(1000)
				IF ( LEN(@WhereClause) >= 1) 
				BEGIN
					SET @WhereClause = REPLACE(@WhereClause,'~~' + 'PortalId' + '~~',' @PortalId')
					SET @WhereClause = REPLACE(@WhereClause,'~~' + 'CategoryId' + '~~',' @CategoryId')
					SET @WhereClause = REPLACE(@WhereClause,'~~' + 'ProductID' + '~~','@ProductID')
					SET @WhereClause = REPLACE(@WhereClause,'~~' + 'ProductName' + '~~','@ProductName')
					SET @WhereClause = REPLACE(@WhereClause,'~~' + 'ProductNum' + '~~','@ProductNum')
					SET @WhereClause = REPLACE(@WhereClause,'~~' + 'SKU' + '~~','@SKU')
					SET @WhereClause = Replace(Replace(Replace(REPLACE(REPLACE(REPLACE(REPLACE(@WhereClause,' and ',','),'>','='),'<','='),'like','='),'==','='),' or ',''),'%','')
					
					set @SSql1 = ' ZNode_GetFrequentlyBoughtProduct '+ @WhereClause  

					insert into ZNodeGetFrequentlyBoughtProduct (id, ProductID,NAME,CrossSellProdName1,CrossSellProdName2) 
					Exec (@SSql1 )
					print @SSql1 
				END 

				SET @Select_Clause = 'id, ProductID,ProductName ,FrequentlyBoughtProduct1,FrequentlyBoughtProduct2 '
				SET @Select_FROM_CLAUSE = ' id,ProductID,NAME ProductName ,CrossSellProdName1  FrequentlyBoughtProduct1 ,CrossSellProdName2  FrequentlyBoughtProduct2 '
                SET @FROM_CLAUSE = ' FROM ZNodeGetFrequentlyBoughtProduct  '
				SET @OrderByDefault =  'id '
				SET @L_GroupByClause = ''
				SET @L_SPfilterClause = ''
                SET @L_innerWhereClause = ''
            END ;



            IF @SPName = 'ZNode_GetCrossSellProductType_5' 
			Begin	
				IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID('ZNodeGetCrossSellProductType_5'))
					DROP TABLE ZNodeGetCrossSellProductType_5

				create table   ZNodeGetCrossSellProductType_5 (id int, ProductID int  ,NAME nvarchar(max), CrossSellProdName nvarchar(max))

            	--Declare @SSql1 Varchar(1000)

				IF ( LEN(@WhereClause) >= 1) 
				BEGIN
					SET @WhereClause = REPLACE(@WhereClause,'~~' + 'PortalId' + '~~',' @PortalId')
					SET @WhereClause = REPLACE(@WhereClause,'~~' + 'CategoryId' + '~~',' @CategoryId')
					SET @WhereClause = REPLACE(@WhereClause,'~~' + 'ProductID' + '~~','@ProductID')
					SET @WhereClause = REPLACE(@WhereClause,'~~' + 'ProductName' + '~~','@ProductName')
					SET @WhereClause = REPLACE(@WhereClause,'~~' + 'ProductNum' + '~~','@ProductNum')
					SET @WhereClause = REPLACE(@WhereClause,'~~' + 'SKU' + '~~','@SKU')
					SET @WhereClause = Replace(Replace(Replace(REPLACE(REPLACE(REPLACE(REPLACE(@WhereClause,' and ',','),'>','='),'<','='),'like','='),'==','='),' or ',''),'%','')

					set @SSql1 = ' ZNode_GetCrossSellProductType_5 '+ @WhereClause  

					insert into ZNodeGetCrossSellProductType_5 (id, ProductID,NAME,CrossSellProdName) 
					Exec (@SSql1 )
				END 

				SET @Select_Clause = 'id, ProductID,ProductName,CrossSellProducts'
				SET @Select_FROM_CLAUSE = ' id, ProductID,NAME ProductName,CrossSellProdName CrossSellProducts'
                SET @FROM_CLAUSE = ' FROM ZNodeGetCrossSellProductType_5  '
				SET @OrderByDefault =  'id '
				SET @L_GroupByClause = ''
				SET @L_SPfilterClause = ''
                SET @L_innerWhereClause = ''
            END ;

			IF @SPName = 'PRFT_CreditApplicationList' 
            BEGIN
            	SET @Select_Clause = 'CreditApplicationID,BusinessName,IsTaxExempt,RequestDate,IsCreditApproved,CreditDays'
				SET @Select_FROM_CLAUSE = ' prftc.[CreditApplicationID] ,prftc.[BusinessName], prftc.[IsTaxExempt], prftc.[RequestDate] ,prftc.[IsCreditApproved] ,prftc.[CreditDays]' 
				SET @FROM_CLAUSE = ' FROM PRFTCreditApplication  prftc  '
        		SET @OrderByDefault = ' CreditApplicationID DESC'
				SET @L_GroupByClause = ''
				SET @L_SPfilterClause =   ''

				if(@L_inLineWhereClause != '')
					SET @L_SPfilterClause =  @L_inLine
				else 
					SET @L_SPfilterClause =  ''
				
				IF ( @L_innerWhereClause IS NOT NULL OR LEN(@L_innerWhereClause) >= 1) 
				BEGIN
					SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'BusinessName'+ '~~','prftc.BusinessName')
					SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'CreditApplicationID'+ '~~','prftc.CreditApplicationID')
					SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'IsCreditApproved'+ '~~','prftc.[IsCreditApproved]')
				END
            END ;


			IF @SPName = 'PRFT_InventoryList' 
            BEGIN
            	SET @Select_Clause = 'ProductID,Name,ProductNum'
				SET @Select_FROM_CLAUSE = ' prftinv.[ProductID] ,prftinv.[Name], prftinv.[ProductNum]' 
				SET @FROM_CLAUSE = ' FROM ZNodeProduct  prftinv  '
        		SET @OrderByDefault = ' ProductID '
				SET @L_GroupByClause = ''
				SET @L_SPfilterClause =   ''

				if(@L_inLineWhereClause != '')
					SET @L_SPfilterClause =  @L_inLine
				else 
					SET @L_SPfilterClause =  ''

				IF ( @L_innerWhereClause IS NOT NULL OR LEN(@L_innerWhereClause) >= 1) 
				BEGIN
					SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ProductID'+ '~~','prftinv.[ProductID]')
					SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'name'+ '~~','prftinv.[Name]')
					SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ProductNum'+ '~~','prftinv.[ProductNum]')
				END
            END ;
            
            IF @SPName = 'PRFT_GetCustomerUserMappingList' 
            BEGIN
            	SET @Select_Clause = 'CustomerUserMappingId,AccountID,Email,FullName,PhoneNumber,ExternalAccountNo,CompanyName'
				SET @Select_FROM_CLAUSE = ' cu.[CustomerUserMappingId], acc.[AccountID], acc.[Email], addr.[FirstName] + addr.[LastName] as FullName, addr.[PhoneNumber],acc.[ExternalAccountNo],addr.[CompanyName]' 
				SET @FROM_CLAUSE = ' FROM PRFT_CustomerUserMapping cu INNER JOIN ZNodeAccount acc ON cu.[CustomerAccountId]=acc.[AccountID] INNER JOIN ZNodeAddress addr ON acc.[AccountID]=addr.[AccountID] '
        		SET @OrderByDefault = ' [AccountID]'
				SET @L_GroupByClause = ''
				SET @L_SPfilterClause =   ''

				if(@L_inLineWhereClause != '')
					SET @L_SPfilterClause =  @L_inLine
				else 
					SET @L_SPfilterClause =  ''
				
				IF ( @L_innerWhereClause IS NOT NULL OR LEN(@L_innerWhereClause) >= 1) 
				BEGIN
					SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'AccountID'+ '~~','cu.[UserAccountid]')
					SET @L_innerWhereClause = @L_innerWhereClause + ' and addr.[IsDefaultBilling]=1'
				END
            END ;
            
            IF @SPName = 'PRFT_GetSubUserList' 
            BEGIN
            	SET @Select_Clause = 'CustomerUserMappingId,AccountID,Email,ExternalAccountNo,IsActive'
				SET @Select_FROM_CLAUSE = ' cu.[CustomerUserMappingId], acc.[AccountID], acc.[Email],acc.[ExternalAccountNo],acc.[ActiveInd] as IsActive' 
				SET @FROM_CLAUSE = ' FROM PRFT_CustomerUserMapping cu INNER JOIN ZNodeAccount acc ON cu.[UserAccountId]=acc.[AccountID]'
        		SET @OrderByDefault = ' [AccountID]'
				SET @L_GroupByClause = ''
				SET @L_SPfilterClause =   ''

				if(@L_inLineWhereClause != '')
					SET @L_SPfilterClause =  @L_inLine
				else 
					SET @L_SPfilterClause =  ''
				
				IF ( @L_innerWhereClause IS NOT NULL OR LEN(@L_innerWhereClause) >= 1) 
				BEGIN
					SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'parentaccountid'+ '~~','cu.[CustomerAccountId]')
					--SET @L_innerWhereClause = @L_innerWhereClause + ' and addr.[IsDefaultBilling]=1'
				END
            END ;
            
            IF @SPName = 'PRFT_GetNotAssociatedCustomerList' 
            BEGIN
				IF (@L_inLineWhereClause IS NOT NULL OR LEN(@L_inLineWhereClause) >= 1) 

				BEGIN
					SET @L_inLineWhereClause = REPLACE(@L_inLineWhereClause,'~~' + 'AccountID'+ '~~','UserAccountid')
					SET @L_inLineWhereClause = ' and acc.[AccountID] NOT IN (select CustomerAccountId from PRFT_CustomerUserMapping WHERE '+ @L_inLineWhereClause  +')'					
				END ;
									
            	SET @Select_Clause = 'AccountID,ParentAccountID,FirstName,LastName,Email,PhoneNumber,ExternalAccountNo'
				SET @Select_FROM_CLAUSE = 'acc.[AccountID], acc.[ParentAccountID], addr.[FirstName], addr.[LastName],acc.[Email], addr.[PhoneNumber],acc.[ExternalAccountNo]' 
				SET @FROM_CLAUSE = ' FROM znodeaccount acc inner join znodeaddress addr ON acc.AccountId=addr.AccountId '									
				SET @FROM_CLAUSE =  @FROM_CLAUSE + ' LEFT OUTER JOIN webpages_users WUS ON acc.UserID = WUS.UserID  ' 
				SET @FROM_CLAUSE =  @FROM_CLAUSE + ' LEFT OUTER JOIN dbo.ZNodeAccountProfile ZAP ON acc.AccountID = Zap.AccountID '
				SET @FROM_CLAUSE =  @FROM_CLAUSE + ' LEFT OUTER JOIN dbo.ZNodePortalProfile ZPP ON ZAP.ProfileID = ZPP.ProfileID '
				SET @FROM_CLAUSE =  @FROM_CLAUSE + ' LEFT OUTER JOIN dbo.webpages_Membership wpm ON acc.UserID = wpm.userid'

        		SET @OrderByDefault = ' [AccountID]'
				SET @L_GroupByClause = ''				
				SET @L_SPfilterClause =   @L_inLineWhereClause 
				
			
				IF (len(isnull(@L_innerWhereClause,'')) > 0) 
				Begin       

                    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'FirstName'+ '~~','addr.FirstName')
                    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'LastName'+ '~~','addr.LastName') 
                    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ProfileID'+ '~~','ZPP.ProfileID')
                    --SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'RoleName'+ '~~','wr.RoleName')
                    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'LoginName'+ '~~','WUS.UserName')    
					SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'AccountID'+ '~~','acc.AccountID')									
                    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ExternalAccountNum'+ '~~','acc.ExternalAccountNo')
                    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ExternalAccountNo'+ '~~','acc.ExternalAccountNo')
				    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'PhoneNumber'+ '~~','addr.PhoneNumber')
                    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'Email'+ '~~','acc.Email')
                    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'CompanyName'+ '~~','acc.CompanyName')
                    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'ReferralStatus'+ '~~','acc.ReferralStatus')
                    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'PortalID'+ '~~','ZPP.PortalID')
                    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'CreateDate'+ '~~','Convert(date,acc.CreateDte)')
                    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'CompanyName'+ '~~','acc.CompanyName')
                    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'PostalCode'+ '~~','addr.PostalCode')
                    SET @L_innerWhereClause = REPLACE(@L_innerWhereClause,'~~'+ 'PostalCode'+ '~~','addr.PostalCode')
					
                    SET @L_innerWhereClause = @L_innerWhereClause + ' and acc.custom2=1'
					SET @L_innerWhereClause = @L_innerWhereClause + ' and addr.[IsDefaultBilling]=1'
					

                 END
                 ELSE
					Begin

						SET @L_innerWhereClause = ' acc.custom2=1'
						SET @L_innerWhereClause = @L_innerWhereClause + ' and addr.[IsDefaultBilling]=1'
					End																
            END ;

			
  	    IF LEN(@L_innerWhereClause) > 0 
            SET @L_innerWhereClause = ' WHERE ' + @L_innerWhereClause

		-- default WHERE if where clause is not passed
        IF ( @L_innerWhereClause IS NULL OR LEN(@L_innerWhereClause) < 1) 
			SET @L_innerWhereClause = ' WHERE 1=1 ' ; 

        IF LEN(@HavingClause) > 0
            SET @L_HavingClause = ' WHERE ' + @HavingClause

		-- default WHERE if where clause is not passed
        IF ( @HavingClause IS NULL OR LEN(@HavingClause) < 1) 
			SET @L_HavingClause = ' WHERE 1=1 ' ; 

        IF LEN(@OrderBy) > 0 --and @OrderBy <> '' 
            SET @OrderBy_Output = ' ' + @OrderBy
	
		-- default order  if order clause is not passed
        IF ( @OrderBy IS NULL  OR LEN(@OrderBy) < 1 ) 
			SET @OrderBy_Output = ' ' + @OrderByDefault ; 

        IF @Flag = 0 
            SET @Sql = ' FROM  (SELECT ' + @Select_FROM_CLAUSE + ' ' + @FROM_CLAUSE + ' ' + @L_innerWhereClause + ' '  + @L_SPfilterClause + ' ' + @L_GroupByClause

                + '  ) TempDataDetails  ' + @L_HavingClause + ' '
        ELSE 
            BEGIN
                SET @Sql = ' Select * FROM  (SELECT ' + @Select_FROM_CLAUSE
                    + ' ' + @FROM_CLAUSE + ' ' + @L_innerWhereClause + ' ' + @L_SPfilterClause + ' ' + @L_GroupByClause
					+ '  ) TempDataDetails  ' + @L_HavingClause + ' '	
            END   
    END
