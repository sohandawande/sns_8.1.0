GO
/****** Object:  StoredProcedure [dbo].[ZNode_DataDetailsSpConfigExtn]    Script Date: 1/6/2016 7:24:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =================================================================================
-- Project Name:	Znode 80
-- Description:		Grid View & List View setting 
-- InParameter:		Product table parameters and SKU tables 
-- OutParameter:	1/0	
-- Created Date:	2015-01-22
-- Revision History:
-- Modified Date Modified By version Description
--					: Version 80.0
-- Unit Level Testing 
-- =============================================
CREATE PROCEDURE [dbo].[ZNode_DataDetailsSpConfigExtn]

    (
     @SPName NVARCHAR(MAX),
    @WhereClause NVARCHAR(MAX) = NULL,
    @HavingClause NVARCHAR(MAX) = NULL,



      @OrderBy NVARCHAR(MAX) = NULL,



      @PageIndex INT = 1,



      @PageSize INT = NULL,



      @TotalRowCount INT OUTPUT,



      @PreFix NVARCHAR(MAX) = NULL,
      @InnerWhereClause NVARCHAR(MAX) = NULL
    )



AS 



    BEGIN



	-- Define Local Varible And Intialize Default Value 



        DECLARE @PageLowerBound INT					



        DECLARE @PageUpperBound INT					



        DECLARE @PARMDEF NVARCHAR(100) = N'@TotalRowCount INT OUTPUT'



        DECLARE @Select_Clause NVARCHAR(MAX)            



        DECLARE @SQL AS NVARCHAR(MAX) 



        DECLARE @Query AS NVARCHAR(MAX) 



        DECLARE @L_PageSizeFilter AS NVARCHAR(MAX)



        DECLARE @L_PageSizeTop AS NVARCHAR(MAX)



        DECLARE @L_PageSize AS NVARCHAR(MAX)



        DECLARE @return_value INT



	    DECLARE @SPNameGraph AS NVARCHAR(100)



		



	    SET @SPNameGraph  = @SPName



	    SET @SPName = Replace(@SPName,'@','')  



	    



       if @SPName  in ( 'ZNodeReportsCouponFiltered','ZNode_ReportsOptIn','ZNode_ReportsRecurring','ZNodeReportsRecurringOrderLineItem',



       'ZNode_ReportsPopularFiltered','ZNode_ReportsPopularFiltered','ZNode_ReportsPicklist','ZNode_ReportsPicklistSubReport','ZNode_ReportsMain'



       ,'ZNode_ReportsProductSoldOnVendorSites','ZNode_ReportsSEOFiltered','ZNode_ReportsAffiliateFiltered','ZNode_ReportsFrequentFiltered','ZNode_ReportsReOrder',



       'ZNode_ReportsCustom','ZNode_ReportsCustomOrderLineItem','ZNode_ReportsVolumeFiltered','ZNode_ReportsTaxFiltered','ZNode_ReportsTaxFiltered_Monthly',



       'ZNode_ReportsTaxFiltered_quarter','ZNode_ReportsTaxFiltered_All','ZNode_GetRMARequestReport','ZNode_ReportsSupplierList','ZNode_ReportsAccounts','ZNode_ReportsActivityLog',



       'ZNode_ReportsFeedback','ZNode_ReportVendorRevenue','ZNode_ReportVendorProductRevenue','ZNode_ReportsSupplierListFor80')



       Begin 



		if (left(@SPNameGraph  ,1) = '@'	)



			EXEC @return_value = [dbo].[ZNode_ReportDataDetailsSpConfigWithoutCurrency] @SPName = @SPName,



				@WhereClause = @WhereClause, @HavingClause = @HavingClause,



				@OrderBy = @OrderBy, @PreFix = @PreFix, @Flag = 0,



				@WhereClause1 = @InnerWhereClause,



				@Select_Clause = @Select_Clause OUTPUT, @Sql = @Query OUTPUT,



				@OrderBy_Output = @OrderBy OUTPUT


		 Else



			EXEC @return_value = [dbo].[ZNode_ReportDataDetailsSpConfig_getQuery] @SPName = @SPName,



				@WhereClause = @WhereClause, @HavingClause = @HavingClause,



				@OrderBy = @OrderBy, @PreFix = @PreFix, @Flag = 0,



				@WhereClause1 = @InnerWhereClause,



				@Select_Clause = @Select_Clause OUTPUT, @Sql = @Query OUTPUT,



				@OrderBy_Output = @OrderBy OUTPUT



	   END 



	   Else 



		If @SPName in ( 'ZNodeGetCustomerBasedPricing','ZNode_SearchCustomerPricingProduct' ,'ZNode_GetProductPriceListByPortalID' , 'ZNodeSearchProductsByKeyword' ,'ZNode_SearchVendorOrder' , 'ZNode_GetFrequentlyBoughtProduct' ,



							'ZNodeGetProductByCriteriaWithBoost','ZNodeGetDistinctProductsByCriteria','ZNodeGetVendorProduct','ZNodeAccountDetails','ZNode_GetCrossSellProductType_5' ,'ZNodeFullAccountDetails',



							'ZNodeGetProductWithProductBoost','ZNodeGetSKUProfileEffective','ZNodeGetSKUFacetGroup','ZNodeGetVendorProductforMallAdmin','ZNode_GetAvailableAddOn',



							'ZNode_GetAvailableHighLights','ProductTypeWiseAttributes','Znode_TaxClass','ZNodeAdminAccounts','ZNodeGetImageType','ZNode_GetAvailableProfiles','ZNodeExportFullAccountDetails','ZNodeSearchAddons'



							,'ZnodeGetWebpagesProfile','PRFT_CreditApplicationList') --PRFT Changes - Added PRFT_CreditApplicationList



		Begin 



			--EXEC @return_value = [dbo].[ZNode_DataDetailsSpConfig_getQuery] @SPName = @SPName, --Existing znode call is replaced with PRFT Call
			EXEC @return_value = [dbo].[ZNode_DataDetailsSpConfig_getQueryExtn] @SPName = @SPName,


			@WhereClause = @WhereClause, @HavingClause = @HavingClause,



			@OrderBy = @OrderBy, @PreFix = @PreFix, @Flag = 0,



			@WhereClause1 = @InnerWhereClause,



			@Select_Clause = @Select_Clause OUTPUT, @Sql = @Query OUTPUT,



			@OrderBy_Output = @OrderBy OUTPUT







			END 	  



		Else 



		   Begin 



				EXEC @return_value = [dbo].[Znode_DataViewDetailsSpConfig_getQuery] @SPName = @SPName,



					@WhereClause = @WhereClause, @HavingClause = @HavingClause,



					@OrderBy = @OrderBy, @PreFix = @PreFix, @Flag = 0,



					@WhereClause1 = @InnerWhereClause,



					@Select_Clause = @Select_Clause OUTPUT, @Sql = @Query OUTPUT,



					@OrderBy_Output = @OrderBy OUTPUT		       



			END 	  



			--PRINT 'MRR Hack ..................'+   @Select_Clause 



			--PRINT 'MRR Hack ..................'+   @Sql 



		 --   PRINT 'MRR Hack ..................'+   @Sql 







	-- no of record in one page are not specified then all records will be come



        IF @PageSize <= 0



            OR @PageSize IS  NULL 



            BEGIN



                SET @PageLowerBound = 0



                SET @PageUpperBound = NULL 



                SET @L_PageSizeFilter = ' ' 



                SET @L_PageSizeTop = ' ' 



            END



   



   -- no of record in one page are  specified then according to that records will be come	 



        IF @PageSize > 0



            AND @PageSize IS NOT NULL 



            BEGIN



                SET @PageLowerBound = @PageSize * ( @PageIndex - 1 )



                SET @PageUpperBound = @PageLowerBound + @PageSize 



                SET @L_PageSizeFilter = ' AND RowIndex <= '



                    + CONVERT(NVARCHAR, @PageUpperBound)



                SET @L_PageSizeTop = ' TOP '



                    + CONVERT(NVARCHAR, @PageUpperBound)



            END



	-- get records according to filter condtion and specified no of record in one page



        SET @SQL = 'WITH PageIndex AS ( SELECT ' + @L_PageSizeTop



            + ' ROW_NUMBER() OVER (ORDER BY ' + @OrderBy + ') as RowIndex ,'



            + @Select_Clause + @Query + ' ) SELECT  ' + @Select_Clause



            + ' FROM PageIndex WHERE RowIndex > '



            + CONVERT(NVARCHAR, @PageLowerBound) + @L_PageSizeFilter



            + ' ORDER BY ' + @OrderBy 







	 -- to check query made or not made  ==> Select @sql  



        EXEC sp_executesql @SQL



		--PRINT 'MRR Hack ..................'+   @Sql 



  -- get row count of total record of query



        SET @SQL = 'SELECT @TotalRowCount = COUNT(*)  ' + @Query



        EXEC sp_executesql @SQL, @PARMDEF, @TotalRowCount OUTPUT



    END
