

CREATE TABLE [dbo].[ZNodeCaseRequestExtn](
	[CaseExtnID] [int] IDENTITY(1,1) NOT NULL,
	[CaseID] [int] NOT NULL,
	[City] [nvarchar](max) NULL,
	[StateCode] [nvarchar](max) NULL,	
	[CountryCode] [nvarchar](max) NULL,
	[SalesRepID] [int] NULL,
	[Custom1] [nvarchar](max) NULL,
	[Custom2] [nvarchar](max) NULL,
	[Custom3] [nvarchar](max) NULL,
	[Custom4] [nvarchar](max) NULL,
	[Custom5] [nvarchar](max) NULL
 CONSTRAINT [PK_CaseExtnID] PRIMARY KEY CLUSTERED 
(
	[CaseExtnID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[ZNodeCaseRequestExtn]  WITH NOCHECK ADD  CONSTRAINT [FK_CaseRequestExtn_CaseRequest] FOREIGN KEY([CaseID])
REFERENCES [dbo].[ZNodeCaseRequest] ([CaseID])

ALTER TABLE [dbo].[ZNodeCaseRequestExtn]  WITH NOCHECK ADD  CONSTRAINT [FK_CaseRequestExtn_Account] FOREIGN KEY([SalesRepID])
REFERENCES [dbo].[ZNodeAccount] ([AccountID])

