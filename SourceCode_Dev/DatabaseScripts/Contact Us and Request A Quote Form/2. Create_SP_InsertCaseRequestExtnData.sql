-- =============================================
-- Author:		<Author,,Krunal Tule>
-- Create date: <Create Date,MMDDYYYY,11/5/2015>
-- Description:	<Description,,Insert Case Request Extn Data>
-- =============================================
CREATE PROCEDURE [dbo].[PRFT_InsertCaseRequestExtnData] 

@CaseId int,
@CountryCode  nvarchar(Max) =null,
@StateCode  nvarchar(Max) =null,
@City nvarchar(Max) =null,
@SalesRep int,
@FaxNumber nvarchar(Max)=null,
@DueDate datetime =null,
@ContactChoice nvarchar(Max) =null,
@Custom1 nvarchar(Max)=null,
@Custom2 nvarchar(Max)=null,
@Custom3 nvarchar(Max)=null,
@Custom4 nvarchar(Max)=null,
@Custom5 nvarchar(Max)=null

AS
BEGIN
	
	INSERT INTO [ZNodeCaseRequestExtn] ([CaseID],[City],[StateCode],[CountryCode],[SalesRepID],[FaxNumber],[DueDate],[ContactChoice],[Custom1],[Custom2],[Custom3],[Custom4],[Custom5])
    VALUES (@CaseId,@City,@StateCode,@CountryCode,@SalesRep,@FaxNumber,@DueDate,@ContactChoice,@Custom1,@Custom2,@Custom3,@Custom4,@Custom5)
	
END
