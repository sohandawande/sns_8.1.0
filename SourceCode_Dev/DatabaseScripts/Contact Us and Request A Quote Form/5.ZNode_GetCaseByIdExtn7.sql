-- =============================================
-- Author:		<Author,,Krunal Tule>
-- Create date: <Create Date,MMDDYYYY,11/5/2015>
-- Description:	<Description,,Get Case By Id Extn Data>
-- =============================================

Alter PROCEDURE [dbo].[ZNode_GetCaseByIdExtn] 
    (  
      @CASEID VARCHAR(100)  
     
    )  
AS   
    BEGIN      
 -- SET NOCOUNT ON added to prevent extra result sets from      
 -- interfering with SELECT statements.      
        SET NOCOUNT ON ;          
         
      
       
       
     Declare @City nvarchar(Max);Declare @Country nvarchar(Max);Declare @State nvarchar(Max);
     Declare @SalesRepName nvarchar(Max);Declare @CountryCode nvarchar(Max);Declare @StateCode nvarchar(Max);
     Declare @SalesRepID nvarchar(Max);Declare @FaxNumber nvarchar(Max);Declare @ContactChoice nvarchar(Max);
     Declare @DueDate datetime;Declare @Custom1 nvarchar(Max);Declare @Custom2 nvarchar(Max);
     Declare @Custom3 nvarchar(Max);Declare @Custom4 nvarchar(Max);Declare @Custom5 nvarchar(Max);
     
     
     SELECT @City = [City],@StateCode=[StateCode],@CountryCode =[CountryCode],@SalesRepID = [SalesRepID],@Custom1 =[Custom1],@Custom2 =[Custom2]
     ,@Custom3 =[Custom3],@Custom4 =[Custom4],@Custom5 =[Custom5],@FaxNumber =[FaxNumber],@ContactChoice=[ContactChoice],@DueDate=[DueDate]
	 FROM [ZNodeCaseRequestExtn] where [CaseID]=@CASEID
     
     
     Select @Country = [Name] from ZNodeCountry where [Code] = @CountryCode
     Select @State = [Name] from ZNodeState where [Code] = @StateCode
     SELECT @SalesRepName = wu.UserName FROM ZNodeAccount za INNER JOIN webpages_Users wu ON za.UserId = wu.UserId WHERE za.AccountId = @SalesRepID
    
     
     
     SELECT  R.[CaseID] as CaseRequestId,  
        R.PortalID,
                P.StoreName,  
                R.[AccountID],  
                R.[OwnerAccountID],  
                 S.CaseStatusID,  
                S.CaseStatusNme as CaseStatusName, 
                Py.CasePriorityID , 
                Py.CasePriorityNme as CasePriorityName,  
                R.[CaseTypeID],  
                R.[CaseOrigin],  
                R.[Title],  
                R.[Description],  
                R.[FirstName],  
                R.[LastName],  
                R.[CompanyName],  
                R.[EmailID] as [Email],  
                R.[PhoneNumber],  
                R.[CreateDte] as [CreateDate],  
                R.[CreateUser] 
                
                ,@City as [City]
                  
                ,@State as [StateCode]
				,@Country as [CountryCode]
				,@SalesRepName as [SalesRepName]
				  
				,@FaxNumber  as [FaxNumber]
				,@ContactChoice  as [ContactChoiceSelected]
				,@DueDate  as [DueDate]
				,@Custom1 as [Custom1]
				,@Custom2 as [Custom2]
				,@Custom3 as [Custom3]
				,@Custom4 as [Custom4]
				,@Custom5 as [Custom5]
        FROM      
   ZNodeCaseRequest  R, 
   ZNodePortal P,  
   ZNodeCasePriority Py,  
   ZNodeCaseStatus S  
        WHERE     
    R.PortalID=P.PortalID AND  
   R.CasePriorityID=Py.CasePriorityID AND  
   R.CaseStatusID=S.CaseStatusID AND     
   R.CaseID = @CASEID
    END
