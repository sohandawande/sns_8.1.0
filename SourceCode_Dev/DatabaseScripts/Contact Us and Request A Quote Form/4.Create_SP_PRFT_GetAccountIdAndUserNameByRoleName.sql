
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,Krunal Tule>
-- Create date: <Create Date,10/23/15>
-- Description:	<Description,To get the User data based on roles>
-- =============================================
CREATE PROCEDURE PRFT_GetAccountIdAndUserNameByRoleName

@RoleName nvarchar(Max)

AS
BEGIN
	
	SELECT za.AccountId,wu.UserName FROM ZNodeAccount za 
	INNER JOIN webpages_UsersInRoles wuir ON za.UserID = wuir.UserId 
	INNER JOIN webpages_Users wu ON za.UserId = wu.UserId 
	INNER JOIN webpages_Roles wr ON wuir.RoleId = wr.RoleId
	WHERE wr.RoleName = @RoleName
	
END
GO
