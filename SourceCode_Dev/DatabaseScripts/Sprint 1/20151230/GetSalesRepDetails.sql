
GO
/****** Object:  StoredProcedure [dbo].[PRFT_GetSalesRepInfoByRoleName]    Script Date: 12/30/2015 10:10:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================

-- Author:		<Author,Krunal Tule>

-- Create date: <Create Date,10/23/15>

-- Description:	<Description,To get the User data based on roles>

-- =============================================

ALTER PROCEDURE [dbo].[PRFT_GetSalesRepInfoByRoleName]

@RoleName nvarchar(Max)

AS

BEGIN

	

	SELECT za.AccountId as SalesRepID,zadd.FirstName+' '+zadd.LastName+' - '+zadd.City+','+zadd.StateCode+','+zadd.CountryCode as SaleRepNameWithLocation, za.Email 
	FROM ZNodeAccount za inner join ZNodeAddress zadd on za.AccountID=zadd.AccountID inner join 
	(
		select wu.UserId from webpages_Users wu INNER JOIN webpages_UsersInRoles wuir ON wu.UserID = wuir.UserId 

		INNER JOIN webpages_Roles wr ON wuir.RoleId = wr.RoleId
		WHERE wr.RoleName = @RoleName

	)membership on za.UserID = membership.UserId where zadd.IsDefaultBilling=1
END

