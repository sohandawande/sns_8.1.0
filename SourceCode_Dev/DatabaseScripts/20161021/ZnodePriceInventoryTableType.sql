USE [SnS_Stage_20160421]
GO
/****** Object:  UserDefinedTableType [dbo].[ZnodePriceInventoryTableType]    Script Date: 10/21/2016 16:10:19 ******/
CREATE TYPE [dbo].[ZnodePriceInventoryTableType] AS TABLE(
	[ProductId] [int] NULL,
	[ProductNum] [nvarchar](500) NULL,
	[ERPPrice] [money] NULL,
	[ERPInventory] [int] NULL
)
GO
