USE [SnS_Stage_20160421]
GO
/****** Object:  StoredProcedure [dbo].[PRFT_InsertPriceInventory]    Script Date: 10/21/2016 15:57:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- DROP PROCEDURE PRFT_InsertPriceInventory

CREATE PROCEDURE [dbo].[PRFT_InsertPriceInventory]
(
	@IPI ZnodePriceInventoryTableType READONLY

)
AS 
BEGIN
    SET NOCOUNT ON
    
	update zp
	set zp.RetailPrice = I.ERPPrice
	FROM @IPI I
	INNER JOIN ZNodeProduct zp on (zp.ProductID = I.productid)


	update zp
	set zp.Custom3 = I.ERPInventory
	FROM @IPI I
	INNER JOIN ZNodeProduct zp on (zp.ProductID = I.productid)

         
END
GO
