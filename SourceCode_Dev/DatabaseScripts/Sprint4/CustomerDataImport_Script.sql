USE [SnS_DataImport]
GO
/****** Object:  StoredProcedure [dbo].[PRFT_Update_ZnodeAccount]    Script Date: 01/14/2016 20:00:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Author:	Farhat Khan

-- Create date: 08 Jan 2016

-- Description:	Updating UserID 

-- =============================================

CREATE PROCEDURE [dbo].[PRFT_Update_ZnodeAccount] 

	@UserID nvarchar(MAX),

	@AccountId int

AS

BEGIN

	UPDATE

	 ZNodeAccount 

	 SET

	  Userid=@UserID 

	 WHERE 

		AccountID =  @AccountId

END
GO
/****** Object:  StoredProcedure [dbo].[PRFT_GetUser]    Script Date: 01/14/2016 20:00:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================

-- Author:	Farhat Khan

-- Create date: 08 Jan 2016

-- Description:	Fatching Eamil IDS 

-- =============================================

CREATE PROCEDURE [dbo].[PRFT_GetUser] 
	
AS

BEGIN

	SELECT AccountId, Email  

	FROM 

		ZNodeAccount

	WHERE

	 AccountID != 11521

	 AND UserID Is NULL

END
GO
