USE [master]
GO
/****** Object:  Database [znode_multifront_payment]    Script Date: 09/10/2015 16:32:42 ******/
CREATE DATABASE [znode_multifront_payment] 
GO
ALTER DATABASE [znode_multifront_payment] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [znode_multifront_payment].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [znode_multifront_payment] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [znode_multifront_payment] SET ANSI_NULLS OFF
GO
ALTER DATABASE [znode_multifront_payment] SET ANSI_PADDING OFF
GO
ALTER DATABASE [znode_multifront_payment] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [znode_multifront_payment] SET ARITHABORT OFF
GO
ALTER DATABASE [znode_multifront_payment] SET AUTO_CLOSE ON
GO
ALTER DATABASE [znode_multifront_payment] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [znode_multifront_payment] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [znode_multifront_payment] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [znode_multifront_payment] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [znode_multifront_payment] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [znode_multifront_payment] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [znode_multifront_payment] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [znode_multifront_payment] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [znode_multifront_payment] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [znode_multifront_payment] SET  ENABLE_BROKER
GO
ALTER DATABASE [znode_multifront_payment] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [znode_multifront_payment] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [znode_multifront_payment] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [znode_multifront_payment] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [znode_multifront_payment] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [znode_multifront_payment] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [znode_multifront_payment] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [znode_multifront_payment] SET  READ_WRITE
GO
ALTER DATABASE [znode_multifront_payment] SET RECOVERY SIMPLE
GO
ALTER DATABASE [znode_multifront_payment] SET  MULTI_USER
GO
ALTER DATABASE [znode_multifront_payment] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [znode_multifront_payment] SET DB_CHAINING OFF
GO
USE [znode_multifront_payment]
GO

/****** Object:  Table [dbo].[ZNodeGateway]    Script Date: 09/10/2015 16:32:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ZNodeGateway](
	[GatewayTypeID] [int] IDENTITY(1,1) NOT NULL,
	[GatewayName] [varchar](max) NOT NULL,
	[WebsiteURL] [varchar](max) NULL,
 CONSTRAINT [PK_SC_Gateway] PRIMARY KEY CLUSTERED 
(
	[GatewayTypeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[ZNodeGateway] ON
INSERT [dbo].[ZNodeGateway] ([GatewayTypeID], [GatewayName], [WebsiteURL]) VALUES (1, N'Authorize.Net', N'http://www.authorize.net')
INSERT [dbo].[ZNodeGateway] ([GatewayTypeID], [GatewayName], [WebsiteURL]) VALUES (2, N'TwoCheckout', N'')
INSERT [dbo].[ZNodeGateway] ([GatewayTypeID], [GatewayName], [WebsiteURL]) VALUES (3, N'CyberSource', N'http://www.cybersource.com')
INSERT [dbo].[ZNodeGateway] ([GatewayTypeID], [GatewayName], [WebsiteURL]) VALUES (4, N'Stripe', N'')
INSERT [dbo].[ZNodeGateway] ([GatewayTypeID], [GatewayName], [WebsiteURL]) VALUES (5, N'Paypal', N'http://www.paypal.com')
INSERT [dbo].[ZNodeGateway] ([GatewayTypeID], [GatewayName], [WebsiteURL]) VALUES (6, N'PaymentTech', NULL)
INSERT [dbo].[ZNodeGateway] ([GatewayTypeID], [GatewayName], [WebsiteURL]) VALUES (8, N'WorldPay', N'http://www.wordlpay.com')
SET IDENTITY_INSERT [dbo].[ZNodeGateway] OFF
/****** Object:  Table [dbo].[ZNodeActivityLog]    Script Date: 09/10/2015 16:32:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ZNodeActivityLog](
	[ActivityLogID] [int] IDENTITY(1,1) NOT NULL,
	[PaymentSettingID] [int] NULL,
	[CreateDte] [datetime] NOT NULL,
	[EndDte] [datetime] NULL,
	[PortalID] [int] NULL,
	[URL] [nvarchar](max) NULL,
	[Data1] [nvarchar](255) NULL,
	[Data2] [nvarchar](255) NULL,
	[Data3] [nvarchar](255) NULL,
	[Status] [nvarchar](255) NULL,
	[LongData] [nvarchar](max) NULL,
	[Source] [nvarchar](255) NULL,
	[Target] [nvarchar](255) NULL,
 CONSTRAINT [PK_ZNodeActivityLog] PRIMARY KEY CLUSTERED 
(
	[ActivityLogID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ZNodePaymentSetting]    Script Date: 09/10/2015 16:32:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ZNodePaymentSetting](
	[PaymentSettingID] [int] NOT NULL,
	[PaymentTypeID] [int] NOT NULL,
	[ProfileID] [int] NULL,
	[GatewayTypeID] [int] NULL,
	[GatewayUsername] [nvarchar](max) NULL,
	[GatewayPassword] [nvarchar](max) NULL,
	[EnableVisa] [bit] NULL,
	[EnableMasterCard] [bit] NULL,
	[EnableAmex] [bit] NULL,
	[EnableDiscover] [bit] NULL,
	[EnableRecurringPayments] [bit] NULL,
	[EnableVault] [bit] NULL,
	[TransactionKey] [nvarchar](max) NULL,
	[ActiveInd] [bit] NOT NULL,
	[DisplayOrder] [int] NOT NULL,
	[TestMode] [bit] NOT NULL,
	[Partner] [nvarchar](max) NULL,
	[Vendor] [nvarchar](max) NULL,
	[PreAuthorize] [bit] NOT NULL,
	[IsRMACompatible] [bit] NULL,
 CONSTRAINT [PK_SC_PaymentSetting] PRIMARY KEY CLUSTERED 
(
	[PaymentSettingID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[ZNodePaymentSetting] ([PaymentSettingID], [PaymentTypeID], [ProfileID], [GatewayTypeID], [GatewayUsername], [GatewayPassword], [EnableVisa], [EnableMasterCard], [EnableAmex], [EnableDiscover], [EnableRecurringPayments], [EnableVault], [TransactionKey], [ActiveInd], [DisplayOrder], [TestMode], [Partner], [Vendor], [PreAuthorize], [IsRMACompatible]) VALUES (30, 1, NULL, 1, NULL, NULL, 1, 1, 1, 1, NULL, NULL, NULL, 0, 1, 1, NULL, NULL, 1, 1)
/****** Object:  Table [dbo].[Znode_Transactions]    Script Date: 09/10/2015 16:32:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Znode_Transactions](
	[GUID] [uniqueidentifier] NOT NULL,
	[CustomerProfileId] [nvarchar](max) NULL,
	[CustomerPaymentId] [nvarchar](max) NULL,
	[TransactionId] [nvarchar](max) NULL,
	[TransactionDate] [datetime] NULL,
	[CaptureTransactionDate] [datetime] NULL,
	[RefundTransactionDate] [datetime] NULL,
	[ResponseText] [nvarchar](max) NULL,
	[ResponseCode] [nvarchar](50) NULL,
	[Custom1] [nvarchar](max) NULL,
	[Custom2] [nvarchar](max) NULL,
	[Custom3] [nvarchar](max) NULL,
	[Amount] [money] NULL,
	[PaymentSettingId] [int] NULL,
	[CurrencyCode] [nvarchar](50) NULL,
	[SubscriptionId] [nvarchar](max) NULL,
	[PaymentStatusId] [int] NULL,
 CONSTRAINT [PK_Znode_Transactions] PRIMARY KEY CLUSTERED 
(
	[GUID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Default [DF_ZNodeActivityLog_CreateDte]    Script Date: 09/10/2015 16:32:42 ******/
ALTER TABLE [dbo].[ZNodeActivityLog] ADD  CONSTRAINT [DF_ZNodeActivityLog_CreateDte]  DEFAULT (getdate()) FOR [CreateDte]
GO
/****** Object:  Default [DF_ZNodePaymentSetting_EnableRecurringPayments]    Script Date: 09/10/2015 16:32:42 ******/
ALTER TABLE [dbo].[ZNodePaymentSetting] ADD  CONSTRAINT [DF_ZNodePaymentSetting_EnableRecurringPayments]  DEFAULT ((0)) FOR [EnableRecurringPayments]
GO
/****** Object:  Default [DF_ZNodePaymentSetting_EnableVault]    Script Date: 09/10/2015 16:32:42 ******/
ALTER TABLE [dbo].[ZNodePaymentSetting] ADD  CONSTRAINT [DF_ZNodePaymentSetting_EnableVault]  DEFAULT ((0)) FOR [EnableVault]
GO
/****** Object:  Default [DF_ZNodePaymentSetting_PreAuthorize]    Script Date: 09/10/2015 16:32:42 ******/
ALTER TABLE [dbo].[ZNodePaymentSetting] ADD  CONSTRAINT [DF_ZNodePaymentSetting_PreAuthorize]  DEFAULT ((0)) FOR [PreAuthorize]
GO
/****** Object:  ForeignKey [FK_SC_PaymentSetting_SC_Gateway]    Script Date: 09/10/2015 16:32:42 ******/
ALTER TABLE [dbo].[ZNodePaymentSetting]  WITH CHECK ADD  CONSTRAINT [FK_SC_PaymentSetting_SC_Gateway] FOREIGN KEY([GatewayTypeID])
REFERENCES [dbo].[ZNodeGateway] ([GatewayTypeID])
GO
ALTER TABLE [dbo].[ZNodePaymentSetting] CHECK CONSTRAINT [FK_SC_PaymentSetting_SC_Gateway]
GO
/****** Object:  ForeignKey [FK_Znode_Transactions_ZNodePaymentSetting]    Script Date: 09/10/2015 16:32:42 ******/
ALTER TABLE [dbo].[Znode_Transactions]  WITH CHECK ADD  CONSTRAINT [FK_Znode_Transactions_ZNodePaymentSetting] FOREIGN KEY([PaymentSettingId])
REFERENCES [dbo].[ZNodePaymentSetting] ([PaymentSettingID])
GO
ALTER TABLE [dbo].[Znode_Transactions] CHECK CONSTRAINT [FK_Znode_Transactions_ZNodePaymentSetting]
GO
